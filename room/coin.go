// +build !headless

package room

import (
	"context"
	"sync"

	"git.lubar.me/ben/spy-cards/gfx"
)

var (
	coinStage *customStage
	coinErr   error
	coinOnce  sync.Once

	dieStage *customStage
	dieErr   error
	dieOnce  sync.Once
)

func loadCoin() {
	coinStage, coinErr = loadStage(context.Background(), "img/room/coin.stage")

	if coinStage != nil {
		coinStage.noDepth = true
	}
}

// PreloadCoin loads the coin 3D model.
func PreloadCoin() error {
	coinOnce.Do(loadCoin)

	return coinErr
}

// RenderCoin draws the coin 3D model to the screen.
func RenderCoin(cam *gfx.Camera) {
	coinOnce.Do(loadCoin)

	if coinErr == nil {
		coinStage.Render(cam)
	}
}

func loadDie() {
	dieStage, dieErr = loadStage(context.Background(), "img/room/carminadice.stage")

	if dieStage != nil {
		dieStage.noDepth = true
	}
}

// PreloadDie loads the die 3D model.
func PreloadDie() error {
	dieOnce.Do(loadDie)

	return dieErr
}

// RenderDie draws the die 3D model to the screen.
func RenderDie(cam *gfx.Camera) {
	dieOnce.Do(loadDie)

	if dieErr == nil {
		dieStage.Render(cam)
	}
}
