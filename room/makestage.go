// +build ignore

package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	name := os.Args[1]

	img, err := ioutil.ReadFile(name + ".jpeg")
	if err != nil {
		panic(err)
	}

	f, err := os.Open(name + ".obj")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	s := bufio.NewScanner(f)

	var (
		pos  [][3]float32
		tex  [][2]float32
		data []float32
		elem []uint16
		m    = make(map[[2]int]uint16)
	)

	for s.Scan() {
		parts := strings.Split(s.Text(), " ")

		switch parts[0] {
		case "#":
			// comment
			continue
		case "v":
			x, err := strconv.ParseFloat(parts[1], 32)
			if err != nil {
				panic(err)
			}

			y, err := strconv.ParseFloat(parts[2], 32)
			if err != nil {
				panic(err)
			}

			z, err := strconv.ParseFloat(parts[3], 32)
			if err != nil {
				panic(err)
			}

			pos = append(pos, [3]float32{float32(x), float32(y), float32(z)})
		case "vt":
			s, err := strconv.ParseFloat(parts[1], 32)
			if err != nil {
				panic(err)
			}

			t, err := strconv.ParseFloat(parts[2], 32)
			if err != nil {
				panic(err)
			}

			tex = append(tex, [2]float32{float32(s), float32(t)})
		case "l":
			// polyline (ignore)
		case "s":
			// smooth/sharp (ignore)
		case "f":
			for i := 1; i <= 3; i++ {
				var posi, texi int
				if _, err := fmt.Sscanf(parts[i], "%d/%d", &posi, &texi); err != nil {
					panic(err)
				}

				vi, ok := m[[2]int{posi, texi}]
				if !ok {
					vi = uint16(len(m))
					m[[2]int{posi, texi}] = vi
					data = append(data, pos[posi-1][:]...)
					data = append(data, tex[texi-1][:]...)
				}

				elem = append(elem, vi)
			}
		default:
			panic("unexpected waveform OBJ command " + parts[0])
		}
	}

	w, err := os.Create(name + ".stage")
	if err != nil {
		panic(err)
	}
	defer w.Close()

	var varint [binary.MaxVarintLen64]byte
	_, err = w.Write(varint[:binary.PutUvarint(varint[:], 0)])
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(elem)*2))])
	if err != nil {
		panic(err)
	}

	err = binary.Write(w, binary.LittleEndian, elem)
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(data)*4))])
	if err != nil {
		panic(err)
	}

	err = binary.Write(w, binary.LittleEndian, data)
	if err != nil {
		panic(err)
	}

	_, err = w.Write(varint[:binary.PutUvarint(varint[:], uint64(len(img)))])
	if err != nil {
		panic(err)
	}

	_, err = w.Write(img)
	if err != nil {
		panic(err)
	}
}
