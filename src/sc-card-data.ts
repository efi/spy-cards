module SpyCards.CardData {
	export type CardID = GlobalCardID | number;

	export enum GlobalCardID {
		Zombiant = 0,
		Jellyshroom = 1,
		Spider = 2,
		Zasp = 3,
		Cactiling = 4,
		Psicorp = 5,
		Thief = 6,
		Bandit = 7,
		Inichas = 8,
		Seedling = 9,
		Numbnail = 14,
		Mothiva = 15,
		Acornling = 16,
		Weevil = 17,
		VenusBud = 19,
		Chomper = 20,
		AcolyteAria = 21,
		Kabbu = 23,
		VenusGuardian = 24,
		WaspTrooper = 25,
		WaspBomber = 26,
		WaspDriller = 27,
		WaspScout = 28,
		Midge = 29,
		Underling = 30,
		MonsieurScarlet = 31,
		GoldenSeedling = 32,
		ArrowWorm = 33,
		Carmina = 34,
		SeedlingKing = 35,
		Broodmother = 36,
		Plumpling = 37,
		Flowerling = 38,
		Burglar = 39,
		Astotheles = 40,
		MotherChomper = 41,
		Ahoneynation = 42,
		BeeBoop = 43,
		SecurityTurret = 44,
		Denmuki = 45,
		HeavyDroneB33 = 46,
		Mender = 47,
		Abomihoney = 48,
		DuneScorpion = 49,
		TidalWyrm = 50,
		Kali = 51,
		Zombee = 52,
		Zombeetle = 53,
		TheWatcher = 54,
		PeacockSpider = 55,
		Bloatshroom = 56,
		Krawler = 57,
		HauntedCloth = 58,
		Warden = 61,
		JumpingSpider = 63,
		MimicSpider = 64,
		LeafbugNinja = 65,
		LeafbugArcher = 66,
		LeafbugClubber = 67,
		Madesphy = 68,
		TheBeast = 69,
		ChomperBrute = 70,
		Mantidfly = 71,
		GeneralUltimax = 72,
		WildChomper = 73,
		Cross = 74,
		Poi = 75,
		PrimalWeevil = 76,
		FalseMonarch = 77,
		Mothfly = 78,
		MothflyCluster = 79,
		Ironnail = 80,
		Belostoss = 81,
		Ruffian = 82,
		WaterStrider = 83,
		DivingSpider = 84,
		Cenn = 85,
		Pisci = 86,
		DeadLanderα = 87,
		DeadLanderβ = 88,
		DeadLanderγ = 89,
		WaspKing = 90,
		TheEverlastingKing = 91,
		Maki = 92,
		Kina = 93,
		Yin = 94,
		UltimaxTank = 95,
		Zommoth = 96,
		Riz = 97,
		Devourer = 98
	}

	export enum BossCardOrder {
		Spider,
		VenusGuardian,
		HeavyDroneB33,
		TheWatcher,
		TheBeast,
		UltimaxTank,
		MotherChomper,
		Broodmother,
		Zommoth,
		SeedlingKing,
		TidalWyrm,
		PeacockSpider,
		Devourer,
		FalseMonarch,
		Maki,
		WaspKing,
		TheEverlastingKing
	}

	export enum MiniBossCardOrder {
		Ahoneynation,
		AcolyteAria,
		Mothiva,
		Zasp,
		Astotheles,
		DuneScorpion,
		PrimalWeevil,
		Cross,
		Poi,
		GeneralUltimax,
		Cenn,
		Pisci,
		MonsieurScarlet,
		Kabbu,
		Kali,
		Carmina,
		Riz,
		Kina,
		Yin,
		DeadLanderα,
		DeadLanderβ,
		DeadLanderγ
	}

	export enum AttackerCardOrder {
		Seedling,
		Underling,
		GoldenSeedling,
		Zombiant,
		Zombee,
		Zombeetle,
		Jellyshroom,
		Bloatshroom,
		Chomper,
		ChomperBrute,
		Psicorp,
		ArrowWorm,
		Thief,
		Bandit,
		Burglar,
		Ruffian,
		SecurityTurret,
		Abomihoney,
		Krawler,
		Warden,
		HauntedCloth,
		Mantidfly,
		JumpingSpider,
		MimicSpider,
		DivingSpider,
		WaterStrider,
		Belostoss,
		Mothfly,
		MothflyCluster,
		WaspScout,
		WaspTrooper
	}

	export enum EffectCardOrder {
		Acornling,
		Cactiling,
		Flowerling,
		Plumpling,
		Inichas,
		Denmuki,
		Madesphy,
		Numbnail,
		Ironnail,
		Midge,
		WildChomper,
		Weevil,
		BeeBoop,
		Mender,
		LeafbugNinja,
		LeafbugArcher,
		LeafbugClubber,
		WaspBomber,
		WaspDriller,
		VenusBud
	}

	export const CardNameOverride: { [id: number]: string } = {
		[GlobalCardID.VenusBud]: "Venus' Bud",
		[GlobalCardID.AcolyteAria]: "Acolyte Aria",
		[GlobalCardID.VenusGuardian]: "Venus' Guardian",
		[GlobalCardID.WaspTrooper]: "Wasp Trooper",
		[GlobalCardID.WaspBomber]: "Wasp Bomber",
		[GlobalCardID.WaspDriller]: "Wasp Driller",
		[GlobalCardID.WaspScout]: "Wasp Scout",
		[GlobalCardID.MonsieurScarlet]: "Monsieur Scarlet",
		[GlobalCardID.GoldenSeedling]: "Golden Seedling",
		[GlobalCardID.ArrowWorm]: "Arrow Worm",
		[GlobalCardID.SeedlingKing]: "Seedling King",
		[GlobalCardID.MotherChomper]: "Mother Chomper",
		[GlobalCardID.BeeBoop]: "Bee-Boop",
		[GlobalCardID.SecurityTurret]: "Security Turret",
		[GlobalCardID.HeavyDroneB33]: "Heavy Drone B-33",
		[GlobalCardID.DuneScorpion]: "Dune Scorpion",
		[GlobalCardID.TidalWyrm]: "Tidal Wyrm",
		[GlobalCardID.TheWatcher]: "The Watcher",
		[GlobalCardID.PeacockSpider]: "Peacock Spider",
		[GlobalCardID.HauntedCloth]: "Haunted Cloth",
		[GlobalCardID.JumpingSpider]: "Jumping Spider",
		[GlobalCardID.MimicSpider]: "Mimic Spider",
		[GlobalCardID.LeafbugNinja]: "Leafbug Ninja",
		[GlobalCardID.LeafbugArcher]: "Leafbug Archer",
		[GlobalCardID.LeafbugClubber]: "Leafbug Clubber",
		[GlobalCardID.TheBeast]: "The Beast",
		[GlobalCardID.ChomperBrute]: "Chomper Brute",
		[GlobalCardID.GeneralUltimax]: "General Ultimax",
		[GlobalCardID.WildChomper]: "Wild Chomper",
		[GlobalCardID.PrimalWeevil]: "Primal Weevil",
		[GlobalCardID.FalseMonarch]: "False Monarch",
		[GlobalCardID.MothflyCluster]: "Mothfly Cluster",
		[GlobalCardID.WaterStrider]: "Water Strider",
		[GlobalCardID.DivingSpider]: "Diving Spider",
		[GlobalCardID.DeadLanderα]: "Dead Lander α",
		[GlobalCardID.DeadLanderβ]: "Dead Lander β",
		[GlobalCardID.DeadLanderγ]: "Dead Lander γ",
		[GlobalCardID.WaspKing]: "Wasp King",
		[GlobalCardID.TheEverlastingKing]: "The Everlasting King",
		[GlobalCardID.UltimaxTank]: "ULTIMAX Tank"
	};

	const vanillaCardByName: { [name: string]: GlobalCardID } = {};
	for (let card = 0; card < 128; card++) {
		if (!GlobalCardID[card]) {
			continue;
		}

		if (CardNameOverride[card]) {
			vanillaCardByName[CardNameOverride[card]] = card;
		} else {
			vanillaCardByName[GlobalCardID[card]] = card;
		}
	}
	// typo fixes
	vanillaCardByName["Belosstoss"] = GlobalCardID.Belostoss;

	export function cardByName(name: string): CardID {
		if (name.startsWith("Custom ")) {
			let rank: Rank;
			let index: number;
			if (name.startsWith("Custom Attacker #")) {
				rank = Rank.Attacker;
				index = parseInt(name.substr("Custom Attacker #".length), 10);
			} else if (name.startsWith("Custom Effect #")) {
				rank = Rank.Effect;
				index = parseInt(name.substr("Custom Effect #".length), 10);
			} else if (name.startsWith("Custom Mini-Boss #")) {
				rank = Rank.MiniBoss;
				index = parseInt(name.substr("Custom Mini-Boss #".length), 10);
			} else if (name.startsWith("Custom Boss #")) {
				rank = Rank.Boss;
				index = parseInt(name.substr("Custom Boss #".length), 10);
			} else {
				throw new Error("unknown card name: " + name);
			}

			if (index >= 1) {
				index--;
				return 128 + ((index & ~31) << 2) | (rank << 5) | (index & 31);
			}
		}

		if (vanillaCardByName.hasOwnProperty(name)) {
			return vanillaCardByName[name];
		}

		throw new Error("unknown card name: " + name);
	}
}
