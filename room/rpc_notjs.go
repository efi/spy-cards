// +build !js !wasm

package room

import (
	"context"

	"golang.org/x/xerrors"
)

// RunRPC implements the RPC compatibility layer for the JavaScript
// implementation of Spy Cards Online.
func RunRPC(ctx context.Context) error {
	return xerrors.Errorf("room: RPC is only available on js/wasm target")
}
