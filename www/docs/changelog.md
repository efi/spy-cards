---
title: Changelog
toc: false
...

## v0.2.80

- **Spy Cards:** Fixed numb not being able to target cards with 0 ATK and 0 DEF.

## v0.2.79

- **Spy Cards:** Max-size audience is now much rarer (1 in 200,000,000 rather than 1 in 20,000 for 100 members).

## v0.2.78

- **Spy Cards:** Optimized mine lightmap.

## v0.2.77

- **Spy Cards:** Numb now also considers the lower stat of cards with both ATK and DEF, so a card with 3 ATK 0 DEF will be numbed before a card with 3 ATK 1 DEF.

## v0.2.76

- **Spy Cards:** Greatly improved stage rendering performance for audience members standing near the camera.

## v0.2.75

- **Deck Editor:** Fixed deck corruption when entering something that is not a deck code into the importer.
- **Spy Cards:** Improved rendering performance.
- **Spy Cards:** Fixed inconsistencies in the random number generator.
- **Spy Cards:** Fixed several desyncs in singleplayer mode.

## v0.2.74

- **Spy Cards:** The "mine" background is now rendered in WebGL.

## v0.2.73

- **Card Editor:** Fixed Deck Limit Filter not properly displaying assigned tribes.
- **Deck Editor:** Fixed an issue with encoding decks that contained only custom cards and at least one card's number was above 32.
- **Spy Cards:** Fixed all but one card being dropped for player 2 if a variant is active in the custom client.
- **Spy Cards:** Improved performance when the Disable 3D Scenes option is turned on.

## v0.2.72

- **Card Editor:** Added the ability to force a round timer.

## v0.2.71

- **Spy Cards:** The "stage" background is now rendered in WebGL.

## v0.2.70

- **Termacade:** Added an emulated controller for touch screens.
- **Termacade:** Fixed a race condition if textured loaded while a frame was being rendered that could cause the wrong texture to be uploaded to the GPU.

## v0.2.69

- **Spy Cards:** Added an option to automatically upload recordings at the end of matches. This does not apply to matches where an NPC was playing.
- **Spy Cards:** Effects hidden by Flavor Text now run as a group rather than playing a sound and pausing for each effect.
- **Settings:** Clarified that "Clear Game Cache" does not affect saved data.
- **Settings:** Changed "Copy Control Set" to "New Control Set" based on feedback.
- **Settings:** Added High Scores Name.
- **Settings:** Added Disable CRT Effect.
- **Settings:** Added Always Show Buttons.
- **Settings:** Added Auto-Upload Recordings.
- Nice

## v0.2.68

- **Card Editor:** Tribes with names that begin with an underscore are hidden.
- **Card Editor:** Added Deck Limit Filter game rule type.
- **Spy Cards:** Cards with nested limit conditions now display the limit as a prefix to avoid confusion about where the limit is.
- **Termacade:** Improved priority order of preloading assets.

## v0.2.67

- **Spy Cards:** Improvements to variant handling that should fix some crashes.
- **Termacade:** The Termacade now runs using WebAssembly.

## v0.2.66

- **Spy Cards:** Stat changes within On Numb effects are applied directly to the player.

## v0.2.65

- **Spy Cards:** Fixed failure to connect on custom variants.
- **Card Editor:** Added "On Numb" condition.

## v0.2.64

- **Card Editor:** Page URL only updates after no edits have been made for 1 second. (This should fix browser lag issues when typing a long description.)
- **Spy Cards:** Singleplayer now uses a fake matchmaking server to avoid singleplayer-specific connection edge cases.

## v0.2.63

- **Card Editor:** Added "Unfilter Card" rule, which prevents a card from being targeted by filters.

## v0.2.62

- **Spy Cards:** Fixed NPC players processing cards as if they were numbed permanently rather than just for the turn.

## v0.2.61

- **Spy Cards:** Fixed Empower Opponent inconsistently targeting players.

## v0.2.60

- **Card Editor:** Fixed Setup effects on the opponent with even numbers of turns applying to the current player instead.
- **Match Viewer:** Added game log.

## v0.2.59

- **Spy Cards:** Cards that do replacement summons are no longer considered to exist after they perform the summon. (This matches Bug Fables.)
- **Spy Cards:** "If Card" effect now allows numbed cards to satisfy the condition.
- **Card Editor:** Individual card editors no longer load immediately. Instead, a button to edit each card is available. This cuts load times in half for large card sets.

## v0.2.58

- **Spy Cards:** Removed STUN servers from singleplayer in an attempt to make local connection attempts more reliable.
- **Spy Cards:** (hopefully) fixed a race condition when a connection was established too quickly.

## v0.2.57

- **Card Editor:** Added a new condition type: "Priority Override". This forces an effect to run at a different stage of processing than it normally would.

## v0.2.56

- **Flower Journey:** Added the random seed to the game over screen.
- ~~**Mite Knight:** Experimentally reduced speed of certain timers by one half in an attempt to better match Bug Fables.~~

## v0.2.55

- **Spy Cards:** Fixed custom client always using variant 0 from the joining player's perspective.
- **Spy Cards:** Fixed setup field not showing up until the first turn was confirmed.
- **Match Viewer:** Fixed a crash when a desync caused no player to win by the end of the match.

## v0.2.54

- **Spy Cards:** Custom cards that cost infinity TP and are also banned no longer display the TP icon and are no longer listed in the banned cards list.

## v0.2.53

- **Card Editor:** Improved performance when loading a large card set.
- **Card Editor:** Improved performance when editing a card in a large card set.
- **Card Editor:** Improved performance when opening the card selector.

## v0.2.52

- **Spy Cards:** Added some internal logging to help diagnose singleplayer desyncs.

## v0.2.51

- **Spy Cards:** Made the effect processor more resilient against timing-related desynchronization in cases where an effect would cause multiple other effects to occur.

## v0.2.50

- **Spy Cards:** Fixed HP display being clamped to 5 if it changed during a turn.
- **Spy Cards:** Fixed an encoding error in the match recording for game modes with variants.

## v0.2.49

- **Spy Cards:** Added code to help detect and diagnose desyncs at the networking layer.

## v0.2.48

- **Spy Cards:** Fixed a crash when an invisible card summons a visible card.
- **Spy Cards:** Fixed singleplayer NPC opponent closing the connection too quickly.
- **Spoiler Guard:** Fixed a crash related to a `keydown` event with no `key`.

## v0.2.47

- **Card Editor:** Added "variant" game mode field.

## v0.2.46

- **Mite Knight:** Fixed game end sound effects playing 1 second too soon.

## v0.2.45

- **Mite Knight:** Fixed end-of-game hearts display missing the first heart.
- **Termacade:** Fixed a crash related to custom controller bindings.

## v0.2.44

- **Mite Knight:** Fixed a desynchronization introduced in v0.2.41 related to being hit by an enemy shortly before moving. Recordings of versions 0.2.41, 0.2.42, and 0.2.43 will emulate this bug to maintain compatibility.

## v0.2.43

- **Card Editor:** Fixed a crash when attempting to copy an unselected effect.
- **Card Editor:** Added the ability to make a card summon as invisible. Invisible cards apply their effects with no sounds or delays.

## v0.2.42

- **Card Editor:** Added "if HP at end of round" condition.

## v0.2.41

- **Termacade:** Recordings can now be fast forwarded and rewound. Press left to go back by 10 seconds, and press right to go forward by 5 seconds.

## v0.2.40

- **Termacade:** Added support for binding actions to analog controller inputs.
- **Mite Knight:** Reduced the size of the smoke particle effect to match Bug Fables more closely.
- **Mite Knight:** Reduced duration of fade-out when starting the game.

## v0.2.39

- **Mite Knight:** Corrected the width of the hearts on the end screen.
- **Termacade:** Added support for viewing recordings to the high scores screen. Playback controls will be added later.

## v0.2.38

- **Mite Knight:** Altered particle effects to be more accurate.
    - Slightly decreased particle size.
    - Increased initial particle gravity.
    - Moved spawn point up slightly.
    - Smoke now always starts moving up.
    - Increased size and decreased intensity of grass particles.
    - Darkened smoke slightly.
- **Mite Knight:** Mite Knight now appears in front of other objects.

## v0.2.37

- **Termacade:** High Scores can now be viewed from the Termacade menu. The recording viewer is coming soon.

## v0.2.36

- **Spy Cards:** Fixed HP being limited to 5 instead of Max HP when changed.

## v0.2.35

- **Termacade:** Fixed an edge case where the frame counter could increase without actually processing a frame.
- **Card Editor:** Added a "Copy" button to effects.

## v0.2.34

- **Spy Cards:** Fixed a race condition that would cause the wrong number to be displayed when updating the stats of multiple cards via a single effect (eg. Empower).

## v0.2.33

- **Termacade:** High scores can now be recorded. They are not yet displayed.

## v0.2.32

- **Settings:** Added the ability to modify what buttons the controls are mapped onto. This currently only affects the Termacade.
- **Spy Cards:** Fixed a crash at the start of the match in Vanilla.

## v0.2.31

- **Card Editor:** Added the ability to summon a card at the start of the match, which can be used to implement more complex game rules.
- **Spy Cards:** Added the button handling logic from the Termacade. Does not yet accept gamepad buttons. Let me know if anything unexpected happens.
- **Game Log:** Added messages about random cards being selected for summon effects.

## v0.2.30

- **Card Editor:** Fixed a crash when setting a card with no effects to 0 or 10 TP.
- **Card Editor:** Fixed a crash when setting a card image to a very large file.
- **Play Online:** Fixed a crash when entering a match code before the connection to the matchmaking server is ready.

## v0.2.29

- Improved rendering performance in Mite Knight.
- Added particle effects to Mite Knight.
- Fixed dead enemies visually disappearing before having a chance to be knocked back in Mite Knight.
- Added a version number to the Termacade.
- Added instructions on how to use the Termacade.
- If you press a button on a supported game controller, key prompts will now show controller buttons in the Termacade.
- Added a placeholder "controllers" section to the settings page. In the future, it will be possible to change button mapping here.

## v0.2.28

- Fixed conditional effects not being waited for in some cases.

## v0.2.27

- The Numb effect now ignores whether the card holding it is Numbed.
- Added a settings option to the Termacade menu.
- Constructed some snazzy new neon signs in the Termacade.

## v0.2.26

- Fixed negative stat effects inside limit conditions not displaying their sign.

## v0.2.25

- Fixed a race condition between NPC movement ticks in Mite Knight.
- Added the random seed to the end screen in Mite Knight.
- Reduced delay before inputs are allowed during floor changes in Mite Knight.
- Added `?drawButtons` to display buttons being pressed in the arcade.

## v0.2.24

- Fixed fog distance in Mite Knight.
- Fixed initial camera position in Mite Knight.
- Fixed clouds being invisible 25% of the time in Flower Jouney.
- Added the ability to override game rules in Mite Knight and Flower Journey.  
  For example, `?overrideFlowerJourneyGameRule_countdownLength=5` makes Flower Journey count down from 5 rather than 3.  
  More game rules will be documented later.

## v0.2.23

- Fixed Mite Knight camera.
- Fixed Mite Knight sprite angles.

## v0.2.22

- Added compass to Mite Knight.
- Fixed fog shading on door in Mite Knight.
- Fireball now properly spins in Mite Knight.
- Fixed Termacade crash in Google Chrome.

## v0.2.21

- Fixed corridor generation in Mite Knight.
- Fixed pause screen layout in Mite Knight.

## v0.2.20

- Fixed terrain texture distortion in Mite Knight.
- Reduced GPU texture memory usage.

## v0.2.19

- Added controller support to Termacade.
- Reduced lag in Mite Knight.

## v0.2.18

- Fixed random summons summoning banned cards in some cases.

## v0.2.17

- Fixed the numb animation flipping the card that performed the numb rather than the card that was numbed.

## v0.2.16

- Changed the wording of the "If Card" condition to ensure it's always clear whether it requires a specific card or a tribe.
- Fixed a crash in the deck editor when using the combining operator (`;`) with a custom game mode.
- Reduced the amount of data downloaded when updating to a new version.
- Fixed face-down cards sometimes appearing face-up in reverse.

## v0.2.15

- Added a [settings page](/settings.html).

## v0.2.14

- Modes (eg. `midge.0`) can now be entered into the custom card editor and other places that allow custom card codes to be joined with `;`.

## v0.2.13

- Added a message when an update is available and being downloaded.
- Fixed a crash when playing a card above ID 255.

## v0.2.12

- Fixed clipping issues on Google Chrome with 3D backgrounds.
- Added a workaround for issues caused by having more than 30 cards in the player's hand.

## v0.2.11

- Fixed a crash when selecting the last mini-boss in the card list as the first mini-boss in the deck.
- Fixed a crash when creating a deck in the deck editor with a custom card set that has no game rules.
- Card TP can now be set to any value in \[-250, 250\] rather than the old limits of \[0, 10\]
- Added support for backwards-compatibly changing vanilla cards in a future release.
- Added a page at [service-worker-status](/service-worker-status) to show the exact version of Spy Cards Online's ServiceWorker that is running.

## v0.2.10

- Added "Game Rules" game mode field.
- The following properties of Spy Cards can now be modified using the custom card editor:
    - Max HP
    - Hand
        - Minimum Size
        - Maximum Size
        - Cards Drawn Per Turn
    - Teamwork Points (TP)
        - Minimum
        - Maximum
        - Gained Per Turn
    - Cards Per Deck
        - Total
        - Boss
        - Mini-Boss

## v0.2.9

- Added a notification when an update is available.
- Spy Cards Online should now load slightly faster when not updating.
- Your browser may now allow you to install Spy Cards Online as an app.

## v0.2.8

- Added support for keyboard control of the game during a match.
- Added search support to the character selector.
- Major work towards improving interface accessibility for those using assistive technologies.

## v0.2.7

- Fixed animation for setup cards being replaced via the summon effect.
- Fixed match winner calculation when both players have negative HP.

## v0.2.6

- Fixed game mode description appearing over the match.
- Renamed Empower variants:
    - +ATK remains Empower
    - -ATK is now Enfeeble
    - +DEF is now Fortify
    - -DEF is now Break
- Fixed empower opponent once being displayed as Unity.
- Numbing cards now recalculates stats, so numbing infinity ATK or DEF now works as if the card was not played in the first place.
- Numb can now target specific cards and filters other than any-tribe Attacker.

## v0.2.5

- Cards that require infinite TP (i.e. cards that cannot be added to a deck) now show a "no" symbol rather than an infinity TP symbol.
- Added the ability to have more than 2 tribes assigned to a card.
- Added some internal infrastructure for custom game mode metadata.
    - **Update:** This is now modifiable in the card editor.

## v0.2.4

- Fixed numb subtracting base attacker stats twice.

## v0.2.3

- Allowed Multiply Healing to specifically affect positive or negative heal effects.
- Setup cards that show the original card description will share a ghost card if the same card instance creates multiple setup effects.
- Added "Clone Card" button to card editor.

## v0.2.2

- Fixed a crash related to the end-of-match deck display.
- Fixed the background color of form inputs on light mode.
- Fixed negating infinity not updating the card editor preview.
- Fixed modifying the tribe of a card filter not updating the card editor preview.
- Adjusted spacing between available cards on the home page.
- Added "Number of Turns" to setup effects in the card editor.

## v0.2.1

- Fixed replace-with-random saying "cards cards".
- Fixed attacker cards with negative stats not showing the minus sign.
- Fixed giant coins in the card editor on some devices.
- Fixed logs not containing card names.
- Fixed deck encoding causing match validation errors.
- Fixed If Tribe checking the tribe of the acting card rather than the tribe of other cards.
- Improved compatibility with iOS devices.

## v0.2.0

- The custom card system has been completely rewritten.
- All existing card codes will be converted to the new system automatically.
- Most effects can now be applied directly to the opponent.
- Custom card sets can be combined using `;`. The card loader will automatically change IDs so they don't overlap.

## v0.1.43

- Added a [simulator](../simulator.html) page
- Please forgive me for I have added `?npc=mender-spam`

## v0.1.42

- The `?npc` parameter has been expanded to support the following AI presets:
    - Any tournament player (eg. `?npc=bu-gi`, `?npc=kage`, `?npc=ritchee`)
    - Any card master (not listed here; only the fourth currently works as the rest have illegal decks)
    - Carmina's first two decks (`?npc=tutorial` and `?npc=carmina2` (currently does not work as she has an illegal deck))
    - Saved Decks mode (`?npc=saved-decks`)
    - Generic mode (`?npc`, or eg. `?npc=invalid-name`)

## v0.1.41

- Added VS HP condition.

## v0.1.40

- Added the ability to reorder cards and effects in the custom card editor.

## v0.1.39

- Added `?npc=saved-decks`, which uses a random saved deck if there is a saved deck available for picking rather than generate a deck from random cards.

## v0.1.38

- Added [Spoiler Guard](../spoiler-guard.html), which allows players to avoid seeing cards they would not see in Bug Fables during vanilla matches.

## v0.1.37

- Fixed multi-turn setup effects being saved as "override card text" for all turns after the first.

## v0.1.36

- Fixed desync caused by cards not being properly removed from opponent's hand when played.

## v0.1.35

- Fixed Unity and ATK (once) only applying once per card even if there were multiple effects of that type.
- Added an option to Setup to use the original card description.
- Audience size will slowly grow every rematch.
- Modified the game protocol to afford more secrecy to readied cards and remove the theoretical ability to predict coin flips.

## v0.1.34

- Setup cards now show the effect that was setup rather than the full description of the card.

## v0.1.33

- Fixed a crash when trying to summon a random card with a rank that does not exist in a specific tribe.

## v0.1.32

- Empower effects that apply to one card are now written as "Empower Card" rather than "Empower" to reduce confusion.

## v0.1.31

- Custom clients have moved in preparation for future infrastructure changes. Old addresses will redirect.

## v0.1.30

- Added an "invert" option to the coin flip condition, which visually swaps the heads and tails sides of the coin.

## v0.1.29

- Fixed a crash when a card with flavor text or a TP effect was summoned.

## v0.1.28

- Summon Random can now have a card type specified.

## v0.1.27

- Added Flavor Text effect type.

## v0.1.26

- Added Quick Join button.

## v0.1.25

- Fixed match verification failing on rematches.
- Opponent Left now overrides offered rematches in both directions on the end-of-match screen.
- Fixed HP indicators being missing on rematches.

## v0.1.24

- Fixed match recording corruption.
- Fixed a crash when a card had multi-turn setup.

## v0.1.23

- Fixed Multiply Healing not allowing 0 in the card editor.
- Added an Auto-Play checkbox to the match viewer.

## v0.1.22

- Card definitions are now local to each match rather than being global per browser tab. This only affects the random match viewer for now.
- Custom card editor now removes already-replaced cards from the base card selector.

## v0.1.21

- Added new effect types for custom cards:
    - TP (only allowed unconditionally or inside Setup)
    - Stat (after modifiers)
    - VS Stat
    - VS Stat (after modifiers)
    - If HP
    - Per Tribe
    - Per Rank
    - Multiply Healing
    - Summon as Opponent
- Improved consistency of tribe names in effect descriptions.
- Effects with negative amounts are now displayed with easier-to-understand phrasing.
- Setup cards now remain visible on the field between turns.

## v0.1.20

- Added a blue highlight on the current effect being processed.

## v0.1.19

- Match viewer:
    - Added HP indicators.
    - Fixed a race condition when changing the currently viewed round number very quickly.
    - Clicking the "next round" button now plays the current round's animations.
    - Added `?autoplay` mode.

## v0.1.18

- Split the Audio toggle into separate Music and Sound toggles. When both are disabled, the full-sized "Enable Audio" button will be used.

## v0.1.17

- Added support for `?no3d`, which disables all 3D backgrounds.

## v0.1.16

- Added audio.

## v0.1.15

- Fixed a crash on match end if using custom cards that replace vanilla cards.
- A preliminary version of the match viewer is now available.

## v0.1.14

- Fixed a crash on match end.

## v0.1.13

- Re-added match recording code at end of match.

## v0.1.12

- Fixed match not ending if one player dropped below 1 HP on a turn that resulted in a tie.

## v0.1.11

- Player characters:
    - Added Arie.
    - Added Bu-gi.
    - Added Jayde.
    - Added Johnny.
    - Added Kage.
    - Added Kenny.
    - Added Malbee.
    - Added Nero.
    - Added Pibu.
    - Added Richee.
    - Added Serene.
    - Added Shay.
    - Added Vanessa.

## v0.1.10

- Fixed If Card only being able to see 1 copy of the card.

## v0.1.9

- Fixed not being able to select custom decks.
- Midge now has 1 ATK rather than a very complicated effect equivalent to midges having 1 ATK.

## v0.1.8

- Fixed the Limit condition checking the limit for the current player rather than the player that played the card.

## v0.1.7

- Added coin flips and condition results to the game log.

## v0.1.6

- Fixed On Win, On Loss, On Tie, and Unless Tie activating on loss, on win, unless tie, and on tie, respectively.
- Added card pack client support to the deck editor.

## v0.1.5

- Modified the character selector to work better on Mobile Safari.
- Coin flip animation for heads-or-tails now only uses the ATK/DEF icon if the heads effect is an effect-atk and the tails effect is an effect-def.
- Fixed custom card lists not being properly removed in clients other than custom.html.

## v0.1.4

- Mobile devices will now display a reduced-detail version of the character selector.
- Added the ability to drag the character selector.
- Improved layout of pre-match menus.

## v0.1.3

- Modified character select room geometry to improve performance.
- Altered audience distribution to improve player visibility.
- Match codes for different client types are now visibly different.

## v0.1.2

- Fixed custom client not being able to play matches since the 3D update.

## v0.1.1

- Inverting the On Win condition now results in On Opponent Win rather than On Loss Or Tie.

## v0.1.0

- Spy Cards is now 3D.
- You can play as Tanjerin.
- No, it's July, not April.

## v0.0.45

- Added a Game Log button to allow players to review what happened so far in the match.

## v0.0.44

- ATK and DEF can now become negative. Negative ATK and DEF are still shown as 0 and processed as 0 when the turn ends.

## v0.0.43

- Added "On Win" and "On Tie" conditions for custom cards.
- In cases where both players have zero or negative HP at the end of a single turn, the winner is whichever player won the turn. This is only possible via negative healing on custom cards.
- Fixed a card editor crash when a card had a custom second tribe but not a custom first tribe.

## v0.0.42

- Added link to changelog.
- Added link to card editor.

## v0.0.41

- Fixed a crash related to conditional effects.

## v0.0.40

- Fixed a crash that would occur when a card had "replace with a random card" as well as another effect.

## v0.0.39

- Added a button to submit an error report to the crash screen.
- Nested setup is now replaced by "Setup (X turns)" in the UI.

## v0.0.38

- Possible fix for old Attacker card links not working.

## v0.0.37

- Fixed Broodmother's Empower effect in the custom client.
- Added an indicator for when your connection to your opponent is lost.

## v0.0.36

- Fixed several condition types having their "invert" flag inverted on save. (Ironic!)

## v0.0.35

- "Summon Random Mini-Boss" has been expanded to all four card types.
- Fixed deck editor not allowing multiple custom cards at once.
- Added condition types:
    - VS Card
    - If Card Type
    - VS Card Type
- Most conditions (with the exception of conditions that occur multiple times and Setup) can be inverted.
- Added count fields to VS Tribe and If Card.

## v0.0.34

- Fixed the tribe list becoming longer every time it was clicked.
- Custom tribes are now available in the card editor.
- Custom Attacker cards can now have DEF, and their ATK can be modified.

## v0.0.33

- Added support for whitelisted community-created portrait icons in the custom card editor.

## v0.0.32

- Fixed Numb not counting base ATK.

## v0.0.31

- Removed "Only Once" condition for custom cards.
- Added "Limit" condition for custom cards, which is equivalent to "Only Once" if the count is set to 1.
- Fixed attacker card descriptions being squished.

## v0.0.30

- Added support for custom cards in the deck editor.
- Added a deck editor link to the custom client landing page.
- Decks containing placeholder cards can now be imported and exported.
- The card editor now allows negative values for the Amount field of effects.

## v0.0.29

- Card effects added by the summoning of a card are now processed after all summons of that type, with the correct card highlighted.
- The Per-Card condition now ignores any cards summoned during its processing.

## v0.0.28

- Added "Only Once" condition type for custom cards. This will apply its result the first time it is encountered per turn (per effect instance, per player).
- Custom card descriptions that are too long are now squished vertically.
- Card images are now uploaded to a server, greatly reducing the size of custom card codes for custom portraits.
- Custom cards can now cost 0 or infinite TP. Cards that cost infinite TP cannot be chosen when making a deck.

## v0.0.27

- Fixed "If Tribe" not encoding the count in custom cards.
- Fixed "Unity" being missing from the effect list in the card editor.
- Added an editor button to the custom cards landing page if custom cards are present.
- Fixed Setup counting as a card being played for various effects.

## v0.0.26

- Matches with custom cards are now available. Only the host needs the URL of the custom cards. Player 2 will receive the card list and have a chance to review it before accepting the match.

## v0.0.25

- A preview of the card editor is now available. It does not yet have all features implemented, and custom cards cannot yet be played, but the features that are currently in place will continue working, and card codes created now will continue working when the custom card feature is fully released.

## v0.0.24

- Fixed cards being permanently flipped on several screens related to deck selection on iOS browsers.

## v0.0.23

- Fixed card names being missing in the deck editor.

## v0.0.22

- Added fancier tribe labels to cards.
- Reduced the height of the card nameplate to better match the game.
- Added modification data to the version number to avoid playing against someone on a different client.

## v0.0.21

- Fixed extreme blurriness on Firefox when hovering over cards in some places where cards are displayed very small.
- Removed the tribe names from cards in places that display entire decks as they were unreadable anyway at such a small font size.
- Fixed a situation where the "opponent has disconnected" error handler would activate due to the matchmaking server disconnecting when it was no longer needed.

## v0.0.20

- Improved coin layout when a card has more than 3 coins.
- Fixed card summoning animation.
- Reduced the size of cards on the field slightly.

## v0.0.19

- Fix infinitely spinning coins in Google Chrome.

## v0.0.18

- Fixed summoned cards not applying effects that had already been processed.
- Fixed turn counter disappearing when playing a rematch.

## v0.0.17

- Swapped the order of the coin effects on Wasp Bomber for clarity as numb coin effects happen later in the turn.
- Added buttons to offer a rematch and to exit on the end-of-match screen.
- Added a minimum amount of time (1 second) per turn before both players being ready can end the turn.
- Fixed the icon on a certain card that has Unity.

## v0.0.16

- Fixed a crash when Carmina.

## v0.0.15

- Added an animation for coin flips.
- Added a delay after a numb effect is applied to make sure the card has time to flip.
- The card that is currently having its effect acted upon is now raised above other cards so it is always visible.

## v0.0.14

- Changed Mender from Bots (4) to Bots (5) to match ingame behavior.
- Added an experimental relayed connection method that sends all data through the matchmaking server. (opt-in)

## v0.0.13

- Added a list of tribes the card is part of to each card. This design is temporary until a better visual design is created.
- Both players now have the ATK indicator above the DEF indicator.
- Added some logging to help diagnose situations where the fatal error handler would partially activate.
- You can now click on a card while creating a deck to remove that card.
- Added a big link to the deck editor on the front page.
- Deck editor now has an "Edit" button rather than a "Save" button for decks you already have saved. The deck that was selected to be edited is not removed automatically, but can be deleted later.
- Deck imports now process in reverse order so the top code will be the top deck after importing multiple decks.

## v0.0.12

- Decks larger than 15 cards can be added to the saved deck list. Only 15-card decks can be played, for now.

## v0.0.11

- Reordered effects to make sure Scarlet's heal happens before numb.

## v0.0.10

- Fixed a possible race condition if cards or the ready button were clicked before the players' hands were fully set up.

## v0.0.9

- Fixed Unity. Again.
- Midges now have the special "1 attack per turn" effect rather than the "1 attack per card per turn" effect most cards that say "ATK: 1" have.
- Your and your opponent's decks are displayed at the end of the match.
- Added version number to the deck editor.

## v0.0.8

- Fixed saved deck corruption bug.

## v0.0.7

- Fixed "init was already called" or a similarly bizarre error message if the connection to your opponent dropped but was reestablished.
- Added webrtc-adapter.js, which should smooth out some browser implementation differences.
- Converted Spy Cards to TypeScript.

## v0.0.6

- Fixed added effects only being applied once per match.

## v0.0.5

- Fixed Setup.
- Reduced the size of cards in the deck builder.

## v0.0.4

- Replaced Venus' Guardian's Unity with Per-Card to fix the difference caused by Unity fixes.
- Fixed Unity activating for cards that already contributed to Unity.
- All tribes have been updated to match their ingame names.
- Card IDs have been updated to match their ingame values.
- Corrected spelling of "Belostoss".

## v0.0.3

- Reduced the size of the random state per turn from 2x32 bytes to 2x4 bytes. Reduced the initial random state from 2x32 bytes to 2x16 bytes. (This will greatly reduce the size of match recordings.)
- Added SRI (subresource integrity).

## v0.0.2

- Fixed stat-based conditions ignoring boosts from empower/VS.

## v0.0.1

- Added version numbers.
- Fixed summon abilities summoning a card with the summoner's description rather than the summoned card's description.
- Reverted Unity changes. Unity now matches what exists within the game.
