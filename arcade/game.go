//go:generate stringer -type Game

// Package arcade implements data structures used by the Termacade.
package arcade

// Game is a Termacade game.
type Game uint8

// Constants for Game.
const (
	MiteKnight    Game = 1
	FlowerJourney Game = 2
)
