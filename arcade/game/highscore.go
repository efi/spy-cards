// +build !headless

package game

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

var highScoreKeyboard = [][]rune{
	{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'},
	{'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X'},
	{'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'},
}

func (s *State) highScore(ctx context.Context) error {
	s.Camera.SetDefaults()
	s.CRT.SetDefaults()

	s.AISpamButton(arcade.BtnCancel)

	x, y := 0, 0

	name := []rune(internal.LoadSettings().LastTermacadeName)

	scoreText := fmt.Sprintf("%.0f", s.Score)

	tb := sprites.NewTextBatch(&s.Camera)

	stop := false
	for !stop {
		s.CRT.Draw(func() {
			gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
			gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

			paddedName := string(name)
			paddedName += strings.Repeat("?", 3-len(name))

			tb.Reset(&s.Camera)

			sprites.DrawTextCentered(tb, sprites.FontD3Streetism, paddedName+"'S HIGH SCORE", 0, 75, 0, 2, 2, sprites.White, true)
			sprites.DrawTextCentered(tb, sprites.FontD3Streetism, scoreText, 0, 72.75, 0, 3, 3, sprites.White, true)

			for i := 0; i < len(highScoreKeyboard); i++ {
				for j := 0; j < len(highScoreKeyboard[i]); j++ {
					offsetX := float32(0.25)
					if j >= len(highScoreKeyboard[i])/2 {
						offsetX = 0.75
					}

					color := sprites.Gray
					if j == x && i == y && len(name) != 3 {
						color = sprites.Rainbow
					}

					sprites.DrawTextCentered(tb, sprites.FontD3Streetism, string(highScoreKeyboard[i][j]), float32(j)-float32(len(highScoreKeyboard[i]))/2+offsetX, 71.5-float32(i), 0, 1, 1, color, true)
				}
			}

			if len(name) == 3 {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnConfirm)+" Submit High Score", -4, 68, 0, 1, 1, sprites.Rainbow, true)
			} else {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnConfirm)+" Add Letter", -4, 68, 0, 1, 1, sprites.White, true)
			}

			if len(name) == 0 {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnCancel)+" Do Not Submit", 4, 68, 0, 1, 1, sprites.Red, true)
			} else {
				sprites.DrawTextCentered(tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnCancel)+" Remove Letter", 4, 68, 0, 1, 1, sprites.White, true)
			}

			tb.Render()
		})

		gfx.NextFrame()

		s.Input.Tick()

		switch {
		case s.Input.Consume(arcade.BtnCancel):
			audio.PageFlip.PlaySound(0, 0, 0)

			if len(name) != 0 {
				name = name[:len(name)-1]
			} else {
				stop = true
			}
		case s.Input.Consume(arcade.BtnConfirm):
			audio.Confirm.PlaySound(0, 0, 0)

			if len(name) == 3 {
				stop = true
			} else {
				name = append(name, highScoreKeyboard[y][x])
			}
		case s.Input.ConsumeAllowRepeat(arcade.BtnUp, 60):
			audio.Confirm1.PlaySound(0, 0, 0)

			y--
			if y < 0 {
				y = len(highScoreKeyboard) - 1
			}
		case s.Input.ConsumeAllowRepeat(arcade.BtnDown, 60):
			audio.Confirm1.PlaySound(0, 0, 0)

			y++
			if y >= len(highScoreKeyboard) {
				y = 0
			}
		case s.Input.ConsumeAllowRepeat(arcade.BtnLeft, 60):
			audio.Confirm1.PlaySound(0, 0, 0)

			x--
			if x < 0 {
				x = len(highScoreKeyboard[0]) - 1
			}
		case s.Input.ConsumeAllowRepeat(arcade.BtnRight, 60):
			audio.Confirm1.PlaySound(0, 0, 0)

			x++
			if x >= len(highScoreKeyboard[0]) {
				x = 0
			}
		}
	}

	settings := internal.LoadSettings()
	settings.LastTermacadeName = string(name)
	internal.SaveSettings(settings)

	s.AIClearButtons()

	if len(name) != 0 {
		s.recording.PlayerName = string(name)

		b, err := s.recording.MarshalBinary()
		if err != nil {
			return xerrors.Errorf("game: encoding high score recording: %w", err)
		}

		req, err := http.NewRequestWithContext(ctx, http.MethodPut, internal.GetConfig(ctx).ArcadeAPIBaseURL+"upload", bytes.NewReader(b))
		if err != nil {
			return xerrors.Errorf("game: submitting high score: %w", err)
		}

		req = internal.ModifyRequest(req)

		s.CRT.Draw(func() {
			gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
			gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

			tb.Reset(&s.Camera)

			sprites.DrawTextCentered(tb, sprites.FontD3Streetism, s.recording.PlayerName+"'S HIGH SCORE", 0, 72.5, 0, 2, 2, sprites.White, true)
			sprites.DrawTextCentered(tb, sprites.FontD3Streetism, scoreText, 0, 70.25, 0, 3, 3, sprites.White, true)
			sprites.DrawTextCentered(tb, sprites.FontD3Streetism, "(submitting...)", 0, 68, 0, 1, 1, sprites.White, true)

			tb.Render()
		})

		minDelay := time.After(500 * time.Millisecond)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return xerrors.Errorf("game: submitting high score: %w", err)
		}

		if err = resp.Body.Close(); err != nil {
			return xerrors.Errorf("game: submitting high score: %w", err)
		}

		<-minDelay
	}

	return ctx.Err()
}
