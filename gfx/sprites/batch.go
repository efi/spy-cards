package sprites

import (
	"encoding/binary"
	"image/color"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

var spriteDataShared = [4][4]uint8{
	{0, 1, 0, 0},
	{0, 0, 0, 1},
	{1, 0, 1, 1},
	{1, 1, 1, 0},
}

var spriteElem = [6]uint16{
	3, 2, 1,
	0, 3, 1,
}

// Batch is a sprite batch.
type Batch struct {
	camMatrix [2]gfx.Matrix
	texture   [8]*gfx.AssetTexture
	data      []byte
	elem      []byte
	count     int
	dataBuf   gl.Buffer
	elemBuf   gl.Buffer
	index     uint16
	haveBuf   bool
	dirtyBuf  bool
}

// NewBatch starts a sprite batch.
func NewBatch(cam *gfx.Camera) *Batch {
	return &Batch{
		camMatrix: [2]gfx.Matrix{
			cam.Perspective,
			cam.Combined(),
		},
	}
}

// RenderFlag is a sprite rendering modifier.
type RenderFlag uint16

// Constants for RenderFlag.
const (
	FlagBillboard RenderFlag = 1 << 0
	FlagRainbow   RenderFlag = 1 << 1
	FlagMKFog     RenderFlag = 1 << 2
	FlagNoDiscard RenderFlag = 1 << 3
	FlagBorder    RenderFlag = 1 << 4
	FlagIsMask    RenderFlag = 1 << 5
	FlagHalfGlow  RenderFlag = 1 << 6
)

type batchData struct {
	X       [2]float32
	Y       [2]float32
	S       [2]float32
	T       [2]float32
	Pos     [3]float32
	Rot     [3]float32
	Blur    float32
	Flags   RenderFlag
	Texture uint16
	Color   color.RGBA
}

// Append adds a sprite to the batch.
func (sb *Batch) Append(s *Sprite, x, y, z, sx, sy float32, tint color.RGBA, flags RenderFlag, ry, rz, blur float32) {
	tex := ^uint16(0)

	for i := range sb.texture {
		if sb.texture[i] == nil {
			sb.texture[i] = s.AssetTexture
		}

		if sb.texture[i] == s.AssetTexture {
			tex = uint16(i)

			break
		}
	}

	if tex > uint16(len(sb.texture)) {
		panic(xerrors.New("sprite: too many sprite sheets in batch"))
	}

	blur *= s.unitPerPx / float32(s.spriteSheet.width)

	if tint == Rainbow {
		flags |= FlagRainbow
		tint = White
	}

	var data batchData

	data.X[0], data.X[1] = s.X0*sx, s.X1*sx
	data.Y[0], data.Y[1] = s.Y0*sy, s.Y1*sy
	data.S[0], data.S[1] = s.S0, s.S1
	data.T[0], data.T[1] = s.T0, s.T1
	data.Pos[0], data.Pos[1], data.Pos[2] = x, y, z
	data.Rot[0], data.Rot[1], data.Rot[2] = 0, ry, rz
	data.Blur = blur
	data.Flags = flags
	data.Texture = tex
	data.Color = tint

	for _, shared := range spriteDataShared {
		sb.appendData(shared, &data)
	}

	var elems [6 * 2]uint8

	for i, n := range spriteElem {
		binary.LittleEndian.PutUint16(elems[i*2:], sb.index+n)
	}

	sb.elem = append(sb.elem, elems[:]...)

	sb.index += 4
	sb.count += 6

	sb.dirtyBuf = true
}

func (sb *Batch) appendData(shared [4]uint8, data *batchData) {
	sb.data = append(sb.data, shared[:]...)
	/*#nosec*/
	sb.data = append(sb.data, (*[unsafe.Sizeof(*data)]byte)(unsafe.Pointer(data))[:]...)
}

func (sb *Batch) reserve(sprites int) {
	/*#nosec*/
	extraData := int(4+unsafe.Sizeof(batchData{})) * 4 * sprites
	extraElem := 2 * 6 * sprites

	if len(sb.data)+extraData > cap(sb.data) {
		sb.data = append(sb.data, make([]byte, extraData)...)[:len(sb.data)]
	}

	if len(sb.elem)+extraElem > cap(sb.elem) {
		sb.elem = append(sb.elem, make([]byte, extraElem)...)[:len(sb.elem)]
	}
}

// Reset clears the state of the Batch, but keeps its allocated memory,
// allowing it to be reused.
func (sb *Batch) Reset(cam *gfx.Camera) {
	sb.SetCamera(cam)

	for i := range sb.texture {
		sb.texture[i] = nil
	}

	sb.data = sb.data[:0]
	sb.elem = sb.elem[:0]
	sb.count = 0
	sb.index = 0

	sb.dirtyBuf = true
}

// SetCamera changes the camera of the batch without changing its contents.
func (sb *Batch) SetCamera(cam *gfx.Camera) {
	sb.camMatrix[0] = cam.Perspective
	sb.camMatrix[1] = cam.Combined()
}
