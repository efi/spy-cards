"use strict";
class SignalingConnection {
    constructor(fake = false) {
        this.onCloseCode = (code) => { debugger; };
        if (!fake) {
            this.fake = null;
            this.ws = new WebSocket(matchmaking_server);
            this.ws.addEventListener("message", this.onMessage.bind(this));
            this.ws.addEventListener("error", this.onError.bind(this));
            this.ws.addEventListener("close", this.onClose.bind(this));
            this.sessionID = new Promise((resolve, reject) => {
                this.sessionIDResolve = resolve;
                this.ws.addEventListener("close", function (e) {
                    reject(e);
                });
            });
            this.keepAlive = setInterval(() => this.ws.send("k"), 45000);
        }
        else {
            this.fake = fake === true ? new SignalingConnection(this) : fake;
            this.sessionID = new Promise((resolve) => {
                this.sessionIDResolve = resolve;
            });
        }
    }
    close() {
        clearInterval(this.keepAlive);
        this.keepAlive = null;
        this.onCloseCode = function () { };
        if (this.fake) {
            return;
        }
        if (this.ws.readyState === WebSocket.OPEN)
            this.ws.send("q");
        this.ws.close();
    }
    onMessage(e) {
        if (!e.data.length) {
            return;
        }
        switch (e.data.charAt(0)) {
            case "s":
                // session id
                this.sessionIDResolve(e.data.substr(1));
                break;
            case "k":
                // keep-alive
                break;
            case "p":
                // handshake
                this.onHandshake();
                break;
            case "r":
                // relayed message
                this.onRelayedMessage(JSON.parse(e.data.substr(1)));
                break;
            case "q":
                // quit
                if (!this.connectionReady) {
                    this.onCloseCode(3002);
                }
                this.close();
                break;
            default:
                console.log("SIGNALING SERVER MESSAGE:", e);
                debugger;
                break;
        }
    }
    onError(e) {
        console.log("SIGNALING SERVER ERROR:", e);
        debugger;
    }
    onClose(e) {
        clearTimeout(this.keepAlive);
        this.keepAlive = null;
        if (this.onCloseCode) {
            this.onCloseCode(e.code);
            return;
        }
        console.log("SIGNALING SERVER CLOSE:", e);
        debugger;
    }
    initPlayer(playerNumber) {
        if (this.fake) {
            switch (playerNumber) {
                case "q":
                    throw new Error("quick join not implemented in mock signaling connection");
                case 1:
                    this.onMessage(new MessageEvent("message", { data: "sFAKE FAKE FAKE FAKE FAKE FAKE" }));
                    break;
                case 2:
                    break;
            }
            return;
        }
        if (this.ws.readyState === WebSocket.CONNECTING) {
            this.ws.addEventListener("open", () => {
                this.ws.send("p" + playerNumber + spyCardsVersionSuffix);
            });
            return;
        }
        this.ws.send("p" + playerNumber + spyCardsVersionSuffix);
    }
    setSessionID(sessionID) {
        this.sessionIDResolve(sessionID);
        if (this.fake) {
            this.onMessage(new MessageEvent("message", { data: "p" }));
            this.fake.onMessage(new MessageEvent("message", { data: "p" }));
            return;
        }
        if (this.ws.readyState === WebSocket.CONNECTING) {
            this.ws.addEventListener("open", () => {
                this.ws.send("s" + sessionID);
            });
            return;
        }
        this.ws.send("s" + sessionID);
    }
    sendRelayedMessage(message) {
        if (this.fake) {
            this.fake.onMessage(new MessageEvent("message", { data: "r" + JSON.stringify(message) }));
            return;
        }
        if (this.ws.readyState === WebSocket.OPEN) {
            this.ws.send("r" + JSON.stringify(message));
        }
    }
}
const defaultICEServers = function () {
    if (window.RTCPeerConnection && RTCPeerConnection.getDefaultIceServers) {
        const servers = RTCPeerConnection.getDefaultIceServers();
        if (servers.length) {
            return servers;
        }
    }
    return [
        { urls: ["stun:stun.stunprotocol.org"] },
        { urls: ["stun:stun.sipnet.net"] }
    ];
}();
class CardGameConnection {
    constructor(config) {
        this.logID = "(conn-" + (CardGameConnection.nextLogID++) + ")";
        this.conn = new RTCPeerConnection(config || {
            iceServers: defaultICEServers,
            iceCandidatePoolSize: 4
        });
        this.conn.addEventListener("negotiationneeded", (e) => console.log("NEGOTIATION NEEDED", this.logID));
        this.conn.addEventListener("connectionstatechange", (e) => console.log("CONNECTION STATE CHANGE:", this.conn.connectionState, this.logID));
        this.conn.addEventListener("iceconnectionstatechange", (e) => console.log("ICE CONNECTION STATE CHANGE:", this.conn.iceConnectionState, this.logID));
        this.conn.addEventListener("icegatheringstatechange", (e) => console.log("ICE GATHERING STATE CHANGE:", this.conn.iceGatheringState, this.logID));
        this.conn.addEventListener("signalingstatechange", (e) => console.log("SIGNALING STATE CHANGE:", this.conn.signalingState, this.logID));
        this.conn.addEventListener("connectionstatechange", (e) => {
            if (this.weWantRelay || this.opponentWantsRelay || !this.onConnectionStateChange) {
                return;
            }
            this.onConnectionStateChange(this.conn.connectionState);
        });
        this.scg = new SecureCardGame();
        this.secure = this.conn.createDataChannel("secure", {
            negotiated: true,
            id: 0
        });
        this.secure.binaryType = "arraybuffer";
        this.queuedSecureMessages = [];
        this.secureMessage = this.createSecureMessagePromise();
        this.secure.addEventListener("message", (e) => {
            if (this.secureMessageResolve) {
                this.secureMessageResolve(e.data);
            }
            else {
                this.queuedSecureMessages.push(e.data);
            }
        });
        this.recvCustomCards = new Promise((resolve) => this.customCardsResolve = resolve);
        this.recvCosmeticData = new Promise((resolve) => this.cosmeticDataResolve = resolve);
        this.recvSpoilerGuard = new Promise((resolve) => this.spoilerGuardResolve = resolve);
        this.control = this.conn.createDataChannel("control", {
            negotiated: true,
            id: 1
        });
        this.control.binaryType = "arraybuffer";
        this.control.addEventListener("message", (e) => {
            const data = new Uint8Array(e.data, 0);
            switch (String.fromCharCode(data[0])) {
                case "v":
                    if (this.onVersion) {
                        this.onVersion(String.fromCharCode.apply(String, SpyCards.toArray(data.subarray(1))));
                    }
                    break;
                case "q":
                    this.onQuit();
                    break;
                case "r":
                    if (this.opponentReady) {
                        console.warn("spurious READY from opponent");
                        break;
                    }
                    this.opponentReady = true;
                    this.onReady(data.slice(1));
                    break;
                case "a":
                    if (this.weWantRematch) {
                        this.setupRematch();
                    }
                    else if (!this.opponentWantsRematch) {
                        this.opponentWantsRematch = true;
                        if (this.onOfferRematch) {
                            this.onOfferRematch();
                        }
                    }
                    break;
                case "c":
                    this.customCardsResolve([].map.call(data.subarray(1), (ch) => String.fromCharCode(ch)).join(""));
                    break;
                case "o":
                    this.cosmeticDataResolve(JSON.parse([].map.call(data.subarray(1), (ch) => String.fromCharCode(ch)).join("")));
                    break;
                case "g":
                    this.spoilerGuardResolve(data.slice(1));
                    break;
                default:
                    console.log("CONTROL CHANNEL MESSAGE:", data);
                    debugger;
            }
        });
    }
    async rand(shared, max) {
        return this.scg.rand(shared, max);
    }
    async shuffle(shared, items) {
        return this.scg.shuffle(shared, items);
    }
    async publicShuffle(cards, cardBacks) {
        return this.scg.publicShuffle(cards, cardBacks);
    }
    async privateShuffle(cards, cardBacks, remote) {
        return this.scg.privateShuffle(cards, cardBacks, remote);
    }
    setOnConnectionStateChange(callback) {
        this.onConnectionStateChange = callback;
        if (callback) {
            callback(this.conn.connectionState);
        }
    }
    createSecureMessagePromise() {
        if (this.queuedSecureMessages.length) {
            return Promise.resolve(this.queuedSecureMessages.shift());
        }
        return new Promise((resolve) => {
            this.secureMessageResolve = (data) => {
                this.secureMessageResolve = null;
                resolve(data);
            };
        });
    }
    useSignalingServer(signalConn, playerNumber) {
        this.sc = signalConn;
        this.conn.addEventListener("icecandidate", (e) => {
            if (this.weWantRelay) {
                return;
            }
            this.sc.sendRelayedMessage({
                type: "ice-candidate",
                candidate: e.candidate
            });
        });
        this.sc.onRelayedMessage = async (msg) => {
            switch (msg.type) {
                case "player1-offer":
                    if (playerNumber === 2) {
                        const answer = await this.initPlayer2(msg.offer);
                        this.sc.sendRelayedMessage({
                            type: "player2-answer",
                            answer: answer
                        });
                    }
                    else {
                        console.log("unexpected player1-offer as player 1");
                        debugger;
                    }
                    break;
                case "player2-answer":
                    if (playerNumber === 1) {
                        await this.acceptPlayer2(msg.answer);
                    }
                    else {
                        console.log("unexpected player2-answer as player 2");
                        debugger;
                    }
                    break;
                case "ice-candidate":
                    // Google Chrome doesn't like null candidates here.
                    if (msg.candidate) {
                        await this.conn.addIceCandidate(msg.candidate);
                    }
                    break;
                case "use-relay":
                    if (!this.opponentWantsRelay && !this.weWantRelay && this.onConnectionStateChange) {
                        this.onConnectionStateChange("want-relay");
                    }
                    this.opponentWantsRelay = true;
                    if (!this.usingRelay && this.weWantRelay) {
                        this.setupRelay();
                    }
                    break;
                case "r":
                    if (!this.usingRelay) {
                        console.log("unexpected relayed game message");
                        debugger;
                    }
                    switch (msg.c) {
                        case "control":
                            this.control.dispatchEvent(new MessageEvent("message", {
                                data: Base64.decode(msg.p).buffer
                            }));
                            break;
                        case "secure":
                            this.secure.dispatchEvent(new MessageEvent("message", {
                                data: Base64.decode(msg.p).buffer
                            }));
                            break;
                        default:
                            console.log("unexpected channel in relayed message:", msg.c, msg);
                            break;
                    }
                    break;
                default:
                    console.log("RELAYED MESSAGE:", msg);
                    debugger;
                    break;
            }
        };
        this.sc.onHandshake = async () => {
            try {
                if (new URLSearchParams(location.search).has("forceRelay") || SpyCards.loadSettings().forceRelay) {
                    this.weWantRelay = true;
                    if (this.onConnectionStateChange) {
                        this.onConnectionStateChange("connecting");
                    }
                    if (!this.usingRelay && this.opponentWantsRelay) {
                        this.setupRelay();
                    }
                    this.sc.sendRelayedMessage({ type: "use-relay" });
                }
            }
            catch (ex) {
                debugger;
            }
            if (playerNumber === 1) {
                const offer = await this.initPlayer1();
                this.sc.sendRelayedMessage({
                    type: "player1-offer",
                    offer: offer
                });
            }
        };
    }
    async closeSignalingConnection(sc) {
        sc.connectionReady = true;
        if (this.usingRelay) {
            return;
        }
        await Promise.all([
            this.ensureChannelOpen(this.secure),
            this.ensureChannelOpen(this.control)
        ]);
        sc.close();
    }
    async close() {
        try {
            await this.sendMessage(this.control, new Uint8Array(["q".charCodeAt(0)]));
        }
        catch (ex) {
            // ignore
        }
        this.conn.close();
    }
    useRelay() {
        this.weWantRelay = true;
        this.sc.sendRelayedMessage({ type: "use-relay" });
        if (!this.usingRelay && this.opponentWantsRelay) {
            this.setupRelay();
        }
    }
    setupRelay() {
        this.usingRelay = true;
        this.control.dispatchEvent(new Event("use-relay"));
        this.secure.dispatchEvent(new Event("use-relay"));
        if (this.onConnectionStateChange) {
            this.onConnectionStateChange("connected");
        }
    }
    offerRematch() {
        if (this.weWantRematch) {
            return;
        }
        this.weWantRematch = true;
        this.sendMessage(this.control, new Uint8Array(["a".charCodeAt(0)]));
        if (this.opponentWantsRematch) {
            this.setupRematch();
        }
    }
    setupRematch() {
        this.weWantRematch = false;
        this.opponentWantsRematch = false;
        this.finalized = false;
        this.opponentForfeit = false;
        this.scg = new SecureCardGame();
        this.onRematch();
    }
    async initPlayer1() {
        if (this.weWantRelay) {
            return null;
        }
        this.logID += " player-1";
        const offer = await this.conn.createOffer();
        await this.conn.setLocalDescription(offer);
        return this.conn.localDescription;
    }
    async initPlayer2(player1Offer) {
        if (!player1Offer || this.weWantRelay) {
            return null;
        }
        this.logID += " player-2";
        if (player1Offer.type !== "offer" || !player1Offer.sdp) {
            throw new Error("invalid offer");
        }
        await this.conn.setRemoteDescription(player1Offer);
        const answer = await this.conn.createAnswer();
        await this.conn.setLocalDescription(answer);
        return this.conn.localDescription;
    }
    async acceptPlayer2(player2Answer) {
        if (!player2Answer) {
            return;
        }
        if (player2Answer.type !== "answer" || !player2Answer.sdp) {
            throw new Error("invalid answer");
        }
        await this.conn.setRemoteDescription(player2Answer);
    }
    async ensureChannelOpen(channel) {
        while (!this.usingRelay && channel.readyState !== "open" && channel.readyState !== "closed") {
            await new Promise((resolve) => {
                function f() {
                    channel.removeEventListener("open", f);
                    channel.removeEventListener("closed", f);
                    channel.removeEventListener("use-relay", f);
                    resolve();
                }
                channel.addEventListener("open", f);
                channel.addEventListener("closed", f);
                channel.addEventListener("use-relay", f);
            });
        }
        if (this.usingRelay) {
            return {
                send: (data) => {
                    this.sc.sendRelayedMessage({
                        type: "r",
                        c: channel.label,
                        p: Base64.encode(new Uint8Array(data.buffer, 0)),
                    });
                }
            };
        }
        if (channel.readyState === "open") {
            return channel;
        }
        throw new Error(channel.label + " channel " + channel.readyState);
    }
    async sendMessage(channel, data) {
        (await this.ensureChannelOpen(channel)).send(data);
    }
    async exchangeDataAsync(input) {
        try {
            await this.sendMessage(this.secure, input);
        }
        catch (ex) {
            if (ex.message === "secure channel closed") {
                // another error message will appear first, as the connection has been
                // terminated. don't activate the crash handler in this situation.
                await new Promise(() => { }); // never resolve
            }
            throw ex;
        }
        const output = new Uint8Array(await this.secureMessage, 0);
        this.secureMessage = this.createSecureMessagePromise();
        return output;
    }
    initSecure(playerNumber) {
        return this.scg.init(playerNumber, this.exchangeDataAsync.bind(this));
    }
    initDeck(deck, cardBacks) {
        return this.scg.setDeck(this.exchangeDataAsync.bind(this), deck, cardBacks);
    }
    beginTurn() {
        this.opponentReady = false;
        this.confirmedTurn = false;
        return this.scg.beginTurn(this.exchangeDataAsync.bind(this));
    }
    sendVersion(version) {
        const versionData = ("v" + version).split("").map((ch) => ch.charCodeAt(0));
        this.sendMessage(this.control, new Uint8Array(versionData));
    }
    sendCustomCards(customCards) {
        const data = ("c" + customCards).split("").map((ch) => ch.charCodeAt(0));
        this.sendMessage(this.control, new Uint8Array(data));
    }
    sendCosmeticData(cosmetic) {
        const data = ("o" + JSON.stringify(cosmetic)).split("").map((ch) => ch.charCodeAt(0));
        this.sendMessage(this.control, new Uint8Array(data));
    }
    sendSpoilerGuardData(guardData) {
        const guard = Base64.decode(guardData);
        const data = new Uint8Array(guard.length + 1);
        data[0] = "g".charCodeAt(0);
        data.set(guard, 1);
        this.sendMessage(this.control, data);
    }
    async sendReady(data) {
        const promise = await this.scg.prepareTurn(data);
        const ready = new Uint8Array(promise.length + 1);
        ready[0] = "r".charCodeAt(0);
        ready.set(promise, 1);
        this.sendMessage(this.control, ready);
    }
    async confirmTurn() {
        this.confirmedTurn = true;
        return await this.scg.confirmTurn(this.exchangeDataAsync.bind(this));
    }
    async finalizeMatch(verifyDeckAsync, verifyTurnAsync) {
        this.finalized = true;
        return await this.scg.finalize(this.exchangeDataAsync.bind(this), verifyDeckAsync, verifyTurnAsync);
    }
}
CardGameConnection.nextLogID = 0;
