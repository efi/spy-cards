declare function requestIdleCallback(cb: () => void): number;

module SpyCards.TheRoom {
	export class Stage {
		private rpc: Native.RoomRPC;
		private id: number;

		async init() {
			UI.html.classList.add("in-room");

			this.rpc = await Native.roomRPC();
			if (this.rpc) {
				this.id = await new Promise((resolve) => this.rpc.newStage(resolve));
				this.rpc.setRoom(this.id);
			}
		}

		async deinit() {
			if (this.rpc) {
				this.rpc.exit();

				this.id = 0;
				this.rpc = null;

				UI.html.classList.remove("in-room", "room-flipped");
			}
		}

		public setFlipped(flipped: boolean) {
			if (this.rpc) {
				this.rpc.setCurrentPlayer(this.id, flipped ? 2 : 1);
			}
		}

		public setPlayer(player: Player) {
			if (!this.rpc) {
				return;
			}

			player.rpc = this.rpc;
			player.id = this.rpc.setPlayer(this.id, player.name, player.num);
		}

		public addAudienceMember(member: Audience) {
			if (!this.rpc) {
				return;
			}

			this.rpc.addAudience(this.id, member.id);
		}

		public createAudienceMember(type: number, x: number, y: number, back: boolean, flip: boolean, color: number, excitement: number): Audience {
			if (!this.rpc) {
				return new Audience(null, 0);
			}

			return new Audience(this.rpc, this.rpc.createAudienceMember(type, x, y, back, flip, color, excitement));
		}

		public async createAudience(seed: Uint8Array, rematches: number): Promise<Audience[]> {
			if (!this.rpc) {
				return [];
			}

			const a = this.rpc.createAudience(seed, rematches).map((a) => new Audience(this.rpc, a));
			for (let member of a) {
				this.addAudienceMember(member);
			}
			return a;
		}
	}

	export enum PlayerFlags {
		Ant,
		Bee,
		Beefly,
		Beetle,
		Butterfly,
		Cricket,
		Dragonfly,
		Firefly,
		Ladybug,
		Mantis,
		Mosquito,
		Moth,
		Tangy,
		Wasp,
		NonAwakened,

		French,
		Explorer,
		Snakemouth,
		Criminal,
		CardMaster,
		MetalIsland,
		Developer
	}

	export interface PlayerDef {
		name: string;
		displayName?: string;
		flags: PlayerFlags[];
	}
	export function playerDisplayName(character: PlayerDef): string {
		return character.displayName || (character.name.substr(0, 1).toUpperCase() + character.name.substr(1));
	}

	export class Player {
		public static readonly characters: PlayerDef[] = [
			{
				name: "amber",
				flags: [
					PlayerFlags.Ant
				]
			},
			{
				name: "aria",
				displayName: "Acolyte Aria",
				flags: [
					PlayerFlags.Mantis
				]
			},
			{
				name: "arie",
				flags: [
					PlayerFlags.Butterfly,
					PlayerFlags.CardMaster
				]
			},
			{
				name: "astotheles",
				flags: [
					PlayerFlags.Cricket,
					PlayerFlags.Criminal
				]
			},
			{
				name: "bomby",
				flags: [
					PlayerFlags.Bee
				]
			},
			{
				name: "bu-gi",
				flags: [
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "carmina",
				flags: [
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "celia",
				flags: [
					PlayerFlags.Ant,
					PlayerFlags.Explorer
				]
			},
			{
				name: "cenn",
				flags: [
					PlayerFlags.Butterfly,
					PlayerFlags.Criminal
				]
			},
			{
				name: "cerise",
				flags: [
					PlayerFlags.Tangy
				]
			},
			{
				name: "chompy",
				flags: [
					PlayerFlags.NonAwakened
				]
			},
			{
				name: "chubee",
				flags: [
					PlayerFlags.Bee
				]
			},
			{
				name: "chuck",
				flags: [
					PlayerFlags.CardMaster
				]
			},
			{
				name: "crisbee",
				flags: [
					PlayerFlags.Bee
				]
			},
			{
				name: "crow",
				flags: [
					PlayerFlags.Bee,
					PlayerFlags.CardMaster
				]
			},
			{
				name: "diana",
				flags: [
					PlayerFlags.Ant
				]
			},
			{
				name: "eremi",
				flags: [
					PlayerFlags.Mantis
				]
			},
			{
				name: "futes",
				flags: [
					PlayerFlags.Mantis,
					PlayerFlags.Developer
				]
			},
			{
				name: "genow",
				flags: [
					PlayerFlags.Bee,
					PlayerFlags.Developer
				]
			},
			{
				name: "honeycomb",
				displayName: "Professor Honeycomb",
				flags: [
					PlayerFlags.Bee
				]
			},
			{
				name: "janet",
				flags: [
					PlayerFlags.Ant,
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "jaune",
				flags: [
					PlayerFlags.Bee,
					PlayerFlags.French
				]
			},
			{
				name: "jayde",
				flags: [
					PlayerFlags.Wasp
				]
			},
			{
				name: "johnny",
				flags: [
					PlayerFlags.Bee,
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "kabbu",
				flags: [
					PlayerFlags.Beetle,
					PlayerFlags.Explorer,
					PlayerFlags.Snakemouth
				]
			},
			{
				name: "kage",
				flags: [
					PlayerFlags.Ladybug,
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "kali",
				flags: [
					PlayerFlags.Moth
				]
			},
			{
				name: "kenny",
				flags: [
					PlayerFlags.Beetle
				]
			},
			{
				name: "kina",
				flags: [
					PlayerFlags.Mantis,
					PlayerFlags.Explorer
				]
			},
			{
				name: "lanya",
				flags: [
					PlayerFlags.Firefly
				]
			},
			{
				name: "leif",
				flags: [
					PlayerFlags.Moth,
					PlayerFlags.Snakemouth,
					PlayerFlags.Explorer
				]
			},
			{
				name: "levi",
				flags: [
					PlayerFlags.Ladybug,
					PlayerFlags.Explorer
				]
			},
			{
				name: "maki",
				flags: [
					PlayerFlags.Mantis,
					PlayerFlags.Explorer
				]
			},
			{
				name: "malbee",
				flags: [
					PlayerFlags.Bee
				]
			},
			{
				name: "mar",
				flags: [
					PlayerFlags.Mantis,
					PlayerFlags.Developer
				]
			},
			{
				name: "mothiva",
				flags: [
					PlayerFlags.Moth,
					PlayerFlags.Explorer
				]
			},
			{
				name: "neolith",
				displayName: "Professor Neolith",
				flags: [
					PlayerFlags.Moth
				]
			},
			{
				name: "nero",
				flags: [
					PlayerFlags.NonAwakened
				]
			},
			{
				name: "pibu",
				flags: [
					PlayerFlags.NonAwakened
				]
			},
			{
				name: "pisci",
				flags: [
					PlayerFlags.Beetle,
					PlayerFlags.Criminal
				]
			},
			{
				name: "ritchee",
				flags: [
					PlayerFlags.Bee,
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "riz",
				flags: [
					PlayerFlags.Dragonfly
				]
			},
			{
				name: "samira",
				flags: [
					PlayerFlags.Beefly
				]
			},
			{
				name: "scarlet",
				displayName: "Monsieur Scarlet",
				flags: [
					PlayerFlags.Ant,
					PlayerFlags.Criminal
				]
			},
			{
				name: "serene",
				flags: [
					PlayerFlags.Moth,
					PlayerFlags.MetalIsland
				]
			},
			{
				name: "shay",
				flags: [
					PlayerFlags.Mosquito,
					PlayerFlags.CardMaster
				]
			},
			{
				name: "tanjerin",
				flags: [
					PlayerFlags.Tangy
				]
			},
			{
				name: "ultimax",
				displayName: "General Ultimax",
				flags: [
					PlayerFlags.Wasp
				]
			},
			{
				name: "vanessa",
				flags: [
					PlayerFlags.Wasp
				]
			},
			{
				name: "vi",
				flags: [
					PlayerFlags.Bee,
					PlayerFlags.Snakemouth,
					PlayerFlags.Explorer,
					PlayerFlags.French
				]
			},
			{
				name: "yin",
				flags: [
					PlayerFlags.Moth,
					PlayerFlags.Explorer
				]
			},
			{
				name: "zaryant",
				flags: [
					PlayerFlags.Ant
				]
			},
			{
				name: "zasp",
				flags: [
					PlayerFlags.Wasp,
					PlayerFlags.Explorer
				]
			}
		];

		public readonly num: 1 | 2;
		public name: string;
		rpc: Native.RoomRPC;
		id: number;

		constructor(num: 1 | 2, name: string) {
			this.num = num;
			this.name = name;
		}

		public setCharacter(character: PlayerDef) {
			if (this.rpc) {
				this.rpc.setCharacter(this.id, character.name);
			}

			this.name = character.name;
		}
		public becomeAngry(ms: number = 2000) {
			if (this.rpc) {
				this.rpc.becomeAngry(this.id, ms / 1000);
			}
		}
	}

	export class Audience {
		private readonly rpc: Native.RoomRPC;
		readonly id: number;

		constructor(rpc: Native.RoomRPC, id: number) {
			this.rpc = rpc;
			this.id = id;
		}

		public startCheer(ms: number = 5000) {
			if (!this.rpc) {
				return;
			}

			this.rpc.startCheer(this.id, ms / 1000);
		}

		public isBack(): boolean {
			if (!this.rpc) {
				return false;
			}

			return this.rpc.isAudienceBack(this.id);
		}
	}
}
