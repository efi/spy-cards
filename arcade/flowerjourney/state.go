//go:generate stringer -type gameState -trimprefix state
//go:generate stringer -type itemType -trimprefix item

// Package flowerjourney implements the Termacade game Flower Journey.
package flowerjourney

import (
	"context"
	"fmt"
	"image/color"
	"math"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

// FlowerJourney holds the game state for Flower Journey.
type FlowerJourney struct {
	rules arcade.FlowerJourneyRules
	state *game.State

	gameState           gameState
	drawPlayer          bool
	itemEnabled         bool
	flipItemX           bool
	enemyEnabled        bool
	passedEnemy         bool
	passedHive          bool
	didLastAITick       bool
	playerDying         bool
	itemType            itemType
	backColor           color.RGBA
	playerSprite        int
	enemySprite         int
	wallID              int
	cloudID             int
	countdown           float64
	countdownWait       uint64
	scoreText           string
	combo               uint64
	invul               uint64
	inputCooldown       uint64
	gpos0               float64
	gpos1               float64
	speed               float64
	progress            uint64
	playerX             float64
	playerY             float64
	playerXVelocity     float64
	playerYVelocity     float64
	playerAngle         float32
	playerAngleVelocity float32
	itemX               float64
	itemY               float64
	itemProgress        uint64
	enemyX              float64
	enemyY              float64
	walls               [3]struct {
		x, y   float64
		gap    float64
		passed bool
	}
	clouds [6]struct {
		x, y   float64
		sprite int
		scale  float32
	}
	lastCombo     uint64
	comboY        float64
	gameOverReady uint64
	gwidth0       float64
	gwidth1       float64
	lastScore     float64
	pausedFor     uint64
	pausedAt      uint64
	exitWait      uint64

	batch *sprites.Batch
	tb    *sprites.TextBatch
}

// New returns a game.CreateFunc for Flower Journey.
func New(rules arcade.FlowerJourneyRules) game.CreateFunc {
	return func(state *game.State) game.Interface {
		return &FlowerJourney{
			rules: rules,
			state: state,

			countdown:     -1,
			backColor:     sprites.White,
			drawPlayer:    true,
			playerX:       -3,
			playerY:       2.25,
			itemY:         999,
			enemyY:        999,
			gameOverReady: ^uint64(0),
			gwidth0:       float64(sprites.FJGround[0].X1-sprites.FJGround[0].X0) * 2,
			gwidth1:       float64(sprites.FJGround[1].X1-sprites.FJGround[1].X0) * 2,
		}
	}
}

// Type implements game.Interface.
func (fj *FlowerJourney) Type() arcade.Game {
	return arcade.FlowerJourney
}

// Rules implements game.Interface.
func (fj *FlowerJourney) Rules() arcade.GameRules {
	return &fj.rules
}

// HighScore implements game.Interface.
func (fj *FlowerJourney) HighScore() float64 {
	return 4500
}

type gameState uint8

const (
	stateStarting gameState = iota
	stateActive
	statePaused
	stateEnding
	stateExitFade
)

type itemType uint8

const (
	itemFlower itemType = iota
	itemHoney
)

// Init implements game.Interface.
func (fj *FlowerJourney) Init(ctx context.Context) error {
	internal.SetTitle("Flower Journey")

	fj.state.CRT.SetDefaults()
	audio.StopMusic()
	fj.StartMusic()

	fj.state.Camera.SetDefaults()
	fj.state.Camera.PushTransform(gfx.Translation(0, 70, 0))
	fj.state.Camera.Position = gfx.Translation(0, 70, 3)
	fj.state.Camera.Rotation = gfx.Identity()

	return ctx.Err()
}

// Generate implements game.Interface.
func (fj *FlowerJourney) Generate(ctx context.Context, progress func(int)) error {
	for i := range fj.walls {
		fj.walls[i].x = float64(i)*fj.rules.WallDist + fj.rules.FirstWallDist
		fj.walls[i].y = game.Lerp(fj.rules.WallMin, fj.rules.WallMax, fj.state.RNG.Float())
		fj.walls[i].gap = 2.5 * fj.rules.WallGapStart
		fj.walls[i].passed = false
	}

	for i := range fj.clouds {
		fj.clouds[i].x = float64(i) * 3.5
		fj.clouds[i].y = game.Lerp(1.5, 4.5, fj.state.RNG.CheapFloat())
		fj.clouds[i].sprite = fj.state.RNG.RangeInt(0, 3)
		fj.clouds[i].scale = 2
	}

	fj.wallID = len(fj.walls)*2 - 2
	fj.cloudID = len(fj.clouds)*2 - 2

	fj.clouds[0].x = -4
	fj.clouds[0].y = 4
	fj.clouds[0].scale = 3

	return ctx.Err()
}

// StartPressed implements game.Interface.
func (fj *FlowerJourney) StartPressed() {
	audio.FBPoint.PlaySound(0, 0, 0)
}

// StartGameplay implements game.Interface.
func (fj *FlowerJourney) StartGameplay(ctx context.Context) error {
	fj.state.FadeTo(1)
	fj.countdownWait = fj.state.ComputeWait(1)
	fj.countdown = -2

	return ctx.Err()
}

// StartMusic implements game.Interface.
func (fj *FlowerJourney) StartMusic() {
	audio.FlyingBee.PlayMusic(0, true)
}

// Logic implements game.Interface.
func (fj *FlowerJourney) Logic(ctx context.Context) error {
	switch fj.gameState {
	case stateStarting:
		fj.countdownWait--

		if fj.countdownWait != 0 {
			return ctx.Err()
		}

		fj.countdown--

		if fj.countdown == -1 {
			fj.gameState = stateActive

			// run first Active tick immediately
			return fj.Logic(ctx)
		}

		if fj.countdown == 0 {
			audio.FBStart.PlaySound(0, 0, 0)

			fj.countdownWait = fj.state.ComputeWait(fj.rules.GoTime)
		} else {
			if fj.countdown == -3 {
				fj.countdown = fj.rules.CountdownLength
			}

			audio.FBCountdown.PlaySound(0, 0, 0)

			fj.countdownWait = fj.state.ComputeWait(fj.rules.CountdownTime)
		}
	case statePaused:
		if fj.state.Input.Consume(arcade.BtnConfirm) {
			fj.backColor = sprites.White
			fj.gameState = stateActive
			fj.pausedFor += fj.state.TickCount - fj.pausedAt
		} else if fj.state.Input.Consume(arcade.BtnCancel) {
			fj.exitFade()
		}
	case stateActive:
		return fj.update(ctx)
	case stateEnding:
		if fj.state.TickCount >= fj.gameOverReady && fj.state.Input.Consume(arcade.BtnConfirm) {
			fj.exitFade()
		}
	case stateExitFade:
		fj.exitWait--

		if fj.exitWait == 0 {
			fj.state.Exit = true
		}
	}

	return ctx.Err()
}

func (fj *FlowerJourney) gameOver() {
	fj.state.FinalizeRecording()

	fj.backColor = sprites.Black
	fj.gameOverReady = fj.state.TickCount + fj.state.ComputeWait(1.5)

	audio.FBGameOver.PlaySound(1.5, 0, 0)

	fj.state.AISpamButton(arcade.BtnConfirm)
	fj.gameState = stateEnding
}

func (fj *FlowerJourney) exitFade() {
	fj.state.AIClearButtons()
	fj.state.FadeTo(0)
	fj.exitWait = fj.state.ComputeWait(1)
	fj.gameState = stateExitFade
}

func (fj *FlowerJourney) update(ctx context.Context) error {
	fj.scoreText = fmt.Sprintf("Score: %06.0f", fj.state.Score)
	fj.speed = -game.Lerp(fj.rules.SpeedStart, fj.rules.Speed100, float64(fj.progress)/100)

	if fj.invul > 0 {
		flashSpeed := 50.0
		if fj.invul >= 100 {
			flashSpeed = 35.0
		}

		fj.drawPlayer = math.Sin(float64(fj.state.TickCount-fj.pausedFor)/60*flashSpeed) > 0
	} else {
		fj.drawPlayer = true
	}

	if fj.state.Input.Consume(arcade.BtnConfirm) {
		fj.playerYVelocity = fj.rules.FlapVelocity
		fj.inputCooldown = 5
	} else if fj.state.Input.Consume(arcade.BtnPause) {
		fj.gameState = statePaused
		fj.pausedAt = fj.state.TickCount
		fj.backColor = sprites.Gray
	}

	if math.Sin(float64(fj.state.TickCount-fj.pausedFor)/60*25) < 0 {
		fj.playerSprite = 1
	} else {
		fj.playerSprite = 0
	}

	for i := range fj.walls {
		fj.walls[i].x += fj.speed
		if !fj.walls[i].passed && fj.walls[i].x <= fj.playerX {
			fj.state.Score += fj.rules.ScorePerWall + fj.rules.ScorePerWallCombo*float64(fj.combo)

			audio.FBPoint.PlaySound(0, 1, 0.5)

			fj.progress++
			fj.walls[i].passed = true
		}

		if fj.walls[i].x < -7 {
			fj.wallID++
			fj.walls[i].x = fj.walls[fj.wallID%len(fj.walls)].x + fj.rules.WallDist
			fj.walls[i].y = game.Lerp(fj.rules.WallMin, fj.rules.WallMax, fj.state.RNG.Float())
			fj.walls[i].gap = 2.5 * game.Lerp(fj.rules.WallGapStart, fj.rules.WallGap100, float64(fj.progress)/100)
			fj.walls[i].passed = false

			if !fj.itemEnabled {
				itemProgressMod := uint64(7)
				honeyChance := 3

				if fj.enemyEnabled {
					itemProgressMod = 5
					honeyChance = 5
				}

				if float64(fj.combo) >= fj.rules.HoneyMinCombo &&
					fj.itemProgress%itemProgressMod == 0 &&
					fj.state.RNG.RangeInt(0, 10) <= honeyChance {
					fj.itemType = itemHoney
				} else {
					fj.itemType = itemFlower
				}

				fj.itemEnabled = true
				fj.itemX = fj.walls[i].x + 2.5
				fj.itemY = game.Lerp(1.5, 4, fj.state.RNG.Float())
				fj.itemProgress++
			}

			enemyProgressMod := uint64(15)
			enemyChance := 2

			if fj.invul > 30 {
				enemyProgressMod = 5
				enemyChance = 3
			} else if fj.progress <= 100 {
				enemyProgressMod = 10
			}

			if float64(fj.combo) >= fj.rules.EnemyMinCombo &&
				float64(fj.progress) >= fj.rules.EnemyMinProgress &&
				fj.progress%enemyProgressMod == 0 &&
				!fj.enemyEnabled &&
				fj.state.RNG.RangeInt(0, 4) <= enemyChance {
				fj.enemyX = 7
				fj.enemyY = game.Lerp(fj.rules.EnemyHeightMin, fj.rules.EnemyHeightMax, fj.state.RNG.Float())
				fj.enemyEnabled = true
				fj.passedEnemy = false
			}
		}
	}

	for i := range fj.clouds {
		if i == 0 && !fj.passedHive {
			fj.clouds[i].x += fj.speed
		} else {
			fj.clouds[i].x += fj.speed * 0.35
		}

		if fj.clouds[i].x < -7 {
			fj.passedHive = true
			fj.cloudID++
			fj.clouds[i].x = fj.clouds[fj.cloudID%len(fj.clouds)].x + 3.5
			fj.clouds[i].y = game.Lerp(1.5, 4.5, fj.state.RNG.CheapFloat())
			fj.clouds[i].sprite = fj.state.RNG.RangeInt(0, 3)
		}
	}

	if fj.itemEnabled {
		fj.itemX += fj.speed
		fj.flipItemX = math.Sin(float64(fj.state.TickCount-fj.pausedFor)/60*15) > 0

		if fj.itemX < -7 {
			fj.itemEnabled = false
		}

		if fj.itemType == itemFlower && fj.itemX < fj.playerX-1 {
			fj.combo = 0
		}
	}

	if fj.enemyEnabled {
		if !fj.passedEnemy && fj.enemyX <= fj.playerX && math.Abs(fj.playerY-fj.enemyY) < 2 {
			audio.FBPoint.PlaySound(0, 1.1, 0.65)

			fj.state.Score += fj.rules.ScorePerPassedEnemy + fj.rules.ScorePerPassedEnemyCombo*float64(fj.combo)
			fj.passedEnemy = true
		}

		fj.enemyX += fj.speed * fj.rules.EnemySpeed

		if fj.progress > 150 {
			fj.enemyY += math.Sin(float64(fj.state.TickCount-fj.pausedFor) / 60 * 10)
		}

		if math.Sin(float64(fj.state.TickCount-fj.pausedFor)/60*45) <= 0 {
			fj.enemySprite = 1
		} else {
			fj.enemySprite = 0
		}

		if fj.itemX < -7 {
			fj.enemyEnabled = false
		}
	}

	fj.gpos0 += fj.speed
	fj.gpos1 += fj.speed * 0.65

	if fj.gpos0 < -1 {
		fj.gpos0 = math.Abs(fj.gpos0)
	}

	if fj.gpos1 < -1 {
		fj.gpos1 = math.Abs(fj.gpos1)
	}

	if fj.inputCooldown > 0 {
		fj.inputCooldown--
	}

	if fj.invul > 0 {
		fj.invul--
	}

	fj.playerYVelocity += -9.8 * fj.rules.GravityScale / 60
	fj.playerY += fj.playerYVelocity / 60

	if fj.playerDying {
		fj.playerX += fj.playerXVelocity / 60
		fj.playerAngle += fj.playerAngleVelocity / 60
	}

	if fj.itemEnabled && fj.checkCollision(fj.itemX, fj.itemY, 0.5, 0.5) {
		fj.useItem()
	}

	if fj.enemyEnabled && fj.checkCollision(fj.enemyX, fj.enemyY, 0.4, 0.3) {
		fj.dead(true, false)
	}

	if fj.playerY-0.05-0.1 < 0.2012 || fj.playerY-0.05+0.1 > 5.25 {
		fj.dead(false, true)
	}

	for _, wall := range fj.walls {
		if fj.checkCollision(wall.x, wall.y, 0.36, 1.8375) ||
			fj.checkCollision(wall.x, wall.y+wall.gap, 0.36, 1.8375) {
			fj.dead(false, fj.checkCollision(wall.x, wall.y+wall.gap/2, 0.36, wall.gap-1.8375))
		}
	}

	return ctx.Err()
}

func (fj *FlowerJourney) checkCollision(x, y, w, h float64) bool {
	x0 := fj.playerX - 0.3
	x1 := fj.playerX + 0.3
	y0 := fj.playerY - 0.05 - 0.2
	y1 := fj.playerY - 0.05 + 0.2

	overlapX := x-w < x1 && x+w > x0
	overlapY := y-h < y1 && y+h > y0

	return overlapX && overlapY
}

func (fj *FlowerJourney) doCombo() {
	fj.lastCombo = fj.state.TickCount
	fj.comboY = fj.playerY
}

func (fj *FlowerJourney) useItem() {
	if fj.gameState != stateEnding {
		if fj.itemType == itemFlower {
			audio.FBFlower.PlaySound(0, math.Min(1+float64(fj.combo)*0.05, 1.25), 0)

			fj.state.Score += fj.rules.ScorePerFlower + fj.rules.ScorePerFlowerCombo*float64(fj.combo)
			fj.combo++
			fj.doCombo()
		} else {
			fj.invul = uint64(fj.rules.HoneyInvulnTime * 60)

			audio.FBStart.PlaySound(0, 0, 0)
		}

		fj.itemX = 0
		fj.itemY = 999
		fj.itemEnabled = false
	}
}

func (fj *FlowerJourney) dead(isEnemy, horizontal bool) {
	switch {
	case fj.gameState == stateEnding:
		if !isEnemy {
			// this isn't real physics but it's good enough for now
			fj.playerAngleVelocity = -fj.playerAngleVelocity

			if horizontal {
				fj.playerYVelocity = -fj.playerYVelocity
			} else {
				fj.playerXVelocity = -fj.playerXVelocity
			}
		}
	case fj.invul <= 0:
		fj.playerDying = true
		fj.playerXVelocity = -5
		fj.playerYVelocity = 15
		fj.playerAngleVelocity = -20 * math.Pi / 180

		audio.StopMusic()
		audio.FBDeath.PlaySound(0, 0, 0)

		fj.gameOver()
	case isEnemy:
		fj.state.Score += fj.rules.ScorePerKilledEnemy + fj.rules.ScorePerKilledEnemyCombo*float64(fj.combo)
		fj.combo++

		audio.FBStart.PlaySound(0, 1.1, 0)

		fj.doCombo()
		fj.enemyEnabled = false
	}
}

// Cleanup implements game.Interface.
func (fj *FlowerJourney) Cleanup(ctx context.Context) error {
	fj.state.Camera.PopTransform()

	return nil
}

// SaveState implements game.Interface.
func (fj *FlowerJourney) SaveState() interface{} {
	return *fj
}

// LoadState implements game.Interface.
func (fj *FlowerJourney) LoadState(v interface{}) {
	fj.state.Camera.SetDefaults()
	fj.state.Camera.Position = gfx.Translation(0, 70, 3)
	fj.state.Camera.Rotation = gfx.Identity()

	*fj = v.(FlowerJourney)
}
