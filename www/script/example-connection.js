"use strict";
(function (ecd) {
    if (!window.RTCPeerConnection) {
        return;
    }
    ecd.style.display = "";
    ecd.querySelector("button").addEventListener("click", function (e) {
        e.preventDefault();
        ecd.querySelector("p").textContent = "An example connection establishment log is displayed below.";
        const table = ecd.querySelector("table");
        const headerRow = table.querySelector("thead tr");
        SpyCards.UI.clear(headerRow);
        let th = document.createElement("th");
        th.textContent = "Player 1";
        headerRow.appendChild(th);
        th = document.createElement("th");
        th.textContent = "Player 2";
        headerRow.appendChild(th);
        const tbody = table.querySelector("tbody");
        function appendRow(left, right) {
            const tr = document.createElement("tr");
            let td = document.createElement("td");
            let pre = document.createElement("pre");
            pre.textContent = left;
            td.appendChild(pre);
            tr.appendChild(td);
            td = document.createElement("td");
            pre = document.createElement("pre");
            pre.textContent = right;
            td.appendChild(pre);
            tr.appendChild(td);
            tbody.appendChild(tr);
        }
        const ss1 = {}, ss2 = {};
        ss1.sendRelayedMessage = function (msg) {
            appendRow("r" + JSON.stringify(msg), "");
            ss2.onRelayedMessage(msg);
        };
        ss2.sendRelayedMessage = function (msg) {
            appendRow("", "r" + JSON.stringify(msg));
            ss1.onRelayedMessage(msg);
        };
        const p1 = new CardGameConnection();
        const p2 = new CardGameConnection();
        p1.useSignalingServer(ss1, 1);
        p2.useSignalingServer(ss2, 2);
        ss1.onHandshake();
        ss2.onHandshake();
    });
})(document.getElementById("example-connection-data"));
