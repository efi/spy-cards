package format

import (
	"bytes"
	"encoding/binary"

	"golang.org/x/xerrors"
)

// Reader decodes data from a binary format.
//
// There should be a deferred call to Catch when Reader is in use.
type Reader struct {
	buf []byte
	pos int
}

// Init sets the Reader's buffer to b.
func (r *Reader) Init(b []byte) {
	r.buf = b
	r.pos = 0
}

// Len returns the remaining number of bytes in the buffer.
func (r *Reader) Len() int {
	return len(r.buf) - r.pos
}

// Peek returns the next n bytes in the buffer without consuming them.
//
// Peek panics if n is greater than Len.
func (r *Reader) Peek(n int) []byte {
	return r.buf[:len(r.buf):len(r.buf)][r.pos:][:n]
}

// SVarInt decodes a signed varint from the Reader.
func (r *Reader) SVarInt() int64 {
	n, count := binary.Varint(r.buf[r.pos:])
	if count == 0 {
		Throw(xerrors.New("format: reached end of buffer while reading signed integer"))
	}

	if count < 0 {
		Throw(xerrors.New("format: signed integer is larger than 64 bits"))
	}

	r.pos += count

	return n
}

// UVarInt decodes an unsigned varint from the Reader.
func (r *Reader) UVarInt() uint64 {
	n, count := binary.Uvarint(r.buf[r.pos:])
	if count == 0 {
		Throw(xerrors.New("format: reached end of buffer while reading unsigned integer"))
	}

	if count < 0 {
		Throw(xerrors.New("format: unsigned integer is larger than 64 bits"))
	}

	r.pos += count

	return n
}

// Byte consumes the first byte remaining in the buffer.
func (r *Reader) Byte() byte {
	if r.pos == len(r.buf) {
		Throw(xerrors.New("format: reached end of buffer while reading byte"))
	}

	r.pos++

	return r.buf[r.pos-1]
}

// length is an internal function that ensures the result of calling UVarInt
// fits within an int.
func (r *Reader) length() int {
	length := r.UVarInt()
	if int(length) < 0 || uint64(int(length)) != length {
		Throw(xerrors.Errorf("format: buffer length %d overflows", length))
	}

	return int(length)
}

// SubReader returns a Reader for a length-prefixed section of r.
func (r *Reader) SubReader() *Reader {
	length := r.length()

	return &Reader{
		buf: r.Bytes(length),
	}
}

// Bytes returns the next count bytes from the Reader.
func (r *Reader) Bytes(count int) []byte {
	if count < 0 {
		Throw(xerrors.Errorf("format: cannot read %d bytes: count is negative", count))
	}

	if r.pos+count > len(r.buf) {
		Throw(xerrors.Errorf("format: cannot read %d bytes: only %d remaining in buffer", count, len(r.buf)-r.pos))
	}

	start := r.pos
	r.pos += count

	return r.buf[start:][:count:count]
}

// String decodes a length-prefixed string from the Reader.
func (r *Reader) String() string {
	length := r.length()
	b := r.Bytes(length)

	return string(b)
}

// String1 decodes a length-prefixed string from the Reader.
//
// The maximum length of a string in this legacy format is 255 characters.
func (r *Reader) String1() string {
	var length uint8

	r.Read(&length)
	b := r.Bytes(int(length))

	return string(b)
}

// Read decodes a fixed-size data structure from the Reader.
func (r *Reader) Read(v interface{}) {
	size := binary.Size(v)
	if size == -1 {
		Throw(xerrors.Errorf("format: cannot read type %T: non-constant size", v))
	}

	b := r.Bytes(size)
	if err := binary.Read(bytes.NewReader(b), binary.LittleEndian, v); err != nil {
		Throw(xerrors.Errorf("format: failed to read type %T: %w", v, err))
	}
}
