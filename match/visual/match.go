// +build !headless

package visual

import (
	"context"
	"strconv"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
)

func (v *Visual) updateReadyToStart(ctx context.Context) bool {
	d, ok := v.gc.RecvDeck()
	if !ok {
		return false
	}

	if len(d) != int(v.Match.Rules.CardsPerDeck) {
		v.abortMatch("Opponent sent deck with " + strconv.Itoa(len(d)) + " cards, but mode requires " + strconv.FormatUint(v.Match.Rules.CardsPerDeck, 10))

		return true
	}

	totalUnique := int(v.Match.Rules.BossCards + v.Match.Rules.MiniBossCards)

	for i, b := range d {
		var expected card.Rank

		switch {
		case i < int(v.Match.Rules.BossCards):
			expected = card.Boss
		case i < totalUnique:
			expected = card.MiniBoss
		default:
			expected = card.Enemy
		}

		if b != expected {
			v.abortMatch("Opponent's deck has " + b.String() + " card in slot " + strconv.Itoa(i+1) + " (should be " + expected.String() + ")")

			return true
		}
	}

	deck := make([]*match.Card, len(d))

	for i, b := range d {
		deck[i] = &match.Card{
			Set:  v.Match.Set,
			Back: b,
		}
	}

	v.Match.State.Sides[2-v.Match.Perspective].Deck = deck

	audio.BattleStart0.PlaySound(0, 0, 0)
	v.playDefaultMusic(ctx)

	v.state = StateNextRound
	v.myHand.MinPerRow = int(v.Match.Rules.HandMaxSize)
	v.theirHand.MinPerRow = int(v.Match.Rules.HandMaxSize)
	v.myHand.MaxPerRow = int(v.Match.Rules.HandMaxSize)
	v.theirHand.MaxPerRow = int(v.Match.Rules.HandMaxSize)
	v.timerBank = v.Match.Timer.StartTime * 60

	return true
}
