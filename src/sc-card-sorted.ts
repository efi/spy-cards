module SpyCards {
	export class CardDefs {
		public static readonly cardBacks = ["enemy", "mini-boss", "boss"];
		public readonly cardBacks = CardDefs.cardBacks;
		public readonly allCards: CardDef[] = [];
		public readonly cardsByID: { [id: number]: CardDef } = {};
		public readonly cardsByBack: CardDef[][] = [[], [], []];
		public readonly nonRandom: { [rank: number]: CardDef[] } = {
			[Rank.None]: [],
			[Rank.Enemy]: [],
			[Rank.Attacker]: [],
			[Rank.Effect]: [],
			[Rank.MiniBoss]: [],
			[Rank.Boss]: []
		};
		public readonly byTribe: { [rank: number]: { [tribe: string]: CardDef[] } } = {
			[Rank.None]: {},
			[Rank.Enemy]: {},
			[Rank.Attacker]: {},
			[Rank.Effect]: {},
			[Rank.MiniBoss]: {},
			[Rank.Boss]: {},
		};
		public readonly banned: { [id: number]: boolean } = {};
		public readonly unpickable: { [id: number]: boolean } = {};
		public readonly unfilter: { [id: number]: boolean } = {};
		public pr: boolean;
		public readonly mode: GameModeData;
		public readonly rules = new GameModeGameRules();
		public readonly customCardsRaw: string[];

		constructor(overrides?: ParsedCustomCards) {
			this.allCards = CardData.createVanillaCardDefs(overrides ? overrides.packedVersion : null);
			if (overrides) {
				if (overrides.mode) {
					this.mode = overrides.mode;
					for (let banList of overrides.mode.getAll(GameModeFieldType.BannedCards)) {
						if (banList.bannedCards.length === 0) {
							for (let card of this.allCards) {
								this.banned[card.id] = true;
							}
						} else {
							for (let id of banList.bannedCards) {
								this.banned[id] = true;
							}
						}
					}
					for (let unfilter of overrides.mode.getAll(GameModeFieldType.UnfilterCard)) {
						this.unfilter[unfilter.card] = true;
					}
					this.rules = overrides.mode.get(GameModeFieldType.GameRules) || new GameModeGameRules();
				} else {
					this.mode = null;
				}
				this.allCards = this.allCards.filter((c) => !overrides.cards.some((o) => o.id === c.id));
				this.allCards.push(...overrides.cards);
				this.customCardsRaw = overrides.customCardsRaw;
			} else {
				this.customCardsRaw = [];
				this.mode = null;
			}
			for (let cd of this.allCards) {
				const rank = cd.rank();
				this.cardsByID[cd.id] = cd;
				if (cd.effectiveTP() !== Infinity) { // negative infinity is okay
					this.cardsByBack[cd.getBackID()].push(cd);
				}
				if (!cd.effects.some((effect) => effect.type === EffectType.Summon && effect.negate)) {
					this.nonRandom[Rank.None].push(cd);
					if (rank === Rank.Attacker || rank === Rank.Effect) {
						this.nonRandom[Rank.Enemy].push(cd);
					}
					this.nonRandom[rank].push(cd);
				}
				this.indexCard(cd, Rank.None, Tribe.None);
				if (rank === Rank.Attacker || rank === Rank.Effect) {
					this.indexCard(cd, Rank.Enemy, Tribe.None);
				}
				this.indexCard(cd, rank, Tribe.None);
				for (let t of cd.tribes) {
					this.indexCard(cd, Rank.None, t.tribe, t.custom);
					if (rank === Rank.Attacker || rank === Rank.Effect) {
						this.indexCard(cd, Rank.Enemy, t.tribe, t.custom);
					}
					this.indexCard(cd, rank, t.tribe, t.custom);
				}
			}
		}

		private indexCard(card: CardDef, rank: Rank, tribe: Tribe, custom?: CustomTribeData) {
			const tribeName = tribe === Tribe.Custom ? "?" + custom.name : Tribe[tribe];
			if (!this.byTribe[rank][tribeName]) {
				this.byTribe[rank][tribeName] = [];
			}
			this.byTribe[rank][tribeName].push(card);
		}

		public getAvailableCards(rank: Rank, tribe: Tribe, customTribe?: string): CardDef[] {
			const tribeName = tribe === Tribe.Custom ? "?" + customTribe : Tribe[tribe];
			return (this.byTribe[rank][tribeName] || []).filter((c) => !this.banned[c.id]);
		}
	}
}
