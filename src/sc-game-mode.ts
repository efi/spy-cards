module SpyCards {
	export class GameModeData {
		public fields: GameModeField[] = [];

		get<T extends keyof GameModeFieldTypes>(type: T): GameModeFieldTypes[T] {
			return <GameModeFieldTypes[T]><unknown>this.fields.find((f) => f.getType() === type);
		}
		getAll<T extends keyof GameModeFieldTypes>(type: T): GameModeFieldTypes[T][] {
			return <GameModeFieldTypes[T][]><unknown>this.fields.filter((f) => f.getType() === type);
		}

		marshal(buf: number[]) {
			Binary.pushUVarInt(buf, 3); // format version
			Binary.pushUVarInt(buf, this.fields.length);
			for (let field of this.fields) {
				field.marshal(buf);
			}
		}
		unmarshal(buf: number[]) {
			const formatVersion = Binary.shiftUVarInt(buf);
			if (formatVersion !== 3) {
				throw new Error("unexpected game mode format version: " + formatVersion);
			}
			const fieldCount = Binary.shiftUVarInt(buf);
			this.fields = new Array(fieldCount);
			for (let i = 0; i < fieldCount; i++) {
				this.fields[i] = GameModeField.unmarshal(buf);
			}
			if (buf.length) {
				throw new Error("extra data after end of game mode");
			}
		}
	}

	export enum GameModeFieldType {
		Metadata,
		BannedCards,
		GameRules,
		SummonCard,
		Variant,
		UnfilterCard,
		DeckLimitFilter,
		Timer,
	}

	export abstract class GameModeField {
		abstract getType(): GameModeFieldType;
		abstract marshalData(buf: number[]): void;
		abstract unmarshalData(buf: number[]): void;
		abstract replaceCardID(from: CardData.CardID, to: CardData.CardID): void;

		marshal(buf: number[]) {
			Binary.pushUVarInt(buf, this.getType());
			const data: number[] = [];
			this.marshalData(data);
			Binary.pushUVarInt(buf, data.length);
			buf.push(...data);
		}
		static unmarshal(buf: number[]) {
			const type: GameModeFieldType = Binary.shiftUVarInt(buf);
			if (!GameModeFieldConstructors[type]) {
				throw new Error("unknown game mode field type " + type);
			}
			const field = GameModeFieldConstructors[type]();
			const dataLength = Binary.shiftUVarInt(buf);
			if (buf.length < dataLength) {
				throw new Error("game mode field extends past end of buffer");
			}
			const data: number[] = buf.splice(0, dataLength);
			field.unmarshalData(data);
			if (data.length) {
				throw new Error("game mode field (" + GameModeFieldType[type] + ") did not decode full buffer");
			}
			return field;
		}
	}

	export class GameModeMetadata extends GameModeField {
		public title: string = "";
		public author: string = "";
		public description: string = "";
		public latestChanges: string = "";

		getType(): GameModeFieldType {
			return GameModeFieldType.Metadata;
		}
		marshalData(buf: number[]): void {
			Binary.pushUTF8StringVar(buf, this.title);
			Binary.pushUTF8StringVar(buf, this.author);
			Binary.pushUTF8StringVar(buf, this.description);
			Binary.pushUTF8StringVar(buf, this.latestChanges);
		}
		unmarshalData(buf: number[]): void {
			this.title = Binary.shiftUTF8StringVar(buf);
			this.author = Binary.shiftUTF8StringVar(buf);
			this.description = Binary.shiftUTF8StringVar(buf);
			this.latestChanges = Binary.shiftUTF8StringVar(buf);
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void { }
	}

	export class GameModeBannedCards extends GameModeField {
		public bannedCards: CardData.CardID[] = [];

		getType(): GameModeFieldType {
			return GameModeFieldType.BannedCards;
		}
		marshalData(buf: number[]): void {
			Binary.pushUVarInt(buf, this.bannedCards.length);
			for (let card of this.bannedCards) {
				Binary.pushUVarInt(buf, card);
			}
		}
		unmarshalData(buf: number[]): void {
			const count = Binary.shiftUVarInt(buf);
			this.bannedCards = new Array(count);
			for (let i = 0; i < count; i++) {
				this.bannedCards[i] = Binary.shiftUVarInt(buf);
			}
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void {
			this.bannedCards = this.bannedCards.map((c) => c === from ? to : c);
		}
	}

	export enum GameRuleType {
		None,
		MaxHP,
		HandMinSize,
		HandMaxSize,
		DrawPerTurn,
		CardsPerDeck,
		MinTP,
		MaxTP,
		TPPerTurn,
		BossCards,
		MiniBossCards,
	}

	export class GameModeGameRules extends GameModeField {
		public static typeName(type: GameRuleType): string {
			return GameRuleType[type].replace(/([a-z])([A-Z])|([A-Z])([A-Z][a-z])/g, "$1$3 $2$4");
		}
		public static readonly rules: {
			type: GameRuleType;
			get: (r: GameModeGameRules) => number;
			set: (r: GameModeGameRules, n: number) => void;
			signed?: boolean;
			max?: number;
		}[] = [
				{ type: GameRuleType.MaxHP, get: (r) => r.maxHP, set: (r, n) => r.maxHP = n },
				{ type: GameRuleType.HandMinSize, get: (r) => r.handMinSize, set: (r, n) => r.handMinSize = n },
				{ type: GameRuleType.HandMaxSize, get: (r) => r.handMaxSize, set: (r, n) => r.handMaxSize = n, max: 50 },
				{ type: GameRuleType.DrawPerTurn, get: (r) => r.drawPerTurn, set: (r, n) => r.drawPerTurn = n },
				{ type: GameRuleType.CardsPerDeck, get: (r) => r.cardsPerDeck, set: (r, n) => r.cardsPerDeck = n },
				{ type: GameRuleType.MinTP, get: (r) => r.minTP, set: (r, n) => r.minTP = n },
				{ type: GameRuleType.MaxTP, get: (r) => r.maxTP, set: (r, n) => r.maxTP = n },
				{ type: GameRuleType.TPPerTurn, get: (r) => r.tpPerTurn, set: (r, n) => r.tpPerTurn = n },
				{ type: GameRuleType.BossCards, get: (r) => r.bossCards, set: (r, n) => r.bossCards = n },
				{ type: GameRuleType.MiniBossCards, get: (r) => r.miniBossCards, set: (r, n) => r.miniBossCards = n },
			];

		public maxHP: number = 5;
		public handMinSize: number = 3;
		public handMaxSize: number = 5;
		public drawPerTurn: number = 2;
		public cardsPerDeck: number = 15;
		public minTP: number = 2;
		public maxTP: number = 10;
		public tpPerTurn: number = 1;
		public bossCards: number = 1;
		public miniBossCards: number = 2;

		getType(): GameModeFieldType {
			return GameModeFieldType.GameRules;
		}
		marshalData(buf: number[]): void {
			const defaults = new GameModeGameRules();
			for (let { type, get, signed } of GameModeGameRules.rules) {
				if (get(this) === get(defaults)) {
					continue;
				}

				Binary.pushUVarInt(buf, type);
				if (signed) {
					Binary.pushSVarInt(buf, get(this));
				} else {
					Binary.pushUVarInt(buf, get(this));
				}
			}
			Binary.pushUVarInt(buf, GameRuleType.None);
		}
		unmarshalData(buf: number[]): void {
			const defaults = new GameModeGameRules();
			for (let { get, set } of GameModeGameRules.rules) {
				set(this, get(defaults));
			}

			for (; ;) {
				const type: GameRuleType = Binary.shiftUVarInt(buf);
				if (type === GameRuleType.None) {
					break;
				}

				const rule = GameModeGameRules.rules.find((r) => r.type === type);
				if (!rule) {
					throw new Error("unknown game rule ID: " + type);
				}

				const { get, set, signed, max } = rule;
				if (signed) {
					set(this, Binary.shiftSVarInt(buf));
				} else {
					set(this, Binary.shiftUVarInt(buf));
				}
				if (max && get(this) > max) {
					set(this, max);
				}
			}
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void { }
	}

	export enum SummonCardFlags {
		BothPlayers = 1 << 0,
	}

	export class GameModeSummonCard extends GameModeField {
		public flags: number = SummonCardFlags.BothPlayers;
		public card: CardData.CardID = CardData.GlobalCardID.Seedling;

		getType(): GameModeFieldType {
			return GameModeFieldType.SummonCard;
		}
		marshalData(buf: number[]): void {
			Binary.pushUVarInt(buf, this.flags);
			Binary.pushUVarInt(buf, this.card);
		}
		unmarshalData(buf: number[]): void {
			this.flags = Binary.shiftUVarInt(buf);
			this.card = Binary.shiftUVarInt(buf);
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void {
			if (this.card === from) {
				this.card = to;
			}
		}
	}

	export class GameModeVariant extends GameModeField {
		public title: string = "";
		public npc: string = "";
		public rules: GameModeField[] = [];

		getType(): GameModeFieldType {
			return GameModeFieldType.Variant;
		}
		marshalData(buf: number[]): void {
			Binary.pushUTF8StringVar(buf, this.title);
			Binary.pushUTF8StringVar(buf, this.npc);
			Binary.pushUVarInt(buf, this.rules.length);
			for (let rule of this.rules) {
				rule.marshal(buf);
			}
		}
		unmarshalData(buf: number[]): void {
			this.title = Binary.shiftUTF8StringVar(buf);
			this.npc = Binary.shiftUTF8StringVar(buf);
			const numRules = Binary.shiftUVarInt(buf);
			this.rules.length = numRules;
			for (let i = 0; i < numRules; i++) {
				this.rules[i] = GameModeField.unmarshal(buf);
			}
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void {
			for (let rule of this.rules) {
				rule.replaceCardID(from, to);
			}
		}
	}

	export class GameModeUnfilterCard extends GameModeField {
		public flags: number = 0;
		public card: CardData.CardID = CardData.GlobalCardID.Seedling;

		getType(): GameModeFieldType {
			return GameModeFieldType.UnfilterCard;
		}
		marshalData(buf: number[]): void {
			Binary.pushUVarInt(buf, this.flags);
			Binary.pushUVarInt(buf, this.card);
		}
		unmarshalData(buf: number[]): void {
			this.flags = Binary.shiftUVarInt(buf);
			this.card = Binary.shiftUVarInt(buf);
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void {
			if (this.card === from) {
				this.card = to;
			}
		}
	}

	export class GameModeDeckLimitFilter extends GameModeField {
		public count: number = 0;
		public rank: Rank = Rank.Attacker;
		public tribe: Tribe = Tribe.Seedling;
		public customTribe: string = "";
		public card: CardData.CardID = CardData.GlobalCardID.Zombiant;

		getType(): GameModeFieldType {
			return GameModeFieldType.DeckLimitFilter;
		}
		marshalData(buf: number[]): void {
			Binary.pushUVarInt(buf, this.count);
			buf.push((this.rank << 4) | this.tribe);
			if (this.tribe === Tribe.Custom) {
				Binary.pushUTF8StringVar(buf, this.customTribe);
			}
			if (this.rank === Rank.None && this.tribe === Tribe.None) {
				Binary.pushUVarInt(buf, this.card);
			}
		}
		unmarshalData(buf: number[]): void {
			this.count = Binary.shiftUVarInt(buf);
			const rankTribe = buf.shift();
			this.rank = rankTribe >> 4;
			this.tribe = rankTribe & 15;
			if (this.tribe === Tribe.Custom) {
				this.customTribe = Binary.shiftUTF8StringVar(buf);
			} else {
				this.customTribe = "";
			}
			if (this.rank === Rank.None && this.tribe === Tribe.None) {
				this.card = Binary.shiftUVarInt(buf);
			} else {
				this.card = CardData.GlobalCardID.Zombiant;
			}
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void {
			if (this.card === from) {
				this.card = to;
			}
		}
		match(card: CardDef): boolean {
			if (!card) {
				return false;
			}

			if (this.rank === Rank.None && this.tribe === Tribe.None) {
				return card.id === this.card;
			}

			if (this.rank === Rank.Enemy) {
				if (card.rank() !== Rank.Attacker && card.rank() !== Rank.Effect) {
					return false;
				}
			} else if (this.rank !== Rank.None && card.rank() !== this.rank) {
				return false;
			}

			if (this.tribe === Tribe.Custom) {
				if (!card.tribes.some((t) => t.tribe === Tribe.Custom && t.custom.name === this.customTribe)) {
					return false;
				}
			} else if (this.tribe !== Tribe.None) {
				if (!card.tribes.some((t) => t.tribe === this.tribe)) {
					return false;
				}
			}

			return true;
		}
	}

	export class GameModeTimer extends GameModeField {
		public startTime: number = 150;
		public maxTime: number = 300;
		public perTurn: number = 10;
		public maxPerTurn: number = 90;

		getType(): GameModeFieldType {
			return GameModeFieldType.Timer;
		}
		marshalData(buf: number[]): void {
			Binary.pushUVarInt(buf, this.startTime);
			Binary.pushUVarInt(buf, this.maxTime);
			Binary.pushUVarInt(buf, this.perTurn);
			Binary.pushUVarInt(buf, this.maxPerTurn);
		}
		unmarshalData(buf: number[]): void {
			this.startTime = Binary.shiftUVarInt(buf);
			this.maxTime = Binary.shiftUVarInt(buf);
			this.perTurn = Binary.shiftUVarInt(buf);
			this.maxPerTurn = Binary.shiftUVarInt(buf);
		}
		replaceCardID(from: CardData.CardID, to: CardData.CardID): void {
			// no card IDs in this field
		}
	}

	export interface GameModeFieldTypes {
		[GameModeFieldType.Metadata]: GameModeMetadata;
		[GameModeFieldType.BannedCards]: GameModeBannedCards;
		[GameModeFieldType.GameRules]: GameModeGameRules;
		[GameModeFieldType.SummonCard]: GameModeSummonCard;
		[GameModeFieldType.Variant]: GameModeVariant;
		[GameModeFieldType.UnfilterCard]: GameModeUnfilterCard;
		[GameModeFieldType.DeckLimitFilter]: GameModeDeckLimitFilter;
		[GameModeFieldType.Timer]: GameModeTimer;
	}

	export const GameModeFieldConstructors: {
		[T in keyof GameModeFieldTypes]: () => GameModeFieldTypes[T]
	} = {
		[GameModeFieldType.Metadata]: () => new GameModeMetadata(),
		[GameModeFieldType.BannedCards]: () => new GameModeBannedCards(),
		[GameModeFieldType.GameRules]: () => new GameModeGameRules(),
		[GameModeFieldType.SummonCard]: () => new GameModeSummonCard(),
		[GameModeFieldType.Variant]: () => new GameModeVariant(),
		[GameModeFieldType.UnfilterCard]: () => new GameModeUnfilterCard(),
		[GameModeFieldType.DeckLimitFilter]: () => new GameModeDeckLimitFilter(),
		[GameModeFieldType.Timer]: () => new GameModeTimer(),
	};
}
