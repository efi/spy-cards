#!/bin/bash -e

# This relies on some scripts I have locally.
# You're probably looking for build.sh.

cid=$(ipfs add --quieter --recursive --pin=false www)
ipfs files rm -r /data/spy-cards.lubar.me
ipfs files cp "/ipfs/$cid" /data/spy-cards.lubar.me

ipfs files write --truncate /data/dl.spy-cards.lubar.me/latest/spy-cards < spy-cards
ipfs files write --truncate /data/dl.spy-cards.lubar.me/latest/spy-cards.exe < spy-cards.exe
ipfs files write --truncate /data/dl.spy-cards.lubar.me/latest/assets.zip < assets.zip

publish-ipfs spy-cards.lubar.me
publish-ipfs dl.spy-cards.lubar.me
