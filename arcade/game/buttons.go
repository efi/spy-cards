package game

import (
	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
)

// DrawButtons causes buttons pressed in a game to be displayed on the screen.
var DrawButtons bool

// DrawButtons displays the currently-held buttons on the screen.
func (s *State) DrawButtons(tb *sprites.TextBatch, x, y, z, scale float32) {
	if !DrawButtons && s.Playback == nil {
		settings := internal.LoadSettings()

		if !settings.DisplayTermacadeButtons {
			return
		}
	}

	var str []byte

	for btn := arcade.BtnUp; btn <= arcade.BtnHelp; btn++ {
		if s.Input.Held(btn) {
			str = append(str, sprites.Button(btn)...)
		}
	}

	sprites.DrawText(tb, sprites.FontD3Streetism, string(str), x, y, z, scale, scale, sprites.White)
}
