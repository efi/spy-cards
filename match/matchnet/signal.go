package matchnet

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
	"nhooyr.io/websocket"
)

// SignalConn is a connection to the matchmaking server.
type SignalConn struct {
	fake *SignalConn
	ws   *websocket.Conn

	sessionID       string
	sessionIDReady  chan struct{}
	connectionReady bool
	didHandshake    bool
	dead            context.CancelFunc

	onHandshake      func()
	onCloseCode      func(websocket.StatusCode, string)
	onRelayedMessage func(*relayedMessage)
}

// NewSignalConn creates a matchmaking server connection.
func NewSignalConn(ctx context.Context) (*SignalConn, error) {
	var err error

	sc := &SignalConn{}

	ctx, sc.dead = context.WithCancel(ctx)

	sc.ws, _, err = websocket.Dial(ctx, internal.GetConfig(ctx).MatchmakingServer, nil)
	if err != nil {
		sc.dead()

		return nil, xerrors.Errorf("matchnet: dialing matchmaking server: %w", err)
	}

	sc.sessionIDReady = make(chan struct{})

	go sc.read(ctx)
	go sc.keepAlive(ctx)

	return sc, nil
}

// NewFakeSignalConn creates a mock matchmaking server connection pair.
func NewFakeSignalConn() (*SignalConn, *SignalConn) {
	sc1 := &SignalConn{}
	sc2 := &SignalConn{}

	sc1.fake = sc2
	sc2.fake = sc1

	sc1.sessionIDReady = make(chan struct{})
	sc2.sessionIDReady = make(chan struct{})

	return sc1, sc2
}

func (sc *SignalConn) read(ctx context.Context) {
	defer sc.dead()

	defer func() {
		select {
		case <-sc.sessionIDReady:
		default:
			close(sc.sessionIDReady)
		}
	}()

	for {
		_, b, err := sc.ws.Read(ctx)
		if err != nil {
			if ctx.Err() == nil {
				log.Printf("error reading from signaling connection: %+v", err)
			}

			if sc.onCloseCode != nil {
				var ce websocket.CloseError

				if xerrors.As(err, &ce) {
					sc.onCloseCode(ce.Code, ce.Reason)
				} else {
					sc.onCloseCode(-1, err.Error())
				}
			}

			return
		}

		if !sc.onMessage(b) {
			return
		}
	}
}

func (sc *SignalConn) onMessage(b []byte) bool {
	if len(b) == 0 {
		return true
	}

	switch matchmakingMessageType(b[0]) {
	case msgSessionID:
		// session id
		select {
		case <-sc.sessionIDReady:
		default:
			sc.sessionID = string(b[1:])
			close(sc.sessionIDReady)
		}
	case msgKeepAlive:
		// keep-alive
	case msgHandshake:
		// handshake
		sc.didHandshake = true

		if sc.onHandshake != nil {
			sc.onHandshake()
		}
	case msgRelayed:
		// relayed message
		var r relayedMessage

		if err := json.Unmarshal(b[1:], &r); err != nil {
			log.Printf("error decoding relayed message: %+v", err)

			return true
		}

		sc.onRelayedMessage(&r)
	case msgQuit:
		// quit
		if !sc.connectionReady {
			if sc.onCloseCode != nil {
				sc.onCloseCode(internal.MMConnectionError, "Opponent sent quit notification.")
			}
		}

		if err := sc.ws.Close(websocket.StatusNormalClosure, "quit"); err != nil {
			log.Printf("error closing signaling connection: %+v", err)
		}

		return false
	default:
		log.Println("ERROR: unexpected signaling server message:", b)
	}

	return true
}

func (sc *SignalConn) keepAlive(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case <-time.After(45 * time.Second):
		}

		if err := sc.ws.Write(ctx, websocket.MessageText, []byte{byte(msgKeepAlive)}); err != nil {
			log.Printf("error sending keepalive to signaling connection: %+v", err)

			return
		}
	}
}

// Close terminates the matchmaking server connection.
func (sc *SignalConn) Close() error {
	sc.onCloseCode = func(websocket.StatusCode, string) {}

	sc.dead()

	if sc.fake != nil {
		return nil
	}

	_ = sc.ws.Write(context.Background(), websocket.MessageText, []byte{byte(msgQuit)})

	return sc.ws.Close(websocket.StatusNormalClosure, "quit")
}

// InitPlayer starts the matchmaking process as either Player 1 ("1"), Player 2 ("2"), or Quick Join ("q").
func (sc *SignalConn) InitPlayer(ctx context.Context, playerNumber, versionSuffix string) error {
	if sc.fake != nil {
		switch playerNumber {
		case "q":
			return xerrors.New("match: quick join not implemented in mock signaling connection")
		case "1":
			sc.onMessage([]byte("sFAKE FAKE FAKE FAKE FAKE FAKE"))
		}

		return nil
	}

	return sc.ws.Write(ctx, websocket.MessageText, append(append([]byte{byte(msgHandshake)}, playerNumber...), versionSuffix...))
}

// HaveSessionID returns true if the session ID has been obtained.
func (sc *SignalConn) HaveSessionID() bool {
	select {
	case <-sc.sessionIDReady:
		return true
	default:
		return false
	}
}

// SessionID returns the session ID assigned by the matchmaking server.
func (sc *SignalConn) SessionID(ctx context.Context) (string, error) {
	select {
	case <-sc.sessionIDReady:
		return sc.sessionID, nil
	case <-ctx.Done():
		return "", ctx.Err()
	}
}

// SetSessionID starts the Player 2 matchmaking process using the code from Player 1.
func (sc *SignalConn) SetSessionID(ctx context.Context, sessionID string) error {
	select {
	case <-sc.sessionIDReady:
	default:
		sc.sessionID = sessionID
		close(sc.sessionIDReady)
	}

	if sc.fake != nil {
		sc.onMessage([]byte{byte(msgHandshake)})
		sc.fake.onMessage([]byte{byte(msgHandshake)})

		return nil
	}

	return sc.ws.Write(ctx, websocket.MessageText, append([]byte{byte(msgSessionID)}, sessionID...))
}

func (sc *SignalConn) sendRelayedMessage(message *relayedMessage) error {
	b, err := json.Marshal(message)
	if err != nil {
		return xerrors.Errorf("matchnet: encoding relayed message: %w", err)
	}

	b = append([]byte{byte(msgRelayed)}, b...)

	if sc.fake != nil {
		sc.fake.onMessage(b)

		return nil
	}

	return sc.ws.Write(context.TODO(), websocket.MessageText, b)
}
