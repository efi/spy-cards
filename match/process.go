package match

import (
	"log"
	"math/bits"
	"sort"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/match/matchnet"
	"git.lubar.me/ben/spy-cards/rng"
	"golang.org/x/xerrors"
)

func (m *Match) IndexSet() {
	rules, _ := m.Set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		m.Rules = card.DefaultGameRules
	} else {
		m.Rules = *rules
	}

	t, _ := m.Set.Mode.Get(card.FieldTimer).(*card.Timer)
	if t != nil {
		m.Timer = *t
	} else {
		m.Timer = card.Timer{}
	}

	m.Banned = make(map[card.ID]bool)
	m.Unpickable = make(map[card.ID]bool)
	m.Unfiltered = make(map[card.ID]bool)
	m.HasReplaceSummon = make(map[card.ID]bool)
	m.OrderForSummon = make(map[card.ID]int)

	for _, f := range m.Set.Mode.GetAll(card.FieldUnfilterCard) {
		uc := f.(*card.UnfilterCard)

		m.Unfiltered[uc.ID] = true
	}

	for _, f := range m.Set.Mode.GetAll(card.FieldBannedCards) {
		bc := f.(*card.BannedCards)

		banMap := m.Banned
		if bc.Flags&card.BannedCardRandomSummonable != 0 {
			banMap = m.Unpickable
		}

		if len(bc.Cards) == 0 {
			for i := card.ID(0); i < 128; i++ {
				banMap[i] = true
			}
		} else {
			for _, id := range bc.Cards {
				banMap[id] = true
			}
		}
	}

	for _, bc := range m.Set.Spoiler {
		banMap := m.Banned
		if bc.Flags&card.BannedCardRandomSummonable != 0 {
			banMap = m.Unpickable
		}

		for _, id := range bc.Cards {
			banMap[id] = true
		}
	}

	carminaReplaced := false

	for _, c := range m.Set.Cards {
		if c.ID == card.Carmina {
			carminaReplaced = true
		}

		for _, e := range c.Effects {
			if e.Type == card.EffectSummon && e.Flags&card.FlagSummonReplace != 0 {
				m.HasReplaceSummon[c.ID] = true
			}
		}
	}

	if !carminaReplaced {
		m.HasReplaceSummon[card.Carmina] = true
	}

	for i := card.ID(0); i < 128; i++ {
		if bi := i.BasicIndex(); bi != -1 {
			switch i.Rank() {
			case card.Boss:
				m.OrderForSummon[i] = bi
			case card.MiniBoss:
				m.OrderForSummon[i] = bi + 32
			case card.Attacker, card.Effect:
				m.OrderForSummon[i] = bi + 64
			}
		}
	}

	for i, c := range m.Set.Cards {
		m.OrderForSummon[c.ID] = i + 128
	}
}

func (m *Match) InitState(rng *rng.RNG, network Network) {
	m.Start = time.Now()

	for i := range m.State.Sides {
		side := &m.State.Sides[i]
		side.TP = Number{Amount: int64(m.Rules.MinTP)}
		side.HP = Number{Amount: int64(m.Rules.MaxHP)}

		for _, f := range m.Set.Mode.GetAll(card.FieldSummonCard) {
			sc := f.(*card.SummonCard)

			if sc.Flags&card.SummonCardBothPlayers == 0 && i != 0 {
				continue
			}

			c := m.Set.Card(sc.ID)

			side.Setup = append(side.Setup, &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  c,
					Back: c.Rank.Back(),
				},
				Desc:    c.Description(m.Set),
				Effects: append([]*card.EffectDef(nil), c.Effects...),
				Mode:    ModeSetupOriginal,
			})
		}
	}

	for _, f := range m.Set.Mode.GetAll(card.FieldTurn0Effect) {
		t0e := f.(*card.Turn0Effect)

		m.State.Queue = append(m.State.Queue, CardEffect{
			Card: &ActiveCard{
				Mode: ModeSetup,
				Card: &Card{
					Set:  m.Set,
					Def:  &card.Def{Rank: card.Token},
					Back: card.Token,
				},
			},
			Effect: t0e.Effect,
			Player: 0,
		})
	}

	m.Log.Push("Round 0")

	for len(m.ProcessQueuedEffect(rng, network)) != 0 {
	}
}

func (m *Match) ShuffleAndDraw(scg *matchnet.SecureCardGame, haveRemoteSeed bool) {
	m.State.TurnData = nil

	for player := range m.State.Sides {
		side := &m.State.Sides[player]

		for id := range side.ModTP {
			delete(side.ModTP, id)
		}

		side.TP = Number{Amount: int64((m.State.Round-1)*m.Rules.TPPerTurn + m.Rules.MinTP)}
		if side.TP.Amount > int64(m.Rules.MaxTP) {
			side.TP.Amount = int64(m.Rules.MaxTP)
		}

		for _, setup := range side.Setup {
			for _, e := range setup.Effects {
				switch e.Type {
				case card.EffectTP:
					num := Number{Amount: e.Amount}

					if e.Flags&card.FlagTPInfinity != 0 {
						num.Amount, num.AmountInf = 0, num.Amount
					}

					side.TP.Add(num)
				case card.EffectModifyCardCost:
					if side.ModTP == nil {
						side.ModTP = make(map[card.ID]int64)
					}

					for _, id := range m.FilterCards(e.Filter, true, false) {
						side.ModTP[id] += e.Amount
					}
				}
			}
		}

		if side.TP.AmountInf < 0 || (side.TP.AmountInf == 0 && side.TP.Amount < 0) {
			side.TP = Number{NaN: true}
		}

		if haveRemoteSeed || int(m.Perspective-1) == player {
			PrivateShuffle(scg, side.Deck, int(2-m.Perspective) == player)
		} else {
			PublicShuffle(scg, side.Deck)
		}

		for i := 0; (i < int(m.Rules.DrawPerTurn) || len(side.Hand) < int(m.Rules.HandMinSize)) && len(side.Hand) < int(m.Rules.HandMaxSize) && len(side.Deck) != 0; i++ {
			c := side.Deck[len(side.Deck)-1]
			side.Deck = side.Deck[:len(side.Deck)-1]
			side.Hand = append(side.Hand, HandCard{Card: c})
		}

		for i := range side.Hand {
			side.Hand[i].InHandTurns++
		}

		side.Deck = append(side.Deck, side.Discard...)
		side.Discard = nil

		for i := 0; i < len(side.Setup); i++ {
			if side.Setup[i].Mode != ModeSetupOriginal {
				continue
			}

			for j, e := range side.Setup[i].Effects {
				if e.Priority == 0 && e.Type == card.EffectSummon && e.Filter.IsSingleCard() && (e.Filter[0].CardID == side.Setup[i].Card.Def.ID || len(side.Setup[i].Effects) == 1) && e.Amount == 1 && e.Flags&(card.FlagSummonReplace|card.FlagSummonOpponent) == card.FlagSummonReplace {
					if e.Filter[0].CardID != side.Setup[i].Card.Def.ID {
						side.Setup[i].Card.Def = side.Setup[i].Card.Set.Card(e.Filter[0].CardID)
						side.Setup[i].Desc = side.Setup[i].Card.Def.Description(side.Setup[i].Card.Set)
					}

					side.Setup[i].Effects = append(side.Setup[i].Effects[:j], side.Setup[i].Effects[j+1:]...)
					side.Setup[i].Effects = append(side.Setup[i].Effects, side.Setup[i].Card.Def.Effects...)

					if e.Flags&card.FlagSummonInvisible == 0 {
						side.Setup[i].Mode = ModeSummoned
					} else {
						side.Setup[i].Mode = ModeInvisibleSummoned
					}

					break
				}
			}
		}
	}

	m.State.MaxPriority = 0
}

func (m *Match) ApplyInHand() {
	for player := range m.State.Sides {
		side := &m.State.Sides[player]

		for _, inHand := range side.Hand {
			if inHand.Def == nil {
				continue
			}

			for _, e := range inHand.Def.Effects {
				if e.Type != card.CondInHand {
					continue
				}

				if inHand.InHandTurns < e.Amount {
					continue
				}

				if e.Amount2 != 0 && inHand.InHandTurns > e.Amount2 {
					continue
				}

				switch e.Result.Type {
				case card.EffectTP:
					num := Number{Amount: e.Amount}

					if e.Flags&card.FlagTPInfinity != 0 {
						num.Amount, num.AmountInf = 0, num.Amount
					}

					side.TP.Add(num)
				case card.EffectModifyCardCost:
					if side.ModTP == nil {
						side.ModTP = make(map[card.ID]int64)
					}

					for _, id := range m.FilterCards(e.Filter, true, false) {
						side.ModTP[id] += e.Amount
					}
				}
			}
		}
	}
}

func (m *Match) BeginTurn(turnData *card.TurnData) {
	m.Log.Pushf("Round %d", m.State.Round)

	for player := range m.State.Sides {
		side := &m.State.Sides[player]

		side.Field = side.Setup
		side.Setup = nil
		side.ATK = Number{}
		side.DEF = Number{}
		side.RawATK = Number{}
		side.RawDEF = Number{}
		side.FinalATK = Number{}
		side.FinalDEF = Number{}
		side.HealP = Number{}
		side.HealN = Number{}
		side.HealMulP = 1
		side.HealMulN = 1
		side.Limit = make(map[*card.EffectDef]int64)

		for i := len(side.Hand) - 1; i >= 0; i-- {
			if turnData.Ready[player]&(1<<i) != 0 {
				if side.Hand[i].Def != nil {
					// un-apply any In Hand effects that are no longer valid
					var ac *ActiveCard

					for _, e := range side.Hand[i].Def.Effects {
						if e.Type != card.CondInHand {
							continue
						}

						if side.Hand[i].InHandTurns < e.Amount {
							continue
						}

						if e.Amount2 != 0 && side.Hand[i].InHandTurns > e.Amount2 {
							continue
						}

						switch e.Result.Type {
						case card.EffectTP:
							if e.Flags&card.FlagCondInHandOnPlay != 0 {
								continue
							}

							num := Number{Amount: -e.Amount}

							if e.Flags&card.FlagTPInfinity != 0 {
								num.Amount, num.AmountInf = 0, num.Amount
							}

							side.TP.Add(num)
						case card.EffectModifyCardCost:
							if e.Flags&card.FlagCondInHandOnPlay != 0 {
								continue
							}

							if side.ModTP == nil {
								side.ModTP = make(map[card.ID]int64)
							}

							for _, id := range m.FilterCards(e.Filter, true, false) {
								side.ModTP[id] -= e.Amount
							}
						default:
							if e.Flags&card.FlagCondInHandOnPlay == 0 {
								continue
							}

							if ac == nil {
								ac = &ActiveCard{
									Card: &Card{
										Def:  side.Hand[i].Def,
										Back: side.Hand[i].Back,
										Set:  side.Hand[i].Set,
									}, Mode: ModeInHand,
									Effects: []*card.EffectDef{
										e.Result,
									},
								}

								side.Field = append(side.Field, ac)
							} else {
								ac.Effects = append(ac.Effects, e.Result)
							}
						}
					}
				}

				if int(m.UnknownDeck)-1 == player {
					side.Hand[i].Def = nil
				}

				if !side.Hand[i].Temporary {
					side.Discard = append(side.Discard, side.Hand[i].Card)
				}

				side.Hand = append(side.Hand[:i], side.Hand[i+1:]...)
			}
		}

		for i, j := 0, len(side.Discard)-1; i < j; i, j = i+1, j-1 {
			side.Discard[i], side.Discard[j] = side.Discard[j], side.Discard[i]
		}

		for _, inHand := range side.Hand {
			if inHand.Def == nil {
				continue
			}

			var ac *ActiveCard

			for _, e := range inHand.Def.Effects {
				if e.Type != card.CondInHand {
					continue
				}

				if inHand.InHandTurns < e.Amount {
					continue
				}

				if e.Amount2 != 0 && inHand.InHandTurns > e.Amount2 {
					continue
				}

				if e.Result.Type == card.EffectTP || e.Result.Type == card.EffectModifyCardCost {
					// already applied
					continue
				}

				if ac == nil {
					ac = &ActiveCard{
						Card: inHand.Card,
						Mode: ModeInHand,
					}

					side.Field = append(side.Field, ac)
				}

				ac.Effects = append(ac.Effects, e.Result)
			}
		}

		// TODO: verify TP cost

		for _, id := range turnData.Played[player] {
			c := m.Set.Card(id)

			side.Field = append(side.Field, &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  c,
					Back: c.Rank.Back(),
				},
				Desc:    c.Description(m.Set),
				Effects: append([]*card.EffectDef(nil), c.Effects...),
				Mode:    ModeDefault,
			})
		}

		m.Log.Pushf("Player %d plays cards:", player+1)

		for _, c := range side.Field {
			m.Log.Logf("%v (%v)", c.Card.Def.DisplayName(), c.Mode)

			for _, e := range c.Effects {
				m.State.Queue = append(m.State.Queue, CardEffect{
					Card:   c,
					Effect: e,
					Player: uint8(player),
				})
			}
		}

		m.Log.Pop()
	}

	sort.SliceStable(m.State.Queue, func(i, j int) bool {
		if m.State.Queue[i].Effect.Priority == m.State.Queue[j].Effect.Priority {
			return m.State.Queue[i].Player < m.State.Queue[j].Player
		}

		return m.State.Queue[i].Effect.Priority < m.State.Queue[j].Effect.Priority
	})

	m.State.OnNumb = nil
	m.State.MaxPriority = 0
	m.State.RoundWinner = 0
}

func (m *Match) FilterCards(f card.Filter, allowBanned, allowUnfiltered bool) []card.ID {
	if f.IsSingleCard() {
		return []card.ID{f[0].CardID}
	}

	possible := make(map[*card.Def]bool)

	for i := card.ID(0); i < 128; i++ {
		if m.Banned[i] && !allowBanned {
			continue
		}

		if m.Unfiltered[i] && !allowUnfiltered {
			continue
		}

		if c := m.Set.Card(i); c != nil {
			possible[c] = true
		}
	}

	for _, c := range m.Set.Cards {
		if m.Banned[c.ID] && !allowBanned {
			continue
		}

		if m.Unfiltered[c.ID] && !allowUnfiltered {
			continue
		}

		possible[c] = true
	}

	ranks := make(map[card.Rank]bool)

	for _, fc := range f {
		switch fc.Type {
		case card.FilterRank:
			ranks[fc.Rank] = true
		case card.FilterTribe:
			for c, ok := range possible {
				if !ok {
					continue
				}

				found := false

				for _, t := range c.Tribes {
					if t.Tribe == fc.Tribe && (fc.Tribe != card.TribeCustom || t.CustomName == fc.CustomTribe) {
						found = true

						break
					}
				}

				if !found {
					possible[c] = false
				}
			}
		case card.FilterNotTribe:
			for c, ok := range possible {
				if !ok {
					continue
				}

				for _, t := range c.Tribes {
					if t.Tribe == fc.Tribe && (fc.Tribe != card.TribeCustom || t.CustomName == fc.CustomTribe) {
						possible[c] = false

						break
					}
				}
			}
		}
	}

	if len(ranks) != 0 {
		for c := range possible {
			if !ranks[c.Rank] {
				possible[c] = false
			}
		}
	}

	ids := make([]card.ID, 0, len(possible))

	for c, ok := range possible {
		if ok {
			ids = append(ids, c.ID)
		}
	}

	sort.Slice(ids, func(i, j int) bool {
		return ids[i] < ids[j]
	})

	return ids
}

func cardInSet(set []card.ID, id card.ID) bool {
	x := sort.Search(len(set), func(i int) bool {
		return set[i] >= id
	})

	return x < len(set) && set[x] == id
}

func (m *Match) ProcessQueuedEffect(r *rng.RNG, network Network) []CardEffect {
	for _, q := range []*[]CardEffect{&m.State.PreQueue, &m.State.Queue} {
		for len(*q) != 0 && (*q)[0].Effect.Type != card.EffectNumb && (*q)[0].Card.Mode == ModeNumb {
			ce := (*q)[0]
			*q = (*q)[1:]

			if ce.Effect.Type != card.CondOnNumb {
				m.Log.Logf("Skipping player %d card %q effect %q: card is numb", ce.Player+1, ce.Card.Card.Def.DisplayName(), &card.RichDescription{
					Content: ce.Effect.Description(ce.Card.Card.Def, m.Set),
				})
			}
		}

		if len(*q) != 0 {
			break
		}
	}

	if len(m.State.PreQueue) == 0 && len(m.State.Queue) == 0 {
		if m.State.MaxPriority < 224 {
			m.computeRoundWinner()
		}

		m.State.MaxPriority = 255

		m.Log.Pop()

		return nil
	}

	var ce CardEffect

	if len(m.State.PreQueue) != 0 {
		ce = m.State.PreQueue[0]
		m.State.PreQueue = m.State.PreQueue[1:]
	} else {
		ce = m.State.Queue[0]
		m.State.Queue = m.State.Queue[1:]
	}

	ret := []CardEffect{ce}

	if ce.Effect.Priority > m.State.MaxPriority {
		if m.State.MaxPriority < 224 && ce.Effect.Priority >= 224 {
			m.computeRoundWinner()
		}

		m.State.MaxPriority = ce.Effect.Priority
	}

	return m.applyEffect(ret, ce, r, network)
}

func (m *Match) computeRoundWinner() {
	p1ATK := m.State.Sides[0].ATK
	p2ATK := m.State.Sides[1].ATK
	p1DEF := m.State.Sides[0].DEF
	p2DEF := m.State.Sides[1].DEF

	p1ModATK := p1ATK
	p2ModATK := p2ATK
	p1ModDEF := p1DEF
	p2ModDEF := p2DEF

	p1ModDEF.Amount = -p1ModDEF.Amount
	p1ModDEF.AmountInf = -p1ModDEF.AmountInf
	p2ModDEF.Amount = -p2ModDEF.Amount
	p2ModDEF.AmountInf = -p2ModDEF.AmountInf

	if p1ModDEF.AmountInf > 0 || (p1ModDEF.AmountInf == 0 && p1ModDEF.Amount > 0) {
		p1ModDEF.Amount = 0
		p1ModDEF.AmountInf = 0
	}

	if p2ModDEF.AmountInf > 0 || (p2ModDEF.AmountInf == 0 && p2ModDEF.Amount > 0) {
		p2ModDEF.Amount = 0
		p2ModDEF.AmountInf = 0
	}

	p1ModATK.Add(p2ModDEF)
	p2ModATK.Add(p1ModDEF)

	if p1ModATK.AmountInf < 0 || (p1ModATK.AmountInf == 0 && p1ModATK.Amount < 0) {
		p1ModATK.Amount = 0
		p1ModATK.AmountInf = 0
	}

	if p2ModATK.AmountInf < 0 || (p2ModATK.AmountInf == 0 && p2ModATK.Amount < 0) {
		p2ModATK.Amount = 0
		p2ModATK.AmountInf = 0
	}

	m.State.Sides[0].ATK = p1ModATK
	m.State.Sides[1].ATK = p2ModATK

	switch {
	case p1ATK.NaN:
		m.Log.Log("Player 1 ATK is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p2ATK.NaN:
		m.Log.Log("Player 2 ATK is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p1DEF.NaN:
		m.Log.Log("Player 1 DEF is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p2DEF.NaN:
		m.Log.Log("Player 2 DEF is invalid. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p1ModATK.AmountInf > 0 && p2ModATK.AmountInf > 0:
		m.Log.Log("Both players have infinite ATK. Round cannot have a winner.")
		m.State.RoundWinner = 0
	case p1ModATK.AmountInf > 0:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 1 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 1
	case p2ModATK.AmountInf > 0:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 2 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 2
	case p1ModATK.Amount > p2ModATK.Amount:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 1 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 1
	case p1ModATK.Amount < p2ModATK.Amount:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Player 2 wins!", p1ModATK, p2ModATK)
		m.State.RoundWinner = 2
	default:
		m.Log.Logf("Player 1 has %v ATK. Player 2 has %v ATK. Round is a tie.", p1ModATK, p2ModATK)
		m.State.RoundWinner = 0
	}

	if m.State.RoundWinner != 0 {
		m.State.Sides[2-m.State.RoundWinner].HP.Amount--
	}

	m.Log.Logf("Player 1 HP is %v. Player 2 HP is %v.", m.State.Sides[0].HP, m.State.Sides[1].HP)
}

func (m *Match) queueEffect(ce CardEffect, isSummon bool) {
	if !isSummon && ce.Effect.Priority <= m.State.MaxPriority {
		m.State.PreQueue = append(m.State.PreQueue, ce)
	} else {
		i := sort.Search(len(m.State.Queue), func(i int) bool {
			if ce.Effect.Priority > m.State.MaxPriority && m.State.Queue[i].Effect.Priority == ce.Effect.Priority {
				return m.State.Queue[i].Player > ce.Player
			}

			return m.State.Queue[i].Effect.Priority > ce.Effect.Priority
		})

		m.State.Queue = append(m.State.Queue[:i], append([]CardEffect{ce}, m.State.Queue[i:]...)...)
	}
}

func (m *Match) applyEffect(ret []CardEffect, ce CardEffect, r *rng.RNG, network Network) []CardEffect {
	if ce.Effect.Type != card.FlavorText {
		m.Log.Logf("Applying effect %v for player %d card %q: %v",
			ce.Effect.Type,
			ce.Player+1,
			ce.Card.Card.Def.DisplayName(),
			&card.RichDescription{
				Content: ce.Effect.Description(ce.Card.Card.Def, m.Set),
			},
		)
	}

	switch ce.Effect.Type {
	case card.FlavorText:
		// no effect

	case card.EffectStat:
		num := Number{Amount: ce.Effect.Amount}
		if ce.Effect.Flags&card.FlagStatInfinity != 0 {
			num.Amount, num.AmountInf = 0, num.Amount
		}

		if ce.Effect.Flags&card.FlagStatDEF == 0 {
			before := ce.Card.ATK
			ce.Card.ATK.Add(num)
			m.Log.Logf("Card ATK changed from %v to %v", before, ce.Card.ATK)

			before = m.State.Sides[ce.Player].ATK
			m.State.Sides[ce.Player].ATK.Add(num)

			if m.State.MaxPriority < 224 {
				m.State.Sides[ce.Player].FinalATK = m.State.Sides[ce.Player].ATK
			}

			m.Log.Logf("Player ATK changed from %v to %v", before, m.State.Sides[ce.Player].ATK)
		} else {
			before := ce.Card.DEF
			ce.Card.DEF.Add(num)
			m.Log.Logf("Card DEF changed from %v to %v", before, ce.Card.DEF)

			before = m.State.Sides[ce.Player].DEF
			m.State.Sides[ce.Player].DEF.Add(num)
			if m.State.MaxPriority < 224 {
				m.State.Sides[ce.Player].FinalDEF = m.State.Sides[ce.Player].DEF
			}
			m.Log.Logf("Player DEF changed from %v to %v", before, m.State.Sides[ce.Player].DEF)
		}

	case card.EffectEmpower:
		matches := m.FilterCards(ce.Effect.Filter, true, false)
		num := Number{Amount: ce.Effect.Amount}

		player := ce.Player
		if ce.Effect.Flags&card.FlagStatOpponent != 0 {
			player = 1 - player
		}

		if ce.Effect.Flags&card.FlagStatInfinity != 0 {
			num.Amount, num.AmountInf = 0, num.Amount
		}

		for _, c := range m.State.Sides[player].Field {
			if c.Mode.Ignore() {
				continue
			}

			if cardInSet(matches, c.Card.Def.ID) {
				if ce.Effect.Flags&card.FlagStatDEF == 0 {
					before := c.ATK
					c.ATK.Add(num)
					m.Log.Logf("Matching card %q ATK changed from %v to %v.", c.Card.Def.DisplayName(), before, c.ATK)

					before = m.State.Sides[player].ATK
					m.State.Sides[player].ATK.Add(num)

					if m.State.MaxPriority < 224 {
						m.State.Sides[player].FinalATK = m.State.Sides[player].ATK
					}

					m.Log.Logf("Player %d ATK changed from %v to %v.", player+1, before, m.State.Sides[player].ATK)
				} else {
					before := c.DEF
					c.DEF.Add(num)
					m.Log.Logf("Matching card %q DEF changed from %v to %v.", c.Card.Def.DisplayName(), before, c.DEF)

					before = m.State.Sides[player].DEF
					m.State.Sides[player].DEF.Add(num)
					if m.State.MaxPriority < 224 {
						m.State.Sides[player].FinalDEF = m.State.Sides[player].DEF
					}
					m.Log.Logf("Player %d DEF changed from %v to %v.", player+1, before, m.State.Sides[player].DEF)
				}
			}
		}

	case card.EffectSummon:
		allowBanned := false
		if m.Init.Version[0] == 0 && m.Init.Version[1] == 2 && m.Init.Version[2] < 18 {
			allowBanned = true
		}

		choices := m.FilterCards(ce.Effect.Filter, allowBanned, true)
		if len(choices) == 0 {
			m.Log.Log("no cards available to summon")

			break
		}

		selected := choices[0]

		if !ce.Effect.Filter.IsSingleCard() {
			anyTribe := false
			for _, fc := range ce.Effect.Filter {
				anyTribe = fc.Type == card.FilterTribe || fc.Type == card.FilterNotTribe
				if anyTribe {
					break
				}
			}

			sort.Slice(choices, func(i, j int) bool {
				return m.OrderForSummon[choices[i]] < m.OrderForSummon[choices[j]]
			})

			if ce.Effect.Flags&card.FlagSummonReplace != 0 && !anyTribe {
				for i := 0; i < len(choices); i++ {
					if m.HasReplaceSummon[choices[i]] {
						choices = append(choices[:i], choices[i+1:]...)
						i--
					}
				}
			}
		}

		for count := int64(0); count < ce.Effect.Amount; count++ {
			if !ce.Effect.Filter.IsSingleCard() {
				i := r.RangeInt(0, len(choices))
				selected = choices[i]

				m.Log.Logf("selecting card %d of %d (%q)", i+1, len(choices), m.Set.Card(selected).DisplayName())
			}

			after := ce.Card
			player := ce.Player

			if ce.Effect.Flags&card.FlagSummonOpponent != 0 {
				after = nil
				player = 1 - player
			}

			selectedCard := m.Set.Card(selected)

			toSummon := &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  selectedCard,
					Back: selectedCard.Rank.Back(),
				},
				Effects: append([]*card.EffectDef(nil), selectedCard.Effects...),
				Desc:    selectedCard.Description(m.Set),
				Mode:    ModeSummoned,
			}

			ret = append(ret, CardEffect{
				Card:   toSummon,
				Player: player,
			})

			if ce.Effect.Flags&card.FlagSummonInvisible != 0 {
				toSummon.Mode = ModeInvisibleSummoned
			}

			switch replace := ce.Effect.Flags&card.FlagSummonReplace != 0; {
			case replace && after == nil:
				for i, c := range m.State.Sides[ce.Player].Field {
					if c == ce.Card {
						m.State.Sides[ce.Player].Field = append(m.State.Sides[ce.Player].Field[:i], m.State.Sides[ce.Player].Field[i+1:]...)

						break
					}
				}

				m.State.Sides[player].Field = append(m.State.Sides[player].Field, toSummon)
			case after != nil:
				found := false

				for i, c := range m.State.Sides[player].Field {
					if c == after {
						found = true

						if replace {
							m.State.Sides[player].Field[i] = toSummon
						} else {
							m.State.Sides[player].Field = append(m.State.Sides[player].Field[:i+1], append([]*ActiveCard{toSummon}, m.State.Sides[player].Field[i+1:]...)...)
						}

						break
					}
				}

				if !found {
					m.State.Sides[player].Field = append(m.State.Sides[player].Field, toSummon)
				}
			default:
				m.State.Sides[player].Field = append(m.State.Sides[player].Field, toSummon)
			}

			for _, e := range toSummon.Effects {
				m.queueEffect(CardEffect{
					Card:   toSummon,
					Effect: e,
					Player: player,
				}, true)
			}
		}

	case card.EffectHeal:
		player := ce.Player
		if ce.Effect.Flags&card.FlagHealOpponent != 0 {
			player = 1 - player
		}

		num := Number{Amount: ce.Effect.Amount}

		if ce.Effect.Flags&card.FlagHealRaw == 0 {
			if num.Amount < 0 {
				num.Amount *= m.State.Sides[player].HealMulN
			} else {
				num.Amount *= m.State.Sides[player].HealMulP
			}
		}

		if ce.Effect.Flags&card.FlagHealInfinity != 0 {
			if num.Amount > 0 {
				num.Amount, num.AmountInf = 0, 1
			} else if num.Amount < 0 {
				num.Amount, num.AmountInf = 0, -1
			}
		}

		if ce.Effect.Flags&card.FlagHealRaw == 0 {
			m.Log.Logf("Amount after multipliers: %v", num)

			if ce.Effect.Amount < 0 {
				m.State.Sides[player].HealN.Add(num)
			} else {
				m.State.Sides[player].HealP.Add(num)
			}
		} else {
			m.Log.Log("Ignoring multipliers.")
		}

		m.State.Sides[player].HP.Add(num)
		m.Log.Logf("Player %d HP is now %v.", player+1, m.State.Sides[player].HP)

		if m.State.Sides[player].HP.AmountInf > 0 || m.State.Sides[player].HP.Amount > int64(m.Rules.MaxHP) {
			m.State.Sides[player].HP = Number{Amount: int64(m.Rules.MaxHP)}
			m.Log.Log("Limiting to maximum HP.")
		}

	case card.EffectTP:
		// no effect during round

	case card.EffectNumb:
		player := 1 - ce.Player
		if ce.Effect.Flags&card.FlagNumbSelf != 0 {
			player = 1 - player
		}

		matches := m.FilterCards(ce.Effect.Filter, true, false)
		candidates := make([]*ActiveCard, 0, len(m.State.Sides[player].Field))

		for _, c := range m.State.Sides[player].Field {
			if c.Mode.Ignore() {
				continue
			}

			if cardInSet(matches, c.Card.Def.ID) {
				if c.UnNumb > 0 {
					c.UnNumb--
				} else if !c.ATK.NaN && !c.DEF.NaN {
					candidates = append(candidates, c)
				}
			}
		}

		sort.SliceStable(candidates, func(i, j int) bool {
			a1 := candidates[i].ATK
			d1 := candidates[i].DEF
			a2 := candidates[j].ATK
			d2 := candidates[j].DEF

			h1, h2 := a1, a2
			l1, l2 := d1, d2
			switch {
			case ce.Effect.Flags&card.FlagNumbIgnoreATK != 0:
				h1, h2 = d1, d2
			case ce.Effect.Flags&card.FlagNumbIgnoreDEF != 0:
				l1, l2 = a1, a2
			default:
				if h1.Less(l1) {
					h1, l1 = l1, h1
				}

				if h2.Less(l2) {
					h2, l2 = l2, h2
				}
			}

			if m.Init.Version[0] == 0 && m.Init.Version[1] == 2 && m.Init.Version[2] < 77 {
				d1, d2 = Number{}, Number{}
				l1, l2 = Number{}, Number{}
			}

			if ce.Effect.Flags&card.FlagNumbAvoidZero != 0 {
				if h1.AmountInf == 0 && h1.Amount == 0 {
					return false
				}

				if h2.AmountInf == 0 && h2.Amount == 0 {
					return true
				}
			}

			if ce.Effect.Flags&card.FlagNumbHighest != 0 {
				a1, a2 = a2, a1
				d1, d2 = d2, d1
				h1, h2 = h2, h1
				l1, l2 = l2, l1
			}

			if h1.Less(h2) {
				return true
			}

			if h2.Less(h1) {
				return false
			}

			if l1.Less(l2) {
				return true
			}

			if l2.Less(l1) {
				return false
			}

			if h1 != a1 && h2 == a2 {
				return true
			}

			if h1 == a1 && h2 == a2 {
				return d1.Less(d2)
			}

			return false
		})

		if ce.Effect.Flags&card.FlagNumbInfinity == 0 && int64(len(candidates)) > ce.Effect.Amount {
			candidates = candidates[:ce.Effect.Amount]
		}

		for _, c := range candidates {
			m.Log.Logf("Numbing card %q (ATK %v DEF %v)", c.Card.Def.DisplayName(), c.ATK, c.DEF)

			c.Mode = ModeNumb

			ret = append(ret, CardEffect{
				Card:   c,
				Player: player,
			})

			for _, q := range [][]CardEffect{m.State.OnNumb, m.State.PreQueue, m.State.Queue} {
				for _, o := range q {
					if o.Card == c && o.Effect.Type == card.CondOnNumb {
						m.Log.Logf("Applying On Numb effect for player %d card %q.", player+1, c.Card.Def.DisplayName())

						ret = m.applyEffect(ret, CardEffect{
							Card:   c,
							Effect: o.Effect.Result,
							Player: o.Player,
						}, r, network)
					}
				}
			}
		}

		atkBefore := m.State.Sides[player].ATK
		defBefore := m.State.Sides[player].DEF

		m.State.Sides[player].ATK = m.State.Sides[player].RawATK
		m.State.Sides[player].DEF = m.State.Sides[player].RawDEF

		for _, c := range m.State.Sides[player].Field {
			if c.Mode == ModeNumb {
				continue
			}

			m.State.Sides[player].ATK.Add(c.ATK)
			m.State.Sides[player].DEF.Add(c.DEF)
		}

		if m.State.MaxPriority < 224 {
			m.State.Sides[player].FinalATK = m.State.Sides[player].ATK
			m.State.Sides[player].FinalDEF = m.State.Sides[player].DEF
		}

		m.Log.Logf("Player %d ATK recalculated from %v to %v.", player+1, atkBefore, m.State.Sides[player].ATK)
		m.Log.Logf("Player %d DEF recalculated from %v to %v.", player+1, defBefore, m.State.Sides[player].DEF)

	case card.EffectRawStat:
		num := Number{Amount: ce.Effect.Amount}
		if ce.Effect.Flags&card.FlagStatInfinity != 0 {
			num.Amount, num.AmountInf = 0, num.Amount
		}

		player := ce.Player
		if ce.Effect.Flags&card.FlagStatOpponent != 0 {
			player = 1 - player
		}

		if ce.Effect.Flags&card.FlagStatDEF == 0 {
			before := m.State.Sides[player].ATK
			m.State.Sides[player].RawATK.Add(num)
			m.State.Sides[player].ATK.Add(num)

			if m.State.MaxPriority < 224 {
				m.State.Sides[player].FinalATK = m.State.Sides[player].ATK
			}

			m.Log.Logf("Player %d ATK changed from %v to %v.", player+1, before, m.State.Sides[ce.Player].ATK)
		} else {
			before := m.State.Sides[player].DEF
			m.State.Sides[player].RawDEF.Add(num)
			m.State.Sides[player].DEF.Add(num)
			if m.State.MaxPriority < 224 {
				m.State.Sides[player].FinalDEF = m.State.Sides[player].DEF
			}
			m.Log.Logf("Player %d DEF changed from %v to %v.", player+1, before, m.State.Sides[player].DEF)
		}

	case card.EffectMultiplyHealing:
		player := ce.Player
		if ce.Effect.Flags&card.FlagMultiplyHealingOpponent != 0 {
			player = 1 - player
		}

		pos := ce.Effect.Flags&card.FlagMultiplyHealingTypeMask != card.FlagMultiplyHealingTypeNegative
		neg := ce.Effect.Flags&card.FlagMultiplyHealingTypeMask != card.FlagMultiplyHealingTypePositive

		if pos {
			m.State.Sides[player].HealMulP *= ce.Effect.Amount
		}

		if neg {
			m.State.Sides[player].HealMulN *= ce.Effect.Amount
		}

		if ce.Effect.Flags&card.FlagMultiplyHealingFuture == 0 {
			if pos {
				if ce.Effect.Amount < 0 {
					m.State.Sides[player].HealP.AmountInf = -m.State.Sides[player].HealP.AmountInf
				}

				add := m.State.Sides[player].HealP.Amount * (ce.Effect.Amount - 1)
				m.State.Sides[player].HealP.Amount += add
				m.State.Sides[player].HP.Amount += add
			}

			if neg {
				if ce.Effect.Amount < 0 {
					m.State.Sides[player].HealN.AmountInf = -m.State.Sides[player].HealN.AmountInf
				}

				add := m.State.Sides[player].HealN.Amount * (ce.Effect.Amount - 1)
				m.State.Sides[player].HealN.Amount += add
				m.State.Sides[player].HP.Amount += add
			}

			if m.State.Sides[player].HP.AmountInf > 0 || (m.State.Sides[player].HP.AmountInf == 0 && m.State.Sides[player].HP.Amount > int64(m.Rules.MaxHP)) {
				m.State.Sides[player].HP.Amount, m.State.Sides[player].HP.AmountInf = int64(m.Rules.MaxHP), 0
			}
		}

	case card.EffectPreventNumb:
		player := ce.Player
		if ce.Effect.Flags&card.FlagPreventNumbOpponent != 0 {
			player = 1 - player
		}

		matches := m.FilterCards(ce.Effect.Filter, true, false)

		for _, c := range m.State.Sides[player].Field {
			if c.Mode.Ignore() {
				continue
			}

			if cardInSet(matches, c.Card.Def.ID) {
				c.UnNumb++
			}
		}

	case card.EffectModifyAvailableCards:
		player := ce.Player
		if ce.Effect.Flags&card.FlagModifyCardsOpponent != 0 {
			player = 1 - player
		}

		switch ce.Effect.Flags & card.FlagModifyCardsTypeMask {
		case card.FlagModifyCardsTypeAdd:
			matches := m.FilterCards(ce.Effect.Filter, false, true)

			for i := int64(0); i < ce.Effect.Amount; i++ {
				var temp bool

				switch ce.Effect.Flags & card.FlagModifyCardsTargetMask {
				case card.FlagModifyCardsTargetAny:
					temp = true

					fallthrough
				case card.FlagModifyCardsTargetHand:
					var c *card.Def

					if ce.Effect.Filter.IsSingleCard() {
						c = m.Set.Card(ce.Effect.Filter[0].CardID)
					} else {
						j := r.RangeInt(0, len(matches))
						c = m.Set.Card(matches[j])
					}

					m.State.Sides[player].Hand = append(m.State.Sides[player].Hand, HandCard{
						Card: &Card{
							Def:  c,
							Back: c.Rank.Back(),
							Set:  m.Set,
						},
						Temporary: temp,
					})
				case card.FlagModifyCardsTargetDeck:
					var c *card.Def

					if ce.Effect.Filter.IsSingleCard() {
						c = m.Set.Card(ce.Effect.Filter[0].CardID)
					} else {
						j := r.RangeInt(0, len(matches))
						c = m.Set.Card(matches[j])
					}

					def := c

					if m.UnknownDeck-1 == player {
						def = nil
					}

					m.State.Sides[player].Deck = append(m.State.Sides[player].Deck, &Card{
						Def:  def,
						Back: c.Rank.Back(),
						Set:  m.Set,
					})
				case card.FlagModifyCardsTargetField:
					var c *card.Def

					if ce.Effect.Filter.IsSingleCard() {
						c = m.Set.Card(ce.Effect.Filter[0].CardID)
					} else {
						j := r.RangeInt(0, len(matches))
						c = m.Set.Card(matches[j])
					}

					def := c

					if m.UnknownDeck-1 == player {
						def = nil
					}

					ac := &ActiveCard{
						Card: &Card{
							Def:  c,
							Back: c.Rank.Back(),
							Set:  m.Set,
						},
						Desc:    c.Description(m.Set),
						Mode:    ModeSummoned,
						Effects: append([]*card.EffectDef(nil), c.Effects...),
					}
					m.State.Sides[player].Field = append(m.State.Sides[player].Field, ac)

					for _, e := range ac.Effects {
						m.queueEffect(CardEffect{
							Card:   ac,
							Effect: e,
							Player: player,
						}, false)
					}

					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, &Card{
						Def:  def,
						Back: c.Rank.Back(),
						Set:  m.Set,
					})
				}
			}

		case card.FlagModifyCardsTypeRemove:
			var modified []card.ModifiedCardPosition

			if m.UnknownDeck-1 == player {
				modified = network.RecvModifiedCards()
			} else {
				matches := m.FilterCards(ce.Effect.Filter, true, false)
				target := ce.Effect.Flags & card.FlagModifyCardsTargetMask

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetField {
					for i, c := range m.State.Sides[player].Discard {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(len(m.State.Sides[player].Hand) + i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetHand {
					for i, c := range m.State.Sides[player].Hand {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetDeck {
					for i, c := range m.State.Sides[player].Deck {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   false,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if 2-m.UnknownDeck == player {
					network.SendModifiedCards(modified)
				}
			}

			if m.State.TurnData != nil {
				m.State.TurnData.Modified[player] = append(m.State.TurnData.Modified[player], modified)
			}

			var removedField, removedHand, removedDeck int

			for _, c := range modified {
				switch handSize := len(m.State.Sides[player].Hand); {
				case c.InHand && c.Position >= uint64(handSize):
					i := int(c.Position) - handSize - removedField
					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard[:i], m.State.Sides[player].Discard[i+1:]...)
					removedField++
				case c.InHand:
					i := int(c.Position) - removedHand
					m.State.Sides[player].Hand = append(m.State.Sides[player].Hand[:i], m.State.Sides[player].Hand[i+1:]...)
					removedHand++
				default:
					i := int(c.Position) - removedDeck
					m.State.Sides[player].Deck = append(m.State.Sides[player].Deck[:i], m.State.Sides[player].Deck[i+1:]...)
					removedDeck++
				}
			}

		case card.FlagModifyCardsTypeMove:
			var modified []card.ModifiedCardPosition

			if m.UnknownDeck-1 == player {
				modified = network.RecvModifiedCards()
			} else {
				matches := m.FilterCards(ce.Effect.Filter, true, false)
				target := ce.Effect.Flags & card.FlagModifyCardsTargetMask

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetHand {
					for i, c := range m.State.Sides[player].Hand {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   true,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if target == card.FlagModifyCardsTargetAny || target == card.FlagModifyCardsTargetDeck {
					for i, c := range m.State.Sides[player].Deck {
						if int64(len(modified)) >= ce.Effect.Amount {
							break
						}

						if cardInSet(matches, c.Def.ID) {
							modified = append(modified, card.ModifiedCardPosition{
								InHand:   false,
								Position: uint64(i),
								CardID:   c.Def.ID,
							})
						}
					}
				}

				if 2-m.UnknownDeck == player {
					network.SendModifiedCards(modified)
				}
			}

			if m.State.TurnData != nil {
				m.State.TurnData.Modified[player] = append(m.State.TurnData.Modified[player], modified)
			}

			var movedHand, movedDeck int

			for _, c := range modified {
				if c.InHand {
					i := int(c.Position) - movedHand

					if m.UnknownDeck-1 == player {
						m.State.Sides[player].Hand[i].Def = nil
					}

					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, m.State.Sides[player].Hand[i].Card)
					m.State.Sides[player].Hand = append(m.State.Sides[player].Hand[:i], m.State.Sides[player].Hand[i+1:]...)
					movedHand++
				} else {
					i := int(c.Position) - movedDeck
					m.State.Sides[player].Discard = append(m.State.Sides[player].Discard, m.State.Sides[player].Deck[i])
					m.State.Sides[player].Deck = append(m.State.Sides[player].Deck[:i], m.State.Sides[player].Deck[i+1:]...)
					movedDeck++
				}

				def := m.Set.Card(c.CardID)
				ac := &ActiveCard{
					Card: &Card{
						Def:  def,
						Back: def.Rank.Back(),
						Set:  m.Set,
					},
					Desc:    def.Description(m.Set),
					Mode:    ModeSummoned,
					Effects: append([]*card.EffectDef(nil), def.Effects...),
				}

				m.State.Sides[player].Field = append(m.State.Sides[player].Field, ac)

				for _, e := range ac.Effects {
					m.queueEffect(CardEffect{
						Card:   ac,
						Effect: e,
						Player: player,
					}, false)
				}
			}
		}

	case card.EffectModifyCardCost:
		// no effect during round

	case card.EffectDrawCard:
		player := ce.Player
		if ce.Effect.Flags&card.FlagDrawCardOpponent != 0 {
			player = 1 - player
		}

		if ce.Effect.Amount < 0 {
			for i := int64(0); i > ce.Effect.Amount; i-- {
				if len(m.State.Sides[player].Hand) == 0 {
					break
				}

				x := r.RangeInt(0, len(m.State.Sides[player].Hand))

				if m.UnknownDeck-1 == player {
					m.State.Sides[player].Hand[x].Def = nil
				}

				m.State.Sides[player].Deck = append(m.State.Sides[player].Deck, m.State.Sides[player].Hand[x].Card)
				m.State.Sides[player].Hand = append(m.State.Sides[player].Hand[:x], m.State.Sides[player].Hand[x+1:]...)
			}
		} else {
			for i := int64(0); i < ce.Effect.Amount; i++ {
				if len(m.State.Sides[player].Deck) == 0 {
					break
				}

				if ce.Effect.Flags&card.FlagDrawCardIgnoreMax == 0 && len(m.State.Sides[player].Hand) >= int(m.Rules.HandMaxSize) {
					break
				}

				x := r.RangeInt(0, len(m.State.Sides[player].Deck))
				m.State.Sides[player].Hand = append(m.State.Sides[player].Hand, HandCard{Card: m.State.Sides[player].Deck[x]})
				m.State.Sides[player].Deck = append(m.State.Sides[player].Deck[:x], m.State.Sides[player].Deck[x+1:]...)
			}
		}

	case card.CondCard:
		player := ce.Player
		if ce.Effect.Flags&card.FlagCondCardOpponent != 0 {
			player = 1 - player
		}

		matches, count := m.FilterCards(ce.Effect.Filter, true, false), int64(0)

		for _, c := range m.State.Sides[player].Field {
			if c.Mode.Ignore() {
				continue
			}

			if cardInSet(matches, c.Card.Def.ID) {
				m.Log.Logf("Matching card: %v", c.Card.Def.DisplayName())
				count++
			}
		}

		var success int64

		switch ce.Effect.Flags & card.FlagCondCardTypeMask {
		case card.FlagCondCardTypeGreaterEqual:
			if count >= ce.Effect.Amount {
				m.Log.Logf("Matching card count %d is at least %d.", count, ce.Effect.Amount)

				success = 1
			} else {
				m.Log.Logf("Matching card count %d is not at least %d.", count, ce.Effect.Amount)
			}
		case card.FlagCondCardTypeLessThan:
			if count < ce.Effect.Amount {
				m.Log.Logf("Matching card count %d is less than %d.", count, ce.Effect.Amount)

				success = 1
			} else {
				m.Log.Logf("Matching card count %d is not less than %d.", count, ce.Effect.Amount)
			}
		case card.FlagCondCardTypeEach:
			success = count / ce.Effect.Amount
			if success < 0 {
				success = 0
			}

			m.Log.Logf("Matching card count is %d. Applying effect %d times.", count, success)
		}

		ce2 := CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Player: ce.Player,
		}

		for i := int64(0); i < success; i++ {
			ret = append(ret, ce2)
			m.queueEffect(ce2, false)
		}

	case card.CondLimit:
		m.State.Sides[ce.Player].Limit[ce.Effect]++

		n := m.State.Sides[ce.Player].Limit[ce.Effect]
		gt := ce.Effect.Flags&card.FlagCondLimitGreaterThan != 0
		success := false

		switch {
		case !gt && n <= ce.Effect.Amount:
			m.Log.Logf("Effect has occurred %d times, which is at most %d.", n, ce.Effect.Amount)

			success = true
		case gt && n <= ce.Effect.Amount:
			m.Log.Logf("Effect has occurred %d times, which is not greater than %d.", n, ce.Effect.Amount)
		case !gt && n > ce.Effect.Amount:
			m.Log.Logf("Effect has occurred %d times, which is not at most %d.", n, ce.Effect.Amount)
		case gt && n > ce.Effect.Amount:
			m.Log.Logf("Effect has occurred %d times, which is greater than %d.", n, ce.Effect.Amount)

			success = true
		}

		if success {
			ce2 := CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result,
				Player: ce.Player,
			}

			ret = append(ret, ce2)
			m.queueEffect(ce2, false)
		}

	case card.CondWinner:
		success := false

		switch ce.Effect.Flags & card.FlagCondWinnerTypeMask {
		case card.FlagCondWinnerTypeWinner:
			success = m.State.RoundWinner == ce.Player+1
		case card.FlagCondWinnerTypeLoser:
			success = m.State.RoundWinner == 2-ce.Player
		case card.FlagCondWinnerTypeTie:
			success = m.State.RoundWinner == 0
		case card.FlagCondWinnerTypeNotTie:
			success = m.State.RoundWinner != 0
		}

		if success {
			ce2 := CardEffect{
				Card:   ce.Card,
				Effect: ce.Effect.Result,
				Player: ce.Player,
			}

			ret = append(ret, ce2)
			m.queueEffect(ce2, false)
		}

	case card.CondApply:
		player := ce.Player
		if ce.Effect.Flags&card.FlagCondApplyOpponent != 0 {
			player = 1 - player
		}

		var desc *card.RichDescription

		if ce.Effect.Flags&card.FlagCondApplyOriginalText == 0 {
			desc = &card.RichDescription{
				Effect: ce.Effect.Result,

				Content: ce.Effect.Result.Description(ce.Card.Card.Def, m.Set),
				Color:   sprites.Black,
			}
		}

		target := &m.State.Sides[player].Field
		if ce.Effect.Flags&card.FlagCondApplyNextRound != 0 {
			target = &m.State.Sides[player].Setup
		}

		var ghostCard *ActiveCard

		if desc == nil {
			for _, c := range *target {
				if c.Mode == ModeSetupOriginal && c.Card.Def.ID == ce.Card.Card.Def.ID {
					ghostCard = c

					break
				}
			}

			if ghostCard == nil {
				ghostCard = &ActiveCard{
					Card: &Card{
						Set:  m.Set,
						Def:  ce.Card.Card.Def,
						Back: ce.Card.Card.Back,
					},
					Desc: ce.Card.Card.Def.Description(m.Set),
					Mode: ModeSetupOriginal,
				}

				*target = append(*target, ghostCard)
			}
		} else {
			ghostCard = &ActiveCard{
				Card: &Card{
					Set:  m.Set,
					Def:  ce.Card.Card.Def,
					Back: ce.Card.Card.Back,
				},
				Desc: desc,
				Mode: ModeSetup,
			}

			*target = append(*target, ghostCard)
		}

		ghostCard.Effects = append(ghostCard.Effects, ce.Effect.Result)

		ce2 := CardEffect{
			Card:   ghostCard,
			Effect: ce.Effect.Result,
			Player: player,
		}

		if ce.Effect.Flags&card.FlagCondApplyNextRound == 0 {
			m.queueEffect(ce2, false)
		}

		ret = append(ret, ce2)

	case card.CondCoin:
		for i := int64(0); i < ce.Effect.Amount; i++ {
			heads := r.RangeInt(0, 2) != 0

			ce2 := CardEffect{
				Card:   ce.Card,
				Player: ce.Player,
			}

			if heads {
				m.Log.Logf("Coin %d of %d lands on heads.", i+1, ce.Effect.Amount)
				ce2.Effect = ce.Effect.Result
			} else {
				m.Log.Logf("Coin %d of %d lands on tails.", i+1, ce.Effect.Amount)
				ce2.Effect = ce.Effect.Result2
			}

			ret = append(ret, ce2)

			if ce2.Effect != nil {
				m.queueEffect(ce2, false)
			}
		}

	case card.CondStat:
		player := ce.Player
		if ce.Effect.Flags&card.FlagCondStatOpponent != 0 {
			player = 1 - player
		}

		var (
			num  Number
			name string
		)

		switch ce.Effect.Flags & card.FlagCondStatTypeMask {
		case card.FlagCondStatTypeATK:
			num = m.State.Sides[player].ATK
			name = "ATK"
		case card.FlagCondStatTypeDEF:
			num = m.State.Sides[player].DEF
			name = "DEF"
		case card.FlagCondStatTypeHP:
			num = m.State.Sides[player].HP
			name = "HP"
		case card.FlagCondStatTypeTP:
			num = m.State.Sides[player].TP
			name = "TP"
		}

		var success int64

		switch less, each := ce.Effect.Flags&card.FlagCondStatLessThan != 0, ce.Effect.Flags&card.FlagCondStatMultiple != 0; {
		case num.NaN:
			m.Log.Logf("Player %d's %s stat is invalid; skipping comparison.", player+1, name)
		case !each && num.AmountInf != 0:
			switch {
			case !less && num.AmountInf > 0:
				m.Log.Logf("Player %d's %s stat is positive infinity, which is at least %d.", player+1, name, ce.Effect.Amount)

				success = 1
			case less && num.AmountInf > 0:
				m.Log.Logf("Player %d's %s stat is positive infinity, which is not less than %d.", player+1, name, ce.Effect.Amount)
			case !less:
				m.Log.Logf("Player %d's %s stat is negative infinity, which is not at least %d.", player+1, name, ce.Effect.Amount)
			default:
				m.Log.Logf("Player %d's %s stat is negative infinity, which is less than %d.", player+1, name, ce.Effect.Amount)

				success = 1
			}
		case !each:
			switch {
			case !less && num.Amount >= ce.Effect.Amount:
				m.Log.Logf("Player %d's %s stat is %d, which is at least %d.", player+1, name, num.Amount, ce.Effect.Amount)

				success = 1
			case less && num.Amount >= ce.Effect.Amount:
				m.Log.Logf("Player %d's %s stat is %d, which is not less than %d.", player+1, name, num.Amount, ce.Effect.Amount)
			case !less && num.Amount < ce.Effect.Amount:
				m.Log.Logf("Player %d's %s stat is %d, which is not at least %d.", player+1, name, num.Amount, ce.Effect.Amount)
			case less && num.Amount < ce.Effect.Amount:
				m.Log.Logf("Player %d's %s stat is %d, which is less than %d.", player+1, name, num.Amount, ce.Effect.Amount)

				success = 1
			}
		case num.AmountInf < 0:
			m.Log.Logf("Player %d's %s stat is negative infinity. Applying effect 0 times.", player+1, name)
		default:
			success = num.Amount / ce.Effect.Amount
			if success < 0 {
				success = 0
			}

			m.Log.Logf("Player %d's %s stat (ignoring infinities) is %d. Applying effect %d times.", player+1, name, num.Amount, success)
		}

		ce2 := CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Player: ce.Player,
		}

		for i := int64(0); i < success; i++ {
			ret = append(ret, ce2)
			m.queueEffect(ce2, false)
		}

	case card.CondInHand:
		// no effect during round

	case card.CondLastEffect:
		ce2 := CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Player: ce.Player,
		}

		ret = append(ret, ce2)
		ret = m.applyEffect(ret, ce2, r, network)

		for _, q := range []*[]CardEffect{&m.State.PreQueue, &m.State.Queue} {
			for i := 0; i < len(*q); i++ {
				if (*q)[i].Card == ce.Card {
					m.Log.Logf("Cancelling queued effect: %v", &card.RichDescription{
						Content: (*q)[i].Effect.Description(ce.Card.Card.Def, m.Set),
					})

					*q = append((*q)[:i], (*q)[i+1:]...)
					i--
				}
			}
		}

	case card.CondOnNumb:
		m.State.OnNumb = append(m.State.OnNumb, ce)

	case card.CondMultipleEffects:
		m.queueEffect(CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result,
			Player: ce.Player,
		}, false)

		m.queueEffect(CardEffect{
			Card:   ce.Card,
			Effect: ce.Effect.Result2,
			Player: ce.Player,
		}, false)

	default:
		desc := &card.RichDescription{Content: ce.Effect.Description(ce.Card.Card.Def, m.Set)}
		log.Panicf("TODO: process queued effect for player %d card %q: %+v %q", ce.Player+1, ce.Card.Card.Def.DisplayName(), *ce.Effect, desc)
	}

	return ret
}

func (m *Match) SyncInHand(sync func([]byte) []byte, turnData *card.TurnData) error {
	m.State.TurnData = turnData

	turnData.InHand[m.Perspective-1] = 0
	turnData.HandID[m.Perspective-1] = nil

	for i, c := range m.State.Sides[m.Perspective-1].Hand {
		for _, e := range c.Def.Effects {
			if e.Type == card.CondInHand {
				turnData.InHand[m.Perspective-1] |= 1 << i
				turnData.HandID[m.Perspective-1] = append(turnData.HandID[m.Perspective-1], c.Def.ID)

				break
			}
		}
	}

	var w format.Writer

	w.UVarInt(turnData.InHand[m.Perspective-1])

	for _, id := range turnData.HandID[m.Perspective-1] {
		w.UVarInt(uint64(id))
	}

	var r format.Reader

	r.Init(sync(w.Data()))

	turnData.InHand[2-m.Perspective] = r.UVarInt()
	turnData.HandID[2-m.Perspective] = make([]card.ID, bits.OnesCount64(turnData.InHand[2-m.Perspective]))

	for i := range turnData.HandID[2-m.Perspective] {
		turnData.HandID[2-m.Perspective][i] = card.ID(r.UVarInt())
	}

	for player := range m.State.Sides {
		if expectedCards := (64 - bits.LeadingZeros64(turnData.InHand[player])); expectedCards > len(m.State.Sides[player].Hand) {
			return xerrors.Errorf("match: InHand data desync for player %d: expected at least %d cards in hand, but only %d are present", player+1, expectedCards, len(m.State.Sides[player].Hand))
		}

		inHand := turnData.HandID[player]

		if bitsSet := bits.OnesCount64(turnData.InHand[player]); bitsSet != len(inHand) {
			return xerrors.Errorf("match: InHand data desync for player %d: %d placement bits set, but %d card IDs sent", player+1, bitsSet, len(inHand))
		}

		for i, c := range m.State.Sides[player].Hand {
			if turnData.InHand[player]&(1<<i) == 0 {
				continue
			}

			if c.Def == nil {
				c.Def = m.Set.Card(inHand[0])

				if c.Def.Rank.Back() != c.Back {
					return xerrors.Errorf("match: InHand data desync for player %d: card %d in hand is %q, but has back %v (not %v)", player+1, i+1, c.Def.DisplayName(), c.Back, c.Def.Rank.Back())
				}
			} else if c.Def.ID != inHand[0] {
				return xerrors.Errorf("match: InHand data desync for player %d: card %d in hand is %q; expected %q", player+1, i+1, c.Def.DisplayName(), m.Set.Card(inHand[0]).DisplayName())
			}

			inHand = inHand[1:]
		}
	}

	return nil
}
