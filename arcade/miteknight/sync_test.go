// +build headless

package miteknight_test

import (
	"context"
	"io/ioutil"
	"path/filepath"
	"testing"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
	"git.lubar.me/ben/spy-cards/input"
)

type recording struct {
	FileID string
	Player string
	Score  uint64
}

var recordings = []*recording{
	{"JCTBJS65GB2BRCR", "BEN", 10519},
	{"N50D5CNWY88P2F0", "MAR", 10728},
	{"RMMSN28BKJQZ4N0", "DAN", 10843},
	{"TG9WAKK5ZSPMCMR", "ALD", 12632},
}

func TestMiteKnight(t *testing.T) {
	ctx, _ := input.NewContext(context.Background(), func() ([]arcade.Button, float32, float32, bool) {
		return nil, -1, -1, false
	})

	for i := range recordings {
		r := recordings[i]

		t.Run(r.FileID, func(t *testing.T) {
			b, err := ioutil.ReadFile(filepath.Join("testdata", r.FileID))
			if err != nil {
				t.Fatal(err)
			}

			var rec arcade.Recording

			err = rec.UnmarshalBinary(b)
			if err != nil {
				t.Fatal(err)
			}

			state, err := game.PlayRecording(ctx, miteknight.New(*rec.Rules.(*arcade.MiteKnightRules)), &rec)
			if err != nil {
				t.Fatal(err)
			}

			if uint64(state.Score) != r.Score {
				t.Errorf("expected score: %v actual score: %v", r.Score, state.Score)
			}

			if state.Playback.PlayerName != r.Player {
				t.Errorf("expected player name: %v actual player name: %v", r.Player, state.Playback.PlayerName)
			}
		})
	}
}
