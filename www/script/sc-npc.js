"use strict";
var SpyCards;
(function (SpyCards) {
    var AI;
    (function (AI) {
        // Rules for NPCs:
        // - MUST NOT modify arguments or any value reachable from arguments.
        // - MUST NOT modify Spy Cards code or global data.
        // - Promises MUST take a reasonable, finite amount of time to resolve.
        // - MAY read any value reachable from arguments.
        // - SHOULD return a valid response (this will be caught in online games, but not in simulations).
        // - MAY store, access, and modify data local to the NPC.
        class NPC {
            // afterRound(SpyCards.Context): optional, called after a round's winner has been determined.
            async afterRound(ctx) { }
        }
        AI.NPC = NPC;
        // Generic NPC:
        // - picks a random player
        // - creates a random 15 card deck (no ELK)
        // - plays cards, if possible, from left to right
        class GenericNPC extends NPC {
            async pickPlayer() {
                const choices = SpyCards.TheRoom.Player.characters;
                return choices[Math.floor(Math.random() * choices.length)];
            }
            async createDeck(defs) {
                function pick(arr, not) {
                    let card;
                    do {
                        card = arr[Math.floor(Math.random() * arr.length)];
                    } while (defs.banned[card.id] || defs.unpickable[card.id] || (not && not.indexOf(card) !== -1));
                    return card;
                }
                const deck = [];
                for (let i = 0; i < defs.rules.cardsPerDeck; i++) {
                    if (i < defs.rules.bossCards) {
                        deck.push(pick(defs.cardsByBack[2], [defs.cardsByID[91]].concat(deck)));
                    }
                    else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
                        deck.push(pick(defs.cardsByBack[1], deck.slice(defs.rules.bossCards)));
                    }
                    else {
                        deck.push(pick(defs.cardsByBack[0]));
                    }
                }
                return deck;
            }
            async playRound(ctx) {
                let choices = 0;
                const myHand = ctx.state.hand[ctx.player - 1];
                let tp = ctx.state.tp[ctx.player - 1];
                for (let i = 0; i < myHand.length; i++) {
                    const cardTP = myHand[i].card.effectiveTP();
                    if (cardTP <= tp) {
                        tp -= cardTP;
                        choices = SpyCards.Binary.setBit(choices, i, 1);
                    }
                }
                return choices;
            }
        }
        AI.GenericNPC = GenericNPC;
        // Card Master:
        // - has a predefined deck and player
        class CardMaster extends GenericNPC {
            constructor(name, deck) {
                super();
                this.name = name;
                this.deck = deck;
            }
            async pickPlayer() {
                return this.name ?
                    SpyCards.TheRoom.Player.characters.find((p) => p.name === this.name) :
                    super.pickPlayer();
            }
            async createDeck(defs) {
                return SpyCards.Decks.decode(defs, this.deck);
            }
        }
        AI.CardMaster = CardMaster;
        // Tourney Player:
        // - has a predefined player
        class TourneyPlayer extends GenericNPC {
            constructor(name) {
                super();
                this.name = name;
            }
            async pickPlayer() {
                return SpyCards.TheRoom.Player.characters.find((p) => p.name === this.name);
            }
        }
        AI.TourneyPlayer = TourneyPlayer;
        // Janet:
        // - janet
        class Janet extends TourneyPlayer {
            constructor() {
                super("janet");
            }
            async createDeck(defs) {
                const deck = await super.createDeck(defs);
                for (let i = 0; i < defs.rules.cardsPerDeck && i < defs.rules.bossCards; i++) {
                    if (!defs.banned[91] && defs.cardsByID[91].effectiveTP() !== Infinity && Math.random() >= 0.5) {
                        deck[i] = defs.cardsByID[91];
                        break;
                    }
                }
                return deck;
            }
        }
        AI.Janet = Janet;
        // Saved Decks:
        // - chooses a random valid local deck if one is available
        class SavedDecks extends GenericNPC {
            async createDeck(defs) {
                const decks = SpyCards.Decks.loadSaved(defs, true);
                if (decks.length) {
                    return decks[Math.floor(Math.random() * decks.length)];
                }
                return await super.createDeck(defs);
            }
        }
        AI.SavedDecks = SavedDecks;
        class MenderSpam extends NPC {
            constructor(mothiva = true) {
                super();
                this.mothiva = mothiva;
            }
            async pickPlayer() {
                if (this.mothiva) {
                    return SpyCards.TheRoom.Player.characters.find((c) => c.name === "pibu");
                }
                return SpyCards.TheRoom.Player.characters.find((c) => c.name === "nero");
            }
            async createDeck(defs) {
                const deck = [];
                if (defs.rules.bossCards !== 1 || defs.rules.miniBossCards !== 2) {
                    throw new Error("cannot create mender deck with non-standard number of (mini-)boss cards");
                }
                deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.HeavyDroneB33]);
                if (this.mothiva) {
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Mothiva]);
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Zasp]);
                }
                else {
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Kali]);
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Kabbu]);
                }
                while (deck.length < defs.rules.cardsPerDeck) {
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Mender]);
                }
                return deck;
            }
            async playRound(ctx) {
                const myHand = ctx.state.hand[ctx.player - 1];
                const mothiva = myHand.findIndex((c) => c.card.id === SpyCards.CardData.GlobalCardID.Mothiva || c.card.id === SpyCards.CardData.GlobalCardID.Kali);
                const zasp = myHand.findIndex((c) => c.card.id === SpyCards.CardData.GlobalCardID.Zasp || c.card.id === SpyCards.CardData.GlobalCardID.Kabbu);
                const b33 = myHand.findIndex((c) => c.card.id === SpyCards.CardData.GlobalCardID.HeavyDroneB33);
                let tp = ctx.state.tp[ctx.player - 1];
                if (mothiva !== -1 && zasp !== -1 && tp >= myHand[mothiva].card.effectiveTP() + myHand[zasp].card.effectiveTP()) {
                    return SpyCards.Binary.bit(mothiva) + SpyCards.Binary.bit(zasp);
                }
                if (myHand.length < 5) {
                    return 0;
                }
                const mothivaOrZasp = mothiva === -1 ? zasp : mothiva;
                if (mothivaOrZasp !== -1 && tp >= myHand[mothivaOrZasp].card.effectiveTP()) {
                    return SpyCards.Binary.bit(mothivaOrZasp);
                }
                if (ctx.state.hp[ctx.player - 1] > 2) {
                    return 0;
                }
                if (b33 !== -1 && tp < myHand[b33].card.effectiveTP()) {
                    return 0;
                }
                if (b33 === -1 && tp < 5) {
                    return 0;
                }
                let toPlay = 0;
                if (b33 !== -1) {
                    tp -= myHand[b33].card.effectiveTP();
                    toPlay = SpyCards.Binary.setBit(toPlay, b33, 1);
                }
                for (let i = 0; i < myHand.length; i++) {
                    if (SpyCards.Binary.getBit(toPlay, i)) {
                        continue;
                    }
                    if (tp >= myHand[i].card.effectiveTP()) {
                        tp -= myHand[i].card.effectiveTP();
                        toPlay = SpyCards.Binary.setBit(toPlay, i, 1);
                    }
                }
                return toPlay;
            }
        }
        AI.MenderSpam = MenderSpam;
        class TourneyPlayer2 extends TourneyPlayer {
            constructor(name) {
                super(name);
            }
            countSynergies(defs, deck, source, target, perCard, perTribe) {
                // for now, assume all synergies are good
                let count = 0;
                const cards = [target.id];
                const tribes = [];
                for (let tribe of target.tribes) {
                    if (tribe.tribe === SpyCards.Tribe.Custom) {
                        tribes.push(tribe.custom.name);
                    }
                    else {
                        tribes.push(tribe.tribe);
                    }
                }
                for (let effect of target.effects) {
                    if (effect.type === SpyCards.EffectType.CondCoin || effect.type === SpyCards.EffectType.CondLimit) {
                        effect = effect.result;
                    }
                    if (effect.type === SpyCards.EffectType.Summon && !effect.opponent) {
                        for (let i = 0; i < effect.amount; i++) {
                            if (effect.generic) {
                                if (effect.tribe === SpyCards.Tribe.Custom) {
                                    tribes.push(effect.customTribe);
                                }
                                else if (effect.tribe !== SpyCards.Tribe.None) {
                                    tribes.push(effect.tribe);
                                }
                            }
                            else {
                                cards.push(effect.card);
                                for (let tribe of defs.cardsByID[effect.card].tribes) {
                                    tribes.push(tribe.tribe === SpyCards.Tribe.Custom ? tribe.custom.name : tribe.tribe);
                                }
                            }
                        }
                    }
                }
                for (let effect of source.effects) {
                    let mul = 1;
                    if (effect.type === SpyCards.EffectType.CondLimit && !effect.negate && source.id !== target.id) {
                        effect = effect.result;
                    }
                    if ((effect.type === SpyCards.EffectType.Empower || effect.type === SpyCards.EffectType.CondCard) && !effect.opponent) {
                        if (effect.type === SpyCards.EffectType.CondCard) {
                            mul /= effect.amount;
                        }
                        if (effect.generic) {
                            if (effect.tribe === SpyCards.Tribe.Custom) {
                                for (let t of tribes) {
                                    if (t === effect.customTribe) {
                                        count += perTribe * mul;
                                    }
                                }
                            }
                            else if (effect.tribe !== SpyCards.Tribe.None) {
                                for (let t of tribes) {
                                    if (t === effect.tribe) {
                                        count += perTribe * mul;
                                    }
                                }
                            }
                        }
                        else {
                            for (let c of cards) {
                                if (c === effect.card) {
                                    count += perCard * mul;
                                }
                            }
                        }
                    }
                }
                return count;
            }
            async selectCard(deck, defs, choices) {
                const choiceWeights = choices.map((c) => {
                    if (c.id === SpyCards.CardData.GlobalCardID.TheEverlastingKing || defs.unpickable[c.id] || c.effectiveTP() > 10) {
                        return { card: c, weight: 0 };
                    }
                    return { card: c, weight: 1 };
                });
                for (let c of choiceWeights) {
                    let statTotal = 0;
                    const unprocessedEffects = c.card.effects.slice(0);
                    while (unprocessedEffects.length) {
                        const effect = unprocessedEffects.shift();
                        // numb is very powerful
                        if (effect.type === SpyCards.EffectType.Numb && effect.opponent) {
                            statTotal += Math.min(effect.amount * 2, 5);
                        }
                        if (effect.type === SpyCards.EffectType.Stat && !effect.opponent && !effect.negate) {
                            statTotal += effect.amount;
                        }
                        if (effect.type === SpyCards.EffectType.Empower && effect.generic && !effect.opponent && !effect.negate) {
                            statTotal += effect.amount;
                        }
                        if (effect.type === SpyCards.EffectType.Summon && !effect.generic && !effect.opponent) {
                            for (let i = 0; i < effect.amount; i++) {
                                unprocessedEffects.push(...defs.cardsByID[effect.card].effects);
                            }
                        }
                        if (effect.type === SpyCards.EffectType.CondCoin &&
                            effect.result.type === SpyCards.EffectType.Stat && !effect.result.opponent && !effect.result.negate &&
                            (!effect.generic || (effect.tailsResult.type === SpyCards.EffectType.Stat && !effect.tailsResult.opponent && !effect.tailsResult.negate))) {
                            statTotal += effect.result.amount * effect.amount / 2;
                            if (effect.generic) {
                                statTotal += effect.tailsResult.amount * effect.amount / 2;
                            }
                        }
                    }
                    // avoid division by 0
                    const tp = Math.max(0.1, c.card.effectiveTP());
                    // (vanilla) cards that have a nonzero raw stat total that is lower than their TP are weak
                    if (statTotal && isFinite(statTotal)) {
                        c.weight *= statTotal / tp;
                    }
                    // don't add too many cards with high TP
                    c.weight *= Math.min(1, 5 / tp);
                    for (let card of deck) {
                        // favor cards that have synergies with existing cards
                        let synergies = 0;
                        synergies += this.countSynergies(defs, deck, c.card, card, c.card.rank() === SpyCards.Rank.MiniBoss ? 3 : 1, card.rank() >= SpyCards.Rank.MiniBoss ? 2 : 1);
                        synergies += this.countSynergies(defs, deck, card, c.card, c.card.rank() === SpyCards.Rank.MiniBoss ? 3 : 1, card.rank() >= SpyCards.Rank.MiniBoss ? 2 : 1);
                        c.weight *= Math.pow(7.5, synergies);
                        // try not to pick too many cards with the same TP cost
                        if (card.effectiveTP() === c.card.effectiveTP()) {
                            c.weight *= 0.25;
                        }
                        // favor duplicates over non-duplicates
                        if (card.id === c.card.id) {
                            // one duplicate    = 2.50
                            // two duplicates   = 1.56
                            // three duplicates = 0.57
                            // four duplicates  = 0.15
                            c.weight *= 2.5 / deck.reduce((n, o) => o.id === c.card.id ? n + 1 : n, 0);
                        }
                    }
                }
                for (let c of choiceWeights) {
                    if (defs.banned[c.card.id]) {
                        c.weight = 0;
                    }
                    else {
                        c.weight = Math.max(this.adjustWeight(c.card, c.weight, deck, defs), 0);
                    }
                }
                const totalWeight = choiceWeights.reduce((t, w) => t + w.weight, 0);
                let choice = Math.random() * totalWeight;
                for (let { card, weight } of choiceWeights) {
                    choice -= weight;
                    if (choice < 0) {
                        return card;
                    }
                }
                // rounding error, probably
                return choices[choices.length - 1];
            }
            async createDeck(defs) {
                const deck = [];
                function unique(choices, except) {
                    return choices.filter((c) => except.indexOf(c) === -1);
                }
                for (let i = 0; i < defs.rules.cardsPerDeck; i++) {
                    if (i < defs.rules.bossCards) {
                        deck.push(await this.selectCard(deck, defs, unique(defs.cardsByBack[2], deck)));
                    }
                    else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
                        deck.push(await this.selectCard(deck, defs, unique(defs.cardsByBack[1], deck.slice(defs.rules.bossCards))));
                    }
                    else {
                        deck.push(await this.selectCard(deck, defs, defs.cardsByBack[0]));
                    }
                }
                const rankOrder = [SpyCards.Rank.Boss, SpyCards.Rank.MiniBoss, SpyCards.Rank.Attacker, SpyCards.Rank.Effect];
                deck.sort((a, b) => {
                    if (a.rank() !== b.rank()) {
                        return rankOrder.indexOf(a.rank()) - rankOrder.indexOf(b.rank());
                    }
                    return defs.allCards.indexOf(a) - defs.allCards.indexOf(b);
                });
                return deck;
            }
        }
        AI.TourneyPlayer2 = TourneyPlayer2;
        class GenericNPC2 extends TourneyPlayer2 {
            constructor() {
                const choices = SpyCards.TheRoom.Player.characters;
                super(choices[Math.floor(Math.random() * choices.length)].name);
            }
            adjustWeight(card, weight) {
                return weight;
            }
        }
        AI.GenericNPC2 = GenericNPC2;
        class BuGi2 extends TourneyPlayer2 {
            constructor() {
                super("bu-gi");
            }
            adjustWeight(card, weight) {
                // bu-gi likes attacker cards
                if (card.rank() === SpyCards.Rank.Attacker) {
                    return weight * 2.5;
                }
                return weight;
            }
        }
        AI.BuGi2 = BuGi2;
        class Carmina2 extends TourneyPlayer2 {
            constructor() {
                super("carmina");
            }
            adjustWeight(card, weight) {
                // carmina favors cards with random chances on them.
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.CondCoin || (effect.type === SpyCards.EffectType.Summon && effect.generic)) {
                        return weight * 10;
                    }
                }
                return weight;
            }
        }
        AI.Carmina2 = Carmina2;
        class Janet2 extends TourneyPlayer2 {
            constructor() {
                super("janet");
            }
            adjustWeight(card, weight, deck, defs) {
                // janet.
                if (card.id !== SpyCards.CardData.GlobalCardID.TheEverlastingKing) {
                    return weight;
                }
                if (!defs.banned[card.id] && card.effectiveTP() <= 10) {
                    return defs.cardsByBack[2].length;
                }
                return weight;
            }
        }
        AI.Janet2 = Janet2;
        class Johnny2 extends TourneyPlayer2 {
            constructor() {
                super("johnny");
            }
            adjustWeight(card, weight) {
                // johnny likes cards with non-random conditions, and (mini-)bosses with empower.
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.Empower && card.rank() >= SpyCards.Rank.MiniBoss) {
                        return weight * 5;
                    }
                    if (effect.type !== SpyCards.EffectType.CondCoin && effect.type !== SpyCards.EffectType.CondLimit && effect.type >= 128) {
                        if (effect.type === SpyCards.EffectType.CondCard) {
                            return weight * 5 / effect.amount;
                        }
                        return weight * 5;
                    }
                }
                return weight;
            }
        }
        AI.Johnny2 = Johnny2;
        class Kage2 extends TourneyPlayer2 {
            constructor() {
                super("kage");
            }
            adjustWeight(card, weight) {
                // kage likes cards with multiple tribes and cards with empower or unity on them.
                weight *= card.tribes.length / 2 + 0.5;
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.CondLimit && !effect.negate) {
                        effect = effect.result;
                    }
                    if (effect.type === SpyCards.EffectType.Empower && !effect.negate && !effect.opponent) {
                        return weight * 2;
                    }
                }
                return weight;
            }
        }
        AI.Kage2 = Kage2;
        class Ritchee2 extends TourneyPlayer2 {
            constructor() {
                super("ritchee");
            }
            adjustWeight(card, weight) {
                // ritchee prefers high-cost cards.
                return weight * Math.pow(card.effectiveTP(), 3);
            }
        }
        AI.Ritchee2 = Ritchee2;
        class Serene2 extends TourneyPlayer2 {
            constructor() {
                super("serene");
            }
            adjustWeight(card, weight) {
                // serene likes cards with low TP costs.
                if (card.effectiveTP() > 3) {
                    weight *= Math.pow(3 / card.effectiveTP(), 3);
                }
                return weight;
            }
        }
        AI.Serene2 = Serene2;
        class Chuck2 extends TourneyPlayer2 {
            constructor() {
                super("chuck");
            }
            adjustWeight(card, weight) {
                // chuck loves seedlings.
                if (card.tribes.some((t) => t.tribe === SpyCards.Tribe.Seedling)) {
                    return weight * 25;
                }
                return weight;
            }
        }
        AI.Chuck2 = Chuck2;
        class Arie2 extends TourneyPlayer2 {
            constructor() {
                super("arie");
            }
            adjustWeight(card, weight) {
                // arie favors cards that cost near to 2 TP, and dislikes cards
                // that are fungi, or that are bug plus another tribe.
                if (card.effectiveTP() === 2) {
                    weight *= 3;
                }
                else {
                    weight *= Math.pow(1 / (1 + Math.abs(2 - card.effectiveTP())), 4);
                }
                for (let t of card.tribes) {
                    if (t.tribe === SpyCards.Tribe.Fungi) {
                        weight /= 10;
                        break;
                    }
                    if (t.tribe === SpyCards.Tribe.Bug && card.tribes.length > 1) {
                        weight /= 10;
                        break;
                    }
                }
                return weight;
            }
        }
        AI.Arie2 = Arie2;
        class Shay2 extends TourneyPlayer2 {
            constructor() {
                super("shay");
            }
            adjustWeight(card, weight) {
                // shay likes Thugs and cards with numb.
                if (card.tribes.some((t) => t.tribe === SpyCards.Tribe.Thug)) {
                    return weight * (card.rank() >= SpyCards.Rank.MiniBoss ? 25 : 7.5);
                }
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.CondCoin) {
                        effect = effect.result;
                    }
                    if (effect.type === SpyCards.EffectType.Numb) {
                        return weight * 5;
                    }
                }
                return weight;
            }
        }
        AI.Shay2 = Shay2;
        class Crow2 extends TourneyPlayer2 {
            constructor() {
                super("crow");
            }
            adjustWeight(card, weight) {
                // crow likes (non-boss) ??? and (all) Bot cards below 6 TP,
                // and cards with both ATK and a non-card-based condition.
                if (card.effectiveTP() < 6) {
                    for (let t of card.tribes) {
                        if ((t.tribe === SpyCards.Tribe.Unknown && card.rank() !== SpyCards.Rank.Boss) || t.tribe === SpyCards.Tribe.Bot) {
                            return weight * 30;
                        }
                    }
                }
                if (card.rank() === SpyCards.Rank.Boss) {
                    return weight / 100;
                }
                const haveCondition = card.effects.some((e) => e.type >= 128 && e.type !== SpyCards.EffectType.CondCard);
                const haveATK = card.effects.some((e) => e.type === SpyCards.EffectType.Stat && !e.defense && !e.opponent && !e.negate);
                if (haveCondition && haveATK) {
                    return weight * 40;
                }
                return weight;
            }
        }
        AI.Crow2 = Crow2;
        function getNPC(name) {
            if (typeof name === "string" && name.startsWith("cm-")) {
                const parts = name.split("-", 3);
                return new CardMaster(parts[2], parts[1]);
            }
            switch (name) {
                case "janet":
                    return new Janet();
                case "bu-gi":
                case "johnny":
                case "kage":
                case "ritchee":
                case "serene":
                case "carmina":
                    return new TourneyPlayer(name);
                case "saved-decks":
                    return new SavedDecks();
                case "tutorial":
                    return new CardMaster("carmina", "01H00000000013HSMR");
                case "carmina2":
                    return new CardMaster("carmina", "3P7T52H8MA5273HG842YF7KR");
                case "chuck":
                    return new CardMaster("chuck", "4HH0000007VXYZFG84210GG8");
                case "arie":
                    return new CardMaster("arie", "310J10G84212NANCPAD6K7VW");
                case "shay":
                    return new CardMaster("shay", "511KHRWE631GRD6K9MMA5840");
                case "crow":
                    return new CardMaster("crow", "101AXEPH8MCJ8G845G");
                case "mender-spam":
                    return new MenderSpam(Math.random() >= 0.5);
                case "mender-spam-mothiva":
                    return new MenderSpam(true);
                case "mender-spam-kali":
                    return new MenderSpam(false);
                case "tp2-generic":
                    return new GenericNPC2();
                case "tp2-janet":
                    return new Janet2();
                case "tp2-bu-gi":
                    return new BuGi2();
                case "tp2-johnny":
                    return new Johnny2();
                case "tp2-kage":
                    return new Kage2();
                case "tp2-ritchee":
                    return new Ritchee2();
                case "tp2-serene":
                    return new Serene2();
                case "tp2-carmina":
                    return new Carmina2();
                case "tp2-chuck":
                    return new Chuck2();
                case "tp2-arie":
                    return new Arie2();
                case "tp2-shay":
                    return new Shay2();
                case "tp2-crow":
                    return new Crow2();
                default:
                    return new GenericNPC();
            }
        }
        AI.getNPC = getNPC;
    })(AI = SpyCards.AI || (SpyCards.AI = {}));
})(SpyCards || (SpyCards = {}));
