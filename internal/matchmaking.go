package internal

import "nhooyr.io/websocket"

// Matchmaking websocket close codes.
const (
	MMServerError     websocket.StatusCode = 3000
	MMClientError     websocket.StatusCode = 3001
	MMConnectionError websocket.StatusCode = 3002
	MMTimeout         websocket.StatusCode = 3003
	MMNotFound        websocket.StatusCode = 3004
)
