// +build js,wasm
// +build !headless

package audio

import (
	"context"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"sync"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
)

var (
	actx       = js.Global().Get("AudioContext").New()
	musicGain  = actx.Call("createGain")
	_          = musicGain.Call("connect", actx.Get("destination"))
	musicFFT   = actx.Call("createAnalyser")
	_          = musicFFT.Call("connect", musicGain)
	soundsGain = actx.Call("createGain")
	_          = soundsGain.Call("connect", actx.Get("destination"))
)

// UnsuspendContext requests that the browser audio context resume,
// which browsers may prevent until user input.
func UnsuspendContext() {
	if actx.Get("state").String() == "suspended" {
		actx.Call("resume")
	}
}

type implData struct {
	buffer    js.Value
	loadError error
	loadOnce  sync.Once
	gain      js.Value
	musicGain js.Value
}

func (s *Sound) loadData() {
	audioSettingsOnce.Do(initAudioSettings)

	defer func() {
		if r := recover(); r != nil {
			if jsErr, ok := r.(js.Error); ok {
				s.impl.loadError = jsErr
			} else {
				panic(r)
			}
		}
	}()

	if s.Volume == 0 {
		s.Volume = 1
	}

	if s.Pitch == 0 {
		s.Pitch = 1
	}

	url := s.Name

	if !strings.Contains(url, "/") {
		url = "audio/" + url + ".opus"
	}

	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, url, nil)
	if err != nil {
		s.impl.loadError = xerrors.Errorf("audio: failed to fetch sound %q: %w", s.Name, err)

		return
	}

	req = internal.ModifyRequest(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		s.impl.loadError = xerrors.Errorf("audio: failed to fetch sound %q: %w", s.Name, err)

		return
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		s.impl.loadError = xerrors.Errorf("audio: failed to fetch sound %q: %w", s.Name, err)

		return
	}

	jsArray := js.Global().Get("Uint8Array").New(len(b))
	js.CopyBytesToJS(jsArray, b)

	if actx.Get("state").String() == "suspended" {
		_, err := internal.Await(actx.Call("resume"))
		if err != nil {
			s.impl.loadError = xerrors.Errorf("audio: failed to fetch sound %q: %w", s.Name, err)

			return
		}
	}

	buffer, err := internal.Await(actx.Call("decodeAudioData", jsArray.Get("buffer")))
	if err != nil {
		s.impl.loadError = xerrors.Errorf("audio: failed to fetch sound %q: %w", s.Name, err)

		return
	}

	s.impl.buffer = buffer
}

// Preload loads the Sound and returns the error encountered, if any.
func (s *Sound) Preload() error {
	s.impl.loadOnce.Do(s.loadData)

	return s.impl.loadError
}

func (s *Sound) createSource() js.Value {
	s.impl.loadOnce.Do(func() {
		log.Println("WARNING: non-preloaded sound:", s.Name)

		s.loadData()
	})

	if s.impl.loadError != nil {
		return js.Null()
	}

	source := actx.Call("createBufferSource")
	source.Set("buffer", s.impl.buffer)
	source.Get("playbackRate").Call("setValueAtTime", s.Pitch, actx.Get("currentTime"))

	if s.Loop {
		source.Set("loop", true)
		source.Set("loopStart", s.Start)
		source.Set("loopEnd", s.End)
	}

	return source
}

// Release releases any system resources used by the Sound. It is an error to
// use the Sound after calling Release.
func (s *Sound) Release() {
	s.impl.loadError = xerrors.Errorf("audio: sound %q used after free", s.Name)
	s.impl.buffer = js.Undefined()
	s.impl.gain = js.Undefined()
	s.impl.musicGain = js.Undefined()
}

// PlaySound plays a sound, optionally modifying its pitch and volume.
func (s *Sound) PlaySound(delay, overridePitch, overrideVolume float64) {
	audioSettingsOnce.Do(initAudioSettings)

	if SoundsVolume == 0 {
		return
	}

	if IgnoreAudio > 0 {
		return
	}

	src := s.createSource()
	if !src.Truthy() {
		return
	}

	dest := soundsGain

	if s.Volume != 1 {
		if !s.impl.gain.Truthy() {
			s.impl.gain = actx.Call("createGain")
			s.impl.gain.Call("connect", dest)
			s.impl.gain.Get("gain").Call("setValueAtTime", s.Volume, actx.Get("currentTime"))
		}

		dest = s.impl.gain
	}

	if overrideVolume != 0 {
		gain := actx.Call("createGain")
		gain.Call("connect", dest)
		gain.Get("gain").Call("setValueAtTime", overrideVolume, actx.Get("currentTime"))
		dest = gain
	}

	if overridePitch != 0 {
		src.Get("playbackRate").Call("setValueAtTime", overridePitch*s.Pitch, actx.Get("currentTime"))
	}

	src.Call("connect", dest)
	src.Call("start", actx.Get("currentTime").Float()+delay)
}

// PlayMusic plays a sound as the current music track.
func (s *Sound) PlayMusic(delay float64, restart bool) {
	audioSettingsOnce.Do(initAudioSettings)

	if IgnoreAudio > 0 {
		return
	}

	if activeSong == s && !restart {
		return
	}

	src := s.createSource()
	if !src.Truthy() {
		return
	}

	dest := musicFFT

	if s.Volume != 1 {
		if !s.impl.musicGain.Truthy() {
			s.impl.musicGain = actx.Call("createGain")
			s.impl.musicGain.Call("connect", dest)
			s.impl.musicGain.Get("gain").Call("setValueAtTime", s.Volume, actx.Get("currentTime"))
		}

		dest = s.impl.musicGain
	}

	StopMusic()

	src.Call("connect", dest)
	src.Call("start", actx.Get("currentTime").Float()+delay)

	activeMusic = src
	activeSong = s
}

var (
	activeMusic js.Value
	activeSong  *Sound
)

// StopMusic stops the current music track.
func StopMusic() {
	if activeMusic.Truthy() {
		activeMusic.Call("stop")
	}

	activeMusic = js.Null()
	activeSong = nil
}

func onVolumeChanged() {
	musicGain.Get("gain").Call("setValueAtTime", MusicVolume, actx.Get("currentTime"))
	soundsGain.Get("gain").Call("setValueAtTime", SoundsVolume, actx.Get("currentTime"))
}
