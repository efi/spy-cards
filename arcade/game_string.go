// Code generated by "stringer -type Game"; DO NOT EDIT.

package arcade

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[MiteKnight-1]
	_ = x[FlowerJourney-2]
}

const _Game_name = "MiteKnightFlowerJourney"

var _Game_index = [...]uint8{0, 10, 23}

func (i Game) String() string {
	i -= 1
	if i >= Game(len(_Game_index)-1) {
		return "Game(" + strconv.FormatInt(int64(i+1), 10) + ")"
	}
	return _Game_name[_Game_index[i]:_Game_index[i+1]]
}
