package sprites

// GUI sprites for Spy Cards.
var (
	gui = loadSpriteSheet("img/arcade/gui.png", 75, 2048, 2048, false)

	WhiteBar = gui.sprite(1, 975, 438.9732, 48, 0.5000305, 0.5)
	HP       = gui.sprite(1493, 26, 125, 132, 0.5082959, 0.5366576)
	ATK      = gui.sprite(1636.027, 17, 140.9733, 144, 0.4999052, 0.5)
	DEF      = gui.sprite(1638, 179, 124, 132, 0.5, 0.5)
	TP       = gui.sprite(783, 96, 82, 81, 0.5, 0.5)
	CardNum  = [10]*Sprite{
		gui.sprite(10, 480, 32, 58, 0.5, 0.5),
		gui.sprite(43, 479, 23, 59, 0.5, 0.5),
		gui.sprite(69, 480, 33, 58, 0.5, 0.5),
		gui.sprite(103, 479, 32, 59, 0.5, 0.5),
		gui.sprite(136, 480, 34, 58, 0.5, 0.5),
		gui.sprite(171, 478, 34, 60, 0.5, 0.5),
		gui.sprite(206, 479, 34, 62, 0.5, 0.5),
		gui.sprite(241, 480, 32, 59.97325, 0.5, 0.5085601),
		gui.sprite(275, 479, 34, 59, 0.5, 0.5),
		gui.sprite(311, 477, 34, 62, 0.5, 0.5),
	}
	BackEnemy    = gui.sprite(2, 1225, 175, 237, 0.5, 0.5)
	CardFront    = gui.sprite(540, 1225, 174, 237, 0.5, 0.5)
	BackMiniBoss = gui.sprite(181, 1225, 175, 237, 0.5, 0.5)
	BackBoss     = gui.sprite(360, 1225, 175, 237, 0.5, 0.5)
	Pointer      = gui.sprite(1920.027, 1350, 124.9733, 78, 1, 0.5)
	Capsule      = gui.sprite(1657, 1839, 389, 206, 0.5, 0.5)
	CircleArrow  = gui.sprite(0, 919, 52, 52, 0.5, 0.5)
	WideArrow    = gui.sprite(104, 949, 315, 21, 0.5, 0.5)
	Tab          = gui.sprite(3, 550, 429, 134, 0.5, 0.5)

	portraits = loadSpriteSheet("img/portraits2.png", 100, 2048, 2048, false)

	Portraits = func() (p [235]*Sprite) {
		for i := range p {
			p[i] = portraits.sprite(float32(i%16)*128, 2048-float32(i/16+1)*128, 128, 128, 0.5, 0.5)
		}

		return
	}()

	customCard = loadSpriteSheet("img/customcard.png", 75, 512, 512, false)

	CardFrontWindow = customCard.sprite(1, 274, 174, 237, 0.5, 0.5)
	TPInf           = customCard.sprite(427, 428, 84, 84, 0.5, 0.5)
	TribeBubble     = customCard.sprite(175, 459, 123, 26, 0.5, 0.5)
	TribeBubbleWide = customCard.sprite(175, 487, 253, 25, 0.5, 0.5)
	Description     = customCard.sprite(177, 329, 202, 130, 0.5, 0.5)
	BackToken       = customCard.sprite(0, 35, 177, 239, 0.5, 0.5)
	Happy           = customCard.sprite(435, 371, 76, 56, 0.5, 0.5)
	Sad             = customCard.sprite(426, 316, 85, 54, 0.5, 0.5)
)
