package gfx

import (
	"context"
	"image"
	"image/draw"
	"io/ioutil"
	"runtime"
	"sync"
	"sync/atomic"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

// Texture is an OpenGL texture.
type Texture struct {
	Name    string
	Texture gl.Texture
	nearest bool
	img     *image.RGBA
}

var textures []*Texture

// NewTexture creates a texture.
func NewTexture(name string, img image.Image, nearest bool) *Texture {
	rgba, ok := img.(*image.RGBA)
	if !ok {
		bounds := img.Bounds()

		rgba = image.NewRGBA(bounds.Sub(bounds.Min))

		draw.Draw(rgba, rgba.Rect, img, bounds.Min, draw.Src)
	}

	t := &Texture{
		Name:    name,
		img:     rgba,
		nearest: nearest,
	}

	onNewTexture(t)

	return t
}

// AssetTexture is a lazy-loaded texture.
type AssetTexture struct {
	Name     string
	tex      *Texture
	err      error
	load     func() ([]byte, string, error)
	loaded   chan struct{}
	once     sync.Once
	lazyOnce int32
	nearest  bool
	cache    bool
	cached   *AssetTexture
}

// NewAssetTexture creates a lazy-loaded texture from an asset.
func NewAssetTexture(name string, nearest bool) *AssetTexture {
	at := &AssetTexture{
		Name:    name,
		nearest: nearest,
		load: func() ([]byte, string, error) {
			f, err := internal.OpenAsset(context.TODO(), name)
			if err != nil {
				return nil, "", xerrors.Errorf("gfx: loading texture asset %q: %w", name, err)
			}
			defer f.Close()

			b, err := ioutil.ReadAll(f)
			if err != nil {
				return nil, "", xerrors.Errorf("gfx: loading texture asset %q: %w", name, err)
			}

			return b, "image/png", nil
		},
		loaded: make(chan struct{}),
	}

	runtime.SetFinalizer(at, (*AssetTexture).Release)

	return at
}

var texCache = internal.Cache{MaxEntries: 256}

// NewCachedAssetTexture creates a lazy-loaded texture from an asset.
func NewCachedAssetTexture(name string) *AssetTexture {
	at := NewAssetTexture(name, false)
	at.cache = true

	if tex, ok, _ := texCache.Get(name); ok {
		at.cached = tex.(*AssetTexture)
		at.tex, at.err = at.cached.tex, at.cached.err

		close(at.loaded)

		at.once.Do(func() {})
	}

	return at
}

// NewEmbeddedTexture creates an AssetTexture that loads from memory.
func NewEmbeddedTexture(name string, nearest bool, f func() ([]byte, string, error)) *AssetTexture {
	at := &AssetTexture{
		Name:    name,
		nearest: nearest,
		load:    f,
		loaded:  make(chan struct{}),
	}

	go at.once.Do(at.init)

	runtime.SetFinalizer(at, (*AssetTexture).Release)

	return at
}

// Release deletes the texture handle.
func (a *AssetTexture) Release() {
	a.once.Do(a.init)

	if a.cached != nil {
		runtime.KeepAlive(a.cached)

		return
	}

	if a.tex != nil {
		a.tex.Release()
		a.tex = nil
	}
}

// Preload loads the texture and returns any error encountered.
func (a *AssetTexture) Preload() error {
	a.once.Do(a.init)

	return a.err
}

// Texture loads the texture and returns the texture handle.
func (a *AssetTexture) Texture() gl.Texture {
	a.once.Do(a.init)

	if a.tex == nil {
		return gl.Texture{}
	}

	return a.tex.Texture
}

// LazyTexture loads the texture and returns the texture handle.
func (a *AssetTexture) LazyTexture(def gl.Texture) gl.Texture {
	select {
	case <-a.loaded:
		if a.tex == nil {
			return def
		}

		return a.tex.Texture
	default:
		if atomic.CompareAndSwapInt32(&a.lazyOnce, 0, 1) {
			go a.once.Do(a.init)
		}

		return def
	}
}

// BlankAssetTexture returns a 1x1 white square.
func BlankAssetTexture() *AssetTexture {
	t := NewTexture("(blank)", &image.RGBA{
		Pix:    []uint8{255, 255, 255, 255},
		Stride: 4,
		Rect:   image.Rect(0, 0, 1, 1),
	}, true)

	at := &AssetTexture{
		Name:    "(blank)",
		nearest: true,
		tex:     t,
	}

	at.once.Do(func() {})

	return at
}
