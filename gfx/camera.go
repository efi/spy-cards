package gfx

import "math"

// Camera tracks the position and orientation of the 3D camera.
type Camera struct {
	Offset      Matrix
	Rotation    Matrix
	Position    Matrix
	Perspective Matrix
	stack       []Matrix
}

// SetDefaults sets the Matrix fields in Camera to default values.
func (c *Camera) SetDefaults() {
	c.Offset = Translation(0, 2.25, -8.25)
	c.Rotation = RotationX(10 * math.Pi / 180)
	c.Position = Translation(0, 70, 0)
	c.Perspective = Perspective(-60*math.Pi/180, 16.0/9.0, 0.3, 150)

	if len(c.stack) == 0 {
		c.stack = append(make([]Matrix, 0, 16), Identity())
	}
}

// PushTransform adds a transform to the transform stack.
func (c *Camera) PushTransform(m Matrix) {
	c.stack = append(c.stack, MultiplyMatrix(c.stack[len(c.stack)-1], m))
}

// PopTransform removes the last transform added via PushTransform.
func (c *Camera) PopTransform() {
	c.stack = c.stack[:len(c.stack)-1]
}

// Combined returns the combined transformation matrix.
func (c *Camera) Combined() Matrix {
	matrix := Identity()
	matrix = MultiplyMatrix(matrix, c.Offset)
	matrix = MultiplyMatrix(matrix, c.Rotation)
	matrix = MultiplyMatrix(matrix, c.Position)
	matrix = MultiplyMatrix(matrix, Scale(-1, -1, -1))
	matrix = MultiplyMatrix(matrix, c.stack[len(c.stack)-1])

	return matrix
}
