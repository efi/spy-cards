// +build headless

package sprites

import (
	"image/color"
)

// TextAdvance returns the number of units the next letter is offset after this letter.
func TextAdvance(font FontID, letter rune, scaleX float32) float32 {
	return 1
}

// DrawTextFuncEx draws a string to the screen.
func DrawTextFuncEx(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) color.RGBA, flags RenderFlag, ry, rz, blur float32) {
	// do nothing
}
