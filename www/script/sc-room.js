"use strict";
var SpyCards;
(function (SpyCards) {
    var TheRoom;
    (function (TheRoom) {
        class Stage {
            async init() {
                SpyCards.UI.html.classList.add("in-room");
                this.rpc = await SpyCards.Native.roomRPC();
                if (this.rpc) {
                    this.id = await new Promise((resolve) => this.rpc.newStage(resolve));
                    this.rpc.setRoom(this.id);
                }
            }
            async deinit() {
                if (this.rpc) {
                    this.rpc.exit();
                    this.id = 0;
                    this.rpc = null;
                    SpyCards.UI.html.classList.remove("in-room", "room-flipped");
                }
            }
            setFlipped(flipped) {
                if (this.rpc) {
                    this.rpc.setCurrentPlayer(this.id, flipped ? 2 : 1);
                }
            }
            setPlayer(player) {
                if (!this.rpc) {
                    return;
                }
                player.rpc = this.rpc;
                player.id = this.rpc.setPlayer(this.id, player.name, player.num);
            }
            addAudienceMember(member) {
                if (!this.rpc) {
                    return;
                }
                this.rpc.addAudience(this.id, member.id);
            }
            createAudienceMember(type, x, y, back, flip, color, excitement) {
                if (!this.rpc) {
                    return new Audience(null, 0);
                }
                return new Audience(this.rpc, this.rpc.createAudienceMember(type, x, y, back, flip, color, excitement));
            }
            async createAudience(seed, rematches) {
                if (!this.rpc) {
                    return [];
                }
                const a = this.rpc.createAudience(seed, rematches).map((a) => new Audience(this.rpc, a));
                for (let member of a) {
                    this.addAudienceMember(member);
                }
                return a;
            }
        }
        TheRoom.Stage = Stage;
        let PlayerFlags;
        (function (PlayerFlags) {
            PlayerFlags[PlayerFlags["Ant"] = 0] = "Ant";
            PlayerFlags[PlayerFlags["Bee"] = 1] = "Bee";
            PlayerFlags[PlayerFlags["Beefly"] = 2] = "Beefly";
            PlayerFlags[PlayerFlags["Beetle"] = 3] = "Beetle";
            PlayerFlags[PlayerFlags["Butterfly"] = 4] = "Butterfly";
            PlayerFlags[PlayerFlags["Cricket"] = 5] = "Cricket";
            PlayerFlags[PlayerFlags["Dragonfly"] = 6] = "Dragonfly";
            PlayerFlags[PlayerFlags["Firefly"] = 7] = "Firefly";
            PlayerFlags[PlayerFlags["Ladybug"] = 8] = "Ladybug";
            PlayerFlags[PlayerFlags["Mantis"] = 9] = "Mantis";
            PlayerFlags[PlayerFlags["Mosquito"] = 10] = "Mosquito";
            PlayerFlags[PlayerFlags["Moth"] = 11] = "Moth";
            PlayerFlags[PlayerFlags["Tangy"] = 12] = "Tangy";
            PlayerFlags[PlayerFlags["Wasp"] = 13] = "Wasp";
            PlayerFlags[PlayerFlags["NonAwakened"] = 14] = "NonAwakened";
            PlayerFlags[PlayerFlags["French"] = 15] = "French";
            PlayerFlags[PlayerFlags["Explorer"] = 16] = "Explorer";
            PlayerFlags[PlayerFlags["Snakemouth"] = 17] = "Snakemouth";
            PlayerFlags[PlayerFlags["Criminal"] = 18] = "Criminal";
            PlayerFlags[PlayerFlags["CardMaster"] = 19] = "CardMaster";
            PlayerFlags[PlayerFlags["MetalIsland"] = 20] = "MetalIsland";
            PlayerFlags[PlayerFlags["Developer"] = 21] = "Developer";
        })(PlayerFlags = TheRoom.PlayerFlags || (TheRoom.PlayerFlags = {}));
        function playerDisplayName(character) {
            return character.displayName || (character.name.substr(0, 1).toUpperCase() + character.name.substr(1));
        }
        TheRoom.playerDisplayName = playerDisplayName;
        class Player {
            constructor(num, name) {
                this.num = num;
                this.name = name;
            }
            setCharacter(character) {
                if (this.rpc) {
                    this.rpc.setCharacter(this.id, character.name);
                }
                this.name = character.name;
            }
            becomeAngry(ms = 2000) {
                if (this.rpc) {
                    this.rpc.becomeAngry(this.id, ms / 1000);
                }
            }
        }
        Player.characters = [
            {
                name: "amber",
                flags: [
                    PlayerFlags.Ant
                ]
            },
            {
                name: "aria",
                displayName: "Acolyte Aria",
                flags: [
                    PlayerFlags.Mantis
                ]
            },
            {
                name: "arie",
                flags: [
                    PlayerFlags.Butterfly,
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "astotheles",
                flags: [
                    PlayerFlags.Cricket,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "bomby",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "bu-gi",
                flags: [
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "carmina",
                flags: [
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "celia",
                flags: [
                    PlayerFlags.Ant,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "cenn",
                flags: [
                    PlayerFlags.Butterfly,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "cerise",
                flags: [
                    PlayerFlags.Tangy
                ]
            },
            {
                name: "chompy",
                flags: [
                    PlayerFlags.NonAwakened
                ]
            },
            {
                name: "chubee",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "chuck",
                flags: [
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "crisbee",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "crow",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "diana",
                flags: [
                    PlayerFlags.Ant
                ]
            },
            {
                name: "eremi",
                flags: [
                    PlayerFlags.Mantis
                ]
            },
            {
                name: "futes",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Developer
                ]
            },
            {
                name: "genow",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.Developer
                ]
            },
            {
                name: "honeycomb",
                displayName: "Professor Honeycomb",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "janet",
                flags: [
                    PlayerFlags.Ant,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "jaune",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.French
                ]
            },
            {
                name: "jayde",
                flags: [
                    PlayerFlags.Wasp
                ]
            },
            {
                name: "johnny",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "kabbu",
                flags: [
                    PlayerFlags.Beetle,
                    PlayerFlags.Explorer,
                    PlayerFlags.Snakemouth
                ]
            },
            {
                name: "kage",
                flags: [
                    PlayerFlags.Ladybug,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "kali",
                flags: [
                    PlayerFlags.Moth
                ]
            },
            {
                name: "kenny",
                flags: [
                    PlayerFlags.Beetle
                ]
            },
            {
                name: "kina",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "lanya",
                flags: [
                    PlayerFlags.Firefly
                ]
            },
            {
                name: "leif",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.Snakemouth,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "levi",
                flags: [
                    PlayerFlags.Ladybug,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "maki",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "malbee",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "mar",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Developer
                ]
            },
            {
                name: "mothiva",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "neolith",
                displayName: "Professor Neolith",
                flags: [
                    PlayerFlags.Moth
                ]
            },
            {
                name: "nero",
                flags: [
                    PlayerFlags.NonAwakened
                ]
            },
            {
                name: "pibu",
                flags: [
                    PlayerFlags.NonAwakened
                ]
            },
            {
                name: "pisci",
                flags: [
                    PlayerFlags.Beetle,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "ritchee",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "riz",
                flags: [
                    PlayerFlags.Dragonfly
                ]
            },
            {
                name: "samira",
                flags: [
                    PlayerFlags.Beefly
                ]
            },
            {
                name: "scarlet",
                displayName: "Monsieur Scarlet",
                flags: [
                    PlayerFlags.Ant,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "serene",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "shay",
                flags: [
                    PlayerFlags.Mosquito,
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "tanjerin",
                flags: [
                    PlayerFlags.Tangy
                ]
            },
            {
                name: "ultimax",
                displayName: "General Ultimax",
                flags: [
                    PlayerFlags.Wasp
                ]
            },
            {
                name: "vanessa",
                flags: [
                    PlayerFlags.Wasp
                ]
            },
            {
                name: "vi",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.Snakemouth,
                    PlayerFlags.Explorer,
                    PlayerFlags.French
                ]
            },
            {
                name: "yin",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "zaryant",
                flags: [
                    PlayerFlags.Ant
                ]
            },
            {
                name: "zasp",
                flags: [
                    PlayerFlags.Wasp,
                    PlayerFlags.Explorer
                ]
            }
        ];
        TheRoom.Player = Player;
        class Audience {
            constructor(rpc, id) {
                this.rpc = rpc;
                this.id = id;
            }
            startCheer(ms = 5000) {
                if (!this.rpc) {
                    return;
                }
                this.rpc.startCheer(this.id, ms / 1000);
            }
            isBack() {
                if (!this.rpc) {
                    return false;
                }
                return this.rpc.isAudienceBack(this.id);
            }
        }
        TheRoom.Audience = Audience;
    })(TheRoom = SpyCards.TheRoom || (SpyCards.TheRoom = {}));
})(SpyCards || (SpyCards = {}));
