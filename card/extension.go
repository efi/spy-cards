//go:generate stringer -type ExtensionType -trimprefix Extension

package card

import (
	"encoding/base32"
	"strings"

	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

// ExtensionType is an extension field type.
type ExtensionType uint64

// Constants for ExtensionType.
const (
	ExtensionSetStage ExtensionType = 0
	ExtensionSetMusic ExtensionType = 1
)

// Extension flags.
const (
	// ExtensionOverrideCustom (SetStage, SetMusic): override even if a custom
	// override is already active.
	ExtensionOverrideCustom uint64 = 1 << 0
)

// Extension is an extension field that allows cards to have meta-game effects.
type Extension struct {
	Type      ExtensionType
	Flags     uint64
	CID       ContentIdentifier
	LoopStart float32
	LoopEnd   float32
	unparsed  []byte
}

// Marshal encodes the extension field.
func (e *Extension) Marshal(w *format.Writer) error {
	switch e.Type {
	case ExtensionSetStage:
		w.UVarInt(e.Flags)
		w.UVarInt(uint64(len(e.CID)))
		w.Bytes([]byte(e.CID))
	case ExtensionSetMusic:
		w.UVarInt(e.Flags)
		w.UVarInt(uint64(len(e.CID)))
		w.Bytes([]byte(e.CID))
		w.Write(&e.LoopStart)
		w.Write(&e.LoopEnd)
	default:
		return xerrors.Errorf("card: unhandled extension type: %v", e.Type)
	}

	w.Write(e.unparsed)

	return nil
}

// Unmarshal decodes the extension field.
func (e *Extension) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	switch e.Type {
	case ExtensionSetStage:
		e.Flags = r.UVarInt()
		e.CID = r.Bytes(int(r.UVarInt()))
	case ExtensionSetMusic:
		e.Flags = r.UVarInt()
		e.CID = r.Bytes(int(r.UVarInt()))
		r.Read(&e.LoopStart)
		r.Read(&e.LoopEnd)
	default:
		return xerrors.Errorf("card: unhandled extension type: %v", e.Type)
	}

	if r.Len() != 0 {
		e.unparsed = r.Bytes(r.Len())

		return xerrors.Errorf("card: %d extra bytes in extension field", len(e.unparsed))
	}

	e.unparsed = nil

	return nil
}

// ContentIdentifier is an IPFS CID.
type ContentIdentifier []byte

func (cid ContentIdentifier) String() string {
	encoding := base32.StdEncoding.WithPadding(base32.NoPadding)

	return "b" + strings.ToLower(encoding.EncodeToString([]byte(cid)))
}
