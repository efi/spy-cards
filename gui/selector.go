package gui

type Option struct {
	opt     int
	optName string
}

func Options(ops ...string) []Option {
	var r []Option
	for i, o := range ops {
		r = append(r, Option{i, o})
	}
	return r
}

type Selector struct {
	Panel
	left     *Button
	right    *Button
	label    *Label
	Selected int
	options  []Option
}

func NewSelector(ops []Option) *Selector {
	s := &Selector{
		Panel:   *NewPanel(),
		left:    NewButton("<"),
		right:   NewButton(">"),
		options: ops,
		label:   NewLabel(ops[0].optName),
	}
	s.left.Action = func(e Event) {
		s.sel(-1)
	}
	s.right.Action = func(e Event) {
		s.sel(1)
	}
	s.Panel.Control.AddChild(&s.left.Control)
	s.Panel.Control.AddChild(&s.label.Control)
	s.Panel.Control.AddChild(&s.right.Control)
	s.node = s
	return s
}

func (s *Selector) sel(i int) {
	l := len(s.options)
	if l <= 1 {
		return
	}
	if s.Selected == 0 && i < 0 {
		s.Selected = l + i
	} else if s.Selected == l && i > 0 {
		s.Selected = i - 1
	} else {
		s.Selected += i
	}
	if s.Selected >= l || s.Selected < 0 {
		s.Selected = 0
	}
}
