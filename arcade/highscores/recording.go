package highscores

import (
	"context"
	"io/ioutil"
	"net/http"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
)

// RecordingViewer displays a game recording.
type RecordingViewer struct {
	Code string
}

// Run plays the recording.
func (v *RecordingViewer) Run(ctx context.Context) error {
	rec, err := FetchRecording(ctx, v.Code)
	if err != nil {
		return err
	}

	var create game.CreateFunc

	switch rec.Game {
	case arcade.MiteKnight:
		create = miteknight.New(*rec.Rules.(*arcade.MiteKnightRules))
	case arcade.FlowerJourney:
		create = flowerjourney.New(*rec.Rules.(*arcade.FlowerJourneyRules))
	default:
		return xerrors.Errorf("highscores: recording viewer unhandled game %v", rec.Game)
	}

	_, err = game.PlayRecording(ctx, func(s *game.State) game.Interface {
		v.pushListener(s)

		return create(s)
	}, rec)

	v.popListener()

	if err != nil {
		return xerrors.Errorf("highscores: playing recording: %w", err)
	}

	return nil
}

// FetchRecording downloads a high scores recording.
func FetchRecording(ctx context.Context, code string) (*arcade.Recording, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, internal.GetConfig(ctx).ArcadeAPIBaseURL+"get/"+code, nil)
	if err != nil {
		return nil, xerrors.Errorf("highscores: fetching recording: %w", err)
	}

	req = internal.ModifyRequest(req)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, xerrors.Errorf("highscores: fetching recording: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, xerrors.Errorf("highscores: fetch recording: server returned %q", resp.Status)
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, xerrors.Errorf("highscores: fetching recording: %w", err)
	}

	var rec arcade.Recording

	err = rec.UnmarshalBinary(b)

	if err != nil {
		return nil, xerrors.Errorf("highscores: decoding fetched recording: %w", err)
	}

	return &rec, nil
}
