(async function () {
	const params = new URLSearchParams(location.search);

	if (params.get("custom") === "editor") {
		SpyCards.Native.run("-run=CardEditor", "-cards=" + location.hash.substr(1));
	} else {
		const variant = parseInt(params.get("variant"), 10) || 0;

		const args: string[] = ["-run=SpyCards", "-variant=" + variant, "-recording=" + (params.get("recording") || "")];

		if (params.has("custom")) {
			args.push("-mode=custom", "-cards=" + location.hash.substr(1));
		} else if (params.has("mode")) {
			const ver = parseInt(params.get("ver"), 10) || 0;

			args.push("-mode=" + params.get("mode") + "." + ver);
		}

		SpyCards.Native.run(...args);
	}
})();
