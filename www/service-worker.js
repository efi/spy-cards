"use strict";
const cacheHash = "5d9d8bc0fc71e23c6dd246b54f5c637742f816bfca425f8b05f295e2d8163386";
self.importScripts("/script/config.js", "/script/sc-base32.js", "https://spy-cards.lubar.me/spy-cards/user-img/whitelist.js");
self.addEventListener("install", (e) => {
    self.skipWaiting();
    e.waitUntil(installCache());
});
self.addEventListener("activate", (e) => {
    e.waitUntil(Promise.all([
        caches.keys().then((names) => {
            return Promise.all(names.filter((name) => {
                return name !== "spy-cards-" + cacheHash && name !== "spy-cards-static-v0" && !name.startsWith("spy-cards-user-");
            }).map((name) => {
                return caches.delete(name);
            }));
        }),
        self.clients.claim()
    ]));
});
self.addEventListener("fetch", (e) => {
    e.respondWith(caches.match(e.request).then(async (resp) => {
        if (resp) {
            // have cached file
            return resp;
        }
        if (e.request.url.indexOf(".html?") !== -1 || e.request.url.indexOf("/?") !== -1) {
            // HTML pages aren't generated server-side, so query parameters can be ignored
            resp = await caches.match(e.request.url.split("?")[0]);
            if (resp) {
                return resp;
            }
        }
        // fetch from network
        resp = await fetch(e.request);
        if (e.request.url.startsWith(custom_card_api_base_url)) {
            // TODO: cache specific revisions?
            return resp;
        }
        if (e.request.url.startsWith(arcade_api_base_url)) {
            // TODO: cache recordings?
            return resp;
        }
        if (e.request.url.startsWith(match_recording_base_url)) {
            // TODO: cache recordings?
            return resp;
        }
        if (e.request.url.startsWith(user_image_base_url)) {
            if (e.request.url.endsWith(".png")) {
                const clonedResp = resp.clone();
                caches.open("spy-cards-user-img").then((cache) => cache.put(e.request, clonedResp));
            }
            return resp;
        }
        console.warn("Uncached request: ", e.request.url);
        return resp;
    }));
});
async function installCache() {
    const cacheDataResp = await fetch("/cache-data.txt?" + cacheHash);
    const cacheDataText = await cacheDataResp.text();
    const staticCacheURLs = [];
    const cacheURLs = ["/"];
    for (let line of cacheDataText.split("\n")) {
        if (!line) {
            continue;
        }
        const [hash, name] = line.split("  ");
        const hashBytes = new Uint8Array(hash.split(/(?=(?:..)*$)/g).map((b) => parseInt(b, 16)));
        if (name.endsWith(".ico") || name.endsWith(".opus") || name.endsWith(".woff") || name.endsWith(".woff2") || name.endsWith(".ttf")) {
            // these change much less often, if ever
            staticCacheURLs.push(new Request(name, {
                integrity: "sha256-" + Base64.encode(hashBytes)
            }));
        }
        else if (name.endsWith(".html") || name.endsWith(".svg") || name.endsWith(".webmanifest") || name.endsWith(".wasm") || name.endsWith(".png") || name.endsWith(".stage")) {
            cacheURLs.push(new Request(name, {
                integrity: "sha256-" + Base64.encode(hashBytes)
            }));
        }
        else if ((name.startsWith("script/sc-") && name !== "script/sc-error-handler.js") || name === "service-worker.js") {
            // ignore
        }
        else if (name.startsWith("script/") || name.startsWith("style/")) {
            cacheURLs.push(new Request(name + "?" + hash.substr(0, 8), {
                integrity: "sha256-" + Base64.encode(hashBytes)
            }));
        }
        else if (name === "service-worker-status") {
            // skip; added manually with different content
        }
        else if (name.endsWith(".md") || name.endsWith(".txt") || name.startsWith("docs/") || name.endsWith(".br") || name.endsWith(".gz")) {
            // skip; not used by game
        }
        else {
            console.error("unclassified URL:", name);
            debugger;
        }
    }
    await Promise.all([
        self.caches.open("spy-cards-static-v0").then((staticCache) => Promise.all(staticCacheURLs.map((url) => staticCache.match(url).then((existing) => existing ? Promise.resolve() : staticCache.add(url))))),
        self.caches.open("spy-cards-" + cacheHash).then((cache) => Promise.all([
            cache.addAll(cacheURLs),
            cache.put("service-worker-status", new Response("service worker active with cache hash " + cacheHash, {
                headers: [
                    ["Content-Type", "text/plain; charset=utf-8"]
                ]
            }))
        ])),
        self.caches.open("spy-cards-user-img").then((userImageCache) => Promise.all(user_images_whitelist.map((id) => user_image_base_url + id + ".png").map((url) => userImageCache.match(url).then((existing) => existing ? Promise.resolve() : userImageCache.add(url))))),
    ]);
}
self.addEventListener("message", async (e) => {
    switch (e.data.type) {
        case "settings-changed":
            for (let client of await self.clients.matchAll()) {
                if (client.id !== e.source.id) {
                    client.postMessage({ type: "settings-changed" });
                }
            }
            break;
        default:
            debugger;
            break;
    }
});
