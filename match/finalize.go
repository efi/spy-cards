package match

import (
	"log"

	"git.lubar.me/ben/spy-cards/card"
	"golang.org/x/xerrors"
)

// Finalize verifies a match and returns a recording of the match.
func (m *Match) Finalize(fn func(func(card.Deck, card.Deck, card.UnknownDeck, []byte, []byte, []byte) error, func([]byte, []byte, *card.TurnData) error) error) (*card.Recording, error) {
	var rec card.Recording

	rec.FormatVersion = 2
	rec.Version = m.Init.Version
	rec.ModeName = m.Init.Mode
	rec.Perspective = m.Perspective
	rec.Cosmetic = m.Cosmetic
	rec.RematchCount = m.Rematches
	rec.Start = m.Start
	rec.SpoilerGuard = m.Set.SpoilerGuard

	if m.Perspective == 2 {
		rec.SpoilerGuard[0], rec.SpoilerGuard[1] = rec.SpoilerGuard[1], rec.SpoilerGuard[0]
	}

	return &rec, fn(func(myDeck, theirDeck card.Deck, backs card.UnknownDeck, sharedSeed, localSeed, remoteSeed []byte) error {
		copy(rec.SharedSeed[:], sharedSeed)
		copy(rec.PrivateSeed[m.Perspective-1][:], localSeed)
		copy(rec.PrivateSeed[2-m.Perspective][:], remoteSeed)
		rec.CustomCards = *m.Set
		rec.CustomCardsRaw = m.Init.Custom
		rec.InitialDeck[m.Perspective-1] = myDeck
		rec.InitialDeck[2-m.Perspective] = theirDeck
		rec.Rounds = make([]card.RecordingRound, 0, m.State.Round)

		if len(theirDeck) != len(backs) {
			return xerrors.Errorf("opponent's deck of %d cards cannot match %d card backs", len(theirDeck), len(backs))
		}

		for i, b := range backs {
			c := m.Set.Card(theirDeck[i])

			if real := c.Rank.Back(); b != real {
				return xerrors.Errorf("card %d of %d in opponent's deck (%d %q) has back %v, not %v", i+1, len(theirDeck), uint64(c.ID), c.DisplayName(), real, b)
			}
		}

		for player, deck := range rec.InitialDeck {
			if err := deck.Validate(m.Set); err != nil {
				return xerrors.Errorf("player %d deck failed validation: %w", player+1, err)
			}
		}

		return nil
	}, func(seed, seed2 []byte, turn *card.TurnData) error {
		var round card.RecordingRound

		copy(round.TurnSeed[:], seed)
		copy(round.TurnSeed2[:], seed2)
		round.Ready = turn.Ready

		rec.Rounds = append(rec.Rounds, round)

		log.Println("TODO: validate turn") // TODO

		return nil
	})
}
