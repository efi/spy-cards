// +build !headless

package sprites

import (
	"runtime"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
)

// Render draws the sprite batch to the screen.
func (sb *Batch) Render() {
	if sb.count == 0 {
		return
	}

	gfx.Lock.Lock()

	if !sb.haveBuf {
		sb.dataBuf = gfx.GL.CreateBuffer()
		sb.elemBuf = gfx.GL.CreateBuffer()
		sb.haveBuf = true
		sb.dirtyBuf = true

		runtime.SetFinalizer(sb, (*Batch).dropBuffers)
	}

	if sb.dirtyBuf {
		gfx.GL.BindBuffer(gl.ARRAY_BUFFER, sb.dataBuf)
		gfx.GL.BufferData(gl.ARRAY_BUFFER, sb.data, gl.DYNAMIC_DRAW)

		gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, sb.elemBuf)
		gfx.GL.BufferData(gl.ELEMENT_ARRAY_BUFFER, sb.elem, gl.DYNAMIC_DRAW)

		sb.dirtyBuf = false
	}

	if sb.fastRender() {
		gfx.Lock.Unlock()

		runtime.KeepAlive(sb)

		return
	}

	gfx.GL.UseProgram(spriteProgram.Program)

	gfx.GL.UniformMatrix4fv(spritePerspective.Uniform, sb.camMatrix[0][:])
	gfx.GL.UniformMatrix4fv(spriteCamera.Uniform, sb.camMatrix[1][:])

	for i := range sb.texture {
		gfx.GL.ActiveTexture(gl.TEXTURE0 + gl.Enum(i))

		if sb.texture[i] != nil {
			gfx.GL.BindTexture(gl.TEXTURE_2D, sb.texture[i].LazyTexture(Blank.Texture()))
		} else {
			gfx.GL.BindTexture(gl.TEXTURE_2D, Blank.Texture())
		}
	}

	gfx.GL.ActiveTexture(gl.TEXTURE0)

	gfx.GL.BindBuffer(gl.ARRAY_BUFFER, sb.dataBuf)

	gfx.GL.VertexAttribPointer(spritePolygon.Attrib, 4, gl.UNSIGNED_BYTE, false, 72, 0)
	gfx.GL.VertexAttribPointer(spriteMinsMaxs.Attrib, 4, gl.FLOAT, false, 72, 4)
	gfx.GL.VertexAttribPointer(spriteTexMinsMaxs.Attrib, 4, gl.FLOAT, false, 72, 20)
	gfx.GL.VertexAttribPointer(spritePosition.Attrib, 3, gl.FLOAT, false, 72, 36)
	gfx.GL.VertexAttribPointer(spriteRotation.Attrib, 3, gl.FLOAT, false, 72, 48)
	gfx.GL.VertexAttribPointer(spriteBlur.Attrib, 1, gl.FLOAT, false, 72, 60)
	gfx.GL.VertexAttribPointer(spriteFlags.Attrib, 1, gl.UNSIGNED_SHORT, false, 72, 64)
	gfx.GL.VertexAttribPointer(spriteTex.Attrib, 1, gl.UNSIGNED_SHORT, false, 72, 66)
	gfx.GL.VertexAttribPointer(spriteColor.Attrib, 4, gl.UNSIGNED_BYTE, true, 72, 68)

	gfx.GL.EnableVertexAttribArray(spritePolygon.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteMinsMaxs.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteTexMinsMaxs.Attrib)
	gfx.GL.EnableVertexAttribArray(spritePosition.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteRotation.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteBlur.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteFlags.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteColor.Attrib)
	gfx.GL.EnableVertexAttribArray(spriteTex.Attrib)

	gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, sb.elemBuf)

	gfx.GL.Uniform1f(spriteTime.Uniform, float32(time.Since(startTime).Seconds()))

	gfx.GL.DrawElements(gl.TRIANGLES, sb.count, gl.UNSIGNED_SHORT, 0)

	gfx.GL.DisableVertexAttribArray(spritePolygon.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteMinsMaxs.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteTexMinsMaxs.Attrib)
	gfx.GL.DisableVertexAttribArray(spritePosition.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteRotation.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteBlur.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteFlags.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteColor.Attrib)
	gfx.GL.DisableVertexAttribArray(spriteTex.Attrib)

	gfx.Lock.Unlock()

	runtime.KeepAlive(sb)
}

func (sb *Batch) dropBuffers() {
	if sb.haveBuf {
		gfx.GL.DeleteBuffer(sb.dataBuf)
		gfx.GL.DeleteBuffer(sb.elemBuf)
		sb.haveBuf = false
		runtime.SetFinalizer(sb, nil)
	}
}
