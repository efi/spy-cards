//go:generate stringer -type CharacterFlag -trimprefix Flag

package room

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// CharacterFlag is a property of a Character.
type CharacterFlag uint

// Character flags.
const (
	FlagAnt CharacterFlag = iota
	FlagBee
	FlagBeefly
	FlagBeetle
	FlagButterfly
	FlagCricket
	FlagDragonfly
	FlagFirefly
	FlagLadybug
	FlagMantis
	FlagMosquito
	FlagMoth
	FlagTangy
	FlagWasp
	FlagNonAwakened
	FlagFrench
	FlagExplorer
	FlagSnakemouth
	FlagCriminal
	FlagCardMaster
	FlagMetalIsland
	FlagDeveloper
	FlagShe
	FlagHe
)

// Character is a playable character.
type Character struct {
	Name        string
	DisplayName string
	Sprites     [3]*sprites.Sprite
	Flags       []CharacterFlag
	PortraitDx  float32
	PortraitDy  float32
	PortraitSz  float32
	CropTop     bool
	CropFront   bool
	Hidden      bool
}

// Portrait returns a cropped version of the character sprite.
func (c *Character) Portrait() *sprites.Sprite {
	s := *c.Sprites[0]

	size := 1 + c.PortraitSz
	texSize := size * 100 / 2048 / 2

	centerS := (s.S0+s.S1)/2 + c.PortraitDx/2048
	centerT := s.T0 + texSize + c.PortraitDy/2048

	if s0 := centerS - texSize; c.CropFront && s0 > s.S0 {
		s.S0 = s0
		s.X0 = -0.5
	} else {
		s.X0 = 0.5 * (s.S0 - centerS) / texSize
	}

	if s1 := centerS + texSize; s1 < s.S1 {
		s.S1 = s1
		s.X1 = 0.5
	} else {
		s.X1 = 0.5 * (s.S1 - centerS) / texSize
	}

	if t0 := centerT - texSize; c.CropTop && t0 > s.T0 {
		s.T0 = t0
		s.Y1 = 0.5
	} else {
		s.Y1 = 0.5 * (centerT - s.T0) / texSize
	}

	if t1 := centerT + texSize; t1 < s.T1 {
		s.T1 = t1
		s.Y0 = -0.5
	} else {
		s.Y0 = 0.5 * (centerT - s.T1) / texSize
	}

	return &s
}

// CharacterByName is a convenience lookup map.
var CharacterByName = func() map[string]*Character {
	m := make(map[string]*Character, len(PlayerCharacters))

	for _, c := range PlayerCharacters {
		m[c.Name] = c
	}

	return m
}()

// PlayerCharacters are the available player characters.
var PlayerCharacters = []*Character{
	{
		Name:        "amber",
		DisplayName: "Amber",
		Sprites: [3]*sprites.Sprite{
			sprites.Amber1,
			sprites.Amber2,
			sprites.Ambera,
		},
		Flags: []CharacterFlag{
			FlagAnt,
			FlagShe,
		},
		PortraitDx: -55,
		PortraitDy: 80,
		CropTop:    true,
	},
	{
		Name:        "aria",
		DisplayName: "Acolyte Aria",
		Sprites: [3]*sprites.Sprite{
			sprites.Aria1,
			sprites.Aria2,
			sprites.Ariaa,
		},
		Flags: []CharacterFlag{
			FlagMantis,
			FlagShe,
		},
	},
	{
		Name:        "arie",
		DisplayName: "Arie",
		Sprites: [3]*sprites.Sprite{
			sprites.Arie1,
			sprites.Arie2,
			sprites.Ariea,
		},
		Flags: []CharacterFlag{
			FlagButterfly,
			FlagCardMaster,
			FlagShe,
		},
		PortraitDx: -16,
	},
	{
		Name:        "astotheles",
		DisplayName: "Astotheles",
		Sprites: [3]*sprites.Sprite{
			sprites.Astotheles1,
			sprites.Astotheles2,
			sprites.Astothelesa,
		},
		Flags: []CharacterFlag{
			FlagCricket,
			FlagCriminal,
			FlagHe,
		},
		PortraitDy: 24,
	},
	{
		Name:        "bomby",
		DisplayName: "Bomby",
		Sprites: [3]*sprites.Sprite{
			sprites.Bomby1,
			sprites.Bomby2,
			sprites.Bombya,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagHe,
		},
		PortraitDx: -4,
		PortraitDy: 12,
	},
	{
		Name:        "bu-gi",
		DisplayName: "Bu-Gi",
		Sprites: [3]*sprites.Sprite{
			sprites.BuGi1,
			sprites.BuGi2,
			sprites.BuGia,
		},
		Flags: []CharacterFlag{
			FlagMetalIsland,
			FlagHe,
		},
		PortraitDx: 30,
		PortraitDy: 70,
		CropFront:  true,
	},
	{
		Name:        "carmina",
		DisplayName: "Carmina",
		Sprites: [3]*sprites.Sprite{
			sprites.Carmina1,
			sprites.Carmina2,
			sprites.Carminaa,
		},
		Flags: []CharacterFlag{
			FlagMetalIsland,
			FlagShe,
		},
		PortraitDx: -8,
		PortraitDy: 20,
	},
	{
		Name:        "celia",
		DisplayName: "Celia",
		Sprites: [3]*sprites.Sprite{
			sprites.Celia1,
			sprites.Celia2,
			sprites.Celiaa,
		},
		Flags: []CharacterFlag{
			FlagAnt,
			FlagExplorer,
			FlagShe,
		},
	},
	{
		Name:        "cenn",
		DisplayName: "Cenn",
		Sprites: [3]*sprites.Sprite{
			sprites.Cenn1,
			sprites.Cenn2,
			sprites.Cenna,
		},
		Flags: []CharacterFlag{
			FlagButterfly,
			FlagCriminal,
			FlagHe,
		},
		PortraitDx: -30,
	},
	{
		Name:        "cerise",
		DisplayName: "Cerise",
		Sprites: [3]*sprites.Sprite{
			sprites.Cerise1,
			sprites.Cerise2,
			sprites.Cerisea,
		},
		Flags: []CharacterFlag{
			FlagTangy,
			FlagShe,
		},
		PortraitDx: -10,
		PortraitDy: 10,
		PortraitSz: -0.25,
	},
	{
		Name:        "chompy",
		DisplayName: "Chompy",
		Sprites: [3]*sprites.Sprite{
			sprites.Chompy1,
			sprites.Chompy2,
			sprites.Chompya,
		},
		Flags: []CharacterFlag{
			FlagNonAwakened,
			FlagShe,
		},
	},
	{
		Name:        "chubee",
		DisplayName: "Chubee",
		Sprites: [3]*sprites.Sprite{
			sprites.Chubee1,
			sprites.Chubee2,
			sprites.Chubeea,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagShe,
		},
		PortraitDx: -25,
		PortraitDy: 70,
	},
	{
		Name:        "chuck",
		DisplayName: "Chuck",
		Sprites: [3]*sprites.Sprite{
			sprites.Chuck1,
			sprites.Chuck2,
			sprites.Chucka,
		},
		Flags: []CharacterFlag{
			FlagCardMaster,
			FlagHe,
		},
		PortraitDx: -10,
	},
	{
		Name:        "crisbee",
		DisplayName: "Crisbee",
		Sprites: [3]*sprites.Sprite{
			sprites.Crisbee1,
			sprites.Crisbee2,
			sprites.Crisbeea,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagHe,
		},
		PortraitSz: 0.25,
		PortraitDx: -10,
	},
	{
		Name:        "crow",
		DisplayName: "Crow",
		Sprites: [3]*sprites.Sprite{
			sprites.Crow1,
			sprites.Crow2,
			sprites.Crowa,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagCardMaster,
			FlagShe,
		},
		PortraitDy: 10,
	},
	{
		Name:        "diana",
		DisplayName: "Diana",
		Sprites: [3]*sprites.Sprite{
			sprites.Diana1,
			sprites.Diana2,
			sprites.Dianaa,
		},
		Flags: []CharacterFlag{
			FlagAnt,
			FlagShe,
		},
		PortraitDx: -10,
		PortraitSz: 0.25,
	},
	{
		Name:        "eremi",
		DisplayName: "Eremi",
		Sprites: [3]*sprites.Sprite{
			sprites.Eremi1,
			sprites.Eremi2,
			sprites.Eremia,
		},
		Flags: []CharacterFlag{
			FlagMantis,
			FlagShe,
		},
		PortraitDy: 20,
		PortraitSz: 0.125,
	},
	{
		Name:        "futes",
		DisplayName: "Futes",
		Sprites: [3]*sprites.Sprite{
			sprites.Futes1,
			sprites.Futes2,
			sprites.Futesa,
		},
		Flags: []CharacterFlag{
			FlagMantis,
			FlagDeveloper,
			FlagShe,
		},
		PortraitDy: 20,
	},
	{
		Name:        "genow",
		DisplayName: "Genow",
		Sprites: [3]*sprites.Sprite{
			sprites.Genow1,
			sprites.Genow2,
			sprites.Genowa,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagDeveloper,
			FlagHe,
		},
	},
	{
		Name:        "honeycomb",
		DisplayName: "Professor Honeycomb",
		Sprites: [3]*sprites.Sprite{
			sprites.Honeycomb1,
			sprites.Honeycomb2,
			sprites.Honeycomba,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagShe,
		},
		PortraitDx: -20,
		PortraitDy: 20,
	},
	{
		Name:        "janet",
		DisplayName: "Janet",
		Sprites: [3]*sprites.Sprite{
			sprites.Janet1,
			sprites.Janet2,
			sprites.Janeta,
		},
		Flags: []CharacterFlag{
			FlagAnt,
			FlagMetalIsland,
			FlagShe,
		},
		PortraitDy: 20,
	},
	{
		Name:        "jaune",
		DisplayName: "Jaune",
		Sprites: [3]*sprites.Sprite{
			sprites.Jaune1,
			sprites.Jaune2,
			sprites.Jaunea,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagFrench,
			FlagShe,
		},
		PortraitDx: -10,
		PortraitDy: 15,
	},
	{
		Name:        "jayde",
		DisplayName: "Jayde",
		Sprites: [3]*sprites.Sprite{
			sprites.Jayde1,
			sprites.Jayde2,
			sprites.Jaydea,
		},
		Flags: []CharacterFlag{
			FlagWasp,
			FlagShe,
		},
		PortraitDx: 25,
		PortraitDy: 30,
	},
	{
		Name:        "johnny",
		DisplayName: "Johnny",
		Sprites: [3]*sprites.Sprite{
			sprites.Johnny1,
			sprites.Johnny2,
			sprites.Johnnya,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagMetalIsland,
			FlagHe,
		},
		PortraitDx: -5,
		PortraitDy: 60,
	},
	{
		Name:        "kabbu",
		DisplayName: "Kabbu",
		Sprites: [3]*sprites.Sprite{
			sprites.Kabbu1,
			sprites.Kabbu2,
			sprites.Kabbua,
		},
		Flags: []CharacterFlag{
			FlagBeetle,
			FlagExplorer,
			FlagSnakemouth,
			FlagHe,
		},
		PortraitDy: 20,
		PortraitSz: 0.25,
	},
	{
		Name:        "kage",
		DisplayName: "Kage",
		Sprites: [3]*sprites.Sprite{
			sprites.Kage1,
			sprites.Kage2,
			sprites.Kagea,
		},
		Flags: []CharacterFlag{
			FlagLadybug,
			FlagMetalIsland,
			FlagHe,
		},
		PortraitSz: 0.125,
	},
	{
		Name:        "kali",
		DisplayName: "Kali",
		Sprites: [3]*sprites.Sprite{
			sprites.Kali1,
			sprites.Kali2,
			sprites.Kalia,
		},
		Flags: []CharacterFlag{
			FlagMoth,
			FlagShe,
		},
		PortraitDy: 40,
	},
	{
		Name:        "kenny",
		DisplayName: "Kenny",
		Sprites: [3]*sprites.Sprite{
			sprites.Kenny1,
			sprites.Kenny2,
			sprites.Kennya,
		},
		Flags: []CharacterFlag{
			FlagBeetle,
			FlagHe,
		},
		PortraitDx: -10,
		PortraitDy: 20,
		PortraitSz: 0.25,
	},
	{
		Name:        "kina",
		DisplayName: "Kina",
		Sprites: [3]*sprites.Sprite{
			sprites.Kina1,
			sprites.Kina2,
			sprites.Kinaa,
		},
		Flags: []CharacterFlag{
			FlagMantis,
			FlagExplorer,
			FlagShe,
		},
		PortraitDy: 20,
	},
	{
		Name:        "lanya",
		DisplayName: "Lanya",
		Sprites: [3]*sprites.Sprite{
			sprites.Lanya1,
			sprites.Lanya2,
			sprites.Lanyaa,
		},
		Flags: []CharacterFlag{
			FlagFirefly,
			FlagShe,
		},
	},
	{
		Name:        "leif",
		DisplayName: "Leif",
		Sprites: [3]*sprites.Sprite{
			sprites.Leif1,
			sprites.Leif2,
			sprites.Leifa,
		},
		Flags: []CharacterFlag{
			FlagMoth,
			FlagSnakemouth,
			FlagExplorer,
			FlagHe,
		},
		PortraitSz: -0.125,
	},
	{
		Name:        "levi",
		DisplayName: "Levi",
		Sprites: [3]*sprites.Sprite{
			sprites.Levi1,
			sprites.Levi2,
			sprites.Levia,
		},
		Flags: []CharacterFlag{
			FlagLadybug,
			FlagExplorer,
			FlagHe,
		},
		PortraitDy: 10,
	},
	{
		Name:        "maki",
		DisplayName: "Maki",
		Sprites: [3]*sprites.Sprite{
			sprites.Maki1,
			sprites.Maki2,
			sprites.Makia,
		},
		Flags: []CharacterFlag{
			FlagMantis,
			FlagExplorer,
			FlagHe,
		},
	},
	{
		Name:        "malbee",
		DisplayName: "Malbee",
		Sprites: [3]*sprites.Sprite{
			sprites.Malbee1,
			sprites.Malbee2,
			sprites.Malbeea,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagShe,
		},
		PortraitDx: -15,
		PortraitDy: 20,
		PortraitSz: 0.25,
	},
	{
		Name:        "mar",
		DisplayName: "Mar",
		Sprites: [3]*sprites.Sprite{
			sprites.Mar1,
			sprites.Mar2,
			sprites.Mara,
		},
		Flags: []CharacterFlag{
			FlagMantis,
			FlagDeveloper,
			FlagHe,
		},
		PortraitDx: -20,
		PortraitDy: 10,
	},
	{
		Name:        "mothiva",
		DisplayName: "Mothiva",
		Sprites: [3]*sprites.Sprite{
			sprites.Mothiva1,
			sprites.Mothiva2,
			sprites.Mothivaa,
		},
		Flags: []CharacterFlag{
			FlagMoth,
			FlagExplorer,
			FlagShe,
		},
		PortraitSz: -0.125,
	},
	{
		Name:        "neolith",
		DisplayName: "Professor Neolith",
		Sprites: [3]*sprites.Sprite{
			sprites.Neolith1,
			sprites.Neolith2,
			sprites.Neolitha,
		},
		Flags: []CharacterFlag{
			FlagMoth,
			FlagHe,
		},
		PortraitDy: 20,
	},
	{
		Name:        "nero",
		DisplayName: "Nero",
		Sprites: [3]*sprites.Sprite{
			sprites.Nero1,
			sprites.Nero2,
			sprites.Neroa,
		},
		Flags: []CharacterFlag{
			FlagNonAwakened,
			FlagHe,
		},
		PortraitDy: -10,
	},
	{
		Name:        "pibu",
		DisplayName: "Pibu",
		Sprites: [3]*sprites.Sprite{
			sprites.Pibu1,
			sprites.Pibu2,
			sprites.Pibua,
		},
		Flags: []CharacterFlag{
			FlagNonAwakened,
		},
		PortraitDy: -35,
	},
	{
		Name:        "pisci",
		DisplayName: "Pisci",
		Sprites: [3]*sprites.Sprite{
			sprites.Pisci1,
			sprites.Pisci2,
			sprites.Piscia,
		},
		Flags: []CharacterFlag{
			FlagBeetle,
			FlagCriminal,
			FlagShe,
		},
		PortraitDx: -10,
		PortraitSz: 0.125,
	},
	{
		Name:        "ritchee",
		DisplayName: "Ritchee",
		Sprites: [3]*sprites.Sprite{
			sprites.Ritchee1,
			sprites.Ritchee2,
			sprites.Ritcheea,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagMetalIsland,
			FlagHe,
		},
		PortraitDy: 40,
	},
	{
		Name:        "riz",
		DisplayName: "Riz",
		Sprites: [3]*sprites.Sprite{
			sprites.Riz1,
			sprites.Riz2,
			sprites.Riza,
		},
		Flags: []CharacterFlag{
			FlagDragonfly,
			FlagHe,
		},
		CropFront: true,
	},
	{
		Name:        "samira",
		DisplayName: "Samira",
		Sprites: [3]*sprites.Sprite{
			sprites.Samira1,
			sprites.Samira2,
			sprites.Samiraa,
		},
		Flags: []CharacterFlag{
			FlagBeefly,
			FlagShe,
		},
	},
	{
		Name:        "scarlet",
		DisplayName: "Monsieur Scarlet",
		Sprites: [3]*sprites.Sprite{
			sprites.Scarlet1,
			sprites.Scarlet2,
			sprites.Scarleta,
		},
		Flags: []CharacterFlag{
			FlagAnt,
			FlagCriminal,
			FlagHe,
		},
	},
	{
		Name:        "serene",
		DisplayName: "Serene",
		Sprites: [3]*sprites.Sprite{
			sprites.Serene1,
			sprites.Serene2,
			sprites.Serenea,
		},
		Flags: []CharacterFlag{
			FlagMoth,
			FlagMetalIsland,
			FlagShe,
		},
		PortraitDy: 20,
	},
	{
		Name:        "shay",
		DisplayName: "Shay",
		Sprites: [3]*sprites.Sprite{
			sprites.Shay1,
			sprites.Shay2,
			sprites.Shaya,
		},
		Flags: []CharacterFlag{
			FlagMosquito,
			FlagCardMaster,
			FlagHe,
		},
		PortraitDx: -10,
		PortraitDy: 25,
		PortraitSz: 0.125,
	},
	{
		Name:        "skirby",
		DisplayName: "Skirby",
		Sprites: [3]*sprites.Sprite{
			sprites.Skirby1,
			sprites.Skirby2,
			sprites.Skirbya,
		},
		Flags: []CharacterFlag{
			FlagTangy,
			FlagHe,
		},
		PortraitSz: 0.25,
		PortraitDy: 20,
	},
	{
		Name:        "tanjerin",
		DisplayName: "Tanjerin",
		Sprites: [3]*sprites.Sprite{
			sprites.Tanjerin1,
			sprites.Tanjerin2,
			sprites.Tanjerina,
		},
		Flags: []CharacterFlag{
			FlagTangy,
			FlagHe,
		},
		PortraitDx: -12,
		PortraitDy: 70,
		PortraitSz: -0.375,
	},
	{
		Name:        "ultimax",
		DisplayName: "General Ultimax",
		Sprites: [3]*sprites.Sprite{
			sprites.Ultimax1,
			sprites.Ultimax2,
			sprites.Ultimaxa,
		},
		Flags: []CharacterFlag{
			FlagWasp,
			FlagHe,
		},
		PortraitDx: -30,
		PortraitSz: 0.25,
	},
	{
		Name:        "vanessa",
		DisplayName: "Vanessa",
		Sprites: [3]*sprites.Sprite{
			sprites.Vanessa1,
			sprites.Vanessa2,
			sprites.Vanessaa,
		},
		Flags: []CharacterFlag{
			FlagWasp,
			FlagShe,
		},
		PortraitDx: -20,
	},
	{
		Name:        "vi",
		DisplayName: "Vi",
		Sprites: [3]*sprites.Sprite{
			sprites.Vi1,
			sprites.Vi2,
			sprites.Via,
		},
		Flags: []CharacterFlag{
			FlagBee,
			FlagSnakemouth,
			FlagExplorer,
			FlagFrench,
			FlagShe,
		},
		PortraitDx: -20,
		PortraitSz: 0.125,
	},
	{
		Name:        "yin",
		DisplayName: "Yin",
		Sprites: [3]*sprites.Sprite{
			sprites.Yin1,
			sprites.Yin2,
			sprites.Yina,
		},
		Flags: []CharacterFlag{
			FlagMoth,
			FlagExplorer,
			FlagShe,
		},
		PortraitDy: 20,
		PortraitSz: 0.25,
	},
	{
		Name:        "zaryant",
		DisplayName: "Zaryant",
		Sprites: [3]*sprites.Sprite{
			sprites.Zaryant1,
			sprites.Zaryant2,
			sprites.Zaryanta,
		},
		Flags: []CharacterFlag{
			FlagAnt,
			FlagShe,
		},
	},
	{
		Name:        "zasp",
		DisplayName: "Zasp",
		Sprites: [3]*sprites.Sprite{
			sprites.Zasp1,
			sprites.Zasp2,
			sprites.Zaspa,
		},
		Flags: []CharacterFlag{
			FlagWasp,
			FlagExplorer,
			FlagHe,
		},
		PortraitDy: 20,
	},
}
