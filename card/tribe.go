//go:generate stringer -type Tribe -linecomment

package card

import "image/color"

// Tribe is a 4-bit identifier for a Bug Fables Spy Cards tribe.
type Tribe uint8

// Constants for Tribe.
const (
	TribeSeedling   Tribe = 0  // Seedling
	TribeWasp       Tribe = 1  // Wasp
	TribeFungi      Tribe = 2  // Fungi
	TribeZombie     Tribe = 3  // Zombie
	TribePlant      Tribe = 4  // Plant
	TribeBug        Tribe = 5  // Bug
	TribeBot        Tribe = 6  // Bot
	TribeThug       Tribe = 7  // Thug
	TribeUnknown    Tribe = 8  // ???
	TribeChomper    Tribe = 9  // Chomper
	TribeLeafbug    Tribe = 10 // Leafbug
	TribeDeadLander Tribe = 11 // Dead Lander
	TribeMothfly    Tribe = 12 // Mothfly
	TribeSpider     Tribe = 13 // Spider
	TribeCustom     Tribe = 14 // (custom)
	TribeNone       Tribe = 15 // (none)
)

var tribeColors = [16]color.RGBA{
	TribeSeedling:   {186, 239, 80, 255},
	TribeWasp:       {223, 174, 21, 255},
	TribeFungi:      {96, 112, 163, 255},
	TribeZombie:     {227, 220, 162, 255},
	TribePlant:      {244, 143, 190, 255},
	TribeBug:        {158, 88, 47, 255},
	TribeBot:        {249, 226, 70, 255},
	TribeThug:       {72, 159, 202, 255},
	TribeUnknown:    {130, 143, 149, 255},
	TribeChomper:    {166, 55, 32, 255},
	TribeLeafbug:    {90, 63, 106, 255},
	TribeDeadLander: {44, 11, 1, 255},
	TribeMothfly:    {44, 44, 44, 255},
	TribeSpider:     {135, 115, 79, 255},
	TribeCustom:     {255, 0, 255, 255},
	TribeNone:       {255, 0, 255, 255},
}

// TribeDef represents a built-in or custom tribe.
type TribeDef struct {
	Tribe Tribe

	Red   uint8
	Green uint8
	Blue  uint8

	CustomName string
}

// Name returns the display name of this TribeDef.
func (t TribeDef) Name() string {
	if t.Tribe == TribeCustom {
		return t.CustomName
	}

	return t.Tribe.String()
}

// Color returns the color associated with this TribeDef.
func (t TribeDef) Color() color.RGBA {
	if t.Tribe == TribeCustom {
		return color.RGBA{t.Red, t.Green, t.Blue, 255}
	}

	return tribeColors[t.Tribe]
}
