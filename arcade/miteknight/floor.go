//go:generate stringer -type tileType -trimprefix tile

package miteknight

import (
	"context"
	"fmt"
	"math"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/xerrors"
)

type tileType uint8

const (
	tileWall tileType = iota
	tileFree
	tileKey
	tilePotion
	tileDoor
	tileStairs
	tileWall2
	tileNone
)

type dungeonFloor struct {
	floor int
	sizeX int
	sizeY int

	roomData       []roomBounds
	enemies        []*entity
	staticGeometry []*gfx.StaticBuffer
	player         *entity
	door           *entity
	items          []*entity
	floorMap       [][]tileType
	compassTarget  []coord3
	hasKey         bool
	gotKey         bool
	spawnedEnemyAt map[[2]int]bool
	dungeon        *MiteKnight
}

func (d *dungeonFloor) generate(ctx context.Context, dungeon *MiteKnight) error {
	d.dungeon = dungeon
	roomCount := int(math.Ceil(dungeon.rules.RoomCount + math.Floor(dungeon.rules.RoomCountPerFloor*float64(d.floor))))
	d.roomData = []roomBounds{
		{
			x0: 1,
			y0: 1,
			x1: 2,
			y1: 2,
		},
		{
			x0: d.sizeX - 6,
			y0: d.sizeY - 5,
			x1: d.sizeX - 2,
			y1: d.sizeY - 2,
		},
	}

	floorMapBuf := make([]tileType, d.sizeX*d.sizeY)
	d.floorMap = make([][]tileType, d.sizeX)

	for x := range d.floorMap {
		d.floorMap[x] = floorMapBuf[:d.sizeY]
		floorMapBuf = floorMapBuf[d.sizeY:]
	}

	for i := 2; i < roomCount; i++ {
		var bounds roomBounds

		bounds.x0 = int(dungeon.state.RNG.RangeIntFloat(1, float64(d.sizeX)-(2+dungeon.rules.MaxRoomSize[0])))
		bounds.y0 = int(dungeon.state.RNG.RangeIntFloat(1, float64(d.sizeY)-(2+dungeon.rules.MaxRoomSize[1])))
		bounds.x1 = bounds.x0 + int(dungeon.state.RNG.RangeIntFloat(1, 2+dungeon.rules.MaxRoomSize[0]))
		bounds.y1 = bounds.y0 + int(dungeon.state.RNG.RangeIntFloat(1, 2+dungeon.rules.MaxRoomSize[1]))

		atLeastOneCornerOfThisRoomDoesNotOverlapAnotherRoom := false

		for retries := 0; retries < 20; retries++ {
			for _, room := range d.roomData {
				if !(bounds.x0 >= room.x0 && bounds.x0 <= room.x1) &&
					!(bounds.x1 >= room.x0 && bounds.x1 <= room.x1) &&
					!(bounds.y0 >= room.y0 && bounds.y0 <= room.y1) &&
					!(bounds.y1 >= room.y0 && bounds.y1 <= room.y1) {
					atLeastOneCornerOfThisRoomDoesNotOverlapAnotherRoom = true

					break
				}
			}
		}

		if atLeastOneCornerOfThisRoomDoesNotOverlapAnotherRoom {
			d.roomData = append(d.roomData, bounds)
		}
	}

	for _, room := range d.roomData {
		for x := room.x0; x <= room.x1; x++ {
			for y := room.y0; y <= room.y1; y++ {
				d.floorMap[x][y] = tileFree
			}
		}
	}

	d.setCorridors()
	d.setExtras()
	d.hideUnseen()

	if err := d.createEntities(); err != nil {
		return xerrors.Errorf("miteknight: creating entities: %w", err)
	}

	d.compassTarget = make([]coord3, 2)

	for baseY := 0; baseY < d.sizeY; baseY += 250 {
		for baseX := 0; baseX < d.sizeX; baseX += 250 {
			var (
				staticData     []uint8
				staticElements []uint16
			)

			maxX := d.sizeX
			if baseX+250 < maxX {
				maxX = baseX + 250
			}

			maxY := d.sizeY
			if baseY+250 < maxY {
				maxY = baseY + 250
			}

			for y := maxY - 1; y >= baseY; y-- {
				for x := baseX; x < maxX; x++ {
					switch d.floorMap[x][y] {
					case tileKey:
						d.compassTarget[0] = coord3{x: float32(x), y: 0.5, z: float32(y)}
						d.items = append(d.items, &entity{
							entityType: entityKey,
							sprite:     sprites.MKKey,
							x:          x,
							height:     0.5,
							z:          y,
							billboard:  true,
						})
					case tilePotion:
						d.items = append(d.items, &entity{
							entityType: entityPotion,
							sprite:     sprites.MKPotion,
							x:          x,
							height:     0.5,
							z:          y,
							billboard:  true,
						})
					case tileDoor:
						d.door = &entity{
							entityType: entityDoor,
							x:          x,
							height:     0,
							z:          y,
							sprite:     sprites.MKDoor,
							fog:        true,
						}

						d.enemies = append(d.enemies, d.newEnemy(entityWizard, x+1, y+2))

						if float64(d.floor) >= dungeon.rules.DoubleBossMinFloor {
							d.enemies = append(d.enemies, d.newEnemy(entityWizard, x+1, y+1))
						}
					case tileStairs:
						d.compassTarget[1] = coord3{x: float32(x), y: 0.5, z: float32(y)}
					}

					if d.floorMap[x][y] == tileNone {
						continue
					}

					staticData, staticElements = d.appendTileGeometry(staticData, staticElements, x, y, baseX, baseY)
				}
			}

			d.staticGeometry = append(d.staticGeometry, gfx.NewStaticBuffer(fmt.Sprintf("mk floor %d geom %d", d.floor, len(d.staticGeometry)), staticData, staticElements))
		}
	}

	return ctx.Err()
}

func (d *dungeonFloor) setCorridors() {
	handled := make([]bool, len(d.roomData)-1)

	for roomID := 1; roomID < len(d.roomData)-1; roomID++ {
		for x := d.roomData[0].x0; x < d.sizeX; x++ {
			for y := d.roomData[0].y0; y <= d.roomData[0].y1; y++ {
				if d.floorMap[x][y] == tileFree && !handled[roomID] {
					if d.tileIsInRoomID(x, y) == roomID-1 {
						handled[roomID] = true
					}

					for tx := d.roomData[0].x0; tx < x; tx++ {
						d.floorMap[tx][y] = tileFree
					}

					break
				}
			}
		}

		for y := d.roomData[0].y0; y < d.sizeY; y++ {
			for x := d.roomData[0].x0; x <= d.roomData[0].x1; x++ {
				if d.floorMap[x][y] == tileFree && !handled[roomID] {
					if d.tileIsInRoomID(x, y) == roomID-1 {
						handled[roomID] = true
					}

					for ty := d.roomData[0].y0; ty < y; ty++ {
						d.floorMap[x][ty] = tileFree
					}

					break
				}
			}
		}

		for roomID2 := 1; roomID2 < len(d.roomData); roomID2++ {
			pointX, pointY := d.getRandomPointInRoom(roomID2, false)

			x, y := d.roomData[0].x1, d.roomData[0].y0

			if d.dungeon.state.RNG.RangeInt(0, 2) == 0 {
				for y = d.roomData[0].y0; y < pointY; y++ {
					d.floorMap[x][y] = tileFree
				}

				for x = d.roomData[0].x1; x < pointX; x++ {
					d.floorMap[x][y] = tileFree
				}
			} else {
				for x = d.roomData[0].x1; x < pointX; x++ {
					d.floorMap[x][y] = tileFree
				}

				for y = d.roomData[0].y0; y < pointY; y++ {
					d.floorMap[x][y] = tileFree
				}
			}
		}
	}
}

func (d *dungeonFloor) setExtras() {
	potionCount := int(math.Ceil(float64(len(d.roomData)) / math.Min(math.Max(float64(d.floor), 2), 3)))
	for roomID := 2; roomID < 2+potionCount; roomID++ {
		x, y := d.getRandomPointInRoom(roomID, false)
		if d.tileIsInRoomID(x, y) > 0 {
			d.floorMap[x][y] = tilePotion
		}
	}

	var (
		keyX, keyY int
		haveKeyPos bool
	)

	for retries := 0; retries < 20; retries++ {
		keyX, keyY = d.getRandomPointInRoom(d.dungeon.state.RNG.RangeInt(2, len(d.roomData)), false)
		if d.tileIsInRoomID(keyX, keyY) > 1 {
			haveKeyPos = true

			break
		}
	}

	if !haveKeyPos {
		keyX = d.sizeX / 2
		keyY = d.sizeY / 2
		d.floorMap[keyX+1][keyY] = tileFree
		d.floorMap[keyX+1][keyY+1] = tileFree
		d.floorMap[keyX+1][keyY-1] = tileFree
		d.floorMap[keyX][keyY+1] = tileFree
		d.floorMap[keyX][keyY-1] = tileFree
		d.floorMap[keyX-1][keyY+1] = tileFree
		d.floorMap[keyX-1][keyY] = tileFree
		d.floorMap[keyX-1][keyY-1] = tileFree

		for x := 1; x < d.sizeX-1; x++ {
			d.floorMap[x][keyY] = tileFree
		}

		for y := 1; y < d.sizeY-1; y++ {
			d.floorMap[keyX][y] = tileFree
		}
	}

	d.floorMap[d.sizeX-5][d.sizeY-3] = tileWall2
	d.floorMap[d.sizeX-5][d.sizeY-2] = tileWall2
	d.floorMap[d.sizeX-5][d.sizeY-4] = tileWall2
	d.floorMap[d.sizeX-4][d.sizeY-4] = tileDoor
	d.floorMap[d.sizeX-3][d.sizeY-4] = tileWall2
	d.floorMap[d.sizeX-2][d.sizeY-4] = tileWall2
	d.floorMap[d.sizeX-1][d.sizeY-2] = tileStairs
	d.floorMap[keyX][keyY] = tileKey
}

func (d *dungeonFloor) hideUnseen() {
	var insideWall [][2]int

	for x := 1; x < d.sizeX-1; x++ {
		for y := 1; y < d.sizeY-1; y++ {
			if d.floorMap[x+1][y] == tileWall &&
				d.floorMap[x][y+1] == tileWall &&
				d.floorMap[x-1][y] == tileWall &&
				d.floorMap[x][y-1] == tileWall {
				insideWall = append(insideWall, [2]int{x, y})
			}
		}
	}

	for _, c := range insideWall {
		d.floorMap[c[0]][c[1]] = tileNone
	}
}

func (d *dungeonFloor) createEntities() error {
	d.player = &entity{
		entityType: entityPlayer,
		bounce:     0.2,
		x:          1,
		height:     0,
		z:          1,
		hp:         int(d.dungeon.rules.PlayerMaxHP),
		maxhp:      int(d.dungeon.rules.PlayerMaxHP),
		sprite:     sprites.MKKnightIdle,
		flipX:      true,
		billboard:  true,
	}

	enemyCount := int(math.Ceil(d.dungeon.rules.EnemyCount + float64(d.floor)*d.dungeon.rules.EnemyCountPerFloor))

	enemies := make([]*entity, 0, enemyCount)
	d.spawnedEnemyAt = make(map[[2]int]bool, enemyCount)

	for i := 0; i < enemyCount; i++ {
		e, err := d.newRandomEnemy()
		if err != nil {
			return xerrors.Errorf("miteknight: placing enemy %d/%d: %w", i+1, enemyCount, err)
		}

		enemies = append(enemies, e)
	}

	d.enemies = enemies

	return nil
}

func (d *dungeonFloor) tileIsInRoomID(x, z int) int {
	for i, room := range d.roomData {
		if x >= room.x0 && x <= room.x1 && z >= room.y0 && z <= room.y1 {
			return i
		}
	}

	return -1
}

func (d *dungeonFloor) getRandomEnemy() entityType {
	if float64(d.floor) >= d.dungeon.rules.WizardMinFloor {
		if d.dungeon.state.RNG.RangeIntFloat(0, d.dungeon.rules.AntEnemyWeight+d.dungeon.rules.WizardEnemyWeight) < d.dungeon.rules.AntEnemyWeight {
			return entityAnt
		}

		return entityWizard
	}

	return entityAnt
}

func (d *dungeonFloor) getRandomPointInRoom(roomID int, skipFencepostEmulation bool) (int, int) {
	inc := 0

	if skipFencepostEmulation {
		inc = 1
	}

	room := d.roomData[roomID]

	return d.dungeon.state.RNG.RangeInt(room.x0, room.x1+inc),
		d.dungeon.state.RNG.RangeInt(room.y0, room.y1+inc)
}

func (d *dungeonFloor) newRandomEnemy() (*entity, error) {
	var x, z, retries int

	entityType := d.getRandomEnemy()

	for {
		retries++
		if retries > 1000 {
			return nil, xerrors.New("miteknight: failed to place enemy after 1000 tries")
		}

		x, z = d.getRandomPointInRoom(d.dungeon.state.RNG.RangeInt(2, len(d.roomData)), true)

		if d.floorMap[x][z] == tileFree &&
			d.entityInPos(x, z) == nil &&
			x*x+z*z >= 5*5 &&
			!d.spawnedEnemyAt[[2]int{x, z}] {
			break
		}
	}

	d.spawnedEnemyAt[[2]int{x, z}] = true

	return d.newEnemy(entityType, x, z), nil
}

func (d *dungeonFloor) newEnemy(enemyType entityType, x, z int) *entity {
	entity := &entity{
		entityType: enemyType,
		x:          x,
		height:     0,
		z:          z,
		hp:         2,
		maxhp:      2,
		bounce:     0.3,
		flipX:      true,
		billboard:  true,
	}

	switch entity.entityType {
	case entityAnt:
		entity.sprite = sprites.MKAntFront
	case entityWizard:
		entity.sprite = sprites.MKWizardFront
		entity.child = nil
	case entityFireball:
		entity.sprite = sprites.MKFireball
		entity.height += 0.5
		entity.hp = 1
	}

	return entity
}

func (d *dungeonFloor) entityInPos(x, z int) *entity {
	if d.player.x == x && d.player.z == z {
		return d.player
	}

	for _, enemy := range d.enemies {
		if !enemy.dead && enemy.x == x && enemy.z == z {
			return enemy
		}
	}

	return nil
}

func (d *dungeonFloor) isFrontFree(x, z int, dir direction) bool {
	x, z = getFrontPos(x, z, dir)

	return (d.floorMap[x][z] == tileFree ||
		d.floorMap[x][z] == tileKey ||
		d.floorMap[x][z] == tilePotion) &&
		d.entityInPos(x, z) == nil
}

func (d *dungeonFloor) moveForward(entity *entity, dir direction, player bool) {
	const frames = 15

	x, z := getFrontPos(entity.x, entity.z, dir)

	entity.dx = float32(entity.x - x)
	entity.dz = float32(entity.z - z)

	if entity.moving != 0 {
		entity.moveInterrupt = entity.moving
		entity.moveInterruptX = entity.x
		entity.moveInterruptZ = entity.z
	}

	entity.moving = frames + 2

	pb := d.dungeon.state.Playback
	if player || (pb != nil && pb.Version[0] == 0 && pb.Version[1] == 2 && pb.Version[2] > 40 && pb.Version[2] < 44) {
		entity.playerMoving = entity.moving
	}

	entity.tempBounce = d.dungeon.state.TickCount + entity.moving
	entity.bounce = 0.2
	entity.x = x
	entity.z = z

	if entity == d.player && !d.player.blocking {
		entity.flipX = !entity.flipX

		d.doAction(actionMoving, frames)
	}
}

func (d *dungeonFloor) doAction(act action, time uint64) {
	d.player.action = act
	d.player.actionCooldown = int(time)

	switch act {
	case actionAttacking:
		d.player.sprite = sprites.MKKnightAttack
	case actionMoving, actionTurning:
		d.player.sprite = sprites.MKKnightMove
	}
}

func (d *dungeonFloor) switchDirection(left bool) {
	d.doAction(actionTurning, 20)
	d.player.flipX = left

	dir := dirRight
	if left {
		dir = dirLeft
	}

	d.player.direction = modDir[d.player.direction][dir]

	switch d.player.direction {
	case dirUp:
		d.dungeon.camRotationTarget = 0
	case dirDown:
		d.dungeon.camRotationTarget = 1.0 * math.Pi
	case dirLeft:
		d.dungeon.camRotationTarget = 1.5 * math.Pi
	case dirRight:
		d.dungeon.camRotationTarget = 0.5 * math.Pi
	}

	d.player.iframes = 15
}

func (d *dungeonFloor) doDamage(entity *entity) {
	entity.hp--

	if entity.entityType == entityFireball {
		// possible upstream bug: this will not render within view
		d.spawnParticleSystem(deathSmoke, float32(entity.x)+entity.ox(), entity.height-70, float32(entity.z)+entity.oz(), 1)
		entity.dead = true

		return
	}

	d.spawnParticleSystem(hitPart, float32(entity.x)+entity.ox(), entity.height, float32(entity.z)+entity.oz(), 1)

	if entity == d.player {
		d.dungeon.compassEnabled = false
		d.dungeon.stopDelay = 0
		entity.iframes = int(math.Ceil(d.dungeon.rules.PlayerDamageImmunity * 60))

		if entity.hp == 0 {
			audio.MKDeath.PlaySound(0, 0, 0)
		} else {
			audio.MKHit.PlaySound(0, 0, 0)
		}
	} else {
		audio.MKHit2.PlaySound(0, 0, 0)
		d.dungeon.state.Score += d.dungeon.rules.ScorePerHit
		entity.iframes = int(math.Ceil(d.dungeon.rules.EnemyDamageImmunity * 60))
	}

	dir := modDir[entity.direction][dirDown]

	knockedBack := false

	if d.isFrontFree(entity.x, entity.z, dir) {
		d.moveForward(entity, dir, false)

		knockedBack = true
	}

	if entity.hp <= 0 {
		if entity == d.player {
			d.dungeon.endGame()
		} else {
			entity.dying = knockedBack
			entity.dead = true
			entity.cooldown = 600
			d.dungeon.state.Score += d.dungeon.rules.ScorePerKill
		}
	} else {
		entity.blinkStart = d.dungeon.state.TickCount
		entity.blinkDelay = d.dungeon.state.ComputeWait(0.1)
	}
}

func (d *dungeonFloor) destroyItem(x, z int) {
	for i := 0; i < len(d.items); i++ {
		if d.items[i].x == x && d.items[i].z == z {
			d.items = append(d.items[:i], d.items[i+1:]...)
			i--
		}
	}
}

func (d *dungeonFloor) directionToPlayer(facing direction) direction {
	switch d.player.direction {
	case dirUp:
		switch facing {
		case dirUp:
			return dirDown
		case dirDown:
			return dirUp
		case dirLeft:
			return dirLeft
		case dirRight:
			return dirRight
		}
	case dirDown:
		switch facing {
		case dirUp:
			return dirUp
		case dirDown:
			return dirDown
		case dirLeft:
			return dirRight
		case dirRight:
			return dirLeft
		}
	case dirLeft:
		switch facing {
		case dirUp:
			return dirRight
		case dirDown:
			return dirLeft
		case dirLeft:
			return dirDown
		case dirRight:
			return dirUp
		}
	case dirRight:
		switch facing {
		case dirUp:
			return dirLeft
		case dirDown:
			return dirRight
		case dirLeft:
			return dirUp
		case dirRight:
			return dirDown
		}
	}

	return facing
}

func (d *dungeonFloor) dirToPlayer(observer *entity) direction {
	dx := float32(observer.x) + observer.ox() - float32(d.player.x) - d.player.ox()
	dz := float32(observer.z) + observer.oz() - float32(d.player.z) - d.player.oz()

	if math.Abs(float64(dx)) == math.Abs(float64(dz)) {
		if d.player.direction == dirUp || d.player.direction == dirDown {
			dx = 0
		} else {
			dz = 0
		}
	}

	if math.Abs(float64(dx)) > math.Abs(float64(dz)) {
		if dx > 0 {
			return dirLeft
		}

		return dirRight
	}

	if dz > 0 {
		return dirDown
	}

	return dirUp
}

func (d *dungeonFloor) tempBounce(entity *entity) {
	entity.bounce = 0.2
	entity.tempBounce = d.dungeon.state.TickCount + d.dungeon.state.ComputeWait(0.5)
}
