// +build !headless

package gfx

import (
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/gl"
)

var sz size.Event

// Size returns the current screen size.
func Size() (w, h int) {
	return sz.WidthPx, sz.HeightPx
}

// SetSize processes a size.Event.
func SetSize(e size.Event) {
	sz = e

	if GL != nil {
		GL.Viewport(0, 0, sz.WidthPx, sz.HeightPx)
		GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	}
}
