package arcade

import (
	"reflect"

	"git.lubar.me/ben/spy-cards/format"
)

// GameRules is an interface implemented by all game rules.
type GameRules interface {
	Default() GameRules
}

// MiteKnightRules is GameRules for Mite Knight.
type MiteKnightRules struct {
	PauseStopsAnimations bool
	PauseOverview        bool
	FloorCount           float64
	MapSize              [2]float64
	MapSizePerFloor      [2]float64
	MaxRoomSize          [2]float64
	WizardMinFloor       float64
	AntEnemyWeight       float64
	WizardEnemyWeight    float64
	DoubleBossMinFloor   float64
	RoomCount            float64
	RoomCountPerFloor    float64
	EnemyCount           float64
	EnemyCountPerFloor   float64
	TimeLimit            float64
	PlayerMaxHP          float64
	CompassDelay         float64
	ScorePerHit          float64
	ScorePerKill         float64
	ScorePerKey          float64
	ScorePerDoor         float64
	EnemyDamageImmunity  float64
	PlayerDamageImmunity float64
	CamFollowSpeed       float64
	CamRotationSpeed     float64
	ScoreDigits          float64
}

// DefaultMiteKnightRules is the default game rules for Mite Knight.
var DefaultMiteKnightRules = MiteKnightRules{
	PauseStopsAnimations: false,
	PauseOverview:        false,
	FloorCount:           3,
	MapSize:              [2]float64{25, 25},
	MapSizePerFloor:      [2]float64{5, 5},
	MaxRoomSize:          [2]float64{3, 3},
	WizardMinFloor:       1,
	AntEnemyWeight:       3,
	WizardEnemyWeight:    2,
	DoubleBossMinFloor:   2,
	RoomCount:            8,
	RoomCountPerFloor:    0.25,
	EnemyCount:           8,
	EnemyCountPerFloor:   1,
	TimeLimit:            300,
	PlayerMaxHP:          6,
	CompassDelay:         2,
	ScorePerHit:          10,
	ScorePerKill:         100,
	ScorePerKey:          500,
	ScorePerDoor:         400,
	EnemyDamageImmunity:  1,
	PlayerDamageImmunity: 4.0 / 3.0,
	CamFollowSpeed:       0.1,
	CamRotationSpeed:     0.1,
	ScoreDigits:          5,
}

// Default implements GameRules.
func (r *MiteKnightRules) Default() GameRules {
	return &DefaultMiteKnightRules
}

// FlowerJourneyRules is GameRules for Flower Journey.
type FlowerJourneyRules struct {
	WallMin                  float64
	WallMax                  float64
	WallGapStart             float64
	WallGap100               float64
	FirstWallDist            float64
	WallDist                 float64
	CountdownLength          float64
	CountdownTime            float64
	GoTime                   float64
	SpeedStart               float64
	Speed100                 float64
	FlapVelocity             float64
	GravityScale             float64
	ScorePerWall             float64
	ScorePerWallCombo        float64
	ScorePerPassedEnemy      float64
	ScorePerPassedEnemyCombo float64
	ScorePerKilledEnemy      float64
	ScorePerKilledEnemyCombo float64
	ScorePerFlower           float64
	ScorePerFlowerCombo      float64
	HoneyInvulnTime          float64
	HoneyMinCombo            float64
	EnemyHeightMin           float64
	EnemyHeightMax           float64
	EnemyMinCombo            float64
	EnemyMinProgress         float64
	EnemySpeed               float64
}

// DefaultFlowerJourneyRules is the default game rules for Flower Journey.
var DefaultFlowerJourneyRules = FlowerJourneyRules{
	WallMin:                  -0.9,
	WallMax:                  1.1,
	WallGapStart:             2.5,
	WallGap100:               2.25,
	FirstWallDist:            5,
	WallDist:                 5,
	CountdownLength:          3,
	CountdownTime:            0.75,
	GoTime:                   1,
	SpeedStart:               0.05,
	Speed100:                 0.1,
	FlapVelocity:             4.5,
	GravityScale:             1.5,
	ScorePerWall:             15,
	ScorePerWallCombo:        0,
	ScorePerPassedEnemy:      30,
	ScorePerPassedEnemyCombo: 0,
	ScorePerKilledEnemy:      10,
	ScorePerKilledEnemyCombo: 10,
	ScorePerFlower:           10,
	ScorePerFlowerCombo:      10,
	HoneyInvulnTime:          5,
	HoneyMinCombo:            8,
	EnemyHeightMin:           1.25,
	EnemyHeightMax:           3.5,
	EnemyMinCombo:            4,
	EnemyMinProgress:         8,
	EnemySpeed:               0.75,
}

// Default implements GameRules.
func (r *FlowerJourneyRules) Default() GameRules {
	return &DefaultFlowerJourneyRules
}

func marshalRules(w *format.Writer, v GameRules) {
	rules := reflect.ValueOf(v).Elem()
	defaults := reflect.ValueOf(v.Default()).Elem()

	for i := 0; i < rules.NumField(); i++ {
		if rules.Field(i).Interface() != defaults.Field(i).Interface() {
			w.UVarInt(uint64(i))
			w.Write(rules.Field(i).Interface())
		}
	}
}

func unmarshalRules(r *format.Reader, v GameRules) {
	rules := reflect.ValueOf(v).Elem()
	defaults := reflect.ValueOf(v.Default()).Elem()

	rules.Set(defaults)

	for r.Len() != 0 {
		id := r.UVarInt()
		ruleVal := rules.Field(int(id)).Addr().Interface()
		r.Read(ruleVal)
	}
}
