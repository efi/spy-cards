// +build !headless

// Package visual implements the graphical interface for Spy Cards Online.
package visual

import (
	"context"
	"image/color"
	"log"
	"math"
	"math/bits"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/matchnet"
	"git.lubar.me/ben/spy-cards/room"
	"golang.org/x/xerrors"
)

type Visual struct {
	cam   gfx.Camera
	ictx  *input.Context
	state VisualState

	modeLoad  chan struct{}
	turnReady chan *card.TurnData

	sc *matchnet.SignalConn
	gc *matchnet.GameConn

	mine     room.Room
	stage    room.Room
	audience []*room.Audience
	player   [2]*room.Player

	turnData  *card.TurnData
	timerTurn uint64
	timerBank uint64

	rec   *match.Recording
	Match *match.Match
	deck  *DeckPicker
	sb    *sprites.Batch
	tb    *sprites.TextBatch

	modeX, modeY   float32
	varX, varY     float32
	hostX, hostY   float32
	hostText       [2]string
	joinX, joinY   float32
	quickX, quickY float32
	quickText      [3]string
	activeCard     map[*match.ActiveCard]*Card
	myHand         *Layout
	theirHand      *Layout
	myField        *Layout
	theirField     *Layout
	myDeck         *Layout
	theirDeck      *Layout
	recTimeText    string
	recVersionText string

	message           string
	smallMessage      string
	animationTimer    uint64
	cardAnimation     []match.CardEffect
	healAmount        float32
	myHPAnim          float32
	theirHPAnim       float32
	matchComplete     chan *card.Recording
	networkErrorTimer uint8
	allowQuickJoin    bool
	myReady           bool
	theirReady        bool
	usingCustomMusic  bool
	usingCustomStage  bool
}

func New(ctx context.Context, init *match.Init) (*Visual, error) {
	v := &Visual{
		Match: &match.Match{
			Init: init,
		},
	}

	v.Match.Log.Debug = true

	go preloadMatchAssets()

	v.ictx = input.GetContext(ctx)

	v.cam.SetDefaults()
	v.cam.Offset = gfx.Identity()
	v.cam.Rotation = gfx.Identity()
	v.cam.Position = gfx.Identity()

	v.modeLoad = make(chan struct{})
	v.turnReady = make(chan *card.TurnData)

	v.stage = room.NewStage(ctx, nil)

	go func() {
		set, err := v.Match.Init.Cards(ctx)
		if err != nil {
			v.message = "Failed to load game mode!"
			v.smallMessage = xerrors.Unwrap(err).Error()
			log.Println("failed to load cards for mode:", err)

			return
		}

		v.Match.Set = set

		if v.Match.Init.Mode == "" {
			v.allowQuickJoin = true
		} else if v.Match.Init.External != nil {
			v.allowQuickJoin = v.Match.Init.External.QuickJoin
		}

		v.reloadDefaultStage()

		close(v.modeLoad)
	}()

	var err error

	v.gc, err = matchnet.NewGameConn(ctx)
	if err != nil {
		return nil, xerrors.Errorf("visual: creating match networking connection: %w", err)
	}

	v.myHand = NewLayout(ctx)
	v.theirHand = NewLayout(ctx)
	v.myHand.InputFocus = new(interface{})
	v.theirHand.InputFocus = v.myHand.InputFocus
	v.myHand.InputFocusRight = v.theirHand.Focus
	v.theirHand.InputFocusLeft = v.myHand.Focus

	v.theirHand.Reverse = true
	v.myHand.AllowScroll = false
	v.theirHand.AllowScroll = false
	v.myHand.Padding = -0.125
	v.theirHand.Padding = -0.35
	v.myHand.MinScale = 2
	v.theirHand.MinScale = 1.5
	v.myHand.BaseScale = 3
	v.theirHand.BaseScale = 2.5
	v.myHand.MaxScale = 3
	v.theirHand.MaxScale = 2.5
	v.theirHand.HoverScale = 0.5
	v.myHand.MinPerRow = 50
	v.theirHand.MinPerRow = 50
	v.myHand.MaxPerRow = 50
	v.theirHand.MaxPerRow = 50
	v.myHand.PinCorner = 4
	v.theirHand.PinCorner = 3
	v.myHand.MarginRight = 0.34
	v.theirHand.MarginLeft = 0.66
	v.myHand.GrowDx = 0.125
	v.myHand.GrowDy = 0.25

	v.myHand.OnSelect = func(c *Card) {
		if v.state != StateTurnSelect || v.myReady {
			return
		}

		wasSelected := false
		remainingTP := v.Match.State.Sides[v.Match.Perspective-1].TP

		for i, hc := range v.myHand.Cards {
			if hc == c {
				wasSelected = v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0

				v.turnData.Ready[v.Match.Perspective-1] ^= 1 << i
			}

			if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
				remainingTP.Amount -= hc.Def.TP + v.Match.State.Sides[v.Match.Perspective-1].ModTP[hc.Def.ID]
			}
		}

		switch {
		case wasSelected:
			audio.PageFlip.PlaySound(0, 0.7, 0)
		case remainingTP.AmountInf < 0 || remainingTP.NaN:
			audio.Buzzer.PlaySound(0, 0, 0)
		case remainingTP.AmountInf > 0:
			audio.Confirm.PlaySound(0, 0, 0)
		case remainingTP.Amount < 0:
			audio.Buzzer.PlaySound(0, 0, 0)
		default:
			audio.Confirm.PlaySound(0, 0, 0)
		}

		played := &v.turnData.Played[v.Match.Perspective-1]
		*played = (*played)[:0]

		for i, c := range v.myHand.Cards {
			if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
				*played = append(*played, c.Def.ID)
			}
		}
	}

	v.activeCard = make(map[*match.ActiveCard]*Card)

	v.myField = NewLayout(ctx)
	v.theirField = NewLayout(ctx)
	v.myField.InputFocus = v.myHand.InputFocus
	v.theirField.InputFocus = v.myHand.InputFocus
	v.myField.InputFocusRight = v.theirField.Focus
	v.theirField.InputFocusLeft = v.myField.Focus
	v.myField.InputFocusDown = v.myHand.Focus
	v.theirField.InputFocusDown = v.theirHand.Focus
	v.myHand.InputFocusUp = v.myField.Focus
	v.theirHand.InputFocusUp = v.theirField.Focus

	v.myField.Slide = true
	v.theirField.Slide = true
	v.myField.AllowScroll = false
	v.theirField.AllowScroll = false
	v.myField.CenterLastRow = true
	v.theirField.CenterLastRow = true
	v.theirField.Reverse = true
	v.myField.MarginRight = 0.5
	v.theirField.MarginLeft = 0.5

	return v, nil
}

func (v *Visual) SetRecording(ctx context.Context, code string) error {
	v.Match.Log.Disabled = true

	rec, err := card.FetchRecording(ctx, code)
	if err != nil {
		return xerrors.Errorf("visual: fetching recording: %w", err)
	}

	v.rec, err = match.NewRecording(ctx, rec)
	if err != nil {
		return xerrors.Errorf("visual: preparing recording: %w", err)
	}

	v.gc.SCG, err = matchnet.NewSecureCardGame(&matchnet.SecureCardGameOptions{ForReplay: &rec.Version})
	if err != nil {
		return xerrors.Errorf("visual: preparing RNG for recording: %w", err)
	}

	copy(v.gc.SCG.Seed(matchnet.RNGShared), rec.SharedSeed[:])

	v.Match.Set = &card.Set{}
	*v.Match.Set = rec.CustomCards

	if v.Match.Set.Mode != nil {
		v.Match.Set.Mode, _ = v.Match.Set.Mode.Variant(v.Match.Set.Variant)
	}

	v.Match.IndexSet()

	v.player[0] = room.NewPlayer(room.CharacterByName[rec.Cosmetic[0].CharacterName])
	v.player[1] = room.NewPlayer(room.CharacterByName[rec.Cosmetic[1].CharacterName])

	v.stage.AddPlayer(v.player[0], 1)
	v.stage.AddPlayer(v.player[1], 2)

	player := 0

	if rec.Perspective == 2 {
		player = 1

		v.stage.SetPlayer(2)
	}

	v.Match.Perspective = uint8(player) + 1

	v.audience = room.NewAudience(rec.SharedSeed[:], rec.RematchCount)
	v.stage.AddAudience(v.audience...)

	v.myDeck = NewLayout(ctx)
	v.theirDeck = NewLayout(ctx)

	rules, _ := rec.CustomCards.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	v.myDeck.InputFocus = new(interface{})
	*v.myDeck.InputFocus = v.myDeck
	v.theirDeck.InputFocus = v.myDeck.InputFocus
	v.myDeck.InputFocusDown = v.theirDeck.Focus
	v.theirDeck.InputFocusUp = v.myDeck.Focus

	v.myDeck.MinPerRow = int(rules.CardsPerDeck+1) / 2
	v.myDeck.MaxPerRow = v.myDeck.MinPerRow
	v.theirDeck.MinPerRow = v.myDeck.MinPerRow
	v.theirDeck.MaxPerRow = v.myDeck.MinPerRow
	v.myDeck.CenterLastRow = true
	v.theirDeck.CenterLastRow = true
	v.myDeck.AllowScroll = false
	v.theirDeck.AllowScroll = false
	v.myDeck.Padding = 0
	v.theirDeck.Padding = 0
	v.myDeck.MinScale = 1
	v.myDeck.BaseScale = 1
	v.myDeck.MaxScale = 1
	v.myDeck.HoverScale = 2
	v.theirDeck.MinScale = 1
	v.theirDeck.BaseScale = 1
	v.theirDeck.MaxScale = 1
	v.theirDeck.HoverScale = 2
	v.myDeck.MarginBottom = 0.5
	v.theirDeck.MarginTop = 0.5

	v.myDeck.Cards = make([]*Card, len(rec.InitialDeck[player]))
	v.theirDeck.Cards = make([]*Card, len(rec.InitialDeck[1-player]))

	for i, id := range rec.InitialDeck[player] {
		c := rec.CustomCards.Card(id)

		v.myDeck.Cards[i] = &Card{
			Card: &match.Card{
				Set:  &rec.CustomCards,
				Def:  c,
				Back: c.Rank.Back(),
			},
		}
	}

	for i, id := range rec.InitialDeck[1-player] {
		c := rec.CustomCards.Card(id)

		v.theirDeck.Cards[i] = &Card{
			Card: &match.Card{
				Set:  &rec.CustomCards,
				Def:  c,
				Back: c.Rank.Back(),
			},
		}
	}

	if !rec.Start.IsZero() {
		v.recTimeText = "Recorded " + rec.Start.Format("2 Jan 2006")
	}

	version := []byte("v")
	version = strconv.AppendUint(version, rec.Version[0], 10)
	version = append(version, '.')
	version = strconv.AppendUint(version, rec.Version[1], 10)
	version = append(version, '.')
	version = strconv.AppendUint(version, rec.Version[2], 10)

	if rec.ModeName != "" {
		version = append(version, '-')
		version = append(version, rec.ModeName...)
	}

	v.recVersionText = string(version)

	v.state = StatePlaybackInit

	v.playDefaultMusic(ctx)
	v.reloadDefaultStage()

	return nil
}

func (v *Visual) Update(ctx context.Context) error {
	touchcontroller.SkipFrame = true

	v.ictx.Tick()

	curX, curY, click := v.ictx.Mouse()
	lastX, lastY, lastClick := v.ictx.LastMouse()

	w, h := gfx.Size()
	v.cam.Perspective = gfx.Scale(-128/float32(w), -128/float32(h), -0.01)

	if v.stage != nil {
		v.stage.Update()
	}

	if v.Match.Perspective != 0 && v.gc != nil && v.player[2-v.Match.Perspective] == nil {
		if c := v.gc.RecvCosmeticData(); c != nil {
			v.Match.Cosmetic[2-v.Match.Perspective] = *c
			p := room.NewPlayer(room.CharacterByName[c.CharacterName])
			v.player[2-v.Match.Perspective] = p
			v.stage.AddPlayer(p, int(3-v.Match.Perspective))
		}
	}

again:
	switch v.state {
	case StateHome, StateOnHost, StateOnJoin, StateOnQuickJoin, StateOnVariant:
		v.updateHome(ctx)
	case StateConnecting:
		v.updateConnecting(ctx)
	case StateWait:
		// do nothing
	case StateCharacterSelect:
		v.mine.Update()

		if v.animationTimer > 0 {
			v.animationTimer--

			if v.animationTimer == 0 {
				v.mine = nil
				v.state = StateDeckSelect

				v.deck = NewDeckPicker(ctx, v.Match.Set)
				v.deck.AllowCreate = true
				v.deck.Populate()

				// run next update immediately
				goto again
			}
		}
	case StateDeckSelect:
		v.deck.Update(ctx)

		if v.deck.Selected != nil {
			match.SaveDeck(v.deck.Selected)

			v.gc.SendDeck(ctx, v.deck.Selected)

			deck := make([]*match.Card, len(v.deck.Selected))

			for i, id := range v.deck.Selected {
				c := v.Match.Set.Card(id)

				deck[i] = &match.Card{
					Set:  v.Match.Set,
					Def:  c,
					Back: c.Rank.Back(),
				}
			}

			v.Match.State.Sides[v.Match.Perspective-1].Deck = deck

			v.deck = nil
			v.state = StateReadyToStart
		}
	case StateReadyToStart:
		if v.updateReadyToStart(ctx) {
			goto again
		}
	case StateNextRound:
		if v.rec != nil {
			v.state = StatePlaybackNext

			goto again
		}

		if w := v.Match.Winner(); w != 0 {
			v.state = StateMatchCompleteWait

			v.matchComplete = make(chan *card.Recording, 1)

			go func() {
				rec, err := v.Match.Finalize(v.gc.FinalizeMatch)
				if err != nil {
					v.abortMatch(err.Error())
				}

				v.matchComplete <- rec
			}()

			goto again
		}

		go v.gc.BeginTurn(v.turnReady)
		v.Match.State.Round++
		v.state = StateNextRoundWait
		v.turnData = nil
		v.animationTimer = 1.5 * 60
		v.myReady = false
		v.theirReady = false

		fallthrough
	case StateNextRoundWait:
		if v.turnData == nil {
			select {
			case t := <-v.turnReady:
				if v.Match.State.Round == 1 {
					v.Match.State.Round--
					v.Match.InitState(v.gc.SCG.Rand(matchnet.RNGShared), v.gc)
					v.Match.State.Round++
				}

				v.turnData = t

				v.Match.ShuffleAndDraw(v.gc.SCG, false)
				v.Match.SyncInHand(v.gc.ExchangeInHand, v.turnData)
				v.Match.ApplyInHand()

				v.myHPAnim = float32(v.Match.State.Sides[v.Match.Perspective-1].HP.Amount)
				v.theirHPAnim = float32(v.Match.State.Sides[2-v.Match.Perspective].HP.Amount)
			default:
			}
		}

		if v.animationTimer != 0 {
			v.animationTimer--
		} else if v.turnData != nil {
			v.resetHandSetup()

			v.state = StateTurnSelect

			v.timerBank += v.timerTurn
			if v.timerBank > v.Match.Timer.MaxTime*60 {
				v.timerBank = v.Match.Timer.MaxTime * 60
			}

			v.timerTurn = v.timerBank
			if v.timerTurn > v.Match.Timer.MaxPerTurn*60 {
				v.timerTurn = v.Match.Timer.MaxPerTurn * 60
			}

			v.timerBank -= v.timerTurn
			v.timerBank += v.Match.Timer.PerTurn * 60

			v.myHand.Focus()

			goto again
		}
	case StateTurnSelect:
		for i, c := range v.myHand.Cards {
			if c.hover == nil {
				continue
			}

			if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
				c.hover.dy = c.hover.dy*0.8 + 0.25*0.2
			} else {
				c.hover.dy *= 0.8
			}
		}

		v.myHand.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirHand.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		endY := float32(h)/128 - 1
		if v.Match.Timer.MaxTime != 0 {
			endY -= 0.5
		}

		wantEndTurn := false

		if sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" End Turn", 0, endY, 0, 1, 1, curX, curY, true) {
			if curX != lastX || curY != lastY {
				*v.myHand.InputFocus = v
			}

			if lastClick && !click && !v.ictx.IsMouseDrag(10) {
				wantEndTurn = true
			}
		} else if *v.myHand.InputFocus == v && (curX != lastX || curY != lastY) {
			*v.myHand.InputFocus = nil
		}

		wantEndTurn = wantEndTurn || v.myHand.ictx.Consume(arcade.BtnSwitch)

		if wantEndTurn && !v.myReady {
			if v.setReady() {
				audio.Confirm1.PlaySound(0, 0, 0)
			} else {
				audio.Buzzer.PlaySound(0, 0, 0)
			}
		}

		if !v.myReady && v.timerTurn != 0 {
			v.timerTurn--

			if v.timerTurn == 0 {
				log.Println("DEBUG: timer ran out")

				if !v.setReady() {
					log.Println("DEBUG: not enough TP to play current hand; playing nothing")

					v.turnData.Played[v.Match.Perspective-1] = nil
					v.turnData.Ready[v.Match.Perspective-1] = 0

					v.setReady()
				} else {
					log.Println("DEBUG: playing current hand as-is")
				}
			}
		}

		v.theirReady = v.gc.OpponentReady()

		if v.gc.RecvReady(v.turnData, v.Match.Perspective) {
			v.beginProcessTurn(ctx)

			goto again
		}
	case StateTurnProcess:
		if v.updateTurn() {
			goto again
		}
	case StateTurnEnd:
		if v.animationTimer == 0 {
			v.state = StateNextRound

			v.myField.Slide = true
			v.theirField.Slide = true

			goto again
		}

		animProgress := (1 - float32(v.animationTimer)/60/2)

		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		switch {
		case animProgress >= 0.25:
			// sweep
			animProgress = (animProgress - 0.25) / 0.75

			if v.Match.Perspective == v.Match.State.RoundWinner {
				for _, c := range v.myField.Cards {
					c.hover.x += float32(w)/128 + animProgress*animProgress*animProgress*float32(w)/4
				}
			} else {
				for _, c := range v.myField.Cards {
					c.hover.x -= animProgress*float32(w)/16 - float32(w)/128
					c.hover.y -= animProgress * animProgress * float32(w) / 8
					c.hover.offX *= 1 + 49*animProgress
					c.hover.offY *= 1 + 49*animProgress
					c.hover.falloff = 1.125 - animProgress
				}
			}

			if 3-v.Match.Perspective == v.Match.State.RoundWinner {
				for _, c := range v.theirField.Cards {
					c.hover.x -= float32(w)/128 + animProgress*animProgress*animProgress*float32(w)/4
				}
			} else {
				for _, c := range v.theirField.Cards {
					c.hover.x += animProgress*float32(w)/16 - float32(w)/128
					c.hover.y -= animProgress * animProgress * float32(w) / 8
					c.hover.offX *= 1 + 49*animProgress
					c.hover.offY *= 1 + 49*animProgress
					c.hover.falloff = 1.125 - animProgress
				}
			}
		case animProgress >= 0:
			// move towards
			for _, c := range v.myField.Cards {
				c.hover.x += animProgress * animProgress * float32(w) / 8
			}

			for _, c := range v.theirField.Cards {
				c.hover.x -= animProgress * animProgress * float32(w) / 8
			}
		}

		v.animationTimer--
	case StateMatchCompleteWait:
		select {
		case rec := <-v.matchComplete:
			_ = rec
			panic("TODO") // TODO
		default:
		}
	case StateMatchComplete:
		// TODO
	case StatePlaybackInit:
		height := 300 / float32(h)

		v.myDeck.MarginTop = 0.5 - height
		v.myDeck.MarginBottom = 0.5
		v.theirDeck.MarginTop = 0.5
		v.theirDeck.MarginBottom = 0.5 - height

		v.myDeck.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirDeck.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		if v.rec.RoundProcessed(0) && (v.ictx.Consume(arcade.BtnSwitch) || (sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, curX, curY, true) && lastClick && !click && !v.ictx.IsMouseDrag(10))) {
			v.state = StatePlaybackIdle
			state, turnData := v.rec.Round(0)
			v.turnData = turnData
			v.Match.State = *state

			v.resetHandSetup()

			goto again
		}
	case StatePlaybackIdle:
		if v.ictx.Consume(arcade.BtnLeft) {
			state, turnData := v.rec.Round(v.Match.State.Round - 2)
			if state != nil {
				v.state = StatePlaybackIdle
				v.turnData = turnData
				v.Match.State = *state

				v.resetHandSetup()

				goto again
			}
		}

		if v.ictx.Consume(arcade.BtnRight) {
			state, turnData := v.rec.Round(v.Match.State.Round)
			if state != nil {
				v.state = StatePlaybackIdle
				v.turnData = turnData
				v.Match.State = *state

				v.resetHandSetup()

				goto again
			}
		}

		if v.turnData != nil && (v.ictx.Consume(arcade.BtnSwitch) || (sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, curX, curY, true) && lastClick && !click && !v.ictx.IsMouseDrag(10))) {
			local, remote := v.rec.Recording.PrivateSeed[0][:], v.rec.Recording.PrivateSeed[1][:]
			if v.Match.Perspective == 2 {
				local, remote = remote, local
			}

			v.gc.SCG.InitTurnSeed(v.rec.Recording.Rounds[v.Match.State.Round-1].TurnSeed[:], local, remote)

			if v.rec.Recording.FormatVersion >= 1 {
				v.gc.SCG.WhenConfirmedTurn(v.rec.Recording.Rounds[v.Match.State.Round-1].TurnSeed2[:])
			}

			v.animationTimer = 1.5 * 60
			v.state = StatePlaybackTurnWait

			goto again
		}

		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)
		v.myHand.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirHand.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		if v.turnData != nil {
			for i, c := range v.myHand.Cards {
				if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
					c.hover.dy = 0.25
				} else {
					c.hover.dy = 0
				}
			}

			for i, c := range v.theirHand.Cards {
				if v.turnData.Ready[2-v.Match.Perspective]&(1<<i) != 0 {
					c.hover.dy = 0.25
				} else {
					c.hover.dy = 0
				}
			}
		}
	case StatePlaybackNext:
		state, turnData := v.rec.Round(v.Match.State.Round)
		if state != nil {
			v.state = StatePlaybackIdle
			v.turnData = turnData
			v.Match.State = *state

			v.resetHandSetup()

			goto again
		}
	case StatePlaybackTurnWait:
		v.myHand.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirHand.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		v.animationTimer--

		if v.animationTimer == 0 {
			v.beginProcessTurn(ctx)

			goto again
		}
	}

	return nil
}

func (v *Visual) Render(ctx context.Context) error {
	touchcontroller.SkipFrame = true

	w, h := gfx.Size()

	if v.sb == nil {
		v.sb = sprites.NewBatch(&v.cam)
	} else {
		v.sb.Reset(&v.cam)
	}

	if v.tb == nil {
		v.tb = sprites.NewTextBatch(&v.cam)
	} else {
		v.tb.Reset(&v.cam)
	}

	switch v.state {
	case StateHome, StateOnHost, StateOnJoin, StateOnQuickJoin, StateOnVariant:
		v.renderHome()
	case StateConnecting, StateWait:
		v.stage.Render()
	case StateCharacterSelect:
		v.mine.Render()
	case StateDeckSelect:
		v.stage.Render()

		v.deck.Render(ctx)
	case StateReadyToStart:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Waiting for opponent to select a deck...", 0, 2.5, 0, 1.5, 1.5, sprites.White, true)
	case StateNextRoundWait:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "ROUND "+strconv.FormatUint(v.Match.State.Round, 10), 0, -1, 0, 3, 3, sprites.Rainbow, true)
	case StateTurnSelect:
		v.stage.Render()

		if v.Match.Timer.MaxTime != 0 {
			v.sb.Append(sprites.Tab, 0, float32(h)/128-0.3, 0, 0.5, -0.5, color.RGBA{63, 63, 63, 255}, 0, 0, 0, 0)

			mins := v.timerTurn / 3600
			secs := v.timerTurn / 60 % 60

			timer := strconv.AppendUint(nil, mins, 10)
			timer = append(timer, ':')

			if secs < 10 {
				timer = append(timer, '0')
			}

			timer = strconv.AppendUint(timer, secs, 10)

			r, gb := uint8(255), uint8(255)

			if (mins == 0 && secs <= 10) || secs == 0 || secs == 59 {
				if secs&1 == 0 && v.timerTurn%60 < 30 {
					gb = 127 + uint8((v.timerTurn%30)*128/30)
				} else if secs&1 == 1 && v.timerTurn%60 >= 30 {
					gb = 127 + uint8((29-v.timerTurn%30)*128/30)
				}
			}

			if v.myReady {
				r, gb = 127, 127
			}

			sprites.DrawTextCentered(v.tb, sprites.FontD3Streetism, string(timer), 0, float32(h)/128-0.55, 0, 0.75, 0.75, color.RGBA{r, gb, gb, 255}, false)
		}

		v.drawHP(0)
		v.drawHP(1)

		canPlay := !v.myReady

		if v.Match.State.Sides[v.Match.Perspective-1].TP.AmountInf < 0 || v.Match.State.Sides[v.Match.Perspective-1].TP.NaN {
			v.sb.Append(sprites.TPInf, -float32(w)/128*0.75+1.5, float32(h)/128-0.75, 0, 1, 1, sprites.White, 0, 0, 0, 0)

			if v.turnData.Ready[v.Match.Perspective-1] != 0 {
				canPlay = false
			}
		} else {
			v.sb.Append(sprites.TP, -float32(w)/128*0.75+1.5, float32(h)/128-0.75, 0, 1, 1, sprites.White, 0, 0, 0, 0)

			tp := "∞"
			tpColor := sprites.White

			if v.Match.State.Sides[v.Match.Perspective-1].TP.AmountInf == 0 {
				remainingTP := v.Match.State.Sides[v.Match.Perspective-1].TP

				for i, c := range v.myHand.Cards {
					if v.turnData.Ready[v.Match.Perspective-1]&(1<<i) != 0 {
						remainingTP.Amount -= c.Def.TP + v.Match.State.Sides[v.Match.Perspective-1].ModTP[c.Def.ID]
					}
				}

				tp = remainingTP.String()

				if remainingTP.Amount < 0 {
					tpColor = color.RGBA{255, 63, 63, 255}

					canPlay = false
				}
			}

			sprites.DrawTextCenteredFuncEx(v.tb, sprites.FontD3Streetism, tp, -float32(w)/128*0.75+1.5, float32(h)/128-1.03125, 0, 1, 1.25, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
			sprites.DrawTextCentered(v.tb, sprites.FontD3Streetism, tp, -float32(w)/128*0.75+1.5, float32(h)/128-1.03125, 0, 1, 1.25, tpColor, false)
		}

		for pass := 0; pass < 3; pass++ {
			v.theirField.Render(ctx, pass)
			v.myField.Render(ctx, pass)
			v.theirHand.Render(ctx, pass)
			v.myHand.Render(ctx, pass)
		}

		endColor := sprites.White

		if *v.myHand.InputFocus == v {
			endColor = sprites.Rainbow
		}

		if !canPlay {
			endColor = sprites.Gray
		}

		if v.myReady {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Ready", -float32(w)/128*0.75, float32(h)/128-2, 0, 0.75, 0.75, color.RGBA{0, 191, 0, 255}, true)
		}

		if v.theirReady {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Ready", float32(w)/128*0.75, float32(h)/128-2, 0, 0.75, 0.75, color.RGBA{0, 191, 0, 255}, true)
		}

		endY := float32(h)/128 - 1
		if v.Match.Timer.MaxTime != 0 {
			endY -= 0.5
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" End Turn", 0, endY, 0, 1, 1, endColor, true)
	case StateTurnProcess:
		v.stage.Render()

		v.drawHP(0)
		v.drawHP(1)

		for pass := 0; pass < 3; pass++ {
			v.myField.Render(ctx, pass)
			v.theirField.Render(ctx, pass)
		}

		if v.Match.State.MaxPriority >= 64 {
			v.drawStats()
		}
	case StateTurnEnd:
		v.stage.Render()

		animProgress := (1 - (float32(v.animationTimer)-2*60)/60/1)
		if animProgress > 1 {
			animProgress = 1
		}

		w, h := gfx.Size()

		myATK := v.Match.State.Sides[v.Match.Perspective-1].FinalATK
		theirATK := v.Match.State.Sides[2-v.Match.Perspective].FinalATK
		myDEF := v.Match.State.Sides[v.Match.Perspective-1].FinalDEF
		theirDEF := v.Match.State.Sides[2-v.Match.Perspective].FinalDEF

		for _, stat := range []*match.Number{&myATK, &theirATK, &myDEF, &theirDEF} {
			if stat.Less(match.Number{}) {
				*stat = match.Number{}
			}
		}

		if animProgress > 0.5 {
			myATK.Add(match.Number{
				Amount:    -theirDEF.Amount,
				AmountInf: -theirDEF.AmountInf,
				NaN:       theirDEF.NaN,
			})

			theirATK.Add(match.Number{
				Amount:    -myDEF.Amount,
				AmountInf: -myDEF.AmountInf,
				NaN:       myDEF.NaN,
			})

			v.drawStat(sprites.ATK, -float32(w)/256, float32(h)/256*(1-(animProgress-0.5)/0.5), myATK)
			v.drawStat(sprites.ATK, float32(w)/256, float32(h)/256*(1-(animProgress-0.5)/0.5), theirATK)

			v.drawStat(sprites.DEF, -float32(w)/256*(3-animProgress*8), -float32(h)/256*(3-animProgress*8), myDEF)
			v.drawStat(sprites.DEF, float32(w)/256*(3-animProgress*8), -float32(h)/256*(3-animProgress*8), theirDEF)
		} else {
			v.drawStat(sprites.ATK, -float32(w)/256, float32(h)/256, myATK)
			v.drawStat(sprites.ATK, float32(w)/256, float32(h)/256, theirATK)

			v.drawStat(sprites.DEF, -float32(w)/256*(1-animProgress*4), -float32(h)/256*(1-animProgress*4), myDEF)
			v.drawStat(sprites.DEF, float32(w)/256*(1-animProgress*4), -float32(h)/256*(1-animProgress*4), theirDEF)
		}

		for pass := 0; pass < 3; pass++ {
			v.myField.Render(ctx, pass)
			v.theirField.Render(ctx, pass)
		}
	case StateMatchCompleteWait:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Verifying Match...", 0, 0, 0, 3, 3, sprites.White, true)
	case StateMatchComplete:
		v.stage.Render()

		v.message = "TODO: finalize match" // TODO
	case StatePlaybackInit:
		v.stage.Render()

		flip := v.rec.Recording.Perspective == 2

		leftPlayer := 0
		if flip {
			leftPlayer = 1
		}

		v.Match.Perspective = uint8(leftPlayer) + 1

		v.sb.MatchOverview(0, 0, 0, 4.75, 4, flip)

		if v.player[leftPlayer] != nil && v.player[leftPlayer].Character != nil {
			v.sb.Append(v.player[leftPlayer].Character.Portrait(), -4, 0, 0, -1.5, 1.5, sprites.White, 0, 0, 0, 0)
		}

		if v.player[1-leftPlayer] != nil && v.player[1-leftPlayer].Character != nil {
			v.sb.Append(v.player[1-leftPlayer].Character.Portrait(), 4, 0, 0, 1.5, 1.5, sprites.White, 0, 0, 0, 0)
		}

		sprites.DrawTextCenteredFuncEx(v.tb, sprites.FontBubblegumSans, "VS", 0, -0.5, 0, 1.5, 1.5, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "VS", 0, -0.5, 0, 1.5, 1.5, sprites.White, false)

		var width float32

		for _, l := range v.recVersionText {
			width += sprites.TextAdvance(sprites.FontBubblegumSans, l, 0.45)
		}

		sprites.DrawTextFuncEx(v.tb, sprites.FontBubblegumSans, v.recTimeText, -4.75, 4.25, 0, 0.45, 0.45, func(rune) color.RGBA { return sprites.Black }, sprites.FlagBorder, 0, 0, 0)
		sprites.DrawText(v.tb, sprites.FontBubblegumSans, v.recTimeText, -4.75, 4.25, 0, 0.45, 0.45, sprites.White)

		sprites.DrawTextFuncEx(v.tb, sprites.FontBubblegumSans, v.recVersionText, 4.75-width, 4.25, 0, 0.45, 0.45, func(rune) color.RGBA { return sprites.Black }, sprites.FlagBorder, 0, 0, 0)
		sprites.DrawText(v.tb, sprites.FontBubblegumSans, v.recVersionText, 4.75-width, 4.25, 0, 0.45, 0.45, sprites.White)

		borderColor := sprites.Black
		borderFlag := sprites.FlagBorder

		mx, my, _ := v.ictx.Mouse()
		if v.rec.RoundProcessed(0) && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, mx, my, true) {
			borderColor = sprites.White
			borderFlag |= sprites.FlagRainbow
		}

		sprites.DrawTextCenteredFuncEx(v.tb, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, func(rune) color.RGBA { return borderColor }, false, borderFlag, 0, 0, 0)

		textColor := sprites.White
		if !v.rec.RoundProcessed(0) {
			textColor = sprites.Gray
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Play Recording", 0, -4.75, 0, 1, 1, textColor, false)
	case StatePlaybackIdle:
		v.stage.Render()

		v.drawHP(0)
		v.drawHP(1)

		for pass := 0; pass < 3; pass++ {
			v.theirField.Render(ctx, pass)
			v.myField.Render(ctx, pass)
			v.theirHand.Render(ctx, pass)
			v.myHand.Render(ctx, pass)
		}

		borderColor := sprites.Black
		borderFlag := sprites.FlagBorder

		mx, my, _ := v.ictx.Mouse()
		if v.turnData != nil && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, mx, my, true) {
			borderColor = sprites.White
			borderFlag |= sprites.FlagRainbow
		}

		sprites.DrawTextCenteredFuncEx(v.tb, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, func(rune) color.RGBA { return borderColor }, false, borderFlag, 0, 0, 0)

		textColor := sprites.White
		if v.turnData == nil {
			textColor = sprites.Gray
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, sprites.Button(arcade.BtnSwitch)+" Next Round", 0, float32(h)/128-1, 0, 1, 1, textColor, false)
	case StatePlaybackNext:
		v.stage.Render()

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Processing...", 0, 0, 0, 3, 3, sprites.White, true)
	case StatePlaybackTurnWait:
		v.stage.Render()

		for pass := 0; pass < 3; pass++ {
			v.theirHand.Render(ctx, pass)
			v.myHand.Render(ctx, pass)
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "ROUND "+strconv.FormatUint(v.Match.State.Round, 10), 0, -1, 0, 3, 3, sprites.Rainbow, true)
	}

	sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.message, 0, 0, 0, 1.5, 1.5, sprites.White, true)
	sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.smallMessage, 0, -1, 0, 0.75, 0.75, sprites.White, true)

	v.updateConnectionState()

	v.sb.Render()
	v.tb.Render()

	if v.state == StatePlaybackInit {
		for pass := 0; pass < 3; pass++ {
			v.myDeck.Render(ctx, pass)
			v.theirDeck.Render(ctx, pass)
		}
	}

	return nil
}

func (v *Visual) preloadPortraits(ctx context.Context) {
	for _, def := range v.Match.Set.Cards {
		c := Card{
			Card: &match.Card{
				Set:  v.Match.Set,
				Def:  def,
				Back: def.Rank.Back(),
			},
		}

		c.initPortraitSprite(ctx)
		_ = c.portraitSprite.Preload()
	}
}

func (v *Visual) preloadMusic(ctx context.Context) {
	anySummon := false

	for _, f := range v.Match.Set.Mode.GetAll(card.FieldSummonCard) {
		sc := f.(*card.SummonCard)
		c := v.Match.Set.Card(sc.ID)

		for _, e := range c.Extensions {
			if e.Type == card.ExtensionSetMusic {
				_, err := getCustomMusic(ctx, e.CID, e.LoopStart, e.LoopEnd)
				if err != nil {
					log.Println("ERROR: fetching custom music for card", c.DisplayName(), "failed:", err)
				}

				anySummon = true
			}
		}
	}

	if !anySummon {
		if v.Match.Init.Mode == match.ModeCustom {
			err := audio.Bounty.Preload()
			if err != nil {
				log.Println("ERROR: fetching default music for custom mode:", err)
			}
		} else {
			err := audio.Miniboss.Preload()
			if err != nil {
				log.Println("ERROR: fetching default music:", err)
			}
		}
	}

	// fetch music that isn't summoned as a pre-game setup last
	for _, c := range v.Match.Set.Cards {
		for _, e := range c.Extensions {
			if e.Type == card.ExtensionSetMusic {
				_, err := getCustomMusic(ctx, e.CID, e.LoopStart, e.LoopEnd)
				if err != nil {
					log.Println("ERROR: fetching custom music for card", c.DisplayName(), "failed:", err)
				}
			}
		}
	}
}

func (v *Visual) playDefaultMusic(ctx context.Context) {
	for _, f := range v.Match.Set.Mode.GetAll(card.FieldSummonCard) {
		sc := f.(*card.SummonCard)
		c := v.Match.Set.Card(sc.ID)

		for _, e := range c.Extensions {
			if e.Type == card.ExtensionSetMusic {
				v.playCustomMusic(ctx, e.CID, e.LoopStart, e.LoopEnd)

				return
			}
		}
	}

	if v.Match.Init.Mode == match.ModeCustom {
		audio.Bounty.PlayMusic(0, false)
	} else {
		audio.Miniboss.PlayMusic(0, false)
	}
}

var musicCache = internal.Cache{MaxEntries: 32}

func getCustomMusic(ctx context.Context, cid card.ContentIdentifier, start, end float32) (*audio.Sound, error) {
	path := internal.GetConfig(ctx).IPFSBaseURL + cid.String()

	music, err := musicCache.Do(path, func() (interface{}, error) {
		s := &audio.Sound{
			Name:  path,
			Loop:  true,
			Start: float64(start),
			End:   float64(end),
		}

		return s, s.Preload()
	})
	if err != nil {
		return nil, xerrors.Errorf("match: failed to load custom music: %w", err)
	}

	return music.(*audio.Sound), nil
}

func (v *Visual) playCustomMusic(ctx context.Context, cid card.ContentIdentifier, start, end float32) {
	v.usingCustomMusic = true

	music, err := getCustomMusic(ctx, cid, start, end)
	if err != nil {
		log.Println("ERROR: can't play music:", err)

		return
	}

	music.PlayMusic(0, false)
}

func (v *Visual) abortMatch(reason string) {
	audio.StopMusic()

	v.state = StateWait
	v.message = "Desync Detected"
	v.smallMessage = reason
}

func (v *Visual) reloadDefaultStage() {
	var cid card.ContentIdentifier

	if variant := v.Match.Init.CachedVariant; variant != nil {
		for _, f := range variant.Rules {
			if sc, ok := f.(*card.SummonCard); ok {
				c := v.Match.Set.Card(sc.ID)

				for _, e := range c.Extensions {
					if e.Type == card.ExtensionSetStage {
						cid = e.CID

						break
					}
				}
			}

			if cid != nil {
				break
			}
		}
	}

	if cid == nil {
		for _, f := range v.Match.Set.Mode.GetAll(card.FieldSummonCard) {
			sc := f.(*card.SummonCard)
			c := v.Match.Set.Card(sc.ID)

			for _, e := range c.Extensions {
				if e.Type == card.ExtensionSetStage {
					cid = e.CID

					break
				}
			}

			if cid != nil {
				break
			}
		}
	}

	v.usingCustomStage = cid != nil

	if err := v.stage.SetStage(context.Background(), cid); err != nil {
		log.Println("WARNING: loading stage failed:", err)
	}
}

func preloadMatchAssets() {
	for _, f := range []func() error{
		sprites.CardFront.Preload,
		sprites.CardFrontWindow.Preload,
		sprites.Portraits[0].Preload,
		sprites.HP.Preload,
		audio.Confirm1.Preload,
		room.PreloadMine,
		audio.Confirm.Preload,
		audio.Buzzer.Preload,
		audio.PageFlip.Preload,
		audio.Coin.Preload,
		audio.AtkSuccess.Preload,
		audio.AtkFail.Preload,
		audio.CardSound2.Preload,
		audio.Lazer.Preload,
		audio.CrowdClap.Preload,
		audio.CrowdGasp.Preload,
		audio.CrowdCheer2.Preload,
		audio.Toss11.Preload,
		audio.Damage0.Preload,
		audio.Death3.Preload,
		audio.Fail.Preload,
		audio.Heal.Preload,
	} {
		f := f // shadow loop variable

		go func() {
			if err := f(); err != nil {
				log.Printf("ERROR: during preload: %+v", err)
			}
		}()
	}
}

func (v *Visual) drawHP(player uint8) {
	w, h := gfx.Size()

	side := float32(1)
	smooth := &v.theirHPAnim

	if player == v.Match.Perspective-1 {
		side = -1
		smooth = &v.myHPAnim
	}

	hp := v.Match.State.Sides[player].HP

	*smooth = *smooth*0.9 + float32(hp.Amount)*0.1
	hp.Amount = int64(math.Round(float64(*smooth)))

	var tilt float32

	if len(v.cardAnimation) != 0 && v.cardAnimation[0].Effect.Type == card.EffectHeal {
		healPlayer := v.cardAnimation[0].Player
		if v.cardAnimation[0].Effect.Flags&card.FlagHealOpponent != 0 {
			healPlayer = 1 - healPlayer
		}

		if player == healPlayer {
			v.healAmount *= 0.8

			tilt = v.healAmount
		}
	} else {
		v.healAmount = 0
	}

	shade := color.RGBA{255, 255, 255, 255}

	animTimer := uint8((1 - float32(v.animationTimer)/60/0.5) * 255)

	if tilt > 0 {
		shade.R = animTimer
		shade.B = animTimer
	} else if tilt < 0 {
		shade.G = animTimer
		shade.B = animTimer
	}

	scale := float32(math.Abs(float64(tilt))) / 10

	if tilt >= 0 {
		tilt = 0
	} else {
		tilt /= -5
	}

	v.sb.Append(sprites.HP, side*float32(w)/128*0.75, float32(h)/128-0.75, 0, 0.75+scale, 0.75+scale, sprites.White, 0, 0, side*tilt, 0)

	hpStr := hp.String()

	sprites.DrawTextCenteredFuncEx(v.tb, sprites.FontD3Streetism, hpStr, side*float32(w)/128*0.75, float32(h)/128-1.03125, 0, 1, 1.25, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
	sprites.DrawTextCentered(v.tb, sprites.FontD3Streetism, hpStr, side*float32(w)/128*0.75, float32(h)/128-1.03125, 0, 1, 1.25, shade, false)
}

func (v *Visual) drawStats() {
	w, h := gfx.Size()

	v.drawStat(sprites.ATK, -float32(w)/256, float32(h)/256, v.Match.State.Sides[v.Match.Perspective-1].ATK)
	v.drawStat(sprites.ATK, float32(w)/256, float32(h)/256, v.Match.State.Sides[2-v.Match.Perspective].ATK)
	v.drawStat(sprites.DEF, -float32(w)/256, -float32(h)/256, v.Match.State.Sides[v.Match.Perspective-1].DEF)
	v.drawStat(sprites.DEF, float32(w)/256, -float32(h)/256, v.Match.State.Sides[2-v.Match.Perspective].DEF)
}

func (v *Visual) drawStat(sprite *sprites.Sprite, x, y float32, num match.Number) {
	v.sb.Append(sprite, x, y, 0, 1, 1, sprites.White, 0, 0, 0, 0)

	if num.Less(match.Number{}) {
		num = match.Number{}
	}

	str := num.String()

	sprites.DrawTextCenteredFuncEx(v.tb, sprites.FontD3Streetism, str, x, y-0.5, 0, 2, 2, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
	sprites.DrawTextCentered(v.tb, sprites.FontD3Streetism, str, x, y-0.5, 0, 2, 2, sprites.White, false)
}

func (v *Visual) checkExtensionFields(ctx context.Context) {
	var song, stage *card.Extension

	for player := range v.Match.State.Sides {
		for _, c := range v.Match.State.Sides[player].Field {
			for _, e := range c.Card.Def.Extensions {
				override := e.Flags&card.ExtensionOverrideCustom != 0

				switch e.Type {
				case card.ExtensionSetMusic:
					if override || !v.usingCustomMusic {
						song = e
					}
				case card.ExtensionSetStage:
					if override || !v.usingCustomStage {
						stage = e
					}
				}
			}
		}
	}

	if song != nil {
		v.usingCustomMusic = true

		v.playCustomMusic(ctx, song.CID, song.LoopStart, song.LoopEnd)
	}

	if stage != nil {
		v.usingCustomStage = true

		if err := v.stage.SetStage(ctx, stage.CID); err != nil {
			log.Println("ERROR: setting stage:", err)
		}
	}
}

func (v *Visual) updateConnectionState() {
	w, h := gfx.Size()

	if v.state != StateConnecting && v.gc != nil {
		var message string

		switch cs, _ := v.gc.State(); cs {
		case matchnet.StateRTCConnecting:
			message = "Checking connection to opponent..."
		case matchnet.StateRTCDisconnected:
			message = "Lost connection to opponent. Attempting to reestablish..."
		case matchnet.StateRTCFailed, matchnet.StateRelayFailed:
			message = "Failed to connect to opponent!"
		}

		if message != "" {
			if v.networkErrorTimer >= 255-20 {
				v.networkErrorTimer = 255
			} else {
				v.networkErrorTimer += 20
			}

			v.sb.Append(sprites.Description, float32(w)/128-2, -float32(h)/128+2.6*float32(v.networkErrorTimer)/255-1.3, 0, 1.5, 1.5, color.RGBA{255, 95, 63, 255}, 0, 0, 0, 0)

			(&card.RichDescription{
				Text:  message,
				Color: sprites.White,
			}).Typeset(sprites.FontBubblegumSans, float32(w)/128-3.7, -float32(h)/128+2.6*float32(v.networkErrorTimer)/255-0.7, 0, 0.65, 0.65, 3.5, 2, nil, false, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
				sprites.DrawText(v.tb, sprites.FontBubblegumSans, text, x, y, z, sx, sy, tint)
			})
		} else {
			if v.networkErrorTimer <= 30 {
				v.networkErrorTimer = 0
			} else {
				v.networkErrorTimer -= 30
			}

			v.sb.Append(sprites.Description, float32(w)/128-2, -float32(h)/128+2.6*float32(v.networkErrorTimer)/255-1.3, 0, 1.5, 1.5, color.RGBA{191, 191, 191, 255}, 0, 0, 0, 0)
		}
	}
}

func (v *Visual) resetHandSetup() {
	v.myHand.Cards = make([]*Card, len(v.Match.State.Sides[v.Match.Perspective-1].Hand))
	v.theirHand.Cards = make([]*Card, len(v.Match.State.Sides[2-v.Match.Perspective].Hand))
	v.myField.Cards = make([]*Card, 0, len(v.Match.State.Sides[v.Match.Perspective-1].Setup))
	v.theirField.Cards = make([]*Card, 0, len(v.Match.State.Sides[2-v.Match.Perspective].Setup))

	for i, c := range v.Match.State.Sides[v.Match.Perspective-1].Hand {
		v.myHand.Cards[i] = &Card{Card: c.Card}
	}

	for i, c := range v.Match.State.Sides[2-v.Match.Perspective].Hand {
		v.theirHand.Cards[i] = &Card{Card: c.Card}

		if v.Match.UnknownDeck != 0 && c.Card.Def != nil {
			visible := false

			for _, e := range c.Card.Def.Effects {
				if e.Type == card.CondInHand && e.Flags&card.FlagCondInHandHide == 0 {
					visible = true

					break
				}
			}

			if !visible {
				v.theirHand.Cards[i].flip = 255
				v.theirHand.Cards[i].flipDir = 0
			} else if c.InHandTurns == 1 {
				v.theirHand.Cards[i].flip = 255
				v.theirHand.Cards[i].SetFlip(false)
			}
		}
	}

	for _, c := range v.Match.State.Sides[v.Match.Perspective-1].Setup {
		if c.Mode.Hide() {
			continue
		}

		vc := &Card{Card: c.Card, Active: c}
		v.myField.Cards = append(v.myField.Cards, vc)
	}

	for _, c := range v.Match.State.Sides[2-v.Match.Perspective].Setup {
		if c.Mode.Hide() {
			continue
		}

		vc := &Card{Card: c.Card, Active: c}
		v.theirField.Cards = append(v.theirField.Cards, vc)
	}
}

func (v *Visual) updateTurn() bool {
	_, _, click := v.ictx.Mouse()

	if v.animationTimer != 0 {
		if !click {
			v.animationTimer--
		}

		v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
		v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

		return false
	}

	if v.cardAnimation != nil {
		v.cardAnimation[0].Card.ActiveEffect = nil
	}

	rng := v.gc.SCG.Rand(matchnet.RNGShared)

	ce := v.Match.ProcessQueuedEffect(rng, v.gc)
	for len(ce) != 0 && ((ce[0].Effect.Type >= 128 && ce[0].Effect.Type != card.CondCoin) || (ce[0].Effect.Priority == 0 && ce[0].Effect.Type != card.EffectSummon) || v.activeCard[ce[0].Card] == nil) {
		ce = v.Match.ProcessQueuedEffect(rng, v.gc)
	}

	if len(ce) == 0 {
		v.animationTimer = 60 * 3
		v.state = StateTurnEnd

		audio.Toss11.PlaySound(0, 0, 0)

		switch {
		case v.Match.State.RoundWinner == v.Match.Perspective:
			audio.CrowdCheer2.PlaySound(1.5, 0, 0)
			audio.Damage0.PlaySound(2, 0, 0)
		case v.Match.State.RoundWinner == 0:
			audio.Fail.PlaySound(1.5, 0, 0)
		default:
			audio.CrowdGasp.PlaySound(2, 0, 0)
			audio.Death3.PlaySound(2, 0, 0)
		}

		if v.Match.State.RoundWinner == v.Match.Perspective && v.player[2-v.Match.Perspective] != nil {
			v.player[2-v.Match.Perspective].BecomeAngry(5 * time.Second)
		}

		if v.Match.State.RoundWinner == 3-v.Match.Perspective && v.player[v.Match.Perspective-1] != nil {
			v.player[v.Match.Perspective-1].BecomeAngry(5 * time.Second)
		}

		v.myField.Slide = false
		v.theirField.Slide = false

		if v.Match.State.RoundWinner != 0 || v.Match.State.Sides[0].HP.Less(match.Number{Amount: int64(v.Match.Rules.MaxHP) / 2}) || v.Match.State.Sides[1].HP.Less(match.Number{Amount: int64(v.Match.Rules.MaxHP) / 2}) {
			for _, a := range v.audience {
				a.StartCheer(5 * time.Second)
			}
		}

		return true
	}

	v.animationTimer = 0.5 * 60
	v.cardAnimation = ce

	ce[0].Card.ActiveEffect = ce[0].Effect

	switch ce[0].Effect.Type {
	case card.EffectStat, card.EffectEmpower, card.EffectRawStat:
		audio.CardSound2.PlaySound(0, 0, 0)

		v.animationTimer = 0.25 * 60
	case card.EffectSummon, card.CondApply:
		if ce[0].Effect.Type == card.EffectSummon && ce[0].Effect.Flags&card.FlagSummonReplace != 0 {
			if ce[0].Effect.Priority != 0 {
				audio.PageFlip.PlaySound(0, 0, 0)
			}

			field := v.theirField
			if ce[0].Player == v.Match.Perspective-1 {
				field = v.myField
			}

			for i := 0; i < len(field.Cards); i++ {
				if field.Cards[i].Active == ce[0].Card {
					if ce[0].Effect.Flags&(card.FlagSummonInvisible|card.FlagSummonOpponent) == 0 && len(ce) > 1 {
						var h *HoverTilt

						if field.Cards[i].hover != nil {
							h = new(HoverTilt)
							*h = *field.Cards[i].hover
						}

						delete(v.activeCard, ce[0].Card)

						vc := &Card{
							Card:   ce[1].Card.Card,
							Active: ce[1].Card,
							hover:  h,
						}

						v.activeCard[ce[1].Card] = vc
						field.Cards[i] = vc

						field.Cards[i].flip = 255
						field.Cards[i].SetFlip(false)
					} else {
						delete(v.activeCard, ce[0].Card)
						field.Cards = append(field.Cards[:i], field.Cards[i+1:]...)
					}

					break
				}
			}
		}

		if ce[0].Effect.Type != card.EffectSummon || ce[0].Effect.Flags&card.FlagSummonInvisible == 0 {
			var hover *HoverTilt

			if vc, ok := v.activeCard[ce[0].Card]; ok {
				if vc.hover != nil {
					hover = vc.hover
				}
			}

			for i := 1; i < len(ce); i++ {
				if ce[0].Effect.Priority != 0 {
					audio.CardSound2.PlaySound((float64(i)-1)*0.05, 1+(float64(i)-1)*0.1, 0)
				}

				if i == 1 && ce[0].Effect.Type == card.EffectSummon && ce[0].Effect.Flags&(card.FlagSummonReplace|card.FlagSummonInvisible|card.FlagSummonOpponent) == card.FlagSummonReplace {
					continue
				}

				_, found := v.activeCard[ce[i].Card]
				if found {
					continue
				}

				field := v.theirField
				if ce[i].Player == v.Match.Perspective-1 {
					field = v.myField
				}

				var h *HoverTilt

				if hover != nil {
					h = new(HoverTilt)
					*h = *hover
				}

				vc := &Card{
					Card:   ce[i].Card.Card,
					Active: ce[i].Card,
					hover:  h,
				}

				v.activeCard[ce[i].Card] = vc
				field.Cards = append(field.Cards, vc)
			}
		}

		if ce[0].Effect.Priority == 0 {
			v.animationTimer = 0
		} else {
			v.animationTimer = 0.75 * 60
		}
	case card.EffectHeal:
		v.healAmount = float32(ce[0].Effect.Amount)

		player := ce[0].Player
		if ce[0].Effect.Flags&card.FlagHealOpponent != 0 {
			player = 1 - player
		}

		if ce[0].Effect.Amount < 0 {
			v.healAmount *= float32(v.Match.State.Sides[player].HealMulN)
		} else {
			v.healAmount *= float32(v.Match.State.Sides[player].HealMulP)
		}

		if v.healAmount > 0 {
			audio.Heal.PlaySound(0, 0, 0)
		} else {
			audio.Damage0.PlaySound(0, 0, 0)
		}

		if ce[0].Effect.Flags&card.FlagHealInfinity != 0 {
			v.healAmount = 0
		}
	case card.EffectNumb:
		for i := 1; i < len(ce); i++ {
			audio.Lazer.PlaySound(0.1*float64(i-1), 1+0.05*float64(i-1), 0)
		}
	case card.CondCoin:
		visual := v.activeCard[ce[0].Card]

		for i := 1; i < len(ce); i++ {
			audio.Coin.PlaySound(0.1*float64(i-1), 1+0.05*float64(i-1), 0)

			if ce[0].Effect.Flags&card.FlagCondCoinStat == 0 {
				if ce[i].Effect == ce[0].Effect.Result2 {
					audio.AtkFail.PlaySound(0.5+float64(i)*0.25, 0, 0)
				} else {
					audio.AtkSuccess.PlaySound(0.5+float64(i)*0.25, 0, 0)
				}
			}

			if visual != nil {
				heads := ce[i].Effect != ce[0].Effect.Result2
				if ce[0].Effect.Flags&card.FlagCondCoinNegative != 0 {
					heads = !heads
				}

				visual.coins = append(visual.coins, &coin{
					invisibleTimer: 60 * 0.1 * uint64(i-1),
					revealTimer:    60*0.5 + 60*0.25*uint64(i),
					stat:           ce[0].Effect.Flags&card.FlagCondCoinStat != 0,
					heads:          heads,
				})
			}
		}

		v.animationTimer = 0.25*60 + 0.25*60*uint64(len(ce))
	default:
		log.Printf("TODO: animate effect: %+v %+v", ce[1:], *ce[0].Effect)
	}

	v.myField.Update(v.Match.State.Sides[v.Match.Perspective-1].ModTP)
	v.theirField.Update(v.Match.State.Sides[2-v.Match.Perspective].ModTP)

	return false
}

func (v *Visual) beginProcessTurn(ctx context.Context) {
	v.state = StateTurnProcess
	v.animationTimer = 0.5 * 60

	v.Match.BeginTurn(v.turnData)

	v.checkExtensionFields(ctx)

	for k := range v.activeCard {
		delete(v.activeCard, k)
	}

	v.myField.Cards = make([]*Card, 0, len(v.Match.State.Sides[v.Match.Perspective-1].Field))
	v.theirField.Cards = make([]*Card, 0, len(v.Match.State.Sides[2-v.Match.Perspective].Field))

	hover := make([]*HoverTilt, len(v.turnData.Played[v.Match.Perspective-1]))
	ready := v.turnData.Ready[v.Match.Perspective-1]

	for i := range hover {
		j := bits.TrailingZeros64(ready)
		ready &^= 1 << j

		if len(v.myHand.Cards) > j && v.myHand.Cards[j].hover != nil {
			hover[i] = &HoverTilt{}
			*hover[i] = *v.myHand.Cards[j].hover

			hover[i].y += hover[i].dy
			hover[i].dy = 0
		}
	}

	for _, c := range v.Match.State.Sides[v.Match.Perspective-1].Field {
		if c.Mode.Hide() {
			continue
		}

		vc := &Card{Card: c.Card, Active: c}

		if c.Mode == match.ModeDefault && len(hover) != 0 {
			vc.hover = hover[0]
			hover = hover[1:]
		}

		v.activeCard[c] = vc
		v.myField.Cards = append(v.myField.Cards, vc)
	}

	hover = make([]*HoverTilt, len(v.turnData.Played[2-v.Match.Perspective]))
	ready = v.turnData.Ready[2-v.Match.Perspective]

	for i := range hover {
		j := bits.TrailingZeros64(ready)
		ready &^= 1 << j

		if len(v.theirHand.Cards) > j && v.theirHand.Cards[j].hover != nil {
			hover[i] = &HoverTilt{}
			*hover[i] = *v.theirHand.Cards[j].hover

			hover[i].y += hover[i].dy
			hover[i].dy = 0
		}
	}

	for _, c := range v.Match.State.Sides[2-v.Match.Perspective].Field {
		if c.Mode.Hide() {
			continue
		}

		vc := &Card{Card: c.Card, Active: c}

		if c.Mode == match.ModeDefault && len(hover) != 0 {
			if v.rec == nil {
				vc.flip = 255
				vc.SetFlip(false)
			}

			vc.hover = hover[0]
			hover = hover[1:]
		}

		v.activeCard[c] = vc
		v.theirField.Cards = append(v.theirField.Cards, vc)
	}
}
