"use strict";
var SpyCards;
(function (SpyCards) {
    let Button;
    (function (Button) {
        Button[Button["Up"] = 0] = "Up";
        Button[Button["Down"] = 1] = "Down";
        Button[Button["Left"] = 2] = "Left";
        Button[Button["Right"] = 3] = "Right";
        Button[Button["Confirm"] = 4] = "Confirm";
        Button[Button["Cancel"] = 5] = "Cancel";
        Button[Button["Switch"] = 6] = "Switch";
        Button[Button["Toggle"] = 7] = "Toggle";
        Button[Button["Pause"] = 8] = "Pause";
        Button[Button["Help"] = 9] = "Help";
    })(Button = SpyCards.Button || (SpyCards.Button = {}));
    SpyCards.defaultKeyButton = {
        [Button.Up]: "ArrowUp",
        [Button.Down]: "ArrowDown",
        [Button.Left]: "ArrowLeft",
        [Button.Right]: "ArrowRight",
        [Button.Confirm]: "KeyC",
        [Button.Cancel]: "KeyX",
        [Button.Switch]: "KeyZ",
        [Button.Toggle]: "KeyV",
        [Button.Pause]: "Escape",
        [Button.Help]: "Enter",
    };
    // see https://w3c.github.io/gamepad/standard_gamepad.svg
    SpyCards.standardGamepadButton = {
        [Button.Up]: 12,
        [Button.Down]: 13,
        [Button.Left]: 14,
        [Button.Right]: 15,
        [Button.Confirm]: 0,
        [Button.Cancel]: 1,
        [Button.Switch]: 2,
        [Button.Toggle]: 3,
        [Button.Pause]: 9,
        [Button.Help]: 8,
    };
    const buttonPressed = {};
    const buttonWasPressed = {};
    SpyCards.buttonHeld = {};
    const keyHeld = {};
    let lastKeyboard = 0;
    let lastGamepad = 0;
    let inputTicks = 0;
    let ButtonStyle;
    (function (ButtonStyle) {
        ButtonStyle[ButtonStyle["Keyboard"] = 0] = "Keyboard";
        ButtonStyle[ButtonStyle["GenericGamepad"] = 1] = "GenericGamepad";
    })(ButtonStyle = SpyCards.ButtonStyle || (SpyCards.ButtonStyle = {}));
    let lastGamepadStyle = ButtonStyle.GenericGamepad;
    function getButtonStyle() {
        if (!lastGamepad || lastGamepad < lastKeyboard) {
            return ButtonStyle.Keyboard;
        }
        return lastGamepadStyle;
    }
    SpyCards.getButtonStyle = getButtonStyle;
    function saveInputState() {
        const p = {};
        const w = {};
        const h = {};
        for (let btn = Button.Up; btn <= Button.Help; btn++) {
            p[btn] = buttonPressed[btn];
            w[btn] = buttonWasPressed[btn];
            h[btn] = SpyCards.buttonHeld[btn];
        }
        return { t: inputTicks, p, w, h };
    }
    SpyCards.saveInputState = saveInputState;
    function loadInputState(s) {
        inputTicks = s.t;
        for (let btn = Button.Up; btn <= Button.Help; btn++) {
            buttonPressed[btn] = s.p[btn];
            buttonWasPressed[btn] = s.w[btn];
            SpyCards.buttonHeld[btn] = s.h[btn];
        }
    }
    SpyCards.loadInputState = loadInputState;
    function updateButtons(force) {
        if (force) {
            force(SpyCards.buttonHeld);
        }
        else if (SpyCards.updateAIButtons) {
            SpyCards.updateAIButtons(SpyCards.buttonHeld);
        }
        else {
            updateHeldButtons();
        }
        inputTicks++;
        for (let button = Button.Up; button <= Button.Help; button++) {
            if (!SpyCards.buttonHeld[button]) {
                buttonPressed[button] = false;
                buttonWasPressed[button] = 0;
            }
            else if (buttonWasPressed[button] < inputTicks) {
                buttonWasPressed[button] = Infinity;
                buttonPressed[button] = true;
            }
        }
    }
    SpyCards.updateButtons = updateButtons;
    function updateHeldButtons() {
        const controlsSettings = SpyCards.loadSettings().controls || {};
        const gamepadSettings = controlsSettings.gamepad || {};
        let keyboardMap = SpyCards.defaultKeyButton;
        if (controlsSettings.customKB && controlsSettings.keyboard > 0 && controlsSettings.customKB[controlsSettings.keyboard - 1]) {
            keyboardMap = controlsSettings.customKB[controlsSettings.keyboard - 1].code;
        }
        for (let button = Button.Up; button <= Button.Help; button++) {
            SpyCards.buttonHeld[button] = false;
            if (keyHeld[keyboardMap[button]]) {
                SpyCards.buttonHeld[button] = true;
                lastKeyboard = Date.now();
            }
            for (let gp of navigator.getGamepads()) {
                if (!gp) {
                    continue;
                }
                let mapping = SpyCards.standardGamepadButton;
                let style = ButtonStyle.GenericGamepad;
                if (controlsSettings.customGP && gamepadSettings[gp.id] > 0 && controlsSettings.customGP[gamepadSettings[gp.id] - 1]) {
                    mapping = controlsSettings.customGP[gamepadSettings[gp.id] - 1].button;
                    style = controlsSettings.customGP[gamepadSettings[gp.id] - 1].style;
                }
                else if (gp.mapping !== "standard") {
                    continue;
                }
                const mapped = mapping[button];
                let pressed;
                if (Array.isArray(mapped)) {
                    const axis = gp.axes[mapped[0]];
                    if (mapped[1]) {
                        pressed = axis > 0.5;
                    }
                    else {
                        pressed = axis < -0.5;
                    }
                }
                else {
                    const gpButton = gp.buttons[mapped];
                    pressed = gpButton && gpButton.pressed;
                }
                if (pressed) {
                    SpyCards.buttonHeld[button] = true;
                    lastGamepad = Date.now();
                    lastGamepadStyle = style;
                }
            }
        }
    }
    function consumeButton(btn) {
        if (buttonPressed[btn]) {
            buttonPressed[btn] = false;
            return true;
        }
        return false;
    }
    SpyCards.consumeButton = consumeButton;
    function consumeButtonAllowRepeat(btn, delay = 0.1) {
        if (buttonPressed[btn]) {
            buttonPressed[btn] = false;
            buttonWasPressed[btn] = inputTicks + delay * 60;
            return true;
        }
        return false;
    }
    SpyCards.consumeButtonAllowRepeat = consumeButtonAllowRepeat;
    function isAllowedKeyCode(code) {
        // don't grab OS/Browser special keys
        if (code.startsWith("Alt") ||
            code.startsWith("Control") ||
            code.startsWith("Shift") ||
            code.startsWith("Meta") ||
            code.startsWith("OS") ||
            code.startsWith("Browser") ||
            code.startsWith("Launch") ||
            code.startsWith("Audio") ||
            code.startsWith("Media") ||
            code.startsWith("Volume") ||
            code.startsWith("Lang") ||
            code.startsWith("Page") ||
            code.endsWith("Mode") ||
            code.endsWith("Lock") ||
            /^F[0-9]+$/.test(code) ||
            code === "" ||
            code === "Unidentified" ||
            code === "PrintScreen" ||
            code === "Power" ||
            code === "Pause" ||
            code === "ContextMenu" ||
            code === "Help" ||
            code === "Fn" ||
            code === "Tab" ||
            code === "Home" ||
            code === "End") {
            return false;
        }
        // allow letters, numbers, and some symbols.
        if (/^Key[A-Z]$|^Digit[0-9]$|^Numpad|^Intl|Up$|Down$|Left$|Right$/.test(code)) {
            return true;
        }
        // additional symbols and special keys listed by name:
        switch (code) {
            case "Escape":
            case "Minus":
            case "Equal":
            case "Backspace":
            case "Enter":
            case "Semicolon":
            case "Quote":
            case "Backquote":
            case "Backslash":
            case "Comma":
            case "Period":
            case "Slash":
            case "Space":
            case "Insert":
            case "Delete":
                return true;
            default:
                console.warn("Unhandled keycode " + code + "; ignoring for safety.");
                debugger;
                return false;
        }
    }
    const awaitingKeyPress = [];
    addEventListener("keydown", (e) => {
        if (SpyCards.disableKeyboard) {
            return;
        }
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return;
        }
        if ("value" in e.target) {
            return;
        }
        if (isAllowedKeyCode(e.code)) {
            e.preventDefault();
            keyHeld[e.code] = true;
            for (let resolve of awaitingKeyPress) {
                resolve(e.code);
            }
            awaitingKeyPress.length = 0;
        }
    });
    addEventListener("keyup", (e) => {
        if (SpyCards.disableKeyboard) {
            return;
        }
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return;
        }
        if ("value" in e.target) {
            return;
        }
        if (isAllowedKeyCode(e.code)) {
            e.preventDefault();
            keyHeld[e.code] = false;
        }
    });
    function nextPressedKey() {
        return new Promise((resolve) => awaitingKeyPress.push(resolve));
    }
    SpyCards.nextPressedKey = nextPressedKey;
    function nextPressedButton() {
        const alreadyHeld = [];
        for (let gp of navigator.getGamepads()) {
            for (let i = 0; i < gp.buttons.length; i++) {
                if (gp.buttons[i].pressed) {
                    alreadyHeld.push(gp.id + ":" + i);
                }
            }
            for (let i = 0; i < gp.axes.length; i++) {
                if (gp.axes[i] > 0.5) {
                    alreadyHeld.push(gp.id + ":+" + i);
                }
                if (gp.axes[i] < -0.5) {
                    alreadyHeld.push(gp.id + ":-" + i);
                }
            }
        }
        return new Promise((resolve) => {
            requestAnimationFrame(function check() {
                for (let gp of navigator.getGamepads()) {
                    for (let i = 0; i < gp.buttons.length; i++) {
                        const already = alreadyHeld.indexOf(gp.id + ":" + i);
                        if (already === -1 && gp.buttons[i].pressed) {
                            return resolve(i);
                        }
                        if (already !== -1 && !gp.buttons[i].pressed) {
                            alreadyHeld.splice(already, 1);
                        }
                    }
                    for (let i = 0; i < gp.axes.length; i++) {
                        const alreadyP = alreadyHeld.indexOf(gp.id + ":+" + i);
                        const alreadyN = alreadyHeld.indexOf(gp.id + ":-" + i);
                        if (alreadyP === -1 && gp.axes[i] > 0.5) {
                            return resolve([i, true]);
                        }
                        if (alreadyN === -1 && gp.axes[i] < -0.5) {
                            return resolve([i, false]);
                        }
                        if (alreadyP !== -1 && gp.axes[i] <= 0.5) {
                            alreadyHeld.splice(alreadyP, 1);
                        }
                        if (alreadyN !== -1 && gp.axes[i] >= -0.5) {
                            alreadyHeld.splice(alreadyN, 1);
                        }
                    }
                }
                requestAnimationFrame(check);
            });
        });
    }
    SpyCards.nextPressedButton = nextPressedButton;
})(SpyCards || (SpyCards = {}));
