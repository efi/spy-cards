package internal

import "context"

// Config holds data about external services used by Spy Cards Online.
type Config struct {
	MatchmakingServer     string `json:"matchmaking_server"`
	UserImageBaseURL      string `json:"user_image_base_url"`
	CustomCardAPIBaseURL  string `json:"custom_card_api_base_url"`
	MatchRecordingBaseURL string `json:"match_recording_base_url"`
	ArcadeAPIBaseURL      string `json:"arcade_api_base_url"`
	IPFSBaseURL           string `json:"ipfs_base_url"`
}

// DefaultConfig is the Config used by the official Spy Cards Online site.
var DefaultConfig = &Config{
	MatchmakingServer:     "wss://spy-cards.lubar.me/spy-cards/ws",
	UserImageBaseURL:      "https://spy-cards.lubar.me/spy-cards/user-img/",
	CustomCardAPIBaseURL:  "https://spy-cards.lubar.me/spy-cards/custom/api/",
	MatchRecordingBaseURL: "https://spy-cards.lubar.me/spy-cards/recording/",
	ArcadeAPIBaseURL:      "https://spy-cards.lubar.me/spy-cards/arcade/api/",
	IPFSBaseURL:           "https://ipfs.lubar.me/ipfs/",
}

type configKey struct{}

// WithConfig attaches a Config to a context.
func WithConfig(ctx context.Context, config *Config) context.Context {
	return context.WithValue(ctx, configKey{}, config)
}

// GetConfig returns the Config associated with a context, or DefaultConfig.
func GetConfig(ctx context.Context) *Config {
	cfg := ctx.Value(configKey{})
	if cfg == nil {
		return DefaultConfig
	}

	return cfg.(*Config)
}
