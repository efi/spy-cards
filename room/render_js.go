// +build js,wasm
// +build !headless

package room

import (
	"syscall/js"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

var (
	jsMatrixBuf = js.Global().Get("Uint8Array").New(4 * 4 * 4 * 2)

	jsRender js.Value

	_ = gfx.Custom("renderStage", func() {
		jsRender = js.Global().Get("Function").New("gl,prog,puni,cuni,pbuf,cbuf,pattr,tattr", `

return function renderStage(tex, dbuf, ebuf, count, noDepth) {
	gl.useProgram(prog);

	if (!noDepth) {
		gl.enable(gl.DEPTH_TEST);
	}

	gl.uniformMatrix4fv(puni, false, pbuf);
	gl.uniformMatrix4fv(cuni, false, cbuf);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, tex);

	gl.bindBuffer(gl.ARRAY_BUFFER, dbuf);

	gl.enableVertexAttribArray(pattr);
	gl.enableVertexAttribArray(tattr);
	gl.vertexAttribPointer(pattr, 3, gl.FLOAT, false, 20, 0);
	gl.vertexAttribPointer(tattr, 2, gl.FLOAT, false, 20, 12);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebuf);
	gl.drawElements(gl.TRIANGLES, count, gl.UNSIGNED_SHORT, 0);

	//gl.disableVertexAttribArray(pattr); // leave attrib 0 enabled
	gl.disableVertexAttribArray(tattr);

	if (!noDepth) {
		gl.disable(gl.DEPTH_TEST);
	}
}`).Invoke(
			gfx.GL.(js.Wrapper).JSValue(),
			roomProgram.Program.Value,
			roomPerspective.Uniform.Value,
			roomCamera.Uniform.Value,
			js.Global().Get("Float32Array").New(jsMatrixBuf.Get("buffer"), 0, 4*4),
			js.Global().Get("Float32Array").New(jsMatrixBuf.Get("buffer"), 4*4*4, 4*4),
			posAttrib.Attrib.Value,
			texAttrib.Attrib.Value,
		)
	}, func() {
		jsRender = js.Null()
	})
)

func (s *customStage) Render(cam *gfx.Camera) {
	gfx.Lock.Lock()

	var camBuf [2]gfx.Matrix

	camBuf[0] = cam.Perspective
	camBuf[1] = cam.Combined()

	/*#nosec*/
	js.CopyBytesToJS(jsMatrixBuf, (*[unsafe.Sizeof(camBuf)]byte)(unsafe.Pointer(&camBuf))[:])

	jsRender.Invoke(
		s.tex.LazyTexture(sprites.Blank.Texture()).Value,
		s.buf.Data.Value,
		s.buf.Element.Value,
		s.buf.Count,
		s.noDepth,
	)

	gfx.Lock.Unlock()
}
