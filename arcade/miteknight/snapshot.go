package miteknight

import "git.lubar.me/ben/spy-cards/gfx/sprites"

type snapshot struct {
	camPos            coord3
	camTarget         *coord3
	camRotationTarget float32
	camRotation       float32
	timeMin           float64
	timeSec           float64
	floor             int
	floors            []*floorSnapshot
	particles         []*particleEffect
	pause             uint64
	timeText          string
	stopDelay         float64
	inputDelay        float64
	compassPos        coord3
	compassRotation   float32
	aiReward          float64
	lastScoreTick     uint64
	scoreBefore       float64
	recentTiles       [][2]int
	clockWait         uint64
	inputWait         uint64
	floorWait         uint64
	fadeWait          uint64
	exitWait          uint64
	allowExitAfter    uint64
	overrideHUD       func()
	winner            bool
	canInput          bool
	rotateX           bool
	compassEnabled    bool
	oldBlock          bool
	allowCancelExit   bool
}

type floorSnapshot struct {
	enemies []*entitySnapshot
	player  *entitySnapshot
	door    *entitySnapshot
	items   []*entitySnapshot
	tiles   []tileType
	hasKey  bool
	gotKey  bool
}

type entitySnapshot struct {
	sprite         *sprites.Sprite
	tempBounce     uint64
	moving         uint64
	playerMoving   uint64
	moveInterrupt  uint64
	blinkStart     uint64
	blinkDelay     uint64
	cooldown       int
	iframes        int
	actionCooldown int
	hp, maxhp      int
	x, z           int
	child          int
	moveInterruptX int
	moveInterruptZ int
	height         float32
	dx, dz         float32
	rotationZ      float32
	bounce         float32
	entityType     entityType
	direction      direction
	action         action
	flipX          bool
	blocking       bool
	dead           bool
	dying          bool
	special        bool
	billboard      bool
	fog            bool
}

// SaveState implements game.Interface.
func (mk *MiteKnight) SaveState() interface{} {
	floors := make([]*floorSnapshot, len(mk.floors))
	for i, f := range mk.floors {
		floors[i] = f.saveState()
	}

	particles := make([]*particleEffect, len(mk.particles))
	copy(particles, mk.particles)

	recentTiles := make([][2]int, len(mk.recentTiles))
	copy(recentTiles, mk.recentTiles)

	return &snapshot{
		winner:            mk.winner,
		camPos:            mk.camPos,
		camTarget:         mk.camTarget,
		camRotationTarget: mk.camRotationTarget,
		camRotation:       mk.camRotation,
		timeMin:           mk.timeMin,
		timeSec:           mk.timeSec,
		floor:             mk.floor,
		floors:            floors,
		particles:         particles,
		canInput:          mk.canInput,
		pause:             mk.pause,
		rotateX:           mk.rotateX,
		timeText:          mk.timeText,
		stopDelay:         mk.stopDelay,
		inputDelay:        mk.inputDelay,
		compassEnabled:    mk.compassEnabled,
		compassPos:        mk.compassPos,
		compassRotation:   mk.compassRotation,
		oldBlock:          mk.oldBlock,
		aiReward:          mk.aiReward,
		lastScoreTick:     mk.lastScoreTick,
		scoreBefore:       mk.scoreBefore,
		recentTiles:       recentTiles,
		clockWait:         mk.clockWait,
		inputWait:         mk.inputWait,
		floorWait:         mk.floorWait,
		fadeWait:          mk.fadeWait,
		exitWait:          mk.exitWait,
		allowExitAfter:    mk.allowExitAfter,
		allowCancelExit:   mk.allowCancelExit,
		overrideHUD:       mk.overrideHUD,
	}
}

// LoadState implements game.Interface.
func (mk *MiteKnight) LoadState(save interface{}) {
	s := save.(*snapshot)

	mk.winner = s.winner
	mk.camPos = s.camPos
	mk.camTarget = s.camTarget
	mk.camRotationTarget = s.camRotationTarget
	mk.camRotation = s.camRotation
	mk.timeMin = s.timeMin
	mk.timeSec = s.timeSec
	mk.floor = s.floor

	for i, f := range mk.floors {
		f.loadState(s.floors[i])
	}

	mk.particles = make([]*particleEffect, len(s.particles))
	copy(mk.particles, s.particles)
	mk.canInput = s.canInput
	mk.pause = s.pause
	mk.rotateX = s.rotateX
	mk.timeText = s.timeText
	mk.stopDelay = s.stopDelay
	mk.inputDelay = s.inputDelay
	mk.compassEnabled = s.compassEnabled
	mk.compassPos = s.compassPos
	mk.compassRotation = s.compassRotation
	mk.oldBlock = s.oldBlock
	mk.aiReward = s.aiReward
	mk.lastScoreTick = s.lastScoreTick
	mk.scoreBefore = s.scoreBefore
	mk.recentTiles = make([][2]int, len(s.recentTiles))
	copy(mk.recentTiles, s.recentTiles)
	mk.clockWait = s.clockWait
	mk.inputWait = s.inputWait
	mk.floorWait = s.floorWait
	mk.fadeWait = s.fadeWait
	mk.exitWait = s.exitWait
	mk.allowExitAfter = s.allowExitAfter
	mk.allowCancelExit = s.allowCancelExit
	mk.overrideHUD = s.overrideHUD
}

func (d *dungeonFloor) saveState() *floorSnapshot {
	enemies := make([]*entitySnapshot, len(d.enemies))
	for i, e := range d.enemies {
		enemies[i] = e.saveState(d)
	}

	items := make([]*entitySnapshot, len(d.items))
	for i, e := range d.items {
		items[i] = e.saveState(d)
	}

	tiles := make([]tileType, d.sizeX*d.sizeY)
	copy(tiles, d.floorMap[0][:d.sizeX*d.sizeY])

	s := &floorSnapshot{
		enemies: enemies,
		player:  d.player.saveState(d),
		door:    d.door.saveState(d),
		items:   items,
		tiles:   tiles,
		hasKey:  d.hasKey,
		gotKey:  d.gotKey,
	}

	return s
}

func (d *dungeonFloor) loadState(s *floorSnapshot) {
	enemies := make([]entity, len(s.enemies))
	d.enemies = make([]*entity, len(s.enemies))

	for i, e := range s.enemies {
		d.enemies[i] = &enemies[i]
		d.enemies[i].loadState(e, d)
	}

	d.player = &entity{}
	d.player.loadState(s.player, d)
	d.door = &entity{}
	d.door.loadState(s.door, d)

	items := make([]entity, len(s.items))
	d.items = make([]*entity, len(s.items))

	for i, e := range s.items {
		d.items[i] = &items[i]
		d.items[i].loadState(e, d)
	}

	copy(d.floorMap[0][:d.sizeX*d.sizeY], s.tiles)
	d.hasKey = s.hasKey
	d.gotKey = s.gotKey
}

func (e *entity) saveState(floor *dungeonFloor) *entitySnapshot {
	child := -1

	if e.child != nil {
		for i, c := range floor.enemies {
			if c == e.child {
				child = i

				break
			}
		}
	}

	return &entitySnapshot{
		hp:             e.hp,
		maxhp:          e.maxhp,
		x:              e.x,
		height:         e.height,
		z:              e.z,
		dx:             e.dx,
		dz:             e.dz,
		flipX:          e.flipX,
		rotationZ:      e.rotationZ,
		entityType:     e.entityType,
		child:          child,
		cooldown:       e.cooldown,
		iframes:        e.iframes,
		actionCooldown: e.actionCooldown,
		blocking:       e.blocking,
		dead:           e.dead,
		dying:          e.dying,
		special:        e.special,
		direction:      e.direction,
		action:         e.action,
		sprite:         e.sprite,
		bounce:         e.bounce,
		tempBounce:     e.tempBounce,
		billboard:      e.billboard,
		fog:            e.fog,
		moving:         e.moving,
		playerMoving:   e.playerMoving,
		moveInterrupt:  e.moveInterrupt,
		moveInterruptX: e.moveInterruptX,
		moveInterruptZ: e.moveInterruptZ,
		blinkStart:     e.blinkStart,
		blinkDelay:     e.blinkDelay,
	}
}

func (e *entity) loadState(s *entitySnapshot, floor *dungeonFloor) {
	e.hp = s.hp
	e.maxhp = s.maxhp
	e.x = s.x
	e.height = s.height
	e.z = s.z
	e.dx = s.dx
	e.dz = s.dz
	e.flipX = s.flipX
	e.rotationZ = s.rotationZ
	e.entityType = s.entityType

	if s.child == -1 {
		e.child = nil
	} else {
		e.child = floor.enemies[s.child]
	}

	e.cooldown = s.cooldown
	e.iframes = s.iframes
	e.actionCooldown = s.actionCooldown
	e.blocking = s.blocking
	e.dead = s.dead
	e.dying = s.dying
	e.special = s.special
	e.direction = s.direction
	e.action = s.action
	e.sprite = s.sprite
	e.bounce = s.bounce
	e.tempBounce = s.tempBounce
	e.billboard = s.billboard
	e.fog = s.fog
	e.moving = s.moving
	e.playerMoving = s.playerMoving
	e.moveInterrupt = s.moveInterrupt
	e.moveInterruptX = s.moveInterruptX
	e.moveInterruptZ = s.moveInterruptZ
	e.blinkStart = s.blinkStart
	e.blinkDelay = s.blinkDelay
}
