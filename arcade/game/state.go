// Package game holds the shared code for Termacade games.
package game

import (
	"crypto/sha512"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/crt"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/rng"
	"github.com/BenLubar/convnet/deepqlearn"
)

const (
	// FrameRate is the number of frames per second at which the Termacade runs.
	FrameRate = 60
	// FrameTime is the reciprocal of FrameRate.
	FrameTime = time.Second / FrameRate

	maxOutstandingFrames = 3
)

// State holds the state of a Termacade game.
type State struct {
	TickCount uint64
	Score     float64
	Seed      string

	RNG       *rng.RNG
	Interface Interface
	Input     *input.Context
	Camera    gfx.Camera
	CRT       crt.CRT

	nextTick          time.Time
	outstandingFrames int

	Playback      *arcade.Recording
	PlaybackInput *input.Context
	goalTick      uint64
	savedTicks    []*SavedState

	recording     *arcade.Recording
	lastInputTick uint64
	lastInput     uint64

	fadeCurrent float64
	fadeAmount  float64
	fadeTarget  float64

	aiButtons   []arcade.Button
	aiOutput    []arcade.Button
	aiOutputs   [][]arcade.Button
	aiBrain     *deepqlearn.Brain
	aiVisWidth  int
	aiVisHeight int
	aiConfig    *AIConfig
	aiCurInput  []float64
	aiPrevInput []float64

	Exit         bool
	aiButtonSpam bool
}

func newState(seed string) *State {
	state := &State{
		Seed: seed,
		RNG:  rng.New(sha512.New(), seed),
	}

	state.FadeTo(1)
	state.Camera.SetDefaults()

	return state
}

// Lerp linearly interpolates between x and y.
func Lerp(x, y, blend float64) float64 {
	return x*(1-blend) + y*blend
}

// Lerp32 linearly interpolates between x and y.
func Lerp32(x, y, blend float32) float32 {
	return x*(1-blend) + y*blend
}

// FadeTo sets the fade target (usually 0 or 1).
func (s *State) FadeTo(target float64) {
	s.Fade(target, 0.1)
}

// Fade sets the fade target (usually 0 or 1) and the fade rate per frame.
func (s *State) Fade(target, amount float64) {
	s.fadeTarget = target
	s.fadeAmount = amount
}

// ComputeWait determines the number of ticks to wait.
func (s *State) ComputeWait(seconds float64) uint64 {
	frames := seconds * FrameRate

	if s.Playback != nil && s.Playback.Version[0] == 0 && s.Playback.Version[1] == 2 && s.Playback.Version[2] < 35 && frames == math.Floor(frames) {
		frames++
	}

	return uint64(math.Ceil(frames))
}

func (s *State) updateFade() {
	s.fadeCurrent = Lerp(s.fadeCurrent, s.fadeTarget, s.fadeAmount)
	s.CRT.FadeOut = s.fadeCurrent
}
