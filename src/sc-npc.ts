module SpyCards.AI {
	// Rules for NPCs:
	// - MUST NOT modify arguments or any value reachable from arguments.
	// - MUST NOT modify Spy Cards code or global data.
	// - Promises MUST take a reasonable, finite amount of time to resolve.
	// - MAY read any value reachable from arguments.
	// - SHOULD return a valid response (this will be caught in online games, but not in simulations).
	// - MAY store, access, and modify data local to the NPC.
	export abstract class NPC {
		// pickPlayer(): returns a player character to represent this NPC.
		abstract pickPlayer(): Promise<TheRoom.PlayerDef>;
		// createDeck(SpyCards.CardDefs): returns an array of cards to use as a deck.
		abstract createDeck(defs: CardDefs): Promise<Deck>;
		// playRound(SpyCards.Context): returns bitmask representing the indices of cards from
		// ctx.state.hand[ctx.player - 1] that should be played this round.
		abstract playRound(ctx: Context): Promise<number>;
		// afterRound(SpyCards.Context): optional, called after a round's winner has been determined.
		async afterRound(ctx: Context): Promise<void> { }
	}

	// Generic NPC:
	// - picks a random player
	// - creates a random 15 card deck (no ELK)
	// - plays cards, if possible, from left to right
	export class GenericNPC extends NPC {
		async pickPlayer(): Promise<SpyCards.TheRoom.PlayerDef> {
			const choices = SpyCards.TheRoom.Player.characters;
			return choices[Math.floor(Math.random() * choices.length)];
		}

		async createDeck(defs: CardDefs): Promise<Deck> {
			function pick(arr: CardDef[], not?: CardDef[]): CardDef {
				let card: CardDef;
				do {
					card = arr[Math.floor(Math.random() * arr.length)];
				} while (defs.banned[card.id] || defs.unpickable[card.id] || (not && not.indexOf(card) !== -1));
				return card;
			}

			const deck: Deck = [];
			for (let i = 0; i < defs.rules.cardsPerDeck; i++) {
				if (i < defs.rules.bossCards) {
					deck.push(pick(defs.cardsByBack[2], [defs.cardsByID[91]].concat(deck)));
				} else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
					deck.push(pick(defs.cardsByBack[1], deck.slice(defs.rules.bossCards)));
				} else {
					deck.push(pick(defs.cardsByBack[0]));
				}
			}

			return deck;
		}

		async playRound(ctx: Context): Promise<number> {
			let choices = 0;

			const myHand = ctx.state.hand[ctx.player - 1];
			let tp = ctx.state.tp[ctx.player - 1];

			for (let i = 0; i < myHand.length; i++) {
				const cardTP = myHand[i].card.effectiveTP();
				if (cardTP <= tp) {
					tp -= cardTP;
					choices = Binary.setBit(choices, i, 1);
				}
			}

			return choices;
		}
	}

	// Card Master:
	// - has a predefined deck and player
	export class CardMaster extends GenericNPC {
		private readonly name: string;
		private readonly deck: string;

		constructor(name: string, deck: string) {
			super();
			this.name = name;
			this.deck = deck;
		}

		async pickPlayer(): Promise<TheRoom.PlayerDef> {
			return this.name ?
				TheRoom.Player.characters.find((p) => p.name === this.name) :
				super.pickPlayer();
		}

		async createDeck(defs: CardDefs): Promise<Deck> {
			return Decks.decode(defs, this.deck);
		}
	}

	// Tourney Player:
	// - has a predefined player
	export class TourneyPlayer extends GenericNPC {
		private readonly name: string;

		constructor(name: string) {
			super();
			this.name = name;
		}

		async pickPlayer(): Promise<TheRoom.PlayerDef> {
			return TheRoom.Player.characters.find((p) => p.name === this.name);
		}
	}

	// Janet:
	// - janet
	export class Janet extends TourneyPlayer {
		constructor() {
			super("janet");
		}

		async createDeck(defs: CardDefs): Promise<Deck> {
			const deck = await super.createDeck(defs);

			for (let i = 0; i < defs.rules.cardsPerDeck && i < defs.rules.bossCards; i++) {
				if (!defs.banned[91] && defs.cardsByID[91].effectiveTP() !== Infinity && Math.random() >= 0.5) {
					deck[i] = defs.cardsByID[91];
					break;
				}
			}

			return deck;
		}
	}

	// Saved Decks:
	// - chooses a random valid local deck if one is available
	export class SavedDecks extends GenericNPC {
		async createDeck(defs: CardDefs): Promise<Deck> {
			const decks = Decks.loadSaved(defs, true);
			if (decks.length) {
				return decks[Math.floor(Math.random() * decks.length)];
			}

			return await super.createDeck(defs);
		}
	}

	export class MenderSpam extends NPC {
		private readonly mothiva: boolean;

		constructor(mothiva: boolean = true) {
			super();
			this.mothiva = mothiva;
		}

		async pickPlayer(): Promise<TheRoom.PlayerDef> {
			if (this.mothiva) {
				return TheRoom.Player.characters.find((c) => c.name === "pibu");
			}
			return TheRoom.Player.characters.find((c) => c.name === "nero");
		}

		async createDeck(defs: CardDefs): Promise<Deck> {
			const deck: Deck = [];
			if (defs.rules.bossCards !== 1 || defs.rules.miniBossCards !== 2) {
				throw new Error("cannot create mender deck with non-standard number of (mini-)boss cards");
			}
			deck.push(defs.cardsByID[CardData.GlobalCardID.HeavyDroneB33]);
			if (this.mothiva) {
				deck.push(defs.cardsByID[CardData.GlobalCardID.Mothiva]);
				deck.push(defs.cardsByID[CardData.GlobalCardID.Zasp]);
			} else {
				deck.push(defs.cardsByID[CardData.GlobalCardID.Kali]);
				deck.push(defs.cardsByID[CardData.GlobalCardID.Kabbu]);
			}
			while (deck.length < defs.rules.cardsPerDeck) {
				deck.push(defs.cardsByID[CardData.GlobalCardID.Mender]);
			}
			return deck;
		}

		async playRound(ctx: Context): Promise<number> {
			const myHand = ctx.state.hand[ctx.player - 1];
			const mothiva = myHand.findIndex((c) => c.card.id === CardData.GlobalCardID.Mothiva || c.card.id === CardData.GlobalCardID.Kali);
			const zasp = myHand.findIndex((c) => c.card.id === CardData.GlobalCardID.Zasp || c.card.id === CardData.GlobalCardID.Kabbu);
			const b33 = myHand.findIndex((c) => c.card.id === CardData.GlobalCardID.HeavyDroneB33);

			let tp = ctx.state.tp[ctx.player - 1];
			if (mothiva !== -1 && zasp !== -1 && tp >= myHand[mothiva].card.effectiveTP() + myHand[zasp].card.effectiveTP()) {
				return Binary.bit(mothiva) + Binary.bit(zasp);
			}

			if (myHand.length < 5) {
				return 0;
			}

			const mothivaOrZasp = mothiva === -1 ? zasp : mothiva;
			if (mothivaOrZasp !== -1 && tp >= myHand[mothivaOrZasp].card.effectiveTP()) {
				return Binary.bit(mothivaOrZasp);
			}

			if (ctx.state.hp[ctx.player - 1] > 2) {
				return 0;
			}

			if (b33 !== -1 && tp < myHand[b33].card.effectiveTP()) {
				return 0;
			}

			if (b33 === -1 && tp < 5) {
				return 0;
			}

			let toPlay = 0;
			if (b33 !== -1) {
				tp -= myHand[b33].card.effectiveTP();
				toPlay = Binary.setBit(toPlay, b33, 1);
			}

			for (let i = 0; i < myHand.length; i++) {
				if (Binary.getBit(toPlay, i)) {
					continue;
				}

				if (tp >= myHand[i].card.effectiveTP()) {
					tp -= myHand[i].card.effectiveTP();
					toPlay = Binary.setBit(toPlay, i, 1);
				}
			}

			return toPlay;
		}
	}

	export abstract class TourneyPlayer2 extends TourneyPlayer {
		constructor(name: string) {
			super(name);
		}

		abstract adjustWeight(card: CardDef, weight: number, deck: Deck, defs: CardDefs): number;
		countSynergies(defs: CardDefs, deck: Deck, source: CardDef, target: CardDef, perCard: number, perTribe: number): number {
			// for now, assume all synergies are good
			let count = 0;

			const cards: CardData.CardID[] = [target.id];
			const tribes: (Tribe | string)[] = [];
			for (let tribe of target.tribes) {
				if (tribe.tribe === Tribe.Custom) {
					tribes.push(tribe.custom.name);
				} else {
					tribes.push(tribe.tribe);
				}
			}
			for (let effect of target.effects) {
				if (effect.type === EffectType.CondCoin || effect.type === EffectType.CondLimit) {
					effect = effect.result;
				}
				if (effect.type === EffectType.Summon && !effect.opponent) {
					for (let i = 0; i < effect.amount; i++) {
						if (effect.generic) {
							if (effect.tribe === Tribe.Custom) {
								tribes.push(effect.customTribe);
							} else if (effect.tribe !== Tribe.None) {
								tribes.push(effect.tribe);
							}
						} else {
							cards.push(effect.card);
							for (let tribe of defs.cardsByID[effect.card].tribes) {
								tribes.push(tribe.tribe === Tribe.Custom ? tribe.custom.name : tribe.tribe);
							}
						}
					}
				}
			}

			for (let effect of source.effects) {
				let mul = 1;
				if (effect.type === EffectType.CondLimit && !effect.negate && source.id !== target.id) {
					effect = effect.result;
				}
				if ((effect.type === EffectType.Empower || effect.type === EffectType.CondCard) && !effect.opponent) {
					if (effect.type === EffectType.CondCard) {
						mul /= effect.amount;
					}
					if (effect.generic) {
						if (effect.tribe === Tribe.Custom) {
							for (let t of tribes) {
								if (t === effect.customTribe) {
									count += perTribe * mul;
								}
							}
						} else if (effect.tribe !== Tribe.None) {
							for (let t of tribes) {
								if (t === effect.tribe) {
									count += perTribe * mul;
								}
							}
						}
					} else {
						for (let c of cards) {
							if (c === effect.card) {
								count += perCard * mul;
							}
						}
					}
				}
			}

			return count;
		}
		async selectCard(deck: Deck, defs: CardDefs, choices: CardDef[]): Promise<CardDef> {
			const choiceWeights = choices.map((c) => {
				if (c.id === CardData.GlobalCardID.TheEverlastingKing || defs.unpickable[c.id] || c.effectiveTP() > 10) {
					return { card: c, weight: 0 };
				}
				return { card: c, weight: 1 };
			});

			for (let c of choiceWeights) {
				let statTotal = 0;

				const unprocessedEffects = c.card.effects.slice(0);
				while (unprocessedEffects.length) {
					const effect = unprocessedEffects.shift();

					// numb is very powerful
					if (effect.type === EffectType.Numb && effect.opponent) {
						statTotal += Math.min(effect.amount * 2, 5);
					}

					if (effect.type === EffectType.Stat && !effect.opponent && !effect.negate) {
						statTotal += effect.amount;
					}

					if (effect.type === EffectType.Empower && effect.generic && !effect.opponent && !effect.negate) {
						statTotal += effect.amount;
					}

					if (effect.type === EffectType.Summon && !effect.generic && !effect.opponent) {
						for (let i = 0; i < effect.amount; i++) {
							unprocessedEffects.push(...defs.cardsByID[effect.card].effects);
						}
					}

					if (effect.type === EffectType.CondCoin &&
						effect.result.type === EffectType.Stat && !effect.result.opponent && !effect.result.negate &&
						(!effect.generic || (effect.tailsResult.type === EffectType.Stat && !effect.tailsResult.opponent && !effect.tailsResult.negate))) {
						statTotal += effect.result.amount * effect.amount / 2;
						if (effect.generic) {
							statTotal += effect.tailsResult.amount * effect.amount / 2;
						}
					}
				}

				// avoid division by 0
				const tp = Math.max(0.1, c.card.effectiveTP());

				// (vanilla) cards that have a nonzero raw stat total that is lower than their TP are weak
				if (statTotal && isFinite(statTotal)) {
					c.weight *= statTotal / tp;
				}

				// don't add too many cards with high TP
				c.weight *= Math.min(1, 5 / tp);

				for (let card of deck) {
					// favor cards that have synergies with existing cards
					let synergies = 0;
					synergies += this.countSynergies(defs, deck, c.card, card, c.card.rank() === Rank.MiniBoss ? 3 : 1, card.rank() >= Rank.MiniBoss ? 2 : 1);
					synergies += this.countSynergies(defs, deck, card, c.card, c.card.rank() === Rank.MiniBoss ? 3 : 1, card.rank() >= Rank.MiniBoss ? 2 : 1);
					c.weight *= Math.pow(7.5, synergies);

					// try not to pick too many cards with the same TP cost
					if (card.effectiveTP() === c.card.effectiveTP()) {
						c.weight *= 0.25;
					}

					// favor duplicates over non-duplicates
					if (card.id === c.card.id) {
						// one duplicate    = 2.50
						// two duplicates   = 1.56
						// three duplicates = 0.57
						// four duplicates  = 0.15
						c.weight *= 2.5 / deck.reduce((n, o) => o.id === c.card.id ? n + 1 : n, 0);
					}
				}
			}

			for (let c of choiceWeights) {
				if (defs.banned[c.card.id]) {
					c.weight = 0;
				} else {
					c.weight = Math.max(this.adjustWeight(c.card, c.weight, deck, defs), 0);
				}
			}

			const totalWeight = choiceWeights.reduce((t, w) => t + w.weight, 0);

			let choice = Math.random() * totalWeight;
			for (let { card, weight } of choiceWeights) {
				choice -= weight;
				if (choice < 0) {
					return card;
				}
			}

			// rounding error, probably
			return choices[choices.length - 1];
		}
		async createDeck(defs: CardDefs): Promise<Deck> {
			const deck: Deck = [];
			function unique(choices: CardDef[], except: CardDef[]): CardDef[] {
				return choices.filter((c) => except.indexOf(c) === -1);
			}
			for (let i = 0; i < defs.rules.cardsPerDeck; i++) {
				if (i < defs.rules.bossCards) {
					deck.push(await this.selectCard(deck, defs, unique(defs.cardsByBack[2], deck)));
				} else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
					deck.push(await this.selectCard(deck, defs, unique(defs.cardsByBack[1], deck.slice(defs.rules.bossCards))));
				} else {
					deck.push(await this.selectCard(deck, defs, defs.cardsByBack[0]));
				}
			}
			const rankOrder = [Rank.Boss, Rank.MiniBoss, Rank.Attacker, Rank.Effect];
			deck.sort((a, b) => {
				if (a.rank() !== b.rank()) {
					return rankOrder.indexOf(a.rank()) - rankOrder.indexOf(b.rank());
				}
				return defs.allCards.indexOf(a) - defs.allCards.indexOf(b);
			});
			return deck;
		}
	}

	export class GenericNPC2 extends TourneyPlayer2 {
		constructor() {
			const choices = SpyCards.TheRoom.Player.characters;
			super(choices[Math.floor(Math.random() * choices.length)].name);
		}

		adjustWeight(card: CardDef, weight: number): number {
			return weight;
		}
	}

	export class BuGi2 extends TourneyPlayer2 {
		constructor() {
			super("bu-gi");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// bu-gi likes attacker cards

			if (card.rank() === Rank.Attacker) {
				return weight * 2.5;
			}

			return weight;
		}
	}

	export class Carmina2 extends TourneyPlayer2 {
		constructor() {
			super("carmina");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// carmina favors cards with random chances on them.

			for (let effect of card.effects) {
				if (effect.type === EffectType.CondCoin || (effect.type === EffectType.Summon && effect.generic)) {
					return weight * 10;
				}
			}
			return weight;
		}
	}

	export class Janet2 extends TourneyPlayer2 {
		constructor() {
			super("janet");
		}

		adjustWeight(card: CardDef, weight: number, deck: Deck, defs: CardDefs): number {
			// janet.

			if (card.id !== CardData.GlobalCardID.TheEverlastingKing) {
				return weight;
			}

			if (!defs.banned[card.id] && card.effectiveTP() <= 10) {
				return defs.cardsByBack[2].length;
			}

			return weight;
		}
	}

	export class Johnny2 extends TourneyPlayer2 {
		constructor() {
			super("johnny");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// johnny likes cards with non-random conditions, and (mini-)bosses with empower.

			for (let effect of card.effects) {
				if (effect.type === EffectType.Empower && card.rank() >= Rank.MiniBoss) {
					return weight * 5;
				}
				if (effect.type !== EffectType.CondCoin && effect.type !== EffectType.CondLimit && effect.type >= 128) {
					if (effect.type === EffectType.CondCard) {
						return weight * 5 / effect.amount;
					}
					return weight * 5;
				}
			}

			return weight;
		}
	}

	export class Kage2 extends TourneyPlayer2 {
		constructor() {
			super("kage");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// kage likes cards with multiple tribes and cards with empower or unity on them.

			weight *= card.tribes.length / 2 + 0.5;

			for (let effect of card.effects) {
				if (effect.type === EffectType.CondLimit && !effect.negate) {
					effect = effect.result;
				}
				if (effect.type === EffectType.Empower && !effect.negate && !effect.opponent) {
					return weight * 2;
				}
			}

			return weight;
		}
	}

	export class Ritchee2 extends TourneyPlayer2 {
		constructor() {
			super("ritchee");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// ritchee prefers high-cost cards.

			return weight * Math.pow(card.effectiveTP(), 3);
		}
	}

	export class Serene2 extends TourneyPlayer2 {
		constructor() {
			super("serene");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// serene likes cards with low TP costs.

			if (card.effectiveTP() > 3) {
				weight *= Math.pow(3 / card.effectiveTP(), 3);
			}

			return weight;
		}
	}

	export class Chuck2 extends TourneyPlayer2 {
		constructor() {
			super("chuck");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// chuck loves seedlings.

			if (card.tribes.some((t) => t.tribe === Tribe.Seedling)) {
				return weight * 25;
			}

			return weight;
		}
	}

	export class Arie2 extends TourneyPlayer2 {
		constructor() {
			super("arie");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// arie favors cards that cost near to 2 TP, and dislikes cards
			// that are fungi, or that are bug plus another tribe.

			if (card.effectiveTP() === 2) {
				weight *= 3;
			} else {
				weight *= Math.pow(1 / (1 + Math.abs(2 - card.effectiveTP())), 4);
			}

			for (let t of card.tribes) {
				if (t.tribe === Tribe.Fungi) {
					weight /= 10;
					break;
				}
				if (t.tribe === Tribe.Bug && card.tribes.length > 1) {
					weight /= 10;
					break;
				}
			}

			return weight;
		}
	}

	export class Shay2 extends TourneyPlayer2 {
		constructor() {
			super("shay");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// shay likes Thugs and cards with numb.

			if (card.tribes.some((t) => t.tribe === Tribe.Thug)) {
				return weight * (card.rank() >= Rank.MiniBoss ? 25 : 7.5);
			}

			for (let effect of card.effects) {
				if (effect.type === EffectType.CondCoin) {
					effect = effect.result;
				}
				if (effect.type === EffectType.Numb) {
					return weight * 5;
				}
			}

			return weight;
		}
	}

	export class Crow2 extends TourneyPlayer2 {
		constructor() {
			super("crow");
		}

		adjustWeight(card: CardDef, weight: number): number {
			// crow likes (non-boss) ??? and (all) Bot cards below 6 TP,
			// and cards with both ATK and a non-card-based condition.

			if (card.effectiveTP() < 6) {
				for (let t of card.tribes) {
					if ((t.tribe === Tribe.Unknown && card.rank() !== Rank.Boss) || t.tribe === Tribe.Bot) {
						return weight * 30;
					}
				}
			}

			if (card.rank() === Rank.Boss) {
				return weight / 100;
			}

			const haveCondition = card.effects.some((e) => e.type >= 128 && e.type !== EffectType.CondCard);
			const haveATK = card.effects.some((e) => e.type === EffectType.Stat && !e.defense && !e.opponent && !e.negate);
			if (haveCondition && haveATK) {
				return weight * 40;
			}

			return weight;
		}
	}

	export function getNPC(name: string | true): NPC {
		if (typeof name === "string" && name.startsWith("cm-")) {
			const parts = name.split("-", 3);
			return new CardMaster(parts[2], parts[1]);
		}

		switch (name) {
			case "janet":
				return new Janet();
			case "bu-gi":
			case "johnny":
			case "kage":
			case "ritchee":
			case "serene":
			case "carmina":
				return new TourneyPlayer(name);
			case "saved-decks":
				return new SavedDecks();
			case "tutorial":
				return new CardMaster("carmina", "01H00000000013HSMR");
			case "carmina2":
				return new CardMaster("carmina", "3P7T52H8MA5273HG842YF7KR");
			case "chuck":
				return new CardMaster("chuck", "4HH0000007VXYZFG84210GG8");
			case "arie":
				return new CardMaster("arie", "310J10G84212NANCPAD6K7VW");
			case "shay":
				return new CardMaster("shay", "511KHRWE631GRD6K9MMA5840");
			case "crow":
				return new CardMaster("crow", "101AXEPH8MCJ8G845G");
			case "mender-spam":
				return new MenderSpam(Math.random() >= 0.5);
			case "mender-spam-mothiva":
				return new MenderSpam(true);
			case "mender-spam-kali":
				return new MenderSpam(false);
			case "tp2-generic":
				return new GenericNPC2();
			case "tp2-janet":
				return new Janet2();
			case "tp2-bu-gi":
				return new BuGi2();
			case "tp2-johnny":
				return new Johnny2();
			case "tp2-kage":
				return new Kage2();
			case "tp2-ritchee":
				return new Ritchee2();
			case "tp2-serene":
				return new Serene2();
			case "tp2-carmina":
				return new Carmina2();
			case "tp2-chuck":
				return new Chuck2();
			case "tp2-arie":
				return new Arie2();
			case "tp2-shay":
				return new Shay2();
			case "tp2-crow":
				return new Crow2();
			default:
				return new GenericNPC();
		}
	}
}
