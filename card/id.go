package card

import "strconv"

// ID is a numeric identifier for a Spy Cards card.
//
// IDs below 128 correspond to the card's internal enemy ID in Bug Fables.
//
// IDs 128 and above are custom Attacker, Effect, Mini-Boss, and Boss cards,
// in repeating groups of 32 IDs.
type ID uint64

// Constants for ID.
const (
	Zombiant           ID = 0
	Jellyshroom        ID = 1
	Spider             ID = 2
	Zasp               ID = 3
	Cactiling          ID = 4
	Psicorp            ID = 5
	Thief              ID = 6
	Bandit             ID = 7
	Inichas            ID = 8
	Seedling           ID = 9
	Numbnail           ID = 14
	Mothiva            ID = 15
	Acornling          ID = 16
	Weevil             ID = 17
	VenusBud           ID = 19
	Chomper            ID = 20
	AcolyteAria        ID = 21
	Kabbu              ID = 23
	VenusGuardian      ID = 24
	WaspTrooper        ID = 25
	WaspBomber         ID = 26
	WaspDriller        ID = 27
	WaspScout          ID = 28
	Midge              ID = 29
	Underling          ID = 30
	MonsieurScarlet    ID = 31
	GoldenSeedling     ID = 32
	ArrowWorm          ID = 33
	Carmina            ID = 34
	SeedlingKing       ID = 35
	Broodmother        ID = 36
	Plumpling          ID = 37
	Flowerling         ID = 38
	Burglar            ID = 39
	Astotheles         ID = 40
	MotherChomper      ID = 41
	Ahoneynation       ID = 42
	BeeBoop            ID = 43
	SecurityTurret     ID = 44
	Denmuki            ID = 45
	HeavyDroneB33      ID = 46
	Mender             ID = 47
	Abomihoney         ID = 48
	DuneScorpion       ID = 49
	TidalWyrm          ID = 50
	Kali               ID = 51
	Zombee             ID = 52
	Zombeetle          ID = 53
	TheWatcher         ID = 54
	PeacockSpider      ID = 55
	Bloatshroom        ID = 56
	Krawler            ID = 57
	HauntedCloth       ID = 58
	Warden             ID = 61
	JumpingSpider      ID = 63
	MimicSpider        ID = 64
	LeafbugNinja       ID = 65
	LeafbugArcher      ID = 66
	LeafbugClubber     ID = 67
	Madesphy           ID = 68
	TheBeast           ID = 69
	ChomperBrute       ID = 70
	Mantidfly          ID = 71
	GeneralUltimax     ID = 72
	WildChomper        ID = 73
	Cross              ID = 74
	Poi                ID = 75
	PrimalWeevil       ID = 76
	FalseMonarch       ID = 77
	Mothfly            ID = 78
	MothflyCluster     ID = 79
	Ironnail           ID = 80
	Belostoss          ID = 81
	Ruffian            ID = 82
	WaterStrider       ID = 83
	DivingSpider       ID = 84
	Cenn               ID = 85
	Pisci              ID = 86
	DeadLanderα        ID = 87
	DeadLanderβ        ID = 88
	DeadLanderγ        ID = 89
	WaspKing           ID = 90
	TheEverlastingKing ID = 91
	Maki               ID = 92
	Kina               ID = 93
	Yin                ID = 94
	UltimaxTank        ID = 95
	Zommoth            ID = 96
	Riz                ID = 97
	Devourer           ID = 98
)

// String returns the default name for this card ID.
func (i ID) String() string {
	if i < 128 {
		if basicData[i] != nil {
			return basicData[i].name
		}

		return "MissingCard?" + strconv.FormatUint(uint64(i), 10) + "?"
	}

	n := uint64(((i>>2)&^31)|(i&31)) - 31

	return "Custom " + i.Rank().String() + " #" + strconv.FormatUint(n, 10)
}
