package visual

import (
	"context"
	"log"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/xerrors"
)

func Loop(ctx context.Context, looper interface {
	Update(context.Context) error
	Render(context.Context) error
}) error {
	nextUpdate := time.Now()

	for {
		outstandingFrames := 0

		now := time.Now()

		if nextUpdate.Before(now.Add(-5 * time.Second)) {
			log.Println("DEBUG: significant frame drops; skipping", now.Sub(nextUpdate).Truncate(time.Millisecond), "of frame updates to catch up")
			nextUpdate = now
		}

		for !nextUpdate.After(now) && outstandingFrames < 20 {
			if err := looper.Update(ctx); err != nil {
				return xerrors.Errorf("visual: loop logic error: %w", err)
			}

			nextUpdate = nextUpdate.Add(time.Second / 60)
			outstandingFrames++
		}

		if err := looper.Render(ctx); err != nil {
			return xerrors.Errorf("visual: render error: %w", err)
		}

		gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			return xerrors.Errorf("visual: loop cancelled: %w", err)
		}
	}
}
