---
title: Data Formats (Card v5)
...

(base64-encoded)

- uvarint FormatVersion = 5
- uvarint GlobalCardID
- string DisplayName
- svarint TPCost
- uint8 Portrait
    - Values 0..234 are built-in portraits.
    - Values 235..253 are reserved.
    - Value 254 is an embedded PNG.
    - Value 255 is a File ID.
- uint8 RankTribe
    - Most significant 4 bits are the rank of this card.
        - Value 0 is Attacker.
        - Value 1 is Effect.
        - Value 2 is Mini-Boss.
        - Value 3 is Boss.
        - Value 4 is Token.
        - Values 5..15 are reserved.
    - Least significant 4 bits are the card's first tribe.
        - Values 0..13 are a built-in tribe (see Appendix A).
        - Value 14 denotes a custom tribe.
        - Value 15 is reserved.
- [CustomTribe](#customtribe) CustomTribe0
    - This field is omitted if the first tribe is not custom.
- uvarint AdditionalTribeCount
- repeated ceil(AdditionalTribeCount/2) times:
    - uint8 PackedTribes
        - Both sets of 4 bits have the same meaning as the tribe half of RankTribe.
        - If AdditionalTribeCount is odd, the last entry's least significant 4 bits are set to 15.
    - [CustomTribe](#customtribe) CustomTribe1
        - This field is omitted if the first (most significant bits) tribe is not custom.
    - [CustomTribe](#customtribe) CustomTribe2
        - This field is omitted if the second (least significant bits) tribe is not custom.
- uvarint EffectCount
- repeated EffectCount times:
    - [EffectDef](#effectdef) Effect
- if Portrait is 254 or 255:
    - uvarint CustomPortraitLength
    - byte\[CustomPortraitLength\] CustomPortrait
- uvarint ExtensionFieldCount
- repeated ExtensionFieldCount times:
    - uvarint FieldType
    - uvarint FieldLength
    - [ExtensionField](#extensionfield) Field (FieldLength bytes)

# CustomTribe

- uint8\[3\] ColorRGB
- string Name

# CardFilter

*All Tag values not specified here are reserved.*

Specific card:

- uint8 Tag = 1
- uvarint CardID

Generic filter:

*All tribe filter fields must match to satisfy the card filter. Any rank filter field must match.*

- repeated arbitrary number of times:
    - uint8 Tag
        - 0001xxxx: built-in tribe (see Appendix A)
        - 00011110: custom tribe
        - 00011111: reserved
        - 0010xxxx: not built-in tribe (see Appendix A)
        - 00101110: not custom tribe
        - 00101111: reserved
        - 00110000: attacker
        - 00110001: effect
        - 00110010: mini-boss
        - 00110011: boss
        - 00110100: token
    - string CustomTribe
        - This field is omitted if Tag is not one of the two custom tribe tags.
- uint8 EndTag = 0

# EffectDef

*Root effects cannot be results, and must have priority 0. Passive effects must results in locations where it is specified that they are allowed, and must have priority 0.*

- uint8 EffectID
- uint8 Priority
    - Value 0 is immediate - no sound or visual effects are played, and the effect resolves as soon as possible.
    - Values 1..63 occur before stats are displayed on the screen during a round.
    - Values 64..223 occur during the main part of the round.
    - Values 224..254 occur after the winner of the round has been determined.
    - Value 255 is reserved.
- uvarint Flags (defined per effect)
- ... Data (defined per effect)

*All effect IDs not described here are reserved.*

## 0: Flavor Text

*Has no effect during a match.*

Root.

Flags:

- 0: hide remaining effects on this card's description

Data:

- string Text

## 1: Card Stat

*Modify this card's stats by the given amount. Cards start with 0 ATK and 0 DEF.*

Flags:

- 0: modify DEF. if this flag is not set, modify ATK.
- 1: multiply Amount by infinity. Amount must be 1 or -1.

Data:

- svarint Amount

## 2: Empower

*Modify target cards' stats by the given amount.*

Flags:

- 0: modify DEF. if this flag is not set, modify ATK.
- 1: multiply Amount by infinity. Amount must be 1 or -1.
- 2: affect opponent. if this flag is not set, affect the current player.

Data:

- svarint Amount
- [CardFilter](#cardfilter) Filter

## 3: Summon

*Add a card to the field. Any effects that should have already been resolved in priority order are resolved at this time.*

Flags:

- 0: summon card for opponent. if this flag is not set, summon card for current player.
- 1: hide summoning card. this flag being set causes the replacement summon animation to play.
- 2: hide summoned card. this flag being set causes the summoned card to be invisible.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter

## 4: Heal

*Modify target player's health by the given amount. Health may go below 0, but cannot go above the maximum health for the mode.*

Flags:

- 0: multiply Amount by infinity. Amount must be 1 or -1.
- 1: heal the opponent. if this flag is not set, heal the current player.
- 2: ignore healing multipliers.

Data:

- svarint Amount

## 5: TP

*Modify available TP for this turn.*

Passive.

Flags:

- 0: multiply Amount by infinity. Amount must be 1 or -1.

Data:

- svarint Amount

## 6: Numb

*Flip a target card face-down and remove its stat contribution. This also prevents all future effects from running on the target card except for Numb and On Numb.*

*Cards are targeted as follows: The higher of the card's ATK or DEF stat is selected. The card with the overall lowest stat is numbed. ATK is considered to be higher than DEF if they are numerically equal.*

Flags:

- 0: multiply Count by infinity. Count must be 1.
- 1: target current player's cards. if this flag is not set, target opponent's cards.
- 2: select the card with the highest stats when deciding which to numb.
- 3: ignore ATK when selecting a card to numb.
- 4: ignore DEF when selecting a card to numb.
- 5: target cards with 0 for both stats last.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter

## 7: Raw Stat

*Modify the player's stats by the given amount. Numbing cards will not reverse these changes.*

Flags:

- 0: modify DEF. if this flag is not set, modify ATK.
- 1: multiply Amount by infinity. Amount must be 1 or -1.
- 2: affect opponent. if this flag is not set, affect the current player.

Data:

- svarint Amount

## 8: Multiply Healing

Flags:

- 0: affect healing for the opponent. if this flag is not set, affect healing for the current player.
- 1: only affect future healing. if this flag is not set, heals are multiplied retroactively.
- 2..3:
    - 0: modify all heal effects.
    - 1: only modify heals that start as negative.
    - 2: only modify heals that start as positive.
    - 3: (reserved)

Data:

- svarint Multiplier

## 9: Prevent Numb

*Targeted cards are given immunity to the next numb that could target them. If a card has this immunity, it is skipped when selecting a card to numb, and the immunity is removed.*

Flags:

- 0: prevent numbs for the opponent's cards. if this flag is not set, prevent numbs for the current player's cards.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter

## 10: Modify Available Cards

Flags:

- 0: modify opponent's cards. if this flag is not set, modify current player's cards.
- 1..2:
    - 0: add card.
    - 1: remove card.
    - 2: move card to field.
    - 3: (reserved)
- 3..4:
    - 0: target any location. if adding a card, add to the hand but do not shuffle back into the deck.
    - 1: target hand only.
    - 2: target deck only.
    - 3: target field only. invalid with move card to field.

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter

## 11: Modify Card Cost

Passive.

Flags:

- *(no flags are currently defined)*

Data:

- svarint Amount
- [CardFilter](#cardfilter) Filter

## 12: Draw Card

Flags:

- 0: opponent draws cards. if this flag is not set, the current player draws cards.
- 1: ignore maximum hand size

Data:

- svarint Count
    - If Count is negative, move cards from the hand to the deck instead.

## 128: Card (Condition)

Flags:

- 0: check the opponent's cards. if this flag is not set, check the current player's cards.
- 1..2:
    - 0: apply the effect if at least Count cards match.
    - 1: apply the effect if fewer than Count cards match.
    - 2: apply the effect multiple times if a multiple of Count cards match.
    - 3: (reserved)

Data:

- uvarint Count
- [CardFilter](#cardfilter) Filter
- [EffectDef](#effectdef) Result

## 129: Limit (Condition)

Flags:

- 0: run the effect if this condition has been checked more than Count times. if this flag is not set, run the effect if this condition has been checked less than or equal to Count times.

Data:

- uvarint Count
- [EffectDef](#effectdef) Result

## 130: Winner (Condition)

Priority must be at least 224.

Flags:

- 0..1:
    - 0: run the effect if the current player wins the round.
    - 1: run the effect if the opponent wins the round.
    - 2: run the effect if the round is a tie.
    - 3: run the effect if any player wins the round.

Data:

- [EffectDef](#effectdef) Result

## 131: Apply (Condition)

Flags:

- 0: run the effect in the following round. if this flag is not set, run the effect immediately.
- 1: run the effect for the opponent. if this flag is not set, run the effect for the current player. effects that act on the current card may ignore this flag.
- 2: display the full text of the current card on the ghost card. if this flag is not set, display only the effect text.

Data:

- [EffectDef](#effectdef) Result (passives allowed)

## 132: Coin (Condition)

Flags:

- 0: this coin has an effect on tails.
- 1: visually swap the sides of the coin.
- 2: display ATK/DEF rather than happy/sad sides on the coin.

Data:

- uvarint Count
- [EffectDef](#effectdef) HeadsResult
- [EffectDef](#effectdef) TailsResult
    - If flag 0 is not set, this field is omitted.

## 133: Stat (Condition)

Flags:

- 0: check the stat for the opponent. if this flag is not set, check the stat for the current player.
- 1: run the effect if the stat is less than the Amount. if this flag is not set, run the effect if the stat is greater than or equal to the Amount.
- 2..3:
    - 0: check ATK.
    - 1: check DEF.
    - 2: check HP.
    - 3: check TP.
- 4: run this effect multiple times if the stat satisfies a multiple of Amount.

Data:

- svarint Amount
- [EffectDef](#effectdef) Result

## 134: In Hand (Condition)

Root.

Flags:

- 0: do not reveal the card to the opponent. the card is still revealed to the opponent's computer.
- 1: the effect is also applied when the card is played.

Data:

- uvarint MinRounds
- uvarint MaxRounds
    - If MaxRounds is 0, it is treated as infinity.
- [EffectDef](#effectdef) Result (passives allowed)

## 135: Last Effect (Condition)

*All remaining effects on this card are removed after the result effect is processed. Conditions cannot be the result of this effect.*

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result
    - The priority of this EffectDef must be 0 or 1.

## 136: On Numb (Condition)

Root.

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result
    - The priority of this EffectDef must be 0 or 1.

## 137: Multiple Effects (Condition)

Flags:

- *(no flags are currently defined)*

Data:

- [EffectDef](#effectdef) Result
- [EffectDef](#effectdef) Result2

# ExtensionField

*All type IDs not described here are reserved.*

## 0: SetStage

- uvarint Flags
    - bit 0: override stage even if a custom stage is already active
- uvarint CIDLength
- byte\[CIDLength\] FileCID
    - See IPFS for a description of CIDs.
    - File format: [Spy Cards Online Stage](data-formats-stage.html)

## 1: SetMusic

- uvarint Flags
    - bit 0: override music even if custom music is already playing
- uvarint CIDLength
- byte\[CIDLength\] FileCID
    - See IPFS for a description of CIDs.
    - File format: ogg/opus
- float32 LoopStart
- float32 LoopEnd
