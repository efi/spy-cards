// +build !headless

package visual

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"sync"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/gui"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

var (
	uploadPortrait, _ = format.Decode32("HQCCM847M6RM3XR1")

	communityPortraitsOnce sync.Once
	communityPortraitCache []string
)

func communityPortraits(ctx context.Context) []string {
	communityPortraitsOnce.Do(func() {
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, internal.GetConfig(ctx).UserImageBaseURL+"community.json", nil)
		if err != nil {
			log.Println("WARNING: could not load community portrait list:", err)

			return
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			log.Println("WARNING: could not load community portrait list:", err)

			return
		}
		defer resp.Body.Close()

		err = json.NewDecoder(resp.Body).Decode(&communityPortraitCache)
		if err != nil {
			log.Println("WARNING: could not load community portrait list:", err)

			return
		}
	})

	return communityPortraitCache
}

type CardEditor struct {
	state        EditorState
	cam          gfx.Camera
	Set          card.Set
	card         *card.Def
	layout       *Layout
	backBtn      *Button
	editName     *Button
	editPortrait *Button
	editRank     *Button
	editTP       *Button
	viewport     *gui.Viewport
	sb           *sprites.Batch
	tb           *sprites.TextBatch
}

func (e *CardEditor) SetCards(cards string) error {
	var sets card.Sets

	if err := sets.UnmarshalText([]byte(cards)); err != nil {
		return xerrors.Errorf("visual: preparing cards for editor: %w", err)
	}

	s, err := sets.Apply()
	if err != nil {
		return xerrors.Errorf("visual: preparing cards for editor: %w", err)
	}

	e.Set = *s

	e.dirty()

	return nil
}

func (e *CardEditor) EncodeCards() string {
	b, err := e.Set.MarshalText()
	if err != nil {
		panic(err)
	}

	return string(b)
}

func (e *CardEditor) initCardSelector(ctx context.Context) {
	e.sb = sprites.NewBatch(&e.cam)
	e.tb = sprites.NewTextBatch(&e.cam)
	e.viewport = gui.NewViewport(e.layout.ictx, e.tb, &e.cam)
	e.viewport.Control.AddChild(&gui.NewSelector(gui.Options("A", "B", "C")).Control)

	e.layout.Cards = make([]*Card, 0, 1+len(e.Set.Cards)+4)

	/*
		gm := &match.ActiveCard{
			Card: &match.Card{
				Set:  &e.Set,
				Def:  card.Vanilla(^card.ID(0)),
				Back: card.Token,
			},
			Desc: &card.RichDescription{
				Text:  "Edit game mode data.",
				Color: sprites.Black,
			},
			Mode: match.ModeSetup,
		}

		gm.Card.Def.Name = "Game Mode"
		gm.Card.Def.Rank = card.Token
		gm.Card.Def.Tribes = nil

		e.layout.Cards = append(e.layout.Cards, &Card{
			Card:   gm.Card,
			Active: gm,
		})
	*/
	vanillaBanned := false

	for _, f := range e.Set.Mode.GetAll(card.FieldBannedCards) {
		bc := f.(*card.BannedCards)

		if bc.Flags&card.BannedCardRandomSummonable != 0 {
			continue
		}

		if len(bc.Cards) == 0 {
			vanillaBanned = true

			break
		}
	}

	if !vanillaBanned {
		for _, back := range []card.Rank{card.Boss, card.MiniBoss, card.Enemy} {
			for _, id := range card.VanillaOrder(back) {
				overridden := false

				for _, c := range e.Set.Cards {
					if c.ID == id {
						overridden = true

						break
					}
				}

				if overridden {
					continue
				}

				c := card.Vanilla(id)
				ac := &match.ActiveCard{
					Card: &match.Card{
						Set:  &e.Set,
						Def:  c,
						Back: back,
					},
					Desc: c.Description(&e.Set),
					Mode: match.ModeSetupOriginal,
				}

				e.layout.Cards = append(e.layout.Cards, &Card{
					Card:   ac.Card,
					Active: ac,
				})
			}
		}
	}

	for _, c := range e.Set.Cards {
		e.layout.Cards = append(e.layout.Cards, &Card{
			Card: &match.Card{
				Set:  &e.Set,
				Def:  c,
				Back: c.Rank.Back(),
			},
		})
	}
	/*

	   for rank := card.Attacker; rank <= card.Boss; rank++ {
	   		for i := 0; ; i++ {
	   			id := card.ID(128) + (card.ID(i)>>5)<<7 + card.ID(rank)<<5 + card.ID(i)&31
	   			found := false

	   			for _, c := range e.Set.Cards {
	   				if c.ID == id {
	   					found = true

	   					break
	   				}
	   			}

	   			if !found {
	   				c := card.Vanilla(id)
	   				c.Effects = nil

	   				ac := &match.ActiveCard{
	   					Card: &match.Card{
	   						Set:  &e.Set,
	   						Def:  c,
	   						Back: rank.Back(),
	   					},
	   					Desc: &card.RichDescription{
	   						Text:  "Create a new custom " + rank.String() + " card.",
	   						Color: sprites.Black,
	   					},
	   					Mode: match.ModeSetup,
	   				}

	   				e.layout.Cards = append(e.layout.Cards, &Card{
	   					Card:   ac.Card,
	   					Active: ac,
	   				})

	   				break
	   			}
	   		}
	   	}
	*/
	e.state = EditorSelect
	e.layout.OnSelect = func(c *Card) {
		e.onSelectEdit(ctx, c)
	}
	e.layout.PinCorner = 0
	e.layout.GrowDx = 0
	e.layout.GrowDy = 0
	e.layout.Reverse = false
	e.layout.Update(nil)
}

func (e *CardEditor) initCardEditor(ctx context.Context, c *card.Def) {
	e.layout.Cards = []*Card{
		{
			Card: &match.Card{
				Set:  &e.Set,
				Def:  c,
				Back: c.Rank.Back(),
			},
		},
	}
	e.layout.OnSelect = nil
	e.layout.PinCorner = 2
	e.layout.GrowDx = -0.25
	e.layout.GrowDy = -0.25
	e.layout.Reverse = true
	e.layout.Update(nil)

	e.card = c

	e.editName = &Button{
		Font:       sprites.FontBubblegumSans,
		Label:      c.DisplayName(),
		Sx:         1,
		Sy:         1,
		InputFocus: e.layout.InputFocus,
		OnSelect: func() {
			log.Panicln("TODO: edit card name") // TODO
		},
		InputFocusUp: func() {
			*e.layout.InputFocus = e.backBtn
		},
		InputFocusDown: func() {
			*e.layout.InputFocus = e.editPortrait
		},
	}

	e.editPortrait = &Button{
		Font:       sprites.FontBubblegumSans,
		Label:      "Edit Portrait",
		Sx:         1,
		Sy:         1,
		InputFocus: e.layout.InputFocus,
		OnSelect: func() {
			e.initPortraitSelect(ctx)
		},
		InputFocusUp: func() {
			*e.layout.InputFocus = e.editName
		},
		InputFocusDown: func() {
			*e.layout.InputFocus = e.editRank
		},
	}

	e.editRank = &Button{
		Font:       sprites.FontBubblegumSans,
		Label:      c.Rank.String(),
		Sx:         1,
		Sy:         1,
		InputFocus: e.layout.InputFocus,
		OnSelect: func() {
			e.initRankSelect(ctx)
		},
		InputFocusUp: func() {
			*e.layout.InputFocus = e.editPortrait
		},
		InputFocusDown: func() {
			if c.Rank == card.Token {
			} else {
				*e.layout.InputFocus = e.editTP
			}
		},
	}

	e.editTP = &Button{
		Font:       sprites.FontBubblegumSans,
		Label:      strconv.FormatInt(e.card.TP, 10),
		Sx:         1,
		Sy:         1,
		InputFocus: e.layout.InputFocus,
		InputFocusUp: func() {
			*e.layout.InputFocus = e.editRank
		},
		InputFocusDown: func() {
		},
		InputFocusLeft: func() {
			e.card.TP--
			e.layout.Cards[0].cachedDef = nil
			e.dirty()
			e.editTP.Label = strconv.FormatInt(e.card.TP, 10)
		},
		InputFocusRight: func() {
			e.card.TP++
			e.layout.Cards[0].cachedDef = nil
			e.dirty()
			e.editTP.Label = strconv.FormatInt(e.card.TP, 10)
		},
	}

	*e.layout.InputFocus = e.backBtn

	e.state = EditorCard
}

func (e *CardEditor) initRankSelect(ctx context.Context) {
	e.layout.Cards = make([]*Card, card.Token+1)

	for r := card.Attacker; r <= card.Token; r++ {
		c := *e.card

		c.Rank = r

		e.layout.Cards[r] = &Card{
			Card: &match.Card{
				Set:  &e.Set,
				Def:  &c,
				Back: r.Back(),
			},
		}
	}

	e.state = EditorCardChoices
	e.layout.OnSelect = func(c *Card) {
		e.onSelectRank(ctx, c)
	}
	e.layout.PinCorner = 0
	e.layout.GrowDx = 0
	e.layout.GrowDy = 0
	e.layout.Reverse = false
	e.layout.Update(nil)
}

func (e *CardEditor) initPortraitSelect(ctx context.Context) {
	community := communityPortraits(ctx)
	e.layout.Cards = make([]*Card, 235+len(community)+1)

	for i := 0; i < 235; i++ {
		c := *e.card

		c.Portrait = uint8(i)
		c.CustomPortrait = nil

		e.layout.Cards[i] = &Card{
			Card: &match.Card{
				Set:  &e.Set,
				Def:  &c,
				Back: c.Rank.Back(),
			},
		}
	}

	for i, id := range community {
		c := *e.card

		c.Portrait = card.PortraitCustomExternal
		c.CustomPortrait, _ = format.Decode32(id)

		e.layout.Cards[235+i] = &Card{
			Card: &match.Card{
				Set:  &e.Set,
				Def:  &c,
				Back: c.Rank.Back(),
			},
		}
	}

	c := *e.card

	c.Portrait = card.PortraitCustomExternal
	c.CustomPortrait = uploadPortrait

	e.layout.Cards[235+len(community)] = &Card{
		Card: &match.Card{
			Set:  &e.Set,
			Def:  &c,
			Back: c.Rank.Back(),
		},
	}

	e.state = EditorCardChoices
	e.layout.OnSelect = func(c *Card) {
		e.onSelectPortrait(ctx, c)
	}
	e.layout.PinCorner = 0
	e.layout.GrowDx = 0
	e.layout.GrowDy = 0
	e.layout.Reverse = false
	e.layout.Update(nil)
}

func (e *CardEditor) Update(ctx context.Context) error {
	switch e.state {
	case EditorInit:
		go communityPortraits(ctx) // preload list

		e.cam.SetDefaults()
		e.cam.Offset = gfx.Identity()
		e.cam.Rotation = gfx.Identity()
		e.cam.Position = gfx.Identity()

		e.layout = NewLayout(ctx)
		e.layout.InputFocus = new(interface{})

		e.initCardSelector(ctx)
		e.layout.Focus()

		e.backBtn = &Button{
			Label:      "Back",
			Font:       sprites.FontBubblegumSans,
			Sx:         0.65,
			Sy:         0.65,
			InputFocus: e.layout.InputFocus,
			OnSelect: func() {
				audio.Confirm1.PlaySound(0, 0, 0)

				e.initCardSelector(ctx)
				e.layout.Focus()
			},
		}
		e.viewport.Layout(nil)
	case EditorSelect:
		e.viewport.Render()
		e.layout.Update(nil)
	case EditorGameMode:
		e.layout.ictx.Tick()
		e.updateBack()

		// TODO: edit game mode
	case EditorCard:
		e.layout.Update(nil)
		e.updateBack()

		w, h := gfx.Size()
		x := -float32(w) / 256
		y := float32(h)/128 - 2

		e.editName.X = x
		e.editName.Y = y
		e.editName.Update(&e.cam, e.layout.ictx)

		y--

		e.editPortrait.X = x
		e.editPortrait.Y = y
		e.editPortrait.Update(&e.cam, e.layout.ictx)

		y--

		e.editRank.X = x
		e.editRank.Y = y
		e.editRank.Update(&e.cam, e.layout.ictx)

		if e.card.Rank != card.Token {
			y--

			e.editTP.X = x
			e.editTP.Y = y
			e.editTP.Update(&e.cam, e.layout.ictx)
		}

		_ = e.card.Tribes     // TODO
		_ = e.card.Effects    // TODO
		_ = e.card.Extensions // TODO (delete only)
	case EditorCardChoices:
		e.layout.Update(nil)
		e.updateBack()
	}

	return nil
}

func (e *CardEditor) updateBack() {
	w, h := gfx.Size()

	e.backBtn.X = -float32(w)/128 + 0.25
	e.backBtn.Y = float32(h)/128 - 0.5
	e.backBtn.Update(&e.cam, e.layout.ictx)
}

func (e *CardEditor) Render(ctx context.Context) error {
	w, h := gfx.Size()

	e.cam.Perspective = gfx.Scale(-128/float32(w), -128/float32(h), -0.01)

	if e.sb == nil {
		e.sb = sprites.NewBatch(&e.cam)
		e.tb = sprites.NewTextBatch(&e.cam)
	} else {
		e.sb.Reset(&e.cam)
		e.tb.Reset(&e.cam)
	}

	gfx.GL.ClearColor(0, 0.1333, 0.2, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	switch e.state {
	case EditorSelect:
		for pass := 0; pass < 3; pass++ {
			e.layout.Render(ctx, pass)
		}
	case EditorGameMode:
		e.backBtn.Render(e.tb)
	case EditorCard:
		for pass := 0; pass < 3; pass++ {
			e.layout.Render(ctx, pass)
		}

		e.backBtn.Render(e.tb)

		sprites.DrawText(e.tb, sprites.FontBubblegumSans, "Name: ", e.editName.X-2, e.editName.Y, 0, 1, 1, sprites.Gray)
		e.editName.Render(e.tb)

		e.editPortrait.Render(e.tb)

		sprites.DrawText(e.tb, sprites.FontBubblegumSans, "Rank: ", e.editRank.X-2, e.editRank.Y, 0, 1, 1, sprites.Gray)
		e.editRank.Render(e.tb)

		if e.card.Rank != card.Token {
			sprites.DrawText(e.tb, sprites.FontBubblegumSans, "TP: ", e.editTP.X-2, e.editTP.Y, 0, 1, 1, sprites.Gray)
			e.editTP.Render(e.tb)
		}
	case EditorCardChoices:
		e.backBtn.Render(e.tb)

		for pass := 0; pass < 3; pass++ {
			e.layout.Render(ctx, pass)
		}
	}

	e.viewport.Render()

	e.sb.Render()
	e.tb.Render()

	return nil
}

func (e *CardEditor) onSelectEdit(ctx context.Context, c *Card) {
	switch {
	case c.Active == nil:
		audio.Confirm1.PlaySound(0, 0, 0)

		e.initCardEditor(ctx, c.Card.Def)
	case c == e.layout.Cards[0]:
		audio.Confirm1.PlaySound(0, 0, 0)

		e.state = EditorGameMode
	default:
		audio.Confirm1.PlaySound(0, 0, 0)

		def := card.Vanilla(c.Card.Def.ID)
		e.Set.Cards = append(e.Set.Cards, def)

		e.dirty()

		e.initCardEditor(ctx, def)
	}
}

func (e *CardEditor) onSelectRank(ctx context.Context, c *Card) {
	e.card.Rank = c.Card.Def.Rank
	e.dirty()
	e.initCardEditor(ctx, e.card)
}

func (e *CardEditor) onSelectPortrait(ctx context.Context, c *Card) {
	if c.Card.Def.Portrait == card.PortraitCustomExternal {
		if bytes.Equal(uploadPortrait, c.Card.Def.CustomPortrait) {
			log.Panic("TODO: upload custom portrait") // TODO
		}

		e.card.Portrait = card.PortraitCustomExternal
		e.card.CustomPortrait = c.Card.Def.CustomPortrait
	} else {
		e.card.Portrait = c.Card.Def.Portrait
		e.card.CustomPortrait = nil
	}

	e.dirty()
	e.initCardEditor(ctx, e.card)
}
