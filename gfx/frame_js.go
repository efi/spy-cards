// +build js,wasm
// +build !headless

package gfx

import (
	"syscall/js"

	"golang.org/x/mobile/gl"
)

var (
	requestAnimationFrame = func() js.Value {
		if rpaf := js.Global().Get("requestPostAnimationFrame"); rpaf.Truthy() {
			return rpaf
		}

		return js.Global().Get("requestAnimationFrame")
	}()

	frameCh = make(chan struct{}, 64)
	onFrame = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		frameCh <- struct{}{}

		return js.Undefined()
	})

	// OnFrameEnd is an internal variable.
	OnFrameEnd = func() {}
)

// NextFrame synchronizes a frame boundary.
func NextFrame() {
	OnFrameEnd()

	requestAnimationFrame.Invoke(onFrame)

	<-frameCh

	GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
}
