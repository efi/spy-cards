package gui

type Node interface {
	Handler
	Packable
	chroot(*Viewport)
	AddChild(*Control)
	GetChildren() []Node
	Update()
	Focus()
	Scrollable() bool
	IsVisible() bool
}

type Packable interface {
	Pack(Rect) Rect
}

type Handler interface {
	Propagate(Event)
	EventHandler(Event) bool
}

type Control struct {
	node     Node
	Children []Node
	Parent   Node
	root     *Viewport
	update   func()
}

func (c *Control) Scrollable() bool { return false }

func (c *Control) IsVisible() bool { return false }

func (c *Control) chroot(r *Viewport) {
	c.root = r
	for _, ch := range c.GetChildren() {
		ch.chroot(r)
	}
}

func (c *Control) AddChild(i *Control) {
	i.Parent = c.node
	if c.root != nil {
		i.chroot(c.root)
	}
	c.Children = append(c.Children, i.node)
}

func (c *Control) GetChildren() []Node {
	return c.Children
}

func (c *Control) EventHandler(e Event) bool {
	return false
}

func (c *Control) Propagate(e Event) {
	if c.EventHandler(e) == false {
		Handler(c.Parent).Propagate(e)
	}
}

func (c *Control) Pack(r Rect) Rect {
	return Rect{}
}

func (c *Control) Update() {
	for _, ch := range c.GetChildren() {
		ch.Update()
	}
	c.update()
}

func (c *Control) Focus() {
	c.root.focus.control = c
	c.root.focus.ok = true
}

func (c *Control) Render() {}
