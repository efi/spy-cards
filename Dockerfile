FROM golang

RUN apt-get update \
 && apt-get install -y libegl1-mesa-dev libgles2-mesa-dev libx11-dev

COPY go.* /go/src/spy-cards/

RUN cd /go/src/spy-cards \
 && go mod download

COPY . /go/src/spy-cards/

RUN cd /go/src/spy-cards \
 && go build -tags headless -o /usr/local/bin/spy-cards

EXPOSE 8335

CMD ["spy-cards"]
