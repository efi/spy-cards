module SpyCards.UI {
	export const html: HTMLHtmlElement = document.querySelector("html");

	let nextUniqueID = 0;
	export function uniqueID() {
		return "_tmp_" + (nextUniqueID++);
	}
	export function replace(from: Element, to: Element) {
		if (from && from.parentNode && to) {
			from.parentNode.insertBefore(to, from);
			from.parentNode.removeChild(from);
		} else {
			remove(from);
			remove(to);
		}
	}
	export function remove(el: Element) {
		if (el && el.parentNode) {
			el.parentNode.removeChild(el);
		}
	}
	export function clear(el: Element) {
		if (!el) {
			return;
		}
		el.textContent = "";
	}

	export function button(label: string, classes: string[], click: () => void): HTMLButtonElement {
		const btn = document.createElement("button");
		btn.classList.add(...classes);
		btn.textContent = label;
		btn.addEventListener("click", (e) => {
			e.preventDefault();
			click();
		});
		return btn;
	}
	export function option(label: string, value: string): HTMLOptionElement {
		const opt = document.createElement("option");
		opt.textContent = label;
		opt.value = value;
		return opt;
	}
}
