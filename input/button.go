package input

import "git.lubar.me/ben/spy-cards/arcade"

// Held returns true if a button is currently held.
func (c *Context) Held(btn arcade.Button) bool {
	return c.state.held[btn]
}

// Consume returns true if a button is pressed,
// and consumes the button press indefinitely.
func (c *Context) Consume(btn arcade.Button) bool {
	if c.state.pressed[btn] {
		c.state.pressed[btn] = false

		return true
	}

	return false
}

// ConsumeAllowRepeat returns true if a button is pressed,
// and consumes the button press for delayTicks.
func (c *Context) ConsumeAllowRepeat(btn arcade.Button, delayTicks uint64) bool {
	if c.state.pressed[btn] {
		c.state.pressed[btn] = false
		c.state.wasPressed[btn] = c.state.tick + delayTicks

		return true
	}

	return false
}
