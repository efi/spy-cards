// +build headless

package internal

func LoadData(key string) ([]byte, error) {
	panic("internal: client-side data storage should not be called on server")
}

func StoreData(key string, val []byte) error {
	panic("internal: client-side data storage should not be called on server")
}
