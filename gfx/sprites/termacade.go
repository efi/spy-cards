package sprites

import "git.lubar.me/ben/spy-cards/arcade"

// Attract screen backgrounds.
var (
	AttractMK = loadSpriteSheet("img/arcade/game1.png", 14, 256, 128, true).sprite(0, 0, 256, 128, 0.52, 0.3)
	AttractFJ = loadSpriteSheet("img/arcade/game2.png", 25, 128, 64, true).sprite(0, 0, 128, 64, 0.5, 0.5)
)

// Termacade game sprites.
var (
	dungeonGameSheet = loadSpriteSheet("img/arcade/dungeongame.png", 100, 512, 512, false)

	MKKey          = dungeonGameSheet.sprite(130, 452, 45, 57, 0.5, 0.5)
	MKPotion       = dungeonGameSheet.sprite(180, 456, 37, 48, 0.5, 0.5)
	MKKnightIdle   = dungeonGameSheet.sprite(441, 427, 70, 84, 0.5, 0)
	MKAntFront     = dungeonGameSheet.sprite(392, 434, 45, 78, 0.5, 0)
	MKHeartFull    = dungeonGameSheet.sprite(135, 394, 49, 48, 0.5, 0.5)
	MKHeartEmpty   = dungeonGameSheet.sprite(189, 400, 35, 35, 0.5, 0.4989105)
	MKKnightBlock  = dungeonGameSheet.sprite(440, 338, 71, 85, 0.5280719, 0.003908045)
	MKKnightMove   = dungeonGameSheet.sprite(436, 250, 76, 83, 0.5307799, 0.004722687)
	MKKnightAttack = dungeonGameSheet.sprite(432, 165, 79, 82, 0.5, 0)
	MKAntSide      = dungeonGameSheet.sprite(372, 352, 66, 80, 0.6725016, -0.003305817)
	MKAntBack      = dungeonGameSheet.sprite(384, 270, 51, 79, 0.5435582, -0.002646531)
	UnusedParty    = [...]*Sprite{
		dungeonGameSheet.sprite(2, 85, 75, 82, 0.5, 0.5),
		dungeonGameSheet.sprite(79, 85, 75, 82, 0.5, 0.5),
		dungeonGameSheet.sprite(157, 85, 75, 82, 0.5, 0.5),
		dungeonGameSheet.sprite(2, 1, 75, 82, 0.5, 0.5),
		dungeonGameSheet.sprite(79, 1, 75, 82, 0.5, 0.5),
		dungeonGameSheet.sprite(156, 1, 75, 82, 0.5, 0.5),
	}
	FJGround = [...]*Sprite{
		dungeonGameSheet.sprite(246, 471, 36, 37, 0.5, 0.5),
		dungeonGameSheet.sprite(389, 228, 37, 37, 0.5, 0.5),
	}
	FJBee = [...]*Sprite{
		dungeonGameSheet.sprite(200, 280, 36, 33, 0.5, 0.5),
		dungeonGameSheet.sprite(241, 280, 37, 28, 0.5, 0.5),
	}
	FJWall  = dungeonGameSheet.sprite(246, 320, 36, 147, 0.5, 0.5)
	FJCloud = [...]*Sprite{
		dungeonGameSheet.sprite(289, 450, 74, 58, 0.5, 0.5),
		dungeonGameSheet.sprite(289, 401, 72, 45, 0.5, 0.5),
		dungeonGameSheet.sprite(291, 331, 71, 62, 0.5, 0.5),
	}
	FJBack   = dungeonGameSheet.sprite(405, 233, 7, 7, 0.5, 0.5)
	FJHive   = dungeonGameSheet.sprite(2, 202, 67, 115, 0.5, 0.5)
	FJFlower = dungeonGameSheet.sprite(132, 302, 39, 39, 0.5, 0.5)
	FJHoney  = dungeonGameSheet.sprite(134, 349, 37, 39, 0.5, 0.5)
	FJWasp   = [...]*Sprite{
		dungeonGameSheet.sprite(72, 235, 46, 43, 0.5, 0.5),
		dungeonGameSheet.sprite(72, 282, 55, 35, 0.4028032, 0.5),
	}
	MKFireball        = dungeonGameSheet.sprite(188, 343, 52, 45, 0.4843075, 0.4827393)
	MKWizardFront     = dungeonGameSheet.sprite(316, 244, 67, 84, 0.5, 0)
	MKWizardSide      = dungeonGameSheet.sprite(187, 174, 64, 80, 0.5746202, 0.00238651)
	MKWizardBack      = dungeonGameSheet.sprite(119, 172, 66, 84, 0.5, 0)
	MKWizardCastFront = dungeonGameSheet.sprite(316, 158, 66, 85, 0.4768423, 0)
	MKWizardCastSide  = dungeonGameSheet.sprite(254, 197, 59, 79, 0.4714805, 0)
	MKWizardCastBack  = dungeonGameSheet.sprite(236, 89, 68, 83, 0.464739, 0)
	MKCompass         = dungeonGameSheet.sprite(386, 167, 40, 53, 0.5270554, 0.4959164)
	MKWall            = dungeonGameSheet.sprite(0, 448, 64, 64, 0.5, 0)
	MKCeiling         = dungeonGameSheet.sprite(64, 448, 64, 64, 0.5, 0.5)
	MKFloor           = dungeonGameSheet.sprite(0, 384, 64, 64, 0.5, 0.5)
	MKWall2           = dungeonGameSheet.sprite(64, 384, 64, 64, 0.5, 0)
	MKDoor            = dungeonGameSheet.sprite(0, 320, 64, 64, 0.5, 0)
	MKStairs          = dungeonGameSheet.sprite(64, 320, 64, 64, 0.5, 0)
)

// Particle effect sprites.
var (
	ParticleStar             = loadSpriteSheet("img/arcade/sprite-star.png", 100, 128, 128, false).sprite(4.026751, 7.026751, 119.9219, 114.9465, 0.5001024, 0.4956501)
	ParticleSmoke            = loadSpriteSheet("img/arcade/sprite-smoke-sheet.png", 100, 128, 256, false).sprite(0.0761205, 22.02675, 127.9239, 233.9733, 0.4997025, 0.4529289)
	ParticleGrassPlaceholder = loadSpriteSheet("img/arcade/grass placeholder particle.png", 100, 16, 32, false).sprite(0, 0, 16, 32, 0.5, 0.5)
)

// Button sprites.
var (
	guiBtn = loadSpriteSheet("img/arcade/gui.png", 150, 2048, 2048, false)

	GamepadButton = [10][]*Sprite{
		arcade.BtnUp: {
			guiBtn.sprite(10.07612, 1803.076, 106.8478, 105.8478, 0.0625, 0.25),
			guiBtn.sprite(793, 1584, 107, 107, 0.0625, 0.25),
			guiBtn.sprite(1132, 1542, 96, 106, 0.0625, 0.25),
		},
		arcade.BtnDown: {
			guiBtn.sprite(395.0761, 1675.076, 106.8478, 105.8478, 0.0625, 0.25),
			guiBtn.sprite(794, 1471, 107, 107, 0.0625, 0.25),
			guiBtn.sprite(1231, 1540, 96.97325, 105, 0.0625, 0.25),
		},
		arcade.BtnLeft: {
			guiBtn.sprite(526, 1677, 102, 103, 0.0625, 0.25),
			guiBtn.sprite(902, 1584, 107, 107, 0.0625, 0.25),
			guiBtn.sprite(1235, 1440.027, 105, 96.97325, 0.0625, 0.25),
		},
		arcade.BtnRight: {
			guiBtn.sprite(268.0761, 1675.076, 105.8478, 106.8478, 0.0625, 0.25),
			guiBtn.sprite(902, 1471, 107, 107, 0.0625, 0.25),
			guiBtn.sprite(1129, 1443, 104, 96, 0.0625, 0.25),
		},
		arcade.BtnConfirm: {
			guiBtn.sprite(138, 1800, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(500, 1931, 108, 107, 0.0625, 0.25),
			guiBtn.sprite(638, 1696, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(1136, 1654, 99, 100, 0.0625, 0.25),
		},
		arcade.BtnCancel: {
			guiBtn.sprite(266, 1800, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(628, 1931, 108, 107, 0.0625, 0.25),
			guiBtn.sprite(766, 1696, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(1135, 1756, 99, 99, 0.0625, 0.25),
		},
		arcade.BtnSwitch: {
			guiBtn.sprite(395, 1800, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(756, 1931, 108, 107, 0.0625, 0.25),
			guiBtn.sprite(895, 1696, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(1237, 1653, 99, 99, 0.0625, 0.25),
		},
		arcade.BtnToggle: {
			guiBtn.sprite(523, 1800, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(884, 1931, 108, 107, 0.0625, 0.25),
			guiBtn.sprite(1023, 1696, 107, 108, 0.0625, 0.25),
			guiBtn.sprite(1236, 1757, 99, 99, 0.0625, 0.25),
		},
		arcade.BtnPause: {
			guiBtn.sprite(4, 1676.076, 121, 96.92388, 0.0625, 0.25),
			guiBtn.sprite(1121, 1947, 110, 83, 0.0625, 0.25),
			guiBtn.sprite(880, 1818, 102, 103, 0.0625, 0.25),
		},
		arcade.BtnHelp: {
			guiBtn.sprite(132, 1676.076, 121, 96.92388, 0.0625, 0.25),
			guiBtn.sprite(1121, 1858, 110, 83, 0.0625, 0.25),
			guiBtn.sprite(989, 1850, 102, 39, 0.0625, 0.25),
		},
	}
)

// Neon sprites.
var (
	neon = loadSpriteSheet("img/arcade/termacadeneon.png", 5000, 256, 256, false)

	NeonFJBee  = neon.sprite(0, 207, 65, 49, 0.5, 0.5)
	NeonFJWall = neon.sprite(69, 190, 37, 66, 0.5, 0.5)
	NeonMKWall = [...]*Sprite{
		neon.sprite(108, 175, 57, 81, 0.5, 0.5),
		neon.sprite(167, 174, 57, 82, 0.5, 0.5),
	}
	NeonMKKnight  = neon.sprite(0, 139, 65, 66, 0.5, 0.5)
	NeonHSCrown   = neon.sprite(3, 3, 85, 103, 0.5, 0.5)
	NeonHSSparkle = neon.sprite(94, 8, 57, 76, 0.5, 0.5)
	NeonSparkle   = [...]*Sprite{
		neon.sprite(156, 0, 36, 35, 0.5, 0.5),
		neon.sprite(197, 0, 43, 37, 0.5, 0.5),
	}
	NeonCoin = [...]*Sprite{
		neon.sprite(227, 40, 27, 61, 0.5, 0.5),
		neon.sprite(75, 106, 42, 63, 0.5, 0.5),
		neon.sprite(127, 106, 61, 63, 0.5, 0.5),
		neon.sprite(195, 106, 61, 65, 0.4750847, 0.4799582),
	}
)
