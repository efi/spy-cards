package match

import (
	"context"
	"strconv"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"golang.org/x/xerrors"
)

// ModeCustom is the name of the ad-hoc custom game mode.
const ModeCustom = "custom"

// Init is the initial data, known before a match is established.
type Init struct {
	// Mode is the name of the game mode, one of the following:
	// - an empty string (vanilla)
	// - "custom" (ad-hoc custom)
	// - name.v, where name is a lowercase ascii string and
	//   v is a positive integer (hosted community game mode)
	Mode string

	// Version is the Spy Cards Online version number at the time this match
	// was played.
	Version [3]uint64

	// Custom is the encoded custom cards. For vanilla, this should be empty.
	// For hosted game modes, this should match the mode's data as returned by
	// the server. For ad-hoc custom game modes, this may be any non-empty
	// string of card data.
	Custom string

	// Variant is the game mode variant. Variant may be 0 if there are no
	// variants defined by the game mode. Otherwise, Variant must be less than
	// the number of variants defined by the game mode.
	Variant uint64

	External      *card.ExternalMode
	CachedVariant *card.Variant
	Latest        bool
}

// Suffix returns the versionless game mode suffix for matchmaking.
func (i *Init) Suffix() string {
	if i.Mode == "" {
		return ""
	}

	if dot := strings.IndexByte(i.Mode, '.'); dot != -1 {
		return "-" + i.Mode[:dot]
	}

	return "-" + i.Mode
}

// Cards returns the card set referenced by this Init.
func (i *Init) Cards(ctx context.Context) (*card.Set, error) {
	switch i.Mode {
	default:
		dotIndex := strings.IndexByte(i.Mode, '.')

		if i.Mode[dotIndex+1:] == "0" || i.Custom == "" {
			data, err := card.FetchMode(ctx, i.Mode[:dotIndex], i.Mode[dotIndex+1:])
			if err != nil {
				return nil, xerrors.Errorf("match: fetching custom game mode: %w", err)
			}

			i.Latest = i.Mode[dotIndex+1:] == "0"
			i.External = data

			i.Mode = i.Mode[:dotIndex+1] + strconv.Itoa(data.Revision)
			i.Custom = data.Cards
		}

		fallthrough
	case ModeCustom:
		var sets card.Sets

		if err := sets.UnmarshalText([]byte(i.Custom)); err != nil {
			return nil, xerrors.Errorf("match: decoding custom cards: %w", err)
		}

		set, err := sets.Apply()
		if err != nil {
			return nil, xerrors.Errorf("match: combining custom card sets: %w", err)
		}

		switch variants := set.Mode.GetAll(card.FieldVariant); {
		case len(variants) == 0:
			if i.Variant != 0 {
				return nil, xerrors.Errorf("match: unexpected variant %d with no defined variants", i.Variant)
			}
		case len(variants) <= int(i.Variant):
			return nil, xerrors.Errorf("match: unexpected variant %d (range is [0, %d])", i.Variant, len(variants)-1)
		default:
			set.Variant = int(i.Variant)
			i.CachedVariant = variants[i.Variant].(*card.Variant)
		}

		return set, nil
	case "": // vanilla
		if i.Custom != "" {
			return nil, xerrors.New("match: unexpected custom card data for vanilla")
		}

		return &card.Set{}, nil
	}
}

// Match is the data for a specific match.
type Match struct {
	// Init holds data known before the match is established.
	Init *Init

	// Start is the timestamp of the start of the first round. That is, when
	// both players finished selecting their decks.
	Start time.Time

	// Rematches is the number of matches that happened within this
	// matchmaking session so far. It starts at 0 and increases by 1 each
	// time the players agree to a rematch.
	Rematches uint64

	// Perspective is the player number (either 1 or 2, for hosting player or
	// joining player, respectively).
	Perspective uint8

	UnknownDeck uint8

	// Cosmetic holds non-critical cosmetic data for the players of the match.
	Cosmetic [2]card.CosmeticData

	// Set is the modified card set, with changes applied by spoiler guard, etc.
	Set *card.Set

	// Rules is a copy of the rules from Set for convenience.
	Rules card.GameRules

	// Timer is a copy of the timer from Set for convenience.
	Timer card.Timer

	// Banned is a cache of banned cards from Set.
	Banned map[card.ID]bool

	// Unpickable is a cache of unpickable cards from Set.
	Unpickable map[card.ID]bool

	// Unfiltered is a cache of unfiltered cards from Set.
	Unfiltered map[card.ID]bool

	HasReplaceSummon map[card.ID]bool
	OrderForSummon   map[card.ID]int

	// State is the mutable state of the match.
	State State

	// Log is the game log.
	Log GameLog
}

// State is the mutable state of the match.
type State struct {
	// Round is the current round number.
	Round uint64

	// Sides holds state that is separate for each player.
	Sides [2]Side

	// Queue is the card effect queue.
	Queue []CardEffect

	PreQueue []CardEffect

	// OnNumb is the effects that activate on numb.
	OnNumb []CardEffect

	// MaxPriority is the latest effect priority processed this turn.
	MaxPriority uint8

	// RoundWinner is the player who won this round.
	RoundWinner uint8

	// TurnData is used to record networked card modifications.
	TurnData *card.TurnData
}

func (s *State) clone() *State {
	return &State{
		Round:       s.Round,
		RoundWinner: s.RoundWinner,
		Sides: [2]Side{
			s.Sides[0].clone(),
			s.Sides[1].clone(),
		},
	}
}

// Side is mutable state for a single player.
type Side struct {
	HP       Number
	TP       Number
	ATK      Number
	DEF      Number
	RawATK   Number
	RawDEF   Number
	FinalATK Number
	FinalDEF Number

	HealMulP int64
	HealMulN int64
	HealP    Number
	HealN    Number

	Hand    []HandCard
	Deck    []*Card
	Discard []*Card
	Field   []*ActiveCard
	Setup   []*ActiveCard

	Limit map[*card.EffectDef]int64
	ModTP map[card.ID]int64
}

func (s *Side) clone() Side {
	return Side{
		HP:       s.HP,
		TP:       s.TP,
		HealMulP: 1,
		HealMulN: 1,
		Hand:     cloneHandCards(s.Hand),
		Deck:     cloneCards(s.Deck),
		Discard:  cloneCards(s.Discard),
		Setup:    cloneActiveCards(s.Setup),
	}
}

func cloneCards(cards []*Card) []*Card {
	clone := make([]*Card, len(cards))
	copy(clone, cards)

	return clone
}

func cloneHandCards(cards []HandCard) []HandCard {
	clone := make([]HandCard, len(cards))
	copy(clone, cards)

	return clone
}

func cloneActiveCards(cards []*ActiveCard) []*ActiveCard {
	clone := make([]*ActiveCard, len(cards))

	for i, c := range cards {
		effects := make([]*card.EffectDef, len(c.Effects))
		copy(effects, c.Effects)

		clone[i] = &ActiveCard{
			Card:    c.Card,
			Mode:    c.Mode,
			Desc:    c.Desc,
			Effects: effects,
		}
	}

	return clone
}

// CardEffect is a queued effect.
type CardEffect struct {
	Card   *ActiveCard
	Effect *card.EffectDef
	Player uint8
}

// Number is an integer that may be infinite or invalid.
type Number struct {
	Amount    int64
	AmountInf int64
	NaN       bool
}

func (n Number) String() string {
	if n.NaN {
		return "NaN"
	}

	switch {
	case n.AmountInf > 0:
		return "∞"
	case n.AmountInf < 0:
		return "-∞"
	default:
		return strconv.FormatInt(n.Amount, 10)
	}
}

// Add increases the value of n by x.
func (n *Number) Add(x Number) {
	if n.NaN || x.NaN || (n.AmountInf < 0 && x.AmountInf > 0) || (n.AmountInf > 0 && x.AmountInf < 0) {
		n.Amount = 0
		n.AmountInf = 0
		n.NaN = true

		return
	}

	n.Amount += x.Amount
	n.AmountInf += x.AmountInf
}

// Less returns true if n is less than x.
func (n Number) Less(x Number) bool {
	switch {
	case n.NaN, x.NaN:
		return false
	case n.AmountInf > 0 && x.AmountInf > 0:
		return false
	case n.AmountInf < 0 && x.AmountInf < 0:
		return false
	case n.AmountInf > 0, x.AmountInf < 0:
		return false
	case n.AmountInf < 0, x.AmountInf > 0:
		return true
	default:
		return n.Amount < x.Amount
	}
}

// Winner returns the number of the player who won the match, or 0 if the
// match is incomplete.
func (m *Match) Winner() uint8 {
	p1Dead := m.State.Sides[0].HP.Less(Number{Amount: 1})
	p2Dead := m.State.Sides[1].HP.Less(Number{Amount: 1})

	switch {
	case !p1Dead && !p2Dead:
		return 0
	case !p1Dead && p2Dead:
		return 1
	case p1Dead && !p2Dead:
		return 2
	default:
		return m.State.RoundWinner
	}
}

// Network is the required networking interface for card effect processing.
type Network interface {
	SendModifiedCards([]card.ModifiedCardPosition)
	RecvModifiedCards() []card.ModifiedCardPosition
}
