// +build !headless

package highscores

import (
	"image/color"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

func (hs *HighScores) render() {
	var gameTitle, smallTitle string

	switch hs.Game {
	case arcade.FlowerJourney:
		gameTitle = "Flower Journey"
	case arcade.MiteKnight:
		gameTitle = "Mite Knight"
	default:
		panic(xerrors.Errorf("highscores: missing game title for %v", hs.Game))
	}

	if hs.Mode == Recent {
		gameTitle = "Latest " + gameTitle + " Scores"
	} else {
		gameTitle += " High Scores"
	}

	switch hs.Mode {
	case Recent:
		smallTitle = "(Most Recent)"
	case Weekly:
		smallTitle = "(This Week)"
	case Quarterly:
		smallTitle = "(3 Months)"
	case AllTime:
		smallTitle = "(All Time)"
	}

	gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	if hs.batch == nil {
		hs.batch = sprites.NewBatch(&hs.Camera)
	} else {
		hs.batch.Reset(&hs.Camera)
	}

	hs.batch.Append(sprites.Blank, 0, 6, 0, 20, 2, color.RGBA{31, 31, 31, 255}, 0, 0, 0, 0)
	hs.batch.Append(sprites.Blank, 6.5, 0, 0, 6, 10, color.RGBA{31, 31, 31, 255}, 0, 0, 0, 0)
	hs.batch.Render()

	if hs.tb == nil {
		hs.tb = sprites.NewTextBatch(&hs.Camera)
	} else {
		hs.tb.Reset(&hs.Camera)
	}

	sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, gameTitle, 0, 5.5, 0, 1.5, 1.5, sprites.Rainbow, true)
	sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, smallTitle, 5.4, 4.5, 0, 0.75, 0.75, sprites.White, true)
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnConfirm)+"View", 4, 3-0*0.7, 0, 1, 1, sprites.White)
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnCancel)+"Exit", 4, 3-1*0.7, 0, 1, 1, sprites.White)
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnSwitch)+"Filter", 4, 3-2*0.7, 0, 1, 1, sprites.White)
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnToggle)+"Game", 4, 3-3*0.7, 0, 1, 1, sprites.White)
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnUp)+"Prev. Rank", 4, 3-4*0.7, 0, 1, 1, whiteOrGray(hs.Page != 0 || hs.Index != 0))
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnDown)+"Next Rank", 4, 3-5*0.7, 0, 1, 1, whiteOrGray(hs.Index+1 < len(hs.cur) || len(hs.next) != 0))
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnLeft)+"Prev. Page", 4, 3-6*0.7, 0, 1, 1, whiteOrGray(hs.Page != 0))
	sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, sprites.Button(arcade.BtnRight)+"Next Page", 4, 3-7*0.7, 0, 1, 1, whiteOrGray(len(hs.next) != 0))

	switch {
	case hs.cur != nil && hs.loaded.Sub(hs.loading) > time.Second/4 && time.Since(hs.loaded) < time.Second/4:
		// keep showing "loading" just to avoid flickering text
		sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, "Loading...", -2, 1, 0, 2, 2, sprites.Gray, false)
	case hs.cur == nil && time.Since(hs.loading) > time.Second/4:
		sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, "Loading...", -2, 1, 0, 2, 2, sprites.Gray, false)
	case hs.cur != nil:
		if len(hs.cur) == 0 {
			sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, "There are no scores", -2, 2, 0, 1, 1, color.RGBA{191, 191, 191, 255}, true)
			sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, "on this page.", -2, 1.3, 0, 1, 1, color.RGBA{191, 191, 191, 255}, true)
		}

		for i, entry := range hs.cur {
			rowColor := sprites.White
			if hs.Index == i {
				rowColor = sprites.Yellow
			}

			if !hs.first || hs.loaded.Add(time.Duration(i)*time.Second/2).Before(time.Now()) {
				sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, strconv.Itoa(hs.Page*5+i+1)+".", -7.4, 3.5-float32(i)*1.4, 0, 1.5, 1.5, sprites.White)
				sprites.DrawTextShadow(hs.tb, sprites.FontD3Streetism, entry.Player, -5.3, 3.5-float32(i)*1.4, 0, 2, 2, rowColor)
			}

			if !hs.first || hs.loaded.Add(time.Duration(i)*time.Second/2+time.Second/4).Before(time.Now()) {
				sprites.DrawTextCentered(hs.tb, sprites.FontD3Streetism, strconv.FormatInt(entry.Score, 10), 0.5, 3.5-float32(i)*1.4, 0, 2, 2, rowColor, true)
			}
		}
	}

	hs.tb.Render()
}
