"use strict";
var disableDoSquish = false;
function doSquish(el) {
    if (disableDoSquish) {
        return;
    }
    el.querySelectorAll(".squish").forEach((s) => {
        const max = parseInt(getComputedStyle(s.parentElement).width.replace("px", ""), 10);
        s.style.transform = "";
        const current = s.clientWidth;
        if (current > max) {
            s.style.transform = "scaleX(calc(" + max + " / " + current + "))";
        }
    });
    el.querySelectorAll(".squishy").forEach((s) => {
        const max = parseInt(getComputedStyle(s.parentElement).height.replace("px", ""), 10);
        s.style.transform = "";
        const current = s.clientHeight;
        if (current > max) {
            s.style.transform = "scaleY(calc(" + max + " / " + current + "))";
        }
    });
}
addEventListener("load", function () {
    doSquish(document);
});
function sleep(ms) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve();
        }, ms);
    });
}
var SpyCards;
(function (SpyCards) {
    var Binary;
    (function (Binary) {
        // JavaScript bitwise operators work on signed 32-bit integers.
        // These helper functions work around this.
        function bit(n) {
            let base = 1;
            while (n > 30) {
                base *= 1 << 30;
                n -= 30;
            }
            return (1 << n) * base;
        }
        Binary.bit = bit;
        function getBit(x, n) {
            while (n > 30) {
                x /= 1 << 30;
                n -= 30;
            }
            return ((x >> n) & 1);
        }
        Binary.getBit = getBit;
        function setBit(x, n, b) {
            const ob = getBit(x, n);
            if (b === ob) {
                return x;
            }
            return b ? x + bit(n) : x - bit(n);
        }
        Binary.setBit = setBit;
        function pushUVarInt(buf, x) {
            while (x >= 0x80) {
                buf.push(x & 0xff | 0x80);
                x /= 1 << 7;
            }
            buf.push(x | 0);
        }
        Binary.pushUVarInt = pushUVarInt;
        function pushSVarInt(buf, x) {
            let ux = x * 2;
            if (x < 0) {
                ux = -ux + 1;
            }
            pushUVarInt(buf, ux);
        }
        Binary.pushSVarInt = pushSVarInt;
        function shiftUVarInt(buf) {
            let x = 0, s = 0;
            while (buf.length) {
                const b = buf.shift();
                x += (b & 0x7f) * Math.pow(2, s);
                s += 7;
                if (b < 0x80) {
                    return x;
                }
            }
            throw new Error("reached end of buffer");
        }
        Binary.shiftUVarInt = shiftUVarInt;
        function shiftSVarInt(buf) {
            const ux = shiftUVarInt(buf);
            let x = ux / 1;
            if (ux & 1) {
                x = -x - 0.5;
            }
            return x;
        }
        Binary.shiftSVarInt = shiftSVarInt;
        function pushUTF8String1(buf, s) {
            const b = new TextEncoder().encode(s || "");
            if (b.length > 255) {
                throw new Error("string length (" + b.length + " bytes) longer than maximum (255)");
            }
            buf.push(b.length);
            buf.push(...SpyCards.toArray(b));
        }
        Binary.pushUTF8String1 = pushUTF8String1;
        function shiftUTF8String1(buf) {
            const length = buf.shift();
            if (buf.length < length) {
                throw new Error("buffer too short to hold " + length + "-byte string");
            }
            const b = new Uint8Array(buf.splice(0, length));
            return new TextDecoder().decode(b);
        }
        Binary.shiftUTF8String1 = shiftUTF8String1;
        function pushUTF8StringVar(buf, s) {
            const b = new TextEncoder().encode(s || "");
            pushUVarInt(buf, b.length);
            buf.push(...SpyCards.toArray(b));
        }
        Binary.pushUTF8StringVar = pushUTF8StringVar;
        function shiftUTF8StringVar(buf) {
            const length = shiftUVarInt(buf);
            if (buf.length < length) {
                throw new Error("buffer too short to hold " + length + "-byte string");
            }
            const b = new Uint8Array(buf.splice(0, length));
            return new TextDecoder().decode(b);
        }
        Binary.shiftUTF8StringVar = shiftUTF8StringVar;
    })(Binary = SpyCards.Binary || (SpyCards.Binary = {}));
})(SpyCards || (SpyCards = {}));
(function (SpyCards) {
    async function fetchCustomCardSet(name, revision) {
        const url = revision ?
            (custom_card_api_base_url + "get-revision/" + encodeURIComponent(name) + "/" + encodeURIComponent(revision)) :
            (custom_card_api_base_url + "latest/" + encodeURIComponent(name));
        const response = await fetch(url, { mode: "cors" });
        if (response.status !== 200) {
            throw new Error("Server returned " + response.status + " " + response.statusText);
        }
        return await response.json();
    }
    SpyCards.fetchCustomCardSet = fetchCustomCardSet;
    async function parseCustomCards(codes, variant, allowInvalid) {
        let mode = null;
        const customCardsRaw = [];
        const cards = [];
        const usedIDs = [];
        for (let group of codes.split(";")) {
            if (group.indexOf(".") !== -1 && group.indexOf(",") === -1) {
                const [modeName, revisionStr] = group.split(/\./);
                const revision = parseInt(revisionStr, 10);
                const mode = await fetchCustomCardSet(modeName, revision);
                group = mode.Cards;
            }
            let groupMode = null;
            const groupCards = [];
            const groupUsedIDs = [];
            const idsToReplace = [];
            let first = true;
            let second = false;
            for (let code of group.split(",")) {
                const codeBuf = SpyCards.toArray(Base64.decode(code));
                if (first) {
                    first = false;
                    if (codeBuf[0] === 3) {
                        groupMode = new SpyCards.GameModeData();
                        groupMode.unmarshal(codeBuf);
                        if (mode) {
                            mode.fields = mode.fields.concat(groupMode.fields);
                        }
                        else {
                            mode = groupMode;
                        }
                        second = true;
                        continue;
                    }
                }
                if (second) {
                    second = false;
                    const codeBufClone = codeBuf.slice(0);
                    const encodedVariant = SpyCards.Binary.shiftUVarInt(codeBufClone);
                    if (codeBufClone.length === 0) {
                        if (variant === "parse-variant") {
                            variant = encodedVariant;
                        }
                        continue;
                    }
                }
                const card = new SpyCards.CardDef();
                card.unmarshal(codeBuf);
                if (groupUsedIDs.indexOf(card.id) !== -1) {
                    throw new Error("multiple cards replacing ID " + card.id + " (" + card.originalName() + ")");
                }
                groupUsedIDs.push(card.id);
                if (usedIDs.indexOf(card.id) !== -1) {
                    if (card.id < 128) {
                        if (allowInvalid) {
                            continue;
                        }
                        throw new Error("multiple card groups replacing ID " + card.id + " (" + card.originalName() + ")");
                    }
                    idsToReplace.push(card.id);
                }
                else {
                    usedIDs.push(card.id);
                }
                groupCards.push(card);
            }
            for (let id of idsToReplace) {
                let replacementIDIndex = -1;
                let replacementID;
                do {
                    replacementIDIndex++;
                    replacementID = 128 + ((replacementIDIndex & ~31) << 2) + (replacementIDIndex & 31) + (id & 96);
                } while (usedIDs.indexOf(replacementID) !== -1 || groupUsedIDs.indexOf(replacementID) !== -1);
                usedIDs.push(replacementID);
                if (groupMode) {
                    for (let field of groupMode.fields) {
                        field.replaceCardID(id, replacementID);
                    }
                }
                for (let card of groupCards) {
                    if (card.id === id) {
                        card.id = replacementID;
                    }
                    for (let effect of card.effects) {
                        replaceID(effect, id, replacementID);
                    }
                }
            }
            cards.push(...groupCards);
            if (customCardsRaw.length) {
                customCardsRaw.length = 0;
                if (mode) {
                    const buf = [];
                    mode.marshal(buf);
                    customCardsRaw.push(Base64.encode(new Uint8Array(buf)));
                }
                for (let card of cards) {
                    const buf = [];
                    card.marshal(buf);
                    customCardsRaw.push(Base64.encode(new Uint8Array(buf)));
                }
            }
            else {
                customCardsRaw.push(...group.split(","));
            }
        }
        const variants = mode ? mode.getAll(SpyCards.GameModeFieldType.Variant) : [];
        if (variant === "parse-variant") {
            variant = 0;
        }
        if (variant !== "ignore-variant" && variants.length) {
            const variantID = [];
            variant = Math.min(Math.max(variant || 0, 0), variants.length - 1);
            SpyCards.Binary.pushUVarInt(variantID, variant);
            customCardsRaw.splice(1, 0, Base64.encode(new Uint8Array(variantID)));
            mode.fields.push(...variants[variant].rules);
        }
        else {
            variant = null;
        }
        return { mode, cards, customCardsRaw, variant: variant };
        function replaceID(effect, oldID, newID) {
            if (effect.card === oldID) {
                effect.card = newID;
            }
            if (effect.result) {
                replaceID(effect.result, oldID, newID);
            }
            if (effect.tailsResult) {
                replaceID(effect.tailsResult, oldID, newID);
            }
        }
    }
    SpyCards.parseCustomCards = parseCustomCards;
    function loadSettings() {
        if (localStorage["spy-cards-settings-v0"]) {
            return JSON.parse(localStorage["spy-cards-settings-v0"]);
        }
        const settings = {
            audio: {
                music: 0,
                sounds: 0
            }
        };
        if (localStorage["spy-cards-audio-settings-v0"]) {
            const legacyAudio = JSON.parse(localStorage["spy-cards-audio-settings-v0"]);
            settings.audio.music = legacyAudio.enabled || legacyAudio.musicEnabled ? 0.6 : 0;
            settings.audio.sounds = legacyAudio.enabled || legacyAudio.sfxEnabled ? 0.6 : 0;
        }
        if (localStorage["spy-cards-player-sprite-v0"]) {
            settings.character = localStorage["spy-cards-player-sprite-v0"];
        }
        saveSettings(settings);
        delete localStorage["spy-cards-audio-settings-v0"];
        delete localStorage["spy-cards-player-sprite-v0"];
        return settings;
    }
    SpyCards.loadSettings = loadSettings;
    function saveSettings(settings) {
        localStorage["spy-cards-settings-v0"] = JSON.stringify(settings);
        if (navigator.serviceWorker && navigator.serviceWorker.controller) {
            navigator.serviceWorker.controller.postMessage({ type: "settings-changed" });
        }
        window.dispatchEvent(new Event("spy-cards-settings-changed"));
    }
    SpyCards.saveSettings = saveSettings;
})(SpyCards || (SpyCards = {}));
