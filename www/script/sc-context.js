"use strict";
var SpyCards;
(function (SpyCards) {
    class MatchData {
        constructor() {
            this.customModeName = "";
            this.rematchCount = 0;
            this.losses = 0;
            this.wins = 0;
        }
    }
    SpyCards.MatchData = MatchData;
    class MatchState {
        constructor(defs, npcState) {
            this.hp = [0, 0];
            this.tp = [0, 0];
            this.rawATK = [0, 0];
            this.rawDEF = [0, 0];
            this.ATK = [0, 0];
            this.DEF = [0, 0];
            this.healTotalN = [0, 0];
            this.healTotalP = [0, 0];
            this.healMultiplierN = [1, 1];
            this.healMultiplierP = [1, 1];
            this.turn = 0;
            this.deck = [[], []];
            this.backs = [[], []];
            this.hand = [[], []];
            this.discard = [[], []];
            this.once = [[], []];
            this.setup = [[], []];
            this.ready = [false, false];
            this.played = [[], []];
            this.winner = 0;
            this.players = [null, null];
            this.effectBacklog = [];
            this.gameLog = new SpyCards.GameLog();
            this.hp[0] = this.hp[1] = defs.rules.maxHP;
            this.npcState = npcState;
        }
    }
    SpyCards.MatchState = MatchState;
})(SpyCards || (SpyCards = {}));
