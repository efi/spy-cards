package gui

type Checkbox struct {
	*Button
	state bool
}

func NewCheckbox(label string) *Checkbox {
	c := new(Checkbox)
	c.Button = NewButton(label)
	return c
}

func action(c *Checkbox, e Event) {
	c.state = !c.state
}
