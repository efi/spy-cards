// Package matchnet handles networking for Spy Cards Online.
package matchnet

import (
	"context"
	"log"
	"sync"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/spoilerguard"
	"github.com/pion/webrtc/v3"
	"golang.org/x/xerrors"
	"nhooyr.io/websocket"
)

// DefaultICEServers are the default servers used for interactive connectivity establishment.
var DefaultICEServers = []webrtc.ICEServer{
	{URLs: []string{"stun:stun.stunprotocol.org"}},
	{URLs: []string{"stun:stun.sipnet.net"}},
}

type GameConn struct {
	SCG     *SecureCardGame
	sc      *SignalConn
	conn    *webrtc.PeerConnection
	secure  *webrtc.DataChannel
	control *webrtc.DataChannel

	secureBuf        chan []byte
	spoilerGuardData chan []byte
	customCardData   chan []byte
	mode             *string
	cards            *card.Set
	recvCosmetic     *card.CosmeticData
	opponentDeck     chan card.UnknownDeck
	opponentReady    chan []byte
	recvInHand       chan []byte
	recvModifyCards  chan []byte

	stateLock    sync.Mutex
	stateCond    *sync.Cond
	closeCode    websocket.StatusCode
	closeReason  string
	connState    ConnState
	logicalState LogicalState
	isHost       bool
	sentReady    bool
	recvReady    bool
}

func NewGameConn(ctx context.Context) (*GameConn, error) {
	var (
		err error
		id0 uint16 = 0
		id1 uint16 = 1

		negotiated = true
	)

	gc := &GameConn{}

	gc.SCG, err = NewSecureCardGame(nil)
	if err != nil {
		return nil, xerrors.Errorf("matchnet: creating secure card game instance: %w", err)
	}

	gc.stateCond = sync.NewCond(&gc.stateLock)

	gc.conn, err = webrtc.NewPeerConnection(webrtc.Configuration{
		ICEServers:           DefaultICEServers,
		ICECandidatePoolSize: 4,
	})
	if err != nil {
		return nil, xerrors.Errorf("matchnet: creating peer connection: %w", err)
	}

	gc.secure, err = gc.conn.CreateDataChannel("secure", &webrtc.DataChannelInit{
		Negotiated: &negotiated,
		ID:         &id0,
	})
	if err != nil {
		return nil, xerrors.Errorf("matchnet: creating data channel 0: %w", err)
	}

	gc.control, err = gc.conn.CreateDataChannel("control", &webrtc.DataChannelInit{
		Negotiated: &negotiated,
		ID:         &id1,
	})
	if err != nil {
		return nil, xerrors.Errorf("matchnet: creating data channel 1: %w", err)
	}

	gc.secure.OnMessage(gc.onSecureMessage)
	gc.control.OnMessage(gc.onControlMessage)

	gc.secureBuf = make(chan []byte, 1)
	gc.spoilerGuardData = make(chan []byte)
	gc.customCardData = make(chan []byte)
	gc.opponentDeck = make(chan card.UnknownDeck)
	gc.opponentReady = make(chan []byte, 1)
	gc.recvInHand = make(chan []byte, 1)
	gc.recvModifyCards = make(chan []byte, 128)

	gc.conn.OnConnectionStateChange(gc.onConnectionStateChange)

	return gc, nil
}

// UseSignal negotiates a card game connection using a SingalConn.
func (gc *GameConn) UseSignal(ctx context.Context, sc *SignalConn, isHost bool, mode *string, cards *card.Set) error {
	gc.mode = mode
	gc.cards = cards
	gc.isHost = isHost
	gc.sc = sc

	sc.onRelayedMessage = gc.onRelayedMessage
	sc.onHandshake = gc.handshake
	sc.onCloseCode = gc.onCloseCode

	gc.conn.OnICECandidate(gc.onICECandidate)

	if sc.didHandshake {
		gc.handshake()
	}

	return nil
}

func (gc *GameConn) onConnectionStateChange(s webrtc.PeerConnectionState) {
	switch s {
	case webrtc.PeerConnectionStateConnecting:
		gc.setState(StateRTCConnecting)
	case webrtc.PeerConnectionStateConnected:
		gc.stateLock.Lock()
		if gc.logicalState == StateUninitialized {
			gc.logicalState = StateSecureInitializing
			go gc.initSecure()
		}
		gc.stateLock.Unlock()

		gc.setState(StateRTCConnected)
	case webrtc.PeerConnectionStateDisconnected:
		gc.setState(StateRTCDisconnected)
	case webrtc.PeerConnectionStateFailed:
		gc.setState(StateRTCFailed)

		if err := gc.sc.sendRelayedMessage(&relayedMessage{Type: msgUseRelay}); err != nil {
			log.Println("ERROR: sending relay request:", err)

			gc.setState(StateRelayFailed)
		} else {
			gc.setState(StateOfferedRelay)
		}
	}
}

func (gc *GameConn) onICECandidate(c *webrtc.ICECandidate) {
	if c == nil {
		return
	}

	c2 := c.ToJSON()

	if err := gc.sc.sendRelayedMessage(&relayedMessage{
		Type:      msgIceCandidate,
		Candidate: &c2,
	}); err != nil {
		panic(err)
	}
}

func (gc *GameConn) handshake() {
	if gc.sc.fake != nil {
		gc.setState(StateUsingRelay)

		return
	}

	gc.setState(StateHandshakeWait)

	if gc.isHost {
		offer, err := gc.conn.CreateOffer(nil)
		if err != nil {
			panic(err)
		}

		err = gc.conn.SetLocalDescription(offer)
		if err != nil {
			panic(err)
		}

		err = gc.sc.sendRelayedMessage(&relayedMessage{
			Type:  msgPlayer1Offer,
			Offer: gc.conn.LocalDescription(),
		})
		if err != nil {
			panic(err)
		}
	}
}

func (gc *GameConn) onRelayedMessage(m *relayedMessage) {
	switch m.Type {
	case msgPlayer1Offer:
		if gc.isHost {
			log.Println("WARNING: received unexpected Offer message as host")

			return
		}

		if err := gc.conn.SetRemoteDescription(*m.Offer); err != nil {
			panic(err)
		}

		answer, err := gc.conn.CreateAnswer(nil)
		if err != nil {
			panic(err)
		}

		err = gc.conn.SetLocalDescription(answer)
		if err != nil {
			panic(err)
		}

		err = gc.sc.sendRelayedMessage(&relayedMessage{
			Type:   msgPlayer2Answer,
			Answer: gc.conn.LocalDescription(),
		})
		if err != nil {
			panic(err)
		}

		gc.setState(StateHandshakeClientWait)
	case msgPlayer2Answer:
		if !gc.isHost {
			log.Println("WARNING: received unexpected Answer message as client")

			return
		}

		err := gc.conn.SetRemoteDescription(*m.Answer)
		if err != nil {
			panic(err)
		}

		gc.setState(StateHandshakeHostWait)
	case msgIceCandidate:
		if m.Candidate != nil {
			if gc.conn.RemoteDescription() == nil {
				return
			}

			if err := gc.conn.AddICECandidate(*m.Candidate); err != nil {
				panic(err)
			}
		}
	case msgUseRelay:
		if s, _ := gc.State(); s == StateOpponentWantsRelay {
			return
		}

		gc.setState(StateOpponentWantsRelay)

		if cs, _ := gc.State(); cs != StateUsingRelay {
			if err := gc.sc.sendRelayedMessage(&relayedMessage{
				Type: msgUseRelay,
			}); err != nil {
				panic(err)
			}

			gc.setState(StateOfferedRelay)
		}
	case msgRelay:
		switch m.Channel {
		case "control":
			gc.onControlMessage(webrtc.DataChannelMessage{Data: m.Payload})
		case "secure":
			gc.onSecureMessage(webrtc.DataChannelMessage{Data: m.Payload})
		default:
			log.Println("ERROR: unknown channel name:", m.Channel)
		}
	default:
		log.Panicln("relayed message type:", m.Type)
	}
}

func (gc *GameConn) initSecure() {
	if cs, _ := gc.State(); cs != StateUsingRelay {
		for _, channel := range []*webrtc.DataChannel{gc.control, gc.secure} {
			if channel.ReadyState() != webrtc.DataChannelStateOpen {
				ch := make(chan struct{})

				channel.OnOpen(func() { close(ch) })

				<-ch

				channel.OnOpen(func() {})
			}
		}
	}

	var modeSuffix string

	if gc.isHost && *gc.mode != "" {
		modeSuffix = "-" + *gc.mode
	}

	if err := gc.send(msgVersion, append(internal.VersionNumber(), modeSuffix...)); err != nil {
		panic(err)
	}

	playerNumber := uint8(2)
	if gc.isHost {
		playerNumber = 1
	}

	if err := gc.SCG.Init(playerNumber, gc.exchangeData); err != nil {
		panic(err)
	}

	gc.setLogicalState(StateSecureReady)

	if gc.isHost {
		if variants := gc.cards.Mode.GetAll(card.FieldVariant); len(variants) != 0 {
			gc.cards.Mode.Fields = append(gc.cards.Mode.Fields, variants[gc.cards.Variant].(*card.Variant).Rules...)
		}

		b, err := gc.cards.MarshalText()
		if err != nil {
			panic(err)
		}

		if err = gc.send(msgCustomCards, b); err != nil {
			panic(err)
		}
	} else {
		err := gc.cards.UnmarshalText(<-gc.customCardData)
		if err != nil {
			panic(err)
		}
	}

	go func() {
		sg := spoilerguard.LoadData()
		if sg != nil && len(sg.SeenSpiedEnemies) == 256/8 {
			err := gc.send(msgSpoilerGuard, sg.SeenSpiedEnemies)
			if err != nil {
				panic(err)
			}
		} else {
			err := gc.send(msgSpoilerGuard, nil)
			if err != nil {
				panic(err)
			}
		}
	}()

	spoilerguard.Apply(context.TODO(), gc.cards, <-gc.spoilerGuardData)

	gc.setLogicalState(StateMatchReady)

	if cs, _ := gc.State(); cs == StateRTCConnected {
		err := gc.sc.Close()
		if err != nil {
			panic(err)
		}
	}
}
