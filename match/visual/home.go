// +build !headless

package visual

import (
	"context"
	"image/color"
	"log"
	"math"
	"strings"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/matchnet"
	"git.lubar.me/ben/spy-cards/match/minimal"
	"git.lubar.me/ben/spy-cards/match/npc"
	"git.lubar.me/ben/spy-cards/room"
	"golang.org/x/xerrors"
)

func (v *Visual) updateHome(ctx context.Context) {
	curX, curY, click := v.ictx.Mouse()
	lastX, lastY, lastClick := v.ictx.LastMouse()

	w, h := gfx.Size()

	v.modeX, v.modeY = 0, float32(h)/128-2
	v.varX, v.varY = 0, float32(h)/128-3

	var quickStacked, horizontalLayout, verticalLayout bool

	variant := v.Match.Init.CachedVariant
	isSingleplayer := variant != nil && variant.NPC != ""

	switch aspect := float32(w) / float32(h); {
	case aspect > 2:
		v.hostX, v.hostY = -float32(w)/128/1.5, float32(h)/128-5.5
		v.joinX, v.joinY = 0, float32(h)/128-5.5
		v.quickX, v.quickY = float32(w)/128/1.5, float32(h)/128-4.75
		quickStacked = true

		if h < 450 {
			v.hostY++
			v.joinY++
			v.quickY++
		}

		horizontalLayout = true
	case aspect < 0.75:
		v.hostX, v.hostY = 0, float32(h)/128-5
		v.joinX, v.joinY = 0, float32(h)/128-7.25
		v.quickX, v.quickY = 0, float32(h)/128-9
		quickStacked = true

		if h > 750 {
			v.hostY--
			v.joinY--
			v.quickY--
		}

		if variant == nil {
			v.hostY += 0.75
			v.joinY += 0.75
			v.quickY += 0.75
		}

		verticalLayout = true
	default:
		v.hostX, v.hostY = -float32(w)/128/2, float32(h)/128-5
		v.joinX, v.joinY = float32(w)/128/2, float32(h)/128-5
		v.quickX, v.quickY = 0, float32(h)/128-7.25
		quickStacked = false
	}

	v.hostText = [2]string{"Host", "Invite a friend to play."}

	if isSingleplayer {
		v.hostX = (v.hostX + v.joinX) / 2
		v.hostY = (v.hostY + v.joinY) / 2
		v.hostText = [2]string{"Singleplayer", "Face off against a computer-controlled opponent."}
	}

	switch {
	default:
		v.quickText = [3]string{"Quick", "Join", "Play against a stranger."}
	case v.Match.Init.Mode == match.ModeCustom:
		v.quickText = [3]string{"Card", "Editor", "Create cards and game modes."}
	case isSingleplayer:
		v.hostX = 0

		fallthrough
	case !v.allowQuickJoin:
		v.quickText = [3]string{}
	}

	if !quickStacked {
		v.quickText[0], v.quickText[1] = v.quickText[0]+" "+v.quickText[1], ""
	}

	if lastX != curX || lastY != curY {
		switch {
		case variant != nil && sprites.TextHitTest(&v.cam, sprites.FontD3Streetism, variant.Title, v.varX, v.varY, 0, 1.25, 1.25, curX, curY, true):
			v.state = StateOnVariant
		case sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, v.hostText[0], v.hostX, v.hostY, 0, 2.5, 2.5, curX, curY, true):
			if v.Match.Init.Mode == match.ModeCustom && v.Match.Init.Custom == "" {
				v.state = StateHome
			} else {
				v.state = StateOnHost
			}
		case !isSingleplayer && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, "Join", v.joinX, v.joinY, 0, 2.5, 2.5, curX, curY, true):
			v.state = StateOnJoin
		case v.quickText[0] != "" && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, v.quickText[0], v.quickX, v.quickY, 0, 1.5, 1.5, curX, curY, true):
			v.state = StateOnQuickJoin
		case v.quickText[1] != "" && sprites.TextHitTest(&v.cam, sprites.FontBubblegumSans, v.quickText[1], v.quickX, v.quickY-0.9, 0, 1.5, 1.5, curX, curY, true):
			v.state = StateOnQuickJoin
		default:
			v.state = StateHome
		}
	}

	blankCustom := v.Match.Init.Mode == match.ModeCustom && v.Match.Init.Custom == ""

	switch {
	case v.ictx.Consume(arcade.BtnLeft):
		switch {
		case v.state == StateOnVariant:
			v.switchVariant(-1)
		case horizontalLayout && v.state == StateOnQuickJoin:
			v.state = StateOnJoin
		case v.state == StateOnJoin:
			if !blankCustom {
				v.state = StateOnHost
			}
		case v.state == StateHome:
			if !blankCustom {
				v.state = StateOnHost
			} else {
				v.state = StateOnJoin
			}
		}
	case v.ictx.Consume(arcade.BtnRight):
		switch {
		case v.state == StateOnVariant:
			v.switchVariant(1)
		case v.state == StateOnHost:
			v.state = StateOnJoin
		case v.state == StateOnJoin && horizontalLayout && v.quickText[0] != "":
			v.state = StateOnQuickJoin
		case v.state == StateHome:
			if !blankCustom {
				v.state = StateOnHost
			} else {
				v.state = StateOnJoin
			}
		}
	case v.ictx.Consume(arcade.BtnUp):
		switch {
		case (horizontalLayout || v.state != StateOnQuickJoin) && (!verticalLayout || v.state != StateOnJoin) && v.Match.Init.CachedVariant != nil:
			v.state = StateOnVariant
		case verticalLayout && v.state == StateOnJoin:
			if !blankCustom {
				v.state = StateOnHost
			}
		case verticalLayout && v.state == StateOnQuickJoin:
			if !isSingleplayer {
				v.state = StateOnJoin
			} else {
				v.state = StateOnHost
			}
		case !horizontalLayout && v.state == StateOnQuickJoin:
			if !blankCustom {
				v.state = StateOnHost
			} else {
				v.state = StateOnJoin
			}
		case v.state == StateHome:
			if !blankCustom {
				v.state = StateOnHost
			} else {
				v.state = StateOnJoin
			}
		}
	case v.ictx.Consume(arcade.BtnDown):
		switch {
		case v.state == StateOnVariant:
			v.state = StateOnHost
		case verticalLayout && v.state == StateOnHost && !isSingleplayer:
			v.state = StateOnJoin
		case !horizontalLayout && v.quickText[0] != "":
			v.state = StateOnQuickJoin
		case v.state == StateHome:
			if !blankCustom {
				v.state = StateOnHost
			} else {
				v.state = StateOnJoin
			}
		}
	}

	if v.state != StateHome {
		select {
		case <-v.modeLoad:
			if v.ictx.Consume(arcade.BtnConfirm) || (lastClick && !click && !v.ictx.IsMouseDrag(10)) {
				s := v.state
				v.state = StateWait

				go func() {
					_ = audio.Inside2.Preload()
					_ = audio.BattleStart0.Preload()
				}()

				switch s {
				case StateOnVariant:
					if lastClick && !click && !v.ictx.IsMouseDrag(10) && curX < 0.5 {
						v.switchVariant(-1)
					} else {
						v.switchVariant(1)
					}
				case StateOnHost:
					if variant := v.Match.Init.CachedVariant; variant != nil && variant.NPC != "" {
						v.Match.Perspective = 2
						v.Match.UnknownDeck = 1

						go v.initSinglePlayer(ctx)
					} else {
						v.Match.Perspective = 1
						v.Match.UnknownDeck = 2

						go v.initPlayer(ctx, "1")
					}
				case StateOnJoin:
					v.Match.Perspective = 2
					v.Match.UnknownDeck = 1

					go v.initPlayer(ctx, "2")
				case StateOnQuickJoin:
					if v.Match.Init.Mode == match.ModeCustom {
						v.state = StateInCardEditor

						v.onModeChange()

						var ce CardEditor

						if err := ce.SetCards(v.Match.Init.Custom); err != nil {
							panic(err)
						}

						if err := Loop(ctx, &ce); err != nil {
							panic(err)
						}

						v.Match.Init.Custom = ce.EncodeCards()

						v.state = StateOnQuickJoin
					} else {
						go v.initPlayer(ctx, "q")
					}
				}
			}
		default:
		}
	}

	if v.state == StateOnVariant {
		if v.ictx.Consume(arcade.BtnLeft) {
			v.switchVariant(-1)
		}

		if v.ictx.Consume(arcade.BtnRight) {
			v.switchVariant(1)
		}
	} else {
		if v.ictx.Consume(arcade.BtnLeft) && (v.Match.Init.Mode != match.ModeCustom || v.Match.Init.Custom != "") {
			v.state = StateOnHost
		}

		if v.ictx.Consume(arcade.BtnRight) {
			v.state = StateOnJoin
		}
	}

	if v.ictx.Consume(arcade.BtnDown) {
		switch {
		case v.state == StateOnVariant:
			v.state = StateOnHost
		case v.allowQuickJoin || v.Match.Init.Mode == match.ModeCustom:
			v.state = StateOnQuickJoin
		case v.state != StateOnJoin:
			v.state = StateOnHost
		}
	}

	if v.ictx.Consume(arcade.BtnUp) {
		switch {
		case v.state != StateOnQuickJoin && v.Match.Init.CachedVariant != nil:
			v.state = StateOnVariant
		case v.state != StateOnJoin:
			v.state = StateOnHost
		}
	}
}

func (v *Visual) switchVariant(dir int) {
	variants := v.Match.Set.Mode.GetAll(card.FieldVariant)

	switch variant := int(v.Match.Init.Variant) + dir; {
	case variant >= len(variants):
		v.Match.Init.Variant = 0
	case variant < 0:
		v.Match.Init.Variant = uint64(len(variants) - 1)
	default:
		v.Match.Init.Variant = uint64(variant)
	}

	v.Match.Init.CachedVariant = variants[v.Match.Init.Variant].(*card.Variant)
	v.Match.Set.Variant = int(v.Match.Init.Variant)

	v.onModeChange()
	v.reloadDefaultStage()

	audio.Confirm1.PlaySound(0, 0, 0)

	v.state = StateOnVariant
}

func (v *Visual) updateConnecting(ctx context.Context) {
	cs, ls := v.gc.State()

	if cs != matchnet.StateNone {
		audio.Inside2.PlayMusic(0, false)
	}

	switch code, reason := v.gc.SignalClosed(); code {
	case 0:
		switch cs {
		case matchnet.StateNone:
			// do nothing
		case matchnet.StateHandshakeWait:
			v.message = "Waiting for connection..."
		case matchnet.StateHandshakeClientWait:
			v.message = "Sent connection offer..."
		case matchnet.StateHandshakeHostWait:
			v.message = "Accepted connection offer..."
		case matchnet.StateRTCConnecting:
			v.message = "Connecting to opponent..."
		case matchnet.StateRTCConnected, matchnet.StateUsingRelay:
			switch ls {
			case matchnet.StateUninitialized, matchnet.StateSecureInitializing:
				v.message = "Negotiating game session..."
			case matchnet.StateSecureReady:
				if v.audience == nil {
					v.audience = room.NewAudience(v.gc.SCG.Seed(matchnet.RNGShared), v.Match.Rematches)
					v.stage.AddAudience(v.audience...)
				}

				v.message = "Waiting for card data..."
			case matchnet.StateMatchReady:
				if v.audience == nil {
					v.audience = room.NewAudience(v.gc.SCG.Seed(matchnet.RNGShared), v.Match.Rematches)
					v.stage.AddAudience(v.audience...)
				}

				v.Match.IndexSet()

				go v.preloadPortraits(ctx)
				go v.preloadMusic(ctx)

				v.message = ""
				v.mine = room.NewMine(ctx, func(c *room.Character) {
					p := room.NewPlayer(c)
					v.player[v.Match.Perspective-1] = p
					v.stage.AddPlayer(p, int(v.Match.Perspective))
					v.Match.Cosmetic[v.Match.Perspective-1].CharacterName = c.Name

					if err := v.gc.SendCosmeticData(v.Match.Cosmetic[v.Match.Perspective-1]); err != nil {
						log.Println("match: failed to send cosmetic data:", err)
					}

					v.animationTimer = 1.5 * 60
				})
				v.state = StateCharacterSelect
			default:
				log.Println("match: unhandled logical state:", cs, ls)
			}
		case matchnet.StateRTCDisconnected:
			v.message = "Lost connection to opponent..."
		case matchnet.StateRTCFailed, matchnet.StateRelayFailed:
			v.message = "Failed to connect to opponent."
		case matchnet.StateOfferedRelay, matchnet.StateOpponentWantsRelay:
			v.message = "Falling back to relayed connection..."
		default:
			log.Println("match: unhandled connection state:", cs, ls)
		}
	case internal.MMServerError:
		v.message = "Matchmaking server error."
		v.smallMessage = reason
	case internal.MMClientError:
		v.message = "Matchmaking client error."
		v.smallMessage = reason
	case internal.MMConnectionError:
		v.message = "Matchmaking connection error."
		v.smallMessage = reason
	case internal.MMTimeout:
		v.message = "Matchmaking connection timed out."
		v.smallMessage = reason
	case internal.MMNotFound:
		v.message = "Match not found."
		v.smallMessage = ""
	default:
		v.message = "Unexpected matchmaking connection status " + code.String()
		v.smallMessage = reason
	}
}

func (v *Visual) initPlayer(ctx context.Context, playerNumber string) {
	if playerNumber == "q" {
		v.message = "Enrolling in quick join..."
	} else {
		v.message = "Contacting matchmaking server..."
	}

	var err error

	v.sc, err = matchnet.NewSignalConn(ctx)
	if err != nil {
		panic(err)
	}

	err = v.sc.InitPlayer(ctx, playerNumber, v.Match.Init.Suffix())
	if err != nil {
		panic(err)
	}

	switch playerNumber {
	case "1":
		sid, err := v.sc.SessionID(ctx)
		if err != nil {
			panic(err)
		}

		err = v.gc.UseSignal(ctx, v.sc, true, &v.Match.Init.Mode, v.Match.Set)
		if err != nil {
			panic(err)
		}

		ctx2, cancel := context.WithCancel(ctx)

		go displayCopyableText(ctx2, "Send this match code to your opponent.", sid)

		v.message = ""

		go func() {
			_ = v.gc.ConnStateExcept(matchnet.StateNone)

			cancel()
		}()
	case "2":
		err = v.gc.UseSignal(ctx, v.sc, false, &v.Match.Init.Mode, v.Match.Set)
		if err != nil {
			panic(err)
		}

		ctx2, cancel := context.WithCancel(ctx)

		go displayPastableText(ctx2, "Enter match code from opponent:", func(t string) {
			if len(t)-strings.Count(t, " ") == 24 {
				v.message = "Searching for match..."

				if err := v.sc.SetSessionID(ctx, t); err != nil {
					log.Println("ERROR: in SetSessionID:", err)
				}

				cancel()
			}
		})

		v.message = ""
	case "q":
		playerNumber, err := v.sc.SessionID(ctx)
		if err != nil {
			panic(err)
		}

		var isHost bool

		switch playerNumber {
		case "1":
			v.Match.Perspective = 1
			v.Match.UnknownDeck = 2
			isHost = true
		case "2":
			v.Match.Perspective = 2
			v.Match.UnknownDeck = 1
			isHost = false
		default:
			panic(xerrors.Errorf("match: unexpected player number: %q", playerNumber))
		}

		v.message = "Waiting for opponent..."

		err = v.gc.UseSignal(ctx, v.sc, isHost, &v.Match.Init.Mode, v.Match.Set)
		if err != nil {
			panic(err)
		}
	}

	v.stage.SetPlayer(int(v.Match.Perspective))
	v.state = StateConnecting
}

func (v *Visual) initSinglePlayer(ctx context.Context) {
	v.message = "Initializing..."

	n, err := npc.Get(v.Match.Init.CachedVariant.NPC)
	if err != nil {
		v.message = err.Error()

		return
	}

	sc1, sc2 := matchnet.NewFakeSignalConn()
	v.sc = sc2

	npcInit := *v.Match.Init
	fakeSessionID := make(chan string, 1)

	go func() {
		gc, err := matchnet.NewGameConn(ctx)
		if err != nil {
			log.Panicln("ERROR: creating game conn:", err)
		}

		m := &match.Match{
			Init:        &npcInit,
			Perspective: 1,
		}

		m.Set, err = npcInit.Cards(ctx)
		if err != nil {
			log.Panicln("ERROR: getting cards:", err)
		}

		if err = gc.UseSignal(ctx, sc1, true, &npcInit.Mode, m.Set); err != nil {
			log.Panicln("ERROR: connecting:", err)
		}

		if err = sc1.InitPlayer(ctx, "1", ""); err != nil {
			log.Panicln("ERROR: init player 1:", err)
		}

		sid, err := sc1.SessionID(ctx)
		if err != nil {
			log.Panicln("ERROR: getting session ID:", err)
		}

		fakeSessionID <- sid

		_, err = minimal.PlayMatch(ctx, m, gc, n)
		if err != nil {
			log.Println("ERROR: from NPC:", err)
		}
	}()

	err = v.gc.UseSignal(ctx, v.sc, false, &v.Match.Init.Mode, v.Match.Set)
	if err != nil {
		panic(err)
	}

	if err = sc2.InitPlayer(ctx, "2", ""); err != nil {
		log.Panicln("ERROR: init player 2:", err)
	}

	if err := sc2.SetSessionID(ctx, <-fakeSessionID); err != nil {
		log.Panicln("ERROR: set session id:", err)
	}

	v.stage.SetPlayer(int(v.Match.Perspective))
	v.state = StateConnecting
}

func (v *Visual) renderHome() {
	touchcontroller.SkipFrame = true

	if v.stage != nil {
		v.stage.Render()
	}

	w, _ := gfx.Size()

	select {
	case <-v.modeLoad:
		title := "Spy Cards Online"

		if v.Match.Init.External != nil {
			title = v.Match.Init.External.Name
		} else if v.Match.Init.Mode == match.ModeCustom {
			title = "Spy Cards Online (Custom)"
		}

		(&card.RichDescription{
			Text:  title,
			Color: sprites.White,
		}).Typeset(sprites.FontD3Streetism, v.modeX, v.modeY, 0, 2, 2, float32(w)/64-1, 0, nil, true, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
			sprites.DrawTextShadow(v.tb, sprites.FontD3Streetism, text, x, y, z, sx, sy, tint)
		})

		if variant := v.Match.Init.CachedVariant; variant != nil {
			c := sprites.White
			if v.state == StateOnVariant {
				c = sprites.Yellow

				var totalWidth float32

				for _, letter := range variant.Title {
					totalWidth += sprites.TextAdvance(sprites.FontD3Streetism, letter, 1.25)
				}

				if totalWidth > float32(w)/64-2 {
					totalWidth = float32(w)/64 - 2
				}

				totalWidth = (totalWidth + 0.75) / 2

				v.sb.Append(sprites.CircleArrow, v.varX-totalWidth, v.varY+0.25, 0, 0.5, 0.5, sprites.White, 0, 0, math.Pi/2, 0)
				v.sb.Append(sprites.CircleArrow, v.varX+totalWidth, v.varY+0.25, 0, 0.5, 0.5, sprites.White, 0, 0, -math.Pi/2, 0)
			}

			(&card.RichDescription{
				Text:  variant.Title,
				Color: c,
			}).Typeset(sprites.FontD3Streetism, v.varX, v.varY, 0, 1.25, 1.25, float32(w)/64-2, 0, nil, true, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
				sprites.DrawTextShadow(v.tb, sprites.FontD3Streetism, text, x, y, z, sx, sy, tint)
			})
		}

		c := sprites.White
		if v.state == StateOnHost {
			c = sprites.Yellow
		}

		hostOffY := float32(-0.5)
		if variant := v.Match.Init.CachedVariant; variant != nil && variant.NPC != "" {
			hostOffY = -0.75
		}

		if v.Match.Init.Mode == match.ModeCustom && v.Match.Init.Custom == "" {
			c = sprites.Gray
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.hostText[0], v.hostX, v.hostY, 0, 2.5, 2.5, c, true)
		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.hostText[1], v.hostX, v.hostY+hostOffY, 0, 0.5, 0.5, sprites.White, true)

		if variant := v.Match.Init.CachedVariant; variant == nil || variant.NPC == "" {
			c = sprites.White
			if v.state == StateOnJoin {
				c = sprites.Yellow
			}

			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Join", v.joinX, v.joinY, 0, 2.5, 2.5, c, true)
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Use a match code to connect.", v.joinX, v.joinY-0.5, 0, 0.5, 0.5, sprites.White, true)
		}

		c = sprites.White
		if v.state == StateOnQuickJoin {
			c = sprites.Yellow
		}

		sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.quickText[0], v.quickX, v.quickY, 0, 1.5, 1.5, c, true)

		if v.quickText[1] == "" {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.quickText[2], v.quickX, v.quickY-0.5, 0, 0.5, 0.5, sprites.White, true)
		} else {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.quickText[1], v.quickX, v.quickY-0.9, 0, 1.5, 1.5, c, true)
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, v.quickText[2], v.quickX, v.quickY-1.4, 0, 0.5, 0.5, sprites.White, true)
		}
	default:
		if v.message == "" {
			sprites.DrawTextCentered(v.tb, sprites.FontBubblegumSans, "Fetching game mode data...", 0, 0, 0, 1.5, 1.5, sprites.White, true)
		}
	}
}
