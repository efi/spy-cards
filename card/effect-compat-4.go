package card

import (
	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

const (
	legacyPriorityTP         = 0
	legacyPriorityFlavorText = 0

	legacyPriorityCondApply     = 5
	legacyPriorityCondLimit     = 10
	legacyPriorityCondHP        = 15
	legacyPriorityReplaceSummon = 20
	legacyPrioritySummon        = 25
	legacyPriorityCondCoin      = 30
	legacyPriorityCondCard      = 35
	legacyPriorityHeal          = 40

	legacyPriorityStat             = 65
	legacyPriorityOpponentCondCard = 70
	legacyPriorityEachCondCard     = 75
	legacyPriorityEmpower          = 80
	legacyPriorityCondStat         = 85
	legacyPriorityNumb             = 90
	legacyPriorityOpponentStat     = 95
	legacyPriorityLateCondStat     = 100

	legacyPriorityCondWinner    = 225
	legacyPriorityLateCondHP    = 230
	legacyPriorityLateCondApply = 235

	legacyPriorityCondOnNumb = 0
)

func (e *EffectDef) unmarshalV4(r *format.Reader, formatVersion uint64) error {
	const (
		legacyFlagNegate   = 1 << 0
		legacyFlagOpponent = 1 << 1
		legacyFlagEach     = 1 << 2
		legacyFlagLate     = 1 << 3
		legacyFlagGeneric  = 1 << 4
		legacyFlagDefense  = 1 << 5
	)

	var legacyType, legacyFlags uint8

	r.Read(&legacyType)
	r.Read(&legacyFlags)

	e.Priority = 0
	e.Flags = 0

	e.Text = ""
	e.Amount = 0
	e.Amount2 = 0
	e.Filter = nil

	e.Result = nil
	e.Result2 = nil

	shiftFilter := func(e *EffectDef) {
		if legacyFlags&legacyFlagGeneric == 0 {
			e.Filter = Filter{{
				Type:   FilterSingleCard,
				CardID: ID(r.UVarInt()),
			}}

			return
		}

		rankTribe := r.Byte()

		e.Filter = nil

		if rankTribe>>4 != 7 {
			if rankTribe>>4 == 4 {
				e.Filter = append(e.Filter, FilterComponent{
					Type: FilterRank,
					Rank: Attacker,
				}, FilterComponent{
					Type: FilterRank,
					Rank: Effect,
				})
			} else {
				e.Filter = append(e.Filter, FilterComponent{
					Type: FilterRank,
					Rank: Rank(rankTribe >> 4),
				})
			}
		}

		if t := Tribe(rankTribe & 15); t == TribeCustom {
			e.Filter = append(e.Filter, FilterComponent{
				Type:        FilterTribe,
				Tribe:       TribeCustom,
				CustomTribe: r.String(),
			})
		} else if t != 15 {
			e.Filter = append(e.Filter, FilterComponent{
				Type:  FilterTribe,
				Tribe: t,
			})
		}
	}
	shiftAmount := func(e *EffectDef, infFlag EffectFlag) {
		if amount := r.Byte(); amount == 255 {
			e.Amount = 1
			e.Flags |= infFlag
		} else {
			e.Amount = int64(amount)
		}

		if legacyFlags&legacyFlagNegate != 0 {
			e.Amount = -e.Amount
		}
	}
	shiftCount := func(e *EffectDef) {
		e.Amount = int64(r.Byte()) + 1
	}

	switch legacyType {
	case 0: // FlavorText
		e.Type = FlavorText
		e.Priority = legacyPriorityFlavorText
		e.Text = r.String()

		if legacyFlags&legacyFlagNegate != 0 {
			e.Flags |= FlagFlavorTextHideRemaining
		}
	case 1: // EffectStat
		e.Type = EffectStat
		e.Priority = legacyPriorityStat

		shiftAmount(e, FlagStatInfinity)

		if legacyFlags&legacyFlagDefense != 0 {
			e.Flags |= FlagStatDEF
		}

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Type = EffectRawStat
			e.Priority = legacyPriorityOpponentStat
			e.Flags |= FlagStatOpponent
		}
	case 2: // EffectEmpower
		e.Type = EffectEmpower
		e.Priority = legacyPriorityEmpower

		shiftAmount(e, FlagStatInfinity)
		shiftFilter(e)

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Priority = legacyPriorityEmpower
			e.Flags |= FlagStatOpponent
		}

		if legacyFlags&legacyFlagDefense != 0 {
			e.Flags |= FlagStatDEF
		}
	case 3: // EffectSummon
		e.Type = EffectSummon
		e.Priority = legacyPrioritySummon

		shiftCount(e)
		shiftFilter(e)

		if legacyFlags&legacyFlagDefense != 0 {
			e.Flags |= FlagSummonInvisible
		}

		if legacyFlags&legacyFlagNegate != 0 {
			e.Priority = legacyPriorityReplaceSummon
			e.Flags |= FlagSummonReplace
		}

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Flags |= FlagSummonOpponent
		}
	case 4: // EffectHeal
		e.Type = EffectHeal
		e.Priority = legacyPriorityHeal

		shiftAmount(e, FlagHealInfinity)

		if legacyFlags&legacyFlagEach != 0 {
			e.Type = EffectMultiplyHealing

			if e.Flags&FlagHealInfinity != 0 {
				e.Flags &^= FlagHealInfinity
				e.Amount *= 255
			}

			switch {
			case legacyFlags&legacyFlagGeneric != 0:
				e.Flags |= FlagMultiplyHealingTypeAll
			case legacyFlags&legacyFlagDefense != 0:
				e.Flags |= FlagMultiplyHealingTypePositive
			default:
				e.Flags |= FlagMultiplyHealingTypeNegative
			}

			if legacyFlags&legacyFlagOpponent != 0 {
				e.Flags |= FlagMultiplyHealingOpponent
			}
		} else if legacyFlags&legacyFlagOpponent != 0 {
			e.Flags |= FlagHealOpponent
		}
	case 5: // EffectTP
		e.Type = EffectTP
		e.Priority = legacyPriorityTP

		shiftAmount(e, FlagTPInfinity)
	case 6: // EffectNumb
		e.Type = EffectNumb
		e.Priority = legacyPriorityNumb
		e.Flags |= FlagNumbAvoidZero

		shiftAmount(e, FlagNumbInfinity)

		if e.Amount < 0 {
			e.Amount = -e.Amount
		}

		if formatVersion >= 4 {
			shiftFilter(e)

			if legacyFlags&legacyFlagOpponent == 0 {
				e.Flags |= FlagNumbSelf
			}
		} else {
			e.Filter = Filter{{
				Type: FilterRank,
				Rank: Attacker,
			}}
		}
	case 128: // CondCard
		e.Type = CondCard
		e.Priority = legacyPriorityCondCard

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Flags |= FlagCondCardOpponent
			e.Priority = legacyPriorityOpponentCondCard
		}

		if legacyFlags&legacyFlagEach == 0 {
			shiftCount(e)

			if legacyFlags&legacyFlagNegate != 0 {
				e.Flags |= FlagCondCardTypeLessThan
			} else {
				e.Flags |= FlagCondCardTypeGreaterEqual
			}
		} else {
			e.Flags |= FlagCondCardTypeEach
			e.Amount = 1
			e.Priority = legacyPriorityEachCondCard
		}

		shiftFilter(e)

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 129: // CondLimit
		e.Type = CondLimit
		e.Priority = legacyPriorityCondLimit

		shiftCount(e)

		if legacyFlags&legacyFlagNegate != 0 {
			e.Flags |= FlagCondLimitGreaterThan
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 130: // CondWinner
		e.Type = CondWinner
		e.Priority = legacyPriorityCondWinner

		switch legacyFlags & (legacyFlagNegate | legacyFlagOpponent) {
		case 0:
			e.Flags |= FlagCondWinnerTypeWinner
		case legacyFlagOpponent:
			e.Flags |= FlagCondWinnerTypeLoser
		case legacyFlagNegate:
			e.Flags |= FlagCondWinnerTypeTie
		case legacyFlagNegate | legacyFlagOpponent:
			e.Flags |= FlagCondWinnerTypeNotTie
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 131: // CondApply
		e.Type = CondApply
		e.Priority = legacyPriorityCondApply

		if legacyFlags&legacyFlagNegate != 0 {
			e.Flags |= FlagCondApplyOriginalText
		}

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Flags |= FlagCondApplyOpponent
		}

		if legacyFlags&legacyFlagLate != 0 {
			e.Flags |= FlagCondApplyNextRound
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}

		if e.Flags&(FlagCondApplyOriginalText|FlagCondApplyOpponent|FlagCondApplyNextRound) == FlagCondApplyOriginalText|FlagCondApplyNextRound && e.Result.Type == EffectSummon && e.Result.Flags&(FlagSummonOpponent|FlagSummonReplace) == FlagSummonReplace && e.Result.Amount == 1 && e.Result.Filter.IsSingleCard() {
			e.Result.Priority = 0
		}
	case 132: // CondCoin
		e.Type = CondCoin
		e.Priority = legacyPriorityCondCoin

		shiftCount(e)

		if legacyFlags&legacyFlagNegate != 0 {
			e.Flags |= FlagCondCoinNegative
		}

		if legacyFlags&legacyFlagDefense != 0 {
			e.Flags |= FlagCondCoinStat
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}

		if legacyFlags&legacyFlagGeneric != 0 {
			e.Flags |= FlagCondCoinTails

			e.Result2 = &EffectDef{}
			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return err
			}
		}
	case 133: // CondHP
		e.Type = CondStat
		e.Priority = legacyPriorityCondHP
		e.Flags |= FlagCondStatTypeHP

		if legacyFlags&legacyFlagLate != 0 {
			e.Priority = legacyPriorityLateCondHP
		}

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Flags |= FlagCondStatOpponent
		}

		if legacyFlags&legacyFlagNegate != 0 {
			e.Flags |= FlagCondStatLessThan
		}

		shiftCount(e)
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 134: // CondStat
		e.Type = CondStat
		e.Priority = legacyPriorityCondStat

		shiftCount(e)

		if legacyFlags&legacyFlagLate != 0 {
			e.Priority = legacyPriorityLateCondStat
		}

		if legacyFlags&legacyFlagDefense != 0 {
			e.Flags |= FlagCondStatTypeDEF
		} else {
			e.Flags |= FlagCondStatTypeATK
		}

		if legacyFlags&legacyFlagOpponent != 0 {
			e.Flags |= FlagCondStatOpponent
		}

		if legacyFlags&legacyFlagNegate != 0 {
			e.Flags |= FlagCondStatLessThan
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 135: // CondPriority
		var priority uint8

		switch r.Byte() {
		case 0: // FlavorText
			priority = legacyPriorityFlavorText
		case 1: // EffectStat
			if legacyFlags&legacyFlagOpponent != 0 {
				priority = legacyPriorityOpponentStat
			} else {
				priority = legacyPriorityStat
			}
		case 2: // EffectEmpower
			priority = legacyPriorityEmpower
		case 3: // EffectSummon
			if legacyFlags&legacyFlagNegate != 0 {
				priority = legacyPriorityReplaceSummon
			} else {
				priority = legacyPrioritySummon
			}
		case 4: // EffectHeal
			priority = legacyPriorityHeal
		case 5: // EffectTP
			priority = legacyPriorityTP
		case 6: // EffectNumb
			priority = legacyPriorityNumb
		case 128: // CondCard
			switch legacyFlags & (legacyFlagOpponent | legacyFlagEach) {
			case 0:
				priority = legacyPriorityCondCard
			case legacyFlagOpponent:
				priority = legacyPriorityOpponentCondCard
			default:
				priority = legacyPriorityEachCondCard
			}
		case 129: // CondLimit
			priority = legacyPriorityCondLimit
		case 130: // CondWinner
			priority = legacyPriorityCondWinner
		case 131: // CondApply
			if legacyFlags&legacyFlagLate != 0 {
				priority = legacyPriorityLateCondApply
			} else {
				priority = legacyPriorityCondApply
			}
		case 132: // CondCoin
			priority = legacyPriorityCondCoin
		case 133: // CondHP
			if legacyFlags&legacyFlagLate != 0 {
				priority = legacyPriorityLateCondHP
			} else {
				priority = legacyPriorityCondHP
			}
		case 134: // CondStat
			if legacyFlags&legacyFlagLate != 0 {
				priority = legacyPriorityLateCondStat
			} else {
				priority = legacyPriorityCondStat
			}
		case 136: // CondOnNumb
			priority = legacyPriorityCondOnNumb
		}

		if err := e.Unmarshal(r, formatVersion); err != nil {
			return err
		}

		e.Priority = priority
	case 136: // CondOnNumb
		e.Type = CondOnNumb
		e.Priority = legacyPriorityCondOnNumb

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	default:
		return xerrors.Errorf("card: unhandled effect type: %d (%v)", e.Type, e.Type)
	}

	return nil
}
