module SpyCards.Native {
	if (!WebAssembly.instantiateStreaming) { // polyfill
		WebAssembly.instantiateStreaming = async (resp, importObject) => {
			const source = await (await resp).arrayBuffer();
			return await WebAssembly.instantiate(source, importObject);
		};
	}

	let go = new (window as any).Go();
	let inst: WebAssembly.Instance;
	let mod: WebAssembly.Module;
	let ready = WebAssembly.instantiateStreaming(fetch("spy-cards.wasm"), go.importObject).then(function (result) {
		inst = result.instance;
		mod = result.module;
	});

	export interface RoomRPC {
		exit(): void;
		newStage(cb: (id: number) => void): void;
		newMine(cb: (id: number) => void): void;
		setRoom(id: number): void;
		onMineSelected(cb: (name: string) => void): void;
		createAudience(seed: Uint8Array, rematches: number): number[];
		setPlayer(room: number, name: string, i: number): number;
		setCurrentPlayer(room: number, i: number): void;
		startCheer(id: number, t: number): void;
		becomeAngry(id: number, t: number): void;
		isAudienceBack(id: number): boolean;
		setCharacter(id: number, name: string): void;
		addAudience(room: number, id: number): void;
		createAudienceMember(type: number, x: number, y: number, back: boolean, flip: boolean, color: number, excitement: number): number;
	}

	export var roomRPCInit: (rpc: RoomRPC) => void;
	const roomRPCPromise = new Promise<RoomRPC>((resolve) => roomRPCInit = resolve);
	let roomRPCProcess: Promise<void>;

	export async function roomRPC(): Promise<RoomRPC> {
		if (new URLSearchParams(location.search).has("no3d") || loadSettings().disable3D) {
			return null;
		}

		if (!roomRPCProcess) {
			roomRPCProcess = run("-run=RoomRPC");
		}

		return roomRPCPromise;
	}

	export async function run(...argv: string[]) {
		ready = ready.then(async () => {
			go.argv = ["js"].concat(argv);
			await go.run(inst);
			inst = await WebAssembly.instantiate(mod, go.importObject);
		});

		return ready;
	}
}
