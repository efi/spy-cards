package card

import (
	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

func (e *EffectDef) unmarshalV1(r *format.Reader, formatVersion uint64) error {
	e.Flags = 0
	e.Priority = 0

	e.Text = ""
	e.Amount = 0
	e.Amount2 = 0
	e.Filter = nil

	e.Result = nil
	e.Result2 = nil

	shiftAmount := func(e *EffectDef, infFlag EffectFlag) {
		switch n := r.Byte(); {
		case n == 0:
			e.Flags |= infFlag
			e.Amount = 1
		case n >= 128:
			e.Amount = int64(int8(n))
		default:
			e.Amount = int64(n)
		}
	}
	shiftCard := func() Filter {
		return Filter{{
			Type:   FilterSingleCard,
			CardID: ID(r.Byte()),
		}}
	}
	shiftTribe := func() Filter {
		var t Tribe

		r.Read(&t)

		if (t & 15) != TribeCustom {
			return Filter{{
				Type:  FilterTribe,
				Tribe: t,
			}}
		}

		return Filter{{
			Type:        FilterTribe,
			Tribe:       t,
			CustomTribe: r.String1(),
		}}
	}

	switch legacyType := r.Byte(); legacyType {
	case 0: // ATK (static)
		e.Type = EffectStat
		e.Priority = legacyPriorityStat

		shiftAmount(e, FlagStatInfinity)
	case 1: // DEF (static)
		e.Type = EffectStat
		e.Flags |= FlagStatDEF
		e.Priority = legacyPriorityStat

		shiftAmount(e, FlagStatInfinity)
	case 2: // ATK (static, once per turn)
		e.Type = CondLimit
		e.Priority = legacyPriorityCondLimit
		e.Amount = 1

		e.Result = &EffectDef{}
		e.Result.Type = EffectStat
		e.Result.Priority = legacyPriorityStat
		shiftAmount(e.Result, FlagStatInfinity)
	case 3: // ATK+n
		e.Type = EffectStat
		e.Priority = legacyPriorityStat

		shiftAmount(e, FlagStatInfinity)
	case 4: // DEF+n
		e.Type = EffectStat
		e.Flags |= FlagStatDEF
		e.Priority = legacyPriorityStat

		shiftAmount(e, FlagStatInfinity)
	case 5: // Empower (card)
		e.Type = EffectEmpower
		e.Priority = legacyPriorityEmpower

		e.Filter = shiftCard()
		shiftAmount(e, FlagStatInfinity)
	case 6: // Empower (tribe)
		e.Type = EffectEmpower
		e.Priority = legacyPriorityEmpower

		e.Filter = shiftTribe()
		shiftAmount(e, FlagStatInfinity)
	case 7: // Heal
		e.Type = EffectHeal
		e.Priority = legacyPriorityHeal

		shiftAmount(e, FlagHealInfinity)
	case 8: // Lifesteal
		e.Type = CondWinner
		e.Priority = legacyPriorityCondWinner

		e.Result = &EffectDef{}
		e.Result.Type = EffectHeal
		e.Result.Priority = legacyPriorityHeal
		shiftAmount(e.Result, FlagHealInfinity)
	case 9: // Numb
		e.Type = EffectNumb
		e.Priority = legacyPriorityNumb

		shiftAmount(e, FlagNumbInfinity)
		e.Filter = Filter{{
			Type: FilterRank,
			Rank: Attacker,
		}}
	case 10: // Pierce
		e.Type = EffectStat
		e.Priority = legacyPriorityOpponentStat
		e.Flags |= FlagStatOpponent
		e.Flags |= FlagStatDEF

		shiftAmount(e, FlagStatInfinity)
		e.Amount = -e.Amount
	case 11: // Summon
		e.Type = EffectSummon
		e.Priority = legacyPrioritySummon
		e.Amount = 1
		e.Filter = shiftCard()
	case 12: // Summon Rank
		e.Type = EffectSummon
		e.Priority = legacyPriorityReplaceSummon
		e.Flags |= FlagSummonReplace
		e.Amount = 1

		if formatVersion == 0 {
			e.Filter = Filter{{
				Type: FilterRank,
				Rank: MiniBoss,
			}}
		} else {
			e.Filter = Filter{{
				Type: FilterRank,
				Rank: Rank(r.Byte()),
			}}
		}
	case 13: // Summon Tribe
		e.Type = EffectSummon
		e.Priority = legacyPrioritySummon

		e.Filter = shiftTribe()
		if e.Filter[0].Tribe>>4 == 0 {
			e.Filter = append(e.Filter, FilterComponent{
				Type: FilterRank,
				Rank: Enemy,
			})
		} else {
			e.Filter = append(e.Filter, FilterComponent{
				Type: FilterRank,
				Rank: Rank(e.Filter[0].Tribe>>4) - 1,
			})
			e.Filter[0].Tribe &= 15
		}

		e.Amount = int64(r.Byte())

	case 14: // Unity
		e.Type = CondLimit
		e.Priority = legacyPriorityCondLimit
		e.Amount = 1

		e.Result = &EffectDef{}
		e.Result.Type = EffectEmpower
		e.Result.Priority = legacyPriorityEmpower
		e.Result.Filter = shiftTribe()
		shiftAmount(e.Result, FlagStatInfinity)
	case 15: // TP
		e.Type = EffectTP
		e.Priority = legacyPriorityTP

		shiftAmount(e, FlagTPInfinity)
	case 16: // Summon as Opponent
		e.Type = EffectSummon
		e.Priority = legacyPrioritySummon
		e.Flags |= FlagSummonOpponent
		e.Amount = 1

		e.Filter = shiftCard()
	case 17: // Multiply Healing
		e.Type = EffectMultiplyHealing
		e.Priority = legacyPriorityHeal

		shiftAmount(e, FlagHealInfinity)
		e.Amount++
	case 18: // Flavor Text
		e.Type = FlavorText
		e.Priority = legacyPriorityFlavorText

		if r.Byte()&1 == 1 {
			e.Flags |= FlagFlavorTextHideRemaining
		}

		e.Text = r.String1()
	case 128: // Coin
		e.Type = CondCoin
		e.Priority = legacyPriorityCondCoin

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCoinNegative
			}
		}

		count := r.Byte()
		if count&0x80 != 0 {
			e.Flags |= FlagCondCoinTails
		}

		e.Amount = int64(count&0x7f) + 1

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}

		if e.Flags&FlagCondCoinTails != 0 {
			e.Result2 = &EffectDef{}
			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return err
			}

			if e.Result.Type == EffectStat &&
				e.Result.Flags&FlagStatDEF == 0 &&
				e.Result2.Type == EffectStat &&
				e.Result2.Flags&FlagStatDEF != 0 {
				e.Flags |= FlagCondCoinStat
			}
		}

	case 129: // If Card
		e.Type = CondCard
		e.Priority = legacyPriorityCondCard

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCardTypeLessThan
			}
		}

		e.Filter = shiftCard()

		if formatVersion == 1 {
			e.Amount = int64(r.Byte())
		} else {
			e.Amount = 1
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 130: // Per Card
		e.Type = CondCard
		e.Priority = legacyPriorityEachCondCard

		if formatVersion == 1 {
			r.Byte()
		}

		e.Flags |= FlagCondCardTypeEach
		e.Amount = 1

		e.Filter = shiftCard()

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 131: // If Tribe
		e.Type = CondCard
		e.Priority = legacyPriorityCondCard

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCardTypeLessThan
			}
		}

		e.Filter = shiftTribe()
		if formatVersion == 1 {
			e.Amount = int64(r.Byte())
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 132: // VS Tribe
		e.Type = CondCard
		e.Priority = legacyPriorityOpponentCondCard
		e.Flags |= FlagCondCardOpponent

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCardTypeLessThan
			}
		}

		e.Filter = shiftTribe()
		if formatVersion == 1 {
			e.Amount = int64(r.Byte())
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 133: // If Stat
		e.Type = CondStat
		e.Priority = legacyPriorityCondStat

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondStatLessThan
			}
		}

		e.Amount = int64(r.Byte())
		if e.Amount&0x80 != 0 {
			e.Flags |= FlagCondStatTypeDEF
			e.Amount &= 0x7f
		} else {
			e.Flags |= FlagCondStatTypeATK
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 134: // Setup
		e.Type = CondApply
		e.Priority = legacyPriorityLateCondApply
		e.Flags |= FlagCondApplyNextRound

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondApplyOriginalText
			}
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 135: // Limit
		e.Type = CondLimit
		e.Priority = legacyPriorityCondLimit

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondLimitGreaterThan
			}
		}

		e.Amount = int64(r.Byte())

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 136: // VS Card
		e.Type = CondCard
		e.Priority = legacyPriorityOpponentCondCard
		e.Flags |= FlagCondCardOpponent

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCardTypeLessThan
			}
		}

		e.Filter = shiftCard()
		e.Amount = int64(r.Byte())

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 137: // If Rank
		e.Type = CondCard
		e.Priority = legacyPriorityCondCard

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCardTypeLessThan
			}
		}

		e.Filter = Filter{{
			Type: FilterRank,
			Rank: Rank(r.Byte()),
		}}
		e.Amount = int64(r.Byte())

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 138: // VS Rank
		e.Type = CondCard
		e.Priority = legacyPriorityOpponentCondCard
		e.Flags |= FlagCondCardOpponent

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondCardTypeLessThan
			}
		}

		e.Filter = Filter{{
			Type: FilterRank,
			Rank: Rank(r.Byte()),
		}}
		e.Amount = int64(r.Byte())

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 139: // If Win
		e.Type = CondWinner
		e.Priority = legacyPriorityCondWinner

		if formatVersion == 1 && r.Byte()&1 == 1 {
			e.Flags |= FlagCondWinnerTypeLoser
		} else {
			e.Flags |= FlagCondWinnerTypeWinner
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 140: // If Tie
		e.Type = CondWinner
		e.Priority = legacyPriorityCondWinner

		if formatVersion == 1 && r.Byte()&1 == 1 {
			e.Flags |= FlagCondWinnerTypeNotTie
		} else {
			e.Flags |= FlagCondWinnerTypeTie
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 141: // If Stat (late)
		e.Type = CondStat
		e.Priority = legacyPriorityLateCondStat

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondStatLessThan
			}
		}

		e.Amount = int64(r.Byte())
		if e.Amount&0x80 != 0 {
			e.Flags |= FlagCondStatTypeDEF
			e.Amount &= 0x7f
		} else {
			e.Flags |= FlagCondStatTypeATK
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 142: // VS Stat
		e.Type = CondStat
		e.Priority = legacyPriorityCondStat
		e.Flags |= FlagCondStatOpponent

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondStatLessThan
			}
		}

		e.Amount = int64(r.Byte())
		if e.Amount&0x80 != 0 {
			e.Flags |= FlagCondStatTypeDEF
			e.Amount &= 0x7f
		} else {
			e.Flags |= FlagCondStatTypeATK
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 143: // VS Stat (late)
		e.Type = CondStat
		e.Priority = legacyPriorityLateCondStat
		e.Flags |= FlagCondStatOpponent

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondStatLessThan
			}
		}

		e.Amount = int64(r.Byte())
		if e.Amount&0x80 != 0 {
			e.Flags |= FlagCondStatTypeDEF
			e.Amount &= 0x7f
		} else {
			e.Flags |= FlagCondStatTypeATK
		}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 144: // If HP
		e.Type = CondStat
		e.Priority = legacyPriorityCondHP
		e.Flags |= FlagCondStatTypeHP

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondStatLessThan
			}
		}

		e.Amount = int64(r.Byte())

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 145: // Per Tribe
		e.Type = CondCard
		e.Priority = legacyPriorityEachCondCard

		if formatVersion == 1 {
			r.Byte()
		}

		e.Amount = 1
		e.Flags |= FlagCondCardTypeEach

		e.Filter = shiftTribe()

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 146: // Per Rank
		e.Type = CondCard
		e.Priority = legacyPriorityEachCondCard
		e.Flags |= FlagCondCardTypeEach

		if formatVersion == 1 {
			r.Byte()
		}

		e.Amount = 1

		e.Filter = Filter{{
			Type: FilterRank,
			Rank: Rank(r.Byte()),
		}}

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	case 147: // VS HP
		e.Type = CondStat
		e.Priority = legacyPriorityCondHP
		e.Flags |= FlagCondStatTypeHP
		e.Flags |= FlagCondStatOpponent

		if formatVersion == 1 {
			if r.Byte()&1 == 1 {
				e.Flags |= FlagCondStatLessThan
			}
		}

		e.Amount = int64(r.Byte())

		e.Result = &EffectDef{}
		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return err
		}
	default:
		return xerrors.Errorf("card: unexpected effect type ID %d", legacyType)
	}

	return nil
}
