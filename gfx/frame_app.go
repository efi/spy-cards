// +build !js !wasm
// +build !headless

package gfx

import (
	"git.lubar.me/ben/spy-cards/audio"
	"golang.org/x/mobile/gl"
)

var (
	// FrameSync is an internal variable.
	FrameSync     = make(chan chan struct{}, 1)
	lastFrameSync chan struct{}
)

// NextFrame synchronizes a frame boundary.
func NextFrame() {
	if lastFrameSync == nil {
		lastFrameSync = <-FrameSync
	}

	GL.Flush()

	lastFrameSync <- struct{}{}
	lastFrameSync = <-FrameSync

	GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	audio.Tick()
}
