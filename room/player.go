package room

import (
	"image/color"
	"time"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// Player is a Spy Cards Online player avatar.
type Player struct {
	Character *Character

	X     float32
	Y     float32
	Z     float32
	Scale float32
	Flip  bool
	Color color.RGBA
	Angry uint64
	Anim  uint64
}

var playerAnimationOffset uint64

// NewPlayer creates a Player with a given Character.
func NewPlayer(c *Character) *Player {
	playerAnimationOffset += 23

	return &Player{
		Character: c,
		Scale:     1,
		Color:     sprites.White,
		Anim:      playerAnimationOffset % 40,
	}
}

// Update is called once per 60th of a second to update animations.
func (p *Player) Update() {
	p.Anim++

	if p.Anim/20 >= 2 {
		p.Anim = 0
	}

	if p.Angry != 0 && p.Angry != ^uint64(0) {
		p.Angry--
	}
}

// BecomeAngry makes the player use their "angry" sprite for the given amount of time.
//
// A negative duration causes the player to become angry indefinitely.
func (p *Player) BecomeAngry(d time.Duration) {
	if d < 0 {
		p.Angry = ^uint64(0)
	} else {
		p.Angry = uint64(d * 60 / time.Second)
	}
}

// Render draws the player to the screen.
func (p *Player) Render(sb *sprites.Batch, flags sprites.RenderFlag, ry, rz, blur float32) {
	if p.Character == nil {
		return
	}

	sx := p.Scale
	if p.Flip {
		sx = -sx
	}

	sprite := p.Character.Sprites[2]
	if p.Angry == 0 {
		sprite = p.Character.Sprites[p.Anim/20]
	}

	sb.Append(sprite, p.X, p.Y, p.Z, sx, p.Scale, p.Color, flags, ry, rz, blur)
}
