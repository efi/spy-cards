//go:generate stringer -type Rank -linecomment

package card

// Rank is an enumeration of card types.
type Rank uint8

// Constants for Rank.
const (
	Attacker Rank = 0
	Effect   Rank = 1
	MiniBoss Rank = 2 // Mini-Boss
	Boss     Rank = 3
	Token    Rank = 4
	Enemy    Rank = 5
	_        Rank = 6
	RankNone Rank = 7 // None
)

// Rank returns the rank of a card.
func (i ID) Rank() Rank {
	if i >= 128 {
		return Rank(i>>5) & 3
	}

	if basicData[i] != nil {
		return basicData[i].rank
	}

	return RankNone
}

// Back returns Enemy for Attacker and Effect as these ranks share a card back.
// Otherwise, Back returns r unchanged.
func (r Rank) Back() Rank {
	if r == Attacker || r == Effect {
		return Enemy
	}

	return r
}
