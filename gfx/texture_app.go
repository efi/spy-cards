// +build !js !wasm
// +build !headless

package gfx

import (
	"bytes"
	"image"
	"image/jpeg"
	"image/png"

	"golang.org/x/xerrors"
)

func newDataTexture(name string, b []byte, mime string, nearest bool) (*Texture, error) {
	var (
		img image.Image
		err error
	)

	switch mime {
	case "image/png":
		img, err = png.Decode(bytes.NewReader(b))
	case "image/jpeg":
		img, err = jpeg.Decode(bytes.NewReader(b))
	default:
		err = xerrors.Errorf("gfx: unexpected texture MIME type %q", mime)
	}

	if err != nil {
		return nil, xerrors.Errorf("gfx: for texture %q: %w", name, err)
	}

	return NewTexture(name, img, nearest), nil
}
