SpyCards.Audio.showUI(document.body);

(async function () {
	const argv: string[] = [];

	let restart = true;
	const params = new URLSearchParams(location.search);
	if (params.has("recording")) {
		argv.push("-run=TermacadeRecording", "-recording=" + params.get("recording"));
	} else if (params.has("highscores")) {
		argv.push("-run=HighScores");
	} else if (params.get("autoPlay") === "0") {
		argv.push("-run=FlowerJourney");
	} else if (params.get("autoPlay") === "1") {
		argv.push("-run=MiteKnight");
	} else {
		argv.push("-run=TermacadeMenu");
		restart = false;
	}

	if (params.has("seed")) {
		argv.push("-seed=" + params.get("seed"));
	}

	if (params.has("disableCRT")) {
		argv.push("-disable-crt");
	}

	if (params.has("drawButtons")) {
		argv.push("-draw-buttons");
	}

	if (params.has("superSpeed")) {
		argv.push("-super-speed");
	}

	if (params.has("useAI")) {
		argv.push("-use-ai");
	}

	SpyCards.disableErrorHandler = true;

	let promise = SpyCards.Native.run(...argv);
	if (restart) {
		promise = promise.then(() => {
			if (SpyCards.disableErrorHandler) {
				location.search = "";
			}
		});
	}

	return promise;
})();
