package gui

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"image/color"
)

type Label struct {
	Panel
	Text      string
	FontColor color.RGBA
}

func (l *Label) SetFontColor(c color.RGBA) {
	l.FontColor = c
}

func (l *Label) Pack(r Rect) Rect { return l.Rect }

func (l *Label) Render(tb *sprites.Batch) {
	var x, y = l.Position()
	println("REMDERN")
	sprites.DrawText(tb, sprites.FontD3Streetism, l.Text, x, y, 0, 1, 1, l.FontColor)
}

func NewLabel(t string) *Label {
	l := &Label{
		Panel:     *NewPanel(),
		Text:      t,
		FontColor: sprites.White,
	}
	l.node = l
	return l
}
