function demoRandom<T>(arr: T[]): T {
	return arr[Math.floor(Math.random() * arr.length)];
}

(async function () {
	const selectedP1 = SpyCards.loadSettings().character;
	if (new URLSearchParams(location.search).has("select")) {
		const rpc = await SpyCards.Native.roomRPC();
		const mineID = await new Promise<number>((resolve) => rpc.newMine(resolve));
		rpc.setRoom(mineID);
		await new Promise<string>((resolve) => rpc.onMineSelected(resolve));
		await sleep(500);
		location.href = "room.html";
		return;
	}
	const theRoom = new SpyCards.TheRoom.Stage();
	await theRoom.init();
	const p1 = new SpyCards.TheRoom.Player(1, selectedP1 || demoRandom(SpyCards.TheRoom.Player.characters).name);
	theRoom.setPlayer(p1);
	const p2 = new SpyCards.TheRoom.Player(2, demoRandom(SpyCards.TheRoom.Player.characters).name);
	theRoom.setPlayer(p2);

	const rand = new Uint8Array(16);
	crypto.getRandomValues(rand);
	const audience = await theRoom.createAudience(rand, 0);
	const nearAudience = audience.filter((a) => a.isBack());
	const farAudience = audience.filter((a) => !a.isBack());

	function newAudienceMember(back: boolean) {
		const type = Math.floor(Math.random() * 6);
		const flip = Math.random() >= 0.5;
		const color = Math.random();
		const excitement = Math.random();
		if (back) {
			const x = Math.random() * 80 + 10;
			const y = x < 40 || x > 60 ? (Math.random() * 20 + 25) : (Math.random() * 15 + 30);
			const a = theRoom.createAudienceMember(type, x / 10, -y / 10, true, flip, color, Math.pow(excitement, 1.5));
			theRoom.addAudienceMember(a);
			return a;
		} else {
			const x = Math.random() * 87.5 + 6.25;
			const y = Math.random() * 22 + 25;
			const a = theRoom.createAudienceMember(type, x / 10, y / 10, false, flip, color, Math.pow(excitement, 2));
			theRoom.addAudienceMember(a);
			return a;
		}
	}

	const hint = document.createElement("h1");
	hint.textContent = "1 2 ← ↑ → ↓ F B";
	hint.style.position = "absolute";
	hint.style.left = "100px";
	hint.style.opacity = "0";
	hint.style.color = "#fff";
	document.body.appendChild(hint);

	let hintOpacity = 0;
	const showHintMore = setInterval(function () {
		hintOpacity += 0.0001;
		hint.style.opacity = String(hintOpacity);
		if (hintOpacity >= 1) {
			clearInterval(showHintMore);
		}
	}, 100);

	addEventListener("keydown", function (e) {
		if (SpyCards.disableKeyboard) {
			return;
		}
		let any = true;
		switch (e.code) {
			case "ArrowLeft":
				p1.becomeAngry();
				break;
			case "ArrowRight":
				p2.becomeAngry();
				break;
			case "Digit1":
				location.href = "room.html?select";
				break;
			case "Digit2":
				p2.setCharacter(demoRandom(SpyCards.TheRoom.Player.characters));
				break;
			case "ArrowUp":
				if (farAudience.length) {
					demoRandom(farAudience).startCheer();
				}
				break;
			case "ArrowDown":
				if (nearAudience.length) {
					demoRandom(nearAudience).startCheer();
				}
				break;
			case "KeyF":
				nearAudience.push(newAudienceMember(true));
				break;
			case "KeyB":
				farAudience.push(newAudienceMember(false));
				break;
			default:
				any = false;
				break;
		}
		if (any) {
			hint.style.opacity = "0";
			clearInterval(showHintMore);
		}
	});
})();
