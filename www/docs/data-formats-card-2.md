---
title: Data Formats (Card v2 and v4)
...

(base64-encoded)

- varint FormatVersion (4)
- varint GlobalCardID
    - see Appendix A for a list of possible values below 128
    - for values 128 and above:
        - bits 5-6 are rank:
            - value 0 is attacker
            - value 1 is effect
            - value 2 is mini-boss
            - value 3 is boss
- uint8 Tribes
    - most significant 4 bits are tribe 1
    - least significant 4 bits are tribe 2
    - tribe 1 cannot be (none) and tribe 2 cannot be the same as tribe 1
    - see Appendix A for a list of possible values
- SpecialField\[...\] SpecialFields
- uint8 TP
    - most significant 4 bits must be set to 0.
    - least significant 4 bits are the card's TP cost
        - values 0..10 are used as-is
        - values 11..14 are reserved
        - value 15 means infinity
- uint8 Portrait
    - values 0..234 are an index into [the portrait spritesheet](../img/portraits.png)
    - value 254 means an embedded PNG follows the card
    - value 255 means an external portrait ID follows the card
- varint NameLength (0 means use original name)
- byte\[NameLength\] DisplayName (UTF-8)
- varint EffectCount
- EffectDef\[EffectCount\] Effects
- Remaining fields are conditional:
    - CustomTribeData tribe1 (if tribe 1 is 14)
    - CustomTribeData tribe2 (if tribe 2 is 14)
    - varint CustomPortraitLength (if portrait is 254 or 255)
    - byte\[CustomPortraitLength\] CustomPortrait (if portrait is 254 or 255)

## SpecialField

SpecialField is defined as a 4 bit prefix followed by a payload of a defined length.

The prefix 0000 is invalid as it represents the TP field, which is after the last SpecialField.

Currently defined SpecialField types:

### 0001: ExtraTribe

Allows a card to have more than 2 tribes.

- uint4 tribe (see Appendix A for a list)
- CustomTribeData custom (if tribe is 14)

All tribes on a card must be unique, and if ExtraTribe is present, no tribe can be none (15).

## EffectDef

- uint8 EffectID
- uint8 EffectFlags (each flag is allowed only on effects that define a meaning for it)
    - bit 7: (reserved)
    - bit 6: (reserved)
    - bit 5: defense
    - bit 4: generic
    - bit 3: late
    - bit 2: each
    - bit 1: opponent
    - bit 0: negate
- Data (defined below)

### Effect 0: Flavor Text

Defined flags:

- negate

Data:

- varint TextLength
- byte\[TextLength\] Text

### Effect 1: Stat

Defined flags:

- negate
- opponent
- defense

Data:

- uint8 Amount (255 = infinity)

### Effect 2: Empower

Defined flags:

- opponent
- negate
- generic
- defense

Data:

- uint8 Amount (255 = infinity)
- PackedRankTribe Filter (if flag generic)
- varint GlobalCardID (if not flag generic)

### Effect 3: Summon

Defined flags:

- negate
- opponent
- generic
- defense

Data:

- uint8 CountMinusOne
- PackedRankTribe Filter (if flag generic)
- varint GlobalCardID (if not flag generic)

### Effect 4: Heal

Defined flags:

- negate
- opponent
- each
- generic
- defense (no effect with generic)

Data:

- uint8 Amount (255 = infinity)

### Effect 5: TP

Defined flags:

- negate

Data:

- uint8 Amount (255 = infinity)

### Effect 6: Numb

Defined flags:

- generic
- opponent

Data:

- uint8 Amount (255 = infinity)
- PackedRankTribe Filter (if flag generic, since version 4)
- varint GlobalCardID (if not flag generic, since version 4)

When decoding version 2, act as if the effect has the generic and opponent flags set and is targeting Rank: Attacker, Tribe: None.

### Effect 128: Card (Condition)

Defined flags:

- negate
- opponent
- each
- generic

Data:

- uint8 CountMinusOne (if not flag each)
- PackedRankTribe Filter (if flag generic)
- varint GlobalCardID (if not flag generic)
- EffectDef Result

### Effect 129: Limit (Condition)

Defined flags:

- negate

Data:

- uint8 CountMinusOne
- EffectDef Result

### Effect 130: Winner (Condition)

Defined flags:

- negate
- opponent

Data:

- EffectDef Result

### Effect 131: Apply (Condition)

Defined flags:

- negate
- opponent
- late

Data:

- EffectDef Result

### Effect 132: Coin (Condition)

Defined flags:

- negate
- generic
- defense

Data:

- uint8 CountMinusOne
- EffectDef HeadsResult
- EffectDef TailsResult (if flag generic)

### Effect 133: HP (Condition)

Defined flags:

- negate
- opponent
- late

Data:

- uint8 CountMinusOne
- EffectDef Result

### Effect 134: Stat (Condition)

Defined flags:

- negate
- opponent
- late
- defense

Data:

- uint8 CountMinusOne
- EffectDef Result

### Effect 135: Priority (Condition)

Defined flags:

- (depends on EffectiveType)

Data:

- uint8 EffectiveType
- EffectDef Result

### Effect 136: On Numb (Condition)

Defined flags:

(no defined flags)

Data:

- EffectDef Result

## PackedRankTribe

- uint8 RankTribe
    - most significant bit: reserved (0)
    - bits 4-6: rank
        - value 0 is attacker
        - value 1 is effect
        - value 2 is mini-boss
        - value 3 is boss
        - value 4 is enemy (attacker and effect)
        - value 5 is reserved (invalid)
        - value 6 is reserved (invalid)
        - value 7 is (none)
    - bits 0-3: tribe (see Appendix A for a list)
- if tribe is 14:
    - varint NameLength
    - byte\[NameLength\] TribeName

## CustomTribeData

- uint8 Red
- uint8 Green
- uint8 Blue
- varint NameLength
- byte\[NameLength\] TribeName
