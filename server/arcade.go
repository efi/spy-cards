package server

import (
	"bytes"
	"context"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/input"

	"golang.org/x/xerrors"
)

func handleArcadeAPIUpload(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPut)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPut {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPut {
		w.Header().Add("Allow", "PUT, OPTIONS")
		http.Error(w, "only PUT is allowed", http.StatusMethodNotAllowed)

		return
	}

	defer r.Body.Close()

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("arcade recording upload error (read)", err)
		http.Error(w, "failed to read uploaded recording", http.StatusInternalServerError)

		return
	}

	var id int64

	hash := sha256.Sum256(data)

	err = insertArcadeRecording.QueryRow(r.Header.Get("X-Forwarded-For"), r.UserAgent(), data, hash[:]).Scan(&id)
	if err != nil {
		log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
		log.Println("arcade recording upload error (db)", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	go decodeArcadeRecording(id)

	w.WriteHeader(http.StatusCreated)
}

func handleArcadeAPIGetRecording(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	var id format.FileID

	err := id.UnmarshalText([]byte(r.URL.Path[len("/spy-cards/arcade/api/get/"):]))
	if err != nil {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	var hash, data []byte

	err = getArcadeRecording.QueryRow(id.ID).Scan(&hash, &data)
	if xerrors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=600")
		http.NotFound(w, r)

		return
	}

	if !bytes.Equal(id.Hash[:], hash[:8]) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", "application/vnd.spycards.arcadedata")
	w.Header().Set("Content-Length", strconv.Itoa(len(data)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(data)
	}
}

var playerNamePattern = regexp.MustCompile(`\A[A-Z0-9]{3}\z`)

func decodeArcadeRecording(id int64) {
	var (
		game    sql.NullString
		player  sql.NullString
		score   sql.NullInt64
		vanilla sql.NullBool
		rec     arcade.Recording
	)

	var hash, data []byte

	err := getArcadeRecording.QueryRow(id).Scan(&hash, &data)
	if err != nil {
		goto handleError
	}

	err = rec.UnmarshalBinary(data)
	if err != nil {
		goto handleError
	}

	game.Valid = true
	game.String = rec.Game.String()

	player.Valid = true
	player.String = rec.PlayerName

	if !playerNamePattern.MatchString(rec.PlayerName) {
		err = xerrors.New("invalid player name")

		goto handleError
	}

	vanilla.Valid = true
	vanilla.Bool = reflect.DeepEqual(rec.Rules, rec.Rules.Default())

	{
		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
		defer cancel()
		score.Int64, err = computeScore(ctx, &rec)
		if err == nil || score.Int64 > 0 {
			score.Valid = true
		}
		if err == nil && (score.Int64 < 4500 || (rec.Game == arcade.MiteKnight && score.Int64 < 9500)) {
			err = xerrors.Errorf("arcade: score (%d) is too low and should not have been uploaded", score.Int64)
		}
	}

	goto updateDB

handleError:
	if err == nil {
		err = xerrors.New("unknown error")
	}

updateDB:
	var errString sql.NullString

	if err != nil {
		errString.String = fmt.Sprintf("%+v", err)
		errString.Valid = true
	}

	_, err = decodedArcadeRecording.Exec(id, &errString, &game, &player, &score, &vanilla)
	if err != nil {
		log.Println("failed to update decoded recording:", err)
	}
}

func handleArcadeAPIHighScores(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	var game arcade.Game

	switch r.FormValue("g") {
	case "1":
		game = arcade.MiteKnight
	case "2":
		game = arcade.FlowerJourney
	default:
		http.Error(w, "missing or invalid game (g) parameter", http.StatusBadRequest)

		return
	}

	rawMode := r.FormValue("m")

	var query *sql.Stmt

	switch rawMode {
	case "r":
		query = getArcadeHighScoresRecent
	case "w":
		query = getArcadeHighScoresWeekly
	case "q":
		query = getArcadeHighScoresQuarterly
	case "a":
		query = getArcadeHighScoresAllTime
	default:
		http.Error(w, "missing or invalid mode (m) parameter", http.StatusBadRequest)

		return
	}

	page, err := strconv.Atoi(r.FormValue("p"))
	if err != nil {
		http.Error(w, "parsing page number parameter: "+err.Error(), http.StatusBadRequest)

		return
	}

	if page < 0 {
		http.Error(w, "cannot process negative page number", http.StatusBadRequest)

		return
	}

	type Entry struct {
		ID     format.FileID `json:"c"`
		Score  int64         `json:"s"`
		Player string        `json:"p"`
	}

	entries := make([]Entry, 0, 5)

	rows, err := query.QueryContext(r.Context(), game.String(), page*cap(entries), cap(entries))
	if err != nil {
		log.Println("high scores error (query)", game, page, rawMode, err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			entry Entry
			hash  []byte
		)

		err = rows.Scan(&entry.ID.ID, &hash, &entry.Score, &entry.Player)
		if err != nil {
			log.Println("high scores error (scan)", game, page, rawMode, err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(entry.ID.Hash[:], hash)

		entries = append(entries, entry)
	}

	if err = rows.Err(); err != nil {
		log.Println("high scores error (close)", game, page, rawMode, err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	b, err := json.Marshal(entries)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(b)))
	w.Header().Set("Cache-Control", "public, max-age=0")
	_, _ = w.Write(b)
}

func computeScore(ctx context.Context, rec *arcade.Recording) (int64, error) {
	var create game.CreateFunc

	ctx, _ = input.NewContext(ctx, func() []arcade.Button {
		return nil
	})

	switch rec.Game {
	case arcade.MiteKnight:
		create = miteknight.New(*rec.Rules.(*arcade.MiteKnightRules))
	case arcade.FlowerJourney:
		create = flowerjourney.New(*rec.Rules.(*arcade.FlowerJourneyRules))
	default:
		return 0, xerrors.Errorf("server: don't know how to process game recording type %v", rec.Game)
	}

	state, err := game.PlayRecording(ctx, create, rec)
	if err != nil {
		return int64(state.Score), xerrors.Errorf("server: processing recording: %w", err)
	}

	var totalInputs uint64
	for _, i := range rec.Inputs {
		totalInputs += 1 + i.AdditionalTicks
	}

	if state.TickCount > totalInputs+30 || state.TickCount < totalInputs-30 {
		return int64(state.Score), xerrors.Errorf("server: expected recording to be %d ticks but was %d", totalInputs, state.TickCount)
	}

	if state.RNG.Count != rec.FinalRandCount {
		return int64(state.Score), xerrors.Errorf("server: expected %d RNG calls but had %d", rec.FinalRandCount, state.RNG.Count)
	}

	return int64(state.Score), nil
}
