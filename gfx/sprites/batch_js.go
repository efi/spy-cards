// +build js,wasm
// +build !headless

package sprites

import (
	"syscall/js"
	"time"
	"unsafe"

	"git.lubar.me/ben/spy-cards/gfx"
)

var (
	jsMatrixBuf = js.Global().Get("Uint8Array").New(4 * 4 * 4 * 2)

	jsRender js.Value

	_ = gfx.Custom("Batch.fastRender", jsRenderInit, func() {
		jsRender = js.Null()
	})
)

func (sb *Batch) fastRender() bool {
	if !jsRender.Truthy() {
		return false
	}

	var tex [len(sb.texture)]js.Value

	for i := range sb.texture {
		if sb.texture[i] != nil {
			tex[i] = *sb.texture[i].LazyTexture(Blank.Texture()).Value
		} else {
			tex[i] = *Blank.Texture().Value
		}
	}

	/*#nosec*/
	js.CopyBytesToJS(jsMatrixBuf, (*[4 * 4 * 4 * 2]byte)(unsafe.Pointer(&sb.camMatrix))[:])

	// if this line fails to compile, the number of textures has been
	// changed, and this function needs to be updated.
	_ = [len(sb.texture) - 8 + 1]struct{}{}[0]

	jsRender.Invoke(sb.dataBuf.Value, sb.elemBuf.Value, sb.count, tex[0], tex[1], tex[2], tex[3], tex[4], tex[5], tex[6], tex[7])

	return true
}

func jsRenderInit() {
	jsRender = js.Global().Get("Function").New("gl,prog,pbuf,cbuf,puni,cuni,tuni,tdiff,polyattr,mmattr,tmmattr,posattr,rotattr,blurattr,flagattr,texattr,colorattr", `
	tdiff -= performance.now() / 1000;
	gl.useProgram(prog);

	return function fastRenderBatch(data, elem, count, tex0, tex1, tex2, tex3, tex4, tex5, tex6, tex7) {
		"use strict";

		gl.useProgram(prog);

		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, tex0);
		gl.activeTexture(gl.TEXTURE1);
		gl.bindTexture(gl.TEXTURE_2D, tex1);
		gl.activeTexture(gl.TEXTURE2);
		gl.bindTexture(gl.TEXTURE_2D, tex2);
		gl.activeTexture(gl.TEXTURE3);
		gl.bindTexture(gl.TEXTURE_2D, tex3);
		gl.activeTexture(gl.TEXTURE4);
		gl.bindTexture(gl.TEXTURE_2D, tex4);
		gl.activeTexture(gl.TEXTURE5);
		gl.bindTexture(gl.TEXTURE_2D, tex5);
		gl.activeTexture(gl.TEXTURE6);
		gl.bindTexture(gl.TEXTURE_2D, tex6);
		gl.activeTexture(gl.TEXTURE7);
		gl.bindTexture(gl.TEXTURE_2D, tex7);

		gl.activeTexture(gl.TEXTURE0);

		gl.uniformMatrix4fv(puni, false, pbuf);
		gl.uniformMatrix4fv(cuni, false, cbuf);
	
		gl.bindBuffer(gl.ARRAY_BUFFER, data);
	
		gl.vertexAttribPointer(polyattr, 4, gl.UNSIGNED_BYTE, false, 72, 0);
		gl.vertexAttribPointer(mmattr, 4, gl.FLOAT, false, 72, 4);
		gl.vertexAttribPointer(tmmattr, 4, gl.FLOAT, false, 72, 20);
		gl.vertexAttribPointer(posattr, 3, gl.FLOAT, false, 72, 36);
		gl.vertexAttribPointer(rotattr, 3, gl.FLOAT, false, 72, 48);
		gl.vertexAttribPointer(blurattr, 1, gl.FLOAT, false, 72, 60);
		gl.vertexAttribPointer(flagattr, 1, gl.UNSIGNED_SHORT, false, 72, 64);
		gl.vertexAttribPointer(texattr, 1, gl.UNSIGNED_SHORT, false, 72, 66);
		gl.vertexAttribPointer(colorattr, 4, gl.UNSIGNED_BYTE, true, 72, 68);
	
		gl.enableVertexAttribArray(polyattr);
		gl.enableVertexAttribArray(mmattr);
		gl.enableVertexAttribArray(tmmattr);
		gl.enableVertexAttribArray(posattr);
		gl.enableVertexAttribArray(rotattr);
		gl.enableVertexAttribArray(blurattr);
		gl.enableVertexAttribArray(flagattr);
		gl.enableVertexAttribArray(texattr);
		gl.enableVertexAttribArray(colorattr);
	
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, elem);
	
		gl.uniform1f(tuni, performance.now() / 1000 + tdiff);
	
		gl.drawElements(gl.TRIANGLES, count, gl.UNSIGNED_SHORT, 0);
	
		//gl.disableVertexAttribArray(polyattr); // skip attrib 0
		gl.disableVertexAttribArray(mmattr);
		gl.disableVertexAttribArray(tmmattr);
		gl.disableVertexAttribArray(posattr);
		gl.disableVertexAttribArray(rotattr);
		gl.disableVertexAttribArray(blurattr);
		gl.disableVertexAttribArray(flagattr);
		gl.disableVertexAttribArray(texattr);
		gl.disableVertexAttribArray(colorattr);
	}`).Invoke(
		gfx.GL.(js.Wrapper).JSValue(),
		*spriteProgram.Program.Value,
		js.Global().Get("Float32Array").New(jsMatrixBuf.Get("buffer"), 0, 4*4),
		js.Global().Get("Float32Array").New(jsMatrixBuf.Get("buffer"), 4*4*4, 4*4),
		*spritePerspective.Uniform.Value,
		*spriteCamera.Uniform.Value,
		*spriteTime.Uniform.Value,
		time.Since(startTime).Seconds(),
		*spritePolygon.Attrib.Value,
		*spriteMinsMaxs.Attrib.Value,
		*spriteTexMinsMaxs.Attrib.Value,
		*spritePosition.Attrib.Value,
		*spriteRotation.Attrib.Value,
		*spriteBlur.Attrib.Value,
		*spriteFlags.Attrib.Value,
		*spriteTex.Attrib.Value,
		*spriteColor.Attrib.Value,
	)
}
