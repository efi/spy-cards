---
title: Data Formats
...

This page has been split into sub-pages as it has become very large.

- [Generic](data-formats-generic.html)
- [Card (v0 and v1)](data-formats-card-0.html)
- [Card (v2 and v4)](data-formats-card-2.html)
- [Game Mode (v3)](data-formats-card-3.html)
- [Deck](data-formats-deck.html)
- [Recording](data-formats-recording.html)
- [Termacade](data-formats-termacade.html)
- [Appendices](data-formats-appendix.html)
