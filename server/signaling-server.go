package server

import (
	"context"
	"crypto/rand"
	"database/sql"
	"errors"
	"io"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
	"nhooyr.io/websocket"
)

const (
	msgHandshake = 'p'
	msgSessionID = 's'
	msgKeepAlive = 'k'
	msgRelay     = 'r'
	msgQuit      = 'q'
)

type message struct {
	command byte
	payload string

	err error
}

type matchmakingEndedReason int

const (
	reasonCancelled matchmakingEndedReason = iota
	reasonConnected
)

func matchCodePrefix(client string) *[3]byte {
	var code string

	err := getMatchCodePrefix.QueryRow(client).Scan(&code)
	if xerrors.Is(err, sql.ErrNoRows) {
		return nil
	}

	if err != nil {
		log.Println("match code prefix err", err)

		return nil
	}

	b, err := format.Crockford32.DecodeString(code + "0")
	if err != nil {
		log.Println("match code prefix err", err)

		return nil
	}

	var bits [3]byte

	copy(bits[:], b)

	return &bits
}

func readMessages(conn *websocket.Conn, ch chan<- message) {
	ctx := context.TODO()
	ctx1, cancel := context.WithTimeout(ctx, time.Minute)

	for {
		_, msg, err := conn.Read(ctx1)
		if err != nil {
			cancel()

			ch <- message{err: err}
			close(ch)

			return
		}

		if len(msg) == 0 {
			continue // don't reset deadline
		}

		ch <- message{command: msg[0], payload: string(msg[1:])}

		cancel()
		ctx1, cancel = context.WithTimeout(ctx, time.Minute)
	}
}

func writeMessage(conn *websocket.Conn, msg message) {
	if msg.err != nil {
		log.Println("writeMessage called with err:", msg.err)

		return
	}

	ctx, cancel := context.WithTimeout(context.TODO(), 15*time.Second)
	defer cancel()

	err := conn.Write(ctx, websocket.MessageText, append([]byte{msg.command}, msg.payload...))
	if err != nil {
		log.Println("write err:", err)
	}
}

type pendingSession struct {
	auditID int64
	client  string
	ch      chan chan<- message
}

var sessions = struct {
	m map[[15]byte]pendingSession
	sync.Mutex
}{
	m: make(map[[15]byte]pendingSession),
}

func handler(conn *websocket.Conn, req *http.Request) {
	ch := make(chan message, 4)

	go readMessages(conn, ch)

	defer func() {
		go func() {
			// ensure readMessages goroutine finishes
			for range ch {
				// do nothing
			}
		}()
	}()

	xff := req.Header.Get("X-Forwarded-For")
	ua := req.UserAgent()

	var (
		codePrefix  *[3]byte
		player      byte
		sessionID   [15]byte
		auditID     int64
		waiting     chan chan<- message
		partnerIn   <-chan message
		partnerOut  chan<- message
		sawUseRelay bool
	)

	defer func() {
		if partnerOut != nil {
			close(partnerOut)
		}

		if player == '1' && waiting != nil {
			sessions.Lock()
			if _, ok := sessions.m[sessionID]; ok {
				delete(sessions.m, sessionID)
			} else {
				select {
				case <-waiting:
				case <-time.After(time.Second):
				}
				close(waiting)
			}
			sessions.Unlock()
		}
	}()

	for {
		select {
		case partner := <-waiting:
			if partner == nil {
				_ = conn.Close(internal.MMConnectionError, "no partner")

				log.Println("player", string(player), "received nil partner")

				return
			}

			partnerOut = partner

			if player == '1' {
				p2 := make(chan message, 4)
				select {
				case waiting <- p2:
					partnerIn = p2
				default:
					_ = conn.Close(internal.MMServerError, "failed to send channel")

					log.Println("player 1 failed to send channel")

					return
				}
			}

			if player == '2' {
				writeMessage(conn, message{command: msgHandshake})
				select {
				case partnerOut <- message{command: msgHandshake}:
				case <-time.After(time.Minute):
					_ = conn.Close(internal.MMServerError, "failed to send handshake after 1 minute")

					log.Println("failed to send handshake after 1 minute")

					return
				}
			}

			waiting = nil

		case msg, ok := <-partnerIn:
			if !ok {
				writeMessage(conn, message{
					command: msgQuit,
				})

				return
			}

			writeMessage(conn, msg)

		case msg := <-ch:
			if msg.err != nil {
				if !errors.Is(msg.err, io.EOF) && websocket.CloseStatus(msg.err) != websocket.StatusGoingAway {
					log.Println("recv error:", msg.err)

					_ = conn.Close(internal.MMServerError, "recv err")
				}

				return
			}

			switch msg.command {
			case msgHandshake:
				if player != 0 || len(msg.payload) < 1 {
					_ = conn.Close(internal.MMClientError, "invalid handshake")

					if player != 0 {
						log.Println("duplicate handshake")
					} else {
						log.Printf("invalid handshake packet %x", msg.payload)
					}

					return
				}

				codePrefix = matchCodePrefix(msg.payload[1:])

				switch player = msg.payload[0]; player {
				case 'q':
					if codePrefix == nil {
						_ = conn.Close(internal.MMClientError, "invalid quick join suffix")

						log.Printf("invalid quick join suffix %q", msg.payload)

						return
					}

					copy(sessionID[:], codePrefix[:])
					sessions.Lock()
					if pending, ok := sessions.m[sessionID]; ok {
						delete(sessions.m, sessionID)
						sessions.Unlock()

						auditID, waiting = pending.auditID, pending.ch
						if auditID != 0 {
							_, err := auditCompleteMatchmaking.Exec(auditID, reasonConnected, xff, ua)
							if err != nil {
								log.Println("updating matchmaking audit ID", auditID, "error:", err)
							}
						} else {
							log.Println("audit ID was 0; skipping matchmaking audit log update")
						}

						partner := make(chan message, 4)
						select {
						case waiting <- partner:
							partnerIn = partner
						default:
							_ = conn.Close(internal.MMServerError, "failed to send channel")

							log.Println("quick join player 2 failed to send channel")

							return
						}

						player = '2'

						writeMessage(conn, message{
							command: msgSessionID,
							payload: "2",
						})
					} else {
						if err := auditMatchmakingBegin.QueryRow(format.Encode32(sessionID[:]), msg.payload[1:], xff, ua).Scan(&auditID); err != nil {
							log.Println("creating matchmaking audit error:", err)
						}

						waiting = make(chan chan<- message, 1)
						sessions.m[sessionID] = pendingSession{
							auditID: auditID,
							client:  msg.payload[1:],
							ch:      waiting,
						}
						sessions.Unlock()

						player = '1'
						writeMessage(conn, message{
							command: msgSessionID,
							payload: "1",
						})
					}
				case '1':
					_, err := io.ReadFull(rand.Reader, sessionID[:])
					if err != nil {
						log.Println("read(random) error:", err)

						_ = conn.Close(internal.MMServerError, "read rand error")

						return
					}

					if codePrefix != nil {
						sessionID[0] = codePrefix[0]
						sessionID[1] = codePrefix[1]
						sessionID[2] &= 0xf
						sessionID[2] |= codePrefix[2]
					}

					if err := auditMatchmakingBegin.QueryRow(format.Encode32(sessionID[:]), msg.payload[1:], xff, ua).Scan(&auditID); err != nil {
						log.Println("creating matchmaking audit error:", err)
					}

					waiting = make(chan chan<- message, 1)

					sessions.Lock()

					sessions.m[sessionID] = pendingSession{
						auditID: auditID,
						client:  msg.payload[1:],
						ch:      waiting,
					}

					sessions.Unlock()

					sessionStr := format.Encode32Space(sessionID[:])

					writeMessage(conn, message{
						command: msgSessionID,
						payload: sessionStr,
					})
				case '2':
					// wait for session ID
				default:
					_ = conn.Close(internal.MMClientError, "invalid handshake")

					log.Printf("invalid handshake player number %q", msg.payload)

					return
				}
			case msgSessionID:
				if player != '2' || waiting != nil || partnerIn != nil {
					_ = conn.Close(internal.MMClientError, "unexpected session ID packet")

					log.Println("unexpectedly received session ID from player", string(player))

					return
				}

				b, err := format.Decode32(msg.payload)
				if err != nil || len(b) != len(sessionID) {
					_ = conn.Close(internal.MMClientError, "invalid session ID")

					return
				}

				copy(sessionID[:], b)

				sessions.Lock()

				pending := sessions.m[sessionID]
				delete(sessions.m, sessionID)

				sessions.Unlock()

				auditID, waiting = pending.auditID, pending.ch

				if waiting == nil {
					_ = conn.Close(internal.MMNotFound, "session not found")

					return
				}

				if auditID != 0 {
					_, err := auditCompleteMatchmaking.Exec(auditID, reasonConnected, xff, ua)
					if err != nil {
						log.Println("updating matchmaking audit ID", auditID, "error:", err)
					}
				} else {
					log.Println("audit ID was 0; skipping matchmaking audit log update")
				}

				partner := make(chan message, 4)
				select {
				case waiting <- partner:
					partnerIn = partner
				default:
					_ = conn.Close(internal.MMServerError, "failed to send channel")

					log.Println("player 2 failed to send channel")

					return
				}
			case msgKeepAlive:
				if auditID != 0 && player == '1' {
					_, err := auditMatchmakingPing.Exec(auditID)
					if err != nil {
						log.Println("updating matchmaking audit ID", auditID, "error:", err)
					}
				}

				writeMessage(conn, message{
					command: msgKeepAlive,
				})
			case msgRelay:
				if partnerOut == nil || waiting != nil {
					_ = conn.Close(internal.MMClientError, "cannot relay without partner")

					log.Println("relay without partner")

					return
				}

				if auditID != 0 && player == '1' {
					_, err := auditMatchmakingPing.Exec(auditID)
					if err != nil {
						log.Println("updating matchmaking audit ID", auditID, "error:", err)
					}

					if !sawUseRelay && strings.Contains(msg.payload, `"type":"r"`) {
						_, err := auditMatchmakingUsedRelay.Exec(auditID)
						if err != nil {
							log.Println("updating matchmaking audit ID", auditID, "error:", err)
						}

						sawUseRelay = true
					}
				}

				select {
				case partnerOut <- msg:
				case <-time.After(time.Minute):
					_ = conn.Close(internal.MMTimeout, "relay timed out")

					log.Println("relay timed out")

					return
				}
			case msgQuit:
				if auditID != 0 && player == '1' && waiting != nil {
					_, err := auditCompleteMatchmaking.Exec(auditID, reasonCancelled, nil, nil)
					if err != nil {
						log.Println("updating matchmaking audit ID", auditID, "error:", err)
					}
				}

				return
			default:
				_ = conn.Close(internal.MMClientError, "unknown command ID")

				log.Println("unknown command ID", string(msg.command))

				return
			}
		}
	}
}
