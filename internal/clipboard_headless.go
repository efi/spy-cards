// +build headless

package internal

import "golang.org/x/xerrors"

func ReadClipboard() (string, error) {
	return "", xerrors.New("internal: cannot read clipboard in headless mode")
}

func WriteClipboard(s string) error {
	return xerrors.New("internal: cannot write clipboard in headless mode")
}
