---
title: Secure Card Game Protocol Specification
...

This document describes the protocol used to secure and verify Spy Cards Online matches.

It is expected that you have at least a basic understanding of cryptography and computer networking, as the fundamentals of these concepts will not be explained in this document.

| Placeholder | Value Used | Description                                            |
|-------------|------------|--------------------------------------------------------|
| HASH\_FUNC  | `SHA-256`  | The function used for key stretching and verification. |
| SEED\_LEN   | `16`       | The initial session key length from each side.         |
| RAND\_LEN   | `4`        | The length of various temporary random keys.           |

  : *Tunable algorithm properties and their values as used in Spy Cards Online*

# Networking

## Exchange Data

The protocol uses a single networking primitive: Exchange Data.

Both sides of the connection send a packet and receive the other side's packet. The implementation may send additional metadata (length, type, etc.)

Spy Cards Online prefixes the data with two varints, representing the packet type and the turn number (zero for non-repeated packets).

Additional data must be sent outside the scope of this protocol - for example, most game messages would be transferred using an asymmetric mechanism, as the non-acting player's computer would not have any way of knowing an action was about to take place.

## Initial Handshake

1. Generate SEED\_LEN bytes of random data as `SharedSeedHalf[Local]`.
2. Exchange Data (0): `SharedSeedHalf`
3. `SharedSeed` is defined as the concatination of the two `SharedSeedHalf` values, with player 1/host/listener's half first and player 2/client/dialer's half second.
4. Generate RAND\_LEN bytes of random data as `PrivateSeed[Local]`.
5. Encode the initial deck of cards.
6. Compute HASH\_FUNC(`SharedSeed` + `PrivateSeed[Local]` + `InitialDeck[Local]`) as `InitialHash[Local]`.
7. Encode the backs of the cards in the initial deck as `InitialDeckBacks[Local]`.
8. Exchange Data (1): `InitialHash` + `InitialDeckBacks`
9. Verify that `InitialDeckBacks[Remote]` can be a valid deck, and store the data for later.

## Turn Start

1. Generate RAND\_LEN bytes of random data as `ShuffleSeedHalf[Local]`.
2. Compute HASH\_FUNC(`SharedSeed` + `ShuffleSeedHalf[Local]`) as `ShuffleHash[Local]`.
3. Exchange Data (2): `ShuffleHash`
4. Exchange Data (3): `ShuffleSeedHalf`
5. Verify that HASH\_FUNC(`SharedSeed` + `ShuffleSeedHalf[Remote]`) matches `ShuffleHash[Remote]`
6. `ShuffleSeed` is defined as the concatination of the two `ShuffleSeedHalf`, in the same order as `SharedSeed`.
7. Shuffle the deck and draw cards.

## Turn Ready

TODO

## Turn End

<!--Exchange Data (4): `TurnSeedHalf` + `TurnData`-->
TODO

## Finalization

1. Exchange Data (5): `PrivateSeed` + `InitialDeck`.
2. Verify that `InitialHash[Remote]` matches HASH\_FUNC(`SharedSeed` + `PrivateSeed[Remote]` + `InitialDeck[Remote]`).
3. Verify that `InitialDeckBacks[Remote]` matches `InitialDeck[Remote]`.
4. Re-simulate all turns, ensuring that the cards played each turn were in the opponent's hand.
