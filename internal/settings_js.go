// +build js,wasm
// +build !headless

package internal

import (
	"syscall/js"
)

func init() {
	OnSettingsChanged = append(OnSettingsChanged, func(*Settings) {
		sw := js.Global().Get("navigator").Get("serviceWorker")
		if sw.Truthy() {
			sw = sw.Get("controller")
		}
		if sw.Truthy() {
			sw.Call("postMessage", map[string]interface{}{"type": "settings-changed"})
		}
	})

	js.Global().Call("addEventListener", "spy-cards-settings-changed", js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		s := LoadSettings()

		for _, f := range OnSettingsChanged {
			f(s)
		}

		return js.Undefined()
	}))
}
