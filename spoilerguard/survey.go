package spoilerguard

/*
	TODO:
	export async function surveySaveData(form: HTMLElement): Promise<SaveData> {
		async function ask(question: string, options: string[]): Promise<number> {
			const p = document.createElement("p");
			p.textContent = question;
			form.appendChild(p);

			const buttons = document.createElement("div");
			buttons.classList.add("buttons");
			form.appendChild(buttons);

			const optionPromises = options.map((opt, i) => {
				return new Promise<number>((resolve) => {
					const btn = SpyCards.UI.button(opt, [], () => resolve(i));
					buttons.appendChild(btn);
				});
			});

			const response = await Promise.race(optionPromises);

			SpyCards.UI.remove(p);
			SpyCards.UI.remove(buttons);

			return response;
		}

		if (!await ask("Which do you want to do?", ["Upload Save File", "Answer Some Questions"])) {
			return null;
		}

		const chapterNumber = await ask("What is your current chapter number?", ["1", "2", "3", "4", "5", "6", "7"]) + 1;
		const questCompleted = chapterNumber > 2 && !await ask("Have you completed the sidequest \"Requesting Assistance\"?", ["Yes", "No"]);

		const enemyData = new Uint8Array(256 / 8);
		function seenSpied(id: number) {
			enemyData[id >> 2] |= 3 << ((id & 3) << 1);
		}

		if (questCompleted) {
			seenSpied(31); // Monsieur Scarlet
		}

		switch (chapterNumber) {
			case 7:
				seenSpied(87); // Dead Lander α
				seenSpied(88); // Dead Lander β
				seenSpied(89); // Dead Lander γ

				if (chapterNumber > 7 || (await ask("Which is the furthest area you have reached?", ["Dead Lands", "The Machine", "Sapling Plains"]) === 2 && !await ask("Has the sapling been destroyed?", ["Yes", "No"]))) {
					seenSpied(90); // Wasp King
					seenSpied(91); // The Everlasting King

					if (chapterNumber > 7 || !await ask("Have you fought Team Maki? Answer yes even if you lost.", ["Yes", "No"])) {
						seenSpied(92); // Maki
						seenSpied(93); // Kina
						seenSpied(94); // Yin
					}
				}

			// fallthrough
			case 6:
				if (chapterNumber > 6 || !await ask("Have you crossed the Forsaken Lands?", ["Yes", "No"])) {
					seenSpied(37); // Plumpling
					seenSpied(64); // Mimic Spider
					seenSpied(78); // Mothfly
					seenSpied(79); // Mothfly Cluster
					seenSpied(80); // Ironnail

					if (chapterNumber > 6 || !await ask("Do you have the boat?", ["Yes", "No"])) {
						seenSpied(74); // Cross
						seenSpied(75); // Poi
						seenSpied(76); // Primal Weevil

						if (chapterNumber > 6 || !await ask("Have you reached Rubber Prison?", ["Yes", "No"])) {
							seenSpied(82); // Ruffian

							if (chapterNumber > 6) {
								seenSpied(95); // ULTIMAX Tank
							}
						}
					}
				}

			// fallthrough
			case 5:
				if (chapterNumber > 5 || !await ask("Have you reached the swamp?", ["Yes", "No"])) {
					seenSpied(38); // Flowerling
					seenSpied(63); // Jumping Spider
					seenSpied(71); // Mantidfly
					seenSpied(73); // Wild Chomper

					if (chapterNumber > 5 || !await ask("Have you reached the Wasp Kingdom?", ["Yes", "No"])) {
						seenSpied(65); // Leafbug Ninja
						seenSpied(66); // Leafbug Archer
						seenSpied(67); // Leafbug Clubber
						seenSpied(68); // Madesphy
						seenSpied(69); // The Beast

						if (chapterNumber > 5) {
							seenSpied(26); // Wasp Bomber
							seenSpied(27); // Wasp Driller
							seenSpied(72); // General Ultimax
							seenSpied(97); // Riz
						}
					}
				}

				if (!await ask("Have you discovered a use for the gem dropped by The Watcher?", ["Yes", "No"])) {
					seenSpied(52); // Zombee
					seenSpied(53); // Zombeetle
					seenSpied(56); // Bloatshroom
					seenSpied(96); // Zommoth
				}

			// feedback
			case 4:
				if (chapterNumber > 4 || !await ask("Have you obtained the Earth Key?", ["Yes", "No"])) {
					seenSpied(40); // Astotheles

					if (chapterNumber > 4 || !await ask("Have you reached the sand castle?", ["Yes", "No"])) {
						seenSpied(49); // Dune Scorpion
						seenSpied(81); // Belostoss
						seenSpied(83); // Water Strider
						seenSpied(84); // Diving Spider

						if (chapterNumber > 4 || !await ask("Have you obtained the fourth artifact?", ["Yes", "No"])) {
							seenSpied(54); // The Watcher
							seenSpied(57); // Krawler
							seenSpied(58); // Haunted Cloth
							seenSpied(61); // Warden

							if (chapterNumber > 4) {
								seenSpied(23); // Kabbu
								seenSpied(51); // Kali
								seenSpied(85); // Cenn
								seenSpied(86); // Pisci
							}
						}

						if (!await ask("Have you discovered a use for the machine in Professor Honeycomb's office?", ["Yes", "No"])) {
							seenSpied(41); // Mother Chomper
							seenSpied(70); // Chomper Brute
						}
					}
				}

			// fallthrough
			case 3:
				if (chapterNumber > 3 || !await ask("Have you reached Defiant Root?", ["Yes", "No"])) {
					seenSpied(4); // Cactiling
					seenSpied(5); // Psicorp
					seenSpied(6); // Thief
					seenSpied(7); // Bandit
					seenSpied(28); // Wasp Scout
					seenSpied(33); // Arrow Worm
					seenSpied(39); // Burglar

					if (chapterNumber > 3 || !await ask("Have you found the Overseer?", ["Yes", "No"])) {
						seenSpied(42); // Ahoneynation
						seenSpied(43); // Bee-Boop
						seenSpied(44); // Security Turret
						seenSpied(45); // Denmuki
						seenSpied(48); // Abomihoney

						if (chapterNumber > 3) {
							seenSpied(46); // Heavy Drone B-33

							seenSpied(34); // Carmina
							seenSpied(36); // Broodmother
							seenSpied(47); // Mender
						}
					}
				}

			// fallthrough
			case 2:
				if (chapterNumber > 2 || !await ask("Have you reached Golden Settlement?", ["Yes", "No"])) {
					seenSpied(14); // Numbnail
					seenSpied(16); // Acornling
					seenSpied(17); // Weevil
					seenSpied(20); // Chomper
					seenSpied(25); // Wasp Trooper
					seenSpied(29); // Midge
					seenSpied(30); // Underling
					seenSpied(32); // Golden Seedling

					if (chapterNumber > 2 || !await ask("Have you gained passage to Golden Hills?", ["Yes", "No"])) {
						seenSpied(19); // Venus' Bud
						seenSpied(21); // Acolyte Aria

						if (chapterNumber > 2) {
							seenSpied(3); // Zasp
							seenSpied(15); // Mothiva
							seenSpied(24); // Venus' Guardian
						}
					}
				}

			// fallthrough
			case 1:
				seenSpied(0); // Zombiant
				seenSpied(1); // Jellyshroom
				seenSpied(2); // Spider
				seenSpied(8); // Inichas
				seenSpied(9); // Seedling
		}

		if (chapterNumber >= 3 && !await ask("Have you completed the following bounty: Devourer", ["Yes", "No"])) {
			seenSpied(98);
		}
		if (chapterNumber >= 4 && !await ask("Have you completed the following bounty: Tidal Wyrm", ["Yes", "No"])) {
			seenSpied(50);
		}
		if (chapterNumber >= 5 && !await ask("Have you completed the following bounty: Seedling King", ["Yes", "No"])) {
			seenSpied(35);
		}
		if (chapterNumber >= 6 && !await ask("Have you completed the following bounty: False Monarch", ["Yes", "No"])) {
			seenSpied(77);
		}
		if (chapterNumber >= 6 && enemyData[76 >> 2] & (1 << (76 & 3)) && !await ask("Have you completed the following bounty: Peacock Spider", ["Yes", "No"])) {
			seenSpied(55);
		}

		return {
			q: chapterNumber <= 2 ? -1 : questCompleted ? 2 : 0,
			t: questCompleted,
			a: questCompleted,
			d: null,
			s: Base64.encode(enemyData),
			m: null
		};
	}
*/
