module SpyCards.Fx {
	export const tpEl = document.createElement("span");
	tpEl.classList.add("tp-ui");
	tpEl.setAttribute("aria-label", "Your Teamwork Points");

	export const myHPEl = document.createElement("span");
	myHPEl.classList.add("hp-ui", "my-hp");
	myHPEl.setAttribute("aria-label", "Your Health");

	export const theirHPEl = document.createElement("span");
	theirHPEl.classList.add("hp-ui", "their-hp");
	theirHPEl.setAttribute("aria-label", "Their Health");

	export const myATK = document.createElement("div");
	myATK.classList.add("stat-atk", "my-atk");
	myATK.setAttribute("aria-label", "Your Attack Stat");

	export const theirATK = document.createElement("div");
	theirATK.classList.add("stat-atk", "their-atk");
	theirATK.setAttribute("aria-label", "Their Attack Stat");

	export const myDEF = document.createElement("div");
	myDEF.classList.add("stat-def", "my-def");
	myDEF.setAttribute("aria-label", "Your Defense Stat");

	export const theirDEF = document.createElement("div");
	theirDEF.classList.add("stat-def", "their-def");
	theirDEF.setAttribute("aria-label", "Their Defense Stat");

	export const myField = document.createElement("div");
	myField.classList.add("my-field");
	myField.setAttribute("aria-label", "Your Played Cards");

	export const theirField = document.createElement("div");
	theirField.classList.add("their-field");
	theirField.setAttribute("aria-label", "Their Played Cards");

	export const myFieldSetup = document.createElement("div");
	myFieldSetup.classList.add("my-field", "setup");
	myFieldSetup.setAttribute("aria-label", "Your Setup Cards");

	export const theirFieldSetup = document.createElement("div");
	theirFieldSetup.classList.add("their-field", "setup");
	theirFieldSetup.setAttribute("aria-label", "Their Setup Cards");

	if (window.requestIdleCallback) {
		requestIdleCallback(() => {
			// start loading the room RPC sooner than we need it.
			Native.roomRPC();
		});
	} else {
		setTimeout(() => Native.roomRPC(), 1);
	}

	let fieldEl: [HTMLDivElement, HTMLDivElement];

	let realFxSleep = function (ms: number): Promise<void> {
		const sg = SpoilerGuard.getSpoilerGuardData();
		if (sg && sg.m && (sg.m & 4)) {
			realFxSleep = () => Promise.resolve();
		} else {
			realFxSleep = (ms) => new Promise((resolve) => setTimeout(() => resolve(), ms));
		}

		return realFxSleep(ms);
	};
	let fxSleep = realFxSleep;
	let lastFlavor: EffectDef = null;

	function findEffectRecursive(path: string[], effects: EffectDef[], effect: EffectDef): boolean {
		for (let i = 0; i < effects.length; i++) {
			path.push(":nth-child(" + (i + 1) + ")");
			if (effects[i] === effect) {
				return true;
			}
			if (effects[i].type === EffectType.CondCoin) {
				path.push(".card-coin-heads");
				if (findEffectRecursive(path, [effects[i].result], effect)) {
					return true;
				}
				path.pop();
				path.push(".card-coin-tails");
				if (findEffectRecursive(path, effects[i].tailsResult ? [effects[i].tailsResult] : [], effect)) {
					return true;
				}
				path.pop();
			} else {
				if (effects[i].result && findEffectRecursive(path, [effects[i].result], effect)) {
					return true;
				}
			}
			path.pop();
		}
		return false;
	}

	function findEffect(desc: HTMLDivElement, effects: EffectDef[], effect: EffectDef): HTMLSpanElement {
		const path = [":scope"];

		if (!findEffectRecursive(path, effects, effect)) {
			throw new Error("cannot find effect");
		}

		const selector = path.join(" > ");
		const el = desc.querySelector<HTMLSpanElement>(selector);
		if (!el) {
			throw new Error("cannot find effect for selector " + selector);
		}
		return el;
	}

	export function getCallbacks(ctx: Context, audience: TheRoom.Audience[]): ProcessorCallbacks {
		fieldEl = [
			ctx.player === 1 ? myField : theirField,
			ctx.player === 2 ? myField : theirField
		];
		const setupFieldEl = [
			ctx.player === 1 ? myFieldSetup : theirFieldSetup,
			ctx.player === 2 ? myFieldSetup : theirFieldSetup
		];
		const hpEl = [
			ctx.player === 1 ? myHPEl : theirHPEl,
			ctx.player === 2 ? myHPEl : theirHPEl
		];

		let hpDelay = Promise.resolve();

		return {
			async beforeRound() {
				tpEl.classList.add("hidden");

				for (let player = 0; player < 2; player++) {
					fieldEl[player].classList.remove("won-round", "lost-round");
					fieldEl[player].classList.add("offscreen");
					SpyCards.UI.clear(fieldEl[player]);
					SpyCards.UI.clear(setupFieldEl[player]);

					for (let card of ctx.state.played[player]) {
						if (card.setup) {
							if (!card.setupPlaceholder) {
								card.setupPlaceholder = <HTMLDivElement>card.el.cloneNode(true);
							}
							setupFieldEl[player].appendChild(card.setupPlaceholder);
							card.el.style.visibility = "hidden";
						} else if (setupFieldEl[player].firstChild) {
							const placeholder = document.createElement("div");
							placeholder.classList.add("card");
							placeholder.style.visibility = "hidden";
							setupFieldEl[player].appendChild(placeholder);
						}
						fieldEl[player].appendChild(card.el);
					}

					document.body.appendChild(setupFieldEl[player]);
					document.body.appendChild(fieldEl[player]);
					doSquish(setupFieldEl[player]);
					doSquish(fieldEl[player]);
				}

				await fxSleep(125);

				myField.classList.remove("offscreen");
				theirField.classList.remove("offscreen");

				await fxSleep(250);

				ctx.ui.form.classList.add("hidden");
				for (let player = 0; player < 2; player++) {
					for (let card of ctx.state.played[player]) {
						if (card.setupPlaceholder) {
							UI.remove(card.setupPlaceholder);
							card.el.style.visibility = "";
						}
					}
				}

				lastFlavor = null;
			},
			async afterRound() {
				await fxSleep(250);

				myField.classList.add(ctx.state.winner === ctx.player ? "won-round" : "lost-round");
				theirField.classList.add(ctx.state.winner === 3 - ctx.player ? "won-round" : "lost-round");

				for (let player = 0; player < 2; player++) {
					SpyCards.UI.remove(setupFieldEl[player]);
					setupFieldEl[player].classList.add("offscreen");
					SpyCards.UI.clear(setupFieldEl[player]);
					document.body.appendChild(setupFieldEl[player]);

					for (let card of ctx.state.played[player]) {
						if (card.setup) {
							card.el.style.visibility = "";
						}
					}

					for (let setup of ctx.state.setup[player]) {
						const el = setup.card.createEl(ctx.defs);
						el.classList.add("setup");
						if (!setup.originalDesc) {
							el.querySelectorAll(".card-desc > *").forEach((e) => {
								SpyCards.UI.remove(e);
							});
							el.querySelector(".card-desc").appendChild(setup.effects[0].createEl(ctx.defs, setup.card));
						}
						setupFieldEl[player].insertBefore(el, setupFieldEl[player].firstChild);
					}
				}

				await fxSleep(500);

				for (let player = 0; player < 2; player++) {
					setupFieldEl[player].classList.remove("offscreen");
				}

				await fxSleep(250);

				removeCoins();

				SpyCards.UI.remove(myField);
				SpyCards.UI.remove(theirField);
				SpyCards.UI.remove(myATK);
				SpyCards.UI.remove(theirATK);
				SpyCards.UI.remove(myDEF);
				SpyCards.UI.remove(theirDEF);

				tpEl.classList.remove("hidden");
				ctx.ui.form.classList.remove("hidden");
			},
			async showStats() {
				myATK.classList.remove("stat-ready");
				theirATK.classList.remove("stat-ready");
				myDEF.classList.remove("offscreen");
				theirDEF.classList.remove("offscreen");

				myATK.textContent = "0";
				theirATK.textContent = "0";
				myDEF.textContent = "0";
				theirDEF.textContent = "0";

				document.body.appendChild(myATK);
				document.body.appendChild(theirATK);
				document.body.appendChild(myDEF);
				document.body.appendChild(theirDEF);
			},
			async beforeComputeWinner() {
				if (ctx.state.played[0].length || ctx.state.played[1].length) {
					SpyCards.Audio.Sounds.Toss11.play();
				}

				await fxSleep(500);

				myDEF.classList.add("offscreen");
				theirDEF.classList.add("offscreen");
				await fxSleep(250);
			},
			async afterComputeWinner() {
				myATK.classList.add("stat-ready");
				theirATK.classList.add("stat-ready");

				await fxSleep(750);

				if (ctx.state.winner) {
					if (ctx.state.players[2 - ctx.state.winner]) {
						ctx.state.players[2 - ctx.state.winner].becomeAngry();
					}
				}

				if (ctx.state.winner) {
					hpDelay = fxSleep(800);
				} else {
					hpDelay = Promise.resolve();
				}

				if (ctx.state.winner || ctx.state.hp[0] < ctx.defs.rules.maxHP / 2 || ctx.state.hp[1] < ctx.defs.rules.maxHP / 2) {
					for (let a of audience) {
						a.startCheer();
					}
				}

				if (ctx.state.winner === ctx.player) {
					SpyCards.Audio.Sounds.CrowdCheer2.play();
					SpyCards.Audio.Sounds.Damage0.play(0.5);
				} else if (ctx.state.winner) {
					SpyCards.Audio.Sounds.CrowdGasp.play();
					SpyCards.Audio.Sounds.Death3.play(0.5);
				} else {
					SpyCards.Audio.Sounds.Fail.play();
				}
			},
			async drawCard(player, card) {

			},
			async hpChange(player, amountBefore, amountChanged) {
				hpDelay.then(async function () {
					hpEl[player - 1].textContent = String(Math.min(amountBefore + amountChanged, ctx.defs.rules.maxHP));

					if (amountChanged > 0) {
						hpEl[player - 1].classList.add("healed");
						SpyCards.Audio.Sounds.Heal.play();
						await fxSleep(10);
						hpEl[player - 1].classList.remove("healed");
						await fxSleep(90);
					} else if (amountChanged !== 0) {
						hpEl[player - 1].classList.add("took-hit");
						await fxSleep(10);
						hpEl[player - 1].classList.remove("took-hit");
						await fxSleep(90);
					}
				});
				await fxSleep(100);
			},
			async statChange(player, stat, amountBefore, amountChanged, amountAfter) {
				let newAmount: string;
				if (amountAfter < 0) {
					newAmount = "0";
				} else if (isFinite(amountAfter)) {
					newAmount = String(amountAfter);
				} else {
					newAmount = "∞";
				}

				if (player == ctx.player) {
					if (stat === "ATK") {
						myATK.textContent = newAmount;
					} else {
						myDEF.textContent = newAmount;
					}
				} else {
					if (stat === "ATK") {
						theirATK.textContent = newAmount;
					} else {
						theirDEF.textContent = newAmount;
					}
				}

				await fxSleep(100);
			},
			async cardSummoned(summoner, summoned, special, effects) {
				summoned.el = summoned.card.createEl(ctx.defs);
				if (special === "setup-original" || special === "setup-effect") {
					summoned.setup = true;
					summoned.el.classList.add("setup");
					if (special === "setup-effect") {
						summoned.el.querySelectorAll(".card-desc > *").forEach((e) => {
							SpyCards.UI.remove(e);
						});
						summoned.el.querySelector(".card-desc").appendChild(effects[0].createEl(ctx.defs, summoned.card));
					} else {
						summoned.originalDesc = true;
					}
				}
				const oldParent = summoner.el.parentNode;
				const oldNext = summoner.el.nextSibling;
				if (special === "replace" && !oldParent) {
					special = null;
				}
				if (special === "invisible" || special === "both-invisible") {
					if (special === "both-invisible") {
						summoned.el.classList.add("active");
						SpyCards.flipCard(summoned.el, true);
						summoner.el.classList.add("fast-flip");
						SpyCards.flipCard(summoner.el, true);
						await fxSleep(250);
						SpyCards.UI.remove(summoner.el);
					}
					summoned.invisible = true;
				} else if (special === "replace") {
					summoned.el.classList.add("active");
					SpyCards.flipCard(summoned.el, true);
					summoner.el.classList.add("fast-flip");
					SpyCards.flipCard(summoner.el, true);
					await fxSleep(250);
					oldParent.insertBefore(summoned.el, oldNext);
					SpyCards.UI.remove(summoner.el);
					await fxSleep(10);
					SpyCards.flipCard(summoned.el, false);
				} else {
					summoned.el.classList.add("offscreen");
					if (summoner.el.parentNode) {
						summoner.el.parentNode.insertBefore(summoned.el, summoner.el.nextSibling);
					} else {
						fieldEl[summoned.player - 1].appendChild(summoned.el);
					}
					await fxSleep(10);
					summoned.el.classList.remove("offscreen");
				}
				if (summoner.player !== summoned.player && !summoned.invisible) {
					fieldEl[summoned.player - 1].appendChild(summoned.el);
				}
				doSquish(summoned.el);
			},
			async beforeAddEffects(card, effects, delayProcessing) {
			},
			async addEffect(card, effect, alreadyProcessed, fromSummon) {
			}
		};
	}

	const positionHolders: HTMLDivElement[] = [];
	const highlightedEffect: HTMLSpanElement[] = [];
	const coinsToCleanUp: HTMLDivElement[] = [];
	export const fx: ContextFx = {
		async coin(card: CardInstance, heads: boolean, isStat: boolean, index: number, total: number) {
			let coinsEl: HTMLDivElement = card.el.querySelector(".coins");
			if (!coinsEl) {
				coinsEl = document.createElement("div");
				coinsEl.classList.add("coins");
				coinsToCleanUp.push(coinsEl);
				card.el.appendChild(coinsEl);
			}
			const coinEl = document.createElement("span");
			coinEl.classList.add("coin", heads ? "heads" : "tails", "unknown");
			if (isStat) {
				coinEl.classList.add("atk-def");
			}
			coinEl.style.setProperty("--rand", String(Math.random()));
			coinsEl.appendChild(coinEl);
			await fxSleep(index === 0 ? 250 : 50);
			SpyCards.Audio.Sounds.Coin.play();
			coinEl.classList.remove("unknown");
			if (!isStat) {
				if (heads) {
					SpyCards.Audio.Sounds.AtkSuccess.play(0.75);
				} else {
					SpyCards.Audio.Sounds.AtkFail.play(0.75);
				}
			}
			if (index === total - 1) {
				await fxSleep(750);
			}
		},
		async numb(card: CardInstance, target: CardInstance) {
			SpyCards.flipCard(target.el, true);
			SpyCards.Audio.Sounds.Lazer.play();
			await fxSleep(250);
		},
		async setup(card: CardInstance) {
			SpyCards.Audio.Sounds.CardSound2.play();
		},
		async multiple(index: number) {
			await fxSleep(150 * index);
		},
		async assist() {
		},
		async beforeProcessEffect(card, effect, nested) {
			if (card.invisible) {
				if (!nested) {
					Audio.ignoreAudio++;
					fxSleep = () => Promise.resolve();
				}
				return;
			}

			let playSound = true;
			const isHidden = (!card.setup || card.originalDesc) && Effect.isHidden(card.card, effect);
			if (isHidden) {
				const flavor = card.card.effects.find((e) => e.type === EffectType.FlavorText && e.negate);
				if (lastFlavor !== flavor) {
					lastFlavor = flavor;
				} else {
					playSound = false;
				}
			} else if (lastFlavor) {
				lastFlavor = null;
				await fxSleep(250);
			}

			const el = card.el;

			if (!el.parentNode) {
				// removed by an effect like summon-rank; re-add as pseudo-setup card.
				el.classList.add("setup");
				SpyCards.flipCard(el, false);
				el.classList.remove("fast-flip");
				fieldEl[card.player - 1].appendChild(el);
			}

			const effectEl = card.setup && !card.originalDesc ? el.querySelector<HTMLSpanElement>(".card-desc > *") : findEffect(el.querySelector(".card-desc"), card.card.effects, effect);
			effectEl.classList.add("highlight");
			highlightedEffect.push(effectEl);

			if (!nested) {
				const positionHolderEl = document.createElement("div");
				positionHolderEl.classList.add("card");
				positionHolderEl.style.visibility = "hidden";
				positionHolders.push(positionHolderEl);

				el.parentNode.insertBefore(positionHolderEl, el);
				el.classList.add("active");
				if (playSound) {
					SpyCards.Audio.Sounds.CardSound2.play();
				}
			}
		},
		async afterProcessEffect(card, effect, nested) {
			if (card.invisible) {
				if (!nested) {
					Audio.ignoreAudio--;
					fxSleep = realFxSleep;
				}
				return;
			}

			if (!nested) {
				if (!lastFlavor) {
					await fxSleep(200);
				}

				const el = card.el;

				const positionHolderEl = positionHolders.pop();
				if (!el.parentNode && positionHolderEl.nextSibling) {
					// handle Carmina's replacement
					(<HTMLElement>positionHolderEl.nextSibling).classList.remove("active");
				}
				SpyCards.UI.remove(positionHolderEl);
				el.classList.remove("active");
			}

			const effectEl = highlightedEffect.pop();

			if (!lastFlavor) {
				await fxSleep(50);
			}

			effectEl.classList.remove("highlight");
		},
	};

	export function removeCoins() {
		for (let el of coinsToCleanUp) {
			SpyCards.UI.remove(el);
		}
		coinsToCleanUp.length = 0;
	}
}
