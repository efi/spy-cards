package sprites

// Audience member sprites.
var (
	audience     = loadSpriteSheet("img/room/audience.png", 100, 1024, 1024, false)
	audienceBlur = loadSpriteSheet("img/room/audience-blur.png", 100, 2048, 1024, false)

	Audience = [6][10]*Sprite{
		{
			// ant
			audience.sprite(3, 140, 110, 297, 0.5, 0),
			audience.sprite(116, 142, 110, 295, 0.5, 0),
			audience.sprite(230, 138, 110, 295, 0.5, 0),
			audience.sprite(347, 136, 110, 297, 0.5, 0),
			audienceBlurSprite(5, 281, 724, 110, 295, 0.5),
			audienceBlurSprite(5, 401, 722, 110, 297, 0.5),
			audienceBlurSprite(10, 896, 724, 110, 295, 0.5),
			audienceBlurSprite(10, 1026, 722, 110, 297, 0.5),
			audienceBlurSprite(15, 1521, 714, 110, 295, 0.5),
			audienceBlurSprite(15, 1661, 712, 110, 297, 0.5),
		},
		{
			// bee
			audience.sprite(517, 757, 126, 263, 0.5, 0),
			audience.sprite(645, 757, 125, 263, 0.5, 0),
			audience.sprite(772, 756, 125, 264, 0.5, 0),
			audience.sprite(899, 755, 125, 265, 0.5, 0),
			audienceBlurSprite(5, 5, 423, 125, 263, 0.5),
			audienceBlurSprite(5, 140, 421, 125, 265, 0.5),
			audienceBlurSprite(10, 600, 413, 125, 263, 0.5),
			audienceBlurSprite(10, 745, 411, 125, 265, 0.5),
			audienceBlurSprite(15, 1205, 393, 125, 263, 0.5),
			audienceBlurSprite(15, 1360, 391, 125, 265, 0.5),
		},
		{
			// beetle
			audience.sprite(0, 445, 142, 250, 0.5, 0),
			audience.sprite(144, 446, 143, 251, 0.5, 0),
			audience.sprite(290, 447, 142, 248, 0.5, 0),
			audience.sprite(144, 446, 143, 251, 0.5, 0), // same as non-cheering
			audienceBlurSprite(5, 275, 435, 143, 251, 0.5),
			audienceBlurSprite(5, 275, 435, 143, 251, 0.5),
			audienceBlurSprite(10, 890, 425, 143, 251, 0.5),
			audienceBlurSprite(10, 890, 425, 143, 251, 0.5),
			audienceBlurSprite(15, 1515, 405, 143, 251, 0.5),
			audienceBlurSprite(15, 1515, 405, 143, 251, 0.5),
		},
		{
			// moth
			audience.sprite(0, 701, 102, 323, 0.5, 0),
			audience.sprite(104, 701, 104, 323, 0.5, 0),
			audience.sprite(209, 701, 150, 323, 0.5, 0),
			audience.sprite(360, 701, 152, 323, 0.5, 0),
			audienceBlurSprite(5, 5, 696, 104, 323, 0.5),
			audienceBlurSprite(5, 119, 696, 152, 323, 0.5),
			audienceBlurSprite(10, 600, 696, 104, 323, 0.5),
			audienceBlurSprite(10, 724, 696, 152, 323, 0.5),
			audienceBlurSprite(15, 1205, 686, 104, 323, 0.5),
			audienceBlurSprite(15, 1339, 686, 152, 323, 0.5),
		},
		{
			// termite 1
			audience.sprite(907, 435, 113, 313, 0.5, 0),
			audience.sprite(791, 434, 112, 314, 0.5, 0),
			audience.sprite(673, 435, 111, 313, 0.5, 0),
			audience.sprite(556, 435, 113, 313, 0.5, 0),
			audienceBlurSprite(5, 5, 97, 112, 314, 0.5),
			audienceBlurSprite(5, 127, 98, 113, 313, 0.5),
			audienceBlurSprite(10, 600, 77, 112, 314, 0.5),
			audienceBlurSprite(10, 732, 78, 113, 313, 0.5),
			audienceBlurSprite(15, 1205, 47, 112, 314, 0.5),
			audienceBlurSprite(15, 1347, 48, 113, 313, 0.5),
		},
		{
			// termite 2
			audience.sprite(894, 99, 130, 327, 0.5, 0),
			audience.sprite(765, 98, 128, 330, 0.5, 0),
			audience.sprite(622, 100, 138, 326, 0.5240864, 0),
			audience.sprite(480, 100, 138, 328, 0.5136719, 0),
			audienceBlurSprite(5, 250, 81, 128, 330, 0.5240864),
			audienceBlurSprite(5, 388, 83, 138, 328, 0.5136719),
			audienceBlurSprite(10, 865, 61, 128, 330, 0.5240864),
			audienceBlurSprite(10, 1013, 63, 138, 328, 0.5136719),
			audienceBlurSprite(15, 1490, 31, 128, 330, 0.5240864),
			audienceBlurSprite(15, 1648, 33, 138, 328, 0.5136719),
		},
	}
)

func audienceBlurSprite(pad, x, y, w, h, px float32) *Sprite {
	px = (px*w + pad) / (w + pad + pad)
	py := pad / (h + pad + pad)

	return audienceBlur.sprite(x-pad, y-pad, w+pad+pad, h+pad+pad, px, py)
}

// Player character sprites.
var (
	characters1 = loadSpriteSheet("img/room/characters1.png", 100, 2048, 2048, false)

	Amber1      = characters1.sprite(3, 1765, 239, 279, 0.5, 0.0)
	Amber2      = characters1.sprite(249, 1765, 239, 278, 0.5, 0.0)
	Ambera      = characters1.sprite(493, 1766, 240, 277, 0.5, 0.0)
	Aria1       = characters1.sprite(738, 1771, 151, 272, 0.5, 0.0)
	Aria2       = characters1.sprite(893, 1771, 156, 271, 0.5, 0.0)
	Ariaa       = characters1.sprite(1057, 1752, 142, 291, 0.5, 0.0)
	Arie1       = characters1.sprite(1206, 1800, 187, 243, 0.5, 0.0)
	Arie2       = characters1.sprite(1399, 1801, 188, 241, 0.5, 0.0)
	Ariea       = characters1.sprite(1593, 1799, 160, 243, 0.5, 0.0)
	Astotheles1 = characters1.sprite(1761, 1700, 113, 342, 0.5, 0.0)
	Astotheles2 = characters1.sprite(1881, 1703, 111, 340, 0.5, 0.0)
	Astothelesa = characters1.sprite(1890, 1377, 140, 320, 0.5, 0.0)
	Bomby1      = characters1.sprite(4, 1572, 106, 188, 0.5, 0.0)
	Bomby2      = characters1.sprite(116, 1574, 109, 186, 0.5, 0.0)
	Bombya      = characters1.sprite(234, 1606, 103, 153, 0.5, 0.0)
	BuGi1       = characters1.sprite(343, 1364, 240, 395, 0.5, 0.0)
	BuGi2       = characters1.sprite(591, 1366, 240, 393, 0.5, 0.0)
	BuGia       = characters1.sprite(837, 1387, 135, 373, 0.5, 0.0)
	Carmina1    = characters1.sprite(976, 1477, 118, 283, 0.5, 0.0)
	Carmina2    = characters1.sprite(1096, 1466, 117, 283, 0.5, 0.0)
	Carminaa    = characters1.sprite(1218, 1547, 129, 247, 0.5, 0.0)
	Celia1      = characters1.sprite(1351, 1558, 113, 236, 0.5, 0.0)
	Celia2      = characters1.sprite(1470, 1559, 113, 234, 0.5, 0.0)
	Celiaa      = characters1.sprite(1588, 1567, 112, 220, 0.5, 0.0)
	Cenn1       = characters1.sprite(1729, 1422, 154, 272, 0.5, 0.0)
	Cenn2       = characters1.sprite(1730, 1143, 153, 271, 0.5, 0.0)
	Cenna       = characters1.sprite(1888, 1098, 153, 274, 0.5, 0.0)
	Cerise1     = characters1.sprite(4, 1407, 133, 162, 0.5, 0.0)
	Cerise2     = characters1.sprite(140, 1409, 136, 160, 0.5, 0.0)
	Cerisea     = characters1.sprite(5, 1243, 138, 161, 0.5, 0.0)
	Chompy1     = characters1.sprite(146, 1249, 94, 157, 0.5, 0.0)
	Chompy2     = characters1.sprite(244, 1253, 96, 154, 0.5, 0.0)
	Chompya     = characters1.sprite(7, 1179, 189, 60, 0.5, 0.0)
	Chubee1     = characters1.sprite(5, 883, 185, 285, 0.5, 0.0)
	Chubee2     = characters1.sprite(204, 948, 183, 283, 0.5, 0.0)
	Chubeea     = characters1.sprite(394, 1062, 189, 288, 0.5, 0.0)
	Chuck1      = characters1.sprite(588, 1167, 190, 191, 0.5, 0.0)
	Chuck2      = characters1.sprite(783, 1168, 191, 190, 0.5, 0.0)
	Chucka      = characters1.sprite(987, 1268, 250, 196, 0.5, 0.0)
	Crisbee1    = characters1.sprite(1252, 1249, 223, 289, 0.5, 0.0)
	Crisbee2    = characters1.sprite(1481, 1256, 221, 288, 0.5, 0.0)
	Crisbeea    = characters1.sprite(1257, 948, 223, 288, 0.5, 0.0)
	Crow1       = characters1.sprite(1496, 1035, 95, 207, 0.5, 0.0)
	Crow2       = characters1.sprite(1611, 1034, 97, 205, 0.5, 0.0)
	Crowa       = characters1.sprite(1714, 924, 110, 209, 0.5, 0.0)
	Diana1      = characters1.sprite(9, 569, 233, 300, 0.5, 0.0)
	Diana2      = characters1.sprite(248, 635, 232, 301, 0.5, 0.0)
	Dianaa      = characters1.sprite(483, 730, 238, 324, 0.5, 0.0)
	Eremi1      = characters1.sprite(722, 910, 131, 251, 0.5, 0.0)
	Eremi2      = characters1.sprite(856, 913, 134, 249, 0.5, 0.0)
	Eremia      = characters1.sprite(993, 908, 121, 255, 0.5, 0.0)
	Futes1      = characters1.sprite(1120, 923, 133, 315, 0.5, 0.0)
	Futes2      = characters1.sprite(1124, 607, 133, 314, 0.5, 0.0)
	Futesa      = characters1.sprite(1261, 625, 133, 319, 0.5, 0.0)
	Genow1      = characters1.sprite(1398, 723, 159, 222, 0.5, 0.0)
	Genow2      = characters1.sprite(1560, 726, 160, 218, 0.5, 0.0)
	Genowa      = characters1.sprite(1723, 704, 158, 218, 0.5, 0.0)
	Honeycomb1  = characters1.sprite(748, 721, 171, 175, 0.5, 0.0)
	Honeycomb2  = characters1.sprite(925, 725, 173, 173, 0.5, 0.0)
	Honeycomba  = characters1.sprite(922, 542, 170, 176, 0.5, 0.0)
	Janet1      = characters1.sprite(491, 465, 110, 250, 0.3622384, 0.0)
	Janet2      = characters1.sprite(605, 466, 110, 250, 0.3599749, 0.0)
	Janeta      = characters1.sprite(717, 466, 110, 250, 0.3664821, 0.0)
	Jaune1      = characters1.sprite(1907, 828, 122, 258, 0.5, 0.0)
	Jaune2      = characters1.sprite(1906, 562, 125, 256, 0.5, 0.0)
	Jaunea      = characters1.sprite(1929, 298, 99, 259, 0.5, 0.0)
	Jayde1      = characters1.sprite(1384, 364, 163, 332, 0.5, 0.0)
	Jayde2      = characters1.sprite(1552, 379, 166, 331, 0.5, 0.0)
	Jaydea      = characters1.sprite(1726, 358, 169, 332, 0.5, 0.0)
	Johnny1     = characters1.sprite(8, 287, 197, 279, 0.5, 0.0)
	Johnny2     = characters1.sprite(212, 284, 193, 277, 0.5, 0.0)
	Johnnya     = characters1.sprite(2, 2, 223, 280, 0.5, 0.0)
	Kabbu1      = characters1.sprite(246, 10, 144, 246, 0.5, 0.0)
	Kabbu2      = characters1.sprite(397, 14, 149, 246, 0.5, 0.0)
	Kabbua      = characters1.sprite(553, 14, 166, 240, 0.5, 0.0)
	Kage1       = characters1.sprite(728, 4, 188, 260, 0.5, 0.0)
	Kage2       = characters1.sprite(924, 6, 188, 258, 0.5, 0.0)
	Kagea       = characters1.sprite(1121, 8, 183, 260, 0.5, 0.0)
	Kali1       = characters1.sprite(1315, 14, 155, 321, 0.5, 0.0)
	Kali2       = characters1.sprite(1482, 18, 153, 319, 0.5, 0.0)
	Kalia       = characters1.sprite(1641, 13, 118, 325, 0.5, 0.0)
	Nero1       = characters1.sprite(433, 359, 79, 90, 0.5, 0.0)
	Nero2       = characters1.sprite(513, 360, 79, 88, 0.5, 0.0)
	Neroa       = characters1.sprite(594, 362, 77, 85, 0.5, 0.0)
	Pibu1       = characters1.sprite(423, 273, 75, 65, 0.5, 0.0)
	Pibu2       = characters1.sprite(500, 273, 77, 66, 0.5, 0.0)
	Pibua       = characters1.sprite(579, 274, 92, 64, 0.5, 0.0)
	Pisci1      = characters1.sprite(944, 278, 131, 204, 0.5, 0.0)
	Pisci2      = characters1.sprite(1087, 284, 135, 202, 0.5, 0.0)
	Piscia      = characters1.sprite(1227, 404, 143, 192, 0.5, 0.0)

	characters2 = loadSpriteSheet("img/room/characters2.png", 100, 2048, 2048, false)

	Kenny1    = characters2.sprite(401, 955, 154, 291, 0.5, 0.0)
	Kenny2    = characters2.sprite(572, 868, 154, 289, 0.5, 0.0)
	Kennya    = characters2.sprite(735, 867, 171, 291, 0.5, 0.0)
	Kina1     = characters2.sprite(9, 529, 151, 316, 0.5, 0.0)
	Kina2     = characters2.sprite(165, 530, 156, 315, 0.5, 0.0)
	Kinaa     = characters2.sprite(328, 620, 173, 319, 0.5, 0.0)
	Lanya1    = characters2.sprite(33, 268, 137, 248, 0.5, 0.0)
	Lanya2    = characters2.sprite(177, 269, 141, 246, 0.5, 0.0)
	Lanyaa    = characters2.sprite(322, 268, 127, 249, 0.5, 0.0)
	Leif1     = characters2.sprite(597, 628, 104, 222, 0.5, 0.0)
	Leif2     = characters2.sprite(707, 631, 104, 220, 0.5, 0.0)
	Leifa     = characters2.sprite(835, 586, 152, 236, 0.5, 0.0)
	Levi1     = characters2.sprite(1104, 1058, 122, 228, 0.5, 0.0)
	Levi2     = characters2.sprite(1103, 807, 121, 227, 0.5, 0.0)
	Levia     = characters2.sprite(1016, 579, 159, 224, 0.5, 0.0)
	Maki1     = characters2.sprite(1189, 1328, 109, 293, 0.5, 0.0)
	Maki2     = characters2.sprite(1314, 1328, 109, 290, 0.5, 0.0)
	Makia     = characters2.sprite(1258, 1041, 167, 284, 0.5, 0.0)
	Malbee1   = characters2.sprite(1463, 1572, 178, 261, 0.5, 0.0)
	Malbee2   = characters2.sprite(1464, 1308, 178, 261, 0.5, 0.0)
	Malbeea   = characters2.sprite(1441, 1051, 180, 252, 0.5, 0.0)
	Mar1      = characters2.sprite(1245, 687, 116, 342, 0.5, 0.0)
	Mar2      = characters2.sprite(1368, 688, 117, 343, 0.5, 0.0)
	Mara      = characters2.sprite(1493, 619, 117, 336, 0.5, 0.0)
	Mothiva1  = characters2.sprite(1703, 1473, 153, 217, 0.5, 0.0)
	Mothiva2  = characters2.sprite(1863, 1477, 154, 217, 0.5, 0.0)
	Mothivaa  = characters2.sprite(1671, 1253, 164, 212, 0.5, 0.0)
	Neolith1  = characters2.sprite(767, 295, 182, 273, 0.5, 0.0)
	Neolith2  = characters2.sprite(965, 288, 183, 271, 0.5, 0.0)
	Neolitha  = characters2.sprite(958, 3, 190, 279, 0.5, 0.0)
	Ritchee1  = characters2.sprite(3, 2, 144, 250, 0.5, 0.0)
	Ritchee2  = characters2.sprite(152, 3, 144, 248, 0.5, 0.0)
	Ritcheea  = characters2.sprite(298, 2, 150, 255, 0.5, 0.0)
	Riz1      = characters2.sprite(451, 4, 214, 283, 0.5, 0.0)
	Riz2      = characters2.sprite(669, 5, 214, 281, 0.5, 0.0)
	Riza      = characters2.sprite(452, 290, 305, 326, 0.5, 0.0)
	Samira1   = characters2.sprite(1634, 1007, 176, 220, 0.5, 0.0)
	Samira2   = characters2.sprite(1634, 786, 175, 218, 0.5, 0.0)
	Samiraa   = characters2.sprite(1640, 607, 165, 174, 0.5, 0.0)
	Scarlet1  = characters2.sprite(1864, 1145, 175, 278, 0.5, 0.0)
	Scarlet2  = characters2.sprite(1865, 866, 177, 276, 0.5, 0.0)
	Scarleta  = characters2.sprite(1814, 589, 228, 273, 0.5, 0.0)
	Serene1   = characters2.sprite(1193, 393, 125, 278, 0.5, 0.0)
	Serene2   = characters2.sprite(1323, 394, 130, 277, 0.5, 0.0)
	Serenea   = characters2.sprite(1465, 337, 101, 249, 0.5, 0.0)
	Shay1     = characters2.sprite(1570, 333, 123, 267, 0.5, 0.0)
	Shay2     = characters2.sprite(1698, 335, 126, 265, 0.5, 0.0)
	Shaya     = characters2.sprite(1829, 314, 216, 290, 0.5, 0.0)
	Skirby1   = characters2.sprite(2, 1855, 167, 191, 0.5, 0.0)
	Skirby2   = characters2.sprite(170, 1855, 167, 191, 0.5, 0.0)
	Skirbya   = characters2.sprite(338, 1856, 168, 190, 0.5, 0.0)
	Tanjerin1 = characters2.sprite(1151, 201, 136, 191, 0.5, 0.0)
	Tanjerin2 = characters2.sprite(1181, 6, 136, 188, 0.5, 0.0)
	Tanjerina = characters2.sprite(1297, 193, 160, 200, 0.5, 0.0)
	Tanjerinc = characters2.sprite(1323, 3, 135, 189, 0.5, 0.0)
	Ultimax1  = characters2.sprite(1464, 3, 184, 327, 0.5, 0.0)
	Ultimax2  = characters2.sprite(1651, 3, 184, 326, 0.5, 0.0)
	Ultimaxa  = characters2.sprite(1838, 4, 207, 306, 0.5, 0.0)
	Vanessa1  = characters2.sprite(911, 1700, 172, 320, 0.5, 0.0)
	Vanessa2  = characters2.sprite(1094, 1697, 174, 318, 0.5, 0.0)
	Vanessaa  = characters2.sprite(1282, 1694, 136, 323, 0.5, 0.0)
	Vi1       = characters2.sprite(99, 1527, 172, 189, 0.5, 0.0)
	Vi2       = characters2.sprite(276, 1529, 171, 190, 0.5, 0.0)
	Via       = characters2.sprite(450, 1531, 213, 192, 0.5, 0.0)
	Yin1      = characters2.sprite(220, 1252, 142, 268, 0.5, 0.0)
	Yin2      = characters2.sprite(364, 1253, 151, 266, 0.5, 0.0)
	Yina      = characters2.sprite(522, 1250, 157, 273, 0.5, 0.0)
	Zaryant1  = characters2.sprite(3, 890, 184, 284, 0.5, 0.0)
	Zaryant2  = characters2.sprite(206, 945, 184, 283, 0.5, 0.0)
	Zaryanta  = characters2.sprite(10, 1221, 200, 203, 0.5, 0.0)
	Zasp1     = characters2.sprite(682, 1300, 137, 301, 0.5, 0.0)
	Zasp2     = characters2.sprite(831, 1306, 134, 303, 0.5, 0.0)
	Zaspa     = characters2.sprite(975, 1308, 176, 299, 0.5, 0.0)
)

// Misc. room sprites.
var (
	CardBattle = loadSpriteSheet("img/room/cardbattle2.png", 25.6, 256, 256, false).sprite(0, 64, 256, 192, 0.5, 0.5)
)
