"use strict";
var SpyCards;
(function (SpyCards) {
    var Effect;
    (function (Effect) {
        Effect.order = {
            // no effect during round
            ignored: [
                { type: SpyCards.EffectType.TP },
                { type: SpyCards.EffectType.FlavorText },
            ],
            // before stats are displayed
            pre: [
                { type: SpyCards.EffectType.CondApply, late: false },
                { type: SpyCards.EffectType.CondLimit },
                { type: SpyCards.EffectType.CondHP, late: false },
                { type: SpyCards.EffectType.Summon, negate: true },
                { type: SpyCards.EffectType.Summon, negate: false },
                { type: SpyCards.EffectType.CondCoin },
                { type: SpyCards.EffectType.CondCard, opponent: false, each: false },
                { type: SpyCards.EffectType.Heal },
            ],
            // main part of round
            main: [
                { type: SpyCards.EffectType.Stat, opponent: false },
                { type: SpyCards.EffectType.CondCard, opponent: true, each: false },
                { type: SpyCards.EffectType.CondCard, each: true },
                { type: SpyCards.EffectType.Empower },
                { type: SpyCards.EffectType.CondStat, late: false },
                { type: SpyCards.EffectType.Numb },
                { type: SpyCards.EffectType.Stat, opponent: true },
                { type: SpyCards.EffectType.CondStat, late: true },
            ],
            // after winner is computed
            post: [
                { type: SpyCards.EffectType.CondWinner },
                { type: SpyCards.EffectType.CondHP, late: true },
                { type: SpyCards.EffectType.CondApply, late: true },
            ],
            // triggered by other effects; ignore if still present
            discard: [
                { type: SpyCards.EffectType.CondOnNumb },
            ],
        };
        function filterTargets(ctx, card, effect, allowNumb = false) {
            return ctx.state.played[effect.opponent ? 2 - card.player : card.player - 1].filter((c) => {
                if ((!allowNumb && c.numb) || c.setup) {
                    return false;
                }
                if (ctx.defs.unfilter[c.card.id]) {
                    return false;
                }
                if (!effect.generic) {
                    return effect.card === c.card.id;
                }
                if (effect.rank !== SpyCards.Rank.None) {
                    const rank = c.card.rank();
                    if (effect.rank === SpyCards.Rank.Enemy) {
                        if (rank !== SpyCards.Rank.Attacker && rank !== SpyCards.Rank.Effect) {
                            return false;
                        }
                    }
                    else if (effect.rank !== rank) {
                        return false;
                    }
                }
                if (effect.tribe === SpyCards.Tribe.None) {
                    return true;
                }
                if (effect.tribe === SpyCards.Tribe.Custom) {
                    return c.card.tribes.some((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === effect.customTribe);
                }
                return c.card.tribes.some((t) => t.tribe === effect.tribe);
            });
        }
        async function dispatch(ctx, card, effect) {
            let ok = null;
            let stat;
            let diff;
            let player;
            let targets;
            switch (effect.type) {
                case SpyCards.EffectType.FlavorText:
                    // no effect
                    break;
                case SpyCards.EffectType.Stat:
                    stat = effect.defense ? "DEF" : "ATK";
                    diff = effect.negate ? -effect.amount : effect.amount;
                    player = (effect.opponent ? 3 - card.player : card.player);
                    await ctx.effect.modifyStat(card, effect.opponent || card.becomingNumb ? null : card, effect, player, stat, diff);
                    break;
                case SpyCards.EffectType.Empower:
                    stat = effect.defense ? "DEF" : "ATK";
                    diff = effect.negate ? -effect.amount : effect.amount;
                    player = (effect.opponent ? 3 - card.player : card.player);
                    targets = filterTargets(ctx, card, effect);
                    for (let c of targets) {
                        await ctx.effect.modifyStat(card, c, effect, player, stat, diff);
                    }
                    break;
                case SpyCards.EffectType.Summon:
                    const special = effect.defense ? effect.negate ? "both-invisible" : "invisible" :
                        effect.negate ? "replace" : null;
                    player = (effect.opponent ? 3 - card.player : card.player);
                    const toSummon = [];
                    for (let i = 0; i < effect.amount; i++) {
                        if (effect.generic) {
                            toSummon.push(await randomMatchingCard(ctx, effect));
                        }
                        else {
                            toSummon.push(ctx.defs.cardsByID[effect.card]);
                        }
                    }
                    if (effect.negate) {
                        card.setup = true;
                    }
                    const toSummonWait = toSummon.filter(Boolean).map((c, i) => ({ c, w: ctx.fx.multiple(i) }));
                    for (let { c, w } of toSummonWait) {
                        await w;
                        await ctx.effect.summonCard(player, card, c, special);
                    }
                    break;
                case SpyCards.EffectType.Heal:
                    player = (effect.opponent ? 3 - card.player : card.player);
                    diff = effect.negate ? -effect.amount : effect.amount;
                    if (effect.each) {
                        await ctx.effect.multiplyHealing(card, effect, player, diff, effect.generic ? null : !effect.defense);
                    }
                    else {
                        await ctx.effect.heal(card, effect, player, diff);
                    }
                    break;
                case SpyCards.EffectType.TP:
                    // no effect
                    break;
                case SpyCards.EffectType.Numb:
                    for (let i = 0; i < effect.amount; i++) {
                        let lowest = null;
                        let lowestATK = 0;
                        let lowestDEF = 0;
                        let lowestMin = 0;
                        let lowestMax = 0;
                        targets = filterTargets(ctx, card, effect);
                        for (let c of targets) {
                            if (c.numb || c.setup) {
                                continue;
                            }
                            const atk = c.modifiedATK || 0;
                            const def = c.modifiedDEF || 0;
                            const min = Math.min(atk, def);
                            const max = Math.max(atk, def);
                            if (lowest && atk === 0 && def === 0) {
                                continue;
                            }
                            if (!lowest || (lowestATK === 0 && lowestDEF === 0 && (atk !== 0 || def !== 0))) {
                                lowest = c;
                                lowestATK = atk;
                                lowestDEF = def;
                                lowestMin = min;
                                lowestMax = max;
                                continue;
                            }
                            if (max > lowestMax) {
                                continue;
                            }
                            if (max === lowestMax && min > lowestMin) {
                                continue;
                            }
                            if (max === lowestMax && lowestMax == lowestATK) {
                                continue;
                            }
                            lowest = c;
                            lowestATK = atk;
                            lowestDEF = def;
                            lowestMin = min;
                            lowestMax = max;
                        }
                        if (!lowest) {
                            break;
                        }
                        lowest.becomingNumb = true;
                        for (let i = 0; i < lowest.effects.length; i++) {
                            if (lowest.effects[i].type === SpyCards.EffectType.CondOnNumb) {
                                const trigger = lowest.effects.splice(i, 1)[0];
                                ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[trigger.result.type] + " for player " + lowest.player + " card " + lowest.card.displayName() + " (due to numb)");
                                await ctx.fx.beforeProcessEffect(lowest, trigger);
                                await Effect.dispatch(ctx, lowest, trigger.result);
                                await ctx.fx.afterProcessEffect(lowest, trigger);
                                i--;
                            }
                        }
                        lowest.numb = true;
                        await ctx.effect.recalculateStats((3 - card.player));
                        await ctx.fx.numb(card, lowest);
                    }
                    break;
                case SpyCards.EffectType.CondCard:
                    targets = filterTargets(ctx, card, effect, true);
                    ctx.state.gameLog.log("Matching cards (" + targets.length + "): " + targets.map((c) => c.card.displayName()).join(", "));
                    if (effect.each) {
                        for (let c of targets) {
                            await ctx.fx.assist(card, c);
                            await ctx.effect.applyResult(card, effect, effect.result);
                        }
                    }
                    else {
                        ok = effect.negate ? targets.length < effect.amount : targets.length >= effect.amount;
                    }
                    break;
                case SpyCards.EffectType.CondLimit:
                    const count = ctx.state.once[card.player - 1].reduce((n, e) => e === effect ? n + 1 : n, 0);
                    ok = effect.negate ?
                        count >= effect.amount :
                        count < effect.amount;
                    ctx.state.once[card.player - 1].push(effect);
                    break;
                case SpyCards.EffectType.CondWinner:
                    if (effect.negate) {
                        ok = effect.opponent ?
                            !!ctx.state.winner :
                            !ctx.state.winner;
                    }
                    else {
                        ok = effect.opponent ?
                            ctx.state.winner === 3 - card.player :
                            ctx.state.winner === card.player;
                    }
                    break;
                case SpyCards.EffectType.CondApply:
                    player = (effect.opponent ? 3 - card.player : card.player);
                    if (effect.late) {
                        if (effect.negate) {
                            let found = false;
                            for (let s of ctx.state.setup[player - 1]) {
                                if (s.sourceCard === card && s.originalDesc) {
                                    s.effects.push(effect.result);
                                    found = true;
                                    break;
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                        ctx.state.setup[player - 1].push({
                            card: card.card,
                            effects: [effect.result],
                            originalDesc: effect.negate,
                            sourceCard: card
                        });
                        await ctx.fx.setup(card);
                    }
                    else {
                        await ctx.effect.summonCard(player, card, card.card, effect.negate ? "setup-original" : "setup-effect", [effect.result]);
                    }
                    break;
                case SpyCards.EffectType.CondCoin:
                    const coins = [];
                    for (let i = 0; i < effect.amount; i++) {
                        coins.push(await ctx.scg.rand(true, 2) !== 0);
                    }
                    for (let i = 0; i < coins.length; i++) {
                        await ctx.fx.coin(card, effect.negate ? !coins[i] : coins[i], effect.defense, i, coins.length);
                    }
                    for (let flip of coins) {
                        ctx.state.gameLog.log("Player " + card.player + " card " + card.card.displayName() + " coin lands on " + (flip ? "heads" : "tails"));
                        if (flip) {
                            await ctx.effect.applyResult(card, effect, effect.result);
                        }
                        else if (effect.generic) {
                            await ctx.effect.applyResult(card, effect, effect.tailsResult);
                        }
                    }
                    break;
                case SpyCards.EffectType.CondHP:
                    const hp = ctx.state.hp[effect.opponent ? 2 - card.player : card.player - 1];
                    ok = effect.negate ? hp < effect.amount : hp >= effect.amount;
                    break;
                case SpyCards.EffectType.CondStat:
                    stat = effect.defense ? "DEF" : "ATK";
                    const value = ctx.state[stat][effect.opponent ? 2 - card.player : card.player - 1];
                    ok = effect.negate ? value < effect.amount : value >= effect.amount;
                    break;
                case SpyCards.EffectType.CondPriority:
                    ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[effect.result.type] + " at priority of " + SpyCards.EffectType[effect.amount]);
                    await Effect.dispatch(ctx, card, effect.result);
                    break;
                case SpyCards.EffectType.CondOnNumb:
                    // no effect when run from here
                    break;
                default:
                    throw new Error("unhandled effect type " + effect.type + " (" + SpyCards.EffectType[effect.type] + ")");
            }
            if (ok !== null) {
                ctx.state.gameLog.log("Condition is " + (ok ? "met." : "NOT met."));
            }
            if (ok) {
                await ctx.effect.applyResult(card, effect, effect.result);
            }
        }
        Effect.dispatch = dispatch;
        function isHidden(card, effect) {
            const firstHide = card.effects.findIndex((e) => e.type === SpyCards.EffectType.FlavorText && e.negate) + 1;
            if (firstHide <= 0) {
                return false;
            }
            for (let i = firstHide; i < card.effects.length; i++) {
                if (isInTree(card.effects[i], effect)) {
                    return true;
                }
            }
            return false;
        }
        Effect.isHidden = isHidden;
        function isInTree(tree, effect) {
            if (tree === effect) {
                return true;
            }
            if (tree.result && isInTree(tree.result, effect)) {
                return true;
            }
            if (tree.tailsResult && isInTree(tree.tailsResult, effect)) {
                return true;
            }
            return false;
        }
        async function randomMatchingCard(ctx, effect) {
            let choices;
            if (effect.tribe === SpyCards.Tribe.None && effect.negate) {
                ctx.state.gameLog.log("Selecting a random " + (effect.rank === SpyCards.Rank.None ? "" : SpyCards.rankName(effect.rank)) + " card that does not have a Carmina-style summon...");
                choices = ctx.defs.nonRandom[effect.rank].filter((c) => !ctx.defs.banned[c.id]);
            }
            else {
                ctx.state.gameLog.log("Selecting a random card with filters: Rank: " + SpyCards.rankName(effect.rank) + " Tribe: " + SpyCards.tribeName(effect.tribe, effect.customTribe));
                choices = ctx.defs.getAvailableCards(effect.rank, effect.tribe, effect.customTribe);
            }
            if (!choices.length) {
                ctx.state.gameLog.log("(no cards are available for this filter)");
                return null;
            }
            const index = await ctx.scg.rand(true, choices.length);
            ctx.state.gameLog.log("Selected card " + (index + 1) + " of " + choices.length + ": " + choices[index].displayName());
            return choices[index];
        }
    })(Effect = SpyCards.Effect || (SpyCards.Effect = {}));
})(SpyCards || (SpyCards = {}));
