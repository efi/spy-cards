//go:generate stringer -type entityType -trimprefix entity
//go:generate stringer -type action -trimprefix action

package miteknight

import (
	"image/color"

	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

type entityType uint8

const (
	entityAnt entityType = iota
	entityWizard
	entityFireball
	entityRandomEnemy
	entityPotion
	entityKey
	entityDoor
	entityPlayer
)

type action uint8

const (
	actionNone action = iota
	actionAttacking
	actionTurning
	actionMoving
)

var entityColor = [...]color.RGBA{
	entityAnt:         sprites.Red,
	entityWizard:      {127, 0, 165, 255},
	entityFireball:    sprites.Yellow,
	entityRandomEnemy: sprites.White,
	entityPotion:      sprites.Green,
	entityKey:         sprites.Yellow,
	entityDoor:        sprites.Yellow,
	entityPlayer:      {127, 127, 255, 255},
}

var entityScale = [...]float32{
	entityAnt:         1,
	entityWizard:      1,
	entityFireball:    1,
	entityRandomEnemy: 1,
	entityPotion:      1,
	entityKey:         1,
	entityDoor:        100.0 / 64.0,
	entityPlayer:      0.75,
}

type entity struct {
	height         float32
	dx, dz         float32
	rotationZ      float32
	child          *entity
	cooldown       int
	iframes        int
	actionCooldown int
	sprite         *sprites.Sprite
	tempBounce     uint64
	moving         uint64
	playerMoving   uint64
	moveInterrupt  uint64
	blinkStart     uint64
	blinkDelay     uint64
	hp             int
	maxhp          int
	x, z           int
	moveInterruptX int
	moveInterruptZ int
	bounce         float32
	entityType     entityType
	direction      direction
	action         action
	flipX          bool
	blocking       bool
	dead           bool
	dying          bool
	special        bool
	billboard      bool
	fog            bool
}

func (e *entity) ox() float32 {
	if e.moving >= 2 {
		return float32(e.moving-2) / 15 * e.dx
	}

	return 0
}

func (e *entity) oz() float32 {
	if e.entityType == entityDoor {
		return -0.5
	}

	if e.moving >= 2 {
		return float32(e.moving-2) / 15 * e.dz
	}

	return 0
}

func (e *entity) processMove(dungeon *MiteKnight) {
	if e.moveInterrupt != 0 {
		e.moveInterrupt--
		if e.moveInterrupt == 1 {
			e.processMoveEnd(dungeon, e.moveInterruptX, e.moveInterruptZ)
		}
	}

	if e.playerMoving != 0 {
		e.playerMoving--
	}

	if e.moving != 0 {
		e.moving--
		if e.moving == 1 {
			e.processMoveEnd(dungeon, e.x, e.z)
		}
	}
}

func (e *entity) processMoveEnd(dungeon *MiteKnight, x, z int) {
	floor := dungeon.floors[dungeon.floor]

	if e.entityType == entityPlayer && e.hp > 0 {
		itemType := floor.floorMap[x][z]

		if itemType == tileKey {
			audio.MKKey.PlaySound(0, 0, 0)

			floor.hasKey = true
			floor.gotKey = true

			floor.floorMap[x][z] = tileFree
			floor.destroyItem(x, z)

			dungeon.state.Score += dungeon.rules.ScorePerKey
		} else if itemType == tilePotion && e.hp < e.maxhp {
			audio.MKPotion.PlaySound(0, 0, 0)

			e.hp++

			floor.floorMap[x][z] = tileFree
			floor.destroyItem(x, z)
		}
	}

	if e.dying {
		e.dying = false
		floor.spawnParticleSystem(deathSmoke, float32(x)+e.ox(), e.height, float32(z)+e.oz(), 0.25)
	}
}
