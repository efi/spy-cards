package gui

type Rect struct {
	x, y, w, h float32
}

func (r Rect) Position() (float32, float32) {
	return r.x, r.y
}

func (r Rect) Size() (float32, float32) {
	return r.w, r.h
}

func (r *Rect) SetPosition(x, y float32) {
	r.x = x
	r.y = y
}

func (r *Rect) SetSize(w, h float32) {
	r.w = w
	r.h = h
}
