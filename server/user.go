package server

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"crypto/subtle"
	"database/sql"
	"encoding/base64"
	"encoding/binary"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
	"golang.org/x/xerrors"
)

var (
	tmplPublicLanding = template.Must(template.New("landing").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Online</title>
<meta charset="utf-8">
</head>
<body>
{{if .User -}}
<p>Logged in as <strong>{{.User.LoginName}}</strong></p>
<ul>
<li><a href="change-password">Change Password</a></li>
{{if .User.IsAdmin -}}
<li><a href="admin/">Admin Panel</a></li>
{{end -}}
<li><a href="log-out">Log Out</a></li>
</ul>
{{if .Modes -}}
<h2>Game Modes</h2>
<table border="1">
<thead>
<tr>
<th>Client</th>
<th>Full Title</th>
</tr>
</thead>
<tbody>
{{range .Modes -}}
<tr>
<th><a href="custom/edit-mode/{{.ID}}">{{.Client}}</a></th>
<td>{{.Name}}</td>
</tr>
{{end -}}
</tbody>
</table>
{{end -}}
{{else -}}
<p><a href="log-in">Log In</a></p>
{{end -}}
</body>
</html>
`))
	tmplLogin = template.Must(template.New("log-in").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Online - Log In</title>
<meta charset="utf-8">
</head>
<body>
<form method="post">
<p><label>Login Name:<br><input type="username" autocomplete="username" required name="login-name"></label></p>
<p><label>Password:<br><input type="password" autocomplete="current-password" required name="password"></label></p>
<p><button type="submit">Log In</button></p>
<input type="hidden" name="csrf-token" value="{{.CSRFToken}}">
</form>
</body>
</html>
`))
	tmplLogout = template.Must(template.New("log-out").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Online - Log Out</title>
<meta charset="utf-8">
</head>
<body>
<form method="post">
<p><button type="submit">Log Out</button></p>
<input type="hidden" name="csrf-token" value="{{.CSRFToken}}">
</form>
</body>
</html>
`))
	tmplChangePassword = template.Must(template.New("change-password").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Online - Change Password</title>
<meta charset="utf-8">
</head>
<body>
<form method="post">
<p><label>Current Password:<br><input type="password" autocomplete="current-password" required name="current-password"></label></p>
<p><label>New Password:<br><input type="password" autocomplete="new-password" required name="new-password"></label></p>
<p><label>(again)<br><input type="password" autocomplete="new-password" required name="new-password2"></label></p>
<p><button type="submit">Change Password</button></p>
<input type="hidden" name="csrf-token" value="{{.CSRFToken}}">
</form>
</body>
</html>
`))
)

var sessionKey = func() []byte {
	b, err := ioutil.ReadFile("/spy-cards-session-key")
	if err != nil {
		panic(err)
	}

	return b
}()

// User is a Spy Cards Online user account.
type User struct {
	ID           int64
	LoginName    string
	password     []byte
	SessionReset int
	IsAdmin      bool
}

type currentUserKey struct{}

func spyCardsLanding(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")

	if r.URL.Path != "/spy-cards/" {
		http.NotFound(w, r)

		return
	}

	var data struct {
		User  *User
		Modes []GameMode
	}

	data.User, _ = getCurrentUser(&r)
	if data.User != nil {
		rows, err := getCustomGameModesForOwner.Query(data.User.ID)
		if err != nil {
			log.Println("get custom game modes for user", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
		defer rows.Close()

		for rows.Next() {
			var mode GameMode
			if err := rows.Scan(&mode.ID, &mode.Client, &mode.Name); err != nil {
				log.Println("get custom game modes for user", err)
				http.Error(w, "database error", http.StatusInternalServerError)

				return
			}

			data.Modes = append(data.Modes, mode)
		}

		if err := rows.Err(); err != nil {
			log.Println("get custom game modes for user", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplPublicLanding.Execute(w, &data)
}

func setLoginCookie(w http.ResponseWriter, u *User) {
	buf := make([]byte, sha256.Size+1+binary.MaxVarintLen64*2)
	n1 := binary.PutUvarint(buf[sha256.Size+1:], uint64(u.ID))
	n2 := binary.PutUvarint(buf[sha256.Size+1+n1:], uint64(u.SessionReset))
	buf = buf[:sha256.Size+1+n1+n2]
	hash := hmac.New(sha256.New, sessionKey)
	_, _ = hash.Write(buf[sha256.Size:])
	copy(buf, hash.Sum(nil))

	http.SetCookie(w, &http.Cookie{
		Name:     "spy_cards_login",
		Value:    base64.RawStdEncoding.EncodeToString(buf),
		MaxAge:   86400 * 30,
		SameSite: http.SameSiteStrictMode,
		HttpOnly: true,
		Secure:   true,
	})
}

func getCurrentUser(r **http.Request) (*User, error) {
	u, ok := (*r).Context().Value(currentUserKey{}).(*User)
	if ok {
		return u, nil
	}

	c, err := (*r).Cookie("spy_cards_login")
	if err != nil {
		// no cookie
		return nil, nil
	}

	b, err := base64.RawStdEncoding.DecodeString(c.Value)
	if err != nil {
		// only propagate database errors
		return nil, nil
	}

	if len(b) <= sha256.Size {
		// too short to be valid
		return nil, nil
	}

	hash := hmac.New(sha256.New, sessionKey)
	_, _ = hash.Write(b[sha256.Size:])

	if subtle.ConstantTimeCompare(hash.Sum(nil), b[:sha256.Size]) != 1 {
		return nil, nil
	}

	b = b[sha256.Size:]
	if b[0] != 0 {
		return nil, nil
	}

	b = b[1:]
	userID, n := binary.Uvarint(b)

	if n <= 0 {
		return nil, nil
	}

	b = b[n:]
	sessionReset, n := binary.Uvarint(b)

	if n != len(b) {
		return nil, nil
	}

	u = &User{
		ID: int64(userID),
	}

	switch err := getUserByID.QueryRow(userID).Scan(&u.LoginName, &u.SessionReset, &u.IsAdmin); {
	case xerrors.Is(err, sql.ErrNoRows):
		u = nil
	case err != nil:
		return nil, xerrors.Errorf("server: getting current user: %w", err)
	case u.SessionReset != int(sessionReset):
		u = nil
	}

	*r = (*r).WithContext(context.WithValue((*r).Context(), currentUserKey{}, u))

	return u, nil
}

func loginPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	cu, _ := getCurrentUser(&r)
	if cu != nil {
		onLoggedIn(w, r)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		loginName := r.FormValue("login-name")
		password := []byte(r.FormValue("password"))

		u := &User{
			LoginName: loginName,
		}

		err = getUserByLogin.QueryRow(loginName).Scan(&u.ID, &u.password, &u.SessionReset, &u.IsAdmin)
		if err == nil {
			err = bcrypt.CompareHashAndPassword(u.password, password)
		}

		if err != nil {
			http.Error(w, "login failed", http.StatusBadRequest)

			return
		}

		setLoginCookie(w, u)

		onLoggedIn(w, r)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	csrfToken, err := createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplLogin.Execute(w, &struct {
		CSRFToken string
	}{
		CSRFToken: csrfToken,
	})
}

func logoutPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	cu, _ := getCurrentUser(&r)
	if cu == nil {
		http.Redirect(w, r, "/spy-cards/", http.StatusFound)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login",
			Value:    "",
			Expires:  time.Unix(1, 0),
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})

		http.Redirect(w, r, "/spy-cards/", http.StatusFound)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	csrfToken, err := createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplLogout.Execute(w, &struct {
		CSRFToken string
	}{
		CSRFToken: csrfToken,
	})
}

func changePasswordPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	cu, _ := getCurrentUser(&r)
	if cu == nil {
		http.Error(w, "must be logged in to use this page", http.StatusForbidden)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		currentPassword := []byte(r.FormValue("current-password"))
		newPassword := []byte(r.FormValue("new-password"))
		newPassword2 := []byte(r.FormValue("new-password2"))

		if !bytes.Equal(newPassword, newPassword2) {
			http.Error(w, "new password did not match confirmation", http.StatusBadRequest)

			return
		}

		var currentHash []byte
		if err := getUserCurrentPassword.QueryRow(cu.ID).Scan(&currentHash); err != nil {
			log.Println("change password get current", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if err = bcrypt.CompareHashAndPassword(currentHash, currentPassword); err != nil {
			http.Error(w, "current password did not match", http.StatusForbidden)

			return
		}

		newHash, err := bcrypt.GenerateFromPassword(newPassword, bcrypt.DefaultCost)
		if err != nil {
			http.Error(w, "password crypt error", http.StatusInternalServerError)

			return
		}

		_, err = updateUserPassword.Exec(cu.ID, currentHash, newHash)
		if err != nil {
			log.Println("change password update", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		cu.SessionReset++

		setLoginCookie(w, cu)
		http.Redirect(w, r, "/spy-cards/", http.StatusFound)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	csrfToken, err := createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplChangePassword.Execute(w, &struct {
		CSRFToken string
	}{
		CSRFToken: csrfToken,
	})
}

func getCSRFKey(w http.ResponseWriter, r *http.Request, create bool) ([]byte, error) {
	csrfToken, err := r.Cookie("spy_cards_csrf_token")
	if err != nil && !create {
		return nil, xerrors.New("missing CSRF token")
	}

	if csrfToken != nil {
		b, err := base64.RawStdEncoding.DecodeString(csrfToken.Value)
		if len(b) == 32 && err == nil {
			return b, nil
		}

		if !create {
			return nil, xerrors.New("invalid CSRF token")
		}
	}

	b := make([]byte, 32)

	_, err = rand.Read(b)
	if err != nil {
		return nil, xerrors.Errorf("server: getting random data for CSRF token: %w", err)
	}

	http.SetCookie(w, &http.Cookie{
		Name:     "spy_cards_csrf_token",
		Value:    base64.RawStdEncoding.EncodeToString(b),
		SameSite: http.SameSiteStrictMode,
		Secure:   true,
		HttpOnly: true,
	})

	return b, nil
}

func createCSRFToken(b []byte) (string, error) {
	buf := make([]byte, 64)

	_, err := rand.Read(buf[:32])
	if err != nil {
		return "", xerrors.Errorf("server: getting random data for CSRF token: %w", err)
	}

	for i := 0; i < 32; i++ {
		buf[i+32] = buf[i] ^ b[i]
	}

	return base64.RawStdEncoding.EncodeToString(buf), nil
}

func verifyCSRFToken(b []byte, token string) bool {
	buf, err := base64.RawStdEncoding.DecodeString(token)
	if err != nil || len(buf) != 64 {
		return false
	}

	for i := 0; i < 32; i++ {
		buf[i+32] ^= buf[i]
	}

	return bytes.Equal(buf[32:], b)
}

func onLoggedIn(w http.ResponseWriter, r *http.Request) {
	redirectTo, err := r.Cookie("spy_cards_login_redirect")
	if err == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    "",
			Expires:  time.Unix(1, 0),
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})

		if strings.HasPrefix(redirectTo.Value, "/spy-cards/") {
			http.Redirect(w, r, redirectTo.Value, http.StatusFound)

			return
		}
	}

	http.Redirect(w, r, "/spy-cards/", http.StatusFound)
}
