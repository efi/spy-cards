// +build !headless

package gfx

import (
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

// Program is an OpenGL shader.
type Program struct {
	Name     string
	vs, fs   string
	Program  gl.Program
	uniforms []*Uniform
	attribs  []*Attrib
}

var programs []*Program

// Shader registers an OpenGL shader.
func Shader(name, vertex, fragment string) *Program {
	p := &Program{Name: name, vs: vertex, fs: fragment}

	Lock.Lock()

	programs = append(programs, p)

	if GL != nil {
		p.init()
	}

	Lock.Unlock()

	return p
}

func (p *Program) init() {
	vertex := GL.CreateShader(gl.VERTEX_SHADER)
	defer GL.DeleteShader(vertex)

	fragment := GL.CreateShader(gl.FRAGMENT_SHADER)
	defer GL.DeleteShader(fragment)

	GL.ShaderSource(vertex, p.vs)
	GL.ShaderSource(fragment, p.fs)

	GL.CompileShader(vertex)
	GL.CompileShader(fragment)

	p.Program = GL.CreateProgram()
	GL.AttachShader(p.Program, vertex)
	GL.AttachShader(p.Program, fragment)

	GL.LinkProgram(p.Program)

	if GL.GetProgrami(p.Program, gl.LINK_STATUS) == 0 {
		vert := GL.GetShaderInfoLog(vertex)
		frag := GL.GetShaderInfoLog(fragment)
		link := GL.GetProgramInfoLog(p.Program)
		panic(xerrors.Errorf("gl: failed to link shader for %q:\n\nvertex:\n%s\n\nfragment:\n%s\n\nlink:\n%s", p.Name, vert, frag, link))
	}

	for _, u := range p.uniforms {
		u.Uniform = GL.GetUniformLocation(p.Program, u.Name)
	}

	for _, a := range p.attribs {
		a.Attrib = GL.GetAttribLocation(p.Program, a.Name)
	}
}

func (p *Program) release() {
	GL.DeleteProgram(p.Program)

	p.Program = gl.Program{}
}

// Uniform is a global variable.
type Uniform struct {
	Uniform gl.Uniform
	Name    string
	program *Program
}

// Attrib is a vertex attribute.
type Attrib struct {
	Attrib  gl.Attrib
	Name    string
	program *Program
}

// Uniform gets the location of a global variable.
func (p *Program) Uniform(name string) *Uniform {
	u := &Uniform{
		Name:    name,
		program: p,
	}

	if p.Program.Init {
		u.Uniform = GL.GetUniformLocation(p.Program, name)
	}

	p.uniforms = append(p.uniforms, u)

	return u
}

// Attrib gets the location of a vertex attribute.
func (p *Program) Attrib(name string) *Attrib {
	a := &Attrib{
		Name:    name,
		program: p,
	}

	if p.Program.Init {
		a.Attrib = GL.GetAttribLocation(p.Program, name)
	}

	p.attribs = append(p.attribs, a)

	return a
}
