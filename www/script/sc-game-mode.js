"use strict";
var SpyCards;
(function (SpyCards) {
    class GameModeData {
        constructor() {
            this.fields = [];
        }
        get(type) {
            return this.fields.find((f) => f.getType() === type);
        }
        getAll(type) {
            return this.fields.filter((f) => f.getType() === type);
        }
        marshal(buf) {
            SpyCards.Binary.pushUVarInt(buf, 3); // format version
            SpyCards.Binary.pushUVarInt(buf, this.fields.length);
            for (let field of this.fields) {
                field.marshal(buf);
            }
        }
        unmarshal(buf) {
            const formatVersion = SpyCards.Binary.shiftUVarInt(buf);
            if (formatVersion !== 3) {
                throw new Error("unexpected game mode format version: " + formatVersion);
            }
            const fieldCount = SpyCards.Binary.shiftUVarInt(buf);
            this.fields = new Array(fieldCount);
            for (let i = 0; i < fieldCount; i++) {
                this.fields[i] = GameModeField.unmarshal(buf);
            }
            if (buf.length) {
                throw new Error("extra data after end of game mode");
            }
        }
    }
    SpyCards.GameModeData = GameModeData;
    let GameModeFieldType;
    (function (GameModeFieldType) {
        GameModeFieldType[GameModeFieldType["Metadata"] = 0] = "Metadata";
        GameModeFieldType[GameModeFieldType["BannedCards"] = 1] = "BannedCards";
        GameModeFieldType[GameModeFieldType["GameRules"] = 2] = "GameRules";
        GameModeFieldType[GameModeFieldType["SummonCard"] = 3] = "SummonCard";
        GameModeFieldType[GameModeFieldType["Variant"] = 4] = "Variant";
        GameModeFieldType[GameModeFieldType["UnfilterCard"] = 5] = "UnfilterCard";
        GameModeFieldType[GameModeFieldType["DeckLimitFilter"] = 6] = "DeckLimitFilter";
        GameModeFieldType[GameModeFieldType["Timer"] = 7] = "Timer";
    })(GameModeFieldType = SpyCards.GameModeFieldType || (SpyCards.GameModeFieldType = {}));
    class GameModeField {
        marshal(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.getType());
            const data = [];
            this.marshalData(data);
            SpyCards.Binary.pushUVarInt(buf, data.length);
            buf.push(...data);
        }
        static unmarshal(buf) {
            const type = SpyCards.Binary.shiftUVarInt(buf);
            if (!SpyCards.GameModeFieldConstructors[type]) {
                throw new Error("unknown game mode field type " + type);
            }
            const field = SpyCards.GameModeFieldConstructors[type]();
            const dataLength = SpyCards.Binary.shiftUVarInt(buf);
            if (buf.length < dataLength) {
                throw new Error("game mode field extends past end of buffer");
            }
            const data = buf.splice(0, dataLength);
            field.unmarshalData(data);
            if (data.length) {
                throw new Error("game mode field (" + GameModeFieldType[type] + ") did not decode full buffer");
            }
            return field;
        }
    }
    SpyCards.GameModeField = GameModeField;
    class GameModeMetadata extends GameModeField {
        constructor() {
            super(...arguments);
            this.title = "";
            this.author = "";
            this.description = "";
            this.latestChanges = "";
        }
        getType() {
            return GameModeFieldType.Metadata;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUTF8StringVar(buf, this.title);
            SpyCards.Binary.pushUTF8StringVar(buf, this.author);
            SpyCards.Binary.pushUTF8StringVar(buf, this.description);
            SpyCards.Binary.pushUTF8StringVar(buf, this.latestChanges);
        }
        unmarshalData(buf) {
            this.title = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.author = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.description = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.latestChanges = SpyCards.Binary.shiftUTF8StringVar(buf);
        }
        replaceCardID(from, to) { }
    }
    SpyCards.GameModeMetadata = GameModeMetadata;
    class GameModeBannedCards extends GameModeField {
        constructor() {
            super(...arguments);
            this.bannedCards = [];
        }
        getType() {
            return GameModeFieldType.BannedCards;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.bannedCards.length);
            for (let card of this.bannedCards) {
                SpyCards.Binary.pushUVarInt(buf, card);
            }
        }
        unmarshalData(buf) {
            const count = SpyCards.Binary.shiftUVarInt(buf);
            this.bannedCards = new Array(count);
            for (let i = 0; i < count; i++) {
                this.bannedCards[i] = SpyCards.Binary.shiftUVarInt(buf);
            }
        }
        replaceCardID(from, to) {
            this.bannedCards = this.bannedCards.map((c) => c === from ? to : c);
        }
    }
    SpyCards.GameModeBannedCards = GameModeBannedCards;
    let GameRuleType;
    (function (GameRuleType) {
        GameRuleType[GameRuleType["None"] = 0] = "None";
        GameRuleType[GameRuleType["MaxHP"] = 1] = "MaxHP";
        GameRuleType[GameRuleType["HandMinSize"] = 2] = "HandMinSize";
        GameRuleType[GameRuleType["HandMaxSize"] = 3] = "HandMaxSize";
        GameRuleType[GameRuleType["DrawPerTurn"] = 4] = "DrawPerTurn";
        GameRuleType[GameRuleType["CardsPerDeck"] = 5] = "CardsPerDeck";
        GameRuleType[GameRuleType["MinTP"] = 6] = "MinTP";
        GameRuleType[GameRuleType["MaxTP"] = 7] = "MaxTP";
        GameRuleType[GameRuleType["TPPerTurn"] = 8] = "TPPerTurn";
        GameRuleType[GameRuleType["BossCards"] = 9] = "BossCards";
        GameRuleType[GameRuleType["MiniBossCards"] = 10] = "MiniBossCards";
    })(GameRuleType = SpyCards.GameRuleType || (SpyCards.GameRuleType = {}));
    class GameModeGameRules extends GameModeField {
        constructor() {
            super(...arguments);
            this.maxHP = 5;
            this.handMinSize = 3;
            this.handMaxSize = 5;
            this.drawPerTurn = 2;
            this.cardsPerDeck = 15;
            this.minTP = 2;
            this.maxTP = 10;
            this.tpPerTurn = 1;
            this.bossCards = 1;
            this.miniBossCards = 2;
        }
        static typeName(type) {
            return GameRuleType[type].replace(/([a-z])([A-Z])|([A-Z])([A-Z][a-z])/g, "$1$3 $2$4");
        }
        getType() {
            return GameModeFieldType.GameRules;
        }
        marshalData(buf) {
            const defaults = new GameModeGameRules();
            for (let { type, get, signed } of GameModeGameRules.rules) {
                if (get(this) === get(defaults)) {
                    continue;
                }
                SpyCards.Binary.pushUVarInt(buf, type);
                if (signed) {
                    SpyCards.Binary.pushSVarInt(buf, get(this));
                }
                else {
                    SpyCards.Binary.pushUVarInt(buf, get(this));
                }
            }
            SpyCards.Binary.pushUVarInt(buf, GameRuleType.None);
        }
        unmarshalData(buf) {
            const defaults = new GameModeGameRules();
            for (let { get, set } of GameModeGameRules.rules) {
                set(this, get(defaults));
            }
            for (;;) {
                const type = SpyCards.Binary.shiftUVarInt(buf);
                if (type === GameRuleType.None) {
                    break;
                }
                const rule = GameModeGameRules.rules.find((r) => r.type === type);
                if (!rule) {
                    throw new Error("unknown game rule ID: " + type);
                }
                const { get, set, signed, max } = rule;
                if (signed) {
                    set(this, SpyCards.Binary.shiftSVarInt(buf));
                }
                else {
                    set(this, SpyCards.Binary.shiftUVarInt(buf));
                }
                if (max && get(this) > max) {
                    set(this, max);
                }
            }
        }
        replaceCardID(from, to) { }
    }
    GameModeGameRules.rules = [
        { type: GameRuleType.MaxHP, get: (r) => r.maxHP, set: (r, n) => r.maxHP = n },
        { type: GameRuleType.HandMinSize, get: (r) => r.handMinSize, set: (r, n) => r.handMinSize = n },
        { type: GameRuleType.HandMaxSize, get: (r) => r.handMaxSize, set: (r, n) => r.handMaxSize = n, max: 50 },
        { type: GameRuleType.DrawPerTurn, get: (r) => r.drawPerTurn, set: (r, n) => r.drawPerTurn = n },
        { type: GameRuleType.CardsPerDeck, get: (r) => r.cardsPerDeck, set: (r, n) => r.cardsPerDeck = n },
        { type: GameRuleType.MinTP, get: (r) => r.minTP, set: (r, n) => r.minTP = n },
        { type: GameRuleType.MaxTP, get: (r) => r.maxTP, set: (r, n) => r.maxTP = n },
        { type: GameRuleType.TPPerTurn, get: (r) => r.tpPerTurn, set: (r, n) => r.tpPerTurn = n },
        { type: GameRuleType.BossCards, get: (r) => r.bossCards, set: (r, n) => r.bossCards = n },
        { type: GameRuleType.MiniBossCards, get: (r) => r.miniBossCards, set: (r, n) => r.miniBossCards = n },
    ];
    SpyCards.GameModeGameRules = GameModeGameRules;
    let SummonCardFlags;
    (function (SummonCardFlags) {
        SummonCardFlags[SummonCardFlags["BothPlayers"] = 1] = "BothPlayers";
    })(SummonCardFlags = SpyCards.SummonCardFlags || (SpyCards.SummonCardFlags = {}));
    class GameModeSummonCard extends GameModeField {
        constructor() {
            super(...arguments);
            this.flags = SummonCardFlags.BothPlayers;
            this.card = SpyCards.CardData.GlobalCardID.Seedling;
        }
        getType() {
            return GameModeFieldType.SummonCard;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.flags);
            SpyCards.Binary.pushUVarInt(buf, this.card);
        }
        unmarshalData(buf) {
            this.flags = SpyCards.Binary.shiftUVarInt(buf);
            this.card = SpyCards.Binary.shiftUVarInt(buf);
        }
        replaceCardID(from, to) {
            if (this.card === from) {
                this.card = to;
            }
        }
    }
    SpyCards.GameModeSummonCard = GameModeSummonCard;
    class GameModeVariant extends GameModeField {
        constructor() {
            super(...arguments);
            this.title = "";
            this.npc = "";
            this.rules = [];
        }
        getType() {
            return GameModeFieldType.Variant;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUTF8StringVar(buf, this.title);
            SpyCards.Binary.pushUTF8StringVar(buf, this.npc);
            SpyCards.Binary.pushUVarInt(buf, this.rules.length);
            for (let rule of this.rules) {
                rule.marshal(buf);
            }
        }
        unmarshalData(buf) {
            this.title = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.npc = SpyCards.Binary.shiftUTF8StringVar(buf);
            const numRules = SpyCards.Binary.shiftUVarInt(buf);
            this.rules.length = numRules;
            for (let i = 0; i < numRules; i++) {
                this.rules[i] = GameModeField.unmarshal(buf);
            }
        }
        replaceCardID(from, to) {
            for (let rule of this.rules) {
                rule.replaceCardID(from, to);
            }
        }
    }
    SpyCards.GameModeVariant = GameModeVariant;
    class GameModeUnfilterCard extends GameModeField {
        constructor() {
            super(...arguments);
            this.flags = 0;
            this.card = SpyCards.CardData.GlobalCardID.Seedling;
        }
        getType() {
            return GameModeFieldType.UnfilterCard;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.flags);
            SpyCards.Binary.pushUVarInt(buf, this.card);
        }
        unmarshalData(buf) {
            this.flags = SpyCards.Binary.shiftUVarInt(buf);
            this.card = SpyCards.Binary.shiftUVarInt(buf);
        }
        replaceCardID(from, to) {
            if (this.card === from) {
                this.card = to;
            }
        }
    }
    SpyCards.GameModeUnfilterCard = GameModeUnfilterCard;
    class GameModeDeckLimitFilter extends GameModeField {
        constructor() {
            super(...arguments);
            this.count = 0;
            this.rank = SpyCards.Rank.Attacker;
            this.tribe = SpyCards.Tribe.Seedling;
            this.customTribe = "";
            this.card = SpyCards.CardData.GlobalCardID.Zombiant;
        }
        getType() {
            return GameModeFieldType.DeckLimitFilter;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.count);
            buf.push((this.rank << 4) | this.tribe);
            if (this.tribe === SpyCards.Tribe.Custom) {
                SpyCards.Binary.pushUTF8StringVar(buf, this.customTribe);
            }
            if (this.rank === SpyCards.Rank.None && this.tribe === SpyCards.Tribe.None) {
                SpyCards.Binary.pushUVarInt(buf, this.card);
            }
        }
        unmarshalData(buf) {
            this.count = SpyCards.Binary.shiftUVarInt(buf);
            const rankTribe = buf.shift();
            this.rank = rankTribe >> 4;
            this.tribe = rankTribe & 15;
            if (this.tribe === SpyCards.Tribe.Custom) {
                this.customTribe = SpyCards.Binary.shiftUTF8StringVar(buf);
            }
            else {
                this.customTribe = "";
            }
            if (this.rank === SpyCards.Rank.None && this.tribe === SpyCards.Tribe.None) {
                this.card = SpyCards.Binary.shiftUVarInt(buf);
            }
            else {
                this.card = SpyCards.CardData.GlobalCardID.Zombiant;
            }
        }
        replaceCardID(from, to) {
            if (this.card === from) {
                this.card = to;
            }
        }
        match(card) {
            if (!card) {
                return false;
            }
            if (this.rank === SpyCards.Rank.None && this.tribe === SpyCards.Tribe.None) {
                return card.id === this.card;
            }
            if (this.rank === SpyCards.Rank.Enemy) {
                if (card.rank() !== SpyCards.Rank.Attacker && card.rank() !== SpyCards.Rank.Effect) {
                    return false;
                }
            }
            else if (this.rank !== SpyCards.Rank.None && card.rank() !== this.rank) {
                return false;
            }
            if (this.tribe === SpyCards.Tribe.Custom) {
                if (!card.tribes.some((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === this.customTribe)) {
                    return false;
                }
            }
            else if (this.tribe !== SpyCards.Tribe.None) {
                if (!card.tribes.some((t) => t.tribe === this.tribe)) {
                    return false;
                }
            }
            return true;
        }
    }
    SpyCards.GameModeDeckLimitFilter = GameModeDeckLimitFilter;
    class GameModeTimer extends GameModeField {
        constructor() {
            super(...arguments);
            this.startTime = 150;
            this.maxTime = 300;
            this.perTurn = 10;
            this.maxPerTurn = 90;
        }
        getType() {
            return GameModeFieldType.Timer;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.startTime);
            SpyCards.Binary.pushUVarInt(buf, this.maxTime);
            SpyCards.Binary.pushUVarInt(buf, this.perTurn);
            SpyCards.Binary.pushUVarInt(buf, this.maxPerTurn);
        }
        unmarshalData(buf) {
            this.startTime = SpyCards.Binary.shiftUVarInt(buf);
            this.maxTime = SpyCards.Binary.shiftUVarInt(buf);
            this.perTurn = SpyCards.Binary.shiftUVarInt(buf);
            this.maxPerTurn = SpyCards.Binary.shiftUVarInt(buf);
        }
        replaceCardID(from, to) {
            // no card IDs in this field
        }
    }
    SpyCards.GameModeTimer = GameModeTimer;
    SpyCards.GameModeFieldConstructors = {
        [GameModeFieldType.Metadata]: () => new GameModeMetadata(),
        [GameModeFieldType.BannedCards]: () => new GameModeBannedCards(),
        [GameModeFieldType.GameRules]: () => new GameModeGameRules(),
        [GameModeFieldType.SummonCard]: () => new GameModeSummonCard(),
        [GameModeFieldType.Variant]: () => new GameModeVariant(),
        [GameModeFieldType.UnfilterCard]: () => new GameModeUnfilterCard(),
        [GameModeFieldType.DeckLimitFilter]: () => new GameModeDeckLimitFilter(),
        [GameModeFieldType.Timer]: () => new GameModeTimer(),
    };
})(SpyCards || (SpyCards = {}));
