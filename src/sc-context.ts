module SpyCards {
	export interface Context {
		net: {
			cgc: CardGameConnection;
			sc: SignalingConnection;
		};
		ui: {
			form: HTMLElement;
			h1: HTMLHeadingElement;
			status: HTMLParagraphElement;
			code: HTMLInputElement;
			hadFatalError: boolean;
			fatalErrorPromise: Promise<void>;
			displayFatalError: (message: string, reset: boolean) => void;
		};
		player: 1 | 2;
		scg: SecureCardGame;
		defs: CardDefs;
		matchData: MatchData;
		state?: MatchState;
		fx?: ContextFx;
		effect?: ContextEffect;
		npcCtx?: Context;
	}

	export interface ContextFx {
		coin(card: CardInstance, heads: boolean, isStat: boolean, index: number, total: number): Promise<void>;
		numb(card: CardInstance, target: CardInstance): Promise<void>;
		setup(card: CardInstance): Promise<void>;
		multiple(index: number): Promise<void>;
		assist(actor: CardInstance, assist: CardInstance): Promise<void>;
		beforeProcessEffect(card: CardInstance, effect: EffectDef, nested?: boolean): Promise<void>;
		afterProcessEffect(card: CardInstance, effect: EffectDef, nested?: boolean): Promise<void>;
	}

	export interface ContextEffect {
		modifyStat(card: CardInstance, target: CardInstance, effect: EffectDef, player: 1 | 2, stat: "ATK" | "DEF", diff: number): Promise<void>;
		recalculateStats(player: 1 | 2): Promise<void>;
		applyResult(card: CardInstance, effect: EffectDef, result: EffectDef, delayProcessing?: boolean): Promise<void>;
		summonCard(player: 1 | 2, summoner: CardInstance, toSummon: CardDef, special?: string, effects?: EffectDef[]): Promise<void>;
		heal(card: CardInstance, effect: EffectDef, player: 1 | 2, amount: number): Promise<void>;
		multiplyHealing(card: CardInstance, effect: EffectDef, player: 1 | 2, amount: number, negative?: boolean | null): Promise<void>;
	}

	export class MatchData {
		public customModeName: string = "";
		public rematchCount = 0;
		public losses = 0;
		public wins = 0;
		public selectedCharacter?: string;
	}

	export class MatchState {
		public readonly hp = [0, 0];
		public readonly tp = [0, 0];
		public readonly rawATK = [0, 0];
		public readonly rawDEF = [0, 0];
		public readonly ATK = [0, 0];
		public readonly DEF = [0, 0];
		public readonly healTotalN = [0, 0];
		public readonly healTotalP = [0, 0];
		public readonly healMultiplierN = [1, 1];
		public readonly healMultiplierP = [1, 1];
		public turn = 0;
		public readonly deck: CardInstance[][] = [[], []];
		public readonly backs: (0 | 1 | 2)[][] = [[], []];
		public readonly hand: CardInstance[][] = [[], []];
		public readonly discard: CardInstance[][] = [[], []];
		public readonly once: EffectDef[][] = [[], []];
		public readonly setup: {
			card: CardDef;
			effects: EffectDef[];
			originalDesc: boolean;
			sourceCard?: CardInstance;
		}[][] = [[], []];
		public readonly ready = [false, false];
		public readonly played: CardInstance[][] = [[], []];
		public winner: 0 | 1 | 2 = 0;
		public bothReady?: () => void;
		public players?: TheRoom.Player[] = [null, null];
		public roundSetupComplete?: boolean;
		public roundSetupDelayed?: (() => void)[];
		public turnData?: TurnData;
		public readonly effectBacklog: {
			card: CardInstance;
			effect: EffectDef;
		}[] = [];
		public currentCard?: CardInstance;
		public currentEffect?: EffectDef;
		public readonly gameLog = new GameLog();
		readonly npcState: MatchState;

		constructor(defs: CardDefs, npcState?: MatchState) {
			this.hp[0] = this.hp[1] = defs.rules.maxHP;
			this.npcState = npcState;
		}
	}

	export interface TurnData {
		ready?: number[];
		played?: number[][];
	}

	export interface CardInstance {
		card: CardDef;
		back: 0 | 1 | 2;
		player: 1 | 2;
		el?: HTMLDivElement;
		effects?: EffectDef[];
		numb?: boolean;
		becomingNumb?: boolean;
		modifiedATK?: number;
		modifiedDEF?: number;
		setup?: boolean;
		setupPlaceholder?: HTMLDivElement;
		originalDesc?: boolean;
		invisible?: boolean;
	}
}
