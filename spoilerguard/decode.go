package spoilerguard

import (
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"golang.org/x/xerrors"
)

const (
	sep0    = "ȳ"
	sep1    = "ɟ"
	sep2    = "ȕ"
	sep3    = "ɣɌɏɓɖɋɣ"
	nilval  = "ȯ"
	questID = "ȩ"
)

var encCards = map[string]uint8{
	"ȮȬ": 0, "ȭȭ": 1, "ȭȬ": 2, "ȯ": 3,
	"ȫȭ": 4, "ȫȬ": 5, "Ȯ": 6, "ȫȩ": 7,
	"Ȯȧ": 8, "Ȫȧ": 9, "Ȯȯ": 10, "Ȭȭ": 11,
	"ȮȮ": 12, "Ȯȭ": 13, "ȭȫ": 14, "ȨȮ": 15,
	"ȭȦ": 16, "ȭȨ": 17, "ȫȨ": 18, "ȫȦ": 19,
	"ȫȧ": 20, "ȪȦ": 21, "ȪȮ": 22, "Ȫȭ": 23,
	"ȨȬ": 24, "Ȩȭ": 25, "Ȩȯ": 26, "ȩȨ": 27,
	"ȩȧ": 28, "ȬȮ": 29, "ȭȯ": 30, "ȮȪ": 31,
	"Ȧ": 32, "ȧȦ": 33, "ȧȧ": 34, "ȧ": 35,
	"Ȭȯ": 36, "Ȫȩ": 37, "Ȯȫ": 38, "ȩȦ": 39,
	"ȭȮ": 40, "ȩȭ": 41, "Ȯȩ": 42, "ȭȪ": 43,
	"Ȭȫ": 44, "ȪȬ": 45, "Ȫȫ": 46, "ȪȪ": 47,
	"ȧȩ": 48, "ȧȨ": 49, "ȮȨ": 50, "ȬȬ": 0,
	"ȩ": 1, "Ȫ": 2, "ȫ": 3, "ȬȨ": 4,
	"Ȭȧ": 5, "ȩȪ": 6, "ȩȬ": 7, "ȩȫ": 8,
	"ȩȯ": 9, "Ȩȫ": 10, "ȨȪ": 11, "Ȩ": 12,
	"ȮȦ": 13, "ȫȮ": 14, "ȭȧ": 15, "ȧȫ": 16,
	"ȧȮ": 17, "ȧȭ": 18, "Ȩȩ": 19, "ȨȨ": 20,
	"Ȩȧ": 21, "ȭ": 0, "Ȭ": 1, "ȬȪ": 2,
	"ȫȫ": 3, "ȪȨ": 4, "ȩȮ": 5, "ȭȩ": 6,
	"Ȭȩ": 7, "ȧȬ": 8, "ȫȯ": 9, "ȬȦ": 10,
	"ȫȪ": 11, "ȧȪ": 12, "ȩȩ": 13, "ȧȯ": 14,
	"Ȫȯ": 15, "ȨȦ": 16,
}

func flagMap(s string) []bool {
	parts := strings.Split(s, sep0)
	b := make([]bool, len(parts))

	for i, p := range parts {
		b[i] = len(p)&2 == 0
	}

	return b
}

func parseSaveData(data string) (*Data, error) {
	// to avoid giving away how saves are encoded,
	// we're not actually decoding the save,
	// just grabbing the specific data we need.
	sections := strings.Split(data, sep2)
	flags := flagMap(sections[11])

	var modes uint32

	for i, j := range []int{613, 614, 615, 616, 656, 681} {
		if flags[j] {
			modes |= 1 << i
		}
	}

	questType := QuestUnavailable

	for i, quests := range strings.Split(sections[5], sep1) {
		for _, q := range strings.Split(quests, sep0) {
			if q == questID {
				questType = QuestProgress(i)

				break
			}
		}

		if questType != QuestUnavailable {
			break
		}
	}

	deck0 := strings.Split(strings.Split(sections[12], sep3)[12], sep0)
	deck1 := make([]uint8, len(deck0))

	for i, e := range deck0 {
		var ok bool

		deck1[i], ok = encCards[e]

		if !ok {
			deck1 = nil

			break
		}
	}

	var deck3 card.Deck

	if len(deck1) == 15 {
		deck2 := make([]byte, 11)

		deck2[0] = (deck1[0] << 2) | (deck1[1] >> 3)
		deck2[1] = (deck1[1] << 5) | deck1[2]

		for i, j := 2, 3; j < len(deck1); i, j = i+3, j+4 {
			deck2[i] = (deck1[j] << 2) | (deck1[j+1] >> 4)
			deck2[i+1] = (deck1[j+1] << 4) | (deck1[j+2] >> 2)
			deck2[i+2] = (deck1[j+2] << 6) | deck1[j+3]
		}

		if err := deck3.UnmarshalBinary(deck2); err != nil {
			return nil, xerrors.Errorf("spoilerguard: decoding deck: %w", err)
		}
	}

	spyData := flagMap(strings.Split(sections[10], sep1)[1])
	enemyData0 := sections[17]
	enemyData1 := strings.Split(enemyData0, sep1)
	enemyData3 := make([][]bool, len(enemyData1))

	for i, e := range enemyData1 {
		enemyData2 := strings.Split(e, sep0)
		enemyData3[i] = make([]bool, len(enemyData2))

		for j, c := range enemyData2 {
			enemyData3[i][j] = c != nilval
		}
	}

	enemyData4 := make([]uint8, 256/8)
	i, b := 0, 0

	for j := 0; j < 128; j++ {
		if enemyData3[j][0] {
			enemyData4[i] |= 1 << b
		}

		b++

		if spyData[j] {
			enemyData4[i] |= 1 << b
		}

		b++

		if b >= 8 {
			i++

			b = 0
		}
	}

	if b != 0 || i != len(enemyData4) {
		return nil, xerrors.New("spoilerguard: invalid data")
	}

	return &Data{
		Quest:            questType,
		MetCarmina:       flags[236],
		CarminaApproved:  flags[237],
		Deck:             deck3,
		SeenSpiedEnemies: enemyData4,
		Menu:             modes,
	}, nil
}
