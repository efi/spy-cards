package gui

import "strings"

type Input struct {
	*Label
	builder strings.Builder
}

func (i *Input) Value() string {
	return i.builder.String()
}

func (i *Input) Put(ch rune) {
	i.builder.WriteRune(ch)
	i.Text = i.Value()
}
