package server

import (
	"database/sql"

	// PostgreSQL database driver.
	_ "github.com/jackc/pgx/v4/stdlib"
)

var (
	begin func() (*sql.Tx, error)

	checkAllowedOrigin *sql.Stmt
	getAllowedOrigins  *sql.Stmt

	insertUserImage       *sql.Stmt
	getUserImage          *sql.Stmt
	getUserImageByHash    *sql.Stmt
	getCommunityPortraits *sql.Stmt
	getTanjerinUserImages *sql.Stmt

	insertIssueReport      *sql.Stmt
	getPortraitCount       *sql.Stmt
	getPortraitList        *sql.Stmt
	getReportCount         *sql.Stmt
	getReportList          *sql.Stmt
	updateReportDevComment *sql.Stmt

	getMatchCodePrefix         *sql.Stmt
	getCustomCardSet           *sql.Stmt
	getCustomGameModeCount     *sql.Stmt
	getCustomGameModeList      *sql.Stmt
	getCustomGameModesForOwner *sql.Stmt
	getCustomGameMode          *sql.Stmt
	getCustomGameModeSets      *sql.Stmt
	insertCustomCard           *sql.Stmt
	createCustomCardSet        *sql.Stmt
	addCardToCustomSet         *sql.Stmt
	setLatestGameModeRevision  *sql.Stmt

	insertMatchRecording    *sql.Stmt
	getRecordingCount       *sql.Stmt
	getRecordingList        *sql.Stmt
	getMatchRecordingData   *sql.Stmt
	getRandomMatchRecording *sql.Stmt

	getUserByID            *sql.Stmt
	getUserByLogin         *sql.Stmt
	getUserCurrentPassword *sql.Stmt
	updateUserPassword     *sql.Stmt
	getUserCount           *sql.Stmt
	getUserList            *sql.Stmt
	createUser             *sql.Stmt

	auditCompleteMatchmaking  *sql.Stmt
	auditMatchmakingPing      *sql.Stmt
	auditMatchmakingUsedRelay *sql.Stmt
	auditMatchmakingBegin     *sql.Stmt

	insertArcadeRecording        *sql.Stmt
	getArcadeRecordingCount      *sql.Stmt
	getArcadeRecordingList       *sql.Stmt
	getArcadeRecording           *sql.Stmt
	decodedArcadeRecording       *sql.Stmt
	getArcadeHighScoresRecent    *sql.Stmt
	getArcadeHighScoresWeekly    *sql.Stmt
	getArcadeHighScoresQuarterly *sql.Stmt
	getArcadeHighScoresAllTime   *sql.Stmt
)

func initDB(dsn string) {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		panic(err)
	}

	do := func(_ sql.Result, err error) {
		if err != nil {
			panic(err)
		}
	}

	do(db.Exec(`CREATE TABLE IF NOT EXISTS "user_accounts" (
	"id" BIGSERIAL NOT NULL PRIMARY KEY,
	"login_name" TEXT NOT NULL UNIQUE,
	"password" BYTEA NOT NULL,
	"session_reset" INT NOT NULL DEFAULT 0,
	"is_admin" BOOL NOT NULL DEFAULT FALSE
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "allowed_origins" (
	"origin" TEXT NOT NULL PRIMARY KEY
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "user_images" (
	"id" BIGSERIAL PRIMARY KEY,
	"hash" BYTEA NOT NULL,
	"data" BYTEA NOT NULL,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"whitelisted" BOOL NOT NULL DEFAULT FALSE,
	"tanjerin" BOOL DEFAULT NULL
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "issue_reports" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"user_agent" TEXT NOT NULL,
	"version" TEXT NOT NULL,
	"message" TEXT NOT NULL,
	"stack" TEXT NOT NULL,
	"state" JSONB NOT NULL,
	"custom" TEXT NOT NULL,
	"user_comment" TEXT NOT NULL,
	"dev_comment" TEXT DEFAULT NULL,
	"cache_version" TEXT NOT NULL DEFAULT ''
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_game_modes" (
	"id" BIGSERIAL PRIMARY KEY,
	"name" TEXT NOT NULL,
	"client_slug" TEXT NOT NULL UNIQUE,
	"latest_revision" BIGINT NOT NULL DEFAULT 0,
	"owner" BIGINT NOT NULL REFERENCES "user_accounts"("id")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_card_sets" (
	"id" BIGSERIAL PRIMARY KEY,
	"mode_id" BIGINT NOT NULL REFERENCES "custom_game_modes" ("id"),
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"revision" BIGINT NOT NULL,
	UNIQUE ("mode_id", "revision")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "match_code_prefixes" (
	"client" TEXT PRIMARY KEY,
	"prefix" CHAR(4) NOT NULL UNIQUE CHECK ("prefix" NOT LIKE '%[^0-9A-HJKMNP-TV-Z]%')
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "match_recordings" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_by" INET[] NOT NULL,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"hash" BYTEA NOT NULL,
	"data" BYTEA NOT NULL,
	"format_version" BIGINT NOT NULL,
	"version_major" BIGINT NOT NULL,
	"version_minor" BIGINT NOT NULL,
	"version_patch" BIGINT NOT NULL,
	"mode_prefix" TEXT NOT NULL,
	"mode_suffix" TEXT
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_cards" (
	"id" BIGSERIAL PRIMARY KEY,
	"code" BYTEA NOT NULL UNIQUE,
	"format_version" INT NOT NULL
		GENERATED ALWAYS AS (get_byte("code", 0)) STORED
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "custom_card_set_cards" (
	"set_id" BIGINT NOT NULL REFERENCES "custom_card_sets"("id"),
	"card_id" BIGINT NOT NULL REFERENCES "custom_cards"("id"), 
	"order" INT NOT NULL,
	UNIQUE("set_id", "order")
);`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "matchmaking_audit_log" (
	"id" BIGSERIAL PRIMARY KEY,
	"code" CHAR(24) NOT NULL,
	"client" TEXT NOT NULL,
	"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"last_ping" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"used_relay" BOOL NOT NULL DEFAULT FALSE,
	"ended" TIMESTAMPTZ DEFAULT NULL,
	"end_reason" INT DEFAULT NULL,
	"p1_addr" INET[] NOT NULL,
	"p1_ua" TEXT NOT NULL,
	"p2_addr" INET[] DEFAULT NULL,
	"p2_ua" TEXT DEFAULT NULL
);`))
	do(db.Exec(`DO $$
BEGIN
	BEGIN
		CREATE TYPE "arcade_game" AS ENUM ('MiteKnight', 'FlowerJourney');
	EXCEPTION WHEN duplicate_object THEN
	END;
END
$$;`))
	do(db.Exec(`CREATE TABLE IF NOT EXISTS "arcade_recordings" (
	"id" BIGSERIAL PRIMARY KEY,
	"uploaded_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
	"uploaded_by" INET[] NOT NULL,
	"user_agent" TEXT NOT NULL,
	"data" BYTEA NOT NULL,
	"hash" BYTEA NOT NULL,
	"decode_failed_reason" TEXT DEFAULT 'not attempted',
	"game" "arcade_game",
	"player_name" CHAR(3),
	"score" BIGINT,
	"vanilla" BOOL
);`))

	prepare := func(query string) *sql.Stmt {
		stmt, err := db.Prepare(query)
		if err != nil {
			panic(err)
		}

		return stmt
	}

	begin = db.Begin
	checkAllowedOrigin = prepare(`SELECT EXISTS(SELECT 1 FROM "allowed_origins" WHERE "origin" = $1::TEXT);`)
	getAllowedOrigins = prepare(`SELECT array_agg("origin") FROM "allowed_origins";`)
	insertUserImage = prepare(`INSERT INTO "user_images" ("uploaded_by", "hash", "data") VALUES (('{' || $1::TEXT || '}')::INET[], $2::BYTEA, $3::BYTEA) RETURNING "id";`)
	getUserImage = prepare(`SELECT "hash", "data" FROM "user_images" WHERE "id" = $1::BIGINT;`)
	getUserImageByHash = prepare(`SELECT "id" FROM "user_images" WHERE "hash" = $1::BYTEA;`)
	getCommunityPortraits = prepare(`SELECT "hash", "id" FROM "user_images" WHERE "whitelisted" ORDER BY "id" ASC;`)
	getTanjerinUserImages = prepare(`SELECT "hash", "id", "tanjerin" FROM "user_images" WHERE "tanjerin" IS NOT NULL ORDER BY "id" ASC;`)
	insertIssueReport = prepare(`INSERT INTO "issue_reports" ("uploaded_by", "user_agent", "version", "cache_version", "message", "stack", "state", "custom", "user_comment") VALUES (('{' || $1::TEXT || '}')::INET[], $2::TEXT, $3::TEXT, $4::TEXT, $5::TEXT, $6::TEXT, $7::TEXT::JSONB, $8::TEXT, $9::TEXT);`)
	getPortraitCount = prepare(`SELECT COUNT(*) FROM "user_images";`)
	getPortraitList = prepare(`SELECT "id", "hash" FROM "user_images" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	getReportCount = prepare(`SELECT COUNT(*) FROM "issue_reports";`)
	getReportList = prepare(`SELECT "id", "version", "cache_version", "user_agent", "message", "stack", "state", "custom", "user_comment", "dev_comment" FROM "issue_reports" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	updateReportDevComment = prepare(`UPDATE "issue_reports" SET "dev_comment" = $2::TEXT WHERE "id" = $1::BIGINT;`)
	getMatchCodePrefix = prepare(`SELECT "prefix" FROM "match_code_prefixes" WHERE "client" = $1::TEXT`)
	getCustomCardSet = prepare(`SELECT m."name", array_to_string(ARRAY(SELECT replace(encode("code", 'base64'), E'\n', '') FROM "custom_cards" cc INNER JOIN "custom_card_set_cards" ccsc ON cc."id" = ccsc."card_id" WHERE ccsc."set_id" = s."id" ORDER BY ccsc."order" ASC), ',') "cards", s."revision" FROM "custom_card_sets" s
 INNER JOIN "custom_game_modes" m ON s."mode_id" = m."id"
 WHERE m."client_slug" = $1::TEXT
   AND (s."revision" = $2::INT OR $2::INT IS NULL)
 ORDER BY s."id" DESC LIMIT 1;`)
	getCustomGameModeCount = prepare(`SELECT COUNT(*) FROM "custom_game_modes";`)
	getCustomGameModeList = prepare(`SELECT "id", "client_slug", "prefix", "name", "latest_revision" FROM "custom_game_modes"
  LEFT JOIN "match_code_prefixes" ON "client" = '-' || "client_slug"
 ORDER BY "id" ASC LIMIT $2::INT OFFSET $1::INT;`)
	getCustomGameModesForOwner = prepare(`SELECT "id", "client_slug", "name" FROM "custom_game_modes" WHERE "owner" = $1::BIGINT ORDER BY "id" ASC;`)
	getCustomGameMode = prepare(`SELECT "client_slug", "name", "latest_revision", "owner", "prefix" FROM "custom_game_modes"
  LEFT JOIN "match_code_prefixes" ON "client" = '-' || "client_slug"
 WHERE "id" = $1::BIGINT;`)
	getCustomGameModeSets = prepare(`SELECT "revision", array_to_string(ARRAY(SELECT replace(encode("code", 'base64'), E'\n', '') FROM "custom_cards" cc INNER JOIN "custom_card_set_cards" ccsc ON cc."id" = ccsc."card_id" WHERE ccsc."set_id" = s."id" ORDER BY ccsc."order" ASC), ',') "cards" FROM "custom_card_sets" s
 WHERE "mode_id" = $1::BIGINT
 ORDER BY "revision" ASC;`)
	insertCustomCard = prepare(`WITH "existing" AS (SELECT "id" FROM "custom_cards" WHERE "code" = $1::BYTEA), "inserted" AS (INSERT INTO "custom_cards" ("code") SELECT $1::BYTEA WHERE NOT EXISTS (SELECT * FROM "existing") ON CONFLICT DO NOTHING RETURNING "id") SELECT COALESCE((SELECT "id" FROM "existing"), (SELECT "id" FROM "inserted"), (SELECT "id" FROM "custom_cards" WHERE "code" = $1::BYTEA));`)
	createCustomCardSet = prepare(`INSERT INTO "custom_card_sets" ("mode_id", "uploaded_by", "revision") VALUES ($1::BIGINT, ('{' || $2::TEXT || '}')::INET[], $3::BIGINT) RETURNING "id";`)
	addCardToCustomSet = prepare(`INSERT INTO "custom_card_set_cards" ("set_id", "card_id", "order") VALUES ($1::BIGINT, $2::BIGINT, $3::INT);`)
	setLatestGameModeRevision = prepare(`UPDATE "custom_game_modes" SET "latest_revision" = $2::BIGINT WHERE "id" = $1::BIGINT;`)
	insertMatchRecording = prepare(`INSERT INTO "match_recordings" ("uploaded_by", "hash", "data", "format_version", "version_major", "version_minor", "version_patch", "mode_prefix", "mode_suffix") VALUES (('{' || $1::TEXT || '}')::INET[], $2::BYTEA, $3::BYTEA, $4::BIGINT, $5::BIGINT, $6::BIGINT, $7::BIGINT, $8::TEXT, $9::TEXT) RETURNING "id";`)
	getRecordingCount = prepare(`SELECT COUNT(*) FROM "match_recordings";`)
	getRecordingList = prepare(`SELECT "id", "uploaded_at", "hash", "version_major", "version_minor", "version_patch", "mode_prefix", "mode_suffix" FROM "match_recordings" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	getMatchRecordingData = prepare(`SELECT "hash", "data" FROM "match_recordings" WHERE "id" = $1::BIGINT;`)
	getRandomMatchRecording = prepare(`SELECT "id", "hash" FROM "match_recordings" WHERE "mode_prefix" <> 'custom' AND "id" <> ALL ($1::BIGINT[]) ORDER BY RANDOM() LIMIT 1;`)
	getUserByID = prepare(`SELECT "login_name", "session_reset", "is_admin" FROM "user_accounts" WHERE "id" = $1::BIGINT;`)
	getUserByLogin = prepare(`SELECT "id", "password", "session_reset", "is_admin" FROM "user_accounts" WHERE "login_name" = $1::TEXT;`)
	getUserCurrentPassword = prepare(`SELECT "password" FROM "user_accounts" WHERE "id" = $1::BIGINT;`)
	updateUserPassword = prepare(`UPDATE "user_accounts" SET "password" = $3::BYTEA, "session_reset" = "session_reset" + 1 WHERE "id" = $1::BIGINT AND "password" = $2::BYTEA;`)
	getUserCount = prepare(`SELECT COUNT(*) FROM "user_accounts";`)
	getUserList = prepare(`SELECT "id", "login_name", "session_reset", "is_admin" FROM "user_accounts" ORDER BY "id" ASC LIMIT $2::INT OFFSET $1::INT;`)
	createUser = prepare(`INSERT INTO "user_accounts" ("login_name", "password") VALUES ($1::TEXT, $2::BYTEA) RETURNING "id";`)
	auditCompleteMatchmaking = prepare(`UPDATE "matchmaking_audit_log" SET "end_reason" = $2::INT, "ended" = NOW(), "p2_addr" = ('{' || $3::TEXT || '}')::INET[], "p2_ua" = $4::TEXT WHERE "id" = $1::BIGINT;`)
	auditMatchmakingPing = prepare(`UPDATE "matchmaking_audit_log" SET "last_ping" = NOW() WHERE "id" = $1::BIGINT;`)
	auditMatchmakingUsedRelay = prepare(`UPDATE "matchmaking_audit_log" SET "used_relay" = TRUE WHERE "id" = $1::BIGINT;`)
	auditMatchmakingBegin = prepare(`INSERT INTO "matchmaking_audit_log" ("code", "client", "p1_addr", "p1_ua") VALUES ($1::TEXT, $2::TEXT, ('{' || $3::TEXT || '}')::INET[], $4::TEXT) RETURNING "id";`)
	insertArcadeRecording = prepare(`INSERT INTO "arcade_recordings" ("uploaded_by", "user_agent", "data", "hash") VALUES (('{' || $1::TEXT || '}')::INET[], $2::TEXT, $3::BYTEA, $4::BYTEA) RETURNING "id";`)
	getArcadeRecordingCount = prepare(`SELECT COUNT(*) FROM "arcade_recordings";`)
	getArcadeRecordingList = prepare(`SELECT "id", "uploaded_at", "hash", "game", "player_name", "score", "vanilla", "decode_failed_reason" FROM "arcade_recordings" ORDER BY "id" DESC LIMIT $2::INT OFFSET $1::INT;`)
	getArcadeRecording = prepare(`SELECT "hash", "data" FROM "arcade_recordings" WHERE "id" = $1::BIGINT;`)
	decodedArcadeRecording = prepare(`UPDATE "arcade_recordings" SET "decode_failed_reason" = $2::TEXT, "game" = $3::arcade_game, "player_name" = $4::CHAR(3), "score" = $5::BIGINT, "vanilla" = $6::BOOL WHERE "id" = $1::BIGINT;`)

	const arcadeHighScoresFields = `"id", "hash", "score", "player_name"`

	getArcadeHighScoresRecent = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "uploaded_at" > NOW() - INTERVAL '14 days' AND "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "id" DESC LIMIT $3::INT OFFSET $2::INT;`)
	getArcadeHighScoresWeekly = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "uploaded_at" > DATE_TRUNC('day', NOW()) - INTERVAL '7 days' AND "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "score" desc, "id" ASC LIMIT $3::INT OFFSET $2::INT;`)
	getArcadeHighScoresQuarterly = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "uploaded_at" > DATE_TRUNC('month', NOW()) - INTERVAL '3 months' AND "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "score" desc, "id" ASC LIMIT $3::INT OFFSET $2::INT;`)
	getArcadeHighScoresAllTime = prepare(`SELECT ` + arcadeHighScoresFields + ` FROM "arcade_recordings" WHERE "decode_failed_reason" IS NULL AND "vanilla" AND "score" IS NOT NULL AND "game" = $1::arcade_game ORDER BY "score" desc, "id" ASC LIMIT $3::INT OFFSET $2::INT;`)
}
