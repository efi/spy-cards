module SpyCards {
	export type Deck = CardDef[];
}

module SpyCards.Decks {
	export function decode(defs: CardDefs, code: string): Deck {
		return decodeBinary(defs, CrockfordBase32.decode(code));
	}

	export function decodeBinary(defs: CardDefs, buf: Uint8Array): Deck {
		if (buf.length < 2) {
			throw new Error("invalid deck code (too short)");
		}
		if (!(buf[0] & 0x80)) {
			return toDeck(defs, decodeShort(buf));
		}
		if (buf[0] === 0x80) {
			return toDeck(defs, decodeLongV0(buf));
		}
		if (buf[0] === 0x81) {
			return toDeck(defs, decodeLongV1(buf));
		}
		if (buf[0] === 0x82) {
			return toDeck(defs, decodeLongV2(buf));
		}
		throw new Error("invalid deck code (unrecognized first byte " + buf[0] + ")");
	}

	function decodeShort(buf: Uint8Array): CardData.CardID[] {
		const names: string[] = [];
		names.push(CardData.BossCardOrder[buf[0] >> 2]);
		names.push(CardData.MiniBossCardOrder[((buf[0] & 3) << 3) | (buf[1] >> 5)]);
		names.push(CardData.MiniBossCardOrder[buf[1] & 31]);
		function pushEnemyCard(index: number) {
			const attackerCardCount = 31;
			const name = index < attackerCardCount ?
				CardData.AttackerCardOrder[index] :
				CardData.EffectCardOrder[index - attackerCardCount];
			names.push(name);
		}
		for (let i = 2; i < buf.length;) {
			pushEnemyCard(buf[i] >> 2);
			i++;
			if (i >= buf.length) {
				break;
			}
			pushEnemyCard(((buf[i - 1] & 3) << 4) | (buf[i] >> 4));
			i++;
			if (i >= buf.length) {
				break;
			}
			pushEnemyCard(((buf[i - 1] & 15) << 2) | (buf[i] >> 6));
			i++;
			if (i === buf.length && (buf[i - 1] & 63) === 63) {
				break;
			}
			pushEnemyCard(buf[i - 1] & 63);
		}
		return names.map((name) => (CardData.GlobalCardID as any)[name]);
	}

	function decodeLongV0(buf: Uint8Array): CardData.CardID[] {
		return [].slice.call(buf, 1);
	}

	function decodeLongV1(buf: Uint8Array): CardData.CardID[] {
		const buf2 = [].slice.call(buf, 1);
		const ids: CardData.CardID[] = [];
		while (buf2.length) {
			ids.push(SpyCards.Binary.shiftUVarInt(buf2));
		}
		return ids;
	}

	function decodeLongV2(buf: Uint8Array): CardData.CardID[] {
		const buf2 = [].slice.call(buf, 1);
		const ids: CardData.CardID[] = [];
		while (buf2.length) {
			ids.push(SpyCards.Binary.shiftUVarInt(buf2) + 128);
		}
		return ids;
	}

	function toDeck(defs: CardDefs, ids: CardData.CardID[]): Deck {
		return ids.map((id) => {
			if (defs.cardsByID[id]) {
				return defs.cardsByID[id];
			}
			const placeholder = new CardDef();
			placeholder.id = id;
			return placeholder;
		});
	}

	export function loadSaved(defs: CardDefs, validOnly?: boolean): Deck[] {
		const storedSerialized = localStorage["spy-cards-decks-v0"];
		if (!storedSerialized) {
			return [];
		}
		const storedDecks: string[][] = JSON.parse(storedSerialized).filter((d: string[]) => Array.isArray(d) && d.indexOf("MissingCard?undefined?") === -1);
		const decks = storedDecks.map((s) => toDeck(defs, s.map((name) => CardData.cardByName(name))));
		return decks.filter((deck) => {
			if (!deck || !deck.length) {
				return false;
			}

			if (validOnly && deck.length !== defs.rules.cardsPerDeck) {
				return false;
			}

			let i = 0;
			for (let card of deck) {
				if (!card) {
					return false;
				}
				if (validOnly && !defs.cardsByID[card.id]) {
					return false;
				}
				if (validOnly && (defs.cardsByID[card.id].effectiveTP() === Infinity || defs.banned[card.id] || defs.unpickable[card.id])) {
					return false;
				}
				const expectedBack = i < 3 ? i === 0 ? 2 : 1 : 0;
				if (card.getBackID() !== expectedBack) {
					return false;
				}
				i++;
			}

			if (validOnly && defs.mode) {
				for (let filter of defs.mode.getAll(GameModeFieldType.DeckLimitFilter)) {
					if (deck.filter((c) => filter.match(c)).length > filter.count) {
						return false;
					}
				}
			}

			return deck[1] !== deck[2];
		});
	}

	export function encode(defs: CardDefs, deck: Deck): string {
		return CrockfordBase32.encode(encodeBinary(defs, deck));
	}

	export function encodeBacks(backs: (0 | 1 | 2)[]): Uint8Array {
		const buf = new Uint8Array(Math.floor((backs.length + 3) / 4));

		for (let i = 0; i < backs.length; i++) {
			switch (i & 3) {
				case 0:
					buf[i >> 2] = backs[i] << 6;
					break;
				case 1:
					buf[i >> 2] |= backs[i] << 4;
					break;
				case 2:
					buf[i >> 2] |= backs[i] << 2;
					break;
				case 3:
					buf[i >> 2] |= backs[i];
					break;
			}
		}

		switch (backs.length & 3) {
			case 1:
				buf[buf.length - 1] |= 0x3f;
				break;
			case 2:
				buf[buf.length - 1] |= 0xf;
				break;
			case 3:
				buf[buf.length - 1] |= 0x3;
				break;
		}

		return buf;
	}

	export function decodeBacks(buf: Uint8Array): (0 | 1 | 2)[] {
		const d: (0 | 1 | 2)[] = [];

		for (let i = 0; i < buf.length; i++) {
			for (let shift = 6; shift >= 0; shift -= 2) {
				switch ((buf[i] >> shift) & 3) {
					case 0:
						d.push(0);
						break;
					case 1:
						d.push(1);
						break;
					case 2:
						d.push(2);
						break;
					case 3:
						if (i !== buf.length - 1) {
							throw new Error("card: invalid unknown deck: ends at index " + i + " of " + buf.length);
						}

						for (let shift2 = shift; shift2 >= 0; shift2 -= 2) {
							const v = (buf[i] >> shift2) & 3;
							if (v !== 3) {
								throw new Error("card: invalid unknown deck: ends before non-padding value " + v);
							}
						}

						return d;
				}
			}
		}

		return d
	}

	export function encodeBinary(defs: CardDefs, deck: Deck): Uint8Array {
		if (!deck.length) {
			throw new Error("cannot encode empty deck");
		}

		if (deck.length >= 3 && deck.every((c) => c.id < 128) &&
			deck[0].rank() === Rank.Boss &&
			deck[1].rank() === Rank.MiniBoss &&
			deck[2].rank() === Rank.MiniBoss &&
			deck.slice(3).every((c) => c.getBackID() === 0)) {
			return encodeShort(deck);
		}

		if (deck.every((c) => c.id >= 128)) {
			const buf: number[] = [0x82];
			for (let card of deck) {
				Binary.pushUVarInt(buf, card.id - 128);
			}
			return new Uint8Array(buf);
		}

		if (deck.every((c) => c.id < 256)) {
			const buf: number[] = [0x80];
			for (let card of deck) {
				buf.push(card.id);
			}
			return new Uint8Array(buf);
		}

		const buf: number[] = [0x81];
		for (let card of deck) {
			SpyCards.Binary.pushUVarInt(buf, card.id);
		}
		return new Uint8Array(buf);
	}

	function encodeShort(deck: Deck): Uint8Array {
		const buf: number[] = [];
		let bitsRemaining = 0;

		function appendBits(x: number, n: number) {
			while (bitsRemaining < n) {
				if (bitsRemaining) {
					n -= bitsRemaining;
					buf[buf.length - 1] |= x >> n;
					x &= (1 << n) - 1;
					bitsRemaining = 0;
				} else {
					buf.push(0);
					bitsRemaining += 8;
				}
			}
			bitsRemaining -= n;
			buf[buf.length - 1] |= x << bitsRemaining;
		}

		function cardIndex(card: CardDef) {
			const name = CardData.GlobalCardID[card.id];
			switch (card.rank()) {
				case Rank.Boss:
					return (CardData.BossCardOrder as any)[name];
				case Rank.MiniBoss:
					return (CardData.MiniBossCardOrder as any)[name];
				case Rank.Attacker:
					return (CardData.AttackerCardOrder as any)[name];
				case Rank.Effect:
					return 31 + (CardData.EffectCardOrder as any)[name];
			}
		}

		appendBits(0, 1);
		appendBits(cardIndex(deck[0]), 5);
		appendBits(cardIndex(deck[1]), 5);
		appendBits(cardIndex(deck[2]), 5);
		for (let i = 3; i < deck.length; i++) {
			appendBits(cardIndex(deck[i]), 6);
		}
		if (bitsRemaining >= 6) {
			appendBits(63, 6);
		}
		return new Uint8Array(buf);
	}

	export function createDisplay(defs: CardDefs, deck: Deck): HTMLDivElement {
		const el = document.createElement("div");
		el.classList.add("deck-display");
		el.setAttribute("role", "list");
		for (let card of deck) {
			const cardEl = card.createEl(defs);
			cardEl.setAttribute("role", "listitem");
			el.appendChild(cardEl);
		}
		return el;
	}

	export function saveNewDeck(defs: CardDefs, deck: CardDef[]) {
		let decks = loadSaved(defs);
		decks.unshift(deck);
		let decksToSave = decks.map(function (toSave) {
			return toSave.map(function (card) {
				return card.originalName();
			});
		});
		decksToSave = decksToSave.filter(function (v) {
			const normalized = v.slice(0).sort().toString();
			for (let otherDeck of decksToSave) {
				if (otherDeck === v) {
					return true;
				}
				const other = otherDeck.slice(0).sort().toString();
				if (normalized === other) {
					return false;
				}
			}
			throw new Error("should be unreachable");
		});
		localStorage["spy-cards-decks-v0"] = JSON.stringify(decksToSave);
	}

	export function removeSavedDeck(defs: CardDefs, deck: CardDef[]) {
		let decks = loadSaved(defs).map(function (toSave) {
			return toSave.map(function (card) {
				return card.originalName();
			});
		});
		const normalized = deck.map(function (card) {
			return card.originalName();
		}).sort().toString();
		decks = decks.filter(function (d) {
			const other = d.slice(0).sort().toString();
			return normalized !== other;
		});
		localStorage["spy-cards-decks-v0"] = JSON.stringify(decks);
	}

	export function confirmDeck(status: HTMLParagraphElement, defs: CardDefs, deck: CardDef[], question: string, yesText: string, noText: string, allowEdit?: boolean): Promise<CardDef[]> {
		status.textContent = question;

		const btn1 = document.createElement("button");
		btn1.classList.add("btn1");
		btn1.textContent = yesText;
		const btn2 = document.createElement("button");
		btn2.classList.add("btn2");
		btn2.textContent = noText;

		const deckDisplay = createDisplay(defs, deck);

		const done = new Promise<CardDef[]>(function (resolve) {
			function cleanUp() {
				SpyCards.UI.remove(deckDisplay);
				SpyCards.UI.remove(btn1);
				SpyCards.UI.remove(btn2);
			}

			if (allowEdit) {
				deckDisplay.classList.add("allow-edit", "no-flip");
				deckDisplay.addEventListener("click", function (e) {
					let target = <HTMLElement>e.target;
					while (target && target.classList && !target.classList.contains("card")) {
						target = <HTMLElement>target.parentNode;
					}
					if (!target || !target.classList) {
						return;
					}

					let i = (<HTMLElement[]>[]).indexOf.call(deckDisplay.children, target);
					if (i === -1) {
						return;
					}

					cleanUp();
					deck = deck.slice(0);
					deck[i] = null;
					resolve(editDeck(status, defs, deck).then((editedDeck) => confirmDeck(status, defs, editedDeck, question, yesText, noText, allowEdit)));
				});
				for (let card of <HTMLElement[]>[].slice.call(deckDisplay.children, 0)) {
					card.setAttribute("role", "button");
					card.setAttribute("aria-label", "Remove " + card.getAttribute("aria-label"));
					card.tabIndex = 0;
					card.addEventListener("keydown", function (e) {
						if (SpyCards.disableKeyboard) {
							return;
						}
						if (e.code === "Space" || e.code === "Enter") {
							e.preventDefault();
							cleanUp();

							let i = (<HTMLElement[]>[]).indexOf.call(deckDisplay.children, this);
							if (i === -1) {
								return;
							}

							cleanUp();
							deck = deck.slice(0);
							deck[i] = null;
							resolve(editDeck(status, defs, deck).then((editedDeck) => confirmDeck(status, defs, editedDeck, question, yesText, noText, allowEdit)));
						}
					});
				}
			}

			btn1.addEventListener("click", function (e) {
				e.preventDefault();
				cleanUp();

				resolve(deck);
			});
			btn2.addEventListener("click", function (e) {
				e.preventDefault();
				cleanUp();

				resolve(null);
			});
		});

		status.parentNode.appendChild(deckDisplay);
		status.parentNode.appendChild(btn1);
		status.parentNode.appendChild(btn2);

		return done;
	}

	export async function editDeck(status: HTMLParagraphElement, defs: CardDefs, deck: CardDef[]): Promise<CardDef[]> {
		deck = deck.slice(0);

		for (let i = 0; i < deck.length; i++) {
			// normalize deck (replace undefined with null)
			if (!deck[i]) {
				deck[i] = null;
			}
		}

		const buildingDeckDisplay = document.createElement("div");
		buildingDeckDisplay.classList.add("building-deck-display", "no-flip");
		status.parentNode.appendChild(buildingDeckDisplay);

		for (let i = 0; i < deck.length; i++) {
			if (deck[i]) {
				buildingDeckDisplay.appendChild(deck[i].createEl(defs));
			} else {
				if (i < defs.rules.bossCards) {
					buildingDeckDisplay.appendChild(createUnknownCardEl(2));
				} else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
					buildingDeckDisplay.appendChild(createUnknownCardEl(1));
				} else {
					buildingDeckDisplay.appendChild(createUnknownCardEl(0));
				}
			}
		}

		let cardRemovedResolve: (x: null) => void;
		let cardRemoved = new Promise<null>((resolve) => cardRemovedResolve = resolve);

		buildingDeckDisplay.addEventListener("click", function (e) {
			let target = <HTMLElement>e.target;
			while (target && target.classList && !target.classList.contains("card")) {
				target = <HTMLElement>target.parentNode;
			}
			if (!target || !target.classList) {
				return;
			}

			let i = (<HTMLElement[]>[]).indexOf.call(buildingDeckDisplay.children, target);
			if (deck[i]) {
				deck[i] = null;
				target.classList.add("flip-exception");
				SpyCards.flipCard(target, true);
				cardRemovedResolve(null);
				cardRemoved = new Promise((resolve) => cardRemovedResolve = resolve);
			}
		});

		function addBuiltCard(card: CardDef, i: number) {
			if (!card) {
				return;
			}

			deck[i] = card;

			const el = card.createEl(defs);
			el.classList.add("flip-exception");
			SpyCards.flipCard(el, true);
			const pointer = buildingDeckDisplay.children[i];
			SpyCards.UI.replace(pointer, el);
			requestAnimationFrame(function () {
				SpyCards.flipCard(el, false);
				doSquish(el);
				setTimeout(function () {
					el.classList.remove("flip-exception");
				}, 1000);
			});
		}

		if (deck.indexOf(null) === -1) {
			status.textContent = "Select a card to replace.";
			await cardRemoved;
		}

		function unique(defs: CardDef[], used: CardDef[]): CardDef[] {
			return defs.filter((c) => used.indexOf(c) === -1);
		}

		while (true) {
			let i = deck.indexOf(null);
			if (i === -1) {
				break;
			}

			if (i < defs.rules.bossCards) {
				status.textContent = "Select a boss card.";
				const card = await selectCard(status, defs, unique(defs.cardsByBack[2], deck.slice(0, defs.rules.bossCards)), cardRemoved, deck);
				addBuiltCard(card, i);
			} else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
				const chosenMiniBoss = deck.slice(defs.rules.bossCards).slice(0, defs.rules.miniBossCards);
				status.textContent = chosenMiniBoss.some(Boolean) ? "Select another mini-boss card." : "Select a mini-boss card.";
				const card = await selectCard(status, defs, unique(defs.cardsByBack[1], chosenMiniBoss), cardRemoved, deck);
				addBuiltCard(card, i);
			} else {
				let remaining = 0;
				for (let c of deck) {
					if (!c) {
						remaining++;
					}
				}
				if (remaining === 1) {
					status.textContent = "Select 1 more card.";
				} else {
					status.textContent = "Select " + remaining + " more cards.";
				}
				const card = await selectCard(status, defs, defs.cardsByBack[0], cardRemoved, deck);
				addBuiltCard(card, i);
			}
		}

		SpyCards.UI.remove(buildingDeckDisplay);

		return deck;
	}

	export async function confirmSelectDeck(status: HTMLParagraphElement, defs: CardDefs, deck: CardDef[], startOver: () => Promise<CardDef[]>): Promise<CardDef[]> {
		deck = await confirmDeck(status, defs, deck, "Is this deck okay?", spyCardsVersionSuffix === "-custom" ? "I'M SO SORRY" : "Yes", "Start Over", true);
		if (deck) {
			saveNewDeck(defs, deck);

			return deck;
		}

		return startOver();
	}

	export async function selectDeck(status: HTMLParagraphElement, defs: CardDefs, skipLoad?: boolean, startOver?: () => Promise<CardDef[]>): Promise<CardDef[]> {
		startOver = startOver || selectDeck.bind(null, status, defs, skipLoad, startOver);

		if (!skipLoad) {
			let decks = loadSaved(defs, true);

			const sg = SpoilerGuard.getSpoilerGuardData();
			if (sg && sg.m && (sg.m & 1)) {
				decks = [];
				const npc = new AI.GenericNPC2();
				for (let i = 0; i < 10; i++) {
					decks.push(await npc.createDeck(defs));
				}
			}

			if (decks.length) {
				status.textContent = "Select a deck.";

				const savedDeck = await selectSavedDeck(status, defs, decks);
				if (savedDeck) {
					return await confirmSelectDeck(status, defs, savedDeck, startOver);
				}
			}
		}

		const builtDeck = await editDeck(status, defs, new Array(defs.rules.cardsPerDeck));
		return await confirmSelectDeck(status, defs, builtDeck, startOver);
	}

	export async function selectSavedDeck(status: HTMLParagraphElement, defs: CardDefs, decks: CardDef[][], showDelete?: boolean): Promise<CardDef[]> {
		const selector = document.createElement("div");
		if (!status.id) {
			status.id = UI.uniqueID();
		}
		selector.setAttribute("aria-labelledby", status.id);
		selector.classList.add("deck-selector", "no-flip");
		selector.setAttribute("role", "listbox");
		if (showDelete) {
			selector.classList.add("has-delete-button");
		}
		status.parentNode.insertBefore(selector, status.nextSibling);

		return new Promise(function (resolve) {
			const createNewDeck = document.createElement("div");
			createNewDeck.id = UI.uniqueID();
			createNewDeck.classList.add("deck");
			createNewDeck.appendChild(createUnknownCardEl(2));
			createNewDeck.appendChild(createUnknownCardEl(1));
			createNewDeck.appendChild(createUnknownCardEl(1));
			for (let i = 0; i < 12; i++) {
				createNewDeck.appendChild(createUnknownCardEl(0));
			}
			const createNewDeckHint = document.createElement("div");
			createNewDeckHint.classList.add("create-new-deck-hint");
			createNewDeckHint.textContent = "Create New Deck";
			createNewDeckHint.id = UI.uniqueID();
			createNewDeck.appendChild(createNewDeckHint);
			createNewDeck.setAttribute("aria-labelledby", createNewDeckHint.id);
			createNewDeck.addEventListener("click", function () {
				SpyCards.UI.remove(selector);
				resolve(null);
			});
			createNewDeck.setAttribute("role", "option");
			createNewDeck.setAttribute("aria-selected", "false");
			createNewDeck.tabIndex = 0;
			selector.appendChild(createNewDeck);
			createNewDeck.addEventListener("focus", () => {
				createNewDeck.setAttribute("aria-selected", "true");
				selector.setAttribute("aria-activedescendant", createNewDeck.id);
			});
			createNewDeck.addEventListener("blur", () => {
				createNewDeck.setAttribute("aria-selected", "false");
			});
			createNewDeck.focus();

			let firstDeckEl: HTMLElement;
			let lastDeckEl: HTMLElement;
			createNewDeck.addEventListener("keydown", function (e) {
				if (SpyCards.disableKeyboard) {
					return;
				}
				if (e.code === "Space" || e.code === "Enter") {
					e.preventDefault();
					SpyCards.UI.remove(selector);
					resolve(null);
					return;
				}
				if (e.code === "Home") {
					e.preventDefault();
					return;
				}
				if (e.code === "End") {
					e.preventDefault();
					lastDeckEl.focus();
					return;
				}
				if (e.code === "ArrowUp") {
					e.preventDefault();
					return;
				}
				if (e.code === "ArrowDown") {
					e.preventDefault();
					firstDeckEl.focus();
					return;
				}
			});

			const deckEls: HTMLElement[] = [];
			for (let deck of decks) {
				const deckEl = document.createElement("div");
				deckEl.classList.add("deck");
				deckEl.setAttribute("aria-label", "Deck: " + deck.map((c) => c.displayName()).join(", "));
				for (let card of deck) {
					const cardEl = card.createEl(defs);
					cardEl.setAttribute("role", "presentation");
					deckEl.appendChild(cardEl);
				}
				if (showDelete) {
					const deleteButton = document.createElement("button");
					deleteButton.classList.add("delete");
					deleteButton.textContent = "Delete";
					deleteButton.addEventListener("click", (function (deck) {
						return function (e: MouseEvent) {
							e.preventDefault();
							e.stopPropagation();

							const savedStatus = status.textContent;
							SpyCards.UI.remove(selector);
							confirmDeck(status, defs, deck, "Really delete this deck?", "Yes", "Go Back").then(function (deck) {
								if (deck) {
									removeSavedDeck(defs, deck);
									SpyCards.UI.remove(deckEl);
								}

								status.textContent = savedStatus;
								status.parentNode.appendChild(selector);
							});
						};
					})(deck));
					deckEl.appendChild(deleteButton);
				}
				if (deck.some(c => c.effectiveTP() === Infinity || !defs.cardsByID[c.id])) {
					deckEl.setAttribute("role", "option");
					deckEl.setAttribute("aria-disabled", "true");
					deckEl.classList.add("no-select");
				} else {
					deckEls.push(deckEl);
					if (!firstDeckEl) {
						firstDeckEl = deckEl;
					}
					lastDeckEl = deckEl;
					deckEl.id = UI.uniqueID();
					deckEl.setAttribute("role", "option");
					deckEl.setAttribute("aria-selected", "false");
					deckEl.tabIndex = 0;
					deckEl.addEventListener("focus", () => {
						deckEl.setAttribute("aria-selected", "true");
						selector.setAttribute("aria-activedescendant", deckEl.id);
					});
					deckEl.addEventListener("blur", () => {
						deckEl.setAttribute("aria-selected", "false");
					});
					deckEl.addEventListener("click", (function (deck) {
						return function () {
							SpyCards.UI.remove(selector);
							resolve(deck);
						};
					})(deck));
					deckEl.addEventListener("keydown", (function (deck) {
						return function (e: KeyboardEvent) {
							if (SpyCards.disableKeyboard) {
								return;
							}
							if (e.code === "Space" || e.code === "Enter") {
								e.preventDefault();
								SpyCards.UI.remove(selector);
								resolve(deck);
								return;
							}
							if (e.code === "Home") {
								e.preventDefault();
								createNewDeck.focus();
								return;
							}
							if (e.code === "End") {
								e.preventDefault();
								lastDeckEl.focus();
								return;
							}
							if (e.code === "ArrowUp") {
								e.preventDefault();
								const prev = deckEls[deckEls.indexOf(deckEl) - 1];
								if (prev) {
									prev.focus();
								} else {
									createNewDeck.focus();
								}
								return;
							}
							if (e.code === "ArrowDown") {
								e.preventDefault();
								const next = deckEls[deckEls.indexOf(deckEl) + 1];
								if (next) {
									next.focus();
								}
								return;
							}
						};
					})(deck));
				}
				selector.appendChild(deckEl);
			}
		});
	}

	let lastSelCardPrev: CardDef = null;
	export async function selectCard(status: HTMLParagraphElement, defs: CardDefs, choices: CardDef[], cancel?: Promise<null>, alreadySelected?: Deck): Promise<CardDef> {
		const selector = document.createElement("div");
		selector.classList.add("card-selector", "no-flip");
		selector.setAttribute("role", "listbox");

		const selected = new Promise<CardDef>(function (resolve) {
			disableDoSquish = true;

			choices.filter((card) => {
				return !defs.banned[card.id] && !defs.unpickable[card.id];
			}).filter((card) => {
				if (!defs.mode || !alreadySelected) {
					return true;
				}

				for (let filter of defs.mode.getAll(GameModeFieldType.DeckLimitFilter)) {
					if (!filter.match(card)) {
						continue;
					}

					if (alreadySelected.filter((c) => c && filter.match(c)).length >= filter.count) {
						return false;
					}
				}

				return true;
			}).forEach(function (card, i, filteredChoices) {
				if (i === 0) {
					requestAnimationFrame(() => {
						const nextEl = selector.children[filteredChoices.indexOf(lastSelCardPrev) + 1] as HTMLElement;
						if (nextEl) {
							nextEl.focus();
						} else if (selector.children.length) {
							(selector.children[selector.children.length - 1] as HTMLElement).focus();
						}
					});
				}
				const el = card.createEl(defs);
				el.id = UI.uniqueID();
				el.setAttribute("role", "option");
				el.setAttribute("aria-selected", "false");
				el.tabIndex = 0;
				el.addEventListener("focus", () => {
					el.setAttribute("aria-selected", "true");
					selector.setAttribute("aria-activedescendant", el.id);
				});
				el.addEventListener("blur", () => el.setAttribute("aria-selected", "false"));
				selector.appendChild(el);
				el.addEventListener("click", () => {
					resolve(card);
					lastSelCardPrev = filteredChoices[i - 1];
				});
				el.addEventListener("keydown", (e) => {
					if (SpyCards.disableKeyboard) {
						return;
					}
					if (e.code === "ArrowDown" || e.code === "ArrowRight") {
						e.preventDefault();
						if (el.nextSibling) {
							(el.nextSibling as HTMLElement).focus();
						} else {
							(selector.firstChild as HTMLElement).focus();
						}
						return;
					}
					if (e.code === "ArrowUp" || e.code === "ArrowLeft") {
						e.preventDefault();
						if (el.previousSibling) {
							(el.previousSibling as HTMLElement).focus();
						} else {
							(selector.lastChild as HTMLElement).focus();
						}
						return;
					}
					if (e.code === "Space" || e.code === "Enter") {
						e.preventDefault();
						el.click();
						return;
					}
				});
			});

			requestAnimationFrame(function () {
				disableDoSquish = false;
				doSquish(selector);
			});
		});

		status.parentNode.appendChild(selector);

		const promises = [selected];
		if (cancel) {
			promises.push(cancel);
		}

		const selectedCard = await Promise.race(promises);

		SpyCards.UI.remove(selector);

		return selectedCard;
	}
}
