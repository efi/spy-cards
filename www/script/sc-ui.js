"use strict";
var SpyCards;
(function (SpyCards) {
    var UI;
    (function (UI) {
        UI.html = document.querySelector("html");
        let nextUniqueID = 0;
        function uniqueID() {
            return "_tmp_" + (nextUniqueID++);
        }
        UI.uniqueID = uniqueID;
        function replace(from, to) {
            if (from && from.parentNode && to) {
                from.parentNode.insertBefore(to, from);
                from.parentNode.removeChild(from);
            }
            else {
                remove(from);
                remove(to);
            }
        }
        UI.replace = replace;
        function remove(el) {
            if (el && el.parentNode) {
                el.parentNode.removeChild(el);
            }
        }
        UI.remove = remove;
        function clear(el) {
            if (!el) {
                return;
            }
            el.textContent = "";
        }
        UI.clear = clear;
        function button(label, classes, click) {
            const btn = document.createElement("button");
            btn.classList.add(...classes);
            btn.textContent = label;
            btn.addEventListener("click", (e) => {
                e.preventDefault();
                click();
            });
            return btn;
        }
        UI.button = button;
        function option(label, value) {
            const opt = document.createElement("option");
            opt.textContent = label;
            opt.value = value;
            return opt;
        }
        UI.option = option;
    })(UI = SpyCards.UI || (SpyCards.UI = {}));
})(SpyCards || (SpyCards = {}));
