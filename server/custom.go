package server

import (
	"database/sql"
	"encoding/base64"
	"encoding/json"
	"html/template"
	"log"
	"net/http"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"golang.org/x/xerrors"
)

var tmplGameMode = template.Must(template.New("game-mode").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Online - Custom Game Mode Editor</title>
<meta charset="utf-8">
</head>
<body>
<table border="1">
<tbody>
<tr>
<th>Client URL</th>
<td><code><a href="https://spy-cards.lubar.me/play.html?mode={{.Mode.Client}}">/play.html?mode=<strong>{{.Mode.Client}}</strong></a></code></td>
</tr>
<tr>
<th>Title</th>
<td>{{.Mode.Name}}</td>
</tr>
<tr>
<th>Matchmaking Prefix</th>
<td>{{if .Mode.Prefix.Valid}}<code>{{.Mode.Prefix.String}}</code>{{else}}<em>(quick play not enabled)</em>{{end}}</td>
</tr>
<tr>
<th>Latest Revision</th>
<td>{{if .Mode.Revisions}}<a href="https://spy-cards.lubar.me/play.html?mode={{.Mode.Client}}&amp;rev={{.Mode.Revisions}}">{{.Mode.Revisions}}</a>{{else}}(none){{end}}</td>
</tr>
</tbody>
</table>
<table border="1">
<thead>
<tr>
<th>Revision</th>
<th>Card Codes</th>
</tr>
</thead>
<tbody>
{{range .Sets -}}
<tr>
<th><a href="https://spy-cards.lubar.me/play.html?mode={{$.Mode.Client}}&amp;rev={{.Revision}}">{{.Revision}}</a></th>
<td><textarea readonly>{{.Cards}}</textarea></td>
</tr>
{{end -}}
<tr>
<th>New</th>
<td><form method="post">
<textarea required name="cards"></textarea>
<br>
<button type="submit">Create New Revision</button>
<input type="hidden" name="csrf-token" value="{{.CSRFToken}}">
</form></td>
</tr>
</tbody>
</table>
</body>
</html>
`))

func handleCustomCardAPI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	path := strings.Split(strings.TrimPrefix(r.URL.Path, "/spy-cards/custom/api/"), "/")
	if (len(path) == 2 && path[0] == "latest") || (len(path) == 3 && path[0] == "get-revision") {
		var response struct {
			Name      string
			Cards     string
			Revision  int
			QuickJoin bool `json:",omitempty"`
		}

		mode := path[1]

		var rev sql.NullString

		if len(path) == 3 {
			rev.String = path[2]
			rev.Valid = true
		}

		if err := getCustomCardSet.QueryRow(mode, rev).Scan(&response.Name, &response.Cards, &response.Revision); xerrors.Is(err, sql.ErrNoRows) {
			http.NotFound(w, r)

			return
		} else if err != nil {
			log.Println("custom card API error", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if matchCodePrefix("-"+mode) != nil {
			response.QuickJoin = true
		}

		if rev.Valid {
			w.Header().Set("Cache-Control", "public, max-age=86400")
		} else {
			w.Header().Set("Cache-Control", "public, max-age=30, s-maxage=5")
		}

		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(&response)

		return
	}

	http.NotFound(w, r)
}

func editCustomMode(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Cache-Control", "no-store")

	cu, _ := getCurrentUser(&r)
	if cu == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    r.URL.RequestURI(),
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})
		http.Redirect(w, r, "/spy-cards/log-in", http.StatusFound)

		return
	}

	id, err := strconv.ParseInt(r.URL.Path[len("/spy-cards/custom/edit-mode/"):], 10, 64)
	if err != nil {
		http.NotFound(w, r)

		return
	}

	type ModeRevision struct {
		Revision int
		Cards    string
	}

	var data struct {
		Mode      GameMode
		Sets      []ModeRevision
		CSRFToken string
	}

	data.Mode.ID = id

	var ownerID int64

	err = getCustomGameMode.QueryRow(id).Scan(&data.Mode.Client, &data.Mode.Name, &data.Mode.Revisions, &ownerID, &data.Mode.Prefix)
	if err != nil && xerrors.Is(err, sql.ErrNoRows) {
		log.Println("get custom game mode", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if xerrors.Is(err, sql.ErrNoRows) || ownerID != cu.ID {
		http.Error(w, "this is not your game mode", http.StatusForbidden)

		return
	}

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		if r.FormValue("cards") == "" {
			http.Error(w, "missing cards", http.StatusBadRequest)

			return
		}

		ids, err := validateCustomCardSet(r.FormValue("cards"))
		if err != nil {
			http.Error(w, "cards failed validation: "+err.Error(), http.StatusBadRequest)

			return
		}

		tx, err := begin()
		if err != nil {
			log.Println("create custom card set begin", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		defer tx.Rollback()

		var setID int64

		rev := data.Mode.Revisions + 1

		createSet := tx.Stmt(createCustomCardSet)
		defer createSet.Close()

		if err = createSet.QueryRow(id, r.Header.Get("X-Forwarded-For"), rev).Scan(&setID); err != nil {
			log.Println("create custom card set create set", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		addCard := tx.Stmt(addCardToCustomSet)
		defer addCard.Close()

		for i, cardID := range ids {
			if _, err = addCard.Exec(setID, cardID, i+1); err != nil {
				log.Println("create custom card set add card", err)
				http.Error(w, "database error", http.StatusInternalServerError)

				return
			}
		}

		setLatest := tx.Stmt(setLatestGameModeRevision)
		defer setLatest.Close()

		if _, err = setLatest.Exec(id, rev); err != nil {
			log.Println("create custom card set update revision", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		if err = tx.Commit(); err != nil {
			log.Println("create custom card set commit", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		http.Redirect(w, r, r.URL.Path, http.StatusFound)

		return
	}

	rows, err := getCustomGameModeSets.Query(id)
	if err != nil {
		log.Println("get custom game mode revisions", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var rev ModeRevision

		err = rows.Scan(&rev.Revision, &rev.Cards)
		if err != nil {
			log.Println("get custom game mode revisions", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		data.Sets = append(data.Sets, rev)
	}

	if err := rows.Err(); err != nil {
		log.Println("get custom game mode revisions", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_ = tmplGameMode.Execute(w, &data)
}

func validateCustomCardSet(cardCodes string) ([]int64, error) {
	overridden := make(map[card.ID]bool)
	codes := strings.Split(cardCodes, ",")
	ids := make([]int64, len(codes))

	for i, code := range codes {
		b, err := base64.StdEncoding.DecodeString(code)
		if err != nil {
			return nil, xerrors.Errorf("for card code %q: %w", code, err)
		}

		if i == 0 {
			var gm card.GameMode

			err = gm.UnmarshalBinary(b)
			if err != nil && b[0] != 3 { // current game mode format version
				return nil, xerrors.New("custom game modes must contain a Game Mode Metadata entry. add one and leave the title blank.")
			}

			if err != nil {
				return nil, xerrors.Errorf("expected game mode definition at start: %w", err)
			}

			haveMetadata := false

			for _, gmf := range gm.Fields {
				if md, ok := gmf.(*card.Metadata); ok {
					if haveMetadata {
						return nil, xerrors.New("multiple metadata fields in game mode definition")
					}

					haveMetadata = true

					if md.Title != "" {
						return nil, xerrors.New("game mode title field should be left blank for official custom game modes")
					}
				}
			}

			if !haveMetadata {
				return nil, xerrors.New("missing game mode metadata")
			}
		} else {
			var card card.Def
			err = card.UnmarshalBinary(b)
			if err != nil {
				return nil, xerrors.Errorf("for card code %q: %w", code, err)
			}

			if card.Portrait == 254 {
				return nil, xerrors.Errorf("for card code %q: portrait is embedded in card; reupload card portrait for a shorter card code", code)
			}

			if overridden[card.ID] {
				return nil, xerrors.Errorf("card %d is overridden multiple times by this set", int(card.ID))
			}
			overridden[card.ID] = true
		}

		if err = insertCustomCard.QueryRow(b).Scan(&ids[i]); err != nil {
			return nil, xerrors.Errorf("for card code %q: %w", code, err)
		}
	}

	return ids, nil
}
