var disableDoSquish = false;
function doSquish(el: ParentNode) {
	if (disableDoSquish) {
		return;
	}
	el.querySelectorAll<HTMLElement>(".squish").forEach((s) => {
		const max = parseInt(getComputedStyle(s.parentElement).width.replace("px", ""), 10);
		s.style.transform = "";
		const current = s.clientWidth;
		if (current > max) {
			s.style.transform = "scaleX(calc(" + max + " / " + current + "))";
		}
	});
	el.querySelectorAll<HTMLElement>(".squishy").forEach((s) => {
		const max = parseInt(getComputedStyle(s.parentElement).height.replace("px", ""), 10);
		s.style.transform = "";
		const current = s.clientHeight;
		if (current > max) {
			s.style.transform = "scaleY(calc(" + max + " / " + current + "))";
		}
	});
}

addEventListener("load", function () {
	doSquish(document);
});

function sleep(ms: number): Promise<void> {
	return new Promise(function (resolve) {
		setTimeout(function () {
			resolve();
		}, ms);
	});
}

module SpyCards.Binary {
	// JavaScript bitwise operators work on signed 32-bit integers.
	// These helper functions work around this.
	export function bit(n: number): number {
		let base = 1;
		while (n > 30) {
			base *= 1 << 30;
			n -= 30;
		}
		return (1 << n) * base;
	}
	export function getBit(x: number, n: number): 0 | 1 {
		while (n > 30) {
			x /= 1 << 30;
			n -= 30;
		}
		return <0 | 1>((x >> n) & 1);
	}
	export function setBit(x: number, n: number, b: 0 | 1): number {
		const ob = getBit(x, n);
		if (b === ob) {
			return x;
		}
		return b ? x + bit(n) : x - bit(n);
	}

	export function pushUVarInt(buf: number[], x: number) {
		while (x >= 0x80) {
			buf.push(x & 0xff | 0x80);
			x /= 1 << 7;
		}
		buf.push(x | 0);
	}

	export function pushSVarInt(buf: number[], x: number) {
		let ux = x * 2;
		if (x < 0) {
			ux = -ux + 1;
		}
		pushUVarInt(buf, ux);
	}

	export function shiftUVarInt(buf: number[]): number {
		let x = 0, s = 0;
		while (buf.length) {
			const b = buf.shift();
			x += (b & 0x7f) * Math.pow(2, s);
			s += 7;
			if (b < 0x80) {
				return x;
			}
		}
		throw new Error("reached end of buffer");
	}

	export function shiftSVarInt(buf: number[]): number {
		const ux = shiftUVarInt(buf);
		let x = ux / 1;
		if (ux & 1) {
			x = -x - 0.5;
		}
		return x;
	}

	export function pushUTF8String1(buf: number[], s: string) {
		const b = new TextEncoder().encode(s || "");

		if (b.length > 255) {
			throw new Error("string length (" + b.length + " bytes) longer than maximum (255)");
		}

		buf.push(b.length);
		buf.push(...toArray(b));
	}

	export function shiftUTF8String1(buf: number[]): string {
		const length = buf.shift();

		if (buf.length < length) {
			throw new Error("buffer too short to hold " + length + "-byte string");
		}

		const b = new Uint8Array(buf.splice(0, length));
		return new TextDecoder().decode(b);
	}

	export function pushUTF8StringVar(buf: number[], s: string) {
		const b = new TextEncoder().encode(s || "");

		pushUVarInt(buf, b.length);
		buf.push(...toArray(b));
	}

	export function shiftUTF8StringVar(buf: number[]): string {
		const length = shiftUVarInt(buf);

		if (buf.length < length) {
			throw new Error("buffer too short to hold " + length + "-byte string");
		}

		const b = new Uint8Array(buf.splice(0, length));
		return new TextDecoder().decode(b);
	}
}

module SpyCards {
	export interface CustomCardSet {
		Name: string;
		Cards: string;
		Revision: number;
		QuickJoin?: boolean;
	}

	export async function fetchCustomCardSet(name: string, revision?: number): Promise<CustomCardSet> {
		const url = revision ?
			(custom_card_api_base_url + "get-revision/" + encodeURIComponent(name) + "/" + encodeURIComponent(revision)) :
			(custom_card_api_base_url + "latest/" + encodeURIComponent(name));
		const response = await fetch(url, { mode: "cors" });
		if (response.status !== 200) {
			throw new Error("Server returned " + response.status + " " + response.statusText);
		}
		return await response.json();
	}

	export interface ParsedCustomCards {
		mode?: GameModeData;
		cards: CardDef[];
		packedVersion?: number;
		variant?: number;
		customCardsRaw: string[];
	}

	export async function parseCustomCards(codes: string, variant: number | "parse-variant" | "ignore-variant", allowInvalid?: boolean): Promise<ParsedCustomCards> {
		let mode: GameModeData = null;
		const customCardsRaw: string[] = [];
		const cards: CardDef[] = [];
		const usedIDs: number[] = [];
		for (let group of codes.split(";")) {
			if (group.indexOf(".") !== -1 && group.indexOf(",") === -1) {
				const [modeName, revisionStr] = group.split(/\./);
				const revision = parseInt(revisionStr, 10);
				const mode = await fetchCustomCardSet(modeName, revision);
				group = mode.Cards;
			}

			let groupMode: GameModeData = null;
			const groupCards: CardDef[] = [];
			const groupUsedIDs: number[] = [];
			const idsToReplace: number[] = [];
			let first = true;
			let second = false;
			for (let code of group.split(",")) {
				const codeBuf = toArray(Base64.decode(code));
				if (first) {
					first = false;
					if (codeBuf[0] === 3) {
						groupMode = new GameModeData();
						groupMode.unmarshal(codeBuf);
						if (mode) {
							mode.fields = mode.fields.concat(groupMode.fields);
						} else {
							mode = groupMode;
						}
						second = true;
						continue;
					}
				}
				if (second) {
					second = false;

					const codeBufClone = codeBuf.slice(0);
					const encodedVariant = Binary.shiftUVarInt(codeBufClone);

					if (codeBufClone.length === 0) {
						if (variant === "parse-variant") {
							variant = encodedVariant;
						}

						continue;
					}
				}
				const card = new CardDef();
				card.unmarshal(codeBuf);
				if (groupUsedIDs.indexOf(card.id) !== -1) {
					throw new Error("multiple cards replacing ID " + card.id + " (" + card.originalName() + ")");
				}
				groupUsedIDs.push(card.id);
				if (usedIDs.indexOf(card.id) !== -1) {
					if (card.id < 128) {
						if (allowInvalid) {
							continue;
						}
						throw new Error("multiple card groups replacing ID " + card.id + " (" + card.originalName() + ")");
					}
					idsToReplace.push(card.id);
				} else {
					usedIDs.push(card.id);
				}
				groupCards.push(card);
			}
			for (let id of idsToReplace) {
				let replacementIDIndex = -1;
				let replacementID: number;
				do {
					replacementIDIndex++;
					replacementID = 128 + ((replacementIDIndex & ~31) << 2) + (replacementIDIndex & 31) + (id & 96);
				} while (usedIDs.indexOf(replacementID) !== -1 || groupUsedIDs.indexOf(replacementID) !== -1);
				usedIDs.push(replacementID);

				if (groupMode) {
					for (let field of groupMode.fields) {
						field.replaceCardID(id, replacementID);
					}
				}

				for (let card of groupCards) {
					if (card.id === id) {
						card.id = replacementID;
					}
					for (let effect of card.effects) {
						replaceID(effect, id, replacementID);
					}
				}
			}
			cards.push(...groupCards);

			if (customCardsRaw.length) {
				customCardsRaw.length = 0;
				if (mode) {
					const buf: number[] = [];
					mode.marshal(buf);
					customCardsRaw.push(Base64.encode(new Uint8Array(buf)));
				}
				for (let card of cards) {
					const buf: number[] = [];
					card.marshal(buf);
					customCardsRaw.push(Base64.encode(new Uint8Array(buf)));
				}
			} else {
				customCardsRaw.push(...group.split(","));
			}
		}

		const variants = mode ? mode.getAll(GameModeFieldType.Variant) : [];
		if (variant === "parse-variant") {
			variant = 0;
		}
		if (variant !== "ignore-variant" && variants.length) {
			const variantID: number[] = [];
			variant = Math.min(Math.max(variant || 0, 0), variants.length - 1);
			Binary.pushUVarInt(variantID, variant);
			customCardsRaw.splice(1, 0, Base64.encode(new Uint8Array(variantID)));
			mode.fields.push(...variants[variant].rules);
		} else {
			variant = null;
		}

		return { mode, cards, customCardsRaw, variant: <number>variant };

		function replaceID(effect: EffectDef, oldID: number, newID: number) {
			if (effect.card === oldID) {
				effect.card = newID;
			}
			if (effect.result) {
				replaceID(effect.result, oldID, newID);
			}
			if (effect.tailsResult) {
				replaceID(effect.tailsResult, oldID, newID);
			}
		}
	}

	export interface Settings {
		audio: {
			music: number;
			sounds: number;
		};
		character?: string;

		disable3D?: boolean;
		forceRelay?: boolean;
		disableCRT?: boolean;

		lastTermacadeOption?: number;
		lastTermacadeName?: string;

		autoUploadRecording?: boolean;
		displayTermacadeButtons?: boolean;

		controls?: {
			keyboard?: number;
			gamepad?: { [id: string]: number };
			customKB?: {
				name: string;
				code: {
					[btn: number]: string;
				};
			}[];
			customGP?: {
				name: string;
				button: {
					[btn: number]: number | [number, boolean];
				};
				style: ButtonStyle;
			}[];
		}
	}

	export function loadSettings(): Settings {
		if (localStorage["spy-cards-settings-v0"]) {
			return JSON.parse(localStorage["spy-cards-settings-v0"]);
		}

		const settings: Settings = {
			audio: {
				music: 0,
				sounds: 0
			}
		};

		if (localStorage["spy-cards-audio-settings-v0"]) {
			const legacyAudio: {
				enabled?: boolean;
				musicEnabled?: boolean;
				sfxEnabled?: boolean;
			} = JSON.parse(localStorage["spy-cards-audio-settings-v0"]);

			settings.audio.music = legacyAudio.enabled || legacyAudio.musicEnabled ? 0.6 : 0;
			settings.audio.sounds = legacyAudio.enabled || legacyAudio.sfxEnabled ? 0.6 : 0;
		}

		if (localStorage["spy-cards-player-sprite-v0"]) {
			settings.character = localStorage["spy-cards-player-sprite-v0"];
		}

		saveSettings(settings);
		delete localStorage["spy-cards-audio-settings-v0"];
		delete localStorage["spy-cards-player-sprite-v0"];

		return settings;
	}

	export function saveSettings(settings: Settings) {
		localStorage["spy-cards-settings-v0"] = JSON.stringify(settings);

		if (navigator.serviceWorker && navigator.serviceWorker.controller) {
			navigator.serviceWorker.controller.postMessage({ type: "settings-changed" });
		}

		window.dispatchEvent(new Event("spy-cards-settings-changed"));
	}
}
