// +build android darwin,arm darwin,arm64
// +build !headless

package main

import (
	"log"
	"os"
	"runtime"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/spoilerguard"
	"golang.org/x/mobile/app"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/exp/audio/al"
	"golang.org/x/mobile/gl"
)

var (
	heldInputs []arcade.Button
	inputTime  time.Time
	inputLock  sync.Mutex
)

func appMain(ictx *input.Context) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	var (
		isVisible bool

		frameSyncFinished = make(chan struct{})
	)

	app.Main(func(a app.App) {
		defer al.CloseDevice()

		for e := range a.Events() {
			switch e := a.Filter(e).(type) {
			case lifecycle.Event:
				switch e.Crosses(lifecycle.StageVisible) {
				case lifecycle.CrossOn:
					gfx.Init(e.DrawContext.(gl.Context))

					isVisible = true

					a.Send(paint.Event{})
				case lifecycle.CrossOff:
					isVisible = false

					gfx.Release()

					if runtime.GOOS == "windows" {
						os.Exit(0) // hard-exit or we'll never exit on Windows
					}
				}

			case size.Event:
				gfx.SetSize(e)
				touchcontroller.Size(e)
				ictx.OnSize(e)

			case touch.Event:
				touchcontroller.Touch(e)
				ictx.OnTouch(e)

			case key.Event:
				if e.Modifiers&(key.ModControl|key.ModAlt|key.ModMeta) != 0 {
					continue
				}

				ictx.OnKey(e)
				spoilerguard.OnKey(e)

				var button arcade.Button

				switch e.Code {
				case key.CodeUpArrow:
					button = arcade.BtnUp
				case key.CodeDownArrow:
					button = arcade.BtnDown
				case key.CodeLeftArrow:
					button = arcade.BtnLeft
				case key.CodeRightArrow:
					button = arcade.BtnRight
				case key.CodeC:
					button = arcade.BtnConfirm
				case key.CodeX:
					button = arcade.BtnCancel
				case key.CodeZ:
					button = arcade.BtnSwitch
				case key.CodeV:
					button = arcade.BtnToggle
				case key.CodeEscape:
					button = arcade.BtnPause
				case key.CodeReturnEnter:
					button = arcade.BtnHelp
				default:
					// unhandled key

					continue
				}

				inputLock.Lock()

				switch e.Direction {
				case key.DirPress:
					heldInputs = append(heldInputs, button)
				case key.DirRelease:
					for i := 0; i < len(heldInputs); i++ {
						if heldInputs[i] == button {
							heldInputs = append(heldInputs[:i], heldInputs[i+1:]...)
							i--
						}
					}
				}

				inputTime = time.Now()

				inputLock.Unlock()

			case paint.Event:
				if e.External || !isVisible {
					continue
				}

				gfx.FrameSync <- frameSyncFinished
				<-frameSyncFinished
				touchcontroller.Render()
				a.Publish()
				a.Send(paint.Event{})
			}
		}
	})
}

func getInputs() []arcade.Button {
	inputLock.Lock()

	i := append([]arcade.Button(nil), heldInputs...)

	touchHeld, touchTime := touchcontroller.Held()
	i = append(i, touchHeld...)

	if touchTime.After(inputTime) {
		sprites.ButtonStyle = internal.StyleGenericGamepad
	} else {
		sprites.ButtonStyle = internal.StyleKeyboard
	}

	inputLock.Unlock()

	return i
}
