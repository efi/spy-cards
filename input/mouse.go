package input

import "golang.org/x/mobile/event/key"

// Mouse returns the mouse position and pressed state.
//
// This API may change in the future.
func (c *Context) Mouse() (x, y float32, click bool) {
	return c.state.x, c.state.y, c.state.click
}

// LastMouse returns the mouse state for the previous tick.
//
// This API may change in the future.
func (c *Context) LastMouse() (x, y float32, click bool) {
	return c.state.lastX, c.state.lastY, c.state.lastClick
}

// IsMouseDrag returns true if the mouse has been dragged at least this many pixels.
//
// This API may change in the future.
func (c *Context) IsMouseDrag(dist float32) bool {
	return c.state.drag >= dist
}

// ConsumeWheel returns the mouse wheel delta and sets it to zero.
//
// This API may change in the future.
func (c *Context) ConsumeWheel() float32 {
	w := c.state.wheel
	c.state.wheel = 0

	return w
}

// ConsumeKey takes a key event from the queue.
//
// This API may change in the future.
func (c *Context) ConsumeKey() (key.Event, bool) {
	if len(c.state.key) == 0 {
		return key.Event{}, false
	}

	e := c.state.key[0]
	c.state.key = c.state.key[1:]

	return e, true
}
