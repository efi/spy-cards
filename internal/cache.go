package internal

import (
	"sync"

	"golang.org/x/sync/singleflight"
)

// Cache is a FIFO cache.
type Cache struct {
	MaxEntries int

	sf      singleflight.Group
	entries []*cacheEntry
	lock    sync.RWMutex
}

// Get returns the value and error stored for a given key.
func (c *Cache) Get(key string) (interface{}, bool, error) {
	c.lock.RLock()

	for _, e := range c.entries {
		if e.key == key {
			c.lock.RUnlock()

			return e.value, true, e.err
		}
	}

	c.lock.RUnlock()

	return nil, false, nil
}

// Do returns the cached result of f.
func (c *Cache) Do(key string, f func() (interface{}, error)) (interface{}, error) {
	c.lock.RLock()

	for _, e := range c.entries {
		if e.key == key {
			c.lock.RUnlock()

			return e.value, e.err
		}
	}

	c.lock.RUnlock()

	v, err, _ := c.sf.Do(key, func() (interface{}, error) {
		v, err := f()

		c.lock.Lock()

		if len(c.entries) >= c.MaxEntries {
			copy(c.entries, c.entries[1:])

			if len(c.entries) > 0 {
				c.entries[len(c.entries)-1] = &cacheEntry{
					key:   key,
					value: v,
					err:   err,
				}
			}
		} else {
			c.entries = append(c.entries, &cacheEntry{
				key:   key,
				value: v,
				err:   err,
			})
		}

		c.lock.Unlock()

		return v, err
	})

	return v, err //nolint:wrapcheck
}

type cacheEntry struct {
	key   string
	value interface{}
	err   error
}
