package format

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"

	"golang.org/x/xerrors"
)

// FileID is the decoded form of a file identifier for Spy Cards Online.FileID
//
// It contains the first 8 bytes of the SHA-256 hash of the file, along with
// an opaque integer identifier.
type FileID struct {
	Hash [8]byte
	ID   uint64
}

// String implements fmt.Stringer.
func (f FileID) String() string {
	b, err := f.MarshalText()
	if err != nil {
		panic(err) // should never happen
	}

	return string(b)
}

// IsMatch verifies that the file ID matches the hash of the data.
func (f FileID) IsMatch(data []byte) bool {
	hash := sha256.Sum256(data)

	// we don't need to use subtle here because the data is not secret
	return bytes.Equal(f.Hash[:], hash[:8])
}

// MarshalText implements encoding.TextMarshaler.
func (f FileID) MarshalText() ([]byte, error) {
	b, err := f.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(Encode32(b)), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (f *FileID) UnmarshalText(b []byte) error {
	p, err := Decode32(string(b))
	if err != nil {
		return xerrors.Errorf("format: parsing file ID: %w", err)
	}

	return f.UnmarshalBinary(p)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (f FileID) MarshalBinary() ([]byte, error) {
	var buf [8 + binary.MaxVarintLen64]byte

	copy(buf[:], f.Hash[:])
	n := binary.PutUvarint(buf[8:], f.ID)

	return buf[:8+n], nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (f *FileID) UnmarshalBinary(b []byte) error {
	if len(b) <= 8 {
		return xerrors.New("format: too short to be a file ID")
	}

	id, n := binary.Uvarint(b[8:])
	if n != len(b)-8 {
		return xerrors.New("format: invalid file ID")
	}

	copy(f.Hash[:], b)
	f.ID = id

	return nil
}
