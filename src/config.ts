const matchmaking_server = "wss://spy-cards.lubar.me/spy-cards/ws";
const issue_report_handler = "https://spy-cards.lubar.me/spy-cards/report-issue";
const user_image_base_url = "https://spy-cards.lubar.me/spy-cards/user-img/";
const user_images_whitelist: string[] = [];
const custom_card_api_base_url = "https://spy-cards.lubar.me/spy-cards/custom/api/";
const match_recording_base_url = "https://spy-cards.lubar.me/spy-cards/recording/";
const arcade_api_base_url = "https://spy-cards.lubar.me/spy-cards/arcade/api/";
