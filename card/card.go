// Package card implements data structures used by Spy Cards Online.
package card

import (
	"encoding/base64"

	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

const (
	// PortraitCustomEmbedded is an embedded PNG custom portrait.
	PortraitCustomEmbedded uint8 = 254
	// PortraitCustomExternal is a FileID of an external portrait.
	PortraitCustomExternal uint8 = 255
)

// Def is a card definition.
type Def struct {
	ID               ID
	Name             string
	Tribes           []TribeDef
	TP               int64
	legacyUnpickable bool
	Rank             Rank
	Portrait         uint8
	CustomPortrait   []byte
	Effects          []*EffectDef
	Extensions       []*Extension
}

// DisplayName returns the name of this card.
func (cd *Def) DisplayName() string {
	if cd.Name != "" {
		return cd.Name
	}

	return cd.ID.String()
}

// MarshalText implements encoding.TextMarshaler.
func (cd *Def) MarshalText() ([]byte, error) {
	b, err := cd.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(base64.StdEncoding.EncodeToString(b)), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (cd *Def) UnmarshalText(b []byte) error {
	p, err := base64.StdEncoding.DecodeString(string(b))
	if err != nil {
		return xerrors.Errorf("card: decoding card: %w", err)
	}

	return cd.UnmarshalBinary(p)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (cd *Def) MarshalBinary() ([]byte, error) {
	var w format.Writer

	w.UVarInt(5) // format version
	w.UVarInt(uint64(cd.ID))

	if cd.Name == cd.ID.String() {
		w.String("")
	} else {
		w.String(cd.Name)
	}

	w.SVarInt(cd.TP)
	w.Write(cd.Portrait)

	firstTribe := TribeUnknown
	if len(cd.Tribes) != 0 {
		firstTribe = cd.Tribes[0].Tribe
	}

	w.Write(uint8(cd.Rank<<4) | uint8(firstTribe))

	if firstTribe == TribeCustom {
		w.Write(cd.Tribes[0].Red)
		w.Write(cd.Tribes[0].Green)
		w.Write(cd.Tribes[0].Blue)
		w.String(cd.Tribes[0].CustomName)
	}

	tribeCount := len(cd.Tribes) - 1
	if tribeCount < 0 {
		tribeCount = 0
	}

	w.UVarInt(uint64(tribeCount))

	for i := 1; i < len(cd.Tribes); i += 2 {
		t := cd.Tribes[i].Tribe << 4

		if i+1 == len(cd.Tribes) {
			t |= 15
		} else {
			t |= cd.Tribes[i+1].Tribe
		}

		w.Write(t)

		if cd.Tribes[i].Tribe == TribeCustom {
			w.Write(cd.Tribes[i].Red)
			w.Write(cd.Tribes[i].Green)
			w.Write(cd.Tribes[i].Blue)
			w.String(cd.Tribes[i].CustomName)
		}

		if i+1 < len(cd.Tribes) && cd.Tribes[i+1].Tribe == TribeCustom {
			w.Write(cd.Tribes[i+1].Red)
			w.Write(cd.Tribes[i+1].Green)
			w.Write(cd.Tribes[i+1].Blue)
			w.String(cd.Tribes[i+1].CustomName)
		}
	}

	w.UVarInt(uint64(len(cd.Effects)))

	for i, e := range cd.Effects {
		if err := e.Marshal(&w); err != nil {
			return nil, xerrors.Errorf("marshaling effect %d: %w", i, err)
		}
	}

	if cd.Portrait == PortraitCustomEmbedded || cd.Portrait == PortraitCustomExternal {
		w.UVarInt(uint64(len(cd.CustomPortrait)))
		w.Bytes(cd.CustomPortrait)
	}

	w.UVarInt(uint64(len(cd.Extensions)))

	for i, e := range cd.Extensions {
		w.UVarInt(uint64(e.Type))

		ew, done := w.SubWriter()

		if err := e.Marshal(ew); err != nil {
			return nil, xerrors.Errorf("marshaling extension field %d: %w", i, err)
		}

		done()
	}

	return w.Data(), nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (cd *Def) UnmarshalBinary(b []byte) (err error) {
	loc := "at start of card code"

	defer func() {
		if err != nil && loc != "" {
			err = xerrors.Errorf("%s: %w", loc, err)
		}
	}()

	defer format.Catch(&err)

	var r format.Reader

	r.Init(b)

	var card Def

	switch formatVersion := r.UVarInt(); formatVersion {
	case 0, 1:
		card, err = cd.unmarshalV1(&r, &loc, formatVersion)
	case 2, 4:
		card, err = cd.unmarshalV4(&r, &loc, formatVersion)
	case 5:
		loc = "reading card ID"
		card.ID = ID(r.UVarInt())

		loc = "reading card name"
		card.Name = r.String()

		loc = "reading card TP"
		card.TP = r.SVarInt()

		loc = "reading card portrait byte"
		card.Portrait = r.Byte()

		if card.Portrait > 234 && card.Portrait < 254 {
			return xerrors.Errorf("card: invalid portrait byte: %02x", card.Portrait)
		}

		loc = "reading card rank/tribe byte"

		rankTribe := r.Byte()
		card.Rank = Rank(rankTribe >> 4)

		if card.Rank > 4 {
			return xerrors.Errorf("card: invalid rank: %v", card.Rank)
		}

		firstTribe := TribeDef{
			Tribe: Tribe(rankTribe & 15),
		}

		if firstTribe.Tribe == TribeCustom {
			firstTribe.Red = r.Byte()
			firstTribe.Green = r.Byte()
			firstTribe.Blue = r.Byte()
			firstTribe.CustomName = r.String()
		} else if firstTribe.Tribe == 15 {
			return xerrors.Errorf("card: invalid tribe in slot 0: %v", firstTribe.Tribe)
		}

		loc = "reading card tribe count"

		card.Tribes = make([]TribeDef, r.UVarInt()+1)
		card.Tribes[0] = firstTribe

		loc = "reading card tribes"

		for i := 1; i < len(card.Tribes); i += 2 {
			tribes := r.Byte()

			card.Tribes[i].Tribe = Tribe(tribes >> 4)

			if card.Tribes[i].Tribe == TribeCustom {
				card.Tribes[i].Red = r.Byte()
				card.Tribes[i].Green = r.Byte()
				card.Tribes[i].Blue = r.Byte()
				card.Tribes[i].CustomName = r.String()
			} else if card.Tribes[i].Tribe == 15 {
				return xerrors.Errorf("card: invalid tribe in slot %d: %v", i, card.Tribes[i].Tribe)
			}

			if i+1 == len(card.Tribes) {
				if tribes&15 != 15 {
					return xerrors.Errorf("card: invalid padding on tribe list: %x", tribes&15)
				}

				break
			}

			card.Tribes[i+1].Tribe = Tribe(tribes & 15)

			if card.Tribes[i+1].Tribe == TribeCustom {
				card.Tribes[i+1].Red = r.Byte()
				card.Tribes[i+1].Green = r.Byte()
				card.Tribes[i+1].Blue = r.Byte()
				card.Tribes[i+1].CustomName = r.String()
			} else if card.Tribes[i+1].Tribe == 15 {
				return xerrors.Errorf("card: invalid tribe in slot %d: %v", i+1, card.Tribes[i+1].Tribe)
			}
		}

		loc = "reading card effect count"

		card.Effects = make([]*EffectDef, r.UVarInt())
		effectBuf := make([]EffectDef, len(card.Effects))

		loc = ""

		for i := range card.Effects {
			card.Effects[i] = &effectBuf[i]

			if err := card.Effects[i].Unmarshal(&r, formatVersion); err != nil {
				return xerrors.Errorf("unmarshaling effect %d: %w", i, err)
			}
		}

		loc = "reading card custom portrait"

		if card.Portrait == PortraitCustomEmbedded || card.Portrait == PortraitCustomExternal {
			card.CustomPortrait = make([]byte, r.UVarInt())

			r.Read(card.CustomPortrait)
		}

		loc = "reading card extensions"

		card.Extensions = make([]*Extension, r.UVarInt())
		extensionsBuf := make([]Extension, len(card.Extensions))

		for i := range card.Extensions {
			card.Extensions[i] = &extensionsBuf[i]
			card.Extensions[i].Type = ExtensionType(r.UVarInt())

			er := r.SubReader()

			if err := card.Extensions[i].Unmarshal(er, formatVersion); err != nil {
				return xerrors.Errorf("unmarshaling extension field %d: %w", i, err)
			}
		}
	default:
		return xerrors.Errorf("card: unknown format version %d", formatVersion)
	}

	if err != nil {
		return
	}

	loc = ""

	if r.Len() != 0 {
		return xerrors.New("card: extra data after end of card code")
	}

	*cd = card

	return nil
}

// UpdateID changes all references to oldID to newID.
func (cd *Def) UpdateID(oldID, newID ID) {
	if cd.ID == oldID {
		cd.ID = newID
	}

	for _, e := range cd.Effects {
		e.UpdateID(oldID, newID)
	}
}
