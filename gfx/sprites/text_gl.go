// +build !headless

//go:generate sh -c "go run ./internal/gentext > text-data.go"

package sprites

import (
	"image/color"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/internal"
)

var (
	buttonSprites = [...][10]*Sprite{
		internal.StyleKeyboard: kbSprites,
		internal.StyleGenericGamepad: {
			arcade.BtnUp:      GamepadButton[arcade.BtnUp][0],
			arcade.BtnDown:    GamepadButton[arcade.BtnDown][0],
			arcade.BtnLeft:    GamepadButton[arcade.BtnLeft][0],
			arcade.BtnRight:   GamepadButton[arcade.BtnRight][0],
			arcade.BtnConfirm: GamepadButton[arcade.BtnConfirm][0],
			arcade.BtnCancel:  GamepadButton[arcade.BtnCancel][0],
			arcade.BtnSwitch:  GamepadButton[arcade.BtnSwitch][0],
			arcade.BtnToggle:  GamepadButton[arcade.BtnToggle][0],
			arcade.BtnPause:   GamepadButton[arcade.BtnPause][0],
			arcade.BtnHelp:    GamepadButton[arcade.BtnHelp][0],
		},
	}
	textSheets = [...]map[rune][2]*Sprite{
		FontD3Streetism:   d3Streetism,
		FontBubblegumSans: bubblegumSans,
	}
	textAdvance = [...]map[rune]float32{
		FontD3Streetism:   d3StreetismAdvance,
		FontBubblegumSans: bubblegumSansAdvance,
	}
)

// TextAdvance returns the number of units the next letter is offset after this letter.
func TextAdvance(font FontID, letter rune, scaleX float32) float32 {
	if letter < 32 {
		return 0.6 * scaleX
	}

	if letter == ' ' {
		return 0.3 * scaleX
	}

	if a, ok := textAdvance[font][letter]; ok {
		return a * scaleX
	}

	if a, ok := arialAdvance[letter]; ok {
		return a * scaleX
	}

	if a, ok := dejavuAdvance[letter]; ok {
		return a * scaleX
	}

	return 0.3 * scaleX
}

// DrawTextFuncEx draws a string to the screen.
func DrawTextFuncEx(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) color.RGBA, flags RenderFlag, ry, rz, blur float32) {
	isBorder := 0

	if flags&FlagBorder == FlagBorder {
		flags &^= FlagBorder
		blur = 0

		isBorder = 1
	}

	tb.reserve(len(text))

	for _, letter := range text {
		sprite2, ok := textSheets[font][letter]

		var sprite *Sprite

		flags |= FlagIsMask

		if letter < 10 {
			sprite = buttonSprites[ButtonStyle][letter]
			flags &^= FlagIsMask
		} else if ok {
			sprite = sprite2[isBorder]
		} else if sprite2, ok = arial[letter]; ok {
			sprite = sprite2[isBorder]
		} else if sprite2, ok = dejavu[letter]; ok {
			sprite = sprite2[isBorder]
		} else {
			sprite = textSheets[font][' '][0]
		}

		letterColor := getColor(letter)

		tb.Append(sprite, x, y, z, scaleX, scaleY, letterColor, flags, ry, rz, blur)

		x += TextAdvance(font, letter, scaleX)
	}
}
