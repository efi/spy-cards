// +build !headless

// Package gfx is a thin abstraction layer for OpenGL.
package gfx

import (
	"sync"

	"golang.org/x/mobile/gl"
)

// GL is the OpenGL context.
var GL gl.Context

// Lock should be held whenever an operation depends on or modifies global OpenGL state.
var Lock sync.Mutex

// Init is an internal function that sets up the context.
func Init(ctx gl.Context) {
	Lock.Lock()

	GL = ctx
	GL.Enable(gl.BLEND)
	GL.DepthFunc(gl.LESS)
	GL.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
	GL.ClearColor(0, 0, 0, 1)
	GL.Clear(gl.COLOR_BUFFER_BIT)

	for _, p := range programs {
		p.init()
	}

	for _, sb := range buffers {
		sb.init()
	}

	for _, t := range textures {
		t.init()
	}

	for _, c := range custom {
		c.init()
	}

	Lock.Unlock()
}

// Release is an internal function that shuts down the context.
func Release() {
	Lock.Lock()

	for _, p := range programs {
		p.release()
	}

	for _, sb := range buffers {
		sb.release()
	}

	for _, t := range textures {
		t.release()
	}

	for _, c := range custom {
		c.release()
	}

	GL = nil

	Lock.Unlock()
}

var custom []*CustomInit

// CustomInit is a custom OpenGL init/release function pair.
type CustomInit struct {
	Name    string
	init    func()
	release func()
}

// Custom registers a CustomInit.
func Custom(name string, init, release func()) *CustomInit {
	c := &CustomInit{
		Name:    name,
		init:    init,
		release: release,
	}

	Lock.Lock()

	custom = append(custom, c)

	if GL != nil {
		c.init()
	}

	Lock.Unlock()

	return c
}
