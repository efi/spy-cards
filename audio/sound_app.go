// +build !js !wasm
// +build !headless

package audio

import (
	"io/ioutil"
	"log"
	"strings"
	"sync"

	"github.com/BenLubar/opus"
	"golang.org/x/mobile/asset"
	"golang.org/x/mobile/exp/audio/al"
	"golang.org/x/xerrors"
)

const alPitch = 0x1003

var (
	musicSource al.Source
	sources     []al.Source
	curMusic    *Sound
	silentTick1 al.Buffer
	silentTick2 al.Buffer

	initOnce sync.Once
)

func initAudio() {
	audioSettingsOnce.Do(initAudioSettings)

	if err := al.OpenDevice(); err != nil {
		panic(err)
	}

	sources = al.GenSources(17)
	musicSource = sources[0]
	sources = sources[1:]

	silentTick1 = al.GenBuffers(1)[0]
	silentTick1.BufferData(al.FormatMono16, make([]byte, 48000/60*2), 48000)
	silentTick2 = al.GenBuffers(1)[0]
	silentTick2.BufferData(al.FormatStereo16, make([]byte, 48000/60*4), 48000)
}

type implData struct {
	loadOnce sync.Once
	buf      al.Buffer
	loop     al.Buffer
	stereo   bool
	loadErr  error
}

func (s *Sound) loadData() {
	initOnce.Do(initAudio)

	if s.Volume == 0 {
		s.Volume = 1
	}

	if s.Pitch == 0 {
		s.Pitch = 1
	}

	url := s.Name
	if !strings.Contains(url, "/") {
		url = "audio/" + url + ".opus"
	}

	f, err := asset.Open(url)
	if err != nil {
		s.impl.loadErr = xerrors.Errorf("audio: failed to load %q: %w", s.Name, err)

		return
	}
	defer f.Close()

	b, err := ioutil.ReadAll(f)
	if err != nil {
		s.impl.loadErr = xerrors.Errorf("audio: failed to load %q: %w", s.Name, err)

		return
	}

	pcm, channels, err := opus.DecodeOpus(b)
	if err != nil {
		s.impl.loadErr = xerrors.Errorf("audio: failed to load %q: %w", s.Name, err)

		return
	}

	format := uint32(al.FormatMono16)
	if channels > 1 {
		format = al.FormatStereo16
		s.impl.stereo = true
	}

	s.impl.buf = al.GenBuffers(1)[0]

	if s.Loop {
		start := int(s.Start*48000) * 2 * channels
		end := int(s.End*48000) * 2 * channels

		if end > len(pcm) {
			end = len(pcm)
		}

		if start >= len(pcm) || start == 0 {
			s.impl.loop = s.impl.buf
			pcm = pcm[:end]
		} else {
			s.impl.loop = al.GenBuffers(1)[0]
			s.impl.loop.BufferData(format, pcm[start:end], 48000)
			pcm = pcm[:start]
		}
	}

	s.impl.buf.BufferData(format, pcm, 48000)
}

// Preload loads the Sound and returns the error encountered, if any.
func (s *Sound) Preload() error {
	s.impl.loadOnce.Do(s.loadData)

	return s.impl.loadErr
}

// PlaySound plays a sound, optionally modifying its pitch and volume.
func (s *Sound) PlaySound(delay, overridePitch, overrideVolume float64) {
	if SoundsVolume == 0 {
		return
	}

	if IgnoreAudio > 0 {
		return
	}

	s.impl.loadOnce.Do(func() {
		log.Println("WARNING: non-preloaded sound:", s.Name)

		s.loadData()
	})

	if s.impl.loadErr != nil {
		log.Println("sound load error", s.Name, s.impl.loadErr)
	}

	for _, source := range sources {
		if source.BuffersQueued() != 0 {
			continue
		}

		volume := overrideVolume
		if volume == 0 {
			volume = s.Volume
		}

		source.SetGain(float32(SoundsVolume * volume))

		for d := 0.0; d < delay; d += 1.0 / 60.0 {
			if s.impl.stereo {
				source.QueueBuffers(silentTick2)
			} else {
				source.QueueBuffers(silentTick1)
			}
		}

		pitch := overridePitch
		if pitch == 0 {
			pitch = s.Pitch
		}

		source.Setf(alPitch, float32(pitch))

		source.QueueBuffers(s.impl.buf)

		al.PlaySources(source)

		return
	}

	log.Println("no available audio channel for sound", s.Name)
}

// PlayMusic plays a sound as the current music track.
func (s *Sound) PlayMusic(delay float64, restart bool) {
	if IgnoreAudio > 0 {
		return
	}

	s.impl.loadOnce.Do(func() {
		log.Println("WARNING: non-preloaded sound:", s.Name)

		s.loadData()
	})

	if s.impl.loadErr != nil {
		log.Println("music load error", s.Name, s.impl.loadErr)
	}

	if curMusic == s && !restart {
		return
	}

	StopMusic()

	curMusic = s

	musicSource.SetGain(float32(MusicVolume * s.Volume))

	for d := 0.0; d < delay; d += 1.0 / 60.0 {
		if s.impl.stereo {
			musicSource.QueueBuffers(silentTick2)
		} else {
			musicSource.QueueBuffers(silentTick1)
		}
	}

	musicSource.Setf(alPitch, float32(s.Pitch))

	musicSource.QueueBuffers(s.impl.buf)

	al.PlaySources(musicSource)
}

// Release releases any system resources used by the Sound. It is an error to
// use the Sound after calling Release.
func (s *Sound) Release() {
	s.impl.loadErr = xerrors.Errorf("audio: sound %q used after free", s.Name)
	al.DeleteBuffers(s.impl.buf)
	s.impl.buf = al.Buffer(0)
}

// StopMusic stops the current music track.
func StopMusic() {
	curMusic = nil

	const alBuffer = 0x1009

	al.StopSources(musicSource)

	musicSource.Seti(alBuffer, 0)
}

// Tick is an internal function that performs bookkeeping for the audio package.
func Tick() {
	initOnce.Do(initAudio)

	var buffer [1]al.Buffer

	for _, source := range sources {
		for source.BuffersProcessed() != 0 {
			source.UnqueueBuffers(buffer[:])
		}
	}

	for musicSource.BuffersProcessed() != 0 {
		musicSource.UnqueueBuffers(buffer[:])
	}

	if curMusic != nil && curMusic.impl.loop.Valid() && musicSource.BuffersQueued() < 2 {
		musicSource.QueueBuffers(curMusic.impl.loop)
	}
}

func onVolumeChanged() {
	if curMusic != nil {
		musicSource.SetGain(float32(MusicVolume * curMusic.Volume))
	}
}
