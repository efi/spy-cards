/**
 * Implementation of Crockford Base 32
 * https://www.crockford.com/base32.html
 */
module CrockfordBase32 {
	const alphabet = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";

	function normalize(s: string): string {
		s = s.toUpperCase();
		s = s.replace(/O/g, "0");
		s = s.replace(/[IL]/g, "1");
		s = s.replace(/[ \-_]/g, "");
		return s;
	}

	export function encode(src: Uint8Array): string {
		const dst = [];
		let bits = 0, n = 0;
		for (let i = 0; i < src.length; i++) {
			n <<= 8;
			n |= src[i];
			bits += 8;
			while (bits >= 5) {
				bits -= 5;
				dst.push(alphabet.charAt(n >> bits));
				n &= (1 << bits) - 1;
			}
		}
		if (bits) {
			while (bits < 5) {
				n <<= 1;
				bits++;
			}
			dst.push(alphabet.charAt(n));
		}
		return dst.join("");
	}

	export function decode(src: string): Uint8Array {
		src = normalize(src);
		const dst = new Uint8Array(Math.floor(src.length / 8 * 5));
		let bits = 0, n = 0;
		for (let i = 0, j = 0; i < src.length; i++) {
			const k = alphabet.indexOf(src.charAt(i));
			if (k === -1) {
				throw new Error("invalid character in string");
			}
			n <<= 5;
			n |= k;
			bits += 5;
			while (bits >= 8) {
				bits -= 8;
				dst[j++] = n >> bits;
				n &= (1 << bits) - 1;
			}
		}
		return dst;
	}
}

// Because atob and btoa are weird, fiddly, and don't properly support nul bytes.
module Base64 {
	const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	export function encode(src: Uint8Array): string {
		const dst = [];
		let bits = 0, n = 0;
		for (let i = 0; i < src.length; i++) {
			n <<= 8;
			n |= src[i];
			bits += 8;
			while (bits >= 6) {
				bits -= 6;
				dst.push(alphabet.charAt(n >> bits));
				n &= (1 << bits) - 1;
			}
		}
		if (bits) {
			while (bits < 6) {
				n <<= 1;
				bits++;
			}
			dst.push(alphabet.charAt(n));
		}
		while (dst.length % 4) {
			dst.push("=");
		}
		return dst.join("");
	}

	export function decode(src: string): Uint8Array {
		src = src.replace(/[=]+$/, "");
		const dst = new Uint8Array(Math.floor(src.length / 8 * 6));
		let bits = 0, n = 0;
		for (let i = 0, j = 0; i < src.length; i++) {
			const k = alphabet.indexOf(src.charAt(i));
			if (k === -1) {
				throw new Error("invalid character in string");
			}
			n <<= 6;
			n |= k;
			bits += 6;
			while (bits >= 8) {
				bits -= 8;
				dst[j++] = n >> bits;
				n &= (1 << bits) - 1;
			}
		}
		return dst;
	}
}
