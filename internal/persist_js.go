// +build js,wasm
// +build !headless

package internal

import "syscall/js"

var localStorage = js.Global().Get("localStorage")

// LoadData loads data for a given key from localStorage.
func LoadData(key string) ([]byte, error) {
	val := localStorage.Get(key)
	if val.Truthy() {
		return []byte(val.String()), nil
	}

	return nil, nil
}

// StoreData stores data in a given key of localStorage.
func StoreData(key string, val []byte) error {
	localStorage.Set(key, string(val))

	return nil
}
