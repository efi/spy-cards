// +build !headless

package miteknight

import (
	"image/color"

	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

func (d *dungeonFloor) appendTileGeometry(staticData []uint8, staticElements []uint16, x, y, baseX, baseY int) ([]uint8, []uint16) {
	tt := tileTypes[d.floorMap[x][y]]
	if tt.wallSprite != nil {
		for dir := direction(0); dir < 4; dir++ {
			var dx, dy, up, down, left, right int

			switch dir {
			case dirUp:
				dy = 1
				up = 1
			case dirDown:
				dy = -1
				down = 1
			case dirLeft:
				dx = -1
				left = 1
			case dirRight:
				dx = 1
				right = 1
			}

			if x+dx >= 0 && x+dx < d.sizeX &&
				y+dy >= 0 && y+dy < d.sizeY &&
				d.floorMap[x+dx][y+dy] != tileNone &&
				tileTypes[d.floorMap[x+dx][y+dy]].wallSprite == nil {
				n := uint16(len(staticData) / 8)

				dx2 := (dx + 1) >> 1
				dy2 := (dy + 1) >> 1

				staticData = append(staticData,
					uint8(x+dx2+up-baseX), 0, uint8(y+dy2+left-baseY),
					texCoord(tt.wallSprite, false, false),
					texCoord(tt.wallSprite, true, true),
					tt.color.R, tt.color.G, tt.color.B,

					uint8(x+dx2+down-baseX), 0, uint8(y+dy2+right-baseY),
					texCoord(tt.wallSprite, false, true),
					texCoord(tt.wallSprite, true, true),
					tt.color.R, tt.color.G, tt.color.B,

					uint8(x+dx2+down-baseX), 1, uint8(y+dy2+right-baseY),
					texCoord(tt.wallSprite, false, true),
					texCoord(tt.wallSprite, true, false),
					tt.color.R, tt.color.G, tt.color.B,

					uint8(x+dx2+up-baseX), 1, uint8(y+dy2+left-baseY),
					texCoord(tt.wallSprite, false, false),
					texCoord(tt.wallSprite, true, false),
					tt.color.R, tt.color.G, tt.color.B,
				)

				staticElements = append(staticElements,
					n+0, n+1, n+2,
					n+2, n+3, n+0,
				)
			}
		}
	} else {
		n := uint16(len(staticData) / 8)

		staticData = append(staticData,
			uint8(x-baseX), 0, uint8(y-baseY),
			texCoord(sprites.MKFloor, false, false),
			texCoord(sprites.MKFloor, true, false),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 0, uint8(y-baseY),
			texCoord(sprites.MKFloor, false, false),
			texCoord(sprites.MKFloor, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 0, uint8(y+1-baseY),
			texCoord(sprites.MKFloor, false, true),
			texCoord(sprites.MKFloor, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x-baseX), 0, uint8(y+1-baseY),
			texCoord(sprites.MKFloor, false, true),
			texCoord(sprites.MKFloor, true, false),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x-baseX), 1, uint8(y-baseY),
			texCoord(sprites.MKCeiling, false, false),
			texCoord(sprites.MKCeiling, true, false),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x-baseX), 1, uint8(y+1-baseY),
			texCoord(sprites.MKCeiling, false, false),
			texCoord(sprites.MKCeiling, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 1, uint8(y+1-baseY),
			texCoord(sprites.MKCeiling, false, true),
			texCoord(sprites.MKCeiling, true, true),
			tt.color.R, tt.color.G, tt.color.B,

			uint8(x+1-baseX), 1, uint8(y-baseY),
			texCoord(sprites.MKCeiling, false, true),
			texCoord(sprites.MKCeiling, true, false),
			tt.color.R, tt.color.G, tt.color.B,
		)

		staticElements = append(staticElements,
			n+0, n+1, n+2,
			n+2, n+3, n+0,
			n+4, n+5, n+6,
			n+6, n+7, n+4,
		)
	}

	return staticData, staticElements
}

func texCoord(sprite *sprites.Sprite, y, far bool) uint8 {
	var coord float32

	if y {
		if far {
			coord = sprite.T1
		} else {
			coord = sprite.T0
		}
	} else {
		if far {
			coord = sprite.S1
		} else {
			coord = sprite.S0
		}
	}

	return uint8(coord * 8)
}

var stoneColor = color.RGBA{229, 102, 0, 255}

var (
	geomProgram = gfx.Shader("mkgeom", `uniform mat4 perspective;
uniform mat4 camera;
attribute vec3 in_pos;
attribute vec2 in_tex;
attribute vec3 in_color;
varying vec2 tex_coord;
varying vec4 color;
varying float fog_distance;

void main() {
	tex_coord = in_tex / 8.0;
	gl_Position = perspective * camera * vec4(in_pos - vec3(0.5, 0.0, 0.5), 1.0);
	fog_distance = length(gl_Position.xyz);
	color = vec4(in_color, 1.0);
}`, `precision highp float;

uniform sampler2D tex;
uniform float time;
uniform float fog_end;
varying vec2 tex_coord;
varying vec4 color;
varying float fog_distance;

void main() {
	vec4 texColor = texture2D(tex, tex_coord);
	gl_FragColor = texColor * color;

	gl_FragColor.rgb *= clamp((fog_end - fog_distance) / fog_end, 0.0, 1.0);

	if (texColor.a < 0.5) {
		discard;
	}
}`)

	geomTex = geomProgram.Uniform("tex")

	geomTime        = geomProgram.Uniform("time")
	geomPerspective = geomProgram.Uniform("perspective")
	geomCamera      = geomProgram.Uniform("camera")
	geomFogEnd      = geomProgram.Uniform("fog_end")

	posAttrib   = geomProgram.Attrib("in_pos")
	texAttrib   = geomProgram.Attrib("in_tex")
	colorAttrib = geomProgram.Attrib("in_color")
)

var tileTypes = [...]struct {
	wallSprite *sprites.Sprite
	color      color.RGBA
}{
	tileWall: {
		wallSprite: sprites.MKWall,
		color:      stoneColor,
	},
	tileFree: {
		wallSprite: nil,
		color:      stoneColor,
	},
	tileKey: {
		wallSprite: nil,
		color:      stoneColor,
	},
	tilePotion: {
		wallSprite: nil,
		color:      stoneColor,
	},
	tileDoor: {
		wallSprite: nil,
		color:      sprites.Yellow,
	},
	tileStairs: {
		wallSprite: sprites.MKStairs,
		color:      sprites.Red,
	},
	tileWall2: {
		wallSprite: sprites.MKWall2,
		color:      sprites.White,
	},
}
