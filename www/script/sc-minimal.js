"use strict";
var SpyCards;
(function (SpyCards) {
    var Minimal;
    (function (Minimal) {
        function exchangeDataPipe() {
            let resolve;
            let promise;
            return function (data) {
                if (promise) {
                    const toReturn = promise;
                    resolve(data);
                    resolve = null;
                    promise = null;
                    return toReturn;
                }
                promise = Promise.resolve(data);
                return new Promise((r) => resolve = r);
            };
        }
        function newContext(player, defs) {
            return {
                net: null,
                ui: null,
                scg: null,
                defs: defs,
                player: player,
                matchData: new SpyCards.MatchData(),
                fx: {
                    async coin() { },
                    async numb() { },
                    async setup() { },
                    async multiple() { },
                    async assist() { },
                    async beforeProcessEffect() { },
                    async afterProcessEffect() { },
                },
            };
        }
        async function playMatches(p1, p2, count = 1, maxRounds = null, defs = new SpyCards.CardDefs(), preview, modeName) {
            preview = preview || function () { };
            const p1ctx = newContext(1, defs);
            const p2ctx = newContext(2, defs);
            p1ctx.matchData.customModeName = modeName || "";
            p2ctx.matchData.customModeName = modeName || "";
            const xchg = exchangeDataPipe();
            const matches = [];
            [p1ctx.matchData.selectedCharacter, p2ctx.matchData.selectedCharacter] = await Promise.all([
                p1.pickPlayer().then((p) => p.name),
                p2.pickPlayer().then((p) => p.name),
            ]);
            while (matches.length < count) {
                p1ctx.scg = new SecureCardGame();
                p2ctx.scg = new SecureCardGame();
                p1ctx.state = new SpyCards.MatchState(defs);
                p2ctx.state = new SpyCards.MatchState(defs);
                const match = [];
                SpyCards.Binary.pushUVarInt(match, 1); // format version
                const spyCardsVersionNum = spyCardsVersionPrefix.split(/\./g).map((n) => parseInt(n, 10));
                SpyCards.Binary.pushUVarInt(match, spyCardsVersionNum[0]);
                SpyCards.Binary.pushUVarInt(match, spyCardsVersionNum[1]);
                SpyCards.Binary.pushUVarInt(match, spyCardsVersionNum[2]);
                SpyCards.Binary.pushUTF8String1(match, p1ctx.matchData.customModeName);
                match.push(0); // not from either player's perspective
                SpyCards.Binary.pushUTF8String1(match, p1ctx.matchData.selectedCharacter);
                SpyCards.Binary.pushUTF8String1(match, p2ctx.matchData.selectedCharacter);
                SpyCards.Binary.pushUVarInt(match, matches.length);
                await Promise.all([
                    p1ctx.scg.init(1, xchg),
                    p2ctx.scg.init(2, xchg)
                ]);
                match.push(...SpyCards.toArray(p1ctx.scg.seed));
                match.push(...SpyCards.toArray(p1ctx.scg.localRandSeed));
                match.push(...SpyCards.toArray(p2ctx.scg.localRandSeed));
                SpyCards.Binary.pushUVarInt(match, p1ctx.defs.customCardsRaw.length);
                for (let card of p1ctx.defs.customCardsRaw) {
                    const buf = Base64.decode(card);
                    SpyCards.Binary.pushUVarInt(match, buf.length);
                    match.push(...SpyCards.toArray(buf));
                }
                const [deck1, deck2] = await Promise.all([
                    p1.createDeck(defs),
                    p2.createDeck(defs)
                ]);
                for (let deck of [deck1, deck2]) {
                    const buf = CrockfordBase32.decode(SpyCards.Decks.encode(defs, deck));
                    SpyCards.Binary.pushUVarInt(match, buf.length);
                    match.push(...SpyCards.toArray(buf));
                }
                p1ctx.state.deck[0] = deck1.map((c) => ({ card: c, back: c.getBackID(), player: 1 }));
                p2ctx.state.deck[1] = deck2.map((c) => ({ card: c, back: c.getBackID(), player: 2 }));
                p1ctx.state.deck[1] = deck2.map((c) => ({ card: null, back: c.getBackID(), player: 2 }));
                p2ctx.state.deck[0] = deck1.map((c) => ({ card: null, back: c.getBackID(), player: 1 }));
                p1ctx.state.backs[0] = deck1.map((c) => c.getBackID());
                p1ctx.state.backs[1] = deck2.map((c) => c.getBackID());
                p2ctx.state.backs[0] = deck1.map((c) => c.getBackID());
                p2ctx.state.backs[1] = deck2.map((c) => c.getBackID());
                await Promise.all([
                    p1ctx.scg.setDeck(xchg, SpyCards.Decks.encodeBinary(defs, deck1), new Uint8Array(deck1.map(c => c.getBackID()))),
                    p2ctx.scg.setDeck(xchg, SpyCards.Decks.encodeBinary(defs, deck2), new Uint8Array(deck2.map(c => c.getBackID())))
                ]);
                const p1proc = new SpyCards.Processor(p1ctx, new SpyCards.ProcessorCallbacks());
                p1ctx.effect = p1proc.effect;
                const p2proc = new SpyCards.Processor(p2ctx, new SpyCards.ProcessorCallbacks());
                p2ctx.effect = p2proc.effect;
                while (!p1proc.checkWinner()) {
                    p1ctx.state.turn++;
                    p2ctx.state.turn++;
                    const [turnData] = await Promise.all([
                        p1ctx.scg.beginTurn(xchg),
                        p2ctx.scg.beginTurn(xchg)
                    ]);
                    await Promise.all([
                        p1proc.shuffleAndDraw(false),
                        p2proc.shuffleAndDraw(false)
                    ]);
                    turnData.ready = await Promise.all([
                        p1.playRound(p1ctx),
                        p2.playRound(p2ctx)
                    ]);
                    for (let ctx of [p1ctx, p2ctx]) {
                        for (let player = 0; player < 2; player++) {
                            while (ctx.state.discard[player].length) {
                                const card = ctx.state.discard[player].shift();
                                ctx.state.deck[player].push({
                                    card: card.card,
                                    back: card.back,
                                    player: card.player,
                                });
                                ctx.state.backs[player].push(card.back);
                            }
                            for (let i = 4; i >= 0; i--) {
                                if (turnData.ready[player] & (1 << i)) {
                                    const [card] = ctx.state.hand[player].splice(i, 1);
                                    ctx.state.discard[player].unshift(card);
                                }
                            }
                        }
                    }
                    p1ctx.state.played[0] = p1ctx.state.discard[0].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: 1,
                        effects: c.card.effects.slice(0)
                    }));
                    p2ctx.state.played[1] = p2ctx.state.discard[1].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: 2,
                        effects: c.card.effects.slice(0)
                    }));
                    p1ctx.state.played[1] = p2ctx.state.played[1].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: c.player,
                        effects: c.effects.slice(0)
                    }));
                    p2ctx.state.played[0] = p1ctx.state.played[0].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: c.player,
                        effects: c.effects.slice(0)
                    }));
                    await Promise.all([
                        p1ctx.scg.prepareTurn(new Uint8Array(0)).then((x) => p2ctx.scg.promiseTurn(x)),
                        p2ctx.scg.prepareTurn(new Uint8Array(0)).then((x) => p1ctx.scg.promiseTurn(x))
                    ]);
                    await Promise.all([
                        p1ctx.scg.confirmTurn(xchg),
                        p2ctx.scg.confirmTurn(xchg)
                    ]);
                    await Promise.all([
                        p1proc.processRound(),
                        p2proc.processRound()
                    ]);
                    await Promise.all([
                        p1.afterRound(p1ctx),
                        p2.afterRound(p2ctx)
                    ]);
                    if (maxRounds && p1ctx.state.turn >= maxRounds) {
                        break;
                    }
                }
                let winner;
                if (p1ctx.state.hp[0] > 0) {
                    winner = 1;
                }
                else if (p1ctx.state.hp[1] > 0) {
                    winner = 2;
                }
                else if (p1ctx.state.winner) {
                    winner = p1ctx.state.winner;
                }
                else {
                    winner = 0;
                }
                p1ctx.matchData.rematchCount++;
                p2ctx.matchData.rematchCount++;
                if (winner === 1) {
                    p1ctx.matchData.wins++;
                    p2ctx.matchData.losses++;
                }
                else if (winner === 2) {
                    p1ctx.matchData.losses++;
                    p2ctx.matchData.wins++;
                }
                SpyCards.Binary.pushUVarInt(match, p1ctx.state.turn);
                await Promise.all([
                    p1ctx.scg.finalize(xchg, async () => { }, async (turn) => {
                        match.push(...SpyCards.toArray(turn.seed));
                        match.push(...SpyCards.toArray(turn.seed2));
                        match.push(turn.data.ready[0]);
                        match.push(turn.data.ready[1]);
                    }),
                    p2ctx.scg.finalize(xchg, async () => { }, async () => { })
                ]);
                const summary = {
                    winner: winner,
                    rounds: p1ctx.state.turn,
                    recording: new Uint8Array(match)
                };
                preview(summary);
                matches.push(summary);
            }
            return matches;
        }
        Minimal.playMatches = playMatches;
        async function playMatch(ctx, npc) {
            ctx.net.cgc.onVersion = () => { };
            ctx.net.cgc.onQuit = () => { };
            ctx.net.cgc.sendSpoilerGuardData("");
            ctx.net.cgc.sendVersion(spyCardsVersion + spyCardsVersionVariableSuffix);
            await ctx.net.cgc.initSecure(ctx.player);
            ctx.matchData.selectedCharacter = (await npc.pickPlayer()).name;
            ctx.net.cgc.sendCosmeticData({
                character: ctx.matchData.selectedCharacter,
            });
            ctx.state = new SpyCards.MatchState(ctx.defs);
            ctx.state.roundSetupComplete = false;
            ctx.state.roundSetupDelayed = [];
            const remoteSpoilerGuard = await ctx.net.cgc.recvSpoilerGuard;
            await SpyCards.SpoilerGuard.banSpoilerCards(ctx.defs, remoteSpoilerGuard);
            const deck = await npc.createDeck(ctx.defs);
            const opponentCardBacks = await ctx.net.cgc.initDeck(SpyCards.Decks.encodeBinary(ctx.defs, deck), SpyCards.Decks.encodeBacks(deck.map((c) => c.getBackID())));
            ctx.state.deck[ctx.player - 1] = deck.map((c) => ({
                card: c,
                back: c.getBackID(),
                player: ctx.player,
            }));
            ctx.state.backs[ctx.player - 1] = deck.map((c) => c.getBackID());
            ctx.state.backs[2 - ctx.player] = SpyCards.Decks.decodeBacks(opponentCardBacks);
            ctx.state.deck[2 - ctx.player] = ctx.state.backs[2 - ctx.player].map(function (back) {
                return {
                    card: null,
                    back: back,
                    player: (3 - ctx.player),
                };
            });
            const processor = new SpyCards.Processor(ctx, new SpyCards.ProcessorCallbacks());
            ctx.effect = processor.effect;
            let turnReady;
            ctx.net.cgc.onReady = function (promise) {
                if (!ctx.state.roundSetupComplete) {
                    ctx.state.roundSetupDelayed.push(function () {
                        ctx.net.cgc.onReady(promise);
                    });
                    return;
                }
                this.scg.promiseTurn(promise);
                ctx.state.ready[2 - ctx.player] = true;
                turnReady();
            };
            while (!processor.checkWinner()) {
                ctx.state.turn++;
                ctx.state.winner = 0;
                ctx.state.turnData = await ctx.net.cgc.beginTurn();
                ctx.state.turnData.ready = [null, null];
                await processor.shuffleAndDraw(false);
                ctx.state.ready[0] = ctx.state.ready[1] = false;
                ctx.state.roundSetupComplete = true;
                const waitReady = new Promise((resolve) => turnReady = resolve);
                while (ctx.state.roundSetupDelayed.length) {
                    ctx.state.roundSetupDelayed.shift()();
                }
                ctx.state.played[0].length = 0;
                ctx.state.played[1].length = 0;
                const ready = await npc.playRound(ctx);
                ctx.state.turnData.ready[ctx.player - 1] = ready;
                ctx.state.ready[ctx.player - 1] = true;
                const tap = [];
                const tapped = [[], []];
                const untapped = [[], []];
                SpyCards.Binary.pushUVarInt(tap, ready);
                for (let i = 0; i < ctx.state.hand[ctx.player - 1].length; i++) {
                    const card = ctx.state.hand[ctx.player - 1][i];
                    if (SpyCards.Binary.getBit(ready, i)) {
                        SpyCards.Binary.pushUVarInt(tap, card.card.id);
                        tapped[ctx.player - 1].push(card);
                    }
                    else {
                        untapped[ctx.player - 1].push(card);
                    }
                }
                ctx.net.cgc.sendReady(new Uint8Array(tap));
                await waitReady;
                ctx.state.roundSetupComplete = false;
                const confirmedTurn = await ctx.net.cgc.confirmTurn();
                ctx.state.turnData.played = [null, null];
                ctx.state.turnData.played[ctx.player - 1] = ctx.state.played[ctx.player - 1].map(function (card) {
                    return card.card.id;
                });
                const confirmedBuf = SpyCards.toArray(confirmedTurn);
                ctx.state.turnData.ready[2 - ctx.player] = SpyCards.Binary.shiftUVarInt(confirmedBuf);
                for (let i = 0; i < ctx.state.hand[2 - ctx.player].length; i++) {
                    const card = ctx.state.hand[2 - ctx.player][i];
                    if (SpyCards.Binary.getBit(ctx.state.turnData.ready[2 - ctx.player], i)) {
                        tapped[2 - ctx.player].push(card);
                    }
                    else {
                        untapped[2 - ctx.player].push(card);
                    }
                }
                ctx.state.turnData.played[2 - ctx.player] = [];
                let i = 0;
                while (confirmedBuf.length) {
                    const cardID = SpyCards.Binary.shiftUVarInt(confirmedBuf);
                    ctx.state.turnData.played[2 - ctx.player].push(cardID);
                    tapped[2 - ctx.player][i++].card = ctx.defs.cardsByID[cardID];
                }
                for (let player = 0; player < 2; player++) {
                    for (let card of ctx.state.discard[player]) {
                        ctx.state.deck[player].push({
                            card: card.card,
                            back: card.back,
                            player: card.player,
                        });
                        ctx.state.backs[player].push(card.back);
                    }
                    ctx.state.discard[player] = tapped[player];
                    ctx.state.hand[player] = untapped[player];
                    tapped[player] = tapped[player].slice(0);
                    for (let c of tapped[player]) {
                        c.effects = c.card.effects.slice(0);
                    }
                    ctx.state.played[player] = tapped[player];
                }
                for (let player = 0; player < 2; player++) {
                    while (ctx.state.setup[player].length) {
                        const setup = ctx.state.setup[player].shift();
                        ctx.state.played[player].unshift({
                            card: setup.card,
                            back: setup.card.getBackID(),
                            setup: true,
                            originalDesc: setup.originalDesc,
                            player: (player + 1),
                            effects: setup.effects.slice(0)
                        });
                    }
                }
                await processor.processRound();
                await npc.afterRound(ctx);
            }
            await ctx.net.cgc.finalizeMatch(async () => { }, async () => { });
            setTimeout(() => ctx.net.cgc.close(), 15000);
        }
        Minimal.playMatch = playMatch;
    })(Minimal = SpyCards.Minimal || (SpyCards.Minimal = {}));
})(SpyCards || (SpyCards = {}));
