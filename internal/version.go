// Package internal contains utility functions and constants for Spy Cards Online.
package internal

import "strconv"

// Version is the current version of Spy Cards Online.
var Version = [3]uint64{0, 3, 1}

// VersionNumber returns a string representation of Version.
func VersionNumber() []byte {
	version := make([]byte, 0, 16)
	version = strconv.AppendUint(version, Version[0], 10)
	version = append(version, '.')
	version = strconv.AppendUint(version, Version[1], 10)
	version = append(version, '.')
	version = strconv.AppendUint(version, Version[2], 10)

	return version
}
