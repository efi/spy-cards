"use strict";
(async function () {
    if (!navigator.onLine) {
        return;
    }
    let showCardEditorInsteadOfHost = false;
    let customCardsRaw = null;
    if (spyCardsVersionSuffix === "-custom") {
        customCardsRaw = decodeURIComponent(location.hash.substr(1));
        if (!customCardsRaw) {
            showCardEditorInsteadOfHost = true;
        }
    }
    const form = document.createElement("main");
    form.id = "play-online";
    form.classList.add("play-online");
    const h1 = document.createElement("h1");
    h1.textContent = "Play Online";
    h1.setAttribute("aria-live", "polite");
    h1.setAttribute("aria-atomic", "true");
    const version = document.createElement("span");
    version.classList.add("version-number");
    version.textContent = "Version ";
    const versionLink = document.createElement("a");
    versionLink.href = "docs/changelog.html#v" + spyCardsVersionPrefix;
    versionLink.textContent = spyCardsVersion;
    version.appendChild(versionLink);
    SpyCards.Audio.showUI(form);
    let variant = "parse-variant";
    let isNPC;
    let isSinglePlayer;
    try {
        const params = new URLSearchParams(location.search);
        variant = parseInt(params.get("variant"), 10) || 0;
        if (params.get("npc") === "janet" || params.get("npc") === "tp2-janet") {
            h1.textContent = "Janet.";
            version.textContent = "Janet " + spyCardsVersion;
            isNPC = true;
        }
        else if (params.has("npc")) {
            h1.textContent += " (NPC)";
            isNPC = true;
        }
        if (params.has("editor")) {
            document.body.insertBefore(form, document.body.firstChild);
            await initCardEditor(form, customCardsRaw);
            return;
        }
    }
    catch (ex) {
        errorHandler(ex);
        debugger;
        return;
    }
    if (customCardsRaw) {
        try {
            const modeVis = await visualizeGameModeInfo(customCardsRaw, variant);
            const vis = await visualizeCustomCardData(customCardsRaw, variant);
            const cardEditorButton = SpyCards.UI.button("Card Editor", ["open-editor-button"], () => {
                location.href = "custom.html?editor#" + customCardsRaw;
            });
            vis.appendChild(cardEditorButton);
            const deckEditorButton = SpyCards.UI.button("Deck Editor", ["open-editor-button"], () => {
                location.href = "decks.html#," + customCardsRaw;
            });
            vis.appendChild(deckEditorButton);
            for (let mv of modeVis) {
                document.body.appendChild(mv);
            }
            document.body.appendChild(vis);
            const parsed = await SpyCards.parseCustomCards(customCardsRaw, variant);
            customCardsRaw = parsed.customCardsRaw.join(",");
            isSinglePlayer = parsed.variant !== null && parsed.mode.getAll(SpyCards.GameModeFieldType.Variant)[parsed.variant].npc;
        }
        catch (ex) {
            const error = document.createElement("div");
            error.classList.add("readme", "custom-card-error");
            const h1 = document.createElement("h1");
            h1.textContent = "Custom Card Error";
            const p = document.createElement("p");
            p.textContent = ex.message;
            error.appendChild(h1);
            error.appendChild(p);
            document.body.appendChild(error);
            return;
        }
    }
    const privacyPolicy = document.createElement("a");
    privacyPolicy.classList.add("privacy-policy-link");
    privacyPolicy.textContent = "Privacy Policy";
    privacyPolicy.rel = "privacy-policy";
    privacyPolicy.href = "privacy.html";
    const btn1 = document.createElement("button");
    btn1.classList.add("btn1");
    btn1.textContent = showCardEditorInsteadOfHost ? "Edit Cards" : "Host";
    const btn2 = document.createElement("button");
    btn2.classList.add("btn2");
    btn2.textContent = "Join";
    const btn3 = document.createElement("button");
    btn3.classList.add("btn3");
    btn3.textContent = "Quick Join";
    const btn4 = document.createElement("button");
    btn4.classList.add("btn3");
    btn4.textContent = "Play Solo";
    form.appendChild(version);
    form.appendChild(h1);
    form.appendChild(privacyPolicy);
    if (isSinglePlayer) {
        form.appendChild(btn4);
    }
    else {
        form.appendChild(btn1);
        form.appendChild(btn2);
        if (spyCardsVersionSuffix !== "-custom") {
            form.appendChild(btn3);
        }
    }
    if (spyCardsClientMode) {
        document.body.appendChild(form);
    }
    else {
        document.body.insertBefore(form, document.body.firstChild);
    }
    const status = document.createElement("p");
    status.setAttribute("role", "status");
    status.setAttribute("aria-live", "polite");
    status.setAttribute("aria-atomic", "true");
    const code = document.createElement("input");
    code.addEventListener("focus", function () {
        code.select();
    });
    code.type = "text";
    let modeName = spyCardsVersionSuffix ? spyCardsVersionSuffix.substr(1) : "";
    if (spyCardsClientMode === "vanilla") {
        location.href = ".";
    }
    if (spyCardsClientMode === "custom") {
        location.href = "custom.html";
    }
    let defs = new SpyCards.CardDefs();
    if (spyCardsClientMode) {
        h1.textContent = "Loading...";
        status.textContent = "Loading custom rule set...";
        form.appendChild(status);
        SpyCards.UI.remove(btn1);
        SpyCards.UI.remove(btn2);
        SpyCards.UI.remove(btn3);
        SpyCards.UI.remove(btn4);
        const forcedRevision = parseInt(new URLSearchParams(location.search).get("rev"), 10);
        SpyCards.fetchCustomCardSet(spyCardsClientMode, forcedRevision).then(async function (response) {
            const { Name, Cards, Revision } = response;
            version.appendChild(document.createTextNode(" (rev. " + Revision + ")"));
            spyCardsVersionVariableSuffix = "-r" + Revision;
            modeName += "." + Revision;
            const modeVis = await visualizeGameModeInfo(Cards, variant === "parse-variant" ? null : variant, Name);
            const vis = await visualizeCustomCardData(Cards, variant);
            const deckEditorButton = SpyCards.UI.button("Deck Editor", ["open-editor-button"], () => {
                location.href = "decks.html#," + spyCardsClientMode + "." + (forcedRevision ? Revision : 0);
            });
            vis.appendChild(deckEditorButton);
            document.title = "Spy Cards Online - " + Name;
            for (let v of modeVis) {
                document.body.appendChild(v);
            }
            document.body.appendChild(vis);
            const parsed = await SpyCards.parseCustomCards(Cards, variant);
            defs = new SpyCards.CardDefs(parsed);
            h1.textContent = Name;
            status.textContent = "";
            isSinglePlayer = parsed.variant !== null && parsed.mode.getAll(SpyCards.GameModeFieldType.Variant)[parsed.variant].npc;
            if (isSinglePlayer) {
                form.appendChild(btn4);
            }
            else {
                form.appendChild(btn1);
                form.appendChild(btn2);
                if (response.QuickJoin) {
                    form.appendChild(btn3);
                }
            }
        }, function (err) {
            console.error(err);
            h1.textContent = "Error";
            status.textContent = "Failed to fetch custom rule set:";
            status.appendChild(document.createElement("br"));
            status.appendChild(document.createTextNode(err.message));
        });
    }
    else if (spyCardsVersionSuffix === "-play") {
        location.href = ".";
    }
    async function buttonClicked(playerNumber) {
        if (showCardEditorInsteadOfHost && playerNumber === 1) {
            await initCardEditor(form);
            return;
        }
        if (playerNumber === 2) {
            document.querySelectorAll(".custom-card-visualization").forEach((el) => SpyCards.UI.remove(el));
        }
        // start preloading assets
        SpyCards.Native.roomRPC();
        document.querySelectorAll("article, #available-cards, .spoiler-guard-warning").forEach((el) => SpyCards.UI.remove(el));
        form.classList.add("active");
        SpyCards.UI.remove(privacyPolicy);
        SpyCards.UI.remove(btn1);
        SpyCards.UI.remove(btn2);
        SpyCards.UI.remove(btn3);
        SpyCards.UI.remove(btn4);
        form.appendChild(status);
        let fatalErrorResolve;
        let sc;
        if (playerNumber === "play-solo") {
            playerNumber = 2;
        }
        else {
            sc = new SignalingConnection();
            sc.initPlayer(playerNumber);
        }
        let isQuickJoin = false;
        if (playerNumber === "q") {
            isQuickJoin = true;
            playerNumber = 2;
        }
        const cgc = new CardGameConnection(isSinglePlayer ? {} : null);
        const ctx = {
            player: playerNumber,
            net: {
                sc: sc,
                cgc: cgc
            },
            scg: cgc.scg,
            ui: {
                form: form,
                h1: h1,
                code: code,
                status: status,
                hadFatalError: false,
                fatalErrorPromise: new Promise((resolve) => fatalErrorResolve = resolve),
                displayFatalError: null
            },
            matchData: new SpyCards.MatchData(),
            defs: defs,
        };
        window.matchCtx = ctx;
        ctx.matchData.customModeName = modeName;
        const connectionStateIndicator = document.createElement("div");
        connectionStateIndicator.classList.add("connection-state");
        connectionStateIndicator.style.display = "none";
        document.body.appendChild(connectionStateIndicator);
        ctx.net.cgc.conn.addEventListener("iceconnectionstatechange", function () {
            switch (ctx.net.cgc.conn.iceConnectionState) {
                case "new":
                    connectionStateIndicator.classList.remove("bad");
                    connectionStateIndicator.style.display = "block";
                    connectionStateIndicator.textContent = "Connecting to opponent...";
                    break;
                case "checking":
                    connectionStateIndicator.classList.remove("bad");
                    connectionStateIndicator.style.display = "block";
                    connectionStateIndicator.textContent = "Checking connection to opponent...";
                    break;
                case "connected":
                case "completed":
                    connectionStateIndicator.classList.remove("bad");
                    connectionStateIndicator.style.display = "none";
                    break;
                case "failed":
                    connectionStateIndicator.classList.add("bad");
                    connectionStateIndicator.style.display = "block";
                    connectionStateIndicator.textContent = "Unable to connect to opponent. If this happens frequently, try using the ";
                    const relayModeLink = document.createElement("a");
                    relayModeLink.href = "settings.html";
                    relayModeLink.textContent = "relayed connection mode";
                    connectionStateIndicator.appendChild(relayModeLink);
                    connectionStateIndicator.appendChild(document.createTextNode("."));
                    break;
                case "disconnected":
                    connectionStateIndicator.classList.add("bad");
                    connectionStateIndicator.style.display = "block";
                    connectionStateIndicator.textContent = "Lost connection to opponent. Trying to reconnect...";
                    break;
            }
        });
        function displayFatalError(message, reset) {
            console.error(message);
            if (ctx.ui.hadFatalError) {
                return;
            }
            fatalErrorResolve();
            SpyCards.Audio.stopMusic();
            ctx.ui.hadFatalError = true;
            SpyCards.UI.clear(form);
            status.textContent = message;
            SpyCards.UI.html.classList.remove("in-match");
            form.classList.remove("active", "hidden", "offscreen");
            form.appendChild(h1);
            form.appendChild(status);
            if (reset) {
                form.appendChild(privacyPolicy);
                form.appendChild(btn1);
                form.appendChild(btn2);
            }
        }
        ctx.ui.displayFatalError = displayFatalError;
        ctx.net.cgc.onVersion = function (v) {
            if (v !== spyCardsVersion + spyCardsVersionVariableSuffix) {
                displayFatalError("Version number difference! Opponent's version is " + v + " and your version is " + spyCardsVersion + spyCardsVersionVariableSuffix);
            }
        };
        ctx.net.cgc.onQuit = function () {
            if (ctx.net.cgc.finalized) {
                return;
            }
            ctx.net.cgc.opponentForfeit = true;
            displayFatalError("Opponent closed match before it was finished.");
        };
        if (ctx.net.sc) {
            ctx.net.sc.onCloseCode = function (errorCode) {
                switch (errorCode) {
                    case 3000:
                        displayFatalError("Matchmaking server error.", true);
                        break;
                    case 3001:
                        displayFatalError("Wrong client for this match code.", true);
                        break;
                    case 3002:
                        if (ctx.net.cgc.usingRelay) {
                            ctx.net.cgc.onQuit();
                        }
                        else {
                            displayFatalError("Opponent did not connect.", true);
                        }
                        break;
                    case 3004:
                        displayFatalError("Match was not found.", true);
                        break;
                    case 1006:
                        if (ctx.net.cgc.conn.remoteDescription) {
                            return;
                        }
                        displayFatalError("Matchmaking connection was unexpectedly closed.", true);
                        break;
                    default:
                        displayFatalError("Unhandled error code (" + errorCode + ")", true);
                        debugger;
                        break;
                }
            };
        }
        const acceptRelay = SpyCards.UI.button("Use Relay", ["btn1"], () => {
            ctx.net.cgc.useRelay();
            acceptRelay.disabled = true;
            status.textContent = "Waiting for opponent to accept...";
        });
        const declineRelay = SpyCards.UI.button("Exit", ["btn2"], () => {
            location.reload();
        });
        ctx.net.cgc.setOnConnectionStateChange(async function (state) {
            SpyCards.UI.remove(acceptRelay);
            SpyCards.UI.remove(declineRelay);
            switch (state) {
                case "connecting":
                    SpyCards.UI.remove(code);
                    status.textContent = "Connecting to opponent...";
                    break;
                case "failed":
                    SpyCards.UI.remove(code);
                    status.textContent = "Connection failed. Try to use experimental relayed connection method?";
                    form.appendChild(acceptRelay);
                    form.appendChild(declineRelay);
                    break;
                case "want-relay":
                    SpyCards.UI.remove(code);
                    status.textContent = "Opponent wants to use the experimental relayed connection method.";
                    form.appendChild(acceptRelay);
                    form.appendChild(declineRelay);
                    break;
                case "connected":
                    if (ctx.net.cgc.usingRelay) {
                        SpyCards.UI.remove(connectionStateIndicator);
                    }
                    ctx.net.cgc.setOnConnectionStateChange(null);
                    if (ctx.net.sc) {
                        ctx.net.cgc.closeSignalingConnection(ctx.net.sc);
                    }
                    addEventListener("beforeunload", function (e) {
                        if (ctx.net.cgc.finalized || ctx.net.cgc.opponentForfeit) {
                            return;
                        }
                        e.preventDefault();
                        e.returnValue = "Are you sure you want to forfeit this match?";
                    });
                    addEventListener("unload", function () {
                        ctx.net.cgc.close();
                    });
                    const guardData = SpyCards.SpoilerGuard.getSpoilerGuardData();
                    ctx.net.cgc.sendSpoilerGuardData(guardData ? guardData.s : "");
                    if (spyCardsVersionSuffix === "-custom") {
                        if (!isSinglePlayer) {
                            if (playerNumber === 1) {
                                ctx.net.cgc.sendCustomCards(customCardsRaw);
                                status.textContent = "Waiting for opponent to accept custom cards...";
                                await ctx.net.cgc.recvCustomCards;
                            }
                            else {
                                status.textContent = "Waiting for opponent to send custom cards...";
                                customCardsRaw = await ctx.net.cgc.recvCustomCards;
                                if (!customCardsRaw) {
                                    displayFatalError("Opponent did not send any cards.", true);
                                    return;
                                }
                                try {
                                    const parsed = await SpyCards.parseCustomCards(customCardsRaw, "parse-variant");
                                    variant = parsed.variant;
                                    document.body.appendChild(await visualizeCustomCardData(customCardsRaw, "parse-variant"));
                                }
                                catch (ex) {
                                    console.error(ex.message, ex, ex.stack);
                                    displayFatalError(ex.message, true);
                                    return;
                                }
                                status.textContent = "The custom cards your opponent sent are listed below.";
                                const acceptButton = document.createElement("button");
                                acceptButton.classList.add("btn1");
                                acceptButton.textContent = "Accept Cards";
                                const acceptPromise = new Promise((resolve) => {
                                    if (isNPC) {
                                        resolve();
                                        return;
                                    }
                                    acceptButton.addEventListener("click", function (e) {
                                        e.preventDefault();
                                        resolve();
                                    });
                                });
                                const declineButton = document.createElement("button");
                                declineButton.classList.add("btn2");
                                declineButton.textContent = "Exit";
                                declineButton.addEventListener("click", function (e) {
                                    e.preventDefault();
                                    location.reload();
                                });
                                form.appendChild(acceptButton);
                                form.appendChild(declineButton);
                                await acceptPromise;
                                ctx.net.cgc.sendCustomCards("");
                                if (variant !== null) {
                                    const parts = customCardsRaw.split(",");
                                    customCardsRaw = parts[0] + (parts.length >= 3 ? "," + parts.slice(2).join(",") : "");
                                    history.pushState(null, document.title, "custom.html?variant=" + variant + "#" + customCardsRaw);
                                }
                                else {
                                    location.hash = customCardsRaw;
                                }
                                SpyCards.UI.remove(acceptButton);
                                SpyCards.UI.remove(declineButton);
                            }
                        }
                        const parsedCards = await SpyCards.parseCustomCards(customCardsRaw, variant);
                        ctx.defs = defs = new SpyCards.CardDefs(parsedCards);
                    }
                    else {
                        ctx.net.cgc.sendCustomCards(customCardsRaw || "");
                    }
                    document.querySelectorAll(".custom-card-visualization, .variant-selector, .enable-audio").forEach((el) => SpyCards.UI.remove(el));
                    SpyCards.runSpyCardsGame(ctx);
                    break;
            }
        });
        if (isQuickJoin) {
            status.textContent = "Enrolling in quick join...";
            playerNumber = parseInt(await sc.sessionID, 10);
            ctx.player = playerNumber;
            ctx.net.cgc.useSignalingServer(ctx.net.sc, playerNumber);
            status.textContent = "Waiting for opponent...";
        }
        else if (isSinglePlayer) {
            const hostCgc = new CardGameConnection({});
            if (customCardsRaw) {
                const parsedCards = await SpyCards.parseCustomCards(customCardsRaw, "parse-variant");
                defs = new SpyCards.CardDefs(parsedCards);
            }
            const hostCtx = {
                player: 1,
                net: {
                    sc: null,
                    cgc: hostCgc,
                },
                scg: hostCgc.scg,
                ui: null,
                matchData: new SpyCards.MatchData(),
                defs: defs,
                fx: {
                    async coin() { },
                    async numb() { },
                    async setup() { },
                    async multiple() { },
                    async assist() { },
                    async beforeProcessEffect() { },
                    async afterProcessEffect() { },
                },
            };
            ctx.npcCtx = hostCtx;
            const npc = SpyCards.AI.getNPC(isSinglePlayer);
            hostCtx.net.sc = new SignalingConnection(true);
            ctx.net.sc = hostCtx.net.sc.fake;
            await Promise.all([
                hostCgc.useSignalingServer(hostCtx.net.sc, 1),
                cgc.useSignalingServer(ctx.net.sc, 2),
                ctx.net.sc.setSessionID("[singleplayer]"),
            ]);
            SpyCards.Minimal.playMatch(hostCtx, npc);
        }
        else if (playerNumber === 1) {
            ctx.net.cgc.useSignalingServer(ctx.net.sc, 1);
            status.textContent = "Waiting for match code...";
            const id = await ctx.net.sc.sessionID;
            status.textContent = "Send the match code to your opponent.";
            if (!status.id) {
                status.id = SpyCards.UI.uniqueID();
            }
            code.setAttribute("aria-labelledby", status.id);
            code.value = id;
            code.readOnly = true;
            form.appendChild(code);
            code.select();
        }
        else {
            ctx.net.cgc.useSignalingServer(ctx.net.sc, 2);
            status.textContent = "Enter the match code from your opponent.";
            if (!status.id) {
                status.id = SpyCards.UI.uniqueID();
            }
            code.setAttribute("aria-labelledby", status.id);
            code.value = "";
            code.readOnly = false;
            form.appendChild(code);
            code.select();
            let debounce = null;
            code.addEventListener("input", function f() {
                if (code.value.replace(/[ \-_]/g, "").length !== 24) {
                    clearTimeout(debounce);
                    debounce = null;
                    return;
                }
                clearTimeout(debounce);
                debounce = setTimeout(function () {
                    ctx.net.sc.setSessionID(code.value);
                    code.removeEventListener("input", f);
                    SpyCards.UI.remove(code);
                    status.textContent = "Requesting connection...";
                }, 1000);
            });
        }
    }
    btn1.addEventListener("click", function (e) {
        e.preventDefault();
        buttonClicked(1);
    });
    btn2.addEventListener("click", function (e) {
        e.preventDefault();
        buttonClicked(2);
    });
    btn3.addEventListener("click", function (e) {
        e.preventDefault();
        buttonClicked("q");
    });
    btn4.addEventListener("click", function (e) {
        e.preventDefault();
        buttonClicked("play-solo");
    });
})();
async function visualizeGameModeInfo(codes, variant, overrideName) {
    const { mode } = await SpyCards.parseCustomCards(codes, variant);
    if (!mode) {
        return [];
    }
    const readmes = [];
    const variants = mode.getAll(SpyCards.GameModeFieldType.Variant);
    if (variants.length) {
        const variantSelector = document.createElement("form");
        variantSelector.classList.add("readme", "variant-selector");
        readmes.push(variantSelector);
        const label = document.createElement("label");
        label.textContent = "Variant: ";
        variantSelector.appendChild(label);
        const select = document.createElement("select");
        for (let i = 0; i < variants.length; i++) {
            const opt = SpyCards.UI.option(variants[i].title || ("Untitled Game Mode Variant #" + (i + 1)), String(i));
            opt.selected = i === Math.min(Math.max(variant, 0), variants.length - 1);
            select.appendChild(opt);
        }
        select.addEventListener("change", () => {
            if (select.value !== String(variant)) {
                const newSearch = new URLSearchParams(location.search);
                newSearch.set("variant", select.value);
                location.href = location.pathname + "?" + newSearch.toString() + location.hash;
            }
        });
        label.appendChild(select);
    }
    for (let meta of mode.getAll(SpyCards.GameModeFieldType.Metadata)) {
        const readme = document.createElement("article");
        readme.classList.add("readme", "game-mode-readme");
        const h1 = document.createElement("h1");
        h1.textContent = overrideName || meta.title || "Untitled Game Mode";
        readme.appendChild(h1);
        if (meta.author) {
            const author = document.createElement("p");
            author.classList.add("author");
            author.textContent = "by " + meta.author;
            readme.appendChild(author);
        }
        appendParagraphs(readme, meta.description);
        if (meta.latestChanges) {
            const h2 = document.createElement("h2");
            h2.textContent = "Latest Changes";
            readme.appendChild(h2);
            appendParagraphs(readme, meta.latestChanges);
        }
        readmes.push(readme);
    }
    return readmes;
    function appendParagraphs(readme, text) {
        for (let para of text.split(/\n{2,}/g)) {
            const p = document.createElement("p");
            for (let line of para.split("\n")) {
                if (p.hasChildNodes()) {
                    p.appendChild(document.createElement("br"));
                }
                p.appendChild(document.createTextNode(line));
            }
            readme.appendChild(p);
        }
    }
}
async function visualizeCustomCardData(codes, variant) {
    const vanillaDefs = new SpyCards.CardDefs();
    const processed = await SpyCards.parseCustomCards(codes, variant);
    const customDefs = new SpyCards.CardDefs(processed);
    const visualization = document.createElement("div");
    visualization.classList.add("readme", "custom-card-visualization");
    if (processed.mode) {
        const bannedCards = document.createElement("div");
        bannedCards.classList.add("banned-cards");
        const bannedCardsH1 = document.createElement("h1");
        bannedCardsH1.textContent = "Banned Cards";
        bannedCards.appendChild(bannedCardsH1);
        let bannedVanilla = false;
        for (let banList of processed.mode.getAll(SpyCards.GameModeFieldType.BannedCards)) {
            if (!banList.bannedCards.length) {
                if (!bannedVanilla) {
                    bannedVanilla = true;
                    const bannedVanillaDesc = document.createElement("p");
                    bannedVanillaDesc.classList.add("banned-cards-special");
                    bannedVanillaDesc.textContent = "This card set bans all vanilla cards.";
                    bannedCards.insertBefore(bannedVanillaDesc, bannedCardsH1.nextSibling);
                }
            }
            else {
                for (let id of banList.bannedCards) {
                    const bannedCard = customDefs.cardsByID[id];
                    if (!bannedCard) {
                        const unknownEl = document.createElement("p");
                        unknownEl.classList.add("banned-cards-special");
                        unknownEl.textContent = "Unknown card #" + id;
                        bannedCards.appendChild(unknownEl);
                    }
                    else if (bannedCard.tp !== Infinity || bannedCard.id < 128) {
                        bannedCards.appendChild(bannedCard.createEl(customDefs));
                    }
                }
            }
        }
        if (bannedCards.lastChild !== bannedCardsH1) {
            visualization.appendChild(bannedCards);
        }
        const rules = processed.mode.get(SpyCards.GameModeFieldType.GameRules);
        if (rules) {
            const defaults = new SpyCards.GameModeGameRules();
            const modifiedRules = document.createElement("div");
            modifiedRules.classList.add("modified-rules");
            const modifiedRulesH1 = document.createElement("h1");
            modifiedRulesH1.textContent = "Modified Rules";
            modifiedRules.appendChild(modifiedRulesH1);
            for (let { type, get } of SpyCards.GameModeGameRules.rules) {
                if (get(rules) === get(defaults)) {
                    continue;
                }
                const modifiedRule = document.createElement("div");
                modifiedRule.classList.add("modified-rule");
                const num0 = document.createElement("span");
                num0.classList.add("num");
                num0.textContent = String(get(defaults));
                modifiedRule.appendChild(num0);
                const replacedWith = document.createElement("span");
                replacedWith.classList.add("replaced-with");
                replacedWith.textContent = SpyCards.GameModeGameRules.typeName(type);
                modifiedRule.appendChild(replacedWith);
                const num1 = document.createElement("span");
                num1.classList.add("num");
                num1.textContent = String(get(rules));
                modifiedRule.appendChild(num1);
                modifiedRules.appendChild(modifiedRule);
            }
            if (modifiedRules.lastChild !== modifiedRulesH1) {
                visualization.appendChild(modifiedRules);
            }
        }
        const section = document.createElement("div");
        const h1 = document.createElement("h1");
        h1.textContent = "Special Rules";
        section.appendChild(h1);
        for (let summon of processed.mode.getAll(SpyCards.GameModeFieldType.SummonCard)) {
            const card = customDefs.cardsByID[summon.card];
            const label = document.createElement("p");
            label.textContent = "At the start of the match, summon this card to ";
            if (summon.flags & SpyCards.SummonCardFlags.BothPlayers) {
                label.textContent += " both sides ";
            }
            else {
                label.textContent += " host side ";
            }
            label.textContent += "of the field:";
            section.appendChild(label);
            if (card) {
                section.appendChild(card.createEl(customDefs));
            }
            else {
                label.textContent += " [UNKNOWN]";
            }
        }
        for (let unfilter of processed.mode.getAll(SpyCards.GameModeFieldType.UnfilterCard)) {
            const card = customDefs.cardsByID[unfilter.card];
            const label = document.createElement("p");
            label.textContent = "Card cannot be targeted by filters:";
            section.appendChild(label);
            if (card) {
                section.appendChild(card.createEl(customDefs));
            }
            else {
                label.textContent += " [UNKNOWN]";
            }
        }
        if (section.lastChild !== h1) {
            visualization.appendChild(section);
        }
    }
    const h1 = document.createElement("h1");
    h1.textContent = "Custom Cards";
    visualization.appendChild(h1);
    const brandNewCards = document.createElement("div");
    brandNewCards.classList.add("new-cards");
    for (let newCard of processed.cards) {
        if (newCard.id >= 128) {
            brandNewCards.appendChild(newCard.createEl(customDefs));
        }
        else {
            const replacedCard = document.createElement("div");
            replacedCard.classList.add("replaced-card");
            replacedCard.appendChild(vanillaDefs.cardsByID[newCard.id].createEl(vanillaDefs));
            const replacedWith = document.createElement("span");
            replacedWith.classList.add("replaced-with");
            replacedWith.textContent = "Replaced With";
            replacedCard.appendChild(replacedWith);
            replacedCard.appendChild(newCard.createEl(customDefs));
            visualization.appendChild(replacedCard);
        }
    }
    if (brandNewCards.children.length) {
        if (visualization.lastChild !== h1) {
            const h2 = document.createElement("h2");
            h2.textContent = "Brand New Cards";
            brandNewCards.insertBefore(h2, brandNewCards.firstChild);
        }
        visualization.appendChild(brandNewCards);
    }
    if (visualization.lastChild === h1) {
        visualization.removeChild(h1);
    }
    return visualization;
}
