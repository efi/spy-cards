package card

import (
	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

// Deck is a partially ordered list of Spy Cards cards, with boss first,
// then mini-boss, and then enemy (attacker and effect) cards.
type Deck []ID

// Validate returns a non-nil error if the deck is not a valid deck
// for the given card set.
func (d Deck) Validate(set *Set) error {
	banned := make(map[ID]bool)
	rules := DefaultGameRules

	if set == nil {
		set = &Set{
			Variant: -1,
		}
	}

	var limits []struct {
		Filter *DeckLimitFilter
		Count  uint64
	}

	if set.Mode != nil {
		mode, _ := set.Mode.Variant(set.Variant)

		for _, f := range mode.GetAll(FieldBannedCards) {
			ban := f.(*BannedCards)
			if len(ban.Cards) == 0 {
				for i := ID(0); i < 128; i++ {
					banned[i] = true
				}
			} else {
				for _, id := range ban.Cards {
					banned[id] = true
				}
			}
		}

		if r := mode.Get(FieldGameRules); r != nil {
			rules = *r.(*GameRules)
		}

		limitFields := mode.GetAll(FieldDeckLimitFilter)
		limits = make([]struct {
			Filter *DeckLimitFilter
			Count  uint64
		}, len(limitFields))

		for i := range limits {
			f := limitFields[i].(*DeckLimitFilter)
			limits[i].Filter = f
		}
	}

	for _, ban := range set.Spoiler {
		for _, id := range ban.Cards {
			banned[id] = true
		}
	}

	if uint64(len(d)) != rules.CardsPerDeck {
		return xerrors.Errorf("card: wrong number of cards in deck (expected %d, actual %d)", rules.CardsPerDeck, len(d))
	}

	numTotalBosses := rules.BossCards + rules.MiniBossCards

	for i, id := range d {
		var expectBack Rank

		switch {
		case uint64(i) < rules.BossCards:
			expectBack = Boss
		case uint64(i) < numTotalBosses:
			expectBack = MiniBoss
		default:
			expectBack = Enemy
		}

		c := set.Card(id)
		if c == nil || (id >= 128 && c != set.Card(id)) {
			return xerrors.Errorf("card: for card %d (ID %d): no such card", i, id)
		}

		if c.Rank.Back() != expectBack {
			return xerrors.Errorf("card: card %d in deck (%q) has wrong card back (expected %v, actual %v)", i, c.DisplayName(), expectBack, id.Rank().Back())
		}

		if banned[id] {
			return xerrors.Errorf("card: card %d in deck (%q) is banned", i, c.DisplayName())
		}

		for i := range limits {
			if limits[i].Filter.IsMatch(c) {
				limits[i].Count++

				if limits[i].Count > limits[i].Filter.Count {
					return xerrors.Errorf("card: card %d in deck (%q) exceeds limit", i, c.DisplayName())
				}
			}
		}
	}

	used := make(map[ID]bool)
	for i := uint64(0); i < rules.CardsPerDeck && i < numTotalBosses; i++ {
		if used[d[i]] {
			c := set.Card(d[i])
			if c == nil {
				return xerrors.Errorf("card: for card %d (ID %d): no such card", i, d[i])
			}

			return xerrors.Errorf("card: card %d in deck (%q) is duplicate", i, c.DisplayName())
		}

		used[d[i]] = true
	}

	return nil
}

// MarshalText implements encoding.TextMarshaler.
func (d Deck) MarshalText() ([]byte, error) {
	b, err := d.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(format.Encode32(b)), nil
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (d Deck) MarshalBinary() ([]byte, error) {
	if len(d) == 0 {
		return nil, nil
	}

	var anyNonBasic, anyAbove255 bool

	noVanilla := true

	for i, c := range d {
		if c < 128 {
			noVanilla = false
		}

		if c > 255 {
			anyAbove255 = true
		}

		if c.BasicIndex() == -1 {
			anyNonBasic = true
		}

		switch c.Rank() {
		case Boss:
			if i != 0 {
				anyNonBasic = true
			}
		case MiniBoss:
			if i != 1 && i != 2 {
				anyNonBasic = true
			}
		case Attacker, Effect:
			if i <= 2 {
				anyNonBasic = true
			}
		}
	}

	if len(d) <= 3 {
		anyNonBasic = true
	}

	var w format.Writer

	switch {
	case noVanilla:
		w.Write(uint8(0x82))

		for _, c := range d {
			w.UVarInt(uint64(c - 128))
		}
	case anyAbove255:
		w.Write(uint8(0x81))

		for _, c := range d {
			w.UVarInt(uint64(c))
		}
	case anyNonBasic:
		w.Write(uint8(0x80))

		for _, c := range d {
			w.Write(uint8(c))
		}
	default:
		w.Write(uint8((d[0].BasicIndex() << 2) | (d[1].BasicIndex() >> 3)))
		w.Write(uint8((d[1].BasicIndex() << 5) | (d[2].BasicIndex())))

		d = d[3:]

		var buf [3]uint8

		for len(d) >= 4 {
			buf[0] = uint8((d[0].BasicIndex() << 2) | (d[1].BasicIndex() >> 4))
			buf[1] = uint8((d[1].BasicIndex() << 4) | (d[2].BasicIndex() >> 2))
			buf[2] = uint8((d[2].BasicIndex() << 6) | (d[3].BasicIndex()))

			w.Bytes(buf[:])

			d = d[4:]
		}

		if len(d) >= 1 {
			buf[0] = uint8(d[0].BasicIndex() << 2)
		}

		if len(d) >= 2 {
			buf[0] |= uint8(d[1].BasicIndex() >> 4)
			buf[1] = uint8(d[1].BasicIndex() << 4)
		}

		if len(d) >= 3 {
			buf[1] |= uint8(d[2].BasicIndex() >> 2)
			buf[2] = uint8(d[2].BasicIndex()<<6) | 63
		}

		w.Bytes(buf[:len(d)])
	}

	return w.Data(), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (d *Deck) UnmarshalText(b []byte) error {
	dec, err := format.Decode32(string(b))
	if err != nil {
		return xerrors.Errorf("card: decoding deck: %w", err)
	}

	return d.UnmarshalBinary(dec)
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (d *Deck) UnmarshalBinary(b []byte) (err error) {
	if len(b) == 0 {
		*d = nil

		return nil
	}

	var r format.Reader

	defer format.Catch(&err)

	r.Init(b[1:])

	var deck Deck

	switch {
	case b[0] < 0x80:
		if len(b) < 2 {
			return xerrors.New("deck: invalid deck code: too short")
		}

		cardIndex := func(back int, index uint8) {
			if len(shortIndex[back]) <= int(index) {
				format.Throw(xerrors.Errorf("deck: invalid card index %d for short code type %d", index, back))
			}

			deck = append(deck, shortIndex[back][index])
		}

		cardIndex(2, b[0]>>2)
		cardIndex(1, ((b[0]&3)<<3)|(b[1]>>5))
		cardIndex(1, b[1]&31)

		b = b[2:]
		for len(b) > 3 {
			cardIndex(0, b[0]>>2)
			cardIndex(0, ((b[0]&3)<<4)|(b[1]>>4))
			cardIndex(0, ((b[1]&15)<<2)|(b[2]>>6))
			cardIndex(0, b[2]&63)
			b = b[3:]
		}

		cardIndex(0, b[0]>>2)

		if len(b) >= 2 {
			cardIndex(0, ((b[0]&3)<<4)|(b[1]>>4))

			if len(b) >= 3 {
				cardIndex(0, ((b[1]&15)<<2)|(b[2]>>6))

				if b[2]&63 != 63 {
					cardIndex(0, b[2]&63)
				}
			}
		}
	case b[0] == 0x80:
		for r.Len() != 0 {
			var c uint8

			r.Read(&c)
			deck = append(deck, ID(c))
		}
	case b[0] == 0x81:
		for r.Len() != 0 {
			c := r.UVarInt()
			deck = append(deck, ID(c))
		}
	case b[0] == 0x82:
		for r.Len() != 0 {
			c := r.UVarInt()
			deck = append(deck, ID(c+128))
		}
	default:
		return xerrors.Errorf("deck: invalid first byte of deck code: %02x", b[0])
	}

	*d = deck

	return nil
}

var shortIndex = func() (index [3][]ID) {
	for i, filter := range []func(Rank) bool{
		func(r Rank) bool { return r == Attacker || r == Effect },
		func(r Rank) bool { return r == MiniBoss },
		func(r Rank) bool { return r == Boss },
	} {
		found := true
		for found {
			found = false
			for id := ID(0); id < 128; id++ {
				if filter(id.Rank()) && id.BasicIndex() == len(index[i]) {
					found = true
					index[i] = append(index[i], id)

					break
				}
			}
		}
	}

	return
}()

// VanillaOrder returns the order of default cards for a specified back design.
func VanillaOrder(back Rank) []ID {
	switch back {
	case Enemy:
		return shortIndex[0]
	case MiniBoss:
		return shortIndex[1]
	case Boss:
		return shortIndex[2]
	default:
		return nil
	}
}

// UnknownDeck is a Deck that only knows the backs of the cards within it.
type UnknownDeck []Rank

// MarshalBinary implements encoding.BinaryMarshaler.
func (ud UnknownDeck) MarshalBinary() ([]byte, error) {
	b := make([]byte, (len(ud)+3)>>2)

	for i, back := range ud {
		var x uint8

		switch back {
		case Enemy:
			x = 0
		case MiniBoss:
			x = 1
		case Boss:
			x = 2
		default:
			return nil, xerrors.Errorf("card: unexpected card back type at index %d: %v", i, back)
		}

		switch i & 3 {
		case 0:
			b[i>>2] = x << 6
		case 1:
			b[i>>2] |= x << 4
		case 2:
			b[i>>2] |= x << 2
		case 3:
			b[i>>2] |= x
		}
	}

	switch len(ud) & 3 {
	case 1:
		b[len(b)-1] |= 0x3f
	case 2:
		b[len(b)-1] |= 0xf
	case 3:
		b[len(b)-1] |= 0x3
	}

	return b, nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (ud *UnknownDeck) UnmarshalBinary(b []byte) error {
	d := make(UnknownDeck, 0, len(b)<<2)

	for i, x := range b {
		for shift := 6; shift >= 0; shift -= 2 {
			switch (x >> shift) & 3 {
			case 0:
				d = append(d, Enemy)
			case 1:
				d = append(d, MiniBoss)
			case 2:
				d = append(d, Boss)
			case 3:
				if i != len(b)-1 {
					return xerrors.Errorf("card: invalid unknown deck: ends at index %d of %d", i, len(b))
				}

				for shift2 := shift; shift2 >= 0; shift2 -= 2 {
					if v := (x >> shift2) & 3; v != 3 {
						return xerrors.Errorf("card: invalid unknown deck: ends before non-padding value %d", v)
					}
				}

				*ud = d

				return nil
			}
		}
	}

	*ud = d

	return nil
}
