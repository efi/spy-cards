"use strict";
const p1Input = document.querySelector("#p1");
const p2Input = document.querySelector("#p2");
const winsP1 = document.querySelector("#wins-p1");
const winsP2 = document.querySelector("#wins-p2");
const matchCount = document.querySelector("#num");
const table = document.querySelector("table tbody");
const variant = parseInt(new URLSearchParams(location.search).get("variant"), 10) || 0;
const submit = document.createElement("button");
submit.textContent = "Simulate Matches";
submit.addEventListener("click", async function (e) {
    e.preventDefault();
    p1Input.disabled = true;
    p2Input.disabled = true;
    matchCount.disabled = true;
    submit.disabled = true;
    const p1 = SpyCards.AI.getNPC(p1Input.value);
    const p2 = SpyCards.AI.getNPC(p2Input.value);
    const defs = new SpyCards.CardDefs(location.hash && await SpyCards.parseCustomCards(location.hash.substr(1), variant));
    const modeName = location.hash && location.hash.indexOf(".") !== -1 && location.hash.indexOf(",") === -1 && location.hash.indexOf(";") === -1 ? location.hash.substr(1) : "";
    let matchNo = 0;
    await SpyCards.Minimal.playMatches(p1, p2, matchCount.valueAsNumber, null, defs, (match) => {
        const tr = document.createElement("tr");
        const td1 = document.createElement("td");
        td1.textContent = String(++matchNo);
        const td2 = document.createElement("td");
        td2.textContent = match.winner === 1 ? p1Input.value + " (P1)" : p2Input.value + " (P2)";
        const td3 = document.createElement("td");
        const recordingLink = document.createElement("a");
        recordingLink.href = "viewer.html#" + Base64.encode(match.recording);
        recordingLink.textContent = "watch";
        td3.appendChild(recordingLink);
        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        table.appendChild(tr);
        if (match.winner === 1)
            winsP1.textContent = String(parseInt(winsP1.textContent, 10) + 1);
        if (match.winner === 2)
            winsP2.textContent = String(parseInt(winsP2.textContent, 10) + 1);
    }, modeName);
    p1Input.disabled = false;
    p2Input.disabled = false;
    matchCount.disabled = false;
    submit.disabled = false;
});
document.querySelector("form").appendChild(submit);
const submitDecks = document.createElement("button");
submitDecks.textContent = "Generate Decks (P1)";
submitDecks.addEventListener("click", async function (e) {
    e.preventDefault();
    const npc = SpyCards.AI.getNPC(p1Input.value);
    const defs = new SpyCards.CardDefs(location.hash && await SpyCards.parseCustomCards(location.hash.substr(1), variant));
    const deckPromises = [];
    for (let i = 0; i < matchCount.valueAsNumber; i++) {
        deckPromises.push(npc.createDeck(defs));
    }
    for (let promise of deckPromises) {
        document.body.appendChild(SpyCards.Decks.createDisplay(defs, await promise));
    }
});
document.querySelector("form").appendChild(submitDecks);
