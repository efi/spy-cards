"use strict";
/**
 * Implementation of Crockford Base 32
 * https://www.crockford.com/base32.html
 */
var CrockfordBase32;
(function (CrockfordBase32) {
    const alphabet = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";
    function normalize(s) {
        s = s.toUpperCase();
        s = s.replace(/O/g, "0");
        s = s.replace(/[IL]/g, "1");
        s = s.replace(/[ \-_]/g, "");
        return s;
    }
    function encode(src) {
        const dst = [];
        let bits = 0, n = 0;
        for (let i = 0; i < src.length; i++) {
            n <<= 8;
            n |= src[i];
            bits += 8;
            while (bits >= 5) {
                bits -= 5;
                dst.push(alphabet.charAt(n >> bits));
                n &= (1 << bits) - 1;
            }
        }
        if (bits) {
            while (bits < 5) {
                n <<= 1;
                bits++;
            }
            dst.push(alphabet.charAt(n));
        }
        return dst.join("");
    }
    CrockfordBase32.encode = encode;
    function decode(src) {
        src = normalize(src);
        const dst = new Uint8Array(Math.floor(src.length / 8 * 5));
        let bits = 0, n = 0;
        for (let i = 0, j = 0; i < src.length; i++) {
            const k = alphabet.indexOf(src.charAt(i));
            if (k === -1) {
                throw new Error("invalid character in string");
            }
            n <<= 5;
            n |= k;
            bits += 5;
            while (bits >= 8) {
                bits -= 8;
                dst[j++] = n >> bits;
                n &= (1 << bits) - 1;
            }
        }
        return dst;
    }
    CrockfordBase32.decode = decode;
})(CrockfordBase32 || (CrockfordBase32 = {}));
// Because atob and btoa are weird, fiddly, and don't properly support nul bytes.
var Base64;
(function (Base64) {
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    function encode(src) {
        const dst = [];
        let bits = 0, n = 0;
        for (let i = 0; i < src.length; i++) {
            n <<= 8;
            n |= src[i];
            bits += 8;
            while (bits >= 6) {
                bits -= 6;
                dst.push(alphabet.charAt(n >> bits));
                n &= (1 << bits) - 1;
            }
        }
        if (bits) {
            while (bits < 6) {
                n <<= 1;
                bits++;
            }
            dst.push(alphabet.charAt(n));
        }
        while (dst.length % 4) {
            dst.push("=");
        }
        return dst.join("");
    }
    Base64.encode = encode;
    function decode(src) {
        src = src.replace(/[=]+$/, "");
        const dst = new Uint8Array(Math.floor(src.length / 8 * 6));
        let bits = 0, n = 0;
        for (let i = 0, j = 0; i < src.length; i++) {
            const k = alphabet.indexOf(src.charAt(i));
            if (k === -1) {
                throw new Error("invalid character in string");
            }
            n <<= 6;
            n |= k;
            bits += 6;
            while (bits >= 8) {
                bits -= 8;
                dst[j++] = n >> bits;
                n &= (1 << bits) - 1;
            }
        }
        return dst;
    }
    Base64.decode = decode;
})(Base64 || (Base64 = {}));
