---
title: Data Formats (Stage)
...

- uvarint Version = 0
- uvarint ElementBufSize
- uint8\[ElementBufSize\] ElementBuf
- uvarint DataBufSize
- uint8\[DataBufSize\] DataBuf
- uvarint SheetLength
- uint8\[SheetLength\] TextureSheet (JPEG)

# Buffers

## Element Buffer

- repeated:
    - uint16\[3\] Triangle

## Data Buffer

- repeated:
    - float32\[3\] VertexPos
    - float32\[2\] TexCoord
