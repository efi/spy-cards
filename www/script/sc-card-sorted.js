"use strict";
var SpyCards;
(function (SpyCards) {
    class CardDefs {
        constructor(overrides) {
            this.cardBacks = CardDefs.cardBacks;
            this.allCards = [];
            this.cardsByID = {};
            this.cardsByBack = [[], [], []];
            this.nonRandom = {
                [SpyCards.Rank.None]: [],
                [SpyCards.Rank.Enemy]: [],
                [SpyCards.Rank.Attacker]: [],
                [SpyCards.Rank.Effect]: [],
                [SpyCards.Rank.MiniBoss]: [],
                [SpyCards.Rank.Boss]: []
            };
            this.byTribe = {
                [SpyCards.Rank.None]: {},
                [SpyCards.Rank.Enemy]: {},
                [SpyCards.Rank.Attacker]: {},
                [SpyCards.Rank.Effect]: {},
                [SpyCards.Rank.MiniBoss]: {},
                [SpyCards.Rank.Boss]: {},
            };
            this.banned = {};
            this.unpickable = {};
            this.unfilter = {};
            this.rules = new SpyCards.GameModeGameRules();
            this.allCards = SpyCards.CardData.createVanillaCardDefs(overrides ? overrides.packedVersion : null);
            if (overrides) {
                if (overrides.mode) {
                    this.mode = overrides.mode;
                    for (let banList of overrides.mode.getAll(SpyCards.GameModeFieldType.BannedCards)) {
                        if (banList.bannedCards.length === 0) {
                            for (let card of this.allCards) {
                                this.banned[card.id] = true;
                            }
                        }
                        else {
                            for (let id of banList.bannedCards) {
                                this.banned[id] = true;
                            }
                        }
                    }
                    for (let unfilter of overrides.mode.getAll(SpyCards.GameModeFieldType.UnfilterCard)) {
                        this.unfilter[unfilter.card] = true;
                    }
                    this.rules = overrides.mode.get(SpyCards.GameModeFieldType.GameRules) || new SpyCards.GameModeGameRules();
                }
                else {
                    this.mode = null;
                }
                this.allCards = this.allCards.filter((c) => !overrides.cards.some((o) => o.id === c.id));
                this.allCards.push(...overrides.cards);
                this.customCardsRaw = overrides.customCardsRaw;
            }
            else {
                this.customCardsRaw = [];
                this.mode = null;
            }
            for (let cd of this.allCards) {
                const rank = cd.rank();
                this.cardsByID[cd.id] = cd;
                if (cd.effectiveTP() !== Infinity) { // negative infinity is okay
                    this.cardsByBack[cd.getBackID()].push(cd);
                }
                if (!cd.effects.some((effect) => effect.type === SpyCards.EffectType.Summon && effect.negate)) {
                    this.nonRandom[SpyCards.Rank.None].push(cd);
                    if (rank === SpyCards.Rank.Attacker || rank === SpyCards.Rank.Effect) {
                        this.nonRandom[SpyCards.Rank.Enemy].push(cd);
                    }
                    this.nonRandom[rank].push(cd);
                }
                this.indexCard(cd, SpyCards.Rank.None, SpyCards.Tribe.None);
                if (rank === SpyCards.Rank.Attacker || rank === SpyCards.Rank.Effect) {
                    this.indexCard(cd, SpyCards.Rank.Enemy, SpyCards.Tribe.None);
                }
                this.indexCard(cd, rank, SpyCards.Tribe.None);
                for (let t of cd.tribes) {
                    this.indexCard(cd, SpyCards.Rank.None, t.tribe, t.custom);
                    if (rank === SpyCards.Rank.Attacker || rank === SpyCards.Rank.Effect) {
                        this.indexCard(cd, SpyCards.Rank.Enemy, t.tribe, t.custom);
                    }
                    this.indexCard(cd, rank, t.tribe, t.custom);
                }
            }
        }
        indexCard(card, rank, tribe, custom) {
            const tribeName = tribe === SpyCards.Tribe.Custom ? "?" + custom.name : SpyCards.Tribe[tribe];
            if (!this.byTribe[rank][tribeName]) {
                this.byTribe[rank][tribeName] = [];
            }
            this.byTribe[rank][tribeName].push(card);
        }
        getAvailableCards(rank, tribe, customTribe) {
            const tribeName = tribe === SpyCards.Tribe.Custom ? "?" + customTribe : SpyCards.Tribe[tribe];
            return (this.byTribe[rank][tribeName] || []).filter((c) => !this.banned[c.id]);
        }
    }
    CardDefs.cardBacks = ["enemy", "mini-boss", "boss"];
    SpyCards.CardDefs = CardDefs;
})(SpyCards || (SpyCards = {}));
