//go:generate stringer -type FontID -trimprefix Font

package sprites

import (
	"image/color"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
)

// FontID is a font.
type FontID uint8

// Available fonts.
const (
	FontD3Streetism FontID = iota
	FontBubblegumSans
)

// TextBatch is a sequence of text rendering operations executed simultaneously.
type TextBatch = Batch

// NewTextBatch creates a TextBatch.
func NewTextBatch(cam *gfx.Camera) *TextBatch {
	return NewBatch(cam)
}

func shadowColor(l rune) color.RGBA {
	if l < 32 {
		return color.RGBA{0, 0, 0, 0}
	}

	return color.RGBA{0, 0, 0, 127}
}

// DrawText draws a string to the screen.
func DrawText(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, color color.RGBA) {
	DrawTextFunc(tb, font, text, x, y, z, scaleX, scaleY, getFunc(color))
}

// DrawTextFunc draws a string to the screen.
func DrawTextFunc(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, getColor func(rune) color.RGBA) {
	DrawTextFuncEx(tb, font, text, x, y, z, scaleX, scaleY, getColor, FlagNoDiscard, 0, 0, 0)
}

// DrawTextShadow draws a string to the screen with a drop shadow.
func DrawTextShadow(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, color color.RGBA) {
	DrawTextShadowFunc(tb, font, text, x, y, z, scaleX, scaleY, getFunc(color))
}

// DrawTextShadowFunc draws a string to the screen with a drop shadow.
func DrawTextShadowFunc(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, color func(rune) color.RGBA) {
	DrawTextFunc(tb, font, text, x-0.1, y-0.1, z, scaleX, scaleY, shadowColor)
	DrawTextFunc(tb, font, text, x, y, z, scaleX, scaleY, color)
}

// DrawTextCentered draws a string to the screen center-justified.
func DrawTextCentered(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, color color.RGBA, shadow bool) {
	DrawTextCenteredFunc(tb, font, text, x, y, z, scaleX, scaleY, getFunc(color), shadow)
}

// DrawTextCenteredFunc draws a string to the screen center-justified.
func DrawTextCenteredFunc(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, color func(rune) color.RGBA, shadow bool) {
	var width float32

	for _, letter := range text {
		width += TextAdvance(font, letter, scaleX)
	}

	x -= width / 2

	if shadow {
		DrawTextShadowFunc(tb, font, text, x, y, z, scaleX, scaleY, color)
	} else {
		DrawTextFunc(tb, font, text, x, y, z, scaleX, scaleY, color)
	}
}

// DrawTextCenteredFuncEx draws a string to the screen center-justified.
func DrawTextCenteredFuncEx(tb *TextBatch, font FontID, text string, x, y, z, scaleX, scaleY float32, color func(rune) color.RGBA, shadow bool, flag RenderFlag, ry, rz, blur float32) {
	var width float32

	for _, letter := range text {
		width += TextAdvance(font, letter, scaleX)
	}

	x -= width / 2

	if shadow {
		DrawTextShadowFunc(tb, font, text, x, y, z, scaleX, scaleY, color)
	} else {
		DrawTextFuncEx(tb, font, text, x, y, z, scaleX, scaleY, color, flag, ry, rz, blur)
	}
}

// TextHitTest returns true if the cursor position is sufficiently close to the text.
// Assumes the text is displayed in a rectangle-ish region of the screen.
func TextHitTest(cam *gfx.Camera, font FontID, text string, x, y, z, scaleX, scaleY, curX, curY float32, centered bool) bool {
	var width float32

	for _, letter := range text {
		width += TextAdvance(font, letter, scaleX)
	}

	if centered {
		x -= width / 2
	}

	curX = curX*2 - 1
	curY = 1 - curY*2

	matrix := gfx.MultiplyMatrix(cam.Perspective, cam.Combined())

	min := gfx.MultiplyVector(matrix, gfx.Vector{x - scaleX*0.1, y - scaleY*0.2, z, 1})
	max := gfx.MultiplyVector(matrix, gfx.Vector{x + width + scaleX*0.1, y + scaleY*0.6, z, 1})

	return min[0] <= curX && max[0] >= curX && min[1] <= curY && max[1] >= curY
}

func getFunc(c color.RGBA) func(rune) color.RGBA {
	if c == Rainbow {
		now := time.Duration(time.Now().UnixNano()).Seconds()

		clamp := func(f float64) uint8 {
			return uint8(math.Min(math.Max(f*255, 0), 255))
		}

		return func(letter rune) color.RGBA {
			if letter < 32 {
				return White
			}

			now++

			return color.RGBA{
				clamp(math.Sin((now+20.0)*5.9) * 2.0),
				clamp(math.Sin((now)*5.9) * 2.0),
				clamp(math.Sin((now+60.0)*5.9) * 2.0),
				255,
			}
		}
	}

	return func(letter rune) color.RGBA {
		return c
	}
}

// Button returns a string that will be rendered as a button icon.
func Button(btn arcade.Button) string {
	return string(byte(btn))
}

// ButtonStyle determines the icon type used to display buttons in text.
var ButtonStyle internal.ButtonStyle
