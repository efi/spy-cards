// +build headless

package main

import (
	"log"

	"git.lubar.me/ben/spy-cards/server"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	server.Main()
}
