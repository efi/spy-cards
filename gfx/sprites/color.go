package sprites

import "image/color"

// Colors for sprites.
var (
	Red     = color.RGBA{255, 0, 0, 255}
	Yellow  = color.RGBA{255, 235, 4, 255}
	Green   = color.RGBA{0, 255, 0, 255}
	Gray    = color.RGBA{127, 127, 127, 255}
	White   = color.RGBA{255, 255, 255, 255}
	Black   = color.RGBA{0, 0, 0, 255}
	Rainbow = color.RGBA{1, 1, 1, 0}
)
