// +build headless

package game

import (
	"context"

	"golang.org/x/xerrors"
)

const isHeadless = true

const SuperSpeed = true

func (s *State) highScore(ctx context.Context) error {
	return xerrors.New("game: highScore should not have been called on server")
}
