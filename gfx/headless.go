// +build headless

package gfx

import (
	"image"

	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

func Size() (w, h int) {
	return 640, 480
}

type StaticBuffer struct {
	Data    gl.Buffer
	Element gl.Buffer
	Count   int
}

func (sb *StaticBuffer) Delete() {
}

func NewStaticBuffer(name string, data []uint8, elements []uint16) *StaticBuffer {
	return &StaticBuffer{}
}

var Square = &StaticBuffer{}

func NextFrame() {
	// unbounded frame rate
}

func onNewTexture(t *Texture) {
}

func (t *Texture) init() {
}

func (t *Texture) Release() {
}

func (a *AssetTexture) init() {
}

func loadAssetImage(name string) (image.Image, error) {
	return nil, xerrors.New("gfx: should not be using textures in headless mode")
}

type Program struct{}
type Uniform struct{}
type Attrib struct{}

func Shader(name, vertex, fragment string) *Program {
	return &Program{}
}

func (p *Program) Uniform(name string) *Uniform {
	return &Uniform{}
}

func (p *Program) Attrib(name string) *Attrib {
	return &Attrib{}
}
