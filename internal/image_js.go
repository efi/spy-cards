// +build js,wasm

package internal

import (
	"image"
	"syscall/js"

	"golang.org/x/xerrors"
)

// DecodePNG decodes a PNG using browser APIs to avoid UI warning messages
// from running too many CPU instructions in sequence.
func DecodePNG(b []byte, grayscale bool) (image.Image, error) {
	img, err := decodeImage(b, "image/png")
	if err != nil {
		return nil, err
	}

	if !grayscale {
		return img, nil
	}

	img2 := &image.Gray{
		Pix:    make([]byte, img.Rect.Dx()*img.Rect.Dy()),
		Stride: img.Rect.Dx(),
		Rect:   img.Rect,
	}

	for i, j := 0, 0; i < len(img2.Pix); i, j = i+1, j+4 {
		img2.Pix[i] = img.Pix[j]
	}

	return img2, nil
}

// DecodeJPEG decodes a JPEG using browser APIs to avoid UI warning messages
// from running too many CPU instructions in sequence.
func DecodeJPEG(b []byte) (image.Image, error) {
	return decodeImage(b, "image/jpeg")
}

func decodeImage(b []byte, mime string) (*image.RGBA, error) {
	jsBuf := js.Global().Get("Uint8Array").New(len(b))
	js.CopyBytesToJS(jsBuf, b)

	jsBlob := js.Global().Get("Blob").New([]interface{}{
		jsBuf,
	}, map[string]interface{}{
		"type": mime,
	})

	u := js.Global().Get("URL").Call("createObjectURL", jsBlob)
	defer js.Global().Get("URL").Call("revokeObjectURL", u)

	jsImg := js.Global().Get("Image").New()

	loaded := make(chan error, 2)

	onLoad := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		loaded <- nil

		return js.Undefined()
	})
	defer onLoad.Release()

	onError := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		loaded <- xerrors.New("internal: failed to load image")

		return js.Undefined()
	})
	defer onError.Release()

	jsImg.Set("onload", onLoad)
	jsImg.Set("onerror", onError)
	jsImg.Set("src", u)

	if err := <-loaded; err != nil {
		return nil, err
	}

	canvas := js.Global().Get("document").Call("createElement", "canvas")
	w, h := jsImg.Get("width"), jsImg.Get("height")
	canvas.Set("width", w)
	canvas.Set("height", h)

	ctx := canvas.Call("getContext", "2d")
	ctx.Call("drawImage", jsImg, 0, 0)

	imageData := ctx.Call("getImageData", 0, 0, w, h)

	pix := make([]byte, imageData.Get("data").Length())
	js.CopyBytesToGo(pix, imageData.Get("data"))

	return &image.RGBA{
		Pix:    pix,
		Stride: w.Int() * 4,
		Rect:   image.Rect(0, 0, w.Int(), h.Int()),
	}, nil
}
