package card_test

import (
	"testing"

	"git.lubar.me/ben/spy-cards/card"
)

func TestVanilla(t *testing.T) {
	for i := 0; i < 256; i++ {
		id := card.ID(i)
		t.Run(id.String(), func(t *testing.T) {
			// this will panic if there is a decoding error
			_ = card.Vanilla(id)
		})
	}
}
