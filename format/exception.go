package format

type uncaught struct{ err error }

// Catch is a deferred function that recovers an error passed to Throw in *err.
func Catch(err *error) {
	if r := recover(); r != nil {
		if ex, ok := r.(uncaught); ok {
			*err = ex.err
		} else {
			panic(r)
		}
	}
}

// Throw panics with the given error, which can then be recovered using Catch.
func Throw(err error) {
	panic(uncaught{err})
}
