package match

import (
	"fmt"
	"log"
	"strings"

	"golang.org/x/xerrors"
)

// GameLog is a human-readable description of the events that occurred in a
// Spy Cards Online match.
type GameLog struct {
	stack    []*logGroup
	Disabled bool
	Debug    bool
}

type logGroup struct {
	title     string
	index     int
	messages  []string
	subGroups []*logGroup
}

func (l *GameLog) cur() *logGroup {
	if len(l.stack) == 0 {
		l.stack = []*logGroup{{}}
	}

	return l.stack[len(l.stack)-1]
}

// Log adds a message to the current log group.
func (l *GameLog) Log(message string) {
	if l.Disabled {
		return
	}

	g := l.cur()
	g.messages = append(g.messages, message)

	if l.Debug {
		log.Printf("DEBUG: game log:%s %s", strings.Repeat("  ", len(l.stack)-1), message)
	}
}

// Logf adds a formatted message to the current log group.
func (l *GameLog) Logf(message string, args ...interface{}) {
	l.Log(fmt.Sprintf(message, args...))
}

// Push adds a child log group to the current log group, making it current.
func (l *GameLog) Push(title string) {
	if l.Disabled {
		return
	}

	g := l.cur()

	sg := &logGroup{
		title: title,
		index: len(g.messages),
	}

	g.messages = append(g.messages, "")
	g.subGroups = append(g.subGroups, sg)
	l.stack = append(l.stack, sg)

	if l.Debug {
		log.Printf("DEBUG: game log:%s %s", strings.Repeat("##", len(l.stack)-1), title)
	}
}

// Pushf adds a log group with a formatted title.
func (l *GameLog) Pushf(title string, args ...interface{}) {
	l.Push(fmt.Sprintf(title, args...))
}

// Pop moves to the parent log level, closing the current log group.
func (l *GameLog) Pop() {
	if l.Disabled {
		return
	}

	g := l.cur()

	if g == l.stack[0] {
		panic(xerrors.New("match: internal error: game log level underflow"))
	}

	l.stack = l.stack[:len(l.stack)-1]
}
