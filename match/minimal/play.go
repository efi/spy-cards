// Package minimal provides functions for playing Spy Cards Online in a
// non-interactive context.
package minimal

import (
	"context"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/matchnet"
	"git.lubar.me/ben/spy-cards/match/npc"
	"golang.org/x/xerrors"
)

// PlayMatch plays a networked Spy Cards Online match and returns the recording.
func PlayMatch(ctx context.Context, m *match.Match, gc *matchnet.GameConn, n npc.NPC) (*card.Recording, error) {
	gc.AwaitMatchReady()

	if c := n.PickPlayer(); c != nil {
		m.Cosmetic[m.Perspective-1].CharacterName = c.Name
	}

	m.IndexSet()

	if err := gc.SendCosmeticData(m.Cosmetic[m.Perspective-1]); err != nil {
		return nil, xerrors.Errorf("minimal: sending cosmetic data: %w", err)
	}

	d := n.CreateDeck(m.Set)

	m.State.Sides[m.Perspective-1].Deck = make([]*match.Card, len(d))

	for i, id := range d {
		c := m.Set.Card(id)

		m.State.Sides[m.Perspective-1].Deck[i] = &match.Card{
			Set:  m.Set,
			Def:  c,
			Back: c.Rank.Back(),
		}
	}

	gc.SendDeck(ctx, d)

	od := gc.WaitRecvDeck()
	m.State.Sides[2-m.Perspective].Deck = make([]*match.Card, len(od))

	for i, b := range od {
		m.State.Sides[2-m.Perspective].Deck[i] = &match.Card{
			Set:  m.Set,
			Back: b,
		}
	}

	recvData := make(chan *card.TurnData, 1)

	for m.Winner() == 0 {
		gc.BeginTurn(recvData)
		turnData := <-recvData

		if m.State.Round == 0 {
			m.InitState(gc.SCG.Rand(matchnet.RNGShared), gc)
		}

		m.State.Round++

		m.ShuffleAndDraw(gc.SCG, false)

		if err := m.SyncInHand(gc.ExchangeInHand, turnData); err != nil {
			return nil, xerrors.Errorf("minimal: failed to sync hand state: %w", err)
		}

		m.ApplyInHand()

		turnData.Ready[m.Perspective-1] = n.PlayRound(m)

		for i, c := range m.State.Sides[m.Perspective-1].Hand {
			if turnData.Ready[m.Perspective-1]&(1<<i) != 0 {
				turnData.Played[m.Perspective-1] = append(turnData.Played[m.Perspective-1], c.Def.ID)
			}
		}

		gc.SendReady(turnData, m.Perspective)
		gc.WaitRecvReady(turnData, m.Perspective)

		m.BeginTurn(turnData)

		rng := gc.SCG.Rand(matchnet.RNGShared)

		q := m.ProcessQueuedEffect(rng, gc)
		for len(q) != 0 {
			q = m.ProcessQueuedEffect(rng, gc)
		}
	}

	return m.Finalize(gc.FinalizeMatch)
}
