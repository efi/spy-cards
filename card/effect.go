//go:generate stringer -type EffectType -trimprefix Effect

package card

import (
	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

// EffectType is an enumeration of effect types.
type EffectType uint8

// Constants for EffectType.
const (
	FlavorText                 EffectType = 0
	EffectStat                 EffectType = 1
	EffectEmpower              EffectType = 2
	EffectSummon               EffectType = 3
	EffectHeal                 EffectType = 4
	EffectTP                   EffectType = 5
	EffectNumb                 EffectType = 6
	EffectRawStat              EffectType = 7
	EffectMultiplyHealing      EffectType = 8
	EffectPreventNumb          EffectType = 9
	EffectModifyAvailableCards EffectType = 10
	EffectModifyCardCost       EffectType = 11
	EffectDrawCard             EffectType = 12
	CondCard                   EffectType = 128
	CondLimit                  EffectType = 129
	CondWinner                 EffectType = 130
	CondApply                  EffectType = 131
	CondCoin                   EffectType = 132
	CondStat                   EffectType = 133
	CondInHand                 EffectType = 134
	CondLastEffect             EffectType = 135
	CondOnNumb                 EffectType = 136
	CondMultipleEffects        EffectType = 137
)

// EffectDef is a Spy Cards Online card effect definition.
type EffectDef struct {
	Type     EffectType
	Priority uint8
	Flags    EffectFlag

	Text    string
	Amount  int64
	Amount2 int64
	Filter  Filter

	Result  *EffectDef
	Result2 *EffectDef
}

// Marshal encodes the EffectDef in a binary format.
func (e *EffectDef) Marshal(w *format.Writer) (err error) {
	defer format.Catch(&err)

	w.Write(e.Type)
	w.Write(e.Priority)
	w.UVarInt(uint64(e.Flags))

	switch e.Type {
	case FlavorText:
		w.String(e.Text)
	case EffectStat, EffectHeal, EffectTP, EffectRawStat, EffectMultiplyHealing, EffectDrawCard:
		w.SVarInt(e.Amount)
	case EffectEmpower, EffectModifyCardCost:
		w.SVarInt(e.Amount)

		if err := e.Filter.Marshal(w); err != nil {
			return xerrors.Errorf("in filter: %w", err)
		}
	case EffectSummon, EffectNumb, EffectPreventNumb, EffectModifyAvailableCards:
		w.UVarInt(uint64(e.Amount))

		if err := e.Filter.Marshal(w); err != nil {
			return xerrors.Errorf("in filter: %w", err)
		}
	case CondWinner, CondApply, CondLastEffect, CondOnNumb:
		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondStat:
		w.SVarInt(e.Amount)

		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondLimit:
		w.UVarInt(uint64(e.Amount))

		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondCard:
		w.UVarInt(uint64(e.Amount))

		if err := e.Filter.Marshal(w); err != nil {
			return xerrors.Errorf("in filter: %w", err)
		}

		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondCoin:
		w.UVarInt(uint64(e.Amount))

		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in heads result: %w", err)
		}

		if e.Flags&FlagCondCoinTails != 0 {
			if err := e.Result2.Marshal(w); err != nil {
				return xerrors.Errorf("in tails result: %w", err)
			}
		}
	case CondInHand:
		w.UVarInt(uint64(e.Amount))
		w.UVarInt(uint64(e.Amount2))

		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondMultipleEffects:
		if err := e.Result.Marshal(w); err != nil {
			return xerrors.Errorf("in result 1: %w", err)
		}

		if err := e.Result2.Marshal(w); err != nil {
			return xerrors.Errorf("in result 2: %w", err)
		}
	default:
		return xerrors.Errorf("card: unhandled effect type: %v", e.Type)
	}

	return nil
}

// Unmarshal decodes the EffectDef from a binary format.
func (e *EffectDef) Unmarshal(r *format.Reader, formatVersion uint64) (err error) {
	defer format.Catch(&err)

	switch formatVersion {
	case 0, 1:
		return e.unmarshalV1(r, formatVersion)
	case 2, 4:
		return e.unmarshalV4(r, formatVersion)
	case 5:
		break
	default:
		return xerrors.Errorf("card: unhandled effect version: %d", formatVersion)
	}

	r.Read(&e.Type)
	r.Read(&e.Priority)
	e.Flags = EffectFlag(r.UVarInt())

	e.Text = ""
	e.Amount = 0
	e.Amount2 = 0
	e.Filter = nil
	e.Result = nil
	e.Result2 = nil

	switch e.Type {
	case FlavorText:
		e.Text = r.String()
	case EffectStat, EffectHeal, EffectTP, EffectRawStat, EffectMultiplyHealing, EffectDrawCard:
		e.Amount = r.SVarInt()
	case EffectEmpower, EffectModifyCardCost:
		e.Amount = r.SVarInt()

		if err := e.Filter.Unmarshal(r); err != nil {
			return xerrors.Errorf("in filter: %w", err)
		}
	case EffectSummon, EffectNumb, EffectPreventNumb, EffectModifyAvailableCards:
		e.Amount = int64(r.UVarInt())

		if err := e.Filter.Unmarshal(r); err != nil {
			return xerrors.Errorf("in filter: %w", err)
		}
	case CondWinner, CondApply, CondLastEffect, CondOnNumb:
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondStat:
		e.Amount = r.SVarInt()
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondLimit:
		e.Amount = int64(r.UVarInt())
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondCard:
		e.Amount = int64(r.UVarInt())

		if err := e.Filter.Unmarshal(r); err != nil {
			return xerrors.Errorf("in filter: %w", err)
		}

		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondCoin:
		e.Amount = int64(r.UVarInt())
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in heads result: %w", err)
		}

		if e.Flags&FlagCondCoinTails != 0 {
			e.Result2 = &EffectDef{}

			if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
				return xerrors.Errorf("in tails result: %w", err)
			}
		}
	case CondInHand:
		e.Amount = int64(r.UVarInt())
		e.Amount2 = int64(r.UVarInt())
		e.Result = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result: %w", err)
		}
	case CondMultipleEffects:
		e.Result = &EffectDef{}
		e.Result2 = &EffectDef{}

		if err := e.Result.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result 1: %w", err)
		}

		if err := e.Result2.Unmarshal(r, formatVersion); err != nil {
			return xerrors.Errorf("in result 2: %w", err)
		}
	default:
		return xerrors.Errorf("card: unhandled effect type: %v", e.Type)
	}

	var maxFlag int

	switch e.Type {
	case FlavorText:
		maxFlag = 0
	case EffectStat:
		maxFlag = 1
	case EffectEmpower:
		maxFlag = 2
	case EffectSummon:
		maxFlag = 2
	case EffectHeal:
		maxFlag = 2
	case EffectTP:
		maxFlag = 0
	case EffectNumb:
		maxFlag = 5
	case EffectRawStat:
		maxFlag = 2
	case EffectMultiplyHealing:
		maxFlag = 3

		if e.Flags&FlagMultiplyHealingTypeMask == FlagMultiplyHealingTypeMask {
			return xerrors.New("card: invalid flag value for MultiplyHealing")
		}
	case EffectPreventNumb:
		maxFlag = 0
	case EffectModifyAvailableCards:
		maxFlag = 4

		if e.Flags&FlagModifyCardsTypeMask == FlagModifyCardsTypeMask {
			return xerrors.New("card: invalid flag value for ModifyAvailableCards")
		}

		if e.Flags&(FlagModifyCardsTypeMask|FlagModifyCardsTargetMask) == FlagModifyCardsTypeMove|FlagModifyCardsTargetField {
			return xerrors.New("card: invalid flag value for ModifyAvailableCards")
		}
	case EffectModifyCardCost:
		maxFlag = -1
	case EffectDrawCard:
		maxFlag = 1
	case CondCard:
		maxFlag = 2

		if e.Flags&FlagCondCardTypeMask == FlagCondCardTypeMask {
			return xerrors.New("card: invalid flag value for CondCard")
		}
	case CondLimit:
		maxFlag = 0
	case CondWinner:
		maxFlag = 1
	case CondApply:
		maxFlag = 2
	case CondCoin:
		maxFlag = 2
	case CondStat:
		maxFlag = 4
	case CondInHand:
		maxFlag = 1
	case CondLastEffect:
		maxFlag = -1
	case CondOnNumb:
		maxFlag = -1
	case CondMultipleEffects:
		maxFlag = -1
	default:
		return xerrors.Errorf("card: unhandled effect type for flags: %v", e.Type)
	}

	if e.Flags & ^(1<<(maxFlag+1)-1) != 0 {
		return xerrors.Errorf("card: invalid flag value for %v", e.Type)
	}

	return nil
}

// UpdateID changes all references to oldID to newID.
func (e *EffectDef) UpdateID(oldID, newID ID) {
	if e.Filter.IsSingleCard() && e.Filter[0].CardID == oldID {
		e.Filter[0].CardID = newID
	}

	if e.Result != nil {
		e.Result.UpdateID(oldID, newID)
	}

	if e.Result2 != nil {
		e.Result2.UpdateID(oldID, newID)
	}
}
