package input

import "golang.org/x/mobile/event/key"

// State is a snapshot of the input context state.
type State struct {
	tick       uint64
	wasPressed [10]uint64
	pressed    [10]bool
	held       [10]bool
	click      bool
	lastClick  bool
	x, y       float32
	lastX      float32
	lastY      float32
	wheel      float32
	drag       float32
	key        []key.Event
}

// SaveState takes a snapshot of the input state.
func (c *Context) SaveState() State {
	return c.state
}

// LoadState loads the input state from a snapshot.
func (c *Context) LoadState(s State) {
	c.state = s
}
