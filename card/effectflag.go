package card

// EffectFlag is a combination bitfield type for all effect flags.
type EffectFlag uint64

// Constants for EffectFlag.
const (
	FlagFlavorTextHideRemaining EffectFlag = 1 << 0

	FlagStatDEF      EffectFlag = 1 << 0
	FlagStatInfinity EffectFlag = 1 << 1
	FlagStatOpponent EffectFlag = 1 << 2

	FlagSummonOpponent  EffectFlag = 1 << 0
	FlagSummonReplace   EffectFlag = 1 << 1
	FlagSummonInvisible EffectFlag = 1 << 2

	FlagHealInfinity EffectFlag = 1 << 0
	FlagHealOpponent EffectFlag = 1 << 1
	FlagHealRaw      EffectFlag = 1 << 2

	FlagTPInfinity EffectFlag = 1 << 0

	FlagNumbInfinity  EffectFlag = 1 << 0
	FlagNumbSelf      EffectFlag = 1 << 1
	FlagNumbHighest   EffectFlag = 1 << 2
	FlagNumbIgnoreATK EffectFlag = 1 << 3
	FlagNumbIgnoreDEF EffectFlag = 1 << 4
	FlagNumbAvoidZero EffectFlag = 1 << 5

	FlagMultiplyHealingOpponent     EffectFlag = 1 << 0
	FlagMultiplyHealingFuture       EffectFlag = 1 << 1
	FlagMultiplyHealingTypeMask     EffectFlag = 3 << 2
	FlagMultiplyHealingTypeAll      EffectFlag = 0 << 2
	FlagMultiplyHealingTypeNegative EffectFlag = 1 << 2
	FlagMultiplyHealingTypePositive EffectFlag = 2 << 2

	FlagPreventNumbOpponent EffectFlag = 1 << 0

	FlagModifyCardsOpponent    EffectFlag = 1 << 0
	FlagModifyCardsTypeMask    EffectFlag = 3 << 1
	FlagModifyCardsTypeAdd     EffectFlag = 0 << 1
	FlagModifyCardsTypeRemove  EffectFlag = 1 << 1
	FlagModifyCardsTypeMove    EffectFlag = 2 << 1
	FlagModifyCardsTargetMask  EffectFlag = 3 << 3
	FlagModifyCardsTargetAny   EffectFlag = 0 << 3
	FlagModifyCardsTargetHand  EffectFlag = 1 << 3
	FlagModifyCardsTargetDeck  EffectFlag = 2 << 3
	FlagModifyCardsTargetField EffectFlag = 3 << 3

	FlagDrawCardOpponent  EffectFlag = 1 << 0
	FlagDrawCardIgnoreMax EffectFlag = 1 << 1

	FlagCondCardOpponent         EffectFlag = 1 << 0
	FlagCondCardTypeMask         EffectFlag = 3 << 1
	FlagCondCardTypeGreaterEqual EffectFlag = 0 << 1
	FlagCondCardTypeLessThan     EffectFlag = 1 << 1
	FlagCondCardTypeEach         EffectFlag = 2 << 1

	FlagCondLimitGreaterThan EffectFlag = 1 << 0

	FlagCondWinnerTypeMask   EffectFlag = 3 << 0
	FlagCondWinnerTypeWinner EffectFlag = 0 << 0
	FlagCondWinnerTypeLoser  EffectFlag = 1 << 0
	FlagCondWinnerTypeTie    EffectFlag = 2 << 0
	FlagCondWinnerTypeNotTie EffectFlag = 3 << 0

	FlagCondApplyNextRound    EffectFlag = 1 << 0
	FlagCondApplyOpponent     EffectFlag = 1 << 1
	FlagCondApplyOriginalText EffectFlag = 1 << 2

	FlagCondCoinTails    EffectFlag = 1 << 0
	FlagCondCoinNegative EffectFlag = 1 << 1
	FlagCondCoinStat     EffectFlag = 1 << 2

	FlagCondStatOpponent EffectFlag = 1 << 0
	FlagCondStatLessThan EffectFlag = 1 << 1
	FlagCondStatTypeMask EffectFlag = 3 << 2
	FlagCondStatTypeATK  EffectFlag = 0 << 2
	FlagCondStatTypeDEF  EffectFlag = 1 << 2
	FlagCondStatTypeHP   EffectFlag = 2 << 2
	FlagCondStatTypeTP   EffectFlag = 3 << 2
	FlagCondStatMultiple EffectFlag = 1 << 4

	FlagCondInHandHide   EffectFlag = 1 << 0
	FlagCondInHandOnPlay EffectFlag = 1 << 1
)
