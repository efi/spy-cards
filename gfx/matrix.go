package gfx

import "math"

// Vector is a 4-component geometric vector of (x, y, z, 1).
type Vector [4]float32

// Matrix is a 4x4 transformation matrix.
type Matrix [16]float32

// Identity returns the identity matrix.
func Identity() Matrix {
	return Matrix{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// Translation constructs translation matrix.
func Translation(x, y, z float32) Matrix {
	return Matrix{
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		x, y, z, 1,
	}
}

// Scale constructs a scaling matrix.
func Scale(x, y, z float32) Matrix {
	return Matrix{
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1,
	}
}

// RotationX constructs a rotation matrix over the X axis.
func RotationX(r float32) Matrix {
	s := float32(math.Sin(float64(r)))
	c := float32(math.Cos(float64(r)))

	return Matrix{
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s, c, 0,
		0, 0, 0, 1,
	}
}

// RotationY constructs a rotation matrix over the Y axis.
func RotationY(r float32) Matrix {
	s := float32(math.Sin(float64(r)))
	c := float32(math.Cos(float64(r)))

	return Matrix{
		c, 0, s, 0,
		0, 1, 0, 0,
		-s, 0, c, 0,
		0, 0, 0, 1,
	}
}

// RotationZ constructs a rotation matrix over the Z axis.
func RotationZ(r float32) Matrix {
	s := float32(math.Sin(float64(r)))
	c := float32(math.Cos(float64(r)))

	return Matrix{
		c, -s, 0, 0,
		s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	}
}

// Perspective constructs a perspective matrix.
func Perspective(fov, aspect, nearZ, farZ float32) Matrix {
	f := float32(1 / math.Tan(float64(fov)/2))

	return Matrix{
		f / aspect, 0, 0, 0,
		0, f, 0, 0,
		0, 0, (farZ + nearZ) / (nearZ - farZ), -1,
		0, 0, (2 * farZ * nearZ) / (nearZ - farZ), 0,
	}
}

// MultiplyVector multiplies a matrix by a vector.
func MultiplyVector(m Matrix, v Vector) Vector {
	return Vector{
		m[0]*v[0] + m[4]*v[1] + m[8]*v[2] + m[12]*v[3],
		m[1]*v[0] + m[5]*v[1] + m[9]*v[2] + m[13]*v[3],
		m[2]*v[0] + m[6]*v[1] + m[10]*v[2] + m[14]*v[3],
		m[3]*v[0] + m[7]*v[1] + m[11]*v[2] + m[15]*v[3],
	}
}

// MultiplyMatrix multiplies a matrix by a matrix.
func MultiplyMatrix(m0, m1 Matrix) Matrix {
	x := MultiplyVector(m0, Vector{m1[0], m1[1], m1[2], m1[3]})
	y := MultiplyVector(m0, Vector{m1[4], m1[5], m1[6], m1[7]})
	z := MultiplyVector(m0, Vector{m1[8], m1[9], m1[10], m1[11]})
	w := MultiplyVector(m0, Vector{m1[12], m1[13], m1[14], m1[15]})

	return Matrix{
		x[0], x[1], x[2], x[3],
		y[0], y[1], y[2], y[3],
		z[0], z[1], z[2], z[3],
		w[0], w[1], w[2], w[3],
	}
}
