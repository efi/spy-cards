// +build !headless

package visual

import "git.lubar.me/ben/spy-cards/card"

func (v *Visual) setReady() bool {
	mask := v.turnData.Ready[v.Match.Perspective-1]

	tp := v.Match.State.Sides[v.Match.Perspective-1].TP

	if (tp.NaN || tp.AmountInf < 0) && mask != 0 {
		return false
	}

	if tp.AmountInf == 0 {
		for i, c := range v.myHand.Cards {
			if mask&(1<<i) == 0 {
				continue
			}

			tp.Amount -= c.Def.TP + v.Match.State.Sides[v.Match.Perspective-1].ModTP[c.Def.ID]
		}

		if tp.Amount < 0 {
			return false
		}
	}

	v.myReady = true

	v.turnData.InHand[v.Match.Perspective-1] = 0
	v.turnData.HandID[v.Match.Perspective-1] = nil

	for i, c := range v.Match.State.Sides[v.Match.Perspective-1].Hand {
		for _, e := range c.Def.Effects {
			if e.Type == card.CondInHand {
				v.turnData.InHand[v.Match.Perspective-1] |= 1 << i
				v.turnData.HandID[v.Match.Perspective-1] = append(v.turnData.HandID[v.Match.Perspective-1], c.Def.ID)

				break
			}
		}
	}

	v.gc.SendReady(v.turnData, v.Match.Perspective)

	return true
}
