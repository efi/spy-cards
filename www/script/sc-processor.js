"use strict";
var SpyCards;
(function (SpyCards) {
    class GameLog {
        constructor() {
            this.entries = [];
            this.entryDepth = 0;
            this.el = document.createElement("details");
            const summary = document.createElement("summary");
            summary.textContent = "Game Log";
            this.el.appendChild(summary);
            this.el.classList.add("game-log");
            this.current = this.el;
        }
        startGroup(label, collapsed) {
            const group = document.createElement("details");
            group.open = !collapsed;
            const summary = document.createElement("summary");
            summary.textContent = label;
            group.appendChild(summary);
            this.current.appendChild(group);
            this.current = group;
            this.pushEntry([label]);
            this.entryDepth++;
        }
        endGroup() {
            this.current = this.current.parentNode;
            this.entryDepth--;
        }
        log(message) {
            const entry = document.createElement("div");
            entry.textContent = message;
            this.current.appendChild(entry);
            this.pushEntry(message);
        }
        pushEntry(entry) {
            let parent = this.entries;
            for (let i = 0; i < this.entryDepth; i++) {
                parent = parent[parent.length - 1];
            }
            parent.push(entry);
        }
    }
    SpyCards.GameLog = GameLog;
    class ProcessorCallbacks {
        async beforeRound() { }
        async afterRound() { }
        async drawCard(player, card) { }
        async showStats() { }
        async beforeComputeWinner() { }
        async afterComputeWinner() { }
        async hpChange(player, amountBefore, amountChanged) { }
        async statChange(player, stat, amountBefore, amountChanged, amountAfter) { }
        async cardSummoned(summoner, summoned, special, effects) { }
        async addEffect(card, effect, alreadyProcessed, fromSummon) { }
        async beforeAddEffects(card, effects, delayProcessing) { }
    }
    SpyCards.ProcessorCallbacks = ProcessorCallbacks;
    class Processor {
        constructor(ctx, callbacks) {
            this.overrideSong = null;
            this.effect = {
                heal: async (card, effect, player, amount) => {
                    amount *= (amount < 0 ? this.ctx.state.healMultiplierN : this.ctx.state.healMultiplierP)[player - 1];
                    (amount < 0 ? this.ctx.state.healTotalN : this.ctx.state.healTotalP)[player - 1] += amount;
                    await this.callbacks.hpChange(player, this.ctx.state.hp[player - 1], amount);
                    this.ctx.state.hp[player - 1] = Math.min(this.ctx.state.hp[player - 1] + amount, this.ctx.defs.rules.maxHP);
                },
                multiplyHealing: async (card, effect, player, amount, negative) => {
                    if (negative !== true) {
                        // positive healing
                        if (this.ctx.state.healMultiplierP[player - 1]) {
                            await this.ctx.effect.heal(card, effect, player, this.ctx.state.healTotalP[player - 1] * (amount - 1) / this.ctx.state.healMultiplierP[player - 1]);
                            this.ctx.state.healMultiplierP[player - 1] *= amount;
                        }
                    }
                    if (negative !== false) {
                        // negative healing
                        if (this.ctx.state.healMultiplierN[player - 1]) {
                            await this.ctx.effect.heal(card, effect, player, this.ctx.state.healTotalN[player - 1] * (amount - 1) / this.ctx.state.healMultiplierN[player - 1]);
                            this.ctx.state.healMultiplierN[player - 1] *= amount;
                        }
                    }
                },
                modifyStat: async (card, target, effect, player, stat, amount) => {
                    if (target) {
                        const key = ("modified" + stat);
                        target[key] = target[key] || 0;
                        target[key] += amount;
                    }
                    else {
                        const key = ("raw" + stat);
                        this.ctx.state[key][player - 1] += amount;
                    }
                    await this.callbacks.statChange(player, stat, this.ctx.state[stat][player - 1], amount, this.ctx.state[stat][player - 1] + amount);
                    this.ctx.state[stat][player - 1] += amount;
                    this.ctx.state.gameLog.log("Player " + player + " " + stat + " is now " + this.ctx.state[stat][player - 1] + " (changed by " + amount + ")");
                },
                recalculateStats: async (player) => {
                    const oldATK = this.ctx.state.ATK[player - 1];
                    const oldDEF = this.ctx.state.DEF[player - 1];
                    let newATK = this.ctx.state.rawATK[player - 1];
                    let newDEF = this.ctx.state.rawDEF[player - 1];
                    for (let card of this.ctx.state.played[player - 1]) {
                        if (card.numb) {
                            continue;
                        }
                        newATK += card.modifiedATK || 0;
                        newDEF += card.modifiedDEF || 0;
                    }
                    await this.callbacks.statChange(player, "ATK", oldATK, newATK - oldATK, newATK);
                    await this.callbacks.statChange(player, "DEF", oldDEF, newDEF - oldDEF, newDEF);
                    this.ctx.state.ATK[player - 1] = newATK;
                    this.ctx.state.DEF[player - 1] = newDEF;
                },
                summonCard: async (player, summoner, toSummon, special, effects) => {
                    const summonedCard = {
                        card: toSummon,
                        back: toSummon.getBackID(),
                        player: player,
                        effects: []
                    };
                    effects = effects || toSummon.effects;
                    await this.callbacks.cardSummoned(summoner, summonedCard, special, effects);
                    this.ctx.state.gameLog.log("Player " + summoner.player + " summoned card " + toSummon.displayName() + (player !== summoner.player ? " for player " + player : ""));
                    this.ctx.state.played[player - 1].push(summonedCard);
                    for (let effect of effects) {
                        await this.ctx.effect.applyResult(summonedCard, null, effect, true);
                    }
                },
                applyResult: async (card, effect, result, delayProcessing) => {
                    this.ctx.state.gameLog.log("Adding effects " + SpyCards.EffectType[result.type] + " to player " + card.player + " card " + card.card.displayName());
                    await this.callbacks.beforeAddEffects(card, [result], delayProcessing);
                    const resultType = result.type === SpyCards.EffectType.CondPriority ? result.amount : result.type;
                    const alreadyProcessed = this.processed[resultType] ? this.processed[resultType].some((f) => Processor.checkFilter(f.filter, result)) : false;
                    await this.callbacks.addEffect(card, result, alreadyProcessed, delayProcessing);
                    if (alreadyProcessed) {
                        if (delayProcessing) {
                            this.ctx.state.effectBacklog.push({ card: card, effect: result });
                        }
                        else {
                            this.ctx.state.currentCard = card;
                            this.ctx.state.currentEffect = result;
                            this.ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[result.type] + " for player " + card.player + " card " + card.card.displayName());
                            await this.ctx.fx.beforeProcessEffect(card, result, true);
                            await SpyCards.Effect.dispatch(this.ctx, card, result);
                            await this.ctx.fx.afterProcessEffect(card, result, true);
                        }
                        return;
                    }
                    card.effects.push(result);
                }
            };
            this.ctx = ctx;
            this.callbacks = callbacks;
        }
        checkWinner() {
            const p1Dead = this.ctx.state.hp[0] <= 0;
            const p2Dead = this.ctx.state.hp[1] <= 0;
            if (p1Dead != p2Dead) {
                return p1Dead ? 2 : 1;
            }
            if (p1Dead && p2Dead) {
                return this.ctx.state.winner;
            }
            return 0;
        }
        static checkFilter(filter, effect) {
            const effectType = effect.type === SpyCards.EffectType.CondPriority ? effect.amount : effect.type;
            if (filter.type !== effectType) {
                return false;
            }
            for (let [flag, expected] of [
                [effect.negate, filter.negate],
                [effect.opponent, filter.opponent],
                [effect.each, filter.each],
                [effect.late, filter.late],
                [effect.generic, filter.generic],
                [effect.defense, filter.defense],
                [effect._reserved6, filter._reserved6],
                [effect._reserved7, filter._reserved7]
            ]) {
                if (expected === false && flag) {
                    return false;
                }
                if (expected === true && !flag) {
                    return false;
                }
            }
            return true;
        }
        async processOne(card, effect, skipCallbacks) {
            this.ctx.state.currentCard = card;
            this.ctx.state.currentEffect = effect;
            if (card.numb && effect.type !== SpyCards.EffectType.Numb) {
                return;
            }
            this.ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[effect.type] + " for player " + card.player + " card " + card.card.displayName());
            if (!skipCallbacks) {
                await this.ctx.fx.beforeProcessEffect(card, effect);
            }
            await SpyCards.Effect.dispatch(this.ctx, card, effect);
            if (!skipCallbacks) {
                await this.ctx.fx.afterProcessEffect(card, effect);
            }
        }
        async processEffect(filter, noAnim) {
            this.processed[filter.type] = this.processed[filter.type] || [];
            this.processed[filter.type].push({ filter: filter, animate: !noAnim });
            for (let player = 0; player < 2; player++) {
                for (let card of this.ctx.state.played[player]) {
                    for (let i = 0; i < card.effects.length; i++) {
                        const effect = card.effects[i];
                        if (!Processor.checkFilter(filter, effect)) {
                            continue;
                        }
                        card.effects.splice(i, 1);
                        i--;
                        await this.processOne(card, effect, noAnim);
                    }
                }
            }
            while (this.ctx.state.effectBacklog.length) {
                const { card, effect } = this.ctx.state.effectBacklog.shift();
                await this.processOne(card, effect, !this.processed[effect.type] || !this.processed[effect.type].some((f) => f.animate));
            }
        }
        async shuffleAndDraw(haveRemoteSeed) {
            const rules = this.ctx.defs.rules;
            if (this.ctx.state.turn === 1) {
                if (this.ctx.defs.mode) {
                    for (let summon of this.ctx.defs.mode.getAll(SpyCards.GameModeFieldType.SummonCard)) {
                        const card = this.ctx.defs.cardsByID[summon.card];
                        if (!card) {
                            continue;
                        }
                        this.ctx.state.setup[0].push({
                            card: card,
                            effects: [...card.effects],
                            originalDesc: true,
                        });
                        if (summon.flags & SpyCards.SummonCardFlags.BothPlayers) {
                            this.ctx.state.setup[1].push({
                                card: card,
                                effects: [...card.effects],
                                originalDesc: true,
                            });
                        }
                    }
                }
            }
            for (let player = 0; player < 2; player++) {
                this.ctx.state.tp[player] = Math.min((this.ctx.state.turn - 1) * this.ctx.defs.rules.tpPerTurn + this.ctx.defs.rules.minTP, this.ctx.defs.rules.maxTP);
                for (let setup of this.ctx.state.setup[player]) {
                    for (let e of setup.effects) {
                        if (e.type === SpyCards.EffectType.TP) {
                            this.ctx.state.tp[player] += e.negate ? -e.amount : e.amount;
                        }
                    }
                }
                this.ctx.state.tp[player] = Math.max(this.ctx.state.tp[player], 0);
                if (haveRemoteSeed || this.ctx.player - 1 === player) {
                    await this.ctx.scg.privateShuffle(this.ctx.state.deck[player], this.ctx.state.backs[player], 2 - this.ctx.player === player);
                }
                else {
                    await this.ctx.scg.publicShuffle(this.ctx.state.deck[player], this.ctx.state.backs[player]);
                }
                for (let i = 0; (i < rules.drawPerTurn || this.ctx.state.hand[player].length < rules.handMinSize) && this.ctx.state.hand[player].length < rules.handMaxSize && this.ctx.state.deck[player].length; i++) {
                    const card = this.ctx.state.deck[player].pop();
                    await this.callbacks.drawCard((player + 1), card);
                    this.ctx.state.hand[player].push(card);
                    this.ctx.state.backs[player].pop();
                }
            }
            if (!this.overrideSong && this.ctx.matchData.customModeName && this.ctx.matchData.customModeName.startsWith("bossmode.") && this.ctx.fx === SpyCards.Fx.fx) {
                for (let c of this.ctx.state.setup[2 - this.ctx.player]) {
                    if (c.card.tribes.some((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === "Boss")) {
                        switch (c.card.portrait) {
                            case 2:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle1.opus", 4.665, 71);
                                break;
                            case 21:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle2.opus", 1.8, 80);
                                break;
                            case 32:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle4.opus", 12.95, 83.68);
                                break;
                            case 43:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle5.opus", 1.7, 81.13);
                                break;
                            case 53:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle7.opus", 5.915, 67.4);
                                break;
                            case 72:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Final1.opus", 29.1, 169.54);
                                break;
                            case 126:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle8.opus", 5.52, 82.36);
                                break;
                            case 146:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle9.opus", 2.3, 101.9);
                                break;
                            case 174:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Final2.opus", 32.8, 183.6);
                                break;
                        }
                        if (this.overrideSong) {
                            await this.overrideSong.play();
                            break;
                        }
                    }
                }
            }
        }
        async processRound() {
            this.ctx.state.gameLog.startGroup("Round " + this.ctx.state.turn, true);
            this.ctx.state.gameLog.startGroup("Player 1 plays:");
            for (let c of this.ctx.state.played[0]) {
                this.ctx.state.gameLog.log(c.card.displayName() + (c.setup ? " (setup)" : ""));
            }
            this.ctx.state.gameLog.endGroup();
            this.ctx.state.gameLog.startGroup("Player 2 plays:");
            for (let c of this.ctx.state.played[1]) {
                this.ctx.state.gameLog.log(c.card.displayName() + (c.setup ? " (setup)" : ""));
            }
            this.ctx.state.gameLog.endGroup();
            this.ctx.state.rawATK[0] = this.ctx.state.rawATK[1] = 0;
            this.ctx.state.rawDEF[0] = this.ctx.state.rawDEF[1] = 0;
            this.ctx.state.ATK[0] = this.ctx.state.ATK[1] = 0;
            this.ctx.state.DEF[0] = this.ctx.state.DEF[1] = 0;
            this.ctx.state.healTotalN[0] = this.ctx.state.healTotalN[1] = 0;
            this.ctx.state.healTotalP[0] = this.ctx.state.healTotalP[1] = 0;
            this.ctx.state.healMultiplierN[0] = this.ctx.state.healMultiplierN[1] = 1;
            this.ctx.state.healMultiplierP[0] = this.ctx.state.healMultiplierP[1] = 1;
            this.ctx.state.winner = 0;
            this.ctx.state.effectBacklog.length = 0;
            this.ctx.state.once[0].length = 0;
            this.ctx.state.once[1].length = 0;
            this.processed = {};
            await this.callbacks.beforeRound();
            for (let filter of SpyCards.Effect.order.ignored) {
                await this.processEffect(filter, true);
            }
            for (let filter of SpyCards.Effect.order.pre) {
                await this.processEffect(filter);
            }
            await this.callbacks.showStats();
            for (let filter of SpyCards.Effect.order.main) {
                await this.processEffect(filter);
            }
            await this.callbacks.beforeComputeWinner();
            this.ctx.state.gameLog.log("Subtracting DEF from ATK...");
            this.ctx.state.DEF[0] = Math.max(this.ctx.state.DEF[0], 0);
            this.ctx.state.DEF[1] = Math.max(this.ctx.state.DEF[1], 0);
            await this.ctx.effect.modifyStat(null, null, null, 1, "ATK", -this.ctx.state.DEF[1]);
            await this.ctx.effect.modifyStat(null, null, null, 2, "ATK", -this.ctx.state.DEF[0]);
            this.ctx.state.ATK[0] = Math.max(this.ctx.state.ATK[0], 0);
            this.ctx.state.ATK[1] = Math.max(this.ctx.state.ATK[1], 0);
            if (this.ctx.state.ATK[0] < this.ctx.state.ATK[1]) {
                this.ctx.state.winner = 2;
            }
            else if (this.ctx.state.ATK[0] > this.ctx.state.ATK[1]) {
                this.ctx.state.winner = 1;
            }
            else {
                this.ctx.state.winner = 0;
            }
            if (this.ctx.state.winner) {
                this.ctx.state.gameLog.log("Player " + this.ctx.state.winner + " wins round!");
            }
            else {
                this.ctx.state.gameLog.log("Round was a tie!");
            }
            await this.callbacks.afterComputeWinner();
            if (this.ctx.state.winner === 1) {
                await this.callbacks.hpChange(2, this.ctx.state.hp[1], -1);
                this.ctx.state.hp[1]--;
            }
            else if (this.ctx.state.winner === 2) {
                await this.callbacks.hpChange(1, this.ctx.state.hp[0], -1);
                this.ctx.state.hp[0]--;
            }
            for (let filter of SpyCards.Effect.order.post) {
                await this.processEffect(filter);
            }
            for (let filter of SpyCards.Effect.order.discard) {
                await this.processEffect(filter, true);
            }
            const unhandledEffects = [];
            for (let player = 0; player < 2; player++) {
                for (let card of this.ctx.state.played[player]) {
                    if (card.effects.length) {
                        unhandledEffects.push(card);
                    }
                }
            }
            if (unhandledEffects.length) {
                debugger;
                throw new Error("unhandled effects: " + unhandledEffects.map((c) => {
                    return c.effects.map((e) => e.type);
                }));
            }
            await this.callbacks.afterRound();
            this.ctx.state.gameLog.endGroup();
        }
    }
    SpyCards.Processor = Processor;
})(SpyCards || (SpyCards = {}));
