//go:generate stringer -type ButtonStyle -trimprefix Style

package internal

import (
	"encoding/json"
	"fmt"
	"log"
	"runtime"
	"strconv"

	"golang.org/x/xerrors"
)

// OnSettingsChanged is a slice of callbacks for when a setting is saved.
var OnSettingsChanged []func(*Settings)

// Settings are the user-configurable options for Spy Cards Online.
type Settings struct {
	Audio AudioSettings `json:"audio"`

	Character string `json:"character,omitempty"`

	Disable3D  bool `json:"disable3D,omitempty"`
	ForceRelay bool `json:"forceRelay,omitempty"`
	DisableCRT bool `json:"disableCRT,omitempty"`

	AutoUploadRecording     bool `json:"autoUploadRecording,omitempty"`
	DisplayTermacadeButtons bool `json:"displayTermacadeButtons,omitempty"`

	LastTermacadeOption int    `json:"lastTermacadeOption"`
	LastTermacadeName   string `json:"lastTermacadeName,omitempty"`

	Controls ControlsSettings `json:"controls"`
}

// AudioSettings are the audio volume settings.
type AudioSettings struct {
	Music  float64 `json:"music"`
	Sounds float64 `json:"sounds"`
}

// ControlsSettings are the input settings.
type ControlsSettings struct {
	Keyboard int              `json:"keyboard,omitempty"`
	Gamepad  map[string]int   `json:"gamepad,omitempty"`
	CustomKB []KeyboardLayout `json:"customKB"`
	CustomGP []GamepadLayout  `json:"customGP"`
}

// KeyboardLayout is a custom keyboard layout.
type KeyboardLayout struct {
	Name string     `json:"name"`
	Code [10]string `json:"code"`
}

// GamepadLayout is a custom gamepad layout.
type GamepadLayout struct {
	Name   string            `json:"name"`
	Button [10]GamepadButton `json:"button"`
	Style  ButtonStyle       `json:"style,omitempty"`
}

// ButtonStyle is a graphical style for button prompts.
type ButtonStyle int

// Constants for ButtonStyle.
const (
	StyleKeyboard       ButtonStyle = 0
	StyleGenericGamepad ButtonStyle = 1
)

// GamepadButton is a button on a gamepad.
type GamepadButton struct {
	Button     int
	IsAxis     bool
	IsPositive bool
}

// String implements fmt.Stringer.
func (gpb GamepadButton) String() string {
	if gpb.IsAxis {
		sign := "-"
		if gpb.IsPositive {
			sign = "+"
		}

		return fmt.Sprintf("Axis %d (%s)", gpb.Button, sign)
	}

	return fmt.Sprintf("Button %d", gpb.Button)
}

// MarshalJSON implements json.Marshaler.
func (gpb GamepadButton) MarshalJSON() ([]byte, error) {
	if gpb.IsAxis {
		b := []byte{'['}
		b = strconv.AppendInt(b, int64(gpb.Button), 10)
		b = append(b, ',')
		b = strconv.AppendBool(b, gpb.IsPositive)
		b = append(b, ']')

		return b, nil
	}

	return strconv.AppendInt(nil, int64(gpb.Button), 10), nil
}

// UnmarshalJSON implements json.Unmarshaler.
func (gpb *GamepadButton) UnmarshalJSON(b []byte) error {
	arr := [2]interface{}{
		&gpb.Button,
		&gpb.IsPositive,
	}

	if err := json.Unmarshal(b, &arr); err == nil {
		gpb.IsAxis = true

		return nil
	}

	if err := json.Unmarshal(b, &gpb.Button); err != nil {
		return xerrors.Errorf("internal: unmarshaling gamepad button: %w", err)
	}

	gpb.IsAxis = false
	gpb.IsPositive = false

	return nil
}

const settingsKey = "spy-cards-settings-v0"

// LoadSettings retrieves saved settings from persistent storage.
func LoadSettings() *Settings {
	var s Settings

	b, err := LoadData(settingsKey)
	if err != nil {
		log.Printf("internal: loading settings: %+v", err)
	}

	if runtime.GOOS != "js" {
		s.Audio.Music = 0.6
		s.Audio.Sounds = 0.6
	}

	if b != nil {
		if err = json.Unmarshal(b, &s); err != nil {
			log.Printf("internal: decoding settings: %+v", err)
		}
	}

	return &s
}

// SaveSettings stores the settings in the user's persistent storage
// and invokes OnSettingsChanged callbacks.
func SaveSettings(s *Settings) {
	b, err := json.Marshal(s)
	if err != nil {
		panic(xerrors.Errorf("internal: encoding settings: %w", err))
	}

	err = StoreData(settingsKey, b)
	if err != nil {
		panic(xerrors.Errorf("internal: storing settings: %w", err))
	}

	for _, f := range OnSettingsChanged {
		f(s)
	}
}
