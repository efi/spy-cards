package game

import (
	"context"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
)

// CreateFunc allocates structures for game-specific logic.
type CreateFunc func(*State) Interface

// PlayRecording replays a recorded playthrough of a game.
func PlayRecording(ctx context.Context, create CreateFunc, rec *arcade.Recording) (*State, error) {
	state := newState(rec.Seed)

	if rec.Version[0] == 0 && rec.Version[1] == 2 && rec.Version[2] < 69 {
		state.RNG.ReverseFloat = true
	}

	state.Playback = rec
	state.PlaybackInput = input.GetContext(ctx)
	ctx, state.Input = input.NewContext(ctx, state.playbackInput)
	state.Interface = create(state)

	return state, state.runGame(ctx)
}

// RunGame presents a game to the player.
func RunGame(ctx context.Context, create CreateFunc, seed string) (*State, error) {
	state := newState(seed)

	if AIEnabled {
		ctx, state.Input = input.NewContext(ctx, state.aiInput)
	} else {
		state.Input = input.GetContext(ctx)
	}

	state.Interface = create(state)

	state.initRecording()

	return state, state.runGame(ctx)
}

func (s *State) runGame(ctx context.Context) error {
	if err := s.initAI(); err != nil {
		return xerrors.Errorf("game: in AI setup: %w", err)
	}

	if err := s.Interface.Init(ctx); err != nil {
		return xerrors.Errorf("game: in initialization: %w", err)
	}

	generationProgress := -1
	progress := make(chan int, 1)
	done := make(chan error, 1)

	go s.generate(ctx, progress, done)

	s.AISpamButton(arcade.BtnConfirm)

	if s.Playback == nil {
		initialWait := s.ComputeWait(1)
		fadeWait := uint64(0)

		for {
			s.TickCount++

			s.Input.Tick()

			if fadeWait != 0 {
				fadeWait--
				if fadeWait == 0 {
					break
				}
			} else {
				if s.Input.Consume(arcade.BtnCancel) {
					s.Exit = true

					break
				}

				if initialWait != 0 {
					initialWait--
				} else if generationProgress > 100 && s.Input.Consume(arcade.BtnConfirm) {
					s.Interface.StartPressed()
					s.AIForceButtons()
					s.FadeTo(0)
					fadeWait = s.ComputeWait(1)
				}
			}

			select {
			case generationProgress = <-progress:
			default:
			}

			s.CRT.Draw(func() {
				s.Interface.RenderAttract(generationProgress, initialWait != 0, fadeWait != 0)
			})

			gfx.NextFrame()

			if err := ctx.Err(); err != nil {
				return xerrors.Errorf("game: timed out: %w", err)
			}
		}
	}

	if err := <-done; err != nil {
		return xerrors.Errorf("game: in generation: %w", err)
	}

	s.TickCount = 0
	s.FadeTo(1)

	if err := s.Interface.StartGameplay(ctx); err != nil {
		return xerrors.Errorf("game: starting gameplay: %w", err)
	}

	s.AIClearButtons()

	internal.SetActive(true)

	if s.recording != nil {
		s.recording.Start = time.Now()
	}

	s.nextTick = time.Now()

	for !s.Exit {
		now := time.Now()

		s.outstandingFrames = 0

		for !now.Before(s.nextTick) {
			if !SuperSpeed {
				s.nextTick = s.nextTick.Add(FrameTime)
			}

			s.outstandingFrames++
			if s.outstandingFrames > maxOutstandingFrames {
				break
			}

			s.TickCount++

			s.Input.Tick()

			if s.recording != nil {
				s.recordTick()
			}

			if s.Playback != nil {
				s.playbackTick()
			}

			s.updateFade()

			if err := s.Interface.Logic(ctx); err != nil {
				return xerrors.Errorf("game: in logic for tick %d: %w", s.TickCount, err)
			}

			if AIEnabled {
				input, reward := s.Interface.AIInput()
				if input != nil {
					s.updateAI(input, reward)
				}
			}
		}

		s.CRT.Draw(s.Interface.Render)

		gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			return xerrors.Errorf("game: timed out in tick %d: %w", s.TickCount, err)
		}
	}

	internal.SetActive(false)

	if err := s.Interface.Cleanup(ctx); err != nil {
		return xerrors.Errorf("game: in cleanup: %w", err)
	}

	if s.Playback == nil && s.Score >= s.Interface.HighScore() {
		if err := s.highScore(ctx); err != nil {
			return xerrors.Errorf("game: displaying high score screen: %w", err)
		}
	}

	s.AIClearButtons()

	return ctx.Err()
}
