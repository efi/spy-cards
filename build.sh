#!/bin/bash -e

GO=go
if command -v go_wrapper &> /dev/null; then
	GO=go_wrapper
fi
TINYGO=$GO
if command -v tinygo &> /dev/null; then
	# Does not currently build due to runtime.Callers usage.
	: #TINYGO=tinygo
fi

rm -f assets.zip
zip -D -X -q -r assets.zip assets

echo Building WebASM
GOOS=js GOARCH=wasm $TINYGO build -o www/spy-cards.wasm
gzip -9kfn www/spy-cards.wasm &
brotli -f www/spy-cards.wasm &
echo Building Linux
GOOS=linux $GO build -o /dev/null -tags headless
GOOS=linux $GO build
strip spy-cards
echo Building Windows
GOOS=windows $GO build -ldflags "-H windowsgui"
strip spy-cards.exe
echo Building Android
cp -R ~/.golang-path/pkg/gomobile/lib/* android/app/libs/
GOOS=android GOARCH=386 GO386=sse2 $GO build -buildmode c-shared -o android/app/libs/x86/libspy-cards.so
GOOS=android GOARCH=amd64 $GO build -buildmode c-shared -o android/app/libs/x86_64/libspy-cards.so
GOOS=android GOARCH=arm GOARM=7 $GO build -buildmode c-shared -o android/app/libs/armeabi-v7a/libspy-cards.so
GOOS=android GOARCH=arm64 $GO build -buildmode c-shared -o android/app/libs/arm64-v8a/libspy-cards.so
(cd android && ./gradlew --quiet bundleRelease assembleDebug)
zipalign -f 4 android/app/build/outputs/bundle/release/app-release.aab spy-cards.aab
cp -f android/app/build/outputs/apk/debug/app-debug.apk spy-cards.apk
jarsigner -keystore ~/.spy-cards-online.keystore -storepass "$(cat ~/.spy-cards-online.password)" spy-cards.aab spy-cards-online-release > /dev/null
apksigner sign --ks ~/.spy-cards-online.keystore --ks-pass file:$HOME/.spy-cards-online.password spy-cards.apk

echo Building TypeScript
tsc --build tsconfig.json
cat www/script/sc-version.js www/script/sc-error-handler.js www/script/sc-webrtc-adapter.js www/script/sc-wasm_exec.js www/script/sc-wasm.js > www/script/spy-cards-native.js
cat www/script/sc-*.js > www/script/spy-cards.js

cd www

mv script/service-worker.js .
cp -R ../assets/* .

echo Building Docs
for md in docs/*.md; do
	pandoc --template=docs/_template.html --toc --toc-depth=2 --lua-filter=docs/filter.lua --from=markdown-smart "$md" -o "${md//.md}.html"
done

echo Compressing
wait

echo Building SRI Hashes
sed -e 's/ integrity="[^"]*"//' -i *.html docs/*.html
sha256sum script/*.js style/*.css | while read -r "hash" "file"; do
	b64hash="`xxd -r -p <<<"$hash" | base64`"
	sed -e 's#\("\|/\)'"$file"'?[0-9a-f]*"#\1'"$file"'?'"${hash:0:8}"'" integrity="sha256-'"$b64hash"'"#' -i *.html docs/*.html
done

rm -f cache-data.txt
find * -type f -exec sha256sum {} + > cache-data.txt
sed -e 's/%CACHE_HASH%/'"`sha256sum cache-data.txt | cut -d ' ' -f 1`"'/' -i service-worker.js

echo Build Complete
