// +build js,wasm
// +build !headless

package visual

import (
	"syscall/js"
	"time"
)

var debounceEditor *time.Timer

func (e *CardEditor) dirty() {
	if debounceEditor != nil {
		debounceEditor.Reset(time.Second)

		return
	}

	debounceEditor = time.AfterFunc(time.Second, func() {
		js.Global().Get("location").Set("hash", "#"+e.EncodeCards())

		debounceEditor = nil
	})
}
