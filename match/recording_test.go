package match_test

import (
	"context"
	"io/ioutil"
	"path/filepath"
	"runtime"
	"testing"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
)

var cases = [...]string{
	"YWFGRYDSJB2MHX03",
	"KHGNWYS1E5FEBGG3",
	"A5FKTQ6T0FKC9483",
	"E9FW07QP1JDJN403",
	"PVWZQ4DE0XNYKQG2",
	"QANDC3NR902NXB02",
	"CVGK0Z7M7K7J5YG1",
	"2GNM7RMG9R24KJ01",
	"MDXY1QMM4MB5Z5G1",
	"KWKX9JSW1X732S0",
}

func TestRecording(t *testing.T) {
	t.Parallel()

	for _, c := range cases {
		name := filepath.Join("testdata", c)

		t.Run(c, func(t *testing.T) {
			t.Parallel()

			/*#nosec*/
			b, err := ioutil.ReadFile(name)
			if err != nil {
				t.Fatal(err)
			}

			var rec card.Recording
			if err := rec.UnmarshalBinary(b); err != nil {
				t.Fatal(err)
			}

			m := &match.Match{
				Init: &match.Init{
					Version: rec.Version,
					Mode:    rec.ModeName,
					Custom:  rec.CustomCardsRaw,
				},
				Perspective: rec.Perspective,
				Set:         &rec.CustomCards,
				Start:       rec.Start,
				Cosmetic:    rec.Cosmetic,
			}

			t.Logf("Version: %d.%d.%d %s", m.Init.Version[0], m.Init.Version[1], m.Init.Version[2], m.Init.Mode)

			r, err := match.NewRecording(context.Background(), &rec)
			if err != nil {
				t.Fatal(err)
			}

			max := uint64(r.NumRounds() - 1)
			for !r.RoundProcessed(max) {
				runtime.Gosched()
			}

			m.IndexSet()

			for i := uint64(0); i < max; i++ {
				state, turn := r.Round(i)

				m.State = *state
				m.State.TurnData = turn

				for player := range state.Sides {
					tp := state.Sides[player].TP

					if turn.Ready[player]&^((1<<len(state.Sides[player].Hand))-1) != 0 {
						t.Errorf("P%d played cards %b from %d card hand on round %d", player+1, turn.Ready[player], len(state.Sides[player].Hand), state.Round)
					}

					for i, c := range state.Sides[player].Hand {
						if turn.Ready[player]&(1<<i) == 0 {
							continue
						}

						tp.Add(match.Number{Amount: -c.Def.TP})
					}

					if tp.Less(match.Number{}) {
						t.Errorf("P%d has %v remaining TP on round %d", player+1, tp, state.Round)
					}
				}

				if w := m.Winner(); w != 0 {
					t.Errorf("P%d wins on round %d of %d", w, i, max)
				}
			}

			state, _ := r.Round(max)
			m.State = *state

			if w := m.Winner(); w == 0 {
				t.Errorf("No match winner: P1 Health %v, P2 Health %v, Round Winner %d", m.State.Sides[0].HP, m.State.Sides[1].HP, m.State.RoundWinner)
			}
		})
	}
}
