package game

import (
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/rng"
)

// SavedState is a snapshot of State.
type SavedState struct {
	tickCount uint64
	score     float64
	rng       *rng.RNG

	fadeCurrent float64
	fadeAmount  float64
	fadeTarget  float64

	game  interface{}
	input input.State
}

// Save records a snapshot of State.
func (s *State) Save() *SavedState {
	return &SavedState{
		tickCount: s.TickCount,
		score:     s.Score,
		rng:       s.RNG.Clone(),

		fadeCurrent: s.fadeCurrent,
		fadeAmount:  s.fadeAmount,
		fadeTarget:  s.fadeTarget,

		game:  s.Interface.SaveState(),
		input: s.Input.SaveState(),
	}
}

// Load replaces State with a recorded snapshot.
func (s *State) Load(saved *SavedState) {
	s.TickCount = saved.tickCount
	s.Score = saved.score
	s.RNG = saved.rng.Clone()

	s.fadeCurrent = saved.fadeCurrent
	s.fadeAmount = saved.fadeAmount
	s.fadeTarget = saved.fadeTarget

	s.Interface.LoadState(saved.game)
	s.Input.LoadState(saved.input)
}
