package internal

import (
	"fmt"
	"net/http"
	"runtime"
)

var userAgent = fmt.Sprintf("SpyCardsOnline/%d.%d.%d (%s; %s) Go/%s", Version[0], Version[1], Version[2], runtime.GOOS, runtime.GOARCH, runtime.Version())

// ModifyRequest modifies an HTTP request before it is sent.
func ModifyRequest(r *http.Request) *http.Request {
	if runtime.GOOS == "js" {
		r.Header["js.fetch:mode"] = []string{"cors"}
		r.Header["js.fetch:credentials"] = []string{"omit"}
	} else {
		r.Header.Set("User-Agent", userAgent)
	}

	return r
}
