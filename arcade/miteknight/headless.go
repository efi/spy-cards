// +build headless

package miteknight

import "context"

func clearHUD() {
}

func (mk *MiteKnight) Cleanup(ctx context.Context) error {
	return nil
}

func (mk *MiteKnight) Render() {
}

func (mk *MiteKnight) RenderAttract(int, bool, bool) {
}

func (d *dungeonFloor) appendTileGeometry(staticData []uint8, staticElements []uint16, x, y, baseX, baseY int) ([]uint8, []uint16) {
	return staticData, staticElements
}
