// +build !js !wasm
// +build !headless

package internal

import (
	"runtime"

	"golang.org/x/xerrors"
)

func ReadClipboard() (string, error) {
	return "", xerrors.Errorf("internal: not implemented: read clipboard on %s", runtime.GOOS)
}

func WriteClipboard(s string) error {
	return xerrors.Errorf("internal: not implemented: write clipboard on %s", runtime.GOOS)
}
