// +build !js !wasm
// +build !headless

package room

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/mobile/gl"
)

func (s *customStage) Render(cam *gfx.Camera) {
	gfx.Lock.Lock()

	gfx.GL.UseProgram(roomProgram.Program)

	if !s.noDepth {
		gfx.GL.Enable(gl.DEPTH_TEST)
	}

	gfx.GL.UniformMatrix4fv(roomPerspective.Uniform, cam.Perspective[:])
	combined := cam.Combined()
	gfx.GL.UniformMatrix4fv(roomCamera.Uniform, combined[:])

	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, s.tex.LazyTexture(sprites.Blank.Texture()))
	gfx.GL.BindBuffer(gl.ARRAY_BUFFER, s.buf.Data)

	gfx.GL.EnableVertexAttribArray(posAttrib.Attrib)
	gfx.GL.EnableVertexAttribArray(texAttrib.Attrib)
	gfx.GL.VertexAttribPointer(posAttrib.Attrib, 3, gl.FLOAT, false, 20, 0)
	gfx.GL.VertexAttribPointer(texAttrib.Attrib, 2, gl.FLOAT, false, 20, 12)

	gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, s.buf.Element)
	gfx.GL.DrawElements(gl.TRIANGLES, s.buf.Count, gl.UNSIGNED_SHORT, 0)

	gfx.GL.DisableVertexAttribArray(posAttrib.Attrib)
	gfx.GL.DisableVertexAttribArray(texAttrib.Attrib)

	if !s.noDepth {
		gfx.GL.Disable(gl.DEPTH_TEST)
	}

	gfx.Lock.Unlock()
}
