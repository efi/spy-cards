//go:generate stringer -type AudienceType -trimprefix Audience
//go:generate stringer -type AudienceAnimationState -trimprefix Audience

package room

import (
	"crypto/sha256"
	"image/color"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/rng"
)

// AudienceType is an audience member sprite.
type AudienceType uint8

// Audience member types.
const (
	AudienceAnt AudienceType = iota
	AudienceBee
	AudienceBeetle
	AudienceMoth
	AudienceTermite1
	AudienceTermite2
	numAudienceTypes
)

// AudienceAnimationState designates which sprite to use.
type AudienceAnimationState uint8

// Audience animation states.
const (
	AudienceFront AudienceAnimationState = iota
	AudienceBack
	AudienceFrontCheer
	AudienceBackCheer
	AudienceBackBlur1
	AudienceBackCheerBlur1
	AudienceBackBlur2
	AudienceBackCheerBlur2
	AudienceBackBlur3
	AudienceBackCheerBlur3
)

// Audience is a member of the Spy Cards audience.
type Audience struct {
	Type       AudienceType
	Back       bool
	Flip       bool
	X, Y, Z    float32
	Scale      float32
	Hue        float32
	Color      color.RGBA
	Excitement float32
	Blur       float32
	WantCheer  uint64
	Cheering   uint64
	Hop        uint64
	hopTime    uint64
}

// NewAudience creates an audience.
func NewAudience(seed []uint8, rematches uint64) []*Audience {
	var audience []*Audience

	rand := rng.New(sha256.New(), string(seed))
	randN := func(max float64) int {
		return int(math.Floor(rand.Float() * max))
	}

	audienceNearCount := randN(10+float64(rematches)/3) + randN(1.0001+float64(rematches)*0.0001)*50
	audienceFarCount := randN(8+float64(rematches)/2) + 2 + randN(1.00005+float64(rematches)*0.0001)*50

	for i := 0; i < audienceNearCount; i++ {
		x, z := (float64(i)+rand.Float()*0.75+0.125)*8/float64(audienceNearCount)+1, rand.Float()
		if x < 4 || x > 6 {
			z = -z*1.5 - 1.0
		} else {
			z = -z*2.0 - 2.5
		}

		ty := AudienceType(randN(float64(numAudienceTypes)))
		if x > 3.5 && x < 6.5 && ty == AudienceTermite2 {
			// avoid the tallest character standing in the middle
			ty = AudienceType(x*1000) % numAudienceTypes
		}

		flip := randN(2) == 1
		hue := rand.Float()
		excitement := math.Pow(rand.Float(), 1.5)

		audience = append(audience, &Audience{
			Type:       ty,
			Back:       true,
			Flip:       flip,
			X:          float32(x),
			Z:          float32(z),
			Scale:      1,
			Hue:        float32(hue),
			Color:      hueToColor(hue),
			Excitement: float32(excitement),
			Hop:        uint64(i),
		})
	}

	for i := 0; i < audienceFarCount; i++ {
		x := (float64(i)+rand.Float()*0.8+0.1)*8.75/float64(audienceFarCount) + 0.625
		z := rand.Float()*2.2 + 2.5

		ty := AudienceType(randN(float64(numAudienceTypes)))
		flip := randN(2) == 1
		hue := rand.Float()
		excitement := math.Pow(rand.Float(), 2)

		audience = append(audience, &Audience{
			Type:       ty,
			Back:       false,
			Flip:       flip,
			X:          float32(x),
			Z:          float32(z),
			Scale:      1,
			Hue:        float32(hue),
			Color:      hueToColor(hue),
			Excitement: float32(excitement),
			Hop:        uint64(i),
		})
	}

	return audience
}

// StartCheer makes the audience member play their cheering animation.
//
// Negative durations make the audience member cheer forever.
func (a *Audience) StartCheer(d time.Duration) {
	fullHopTime := uint64(36-30/(2-a.Excitement)) * 2

	var cheer uint64

	if d < 0 {
		cheer = ^uint64(0)
	} else {
		cheer = uint64(d*60/time.Duration(fullHopTime)/time.Second) + 1
	}

	if a.Cheering != 0 {
		a.Cheering = cheer
	} else {
		a.WantCheer = cheer
	}
}

// Sprite returns the audience member's current sprite.
func (a *Audience) Sprite() *sprites.Sprite {
	state := AudienceFront

	cheering := a.Cheering > 0

	switch {
	case cheering && a.Back:
		switch {
		default:
			state = AudienceBackCheerBlur3
		case a.Blur < 0.1:
			state = AudienceBackCheerBlur2
		case a.Blur < 0.05:
			state = AudienceBackCheerBlur1
		case a.Blur < 0.01:
			state = AudienceBackCheer
		}
	case a.Back:
		switch {
		default:
			state = AudienceBackBlur3
		case a.Blur < 0.1:
			state = AudienceBackBlur2
		case a.Blur < 0.05:
			state = AudienceBackBlur1
		case a.Blur < 0.01:
			state = AudienceBack
		}
	case cheering:
		state = AudienceFrontCheer
	}

	return sprites.Audience[a.Type][state]
}

func hueToColor(hue float64) color.RGBA {
	hue *= math.Pi * 2

	r := math.Cos(hue)
	g := math.Cos(hue + 2.0/3.0*math.Pi)
	b := math.Cos(hue + 4.0/3.0*math.Pi)

	c := func(x float64) uint8 {
		return uint8(math.Min(math.Max((x+1.75)*128, 0), 255))
	}

	return color.RGBA{c(r), c(g), c(b), 255}
}

// Update is called once per 60th of a second to update the state of the audience member.
func (a *Audience) Update() {
	a.hopTime = uint64(36 - 30/(2-a.Excitement))

	a.Hop++

	if a.Hop >= a.hopTime*2 {
		a.Hop = 0

		if a.Cheering == 0 {
			a.Cheering = a.WantCheer
			a.WantCheer = 0
		} else if a.Cheering != ^uint64(0) {
			a.Cheering--
		}
	}
}

// Render draws the audience member to the screen.
func (a *Audience) Render(batch *sprites.Batch) {
	sx := a.Scale
	if a.Flip {
		sx = -sx
	}

	e2 := float32(1)
	if a.Cheering != 0 {
		e2 = 0.25
	}

	dy := (a.Excitement/e2*0.5 + ((1 - e2) * 0.4)) * (1 - float32(math.Pow(1-float64(a.Hop)/float64(a.hopTime), 2)))

	batch.Append(a.Sprite(), a.X, a.Y+dy, a.Z, sx, a.Scale, a.Color, sprites.FlagNoDiscard, 0, 0, 0)
}
