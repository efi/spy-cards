const spyCardsVersionPrefix = "0.2.80";

const spyCardsClientMode = location.pathname === "/play.html" ? new URLSearchParams(location.search).get("mode") : "";

const spyCardsVersionSuffix = (function () {
	const path = location.pathname.substr(1);
	if (spyCardsClientMode) {
		return "-" + spyCardsClientMode;
	}
	if (path.substr(path.length - ".html".length) === ".html") {
		return "-" + path.substr(0, path.length - 5);
	}
	return "";
})();

const spyCardsVersion = spyCardsVersionPrefix + spyCardsVersionSuffix;
var spyCardsVersionVariableSuffix: string = "";
