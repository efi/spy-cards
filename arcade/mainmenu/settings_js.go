// +build js,wasm

package mainmenu

import "syscall/js"

func settingsRedirect() {
	js.Global().Get("location").Set("href", "settings.html")
}
