// +build js,wasm
// +build !headless

package gfx

import (
	"syscall/js"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
)

func newDataTexture(name string, b []byte, mime string, nearest bool) (*Texture, error) {
	jsBuf := js.Global().Get("Uint8Array").New(len(b))
	js.CopyBytesToJS(jsBuf, b)

	blob := js.Global().Get("Blob").New([]interface{}{
		jsBuf,
	}, map[string]interface{}{
		"type": mime,
	})

	var (
		jsImg js.Value
		err   error
	)

	if js.Global().Get("createImageBitmap").Truthy() {
		imgPromise := js.Global().Call("createImageBitmap", blob, map[string]interface{}{
			"premultipliedAlpha":   "none",
			"colorSpaceConversion": "none",
		})

		jsImg, err = internal.Await(imgPromise)
	}

	if !js.Global().Get("createImageBitmap").Truthy() || (err != nil && js.Global().Get("createImageBitmap").Length() == 1) {
		// firefox compat
		jsURL := js.Global().Get("URL").Call("createObjectURL", blob)
		defer js.Global().Get("URL").Call("revokeObjectURL", jsURL)

		jsImg = js.Global().Get("Image").New()
		jsImg.Set("src", jsURL)

		if jsImg.Get("decode").Truthy() {
			_, err = internal.Await(jsImg.Call("decode"))
		} else {
			err = nil
		}
	} else {
		if !jsImg.Truthy() {
			return nil, err
		}
		defer jsImg.Call("close")
	}

	Lock.Lock()

	tex := GL.CreateTexture()
	GL.BindTexture(gl.TEXTURE_2D, tex)

	ctx := GL.(js.Wrapper).JSValue()

	ctx.Call("pixelStorei", ctx.Get("UNPACK_PREMULTIPLY_ALPHA_WEBGL"), true)

	if ctx.Get("texStorage2D").Truthy() {
		ctx.Call("texStorage2D", gl.TEXTURE_2D, 1, gl.RGBA8, jsImg.Get("width"), jsImg.Get("height"))
	} else {
		ctx.Call("texImage2D", gl.TEXTURE_2D, 0, gl.RGBA, jsImg.Get("width"), jsImg.Get("height"), 0, gl.RGBA, gl.UNSIGNED_BYTE, js.Null())
	}

	ctx.Call("texSubImage2D", gl.TEXTURE_2D, 0, 0, 0, gl.RGBA, gl.UNSIGNED_BYTE, jsImg)

	if nearest {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	} else {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST)
	}

	GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	GL.GenerateMipmap(gl.TEXTURE_2D)

	Lock.Unlock()

	return &Texture{
		Name:    name,
		Texture: tex,
		nearest: nearest,
	}, err
}
