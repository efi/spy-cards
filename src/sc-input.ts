module SpyCards {
	export enum Button {
		Up,
		Down,
		Left,
		Right,
		Confirm,
		Cancel,
		Switch,
		Toggle,
		Pause,
		Help,
	}

	export const defaultKeyButton: { [button: number]: string } = {
		[Button.Up]: "ArrowUp",
		[Button.Down]: "ArrowDown",
		[Button.Left]: "ArrowLeft",
		[Button.Right]: "ArrowRight",
		[Button.Confirm]: "KeyC",
		[Button.Cancel]: "KeyX",
		[Button.Switch]: "KeyZ",
		[Button.Toggle]: "KeyV",
		[Button.Pause]: "Escape",
		[Button.Help]: "Enter",
	};

	// see https://w3c.github.io/gamepad/standard_gamepad.svg
	export const standardGamepadButton: { [button: number]: number | [number, boolean] } = {
		[Button.Up]: 12,
		[Button.Down]: 13,
		[Button.Left]: 14,
		[Button.Right]: 15,
		[Button.Confirm]: 0,
		[Button.Cancel]: 1,
		[Button.Switch]: 2,
		[Button.Toggle]: 3,
		[Button.Pause]: 9,
		[Button.Help]: 8,
	};

	const buttonPressed: { [btn: number]: boolean } = {};
	const buttonWasPressed: { [btn: number]: number } = {};
	export const buttonHeld: { [btn: number]: boolean } = {};
	const keyHeld: { [code: string]: boolean } = {};
	let lastKeyboard = 0;
	let lastGamepad = 0;
	let inputTicks = 0;

	export enum ButtonStyle {
		Keyboard,
		GenericGamepad,
	}
	let lastGamepadStyle = ButtonStyle.GenericGamepad;

	export function getButtonStyle(): ButtonStyle {
		if (!lastGamepad || lastGamepad < lastKeyboard) {
			return ButtonStyle.Keyboard;
		}
		return lastGamepadStyle;
	}

	export let updateAIButtons: (buttons: { [btn: number]: boolean }) => void;

	export type InputState = {
		t: number;
		p: { [btn: number]: boolean };
		w: { [btn: number]: number };
		h: { [btn: number]: boolean };
	}
	export function saveInputState(): InputState {
		const p: { [btn: number]: boolean } = {};
		const w: { [btn: number]: number } = {};
		const h: { [btn: number]: boolean } = {};
		for (let btn = Button.Up; btn <= Button.Help; btn++) {
			p[btn] = buttonPressed[btn];
			w[btn] = buttonWasPressed[btn];
			h[btn] = buttonHeld[btn];
		}
		return { t: inputTicks, p, w, h };
	}
	export function loadInputState(s: InputState) {
		inputTicks = s.t;
		for (let btn = Button.Up; btn <= Button.Help; btn++) {
			buttonPressed[btn] = s.p[btn];
			buttonWasPressed[btn] = s.w[btn];
			buttonHeld[btn] = s.h[btn];
		}
	}

	export function updateButtons(force?: (buttons: { [btn: number]: boolean }) => void) {
		if (force) {
			force(buttonHeld);
		} else if (updateAIButtons) {
			updateAIButtons(buttonHeld);
		} else {
			updateHeldButtons();
		}

		inputTicks++;
		for (let button = Button.Up; button <= Button.Help; button++) {
			if (!buttonHeld[button]) {
				buttonPressed[button] = false;
				buttonWasPressed[button] = 0;
			} else if (buttonWasPressed[button] < inputTicks) {
				buttonWasPressed[button] = Infinity;
				buttonPressed[button] = true;
			}
		}
	}

	function updateHeldButtons() {
		const controlsSettings = loadSettings().controls || {};
		const gamepadSettings = controlsSettings.gamepad || {};
		let keyboardMap = defaultKeyButton;
		if (controlsSettings.customKB && controlsSettings.keyboard > 0 && controlsSettings.customKB[controlsSettings.keyboard - 1]) {
			keyboardMap = controlsSettings.customKB[controlsSettings.keyboard - 1].code;
		}

		for (let button = Button.Up; button <= Button.Help; button++) {
			buttonHeld[button] = false;

			if (keyHeld[keyboardMap[button]]) {
				buttonHeld[button] = true;
				lastKeyboard = Date.now();
			}

			for (let gp of navigator.getGamepads()) {
				if (!gp) {
					continue;
				}

				let mapping = standardGamepadButton;
				let style = ButtonStyle.GenericGamepad;
				if (controlsSettings.customGP && gamepadSettings[gp.id] > 0 && controlsSettings.customGP[gamepadSettings[gp.id] - 1]) {
					mapping = controlsSettings.customGP[gamepadSettings[gp.id] - 1].button;
					style = controlsSettings.customGP[gamepadSettings[gp.id] - 1].style;
				} else if (gp.mapping !== "standard") {
					continue;
				}

				const mapped = mapping[button];
				let pressed: boolean;
				if (Array.isArray(mapped)) {
					const axis = gp.axes[mapped[0]];
					if (mapped[1]) {
						pressed = axis > 0.5;
					} else {
						pressed = axis < -0.5;
					}
				} else {
					const gpButton = gp.buttons[mapped];
					pressed = gpButton && gpButton.pressed;
				}
				if (pressed) {
					buttonHeld[button] = true;
					lastGamepad = Date.now();
					lastGamepadStyle = style;
				}
			}
		}
	}

	export function consumeButton(btn: Button): boolean {
		if (buttonPressed[btn]) {
			buttonPressed[btn] = false;
			return true;
		}

		return false;
	}
	export function consumeButtonAllowRepeat(btn: Button, delay: number = 0.1): boolean {
		if (buttonPressed[btn]) {
			buttonPressed[btn] = false;
			buttonWasPressed[btn] = inputTicks + delay * 60;
			return true;
		}

		return false;
	}

	function isAllowedKeyCode(code: string): boolean {
		// don't grab OS/Browser special keys
		if (code.startsWith("Alt") ||
			code.startsWith("Control") ||
			code.startsWith("Shift") ||
			code.startsWith("Meta") ||
			code.startsWith("OS") ||
			code.startsWith("Browser") ||
			code.startsWith("Launch") ||
			code.startsWith("Audio") ||
			code.startsWith("Media") ||
			code.startsWith("Volume") ||
			code.startsWith("Lang") ||
			code.startsWith("Page") ||
			code.endsWith("Mode") ||
			code.endsWith("Lock") ||
			/^F[0-9]+$/.test(code) ||
			code === "" ||
			code === "Unidentified" ||
			code === "PrintScreen" ||
			code === "Power" ||
			code === "Pause" ||
			code === "ContextMenu" ||
			code === "Help" ||
			code === "Fn" ||
			code === "Tab" ||
			code === "Home" ||
			code === "End") {
			return false;
		}

		// allow letters, numbers, and some symbols.
		if (/^Key[A-Z]$|^Digit[0-9]$|^Numpad|^Intl|Up$|Down$|Left$|Right$/.test(code)) {
			return true;
		}

		// additional symbols and special keys listed by name:
		switch (code) {
			case "Escape":
			case "Minus":
			case "Equal":
			case "Backspace":
			case "Enter":
			case "Semicolon":
			case "Quote":
			case "Backquote":
			case "Backslash":
			case "Comma":
			case "Period":
			case "Slash":
			case "Space":
			case "Insert":
			case "Delete":
				return true;
			default:
				console.warn("Unhandled keycode " + code + "; ignoring for safety.");
				debugger;
				return false;
		}
	}

	const awaitingKeyPress: ((code: string) => void)[] = [];
	addEventListener("keydown", (e) => {
		if (SpyCards.disableKeyboard) {
			return;
		}

		if (e.ctrlKey || e.altKey || e.metaKey) {
			return;
		}

		if ("value" in e.target) {
			return;
		}

		if (isAllowedKeyCode(e.code)) {
			e.preventDefault();
			keyHeld[e.code] = true;
			for (let resolve of awaitingKeyPress) {
				resolve(e.code);
			}
			awaitingKeyPress.length = 0;
		}
	});

	addEventListener("keyup", (e) => {
		if (SpyCards.disableKeyboard) {
			return;
		}

		if (e.ctrlKey || e.altKey || e.metaKey) {
			return;
		}

		if ("value" in e.target) {
			return;
		}

		if (isAllowedKeyCode(e.code)) {
			e.preventDefault();
			keyHeld[e.code] = false;
		}
	});

	export function nextPressedKey(): Promise<string> {
		return new Promise<string>((resolve) => awaitingKeyPress.push(resolve));
	}

	export function nextPressedButton(): Promise<number | [number, boolean]> {
		const alreadyHeld: string[] = [];
		for (let gp of navigator.getGamepads()) {
			for (let i = 0; i < gp.buttons.length; i++) {
				if (gp.buttons[i].pressed) {
					alreadyHeld.push(gp.id + ":" + i);
				}
			}
			for (let i = 0; i < gp.axes.length; i++) {
				if (gp.axes[i] > 0.5) {
					alreadyHeld.push(gp.id + ":+" + i);
				}
				if (gp.axes[i] < -0.5) {
					alreadyHeld.push(gp.id + ":-" + i);
				}
			}
		}

		return new Promise<number | [number, boolean]>((resolve) => {
			requestAnimationFrame(function check() {
				for (let gp of navigator.getGamepads()) {
					for (let i = 0; i < gp.buttons.length; i++) {
						const already = alreadyHeld.indexOf(gp.id + ":" + i);
						if (already === -1 && gp.buttons[i].pressed) {
							return resolve(i);
						}
						if (already !== -1 && !gp.buttons[i].pressed) {
							alreadyHeld.splice(already, 1);
						}
					}
					for (let i = 0; i < gp.axes.length; i++) {
						const alreadyP = alreadyHeld.indexOf(gp.id + ":+" + i);
						const alreadyN = alreadyHeld.indexOf(gp.id + ":-" + i);
						if (alreadyP === -1 && gp.axes[i] > 0.5) {
							return resolve([i, true]);
						}
						if (alreadyN === -1 && gp.axes[i] < -0.5) {
							return resolve([i, false]);
						}
						if (alreadyP !== -1 && gp.axes[i] <= 0.5) {
							alreadyHeld.splice(alreadyP, 1);
						}
						if (alreadyN !== -1 && gp.axes[i] >= -0.5) {
							alreadyHeld.splice(alreadyN, 1);
						}
					}
				}

				requestAnimationFrame(check);
			});
		});
	}
}
