package gui

import (
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"golang.org/x/mobile/event/key"
)

type Viewport struct {
	Container
	Rect
	Batch *sprites.Batch
	Ictx  *input.Context
	Cam   *gfx.Camera
	mouse struct {
		x, y float32
		c    bool
	}
	focus struct {
		ok      bool
		control Handler
	}
}

func NewViewport(ictx *input.Context, batch *sprites.Batch, cam *gfx.Camera) *Viewport {
	v := new(Viewport)
	v.root = v
	v.Ictx = ictx
	v.Batch = batch
	v.Cam = cam
	v.focus.ok = false
	v.update = func() {
		x, y, c := v.Ictx.Mouse()
		v.mouse.x = x
		v.mouse.y = y
		v.mouse.c = c
		if v.focus.ok {
			switch t := v.focus.control.(type) {
			case *Input:
				e, ok := v.Ictx.ConsumeKey()
				if ok && e.Direction != key.DirRelease && e.Rune != -1 {
					t.Put(e.Rune)
				}
			case *Button:
				t.onClick()
			}
		}
	}
	v.node = v
	return v
}

func (v *Viewport) Layout(c Node) {
	if c == nil {
		v.Layout(v)
	} else {
		r := Rect{}
		for _, i := range c.GetChildren() {
			if i.IsVisible() { // May need to remove this here if hiding stops working
				r = i.Pack(r)
				v.Layout(i)
			}
		}
	}
}

func (v *Viewport) Resize(w, h float32) {
	v.w = w
	v.h = h
	v.Layout(nil)
}

func (v *Viewport) Render() {
	v.Batch.Reset(v.Cam)
	for _, ch := range v.Control.Children {
		if r, ok := ch.(Renderable); ok {
			r.Render()
		}
	}
}
