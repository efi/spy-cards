// +build !headless

package gfx

import (
	"reflect"
	"runtime"
	"unsafe"

	"golang.org/x/mobile/gl"
)

var buffers []*StaticBuffer

// StaticBuffer is a convenience layer for OpenGL vertex buffer objects.
type StaticBuffer struct {
	Name     string
	data     []uint8
	elements []uint8
	Data     gl.Buffer
	Element  gl.Buffer
	Count    int
}

func (sb *StaticBuffer) init() {
	sb.Data = GL.CreateBuffer()
	sb.Element = GL.CreateBuffer()

	GL.BindBuffer(gl.ARRAY_BUFFER, sb.Data)
	GL.BufferData(gl.ARRAY_BUFFER, sb.data, gl.STATIC_DRAW)

	GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, sb.Element)
	GL.BufferData(gl.ELEMENT_ARRAY_BUFFER, sb.elements, gl.STATIC_DRAW)
}

func (sb *StaticBuffer) release() {
	GL.DeleteBuffer(sb.Data)
	GL.DeleteBuffer(sb.Element)

	sb.Data = gl.Buffer{}
	sb.Element = gl.Buffer{}
}

// Delete frees the GPU memory held by StaticBuffer and removes it from the list of buffers.
func (sb *StaticBuffer) Delete() {
	Lock.Lock()
	defer Lock.Unlock()

	for i := range buffers {
		if buffers[i] == sb {
			buffers = append(buffers[:i], buffers[i+1:]...)

			if GL != nil {
				sb.release()
			}

			return
		}
	}
}

// NewStaticBuffer creates a StaticBuffer.
func NewStaticBuffer(name string, data []uint8, elements []uint16) *StaticBuffer {
	sb := &StaticBuffer{
		Name:     name,
		data:     make([]uint8, len(data)),
		elements: make([]uint8, len(elements)*2),
		Count:    len(elements),
	}

	copy(sb.data, data)

	/* #nosec */
	{
		// do a faster in-place copy of the data
		unsafeElements := reflect.SliceHeader{
			Data: uintptr(unsafe.Pointer(&elements[0])),
			Len:  len(elements) * 2,
			Cap:  cap(elements) * 2,
		}

		elements1 := *(*[]uint8)(unsafe.Pointer(&unsafeElements))

		copy(sb.elements, elements1)

		runtime.KeepAlive(elements)
	}

	Lock.Lock()

	buffers = append(buffers, sb)

	if GL != nil {
		sb.init()
	}

	Lock.Unlock()

	return sb
}

const m1 = ^uint8(0)

// Square is a StaticBuffer containing vertices for a square.
var Square = NewStaticBuffer("square", []uint8{
	m1, +1, m1, 1, 0, 0,
	m1, m1, m1, 1, 0, 1,
	+1, m1, +1, 1, 1, 1,
	+1, +1, +1, 1, 1, 0,
}, []uint16{3, 2, 1, 0, 3, 1})
