async function initCardEditor(form: HTMLElement, initialData?: string) {
	const vanillaDefs = new SpyCards.CardDefs();
	let customDefs = vanillaDefs;

	let debounceCode: number = null;

	SpyCards.UI.clear(form);

	form.classList.add("card-editor-top");

	const h1 = document.createElement("h1");
	h1.textContent = "Card Editor";
	form.appendChild(h1);

	const version = document.createElement("span");
	version.classList.add("version-number");
	version.textContent = "Version ";
	const versionLink = document.createElement("a");
	versionLink.href = "docs/changelog.html#v" + spyCardsVersionPrefix;
	versionLink.textContent = spyCardsVersionPrefix;
	version.appendChild(versionLink);
	form.appendChild(version);

	const backLink = document.createElement("a");
	backLink.classList.add("back-link");
	backLink.href = "custom.html";
	backLink.textContent = "← Close Editor";
	form.appendChild(backLink);

	const code = document.createElement("input");
	code.addEventListener("focus", () => code.select());
	code.type = "text";
	code.value = initialData || "";
	form.appendChild(code);

	const npcNames = document.createElement("datalist");
	npcNames.id = SpyCards.UI.uniqueID();
	for (let name of ["generic"]) {
		npcNames.appendChild(SpyCards.UI.option("", name));
	}
	form.appendChild(npcNames);

	const gameModeEditor = document.createElement("div");
	gameModeEditor.classList.add("readme", "card-editor-game-mode");
	document.body.appendChild(gameModeEditor);

	const gameModeAddNew = document.createElement("div");
	gameModeAddNew.classList.add("game-mode-add-field");

	const editors = document.createElement("div");
	editors.classList.add("card-editors");
	document.body.appendChild(editors);

	const addNew = document.createElement("div");
	addNew.classList.add("readme", "card-editor-new-card");
	const addNewHeading = document.createElement("h2");
	addNewHeading.textContent = "Add New";
	addNew.appendChild(addNewHeading);
	document.body.appendChild(addNew);

	interface CustomCard {
		dirty: boolean;
		code: string;
		card: SpyCards.CardDef;
	}
	let fieldIndex = 0;
	const initialParsed = initialData ? await SpyCards.parseCustomCards(initialData, "ignore-variant", true) : { cards: [], customCardsRaw: [] };
	const gameMode = initialParsed.mode || new SpyCards.GameModeData();
	const customCards: CustomCard[] = initialParsed.cards.map((c, i) => ({
		dirty: false,
		code: initialParsed.customCardsRaw[i + (initialParsed.mode ? 1 : 0)],
		card: c,
	}));
	let customTribes: SpyCards.CustomTribeData[] = null;

	const gameModeFieldNames = [
		{ name: "Game Mode Metadata", field: SpyCards.GameModeFieldType.Metadata },
		{ name: "Banned Cards", field: SpyCards.GameModeFieldType.BannedCards },
		{ name: "Game Rules", field: SpyCards.GameModeFieldType.GameRules },
		{ name: "Summon Card", field: SpyCards.GameModeFieldType.SummonCard },
		{ name: "Variant", field: SpyCards.GameModeFieldType.Variant },
		{ name: "Unfilter Card", field: SpyCards.GameModeFieldType.UnfilterCard },
		{ name: "Deck Limit Filter", field: SpyCards.GameModeFieldType.DeckLimitFilter },
		{ name: "Timer", field: SpyCards.GameModeFieldType.Timer },
	];
	let resetting = false;
	function resetEditors() {
		customDefs = new SpyCards.CardDefs({ cards: customCards.map((c) => c.card), customCardsRaw: [] });

		disableDoSquish = true;
		resetting = true;
		SpyCards.UI.clear(gameModeEditor);
		for (let field of gameMode.fields) {
			gameModeEditor.appendChild(createGameModeEditor(field, gameMode.fields));
		}
		gameModeEditor.appendChild(gameModeAddNew);

		SpyCards.UI.clear(editors);
		for (let data of customCards) {
			appendEditor(data, false);
		}
		resetting = false;

		updateCode();

		requestAnimationFrame(function () {
			disableDoSquish = false;
			doSquish(document);
		});
	}
	resetEditors();

	const newFieldSelect = document.createElement("select");
	for (let { name, field } of gameModeFieldNames) {
		newFieldSelect.appendChild(SpyCards.UI.option(name, String(field)));
	}

	const newFieldLabel = document.createElement("label");
	newFieldLabel.textContent = "Data Type: ";
	newFieldLabel.appendChild(newFieldSelect);
	gameModeAddNew.appendChild(newFieldLabel);
	const newFieldButton = SpyCards.UI.button("Add Field", [], () => {
		const type: SpyCards.GameModeFieldType = parseInt(newFieldSelect.value, 10);
		const field = SpyCards.GameModeFieldConstructors[type]();
		gameMode.fields.push(field);
		gameModeEditor.insertBefore(createGameModeEditor(field, gameMode.fields), gameModeAddNew);
		updateCode();
	});
	gameModeAddNew.appendChild(newFieldButton);

	code.addEventListener("input", async () => {
		const originalCodes = code.value;
		let codes = originalCodes.substr(originalCodes.indexOf("#") + 1);
		let parsed: SpyCards.ParsedCustomCards = { cards: [], customCardsRaw: [] };
		if (codes) {
			try {
				parsed = await SpyCards.parseCustomCards(codes, "ignore-variant", true);
				if (originalCodes !== code.value) {
					return;
				}
			} catch (ex) {
				return;
			}
		}
		gameMode.fields.length = 0;
		if (parsed.mode) {
			gameMode.fields = parsed.mode.fields;
		}
		customCards.length = 0;
		customTribes = null;
		const offset = parsed.mode ? 1 : 0;
		for (let i = 0; i < parsed.cards.length; i++) {
			customCards.push({
				dirty: false,
				code: parsed.customCardsRaw[i + offset],
				card: parsed.cards[i],
			});
		}

		updateCode();
		resetEditors();
	});
	code.addEventListener("blur", () => updateCode());

	for (let r of [SpyCards.Rank.Attacker, SpyCards.Rank.Effect, SpyCards.Rank.MiniBoss, SpyCards.Rank.Boss]) {
		const rank = r;
		const btn = SpyCards.UI.button(SpyCards.rankName(rank), [], () => addNewCard(rank));
		addNew.appendChild(btn);
	}

	function updateCode() {
		if (resetting) {
			return;
		}

		if (!gameMode.fields.length && !customCards.length) {
			code.value = "";
			backLink.href = "custom.html";
			history.replaceState(null, "", "custom.html?editor");
			return;
		}

		const customCardData = customCards.map((c) => {
			if (c.dirty) {
				const buf: number[] = [];
				c.card.marshal(buf);
				c.code = Base64.encode(new Uint8Array(buf));
			}
			return c.code;
		});
		if (gameMode.fields.length) {
			const buf: number[] = [];
			gameMode.marshal(buf);
			customCardData.unshift(Base64.encode(new Uint8Array(buf)));
		}
		code.value = customCardData.join(",");
		backLink.href = "custom.html#" + code.value;

		const newURL = "custom.html?editor#" + customCardData.join(",");
		clearTimeout(debounceCode);
		debounceCode = setTimeout(function () {
			history.replaceState(null, "", newURL);
		}, 1000);
	}

	async function modalSelectCard(caption: string, choices: SpyCards.CardDef[]): Promise<SpyCards.CardDef> {
		const modal = document.createElement("div");
		modal.classList.add("readme", "card-editor-modal-select");
		const modalCaption = document.createElement("p");
		modalCaption.textContent = caption;
		modal.appendChild(modalCaption);
		const modalCancel = new Promise<null>((resolve) => {
			const btn = SpyCards.UI.button("Cancel", [], () => resolve(null));
			modal.appendChild(btn);
		});

		SpyCards.UI.html.classList.add("in-modal");
		document.body.appendChild(modal);
		const choice = await SpyCards.Decks.selectCard(modalCaption, customDefs, choices, modalCancel);
		SpyCards.UI.remove(modal);
		SpyCards.UI.html.classList.remove("in-modal");

		return choice;
	}

	async function modalCreateTribe(card: SpyCards.CardDef, index: number): Promise<SpyCards.CustomTribeData> {
		const takenNames: string[] = [];
		for (let tribe = 0; tribe < SpyCards.Tribe.Custom; tribe++) {
			takenNames.push(SpyCards.tribeName(tribe, "").toLowerCase());
		}
		for (let { card: custom } of customCards) {
			for (let i = 0; i < custom.tribes.length; i++) {
				if (custom.tribes[i].custom && (custom !== card || i !== index)) {
					takenNames.push(custom.tribes[i].custom.name.toLowerCase());
				}
			}
		}

		const modal = document.createElement("div");
		modal.classList.add("readme", "card-editor-modal-tribe");
		const customTribe = new SpyCards.CustomTribeData();

		const cardEl = card.createEl(customDefs);
		modal.appendChild(cardEl);

		const placeholders = cardEl.querySelector<HTMLDivElement>(".card-tribe-placeholders");
		let placeholder = placeholders.querySelector<HTMLSpanElement>(":scope > .card-tribe-placeholder:nth-child(" + (index + 1) + ")");
		if (!placeholder) {
			placeholder = document.createElement("span");
			placeholder.classList.add("card-tribe-placeholder");
			placeholders.appendChild(placeholder);
			placeholders.style.setProperty("--numtribes", String(placeholders.children.length));
		}
		placeholder.setAttribute("data-tribe", "");
		placeholder.title = "";
		let placeholderSquish = placeholder.querySelector<HTMLSpanElement>(".squish");
		if (!placeholderSquish) {
			SpyCards.UI.clear(placeholder);
			placeholderSquish = document.createElement("span");
			placeholderSquish.classList.add("squish");
			placeholder.appendChild(placeholderSquish);
		}

		const nameInput = document.createElement("input");
		nameInput.type = "text";
		nameInput.value = customTribe.name;
		const nameLabel = document.createElement("label");
		nameLabel.setAttribute("data-field", "Name");
		nameLabel.textContent = "Name: ";
		nameLabel.appendChild(nameInput);
		modal.appendChild(nameLabel);

		const colorInput = document.createElement("input");
		colorInput.type = "color";
		colorInput.value = "#" + (0x1000000 + customTribe.rgb).toString(16).substr(1);
		const colorLabel = document.createElement("label");
		colorLabel.setAttribute("data-field", "Color");
		colorLabel.textContent = "Color: ";
		colorLabel.appendChild(colorInput);
		modal.appendChild(colorLabel);

		const donePromise = new Promise<boolean>((resolve) => {
			const btn1 = SpyCards.UI.button("Create Tribe", ["btn1"], () => resolve(true));
			btn1.disabled = true;
			modal.appendChild(btn1);
			const btn2 = SpyCards.UI.button("Cancel", ["btn2"], () => resolve(false));
			modal.appendChild(btn2);

			function updatePreview() {
				placeholderSquish.textContent = nameInput.value;
				customTribe.name = nameInput.value;
				placeholder.style.setProperty("--custom-color", colorInput.value);
				customTribe.rgb = parseInt(colorInput.value.substr(1), 16);
				doSquish(placeholder);

				if (!customTribe.name) {
					nameInput.setCustomValidity("Name is required.");
					nameInput.reportValidity();
					btn1.disabled = true;
				} else if (takenNames.indexOf(customTribe.name.toLowerCase()) !== -1) {
					nameInput.setCustomValidity("A tribe with this name already exists.");
					nameInput.reportValidity();
					btn1.disabled = true;
				} else {
					nameInput.setCustomValidity("");
					btn1.disabled = false;
				}
			}
			nameInput.addEventListener("input", updatePreview);
			colorInput.addEventListener("input", updatePreview);
			requestAnimationFrame(updatePreview);
		});

		SpyCards.UI.html.classList.add("in-modal");
		document.body.appendChild(modal);
		const ok = await donePromise;
		SpyCards.UI.remove(modal);
		SpyCards.UI.html.classList.remove("in-modal");

		return ok ? customTribe : null;
	}

	async function addNewCard(rank: SpyCards.Rank) {
		const choices: SpyCards.CardDef[] = [];
		for (let card of vanillaDefs.getAvailableCards(rank, SpyCards.Tribe.None)) {
			if (!customCards.some((c) => c.card.id === card.id)) {
				choices.push(card);
			}
		}
		choices.push(nextFreeCustom(rank));

		const choice = await modalSelectCard("Select a card to replace.", choices);
		if (!choice) {
			return;
		}

		const def = new SpyCards.CardDef();
		const buf: number[] = [];
		choice.marshal(buf);
		const code = Base64.encode(new Uint8Array(buf));
		def.unmarshal(buf);
		const custom = {
			dirty: false,
			code: code,
			card: def,
		};
		customCards.push(custom);
		appendEditor(custom, true);
		updateCode();
	}

	function nextFreeCustom(rank: SpyCards.Rank): SpyCards.CardDef {
		for (let idx = 0; ; idx++) {
			const id = 128 + (rank << 5) + ((idx & ~31) << 2) + (idx & 31);
			if (!customCards.some((c) => c.card.id === id)) {
				const card = new SpyCards.CardDef();
				card.id = id;
				if (rank === SpyCards.Rank.Attacker) {
					card.effects.push(new SpyCards.EffectDef());
					card.effects[0].type = SpyCards.EffectType.Stat;
				}
				return card;
			}
		}
	}

	function appendEditor(data: CustomCard, edit: boolean) {
		editors.appendChild(createEditor(data, edit));
	}

	function createEditor(data: CustomCard, edit: boolean): HTMLDivElement {
		const editor = document.createElement("div");
		editor.classList.add("readme", "card-editor");
		editor.setAttribute("data-rank", SpyCards.Rank[data.card.rank()]);

		const baseCardDef = data.card.id >= 128 ? new SpyCards.CardDef() : vanillaDefs.cardsByID[data.card.id];
		if (data.card.id >= 128) {
			baseCardDef.id = data.card.id;
			if (data.card.rank() === SpyCards.Rank.Attacker) {
				baseCardDef.effects.push(new SpyCards.EffectDef());
				baseCardDef.effects[0].type = SpyCards.EffectType.Stat;
			}
		}

		if (edit) {
			const baseCard = document.createElement("div");
			baseCard.classList.add("base-card", "no-flip");
			baseCard.textContent = "Based on";
			const baseCardEl = baseCardDef.createEl(customDefs);
			baseCardEl.classList.add("clickable");
			baseCardEl.title = "Click to replace base card.";
			baseCard.appendChild(baseCardEl);
			baseCardEl.addEventListener("click", async () => {
				const choices: SpyCards.CardDef[] = [];
				for (let newBase of vanillaDefs.getAvailableCards(data.card.rank(), SpyCards.Tribe.None)) {
					if (!customCards.some((c) => c !== data && c.card.id === newBase.id)) {
						choices.push(newBase);
					}
				}
				if (data.card.id >= 128 && !customCards.some((c) => c !== data && c.card.id === data.card.id)) {
					choices.push(baseCardDef);
				} else {
					choices.push(nextFreeCustom(data.card.rank()));
				}

				const choice = await modalSelectCard("Select a new base card for " + data.card.displayName() + ".", choices);
				if (choice) {
					data.card.id = choice.id;
					data.dirty = true;
					resetEditors();
				}
			});
			editor.appendChild(baseCard);
		}

		const editedCard = document.createElement("div");
		editedCard.classList.add("edited-card");
		editor.appendChild(editedCard);

		function updatePreview() {
			const editedCardEl = data.card.createEl(customDefs);
			const portrait = editedCardEl.querySelector<HTMLDivElement>(".portrait");
			if (edit) {
				portrait.classList.add("clickable");
				portrait.title = "Click to replace this card's portrait.";
				portrait.addEventListener("click", async () => {
					const baseEl = data.card.createEl(customDefs);
					SpyCards.UI.clear(baseEl.querySelector(".portrait"));
					const choices: SpyCards.CardDef[] = [];
					for (let i = 0; i < 235; i++) {
						const portrait = i;
						choices.push(<SpyCards.CardDef><unknown>{
							portrait: i,
							customPortrait: null,
							createEl() {
								const el = <HTMLDivElement>baseEl.cloneNode(true);
								el.querySelector(".portrait").setAttribute("data-x", String(portrait & 15));
								el.querySelector(".portrait").setAttribute("data-y", String(portrait >> 4));
								return el;
							}
						});
					}
					for (let id of user_images_whitelist) {
						const customID = id;
						choices.push(<SpyCards.CardDef><unknown>{
							portrait: 255,
							customPortrait: CrockfordBase32.decode(id),
							createEl() {
								const el = <HTMLDivElement>baseEl.cloneNode(true);
								el.querySelector(".portrait").setAttribute("data-x", "15");
								el.querySelector(".portrait").setAttribute("data-y", "15");
								const img = document.createElement("img");
								img.src = user_image_base_url + customID + ".png";
								img.crossOrigin = "anonymous";
								img.width = 128;
								img.height = 128;
								el.querySelector(".portrait").appendChild(img);
								return el;
							}
						});
					}
					choices.push(<SpyCards.CardDef><unknown>{
						portrait: 254,
						createEl() {
							const el = <HTMLDivElement>baseEl.cloneNode(true);
							el.querySelector(".portrait").setAttribute("data-x", "15");
							el.querySelector(".portrait").setAttribute("data-y", "15");
							const img = document.createElement("img");
							img.src = "img/upload-custom-portrait.png";
							img.width = 128;
							img.height = 128;
							el.querySelector(".portrait").appendChild(img);
							return el;
						}
					});

					const choice = await modalSelectCard("Select a new portrait for " + data.card.displayName() + ".", choices);
					if (!choice) {
						return;
					}

					if (choice.portrait === 254) {
						const upload = document.createElement("input");
						upload.type = "file";
						upload.accept = "image/png";
						const filePromise = new Promise<File>((resolve) => {
							upload.addEventListener("input", () => {
								resolve(upload.files[0]);
							});
						});
						upload.click();

						const file = await filePromise;
						if (!file) {
							return;
						}

						const imageData = new Uint8Array(await file.arrayBuffer());
						data.card.portrait = 254;
						data.card.customPortrait = imageData;
						data.dirty = true;
						updatePreview();

						const xhr = new XMLHttpRequest();
						xhr.open("PUT", user_image_base_url + "upload");
						const uploadPromise = new Promise<string>((resolve, reject) => {
							xhr.addEventListener("load", () => {
								if (xhr.status === 201) {
									resolve(xhr.responseText);
								} else {
									reject(new Error("server returned " + xhr.status + " " + xhr.statusText));
								}
							});
							xhr.addEventListener("error", () => {
								reject(new Error("network error"));
							});
						});
						xhr.send(file);

						try {
							const fileID = await uploadPromise;
							if (data.card.portrait === 254 && data.card.customPortrait === imageData) {
								data.card.portrait = 255;
								data.card.customPortrait = CrockfordBase32.decode(fileID);
								data.dirty = true;
								updatePreview();
							}
						} catch (ex) {
							debugger;
						}

						return;
					}

					data.card.portrait = choice.portrait;
					data.card.customPortrait = choice.customPortrait;
					data.dirty = true;
					updatePreview();
				});
			}
			SpyCards.UI.clear(editedCard);
			editedCard.appendChild(editedCardEl);
			updateCode();
		}
		updatePreview();

		if (!edit) {
			const editButton = SpyCards.UI.button("Edit", [], () => {
				SpyCards.UI.replace(editor, createEditor(data, true));
			});
			editButton.setAttribute("data-field", "Edit");
			editor.appendChild(editButton);
			return editor;
		}

		function appendField(name: string, input: HTMLElement): HTMLLabelElement {
			input.id = input.id || "field" + (fieldIndex++);
			const label = document.createElement("label");
			label.setAttribute("data-field", name);
			label.setAttribute("for", input.id);
			label.textContent = name + ": ";
			label.appendChild(input);
			editor.appendChild(label);
			return label;
		}

		const name = document.createElement("input");
		name.type = "text";
		name.placeholder = data.card.originalName();
		name.value = data.card.name;
		appendField("Name", name);
		let nameChanged = false;
		name.addEventListener("input", () => {
			data.card.name = name.value;
			data.dirty = true;
			nameChanged = true;
			updatePreview();
		});
		// reset editors to update name in all effects
		name.addEventListener("blur", () => {
			if (nameChanged) {
				nameChanged = false;
				resetEditors();
			}
		});

		const tp = document.createElement("input");
		tp.type = "number";
		tp.min = "-250";
		tp.max = "250";
		tp.valueAsNumber = isFinite(data.card.tp) ? data.card.tp : 1;
		const tpField = appendField("TP", tp);
		tp.addEventListener("input", () => {
			if (!tp.reportValidity()) {
				return;
			}
			data.card.tp = tp.valueAsNumber;
			data.dirty = true;
			updatePreview();
		});
		const tpInfinityPlaceholder = document.createElement("input");
		tpInfinityPlaceholder.type = "text";
		tpInfinityPlaceholder.value = "Infinity";
		tpInfinityPlaceholder.disabled = true;
		tpField.appendChild(tpInfinityPlaceholder);
		const tpInfinity = document.createElement("input");
		tpInfinity.type = "checkbox";
		tpInfinity.addEventListener("input", () => {
			if (tpInfinity.checked) {
				data.card.tp = Infinity;
			} else {
				data.card.tp = tp.valueAsNumber;
			}
			data.dirty = true;
			updatePreview();

			tp.style.display = tpInfinity.checked ? "none" : "";
			tpInfinityPlaceholder.style.display = tpInfinity.checked ? "" : "none";
		});
		tpField.insertBefore(tpInfinity, tp);
		if (isFinite(data.card.tp)) {
			tpInfinityPlaceholder.style.display = "none";
		} else {
			tpInfinity.checked = true;
			tp.style.display = "none";
		}

		const tribeSel = [
			createTribeSelect(true, null)
		];
		tribeSel[0][0].addEventListener("input", () => setTribe(0, tribeSel[0][0]));
		const tribeField = appendField("Tribes", tribeSel[0][0]);
		function resetTribeList() {
			data.card.tribes = data.card.tribes.filter((v, i, a) => {
				if (v.tribe === SpyCards.Tribe.None) {
					return false;
				} else if (v.tribe === SpyCards.Tribe.Custom) {
					return a.findIndex((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === v.custom.name) === i;
				} else {
					return a.findIndex((t) => t.tribe === v.tribe) === i;
				}
			});
			if (data.card.tribes.length === 0) {
				data.card.tribes.push({
					tribe: SpyCards.Tribe.Unknown,
					custom: null
				});
			}
			while (tribeSel.length > data.card.tribes.length + 1) {
				SpyCards.UI.remove(tribeSel.pop()[0]);
			}
			while (tribeSel.length < data.card.tribes.length + 1) {
				const i = tribeSel.length;
				const sel = createTribeSelect(true, "(none)");
				sel[0].addEventListener("input", () => setTribe(i, sel[0]));
				tribeSel.push(sel);
				tribeField.appendChild(sel[0]);
			}
			for (let i = 0; i < data.card.tribes.length; i++) {
				tribeSel[i][1]();
				tribeSel[i][0].value = data.card.tribes[i].tribe === SpyCards.Tribe.Custom ? "?" + data.card.tribes[i].custom.name : String(data.card.tribes[i].tribe);
			}
			tribeSel[tribeSel.length - 1][1]();
			tribeSel[tribeSel.length - 1][0].value = String(SpyCards.Tribe.None);
		}
		async function setTribe(i: number, sel: HTMLSelectElement) {
			while (data.card.tribes.length <= i) {
				data.card.tribes.push({
					tribe: SpyCards.Tribe.None,
					custom: null
				});
			}

			if (sel.value[0] === "?") {
				data.card.tribes[i].tribe = SpyCards.Tribe.Custom;
				data.card.tribes[i].custom = new SpyCards.CustomTribeData();
				data.card.tribes[i].custom.name = sel.value.substr(1);
				data.card.tribes[i].custom.rgb = parseInt(sel.selectedOptions[0].getAttribute("data-custom-color"), 10);
			} else if (sel.value === "!") {
				const customTribe = await modalCreateTribe(data.card, i);
				if (customTribe) {
					data.card.tribes[i].tribe = SpyCards.Tribe.Custom;
					data.card.tribes[i].custom = customTribe;
				}
			} else {
				data.card.tribes[i].tribe = parseInt(sel.value, 10);
				data.card.tribes[i].custom = null;
			}

			data.dirty = true;
			customTribes = null;
			resetTribeList();
			updatePreview();
		}

		resetTribeList();

		const deleteButton = SpyCards.UI.button("Delete Card", ["delete"], () => {
			if (confirm("Really delete this card?")) {
				customCards.splice(customCards.indexOf(data), 1);
				resetEditors();
			}
		});
		editor.appendChild(deleteButton);
		const moveUp = SpyCards.UI.button("\u25B2", ["move-up"], () => {
			const i = customCards.indexOf(data);
			customCards.splice(i, 1);
			customCards.splice(i - 1, 0, data);
			editor.parentNode.insertBefore(editor, editor.previousSibling);
			updateCode();
		});
		moveUp.title = "Move card up in list";
		editor.appendChild(moveUp);
		const moveDown = SpyCards.UI.button("\u25BC", ["move-down"], () => {
			const i = customCards.indexOf(data);
			customCards.splice(i, 1);
			customCards.splice(i + 1, 0, data);
			editor.parentNode.insertBefore(editor, editor.nextSibling.nextSibling);
			updateCode();
		});
		moveDown.title = "Move card down in list";
		editor.appendChild(moveDown);

		const effectEditors = document.createElement("div");
		effectEditors.classList.add("card-effect-editors");
		editor.appendChild(effectEditors);

		const cloneCard = SpyCards.UI.button("Clone Card", [], async () => {
			const buf: number[] = [];
			data.card.marshal(buf);
			const clone = new SpyCards.CardDef();
			clone.unmarshal(buf);
			if (data.card.id >= 128) {
				const nextFree = nextFreeCustom(data.card.rank());
				clone.id = nextFree.id;
				customCards.splice(customCards.indexOf(data) + 1, 0, {
					dirty: true,
					code: "",
					card: clone,
				});
				resetEditors();
			} else {
				const choices: SpyCards.CardDef[] = [];
				for (let newBase of vanillaDefs.getAvailableCards(data.card.rank(), SpyCards.Tribe.None)) {
					if (!customCards.some((c) => c.card.id === newBase.id)) {
						choices.push(newBase);
					}
				}
				choices.push(nextFreeCustom(data.card.rank()));

				const choice = await modalSelectCard("Select a base card for the clone of " + data.card.displayName() + ".", choices);
				if (choice) {
					clone.id = choice.id;
					customCards.splice(customCards.indexOf(data) + 1, 0, {
						dirty: true,
						code: "",
						card: clone,
					});
					resetEditors();
				}
			}
		});
		cloneCard.setAttribute("data-field", "Clone Card");
		editor.appendChild(cloneCard);

		if (data.card.rank() === SpyCards.Rank.Attacker) {
			const statEditor = document.createElement("fieldset");
			statEditor.classList.add("card-effect-editor", "card-stat-editor");
			const statLegend = document.createElement("legend");
			statLegend.textContent = "Attacker Stats";
			statEditor.appendChild(statLegend);

			const atkInput = document.createElement("input");
			atkInput.type = "number";
			atkInput.required = true;
			atkInput.min = "-250";
			atkInput.max = "250";
			const atkField = appendField("ATK", atkInput);
			statEditor.appendChild(atkField);

			const defInput = document.createElement("input");
			defInput.type = "number";
			defInput.required = true;
			defInput.min = "-250";
			defInput.max = "250";
			const defField = appendField("DEF", defInput);
			statEditor.appendChild(defField);

			const atkEffect = data.card.effects.find((e) => !e.defense) || new SpyCards.EffectDef();
			const defEffect = data.card.effects.find((e) => e.defense) || new SpyCards.EffectDef();

			if (atkEffect.type !== SpyCards.EffectType.Stat) {
				atkEffect.type = SpyCards.EffectType.Stat;
				atkEffect.amount = 0;
			}
			if (defEffect.type !== SpyCards.EffectType.Stat) {
				defEffect.type = SpyCards.EffectType.Stat;
				defEffect.defense = true;
				defEffect.amount = 0;
			}

			atkInput.valueAsNumber = atkEffect.negate ? -atkEffect.amount : atkEffect.amount;
			defInput.valueAsNumber = defEffect.negate ? -defEffect.amount : defEffect.amount;

			const updateStats = (blur: boolean, input: HTMLInputElement) => {
				if (blur) {
					if (!atkInput.valueAsNumber && !defInput.valueAsNumber) {
						input.valueAsNumber = 1;
					}
					atkInput.setCustomValidity("");
					defInput.setCustomValidity("");
				} else {
					if (!input.reportValidity() || isNaN(input.valueAsNumber)) {
						return;
					}
					if (!atkInput.valueAsNumber && !defInput.valueAsNumber) {
						input.setCustomValidity("ATK and DEF cannot both be 0.");
						input.reportValidity();
						return;
					}
				}

				input.setCustomValidity("");

				atkEffect.amount = Math.abs(atkInput.valueAsNumber);
				defEffect.amount = Math.abs(defInput.valueAsNumber);
				atkEffect.negate = atkInput.valueAsNumber < 0;
				defEffect.negate = defInput.valueAsNumber < 0;

				data.card.effects.length = 0;
				if (atkEffect.amount) {
					data.card.effects.push(atkEffect);
				}
				if (defEffect.amount) {
					data.card.effects.push(defEffect);
				}
				data.dirty = true;
				updatePreview();
			};

			atkInput.addEventListener("input", () => updateStats(false, atkInput));
			defInput.addEventListener("input", () => updateStats(false, defInput));
			atkInput.addEventListener("blur", () => updateStats(true, atkInput));
			defInput.addEventListener("blur", () => updateStats(true, defInput));

			effectEditors.appendChild(statEditor);
		} else {
			const addNewEffect = SpyCards.UI.button("Add New Effect", [], () => {
				const effect = new SpyCards.EffectDef();
				effect.generic = true; // this is a nicer default in most cases
				data.card.effects.push(effect);
				effectEditors.appendChild(createEffectEditor(effect));
				data.dirty = true;
				updatePreview();
			});
			addNewEffect.setAttribute("data-field", "Add Effect");
			editor.appendChild(addNewEffect);
			for (let effect of data.card.effects) {
				effectEditors.appendChild(createEffectEditor(effect));
			}
		}

		return editor;

		function createEffectEditor(effect: SpyCards.EffectDef, parentEffect?: SpyCards.EffectDef, replace?: HTMLFieldSetElement): HTMLFieldSetElement {
			const effectEditor = replace || document.createElement("fieldset");
			effectEditor.classList.add("card-effect-editor");
			effectEditor.setAttribute("data-type", SpyCards.EffectType[effect.type]);

			const legend = document.createElement("legend");
			if (!parentEffect) {
				const buttons = document.createElement("div");
				buttons.classList.add("control-buttons");
				const moveUp = SpyCards.UI.button("\u25B2", ["move-up"], () => {
					const i = data.card.effects.indexOf(effect);
					data.card.effects.splice(i, 1);
					data.card.effects.splice(i - 1, 0, effect);
					effectEditor.parentNode.insertBefore(effectEditor, effectEditor.previousSibling);
					data.dirty = true;
					updatePreview();
				});
				moveUp.title = "Move effect up in list";
				buttons.appendChild(moveUp);
				const moveDown = SpyCards.UI.button("\u25BC", ["move-down"], () => {
					const i = data.card.effects.indexOf(effect);
					data.card.effects.splice(i, 1);
					data.card.effects.splice(i + 1, 0, effect);
					effectEditor.parentNode.insertBefore(effectEditor, effectEditor.nextSibling.nextSibling);
					data.dirty = true;
					updateCode();
				});
				moveDown.title = "Move effect down in list";
				buttons.appendChild(moveDown);
				const copyButton = SpyCards.UI.button("Copy", ["copy"], () => {
					if (!effect.isValid()) {
						return;
					}

					const buf: number[] = [];
					effect.marshal(buf);
					const newEffect = new SpyCards.EffectDef();
					newEffect.unmarshal(buf, 4);
					data.card.effects.splice(data.card.effects.indexOf(effect) + 1, 0, newEffect);
					const newEditor = createEffectEditor(newEffect, null, null);
					effectEditor.parentNode.insertBefore(newEditor, effectEditor.nextSibling);
					data.dirty = true;
					updatePreview();
				});
				copyButton.title = "Make a copy of this effect";
				buttons.appendChild(copyButton);
				const deleteButton = SpyCards.UI.button("Delete", ["delete"], () => {
					data.card.effects.splice(data.card.effects.indexOf(effect), 1);
					SpyCards.UI.remove(effectEditor);
					data.dirty = true;
					updatePreview();
				});
				buttons.appendChild(deleteButton);
				legend.appendChild(buttons);
			}

			if (replace) {
				const deleteButtonForCoin = parentEffect ? replace.querySelector(".delete") : null;
				SpyCards.UI.clear(replace);
				if (deleteButtonForCoin) {
					legend.appendChild(deleteButtonForCoin);
				}
			}

			const typeSelect = document.createElement("select");

			const noneType = SpyCards.UI.option("(select an effect)", "");
			noneType.disabled = true;
			typeSelect.appendChild(noneType);

			if (!parentEffect) {
				typeSelect.appendChild(SpyCards.UI.option("Flavor Text", String(SpyCards.EffectType.FlavorText)));
			}

			const effectGroup = document.createElement("optgroup");
			effectGroup.label = "Effects";
			typeSelect.appendChild(effectGroup);

			effectGroup.appendChild(SpyCards.UI.option("Stat", String(SpyCards.EffectType.Stat)));
			effectGroup.appendChild(SpyCards.UI.option("Empower", String(SpyCards.EffectType.Empower)));
			effectGroup.appendChild(SpyCards.UI.option("Summon", String(SpyCards.EffectType.Summon)));
			effectGroup.appendChild(SpyCards.UI.option("Heal", String(SpyCards.EffectType.Heal)));
			if (!parentEffect || (parentEffect.type === SpyCards.EffectType.CondApply)) {
				effectGroup.appendChild(SpyCards.UI.option("TP", String(SpyCards.EffectType.TP)));
			}
			effectGroup.appendChild(SpyCards.UI.option("Numb", String(SpyCards.EffectType.Numb)));

			const conditionGroup = document.createElement("optgroup");
			conditionGroup.label = "Conditions";
			typeSelect.appendChild(conditionGroup);

			conditionGroup.appendChild(SpyCards.UI.option("If Card", String(SpyCards.EffectType.CondCard)));
			conditionGroup.appendChild(SpyCards.UI.option("Limit", String(SpyCards.EffectType.CondLimit)));
			conditionGroup.appendChild(SpyCards.UI.option("Winner", String(SpyCards.EffectType.CondWinner)));
			conditionGroup.appendChild(SpyCards.UI.option("Apply", String(SpyCards.EffectType.CondApply)));
			conditionGroup.appendChild(SpyCards.UI.option("Coin", String(SpyCards.EffectType.CondCoin)));
			conditionGroup.appendChild(SpyCards.UI.option("If HP", String(SpyCards.EffectType.CondHP)));
			conditionGroup.appendChild(SpyCards.UI.option("If Stat", String(SpyCards.EffectType.CondStat)));
			conditionGroup.appendChild(SpyCards.UI.option("Priority Override", String(SpyCards.EffectType.CondPriority)));
			conditionGroup.appendChild(SpyCards.UI.option("On Numb", String(SpyCards.EffectType.CondOnNumb)));

			typeSelect.value = effect.type === null ? "" : String(effect.type);
			legend.appendChild(typeSelect);
			effectEditor.appendChild(legend);

			typeSelect.addEventListener("input", () => {
				if (String(effect.type) === typeSelect.value) {
					return;
				}

				effect.type = parseInt(typeSelect.value, 10);

				const newEditor = createEffectEditor(effect, parentEffect, effectEditor);
				newEditor.querySelector<HTMLSelectElement>("legend select").focus();
				data.dirty = true;
				updatePreview();
			});

			function addAmountField(fieldName: string, isCount?: boolean, unsigned?: boolean) {
				const input = document.createElement("input");
				input.type = "number";
				input.valueAsNumber = isFinite(effect.amount) ? (!isCount && !unsigned && effect.negate) ? -effect.amount : effect.amount : 1;
				input.required = true;
				input.min = isCount ? "1" : unsigned ? "0" : "-250";
				input.max = "250";
				const field = appendField(fieldName, input);
				effectEditor.appendChild(field);
				input.addEventListener("input", () => {
					if (!input.reportValidity() || isNaN(input.valueAsNumber)) {
						return;
					}
					effect.amount = Math.abs(input.valueAsNumber);
					if (!isCount && !unsigned) {
						effect.negate = input.valueAsNumber < 0;
					}
					data.dirty = true;
					updatePreview();
				});
				if (!isCount) {
					let negInfinity: HTMLInputElement;
					const isInfinity = document.createElement("input");
					isInfinity.type = "checkbox";
					field.insertBefore(isInfinity, input);
					const infinityPlaceholder = document.createElement("input");
					infinityPlaceholder.type = "text";
					infinityPlaceholder.disabled = true;
					infinityPlaceholder.value = "Infinity";
					field.appendChild(infinityPlaceholder);
					isInfinity.addEventListener("input", () => {
						if (isInfinity.checked) {
							input.style.display = "none";
							infinityPlaceholder.style.display = "";
							effect.amount = Infinity;
							if (negInfinity) {
								effect.negate = negInfinity.checked;
								negInfinity.style.display = "";
							}
						} else {
							input.dispatchEvent(new InputEvent("input"));
							input.style.display = "";
							infinityPlaceholder.style.display = "none";
							if (negInfinity) {
								negInfinity.style.display = "none";
							}
						}
						data.dirty = true;
						updatePreview();
					});
					if (!unsigned) {
						negInfinity = document.createElement("input");
						negInfinity.type = "checkbox";
						negInfinity.addEventListener("input", () => {
							effect.negate = negInfinity.checked;
							infinityPlaceholder.value = (negInfinity.checked ? "-" : "+") + "Infinity";
							data.dirty = true;
							updatePreview();
						});
						field.insertBefore(negInfinity, input);
						negInfinity.checked = effect.negate;
						infinityPlaceholder.value = (effect.negate ? "-" : "+") + "Infinity";
					}
					if (isFinite(effect.amount)) {
						infinityPlaceholder.style.display = "none";
						if (negInfinity) {
							negInfinity.style.display = "none";
						}
					} else {
						input.style.display = "none";
						isInfinity.checked = true;
					}
				}
				return field;
			}

			function addSelect(label: string, values: string[], currentValue: number, setValue: (n: number) => void) {
				const sel = document.createElement("select");
				for (let i = 0; i < values.length; i++) {
					sel.appendChild(SpyCards.UI.option(values[i], String(i)));
				}
				sel.value = String(currentValue);
				const field = appendField(label, sel);
				effectEditor.appendChild(field);
				sel.addEventListener("input", () => {
					setValue(parseInt(sel.value, 10));
					data.dirty = true;
					updatePreview();
				});
				return field;
			}

			function addFilter(generalLabel: string, specificLabel: string) {
				const fieldset = document.createElement("fieldset");
				fieldset.classList.add("card-effect-editor", "card-effect-filter-editor");
				const legend = document.createElement("legend");
				const checkbox = document.createElement("input");
				checkbox.type = "checkbox";

				function resetFilterUI() {
					SpyCards.UI.clear(fieldset);
					SpyCards.UI.clear(legend);

					checkbox.checked = effect.generic;

					const toggleLabel = document.createElement("label");
					toggleLabel.appendChild(checkbox);
					toggleLabel.appendChild(document.createTextNode(" " + (effect.generic ? generalLabel : specificLabel)));

					legend.appendChild(toggleLabel);
					fieldset.appendChild(legend);

					if (effect.generic) {
						const rankSel = document.createElement("select");
						rankSel.appendChild(SpyCards.UI.option("(any)", String(SpyCards.Rank.None)));
						rankSel.appendChild(SpyCards.UI.option("Attacker", String(SpyCards.Rank.Attacker)));
						rankSel.appendChild(SpyCards.UI.option("Effect", String(SpyCards.Rank.Effect)));
						rankSel.appendChild(SpyCards.UI.option("Enemy (Attacker or Effect)", String(SpyCards.Rank.Enemy)));
						rankSel.appendChild(SpyCards.UI.option("Mini-Boss", String(SpyCards.Rank.MiniBoss)));
						rankSel.appendChild(SpyCards.UI.option("Boss", String(SpyCards.Rank.Boss)));
						rankSel.value = String(effect.rank);
						rankSel.addEventListener("input", () => {
							effect.rank = parseInt(rankSel.value, 10);
							data.dirty = true;
							updatePreview();
						});
						const rankField = appendField("Rank", rankSel);
						fieldset.appendChild(rankField);

						const [tribeSel] = createTribeSelect(false, "(any)");
						tribeSel.value = effect.tribe === SpyCards.Tribe.Custom ? "?" + effect.customTribe : String(effect.tribe);
						tribeSel.addEventListener("input", () => {
							if (tribeSel.value.startsWith("?")) {
								effect.tribe = SpyCards.Tribe.Custom;
								effect.customTribe = tribeSel.value.substr(1);
							} else {
								effect.tribe = parseInt(tribeSel.value, 10);
								effect.customTribe = null;
							}
							data.dirty = true;
							updatePreview();
						});
						const tribeField = appendField("Tribe", tribeSel);
						fieldset.appendChild(tribeField);
					} else {
						if (effect.card === null) {
							effect.card = data.card.id;
						}
						const cardSel = createCardSelect(effect.card, (id) => {
							effect.card = id;
							data.dirty = true;
							updatePreview();
						});
						const cardField = appendField("Card", cardSel);
						fieldset.appendChild(cardField);
					}
				}

				checkbox.addEventListener("input", () => {
					effect.generic = checkbox.checked;
					data.dirty = true;
					resetFilterUI();
					updatePreview();
				})

				resetFilterUI();

				effectEditor.appendChild(fieldset);
			}

			if (effect.type === SpyCards.EffectType.FlavorText) {
				const flavorText = document.createElement("textarea");
				flavorText.value = effect.text;
				const flavorTextField = appendField("Text", flavorText);
				effectEditor.appendChild(flavorTextField);
				flavorText.addEventListener("input", () => {
					effect.text = flavorText.value;
					data.dirty = true;
					updatePreview();
				});

				addSelect("Remaining Effects", ["Show", "Hide"], effect.negate ? 1 : 0, (negate: number) => effect.negate = !!negate);
			}

			if (effect.type === SpyCards.EffectType.Stat) {
				addSelect("Stat", ["ATK", "DEF"], effect.defense ? 1 : 0, (defense: number) => effect.defense = !!defense);
				addSelect("Apply To", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addAmountField("Amount");
			}
			if (effect.type === SpyCards.EffectType.Empower) {
				addSelect("Stat", ["ATK", "DEF"], effect.defense ? 1 : 0, (defense: number) => effect.defense = !!defense);
				addSelect("Apply To", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addAmountField("Amount");
				addFilter("Card Filter", "Specific Card");
			}
			if (effect.type === SpyCards.EffectType.Summon) {
				addSelect("Style", ["Summon", "Replace", "Invisible Summon", "Replace with Invisible Summon"], (effect.negate ? 1 : 0) | (effect.defense ? 2 : 0), (flags: number) => {
					effect.negate = !!(flags & 1);
					effect.defense = !!(flags & 2);
				});
				addSelect("Summon For", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addAmountField("Count", true);
				addFilter("Random Card", "Specific Card");
			}
			if (effect.type === SpyCards.EffectType.Heal) {
				let multiplyTarget: HTMLLabelElement;
				addSelect("Effect", ["Heal", "Multiply Healing"], effect.each ? 1 : 0, (each: number) => {
					effect.each = !!each;
					multiplyTarget.style.display = each ? "" : "none";
				});
				multiplyTarget = addSelect("Affects", ["All Heal Effects", "Positive (Healing)", "Negative (Damage)"], effect.generic ? 0 : effect.defense ? 1 : 2, (state: number) => {
					effect.generic = state === 0;
					effect.defense = state === 1;
				});
				multiplyTarget.style.display = effect.each ? "" : "none";
				addSelect("Apply To", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addAmountField("Amount");
			}
			if (effect.type === SpyCards.EffectType.TP) {
				addAmountField("Amount");
			}
			if (effect.type === SpyCards.EffectType.Numb) {
				addSelect("Numb Cards For", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addAmountField("Count", false, true);
				addFilter("Card Filter", "Specific Card");
			}

			if (effect.type === SpyCards.EffectType.CondCard) {
				addSelect("Check Cards For", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				let countField: HTMLLabelElement;
				addSelect("Condition", ["Enough Cards", "Not Enough Cards", "Each Card"], effect.each ? 2 : effect.negate ? 1 : 0, (selection: number) => {
					effect.each = selection === 2;
					effect.negate = selection === 1;
					countField.style.display = effect.each ? "none" : "";
				});
				countField = addAmountField("Required Count", true);
				addFilter("Card Filter", "Specific Card");
			}
			if (effect.type === SpyCards.EffectType.CondLimit) {
				addSelect("Condition", ["Until Limit Reached", "After Limit Reached"], effect.negate ? 1 : 0, (negate) => effect.negate = !!negate);
				addAmountField("Limit", true);
			}
			if (effect.type === SpyCards.EffectType.CondWinner) {
				addSelect("Condition", ["Win", "Opponent Wins", "Round Tied", "Someone Wins"], (effect.opponent ? 1 : 0) | (effect.negate ? 2 : 0), (flags: number) => {
					effect.opponent = !!(flags & 1);
					effect.negate = !!(flags & 2);
				});
			}
			if (effect.type === SpyCards.EffectType.CondApply) {
				const applyChain = [effect];
				for (let child = effect.result; child && effect.late && child.late && child.type === effect.type && !child.opponent && child.negate === effect.negate; child = child.result) {
					applyChain.push(child);
				}
				addSelect("Ghost Card Description", ["Just Effect", "Original Description"], effect.negate ? 1 : 0, (negate: number) => {
					for (let c of applyChain) {
						c.negate = !!negate;
					}
				});
				addSelect("Apply To", ["Self", "Opponent", "Self (this turn)", "Opponent (this turn)"], (effect.opponent ? 1 : 0) | (effect.late ? 0 : 2), (flags: number) => {
					const rebuildUI = !!effect.late !== !(flags & 2);
					for (let c of applyChain) {
						c.opponent = !!(flags & 1);
						c.late = !(flags & 2);
					}

					if (rebuildUI) {
						if (!effect.late) {
							effect.result = applyChain[applyChain.length - 1].result;
						}

						createEffectEditor(effect, parentEffect, effectEditor);
						effectEditor.querySelector<HTMLSelectElement>("[data-field=\"Apply To\"] select").focus();
					}
				});
				if (effect.late) {
					const input = document.createElement("input");
					input.type = "number";
					input.valueAsNumber = applyChain.length;
					input.required = true;
					input.min = "1";
					input.max = "250";
					const field = appendField("Number of Turns", input);
					effectEditor.appendChild(field);
					input.addEventListener("input", () => {
						if (!input.reportValidity() || isNaN(input.valueAsNumber)) {
							return;
						}

						const trueResult = applyChain[applyChain.length - 1].result;
						applyChain.splice(1, applyChain.length - 1);
						while (applyChain.length < input.valueAsNumber) {
							const child = new SpyCards.EffectDef();
							child.type = SpyCards.EffectType.CondApply;
							child.late = effect.late;
							child.negate = effect.negate;
							applyChain[applyChain.length - 1].result = child;
							applyChain.push(child);
						}
						applyChain[applyChain.length - 1].result = trueResult;

						data.dirty = true;
						updatePreview();
					});
				}
			}

			let headsSide: HTMLDivElement;
			let tailsIcon: HTMLSpanElement;
			if (effect.type === SpyCards.EffectType.CondCoin) {
				addAmountField("Number of Coins", true);

				headsSide = document.createElement("div");
				headsSide.classList.add("card-editor-coin-heads");
				const coinTypeField = document.createElement("div");
				coinTypeField.classList.add("label");
				coinTypeField.setAttribute("data-field", "Coin Type");
				coinTypeField.textContent = "Heads: ";
				coinTypeField.appendChild(document.createElement("br"));
				const coinTypeFieldName = "field" + (fieldIndex++);
				for (let stat of [false, true]) {
					for (let negate of [false, true]) {
						const coinTypeLabel = document.createElement("label");
						coinTypeLabel.title = stat ?
							negate ? "DEF icon" : "ATK icon" :
							negate ? "Sad face" : "Happy face";
						const coinTypeRadio = document.createElement("input");
						coinTypeRadio.type = "radio";
						coinTypeRadio.checked = stat === !!effect.defense && negate === !!effect.negate;
						const setDefense = stat;
						const setNegate = negate;
						coinTypeRadio.addEventListener("input", () => {
							if (coinTypeRadio.checked) {
								effect.defense = setDefense;
								effect.negate = setNegate;
								tailsIcon.classList.toggle("heads", effect.negate);
								tailsIcon.classList.toggle("tails", !effect.negate);
								tailsIcon.classList.toggle("atk-def", effect.defense);
								tailsIcon.title = effect.defense ?
									effect.negate ? "ATK icon" : "DEF icon" :
									effect.negate ? "Happy face" : "Sad face";
								data.dirty = true;
								updatePreview();
							}
						});
						coinTypeRadio.name = coinTypeFieldName;
						coinTypeLabel.appendChild(coinTypeRadio);
						const coinVis = document.createElement("span");
						coinVis.classList.add("coin", negate ? "tails" : "heads");
						coinVis.classList.toggle("atk-def", stat);
						coinTypeLabel.appendChild(coinVis);
						coinTypeField.appendChild(coinTypeLabel);
					}
				}
				headsSide.appendChild(coinTypeField);
				effectEditor.appendChild(headsSide);
			}

			if (effect.type === SpyCards.EffectType.CondHP) {
				addSelect("Check HP For", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addSelect("When", ["At Start of Round", "After Round Ends"], effect.late ? 1 : 0, (late: number) => effect.late = !!late);
				addSelect("Condition", ["At Least", "Less Than"], effect.negate ? 1 : 0, (negate) => effect.negate = !!negate);
				addAmountField("Amount", true);
			}
			if (effect.type === SpyCards.EffectType.CondStat) {
				addSelect("Stat", ["ATK", "DEF"], effect.defense ? 1 : 0, (defense: number) => effect.defense = !!defense);
				addSelect("Check Stat For", ["Self", "Opponent"], effect.opponent ? 1 : 0, (opponent: number) => effect.opponent = !!opponent);
				addSelect("When", ["Before Numb/Pierce", "After Numb/Pierce"], effect.late ? 1 : 0, (late: number) => effect.late = !!late);
				addSelect("Condition", ["At Least", "Less Than"], effect.negate ? 1 : 0, (negate) => effect.negate = !!negate);
				addAmountField("Amount", true);
			}
			if (effect.type === SpyCards.EffectType.CondPriority) {
				const priorities = SpyCards.Effect.order.pre.concat(SpyCards.Effect.order.main, SpyCards.Effect.order.post).map((filter) => {
					let name = SpyCards.EffectType[filter.type];
					if (filter.negate === true) {
						name += " (negative)";
					} else if (filter.negate === false) {
						name += " (positive)";
					}
					if (filter.opponent === true) {
						name += " (opponent)";
					} else if (filter.opponent === false) {
						name += " (self)";
					}
					if (filter.each === true) {
						name += " (each)";
					} else if (filter.each === false) {
						name += " (once)";
					}
					if (filter.late === true) {
						name += " (late)";
					} else if (filter.late === false) {
						name += " (early)";
					}
					if (filter.generic === true) {
						name += " (generic)";
					} else if (filter.generic === false) {
						name += " (specific)";
					}
					if (filter.defense === true) {
						name += " (defense)";
					} else if (filter.defense === false) {
						name += " (attack)";
					}
					return { filter, name };
				});
				addSelect("Priority", priorities.map((p) => p.name), priorities.findIndex((p) => SpyCards.Processor.checkFilter(p.filter, effect)), (index) => {
					effect.amount = priorities[index].filter.type;
					effect.negate = priorities[index].filter.negate;
					effect.opponent = priorities[index].filter.opponent;
					effect.each = priorities[index].filter.each;
					effect.late = priorities[index].filter.late;
					effect.generic = priorities[index].filter.generic;
					effect.defense = priorities[index].filter.defense;
					effect._reserved6 = priorities[index].filter._reserved6;
					effect._reserved7 = priorities[index].filter._reserved7;
				});
			}

			if (effect.type >= 128) {
				if (!effect.result) {
					effect.result = new SpyCards.EffectDef();
					effect.result.generic = true;
				}
				let result = effect.result;
				while (effect.type === SpyCards.EffectType.CondApply && effect.late && result.type === effect.type && !!result.late === !!effect.late && !!result.opponent === !!effect.opponent && !!result.negate === !!effect.negate) {
					if (!result.result) {
						result.result = new SpyCards.EffectDef();
						result.result.generic = true;
					}
					result = result.result;
				}
				const resultEditor = createEffectEditor(result, effect);
				effectEditor.appendChild(resultEditor);
				if (effect.type === SpyCards.EffectType.CondCoin) {
					headsSide.appendChild(resultEditor);
					if (!effect.tailsResult) {
						effect.tailsResult = new SpyCards.EffectDef();
						effect.tailsResult.generic = true;
					}
					const tailsSide = document.createElement("div");
					tailsSide.classList.add("card-editor-coin-tails");
					const tailsResultEditor = createEffectEditor(effect.tailsResult, effect);
					const tailsLabel = document.createElement("div");
					const addTailsButton = SpyCards.UI.button("Add Tails Effect", ["add-tails"], () => {
						effect.generic = true;
						SpyCards.UI.replace(addTailsButton, tailsResultEditor);
						tailsLabel.classList.remove("no-tails");
						data.dirty = true;
						updatePreview();
					});
					const deleteTailsButton = SpyCards.UI.button("Delete", ["delete"], () => {
						effect.generic = false;
						SpyCards.UI.replace(tailsResultEditor, addTailsButton);
						tailsLabel.classList.add("no-tails");
						data.dirty = true;
						updatePreview();
					});
					tailsResultEditor.querySelector("legend").insertBefore(deleteTailsButton, tailsResultEditor.querySelector("legend > *"));
					tailsLabel.textContent = "Tails: ";
					tailsLabel.appendChild(document.createElement("br"));
					tailsIcon = document.createElement("span");
					tailsIcon.classList.add("coin", effect.negate ? "heads" : "tails");
					tailsIcon.classList.toggle("atk-def", effect.defense);
					tailsIcon.title = effect.defense ?
						effect.negate ? "ATK icon" : "DEF icon" :
						effect.negate ? "Happy face" : "Sad face";
					tailsLabel.appendChild(tailsIcon);
					tailsSide.appendChild(tailsLabel);
					if (effect.generic) {
						tailsSide.appendChild(tailsResultEditor);
					} else {
						tailsSide.appendChild(addTailsButton);
						tailsLabel.classList.add("no-tails");
					}
					effectEditor.appendChild(tailsSide);

					if (parentEffect) {
						requestAnimationFrame(() => {
							let parent = <HTMLElement>effectEditor.parentNode;
							if (parent && !parent.classList.contains("card-effect-editor")) {
								parent = <HTMLElement>parent.parentNode;
							}
							if (parent && parent.classList.contains("card-effect-editor")) {
								parent.classList.add("contains-coin");
							}
						});
					}
				}
			}

			return effectEditor;
		}
	}

	function createGameModeEditor(field: SpyCards.GameModeField, fields: SpyCards.GameModeField[]) {
		const editor = document.createElement("fieldset");
		editor.classList.add("game-mode-field");
		editor.setAttribute("data-type", SpyCards.GameModeFieldType[field.getType()]);
		const deleteButton = SpyCards.UI.button("Delete", ["delete"], () => {
			fields.splice(fields.indexOf(field), 1);
			resetEditors();
		});
		const legend = document.createElement("legend");
		editor.appendChild(legend);
		if (field instanceof SpyCards.GameModeMetadata) {
			legend.textContent = "Game Mode Metadata";

			for (let textField of [{
				name: "Title",
				value: field.title,
				set: (v: string) => field.title = v,
				multi: false
			}, {
				name: "Author",
				value: field.author,
				set: (v: string) => field.author = v,
				multi: false
			}, {
				name: "Description",
				value: field.description,
				set: (v: string) => field.description = v,
				multi: true
			}, {
				name: "Latest Changes",
				value: field.latestChanges,
				set: (v: string) => field.latestChanges = v,
				multi: true
			}]) {
				const { name, value, set, multi } = textField;
				const input = document.createElement(multi ? "textarea" : "input");
				if (!multi) {
					(input as HTMLInputElement).type = "text";
				}
				input.value = value;
				const label = document.createElement("label");
				label.textContent = name + ": ";
				label.appendChild(input);
				input.addEventListener("input", () => {
					set(input.value);
					updateCode();
				});
				editor.appendChild(label);
			}
		} else if (field instanceof SpyCards.GameModeBannedCards) {
			legend.textContent = "Banned Cards";
			const allVanilla = document.createElement("p");
			allVanilla.textContent = "All vanilla cards are banned.";
			editor.appendChild(allVanilla);
			allVanilla.style.display = field.bannedCards.length ? "none" : "";
			const showAllVanilla = () => {
				allVanilla.style.display = "";
				addBan.textContent = "Ban Specific Card";
			};
			const bannedCards = document.createElement("ul");
			for (let id of field.bannedCards) {
				appendBannedCard(bannedCards, showAllVanilla, field.bannedCards, id);
			}
			editor.appendChild(bannedCards);
			const addBan = SpyCards.UI.button(field.bannedCards.length ? "Ban Card" : "Ban Specific Card", [], async () => {
				const specificBanned: { [id: number]: boolean } = {};
				let allVanillaBanned = false;
				for (let f of gameMode.fields.concat(fields)) {
					if (f instanceof SpyCards.GameModeBannedCards) {
						if (f.bannedCards.length) {
							for (let id of f.bannedCards) {
								specificBanned[id] = true;
							}
						} else if (f !== field) {
							allVanillaBanned = true;
						}
					}
				}
				const choices: SpyCards.CardDef[] = [];
				for (let card of customDefs.allCards) {
					if (!specificBanned[card.id] && (card.id >= 128 || !allVanillaBanned)) {
						choices.push(card);
					}
				}
				const choice = await modalSelectCard("Select a card to ban.", choices);
				if (choice) {
					field.bannedCards.push(choice.id);
					appendBannedCard(bannedCards, showAllVanilla, field.bannedCards, choice.id);
					addBan.textContent = "Ban Card";
					allVanilla.style.display = "none";
					updateCode();
				}
			});
			editor.appendChild(addBan);
		} else if (field instanceof SpyCards.GameModeGameRules) {
			const defaults = new SpyCards.GameModeGameRules();
			legend.textContent = "Game Rules";

			for (let { type, get, set, signed, max } of SpyCards.GameModeGameRules.rules) {
				const label = document.createElement("label");
				label.textContent = SpyCards.GameModeGameRules.typeName(type) + ": ";
				const input = document.createElement("input");
				input.type = "number";
				if (!signed) {
					input.min = "0";
				}
				if (max) {
					input.max = String(max);
				}
				input.defaultValue = String(get(defaults));
				input.valueAsNumber = get(field);
				input.classList.toggle("modified", input.value !== input.defaultValue);
				input.addEventListener("input", () => {
					if (!input.reportValidity()) {
						return;
					}

					input.classList.toggle("modified", input.value !== input.defaultValue);
					set(field, input.valueAsNumber);
					updateCode();
				});
				label.appendChild(input);
				editor.appendChild(label);
			}

			const warning = document.createElement("p");
			warning.classList.add("warning");
			warning.textContent = "Only the first Game Rules field has any effect.";
			editor.appendChild(warning);
		} else if (field instanceof SpyCards.GameModeSummonCard) {
			legend.textContent = "Summon Card";
			const flag0Label = document.createElement("label");
			flag0Label.textContent = "On round 1, for ";
			const flag0 = document.createElement("select");
			flag0.appendChild(SpyCards.UI.option("Player 1", "0"));
			flag0.appendChild(SpyCards.UI.option("Both Players", String(SpyCards.SummonCardFlags.BothPlayers)));
			flag0.value = String(field.flags & SpyCards.SummonCardFlags.BothPlayers);
			flag0.addEventListener("input", () => {
				field.flags &= ~SpyCards.SummonCardFlags.BothPlayers;
				field.flags |= parseInt(flag0.value, 10);
				updateCode();
			});
			flag0Label.appendChild(flag0);
			editor.appendChild(flag0Label);

			const cardLabel = document.createElement("label");
			cardLabel.classList.add("no-flip");
			cardLabel.textContent = "Summon Card: ";
			const cardDef = customDefs.cardsByID[field.card];
			if (!cardDef) {
				// custom card was deleted; remove summon card rule
				deleteButton.click();
				return editor;
			}
			let card = cardDef.createEl(customDefs);
			card.id = SpyCards.UI.uniqueID();
			cardLabel.htmlFor = card.id;
			card.classList.add("clickable");
			card.addEventListener("click", async function onClick() {
				const toSummon = await modalSelectCard("Summon which card?", customDefs.allCards);
				if (!toSummon) {
					return;
				}
				field.card = toSummon.id;
				const newCardEl = toSummon.createEl(customDefs);
				newCardEl.id = card.id;
				newCardEl.classList.add("clickable");
				newCardEl.addEventListener("click", onClick);
				SpyCards.UI.replace(card, newCardEl);
				card = newCardEl;
				updateCode();
			});
			cardLabel.appendChild(card);
			editor.appendChild(cardLabel);
		} else if (field instanceof SpyCards.GameModeVariant) {
			legend.textContent = "Variant";

			const titleLabel = document.createElement("label");
			titleLabel.textContent = "Title: ";
			editor.appendChild(titleLabel);
			const title = document.createElement("input");
			title.placeholder = "(untitled)";
			title.value = field.title;
			title.addEventListener("input", () => {
				field.title = title.value;
				updateCode();
			});
			titleLabel.appendChild(title);

			const npcLabel = document.createElement("label");
			npcLabel.textContent = "NPC: ";
			editor.appendChild(npcLabel);
			const npc = document.createElement("input");
			npc.setAttribute("list", npcNames.id);
			npc.placeholder = "(none)";
			npc.value = field.npc;
			npc.addEventListener("input", () => {
				field.npc = npc.value;
				updateCode();
			});
			npcLabel.appendChild(npc);

			const h2 = document.createElement("h2");
			h2.textContent = "Additional Rules";
			editor.appendChild(h2);

			for (let rule of field.rules) {
				editor.appendChild(createGameModeEditor(rule, field.rules));
			}

			const addRule = document.createElement("div");
			addRule.classList.add("game-mode-add-field");
			editor.appendChild(addRule);

			const newFieldSelect = document.createElement("select");
			for (let { name, field } of gameModeFieldNames) {
				if (field !== SpyCards.GameModeFieldType.Variant) {
					newFieldSelect.appendChild(SpyCards.UI.option(name, String(field)));
				}
			}

			const newFieldLabel = document.createElement("label");
			newFieldLabel.textContent = "Data Type: ";
			newFieldLabel.appendChild(newFieldSelect);
			addRule.appendChild(newFieldLabel);
			const newFieldButton = SpyCards.UI.button("Add Rule", [], () => {
				const type: SpyCards.GameModeFieldType = parseInt(newFieldSelect.value, 10);
				const rule = SpyCards.GameModeFieldConstructors[type]();
				field.rules.push(rule);
				editor.insertBefore(createGameModeEditor(rule, field.rules), addRule);
				updateCode();
			});
			addRule.appendChild(newFieldButton);
		} else if (field instanceof SpyCards.GameModeUnfilterCard) {
			legend.textContent = "Unfilter Card";
			const explanation = document.createElement("p");
			explanation.textContent = "Card will not be targetable by filters (eg. tribe, rank), but can still be targeted by effects that name the card directly.";
			editor.appendChild(explanation);

			const cardLabel = document.createElement("label");
			cardLabel.classList.add("no-flip");
			cardLabel.textContent = "Card: ";
			const cardDef = customDefs.cardsByID[field.card];
			if (!cardDef) {
				// custom card was deleted; remove summon card rule
				deleteButton.click();
				return editor;
			}
			let card = cardDef.createEl(customDefs);
			card.id = SpyCards.UI.uniqueID();
			cardLabel.htmlFor = card.id;
			card.classList.add("clickable");
			card.addEventListener("click", async function onClick() {
				const toSummon = await modalSelectCard("Which card?", customDefs.allCards);
				if (!toSummon) {
					return;
				}
				field.card = toSummon.id;
				const newCardEl = toSummon.createEl(customDefs);
				newCardEl.id = card.id;
				newCardEl.classList.add("clickable");
				newCardEl.addEventListener("click", onClick);
				SpyCards.UI.replace(card, newCardEl);
				card = newCardEl;
				updateCode();
			});
			cardLabel.appendChild(card);
			editor.appendChild(cardLabel);
		} else if (field instanceof SpyCards.GameModeDeckLimitFilter) {
			legend.textContent = "Deck Limit Filter";

			const countLabel = document.createElement("label");
			countLabel.textContent = "Max. Count: ";
			editor.appendChild(countLabel);
			const count = document.createElement("input");
			count.type = "number";
			count.min = "0";
			count.valueAsNumber = field.count;
			count.addEventListener("input", () => {
				if (count.reportValidity()) {
					field.count = count.valueAsNumber;
				}
				updateCode();
			});
			countLabel.appendChild(count);

			let updateCardEnabled: () => void;

			const rankLabel = document.createElement("label");
			rankLabel.textContent = "Rank: ";
			editor.appendChild(rankLabel);
			const rank = document.createElement("select");
			rank.appendChild(SpyCards.UI.option("(any)", String(SpyCards.Rank.None)));
			rank.appendChild(SpyCards.UI.option("Attacker", String(SpyCards.Rank.Attacker)));
			rank.appendChild(SpyCards.UI.option("Effect", String(SpyCards.Rank.Effect)));
			rank.appendChild(SpyCards.UI.option("Enemy (Attacker or Effect)", String(SpyCards.Rank.Enemy)));
			rank.appendChild(SpyCards.UI.option("Mini-Boss", String(SpyCards.Rank.MiniBoss)));
			rank.appendChild(SpyCards.UI.option("Boss", String(SpyCards.Rank.Boss)));
			rank.value = String(field.rank);
			rank.addEventListener("input", () => {
				field.rank = parseInt(rank.value, 10);
				updateCode();
				updateCardEnabled();
			});
			rankLabel.appendChild(rank);

			const tribeLabel = document.createElement("label");
			tribeLabel.textContent = "Tribe: ";
			editor.appendChild(tribeLabel);
			const [tribe] = createTribeSelect(false, "(any)");
			tribe.addEventListener("input", () => {
				field.tribe = tribe.value[0] === "?" ? SpyCards.Tribe.Custom : parseInt(tribe.value, 10);
				field.customTribe = tribe.value[0] === "?" ? tribe.value.substr(1) : "";
				updateCode();
				updateCardEnabled();
			});
			tribe.value = field.tribe === SpyCards.Tribe.Custom ? "?" + field.customTribe : String(field.tribe);
			tribeLabel.appendChild(tribe);

			const cardLabel = document.createElement("label");
			cardLabel.textContent = "Specific Card: ";
			editor.appendChild(cardLabel);
			const card = createCardSelect(field.card, (id) => field.card = id);
			cardLabel.appendChild(card);

			updateCardEnabled = () => {
				cardLabel.style.display = field.rank === SpyCards.Rank.None && field.tribe === SpyCards.Tribe.None ? "block" : "none";
			};
			updateCardEnabled();
		} else if (field instanceof SpyCards.GameModeTimer) {
			legend.textContent = "Timer";

			for (let { name, key } of <{ name: string, key: "startTime" | "maxTime" | "perTurn" | "maxPerTurn" }[]>[
				{ name: "Initial Stored Time", key: "startTime" },
				{ name: "Maximum Stored Time", key: "maxTime" },
				{ name: "Store Time Per Turn", key: "perTurn" },
				{ name: "Maximum Time Per Turn", key: "maxPerTurn" },
			]) {
				const label = document.createElement("label");
				label.textContent = name + ": ";
				const input = document.createElement("input");
				input.type = "number";
				input.min = "1";
				input.valueAsNumber = field[key];
				input.addEventListener("input", () => {
					if (!input.reportValidity()) {
						return;
					}

					field[key] = input.valueAsNumber;
					updateCode();
				});
				label.appendChild(input);
				editor.appendChild(label);
			}

			const warning = document.createElement("p");
			warning.classList.add("warning");
			warning.textContent = "Only the first Timer field has any effect.";
			editor.appendChild(warning);
		}
		legend.insertBefore(deleteButton, legend.firstChild);
		return editor;

		function appendBannedCard(bannedCards: HTMLUListElement, showAllVanilla: () => void, ids: number[], id: number) {
			const li = document.createElement("li");
			const deleteButton = SpyCards.UI.button("Unban", ["delete"], () => {
				ids.splice(ids.indexOf(id), 1);
				SpyCards.UI.remove(li);
				if (!ids.length) {
					showAllVanilla();
				}
				updateCode();
			});
			const card = customDefs.cardsByID[id];
			li.appendChild(card ? card.createEl(customDefs) : document.createTextNode("Unknown card #" + id));
			li.appendChild(deleteButton);
			bannedCards.appendChild(li);
		}
	}

	function createTribeSelect(allowCustom: boolean, noneText: string): [HTMLSelectElement, () => void] {
		const sel = document.createElement("select");
		function reset() {
			const origValue = sel.value;
			SpyCards.UI.clear(sel);
			if (noneText) {
				sel.appendChild(SpyCards.UI.option(noneText, String(SpyCards.Tribe.None)));
			}
			for (let tribe = 0; tribe < SpyCards.Tribe.Custom; tribe++) {
				sel.appendChild(SpyCards.UI.option(SpyCards.tribeName(tribe, ""), String(tribe)));
			}
			if (!customTribes) {
				customTribes = [];
				for (let custom of customCards) {
					for (let tribe of custom.card.tribes) {
						if (tribe.tribe === SpyCards.Tribe.Custom) {
							customTribes.push(tribe.custom);
						}
					}
				}
				customTribes = customTribes.filter((v, i, a) => a.findIndex((t) => t.name === v.name) === i);
			}
			for (let tribe of customTribes) {
				const opt = SpyCards.UI.option(tribe.name, "?" + tribe.name);
				opt.setAttribute("data-custom-color", String(tribe.rgb));
				sel.appendChild(opt);
			}
			if (allowCustom) {
				sel.appendChild(SpyCards.UI.option("(custom)", "!"));
			}
			sel.value = origValue;
		}
		reset();
		sel.addEventListener("focus", () => reset());
		return [sel, reset];
	}
	function createCardSelect(id: SpyCards.CardData.CardID, update: (id: SpyCards.CardData.CardID) => void) {
		const cardSel = document.createElement("select");
		const onIdle = window.requestIdleCallback ? requestIdleCallback : (f: () => void) => setTimeout(f, 1);
		onIdle(() => {
			for (let c of customDefs.allCards.slice(0).sort((a, b) => {
				const an = a.displayName();
				const bn = b.displayName();
				if (an === bn) {
					return a.id - b.id;
				}
				return an < bn ? -1 : 1;
			})) {
				cardSel.appendChild(SpyCards.UI.option(c.displayName(), String(c.id)));
			}
			cardSel.value = String(id);
			cardSel.addEventListener("input", () => {
				update(parseInt(cardSel.value, 10));
			});
		});
		return cardSel;
	}
}
