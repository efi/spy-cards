package npc

import (
	"math"

	"git.lubar.me/ben/spy-cards/card"
)

// TourneyPlayer2 is a more advanced card-playing algorithm.
//
// Currently, it implements a more complex method of deck building, but
// uses the default round-to-round logic.
type TourneyPlayer2 struct {
	TourneyPlayer

	AdjustWeight func(*card.Def, float32, card.Deck, *card.Set) float32
}

func (t *TourneyPlayer2) countSynergies(set *card.Set, source, target *card.Def, perCard, perTribe float32) float32 {
	// for now, assume all synergies are good
	var (
		count  = float32(0)
		cards  = []card.ID{target.ID}
		tribes = make([]card.TribeDef, 0, len(target.Tribes))
	)

	tribes = append(tribes, target.Tribes...)

	for _, e := range target.Effects {
		if e.Type == card.CondCoin || e.Type == card.CondLimit {
			e = e.Result
		}

		if e.Type == card.EffectSummon && e.Flags&card.FlagSummonOpponent == 0 {
			for i := int64(0); i < e.Amount; i++ {
				if e.Filter.IsSingleCard() {
					cards = append(cards, e.Filter[0].CardID)

					tribes = append(tribes, set.Card(e.Filter[0].CardID).Tribes...)
				} else {
					for _, fc := range e.Filter {
						if fc.Type == card.FilterTribe {
							tribes = append(tribes, card.TribeDef{
								Tribe:      fc.Tribe,
								CustomName: fc.CustomTribe,
							})
						}
					}
				}
			}
		}
	}

	for _, e := range source.Effects {
		mul := float32(1)

		if e.Type == card.CondLimit && e.Flags&card.FlagCondLimitGreaterThan == 0 && source.ID != target.ID {
			e = e.Result
		}

		if (e.Type == card.EffectEmpower && e.Flags&card.FlagStatOpponent == 0) || (e.Type == card.CondCard && e.Flags&card.FlagCondCardOpponent == 0) {
			if e.Type == card.CondCard {
				mul /= float32(e.Amount)
			}

			if e.Filter.IsSingleCard() {
				for _, c := range cards {
					if c == e.Filter[0].CardID {
						count += perCard * mul
					}
				}
			} else {
				for _, fc := range e.Filter {
					if fc.Type == card.FilterTribe {
						for _, t := range tribes {
							if t.Tribe == fc.Tribe && t.CustomName == fc.CustomTribe {
								count += perTribe * mul
							}
						}
					}
				}
			}
		}
	}

	return count
}

func (t *TourneyPlayer2) selectCard(d card.Deck, set *card.Set, choices []*card.Def) card.ID {
	weights := make([]float32, len(choices))

	for i, c := range choices {
		if c.ID != card.TheEverlastingKing {
			weights[i] = 1
		}
	}

	for i, c := range choices {
		var statTotal float32

		unprocessedEffects := append([]*card.EffectDef(nil), c.Effects...)
		for len(unprocessedEffects) != 0 {
			e := unprocessedEffects[0]
			unprocessedEffects = unprocessedEffects[1:]

			// numb is very powerful
			if e.Type == card.EffectNumb && e.Flags&card.FlagNumbSelf == 0 {
				stat := float32(e.Amount) * 2
				if e.Flags&card.FlagNumbInfinity != 0 || stat > 5 {
					stat = 5
				}

				statTotal += stat
			}

			if (e.Type == card.EffectStat || e.Type == card.EffectRawStat || (e.Type == card.EffectEmpower && !e.Filter.IsSingleCard())) && e.Flags&card.FlagStatOpponent == 0 && e.Amount > 0 {
				statTotal += float32(e.Amount)
			}

			if e.Type == card.EffectSummon && e.Filter.IsSingleCard() && e.Flags&card.FlagSummonOpponent == 0 {
				for i := int64(0); i < e.Amount; i++ {
					unprocessedEffects = append(unprocessedEffects, set.Card(e.Filter[0].CardID).Effects...)
				}
			}

			if e.Type == card.CondCoin &&
				(e.Result.Type == card.EffectStat || e.Result.Type == card.EffectRawStat) && e.Result.Flags&card.FlagStatOpponent == 0 && e.Result.Amount > 0 &&
				(e.Result2 == nil || ((e.Result2.Type == card.EffectStat || e.Result2.Type == card.EffectRawStat) && e.Result2.Flags&card.FlagStatOpponent == 0 && e.Result2.Amount > 0)) {
				statTotal += float32(e.Result.Amount*e.Amount) / 2
				if e.Result2 != nil {
					statTotal += float32(e.Result2.Amount*e.Amount) / 2
				}
			}
		}

		// avoid division by 0
		tp := float32(c.TP)
		if tp < 0.1 {
			tp = 0.1
		}

		// (vanilla) cards that have a nonzero raw stat total that is lower than their TP are weak
		if statTotal != 0 {
			weights[i] *= statTotal / tp
		}

		// don't add too many cards with high TP
		if tp < 5 {
			weights[i] *= 5 / tp
		}

		perCard := float32(1)
		perTribe := float32(1)

		if c.Rank == card.MiniBoss {
			perCard = 3
			perTribe = 2
		} else if c.Rank == card.Boss {
			perTribe = 2
		}

		for _, id := range d {
			dc := set.Card(id)

			// favor cards that have synergies with existing cards
			var synergies float32
			synergies += t.countSynergies(set, c, dc, perCard, perTribe)
			synergies += t.countSynergies(set, dc, c, perCard, perTribe)
			weights[i] *= float32(math.Pow(7.5, float64(synergies)))

			// try not to pick too many cards with the same TP cost
			if c.TP == dc.TP {
				weights[i] *= 0.25
			}

			// favor duplicates over non-duplicates
			if c.ID == id {
				same := 0

				for _, j := range d {
					if j == id {
						same++
					}
				}

				// one duplicate    = 2.50
				// two duplicates   = 1.56
				// three duplicates = 0.57
				// four duplicates  = 0.15
				weights[i] *= 2.5 / float32(same)
			}
		}
	}

	var totalWeight float32

	for i, c := range choices {
		weights[i] = t.AdjustWeight(c, weights[i], d, set)
		if weights[i] < 0 {
			weights[i] = 0
		}

		totalWeight += weights[i]
	}

	choice := t.rng().Float32() * totalWeight
	for i, c := range choices {
		choice -= weights[i]
		if choice < 0 {
			return c.ID
		}
	}

	// rounding error, probably
	return choices[len(choices)-1].ID
}

// CreateDeck creates a deck based on weights and card synergies.
func (t *TourneyPlayer2) CreateDeck(set *card.Set) card.Deck {
	rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	d := make(card.Deck, rules.CardsPerDeck)

	for i := range d {
		switch {
		case i < int(rules.BossCards):
			d[i] = t.selectCard(d[:i], set, set.ByBack(card.Boss, d[:i]))
		case i < int(rules.BossCards+rules.MiniBossCards):
			d[i] = t.selectCard(d[:i], set, set.ByBack(card.MiniBoss, d[:i]))
		default:
			d[i] = t.selectCard(d[:i], set, set.ByBack(card.Enemy, d[:i]))
		}
	}

	return d
}

func genericNPCWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	return weight
}

// bu-gi likes attacker cards.
func buGiWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	if c.Rank == card.Attacker {
		return weight * 2.5
	}

	return weight
}

// carmina favors cards with random chances on them.
func carminaWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	for _, e := range c.Effects {
		if e.Type == card.CondCoin || (e.Type == card.EffectSummon && !e.Filter.IsSingleCard()) {
			return weight * 10
		}
	}

	return weight
}

// janet.
func janetWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	if c.ID != card.TheEverlastingKing {
		return weight
	}

	return float32(len(set.ByBack(card.Boss, d)))
}

// johnny likes cards with non-random conditions, and (mini-)bosses with empower.
func johnnyWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	for _, e := range c.Effects {
		if e.Type == card.EffectEmpower && (c.Rank == card.MiniBoss || c.Rank == card.Boss) {
			return weight * 5
		}

		if e.Type != card.CondCoin && e.Type != card.CondLimit && e.Type >= 128 {
			if e.Type == card.CondCard {
				return weight * 5 / float32(e.Amount)
			}

			return weight * 5
		}
	}

	return weight
}

// kage likes cards with multiple tribes and cards with empower or unity on them.
func kageWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	weight *= float32(len(c.Tribes))/2 + 0.5

	for _, e := range c.Effects {
		if e.Type == card.CondLimit && e.Flags&card.FlagCondLimitGreaterThan == 0 {
			e = e.Result
		}

		if e.Type == card.EffectEmpower && e.Flags&card.FlagStatOpponent == 0 && e.Amount > 0 {
			return weight * 2
		}
	}

	return weight
}

// ritchee prefers high-cost cards.
func ritcheeWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	tp := float32(c.TP)

	return weight * tp * tp * tp
}

// serene likes cards with low TP costs.
func sereneWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	if c.TP > 3 {
		tp := float32(c.TP)
		weight *= 27 / tp / tp / tp
	}

	return weight
}

// chuck loves seedlings.
func chuckWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	for _, t := range c.Tribes {
		if t.Tribe == card.TribeSeedling {
			return weight * 25
		}
	}

	return weight
}

// arie favors cards that cost near to 2 TP, and dislikes cards
// that are fungi, or that are bug plus another tribe.
func arieWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	if c.TP == 2 {
		weight *= 3
	} else {
		weight *= float32(math.Pow(1/(1+math.Abs(float64(2-c.TP))), 4))
	}

	for _, t := range c.Tribes {
		if t.Tribe == card.TribeFungi {
			weight /= 10

			break
		}

		if t.Tribe == card.TribeBug && len(c.Tribes) > 1 {
			weight /= 10

			break
		}
	}

	return weight
}

// shay likes Thugs and cards with numb.
func shayWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	for _, t := range c.Tribes {
		if t.Tribe == card.TribeThug {
			if c.Rank == card.MiniBoss || c.Rank == card.Boss {
				return weight * 25
			}

			return weight * 7.5
		}
	}

	for _, e := range c.Effects {
		if e.Type == card.CondCoin {
			e = e.Result
		}

		if e.Type == card.EffectNumb {
			return weight * 5
		}
	}

	return weight
}

// crow likes (non-boss) ??? and (all) Bot cards below 6 TP,
// and cards with both ATK and a non-card-based condition.
func crowWeight(c *card.Def, weight float32, d card.Deck, set *card.Set) float32 {
	if c.TP < 6 {
		for _, t := range c.Tribes {
			if (t.Tribe == card.TribeUnknown && c.Rank != card.Boss) || t.Tribe == card.TribeBot {
				return weight * 30
			}
		}
	}

	if c.Rank == card.Boss {
		return weight / 100
	}

	haveCondition, haveATK := false, false

	for _, e := range c.Effects {
		if e.Type >= 128 && e.Type != card.CondCard {
			haveCondition = true
		}

		if (e.Type == card.EffectStat || e.Type == card.EffectRawStat) && e.Flags&(card.FlagStatDEF|card.FlagStatOpponent) == 0 && e.Amount > 0 {
			haveATK = true
		}

		if haveCondition && haveATK {
			return weight * 40
		}
	}

	return weight
}
