package spoilerguard

/*
	TODO:
	function requestDataFile(): Promise<File> {
		const upload = document.createElement("input");
		upload.type = "file";
		upload.accept = ".dat";
		const filePromise = new Promise<File>((resolve, reject) => {
			upload.addEventListener("input", function (e) {
				if (upload.files.length) {
					resolve(upload.files[0]);
				} else {
					reject(new Error("no file selected"));
				}
			});
		});
		upload.click();
		return filePromise;
	}

	async function onFile(f: File) {
		if (["save0.dat", "save1.dat", "save2.dat"].indexOf(f.name) === -1) {
			// TODO: confirm user intended this file
			debugger;
		}

		const data = await f.text();
		const parsed = parseSaveData(data);
		localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(parsed);
		updateSpoilerGuardState();
	}

	const enableButtons = document.querySelectorAll(".enable-spoiler-guard");
	if (enableButtons.length) {
		document.addEventListener("dragover", function (e) {
			e.preventDefault();

			e.dataTransfer.dropEffect = "copy";
		});
		document.addEventListener("drop", function (e) {
			e.preventDefault();

			onFile(e.dataTransfer.files[0]);
		});
		enableButtons.forEach((btn) => {
			btn.addEventListener("click", function (e) {
				e.preventDefault();

				const form = document.createElement("div");
				form.classList.add("readme", "spoiler-guard-form");
				form.setAttribute("role", "form");
				form.setAttribute("aria-live", "assertive");
				document.body.appendChild(form);
				surveySaveData(form).then((surveyData) => {
					SpyCards.UI.remove(form);

					if (surveyData === null) {
						requestDataFile().then((f) => onFile(f));
					} else {
						localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(surveyData);
						updateSpoilerGuardState();
					}
				});
			});
		});
	}
	const disableButton = document.querySelector(".disable-spoiler-guard");
	if (disableButton) {
		disableButton.addEventListener("click", function (e) {
			e.preventDefault();

			delete localStorage["spy-cards-spoiler-guard-v0"];
			updateSpoilerGuardState();
		});
	} else if (cannotPlayState[getSpoilerGuardState()]) {
		location.href = "spoiler-guard.html";
	}

	function updateSpoilerGuardState() {
		const data = getSpoilerGuardData();
		const isEnabled = data !== null;
		const state = getSpoilerGuardState();

		SpyCards.UI.html.classList.toggle("room-pr", data && data.m && (data.m & 8) !== 0);

		document.querySelectorAll<HTMLElement>(".spoiler-guard-disabled, .spoiler-guard-enabled, [data-spoiler-guard-state]").forEach((el) => {
			el.hidden = false;
			if (el.classList.contains("spoiler-guard-disabled")) {
				el.hidden = isEnabled;
			}
			if (el.classList.contains("spoiler-guard-enabled")) {
				el.hidden = !isEnabled;
			}
			if (el.hidden) {
				return;
			}

			const stateRaw = el.getAttribute("data-spoiler-guard-state");
			if (!stateRaw) {
				return;
			}
			const expectedState = <GuardState>parseInt(stateRaw, 10);
			el.hidden = expectedState !== state;
		});

		document.querySelectorAll<HTMLElement>(".spoiler-guard-deck").forEach((el) => {
			if (!data || !data.d) {
				el.hidden = true;
				return;
			}

			el.hidden = false;
			el.querySelectorAll("a").forEach((a) => {
				a.href = "decks.html#" + data.d;
			});
		});
	}
	setTimeout(updateSpoilerGuardState, 1);
*/
