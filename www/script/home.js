"use strict";
const availableCards = document.querySelector("#available-cards");
if (availableCards) {
    const defs = new SpyCards.CardDefs();
    SpyCards.SpoilerGuard.banSpoilerCards(defs, new Uint8Array(0));
    for (let card of [
        ...defs.cardsByBack[2],
        ...defs.cardsByBack[1],
        ...defs.cardsByBack[0]
    ]) {
        if (defs.banned[card.id]) {
            continue;
        }
        const el = card.createEl(defs);
        el.style.cursor = "pointer";
        el.setAttribute("role", "button");
        el.setAttribute("aria-expanded", "false");
        SpyCards.flipCard(el, true);
        const flip = () => {
            SpyCards.flipCard(el);
            el.setAttribute("aria-expanded", String(!el.classList.contains("flipped")));
        };
        el.addEventListener("click", flip);
        el.addEventListener("keydown", (e) => {
            if (SpyCards.disableKeyboard) {
                return;
            }
            if (e.code === "Space" || e.code === "Enter") {
                e.preventDefault();
                flip();
            }
        });
        el.tabIndex = 0;
        availableCards.appendChild(el);
        availableCards.appendChild(document.createTextNode(" "));
    }
}
function flipCardsIfViewingAvailable() {
    if (location.hash !== "#available-cards") {
        return;
    }
    const cards = document.querySelectorAll(".card");
    if (!cards.length) {
        return;
    }
    if (getComputedStyle(cards[0]).transitionDuration === "0s") {
        // prefers-reduced-motion
        cards.forEach(function (c) {
            SpyCards.flipCard(c, false);
            c.setAttribute("aria-expanded", "true");
        });
        return;
    }
    cards.forEach(function (c, i) {
        if (!c.classList.contains("flipped")) {
            return;
        }
        setTimeout(function () {
            SpyCards.flipCard(c, false);
            c.setAttribute("aria-expanded", "true");
        }, Math.pow(i, 0.75) * 200);
    });
}
addEventListener("hashchange", flipCardsIfViewingAvailable);
flipCardsIfViewingAvailable();
