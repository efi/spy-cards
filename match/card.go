//go:generate stringer -type CardMode -trimprefix Mode

// Package match implements gameplay logic for Spy Cards Online.
package match

import (
	"git.lubar.me/ben/spy-cards/card"
)

// Card is an instance of a card in the game.
type Card struct {
	Set  *card.Set
	Def  *card.Def
	Back card.Rank
}

// HandCard has additional data for cards currently in a player's hand.
type HandCard struct {
	*Card
	InHandTurns int64
	Temporary   bool
}

// CardMode is the reason a card was played.
type CardMode uint64

// Constants for CardMode.
const (
	ModeDefault CardMode = iota
	ModeSetup
	ModeSetupOriginal
	ModeSummoned
	ModeInvisibleSummoned
	ModeNumb
	ModeInHand
)

// Ignore returns true if cards with this mode should not be considered to be
// "on the field".
func (m CardMode) Ignore() bool {
	return m == ModeSetup || m == ModeSetupOriginal || m == ModeNumb || m == ModeInHand
}

// Hide returns true if cards with this mode should be hidden from players.
func (m CardMode) Hide() bool {
	return m == ModeInvisibleSummoned || m == ModeInHand
}

// Summoned returns true if cards with this mode were summoned by an effect.
func (m CardMode) Summoned() bool {
	return m == ModeSummoned || m == ModeInvisibleSummoned
}

// ActiveCard is a a card that's on the field.
type ActiveCard struct {
	Card         *Card
	Mode         CardMode
	Desc         *card.RichDescription
	Effects      []*card.EffectDef
	UnNumb       int64
	ATK          Number
	DEF          Number
	ActiveEffect *card.EffectDef
}
