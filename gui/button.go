package gui

type AbstractButton interface {
	action(Event)
}

type Button struct {
	Panel
	Action func(Event)
	hover  bool
}

func (b *Button) onClick() {
	if b.mouseOnMe() {
		b.Focus()
		b.action(Event{})
	} else {
		Handler(b.Parent).Propagate(Event{})
	}
}

func (b *Button) action(e Event) {
	b.Action(e)
}

func NewButton(t string) *Button {
	b := &Button{
		Panel: *NewPanel(),
	}
	b.Control.update = func() {
		if b.mouseOnMe() {
			b.hover = true
		} else {
			b.hover = false
		}
	}
	b.AddChild(&NewLabel(t).Control)
	b.node = b
	return b
}

func (b *Button) mouseOnMe() bool {
	x, y, c := b.root.mouse.x, b.root.mouse.y, b.root.mouse.c
	return c == true &&
		x > b.x &&
		x < b.x+b.w &&
		y > b.y &&
		y < b.y+b.h
}
