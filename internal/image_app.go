// +build !js !wasm

package internal

import (
	"bytes"
	"hash/crc32"
	"image"
	"image/jpeg"
	"image/png"
)

func init() {
	// Hide a false-positive data race.
	// https://golang.org/issues/41911
	_ = crc32.MakeTable(crc32.Castagnoli)
}

// DecodePNG allows PNG decoding to be implemented differently per platform.
func DecodePNG(b []byte, grayscale bool) (image.Image, error) {
	return png.Decode(bytes.NewReader(b))
}

// DecodeJPEG allows JPEG decoding to be implemented differently per platform.
func DecodeJPEG(b []byte) (image.Image, error) {
	return jpeg.Decode(bytes.NewReader(b))
}
