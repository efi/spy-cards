// +build !headless

package room

import (
	"context"
	"image/color"
	"math"
	"sort"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

type stage struct {
	tick  uint64
	cam   gfx.Camera
	batch *sprites.Batch
	stage *customStage

	audience  []*Audience
	players   [2]*Player
	player    uint8
	disable3D bool
}

var stageCache = internal.Cache{MaxEntries: 32}

func loadStageFromCache(ctx context.Context, cid card.ContentIdentifier) (*customStage, error) {
	path := "img/room/stage.stage"
	if cid != nil {
		path = internal.GetConfig(ctx).IPFSBaseURL + cid.String()
	}

	v, err := stageCache.Do(path, func() (interface{}, error) {
		go func() {
			_ = sprites.CardBattle.Preload()
			_ = sprites.Audience[0][AudienceFront].Preload()
			_ = sprites.Audience[0][AudienceBackBlur1].Preload()
		}()

		return loadStage(ctx, path)
	})
	if err != nil {
		return nil, xerrors.Errorf("room: loading stage: %w", err)
	}

	cs, _ := v.(*customStage)

	return cs, nil
}

// PreloadStage loads a stage into the cache.
func PreloadStage(ctx context.Context, cid card.ContentIdentifier) error {
	_, err := loadStageFromCache(ctx, cid)

	return err
}

// NewStage creates a Spy Cards Online tournament stage.
func NewStage(ctx context.Context, cid card.ContentIdentifier) Room {
	r := &stage{}
	r.cam.SetDefaults()

	r.disable3D = internal.LoadSettings().Disable3D

	if !r.disable3D {
		s, err := loadStageFromCache(ctx, cid)
		if err != nil {
			panic(err)
		}

		r.stage = s
	}

	return r
}

func (r *stage) Update() {
	w, h := gfx.Size()

	gfx.GL.Viewport(0, 0, w, h)

	if r.disable3D {
		return
	}

	r.tick++
	s := math.Sin(float64(r.tick) * math.Pi / 30 / 140)
	c := math.Cos(float64(r.tick) * math.Pi / 30 / 140)

	for _, a := range r.audience {
		a.Update()
	}

	for _, p := range r.players {
		if p != nil {
			p.Update()
		}
	}

	aspect := float32(w) / float32(h)
	sx, sy := float32(1.0), float32(1.0)

	if r.player == 2 {
		sx = -sx
	}

	if aspect > 16.0/9.0 {
		sy = aspect * 9.0 / 16.0
		aspect = 16.0 / 9.0
	}

	r.cam.Perspective = gfx.MultiplyMatrix(
		gfx.Perspective(-45*math.Pi/180, aspect, 0.3, 150),
		gfx.Scale(sx, sy, 1),
	)
	r.cam.Position = gfx.Translation(5, 0, 0)
	r.cam.Offset = gfx.Translation(0, 2.5, -7.5)
	r.cam.Rotation = gfx.MultiplyMatrix(
		gfx.RotationX(float32((12+4*c)*math.Pi/180)),
		gfx.RotationY(float32((5*s)*math.Pi/180)),
	)
}

func (r *stage) Render() {
	if r.disable3D {
		gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
		gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

		return
	}

	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	if r.stage != nil {
		r.stage.Render(&r.cam)
	}

	if r.batch == nil {
		r.batch = sprites.NewBatch(&r.cam)
	} else {
		r.batch.Reset(&r.cam)
	}

	for _, a := range r.audience {
		if a.Z < 0 {
			break
		}

		a.Render(r.batch)
	}

	for _, p := range r.players {
		if p != nil {
			p.Render(r.batch, sprites.FlagNoDiscard, 0, 0, 0)
		}
	}

	for _, a := range r.audience {
		if a.Z >= 0 {
			continue
		}

		a.Render(r.batch)
	}

	r.batch.Append(sprites.CardBattle, 5, 3.5, -2.5, 0.85, 0.85, color.RGBA{68, 68, 68, 51}, sprites.FlagNoDiscard, 0, 0, 0)

	r.batch.Render()
}

func (r *stage) SetPlayer(i int) {
	if i < 0 || i > 2 {
		i = 0
	}

	r.player = uint8(i)
}

func (r *stage) AddPlayer(p *Player, i int) {
	if i != 1 && i != 2 {
		panic(xerrors.Errorf("room: unexpected index for stage player (expected 1 or 2): %d", i))
	}

	if i == 1 {
		p.X = 3
		p.Flip = true
	} else {
		p.X = 7
		p.Flip = false
	}

	p.Y = 0.5
	p.Z = 1

	r.players[i-1] = p
}

func (r *stage) AddAudience(a ...*Audience) {
	for _, member := range a {
		if member.Z < 0 {
			member.Y = 0
			member.Blur = member.Z*-0.025 + 0.025
		} else {
			member.Y = 0.5
			member.Blur = 0
		}

		r.audience = append(r.audience, member)
	}

	sort.Slice(r.audience, func(i, j int) bool {
		return r.audience[i].Z > r.audience[j].Z
	})
}

func (r *stage) SetStage(ctx context.Context, cid card.ContentIdentifier) error {
	s, err := loadStageFromCache(ctx, cid)
	if err != nil {
		return err
	}

	r.stage = s

	return nil
}
