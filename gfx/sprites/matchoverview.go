package sprites

import (
	"encoding/binary"
	"image/color"

	"golang.org/x/xerrors"
)

var matchOverviewData = [4][4]uint8{
	{0, 116, 0, 0},
	{0, 76, 0, 1},
	{1, 140, 1, 1},
	{1, 180, 1, 0},
}

// MatchOverview renders the versus screen for the start of a
// Spy Cards Online recording.
func (sb *Batch) MatchOverview(x, y, z, sx, sy float32, flip bool) {
	tex := ^uint16(0)

	for i := range sb.texture {
		if sb.texture[i] == nil {
			sb.texture[i] = Blank.AssetTexture
		}

		if sb.texture[i] == Blank.AssetTexture {
			tex = uint16(i)

			break
		}
	}

	if tex > uint16(len(sb.texture)) {
		panic(xerrors.New("sprite: too many sprite sheets in batch"))
	}

	var data batchData

	data.X[0] = -sx
	data.X[1] = sx
	data.Y[0] = -sy
	data.Y[1] = sy
	data.Pos[0] = x
	data.Pos[1] = y
	data.Pos[2] = z
	data.Texture = tex

	if flip {
		data.Color = color.RGBA{54, 41, 27, 204}
	} else {
		data.Color = color.RGBA{27, 41, 54, 204}
	}

	sb.appendData(spriteDataShared[0], &data)
	sb.appendData(matchOverviewData[0], &data)
	sb.appendData(matchOverviewData[3], &data)
	sb.appendData(spriteDataShared[3], &data)

	data.Color = color.RGBA{0, 0, 0, 204}

	sb.appendData(matchOverviewData[0], &data)
	sb.appendData(matchOverviewData[1], &data)
	sb.appendData(matchOverviewData[2], &data)
	sb.appendData(matchOverviewData[3], &data)

	if flip {
		data.Color = color.RGBA{27, 41, 54, 204}
	} else {
		data.Color = color.RGBA{54, 41, 27, 204}
	}

	sb.appendData(matchOverviewData[1], &data)
	sb.appendData(spriteDataShared[1], &data)
	sb.appendData(spriteDataShared[2], &data)
	sb.appendData(matchOverviewData[2], &data)

	for i := 0; i < 3; i++ {
		var elems [6 * 2]uint8

		for j, n := range spriteElem {
			binary.LittleEndian.PutUint16(elems[j*2:], sb.index+n)
		}

		sb.elem = append(sb.elem, elems[:]...)

		sb.index += 4
		sb.count += 6
	}

	sb.dirtyBuf = true

	sb.Append(Blank, x-sx-0.05, y, z, 0.1, sy*2, Black, 0, 0, 0, 0)
	sb.Append(Blank, x+sx+0.05, y, z, 0.1, sy*2, Black, 0, 0, 0, 0)
	sb.Append(Blank, x, y-sy-0.05, z, sx*2+0.2, 0.1, Black, 0, 0, 0, 0)
	sb.Append(Blank, x, y+sy+0.05, z, sx*2+0.2, 0.1, Black, 0, 0, 0, 0)
}
