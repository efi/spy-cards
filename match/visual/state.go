//go:generate stringer -type VisualState -trimprefix State
//go:generate stringer -type EditorState -trimprefix Editor

package visual

type VisualState uint64

const (
	StateHome VisualState = iota
	StateOnHost
	StateOnJoin
	StateOnQuickJoin
	StateOnVariant
	StateConnecting
	StateWait
	StateCharacterSelect
	StateDeckSelect
	StateReadyToStart
	StateNextRound
	StateNextRoundWait
	StateTurnSelect
	StateTurnProcess
	StateTurnEnd
	StateMatchCompleteWait
	StateMatchComplete
	StatePlaybackInit
	StatePlaybackIdle
	StatePlaybackNext
	StatePlaybackTurnWait
	StateInCardEditor
)

type EditorState uint64

const (
	EditorInit EditorState = iota
	EditorSelect
	EditorCard
	EditorCardChoices
	EditorGameMode
)
