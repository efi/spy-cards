// +build !js !wasm
// +build !headless

package visual

import "context"

func displayCopyableText(ctx context.Context, label, text string) {
	panic("TODO: displayCopyableText") // TODO
}

func displayPastableText(ctx context.Context, label string, f func(string)) {
	panic("TODO: displayPastableText") // TODO
}

func (v *Visual) onModeChange() {
	// do nothing
}
