---
title: 0.3 TODO list
...

## Spoiler Guard

- [ ] Survey
- [ ] Save file upload
- [x] Save file parsing
- [ ] Menu codes
    - [ ] H\*\*\*E\*\*
    - [ ] M\*\*\*F\*\*\*
    - [ ] F\*\*\*E\*\*\*
    - [ ] M\*\*\*E\*\*\*
    - [ ] R\*\*\*E\*
    - [x] P\*\*\*R\*\*\*
    - [ ] T\*\*\*B\*\*\*
- [x] Ban/unpickable
    - [x] Match recording changes

## Automated Testing

- [x] Flower Journeey
- [x] Mite Knight
- [x] Spy Cards

## UI

- [ ] Game Mode Info
- [ ] Play Online
    - [x] Host/Join
    - [x] Quick Join
    - [x] Singleplayer
    - [x] Character Select
    - [x] Deck Picker
    - [x] Deck Builder
    - [x] Select Play
    - [ ] Game Log
    - [x] Effect Processor
    - [ ] End of Match
    - [ ] Rematch
- [ ] Deck Editor
- [ ] Card Editor
- [ ] Match Viewer
    - [ ] Code Entry
    - [x] Intro Screen
    - [ ] Scrubber
- [x] Room
    - [x] Mine
    - [x] Stage
- [x] Extension: SetStage
- [x] Extension: SetMusic
- [x] NPC Players
