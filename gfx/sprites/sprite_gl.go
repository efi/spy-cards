// +build !headless

package sprites

import (
	"strconv"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
)

var (
	startTime = time.Now()

	spriteProgram = gfx.Shader("sprite", `uniform mat4 perspective;
uniform mat4 camera;
attribute vec4 tex_mins_maxs;
attribute vec4 mins_maxs;
attribute vec3 base_position;
attribute vec3 rotation;
attribute float flags;
attribute vec4 polygon;
attribute vec4 in_color;
attribute float in_blur;
attribute float in_tex;
varying vec4 position;
varying vec2 tex_coord;
varying float fog_distance;
varying float rainbow;
varying float mk_fog;
varying float no_discard;
varying float border;
varying float is_mask;
varying float half_glow;
varying vec4 color;
varying float blur;
varying vec4 tex_bounds;
varying float texid;

void main() {
	color = in_color;
	blur = in_blur;
	tex_bounds = tex_mins_maxs;
	texid = in_tex + 0.5;

	float billboard = mod(floor(flags / pow(2.0, 0.0)), 2.0);
	rainbow = mod(floor(flags / pow(2.0, 1.0)), 2.0);
	mk_fog = mod(floor(flags / pow(2.0, 2.0)), 2.0);
	no_discard = mod(floor(flags / pow(2.0, 3.0)), 2.0);
	border = mod(floor(flags / pow(2.0, 4.0)), 2.0);
	is_mask = mod(floor(flags / pow(2.0, 5.0)), 2.0);
	half_glow = mod(floor(flags / pow(2.0, 6.0)), 2.0);

	vec4 poly = polygon;
	if (blur > 0.0) {
		vec2 extra = blur / abs(tex_mins_maxs.xz - tex_mins_maxs.yw);
		poly = poly * (1.0 + extra * 2.0).xyxy - extra.xyxy;
	}
	if (poly.x > 1.5) {
		poly.x = poly.x / 255.0;
	}
	if (poly.y > 1.5) {
		poly.y = poly.y / 255.0;
	}

	tex_coord = mix(tex_mins_maxs.xz, tex_mins_maxs.yw, poly.zw);
	position = vec4(mix(mins_maxs.xz, mins_maxs.yw, poly.xy), 0.0, 1.0);
	vec3 rot = rotation;
	if (billboard > 0.5) {
		vec4 relative_base_position = camera * vec4(base_position, 1.0);
		rot.x += -atan(-relative_base_position.y, -relative_base_position.z);
		rot.y += atan(-relative_base_position.x, relative_base_position.z);
	}
	position = mat4(
		cos(rot.z), -sin(rot.z), 0.0, 0.0,
		sin(rot.z), cos(rot.z), 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0
	) * position;
	position = mat4(
		1.0, 0.0, 0.0, 0.0,
		0.0, cos(rot.x), -sin(rot.x), 0.0,
		0.0, sin(rot.x), cos(rot.x), 0.0,
		0.0, 0.0, 0.0, 1.0
	) * position;
	position = mat4(
		cos(rot.y), 0.0, sin(rot.y), 0.0,
		0.0, 1.0, 0.0, 0.0,
		-sin(rot.y), 0.0, cos(rot.y), 0.0,
		0.0, 0.0, 0.0, 1.0
	) * position;
	position.xyz += base_position;
	position = camera * position;
	position = perspective * position;
	fog_distance = length(position.xyz);
	gl_Position = position;
}`, `precision mediump float;

#define blurResolution 3

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
uniform sampler2D tex3;
uniform sampler2D tex4;
uniform sampler2D tex5;
uniform sampler2D tex6;
uniform sampler2D tex7;

uniform float time;
varying vec2 tex_coord;
varying vec4 position;
varying float fog_distance;
varying float rainbow;
varying float mk_fog;
varying float no_discard;
varying float border;
varying float is_mask;
varying float half_glow;
varying vec4 color;
varying float blur;
varying vec4 tex_bounds;
varying float texid;

vec4 texture2Darr(float tex_id, vec2 coord) {
	if (tex_id < 4.0) {
		if (tex_id < 2.0) {
			if (tex_id < 1.0) {
				return texture2D(tex0, coord);
			} else {
				return texture2D(tex1, coord);
			}
		} else {
			if (tex_id < 3.0) {
				return texture2D(tex2, coord);
			} else {
				return texture2D(tex3, coord);
			}
		}
	} else {
		if (tex_id < 6.0) {
			if (tex_id < 5.0) {
				return texture2D(tex4, coord);
			} else {
				return texture2D(tex5, coord);
			}
		} else {
			if (tex_id < 7.0) {
				return texture2D(tex6, coord);
			} else {
				return texture2D(tex7, coord);
			}
		}
	}
}

void main() {
	vec4 texColor = texture2Darr(texid, tex_coord);
	if (tex_coord.x < tex_bounds.x || tex_coord.x > tex_bounds.y || tex_coord.y < tex_bounds.z || tex_coord.y > tex_bounds.w) {
		texColor = vec4(0.0);
	}

	if (blur > 0.0) {
		bool anyPixels = false;
		float maxBlur = 1.0;
		for (int i = -blurResolution; i <= blurResolution; i++) {
			for (int j = -blurResolution; j <= blurResolution; j++) {
				float blurScale = 1.0 / (float(i*i) + float(j*j) + 1.0);
				maxBlur += blurScale;

				vec2 coord = tex_coord + vec2(float(i), float(j)) / float(blurResolution) * blur;
				if (coord.x < tex_bounds.x || coord.x > tex_bounds.y || coord.y < tex_bounds.z || coord.y > tex_bounds.w) {
					continue;
				}
				anyPixels = true;
				if (border > 0.5) {
					if (i*i + j*j <= blurResolution*blurResolution) {
						texColor = max(texColor, texture2Darr(texid, coord));
					}
				} else {
					texColor += texture2Darr(texid, coord) * blurScale;
				}
			}
		}
		if (border < 0.5) {
			texColor /= maxBlur;
		}
		if (!anyPixels) {
			discard;
		}
	}

	if (is_mask > 0.5) {
		texColor = texColor.rrrr;
	}

	if (texColor.a < 0.5 && no_discard < 0.5) {
		discard;
	}

	vec4 rainbowColor = vec4(1.0);

	if (rainbow > 0.5) {
		float variant = position.x * -1.5;

		rainbowColor.r = clamp(sin((time + variant + 20.0) * 5.9) * 2.0, 0.0, 1.0);
		rainbowColor.g = clamp(sin((time + variant) * 5.9) * 2.0, 0.0, 1.0);
		rainbowColor.b = clamp(sin((time + variant + 60.0) * 5.9) * 2.0, 0.0, 1.0);
	}

	gl_FragColor = texColor * rainbowColor * color;

	if (mk_fog > 0.5) {
		gl_FragColor.rgb *= clamp((5.0 - fog_distance) / 5.0, 0.0, 1.0);
	}

	if (half_glow > 0.5) {
		gl_FragColor.rgb = gl_FragColor.rgb * 3.0 / 4.0 + 0.25 * gl_FragColor.a;
	}
}`)

	spritePerspective = spriteProgram.Uniform("perspective")
	spriteCamera      = spriteProgram.Uniform("camera")
	spriteTime        = spriteProgram.Uniform("time")
	spritePolygon     = spriteProgram.Attrib("polygon")
	spriteMinsMaxs    = spriteProgram.Attrib("mins_maxs")
	spriteTexMinsMaxs = spriteProgram.Attrib("tex_mins_maxs")
	spritePosition    = spriteProgram.Attrib("base_position")
	spriteRotation    = spriteProgram.Attrib("rotation")
	spriteBlur        = spriteProgram.Attrib("in_blur")
	spriteFlags       = spriteProgram.Attrib("flags")
	spriteColor       = spriteProgram.Attrib("in_color")
	spriteTex         = spriteProgram.Attrib("in_tex")

	_ = gfx.Custom("sprite-tex-ids", func() {
		gfx.GL.UseProgram(spriteProgram.Program)

		for i := 0; i < 8; i++ {
			gfx.GL.Uniform1i(gfx.GL.GetUniformLocation(spriteProgram.Program, "tex"+strconv.Itoa(i)), i)
		}
	}, func() {})
)
