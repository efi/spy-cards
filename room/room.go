// Package room implements the 3D scenes that appear behind Spy Cards Online matches.
package room

import (
	"bufio"
	"context"
	"encoding/binary"
	"io"
	"reflect"
	"unsafe"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
)

// Room is a Spy Cards Online background.
type Room interface {
	Update()
	Render()
	SetPlayer(int)
	AddPlayer(p *Player, i int)
	AddAudience(a ...*Audience)
	SetStage(context.Context, card.ContentIdentifier) error
}

var (
	roomProgram = gfx.Shader("room", `uniform mat4 perspective;
uniform mat4 camera;
attribute vec3 in_pos;
attribute vec2 in_tex;
varying vec2 tex_coord;

void main() {
	gl_Position = perspective * camera * vec4(in_pos.x, in_pos.y, -in_pos.z, 1.0);
	tex_coord = vec2(in_tex.s, 1.0 - in_tex.t);
}
`, `precision mediump float;

uniform sampler2D tex;
varying vec2 tex_coord;

void main() {
	gl_FragColor = texture2D(tex, tex_coord);
}
`)

	roomPerspective = roomProgram.Uniform("perspective")
	roomCamera      = roomProgram.Uniform("camera")

	posAttrib = roomProgram.Attrib("in_pos")
	texAttrib = roomProgram.Attrib("in_tex")
)

type customStage struct {
	buf *gfx.StaticBuffer
	tex *gfx.AssetTexture

	noDepth bool
}

func loadStage(ctx context.Context, name string) (*customStage, error) {
	f, err := internal.OpenAsset(ctx, name)
	if err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}
	defer f.Close()

	r := bufio.NewReader(f)

	version, err := binary.ReadUvarint(r)
	if err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	if version != 0 {
		return nil, xerrors.Errorf("room: unhandled stage format version: %d", version)
	}

	l, err := binary.ReadUvarint(r)
	if err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	if l&1 != 0 {
		return nil, xerrors.Errorf("room: non-integer number of elements: %d.5", l>>1)
	}

	elements := make([]uint16, l>>1)
	/*#nosec*/
	unsafeElements := reflect.SliceHeader{
		Data: uintptr(unsafe.Pointer(&elements[0])),
		Len:  int(l),
		Cap:  int(l),
	}
	/*#nosec*/
	if _, err := io.ReadFull(r, *(*[]byte)(unsafe.Pointer(&unsafeElements))); err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	l, err = binary.ReadUvarint(r)
	if err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	data := make([]uint8, l)
	if _, err := io.ReadFull(r, data); err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	buf := gfx.NewStaticBuffer(name, data, elements)

	l, err = binary.ReadUvarint(r)
	if err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	imgData := make([]byte, l)
	if _, err := io.ReadFull(r, imgData); err != nil {
		return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
	}

	if _, err = r.ReadByte(); !xerrors.Is(err, io.EOF) {
		if err != nil {
			return nil, xerrors.Errorf("room: loading stage %q: %w", name, err)
		}

		return nil, xerrors.New("room: extra data after EOF")
	}

	return &customStage{
		buf: buf,
		tex: gfx.NewEmbeddedTexture(name, false, func() ([]byte, string, error) {
			return imgData, "image/jpeg", nil
		}),
	}, nil
}
