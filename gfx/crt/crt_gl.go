// +build !headless

package crt

import (
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
)

var (
	crtProgram = gfx.Shader("crt", `precision mediump float;

uniform vec4 _ScreenParams;
attribute vec4 in_POSITION0;
attribute vec2 in_TEXCOORD0;
varying vec2 vs_TEXCOORD0;
varying vec4 _ScreenParams2;

void main()
{
	float wantAspect = 16.0 / 9.0;
	float aspect = _ScreenParams.x / _ScreenParams.y;

	vec2 scale = vec2(
		aspect > wantAspect ? wantAspect / aspect : 1.0,
		aspect < wantAspect ? aspect / wantAspect : 1.0
	);

	_ScreenParams2 = vec4(
		_ScreenParams.x * scale.x,
		_ScreenParams.y * scale.y,
		1.0 + 1.0 / (_ScreenParams.x * scale.x),
		1.0 + 1.0 / (_ScreenParams.y * scale.y)
	);

	vs_TEXCOORD0 = in_TEXCOORD0;
	gl_Position = vec4(
		in_POSITION0.x * scale.x,
		in_POSITION0.y * scale.y,
		in_POSITION0.zw
	);
}`, `precision mediump float;

uniform float _NoiseX;
uniform vec2 _Offset;
uniform float _RGBNoise;
uniform float _SineNoiseWidth;
uniform float _SineNoiseScale;
uniform float _SineNoiseOffset;
uniform float _ScanLineTail;
uniform float _ScanLineSpeed;
uniform float _ScreenSize;
uniform float _Darkening;
uniform float _FadeOut;
uniform float _Time;
uniform vec4 _ScreenParams;
uniform sampler2D _MainTex;
varying vec2 vs_TEXCOORD0;
varying vec4 _ScreenParams2;

float doNoise(vec2 seed)
{
	float noise = dot(seed, vec2(12.9898005, 78.2330017));
	noise = sin(noise);
	noise = noise * 43758.5469;
	noise = fract(noise);
	return noise;
}

void main()
{
	vec2 posCentered = vs_TEXCOORD0 - 0.5;
	float distanceFromCenter = sqrt(dot(posCentered, posCentered));
	float distanceFromEdge = 1.0 - distanceFromCenter * _ScreenSize;
	vec2 posNormal = posCentered / distanceFromEdge;
	float distancePastEdge = max(abs(posNormal.x) - 0.5, abs(posNormal.y) - 0.5);
	if (distancePastEdge > 0.0) {
		discard;
	}

	vec2 texPos = posNormal + 0.5;

	float noise1 = sin(texPos.y * _SineNoiseWidth + _SineNoiseOffset);
	texPos.x = noise1 * _SineNoiseScale + texPos.x;
	texPos = texPos + vec2(_Offset.x, _Offset.y);

	float noise2 = doNoise(vec2(floor(texPos.y * 500.0) + _Time)) - 0.5;
	texPos.x = noise2 * _NoiseX + texPos.x;
	texPos = texPos - floor(texPos);

	float noiseFactor = doNoise(vec2(floor(texPos.y * 500.0) + _Time));
	noiseFactor = doNoise(vec2(noiseFactor + _Time - 0.5));

	texPos.y = 1.0 - texPos.y;

	vec4 pixelColor;
	if (noiseFactor < _RGBNoise) {
		pixelColor = vec4(
			doNoise(posNormal + vec2(_Time + 123.0, 0.0)),
			doNoise(posNormal + vec2(_Time + 123.0, 1.0)),
			doNoise(posNormal + vec2(_Time + 123.0, 2.0)),
			1.0
		);
	} else {
		vec4 color0 = texture2D(_MainTex, texPos);
		vec4 color1 = texture2D(_MainTex, texPos - vec2(0.002, 0.0));
		vec4 color2 = texture2D(_MainTex, texPos - vec2(0.004, 0.0));

		pixelColor = vec4(color0.r, color1.g, color2.b, 1.0);
	}

	float subPixelPos = vs_TEXCOORD0.x * _ScreenParams2.x;
	subPixelPos = fract(subPixelPos / 3.0) * 3.0;

	bool isLeft = subPixelPos < 1.0;
	bool isRight = subPixelPos >= 2.0;
	bool isWeirdPinkColumn = (posCentered.x > 0.0 ?
		posCentered.x <= 5.0/24.0 && posCentered.x + 1.0 / _ScreenParams2.x >= 5.0/24.0 :
		posCentered.x - 1.0 / _ScreenParams2.x >= -5.0/24.0 && posCentered.x - 2.0 / _ScreenParams2.x <= -5.0/24.0) && noiseFactor >= _RGBNoise;
	vec4 subPixelModifiedColor = vec4(
		!isLeft && (!isWeirdPinkColumn || !isRight) ? pixelColor.r : 0.0,
		(isLeft && !isWeirdPinkColumn) || isRight ? pixelColor.g : 0.0,
		!isRight && (!isWeirdPinkColumn || isLeft) ? pixelColor.b : 0.0,
		pixelColor.a
	);

	float scanLine = fract(_Time * _ScanLineSpeed + posNormal.y);
	scanLine = (scanLine + _ScanLineTail - 1.0) / min(_ScanLineTail, 1.0);
	scanLine = clamp(scanLine, 0.0, 1.0);

	vec3 finalColor = vec3(scanLine) * subPixelModifiedColor.rgb;
	float edgeDarkening = 1.0 - distanceFromCenter * _Darkening;

	gl_FragColor = vec4(vec3(edgeDarkening) * finalColor * _FadeOut, 1.0);
}`)

	crtMainTex         = crtProgram.Uniform("_MainTex")
	crtNoiseX          = crtProgram.Uniform("_NoiseX")
	crtOffset          = crtProgram.Uniform("_Offset")
	crtRGBNoise        = crtProgram.Uniform("_RGBNoise")
	crtSineNoiseWidth  = crtProgram.Uniform("_SineNoiseWidth")
	crtSineNoiseScale  = crtProgram.Uniform("_SineNoiseScale")
	crtSineNoiseOffset = crtProgram.Uniform("_SineNoiseOffset")
	crtScanLineTail    = crtProgram.Uniform("_ScanLineTail")
	crtScanLineSpeed   = crtProgram.Uniform("_ScanLineSpeed")
	crtScreenSize      = crtProgram.Uniform("_ScreenSize")
	crtDarkening       = crtProgram.Uniform("_Darkening")
	crtScanLineDarker  = crtProgram.Uniform("_ScanLineDarker")
	crtScanLineDarker2 = crtProgram.Uniform("_ScanLineDarker2")
	crtTime            = crtProgram.Uniform("_Time")
	crtScreenParams    = crtProgram.Uniform("_ScreenParams")
	crtFadeOut         = crtProgram.Uniform("_FadeOut")

	posAttrib = crtProgram.Attrib("in_POSITION0")
	texAttrib = crtProgram.Attrib("in_TEXCOORD0")

	crtFrameBuffer  gl.Framebuffer
	crtFrameTexture gl.Texture
	crtFrameDepth   gl.Renderbuffer

	_ = gfx.Custom("crt buffers", crtInit, crtRelease)
)

func crtInit() {
	crtFrameBuffer = gfx.GL.CreateFramebuffer()
	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, crtFrameBuffer)

	crtFrameTexture = gfx.GL.CreateTexture()
	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, crtFrameTexture)
	gfx.GL.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1024, 1024, gl.RGBA, gl.UNSIGNED_BYTE, nil)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gfx.GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gfx.GL.GenerateMipmap(gl.TEXTURE_2D)
	gfx.GL.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, crtFrameTexture, 0)

	crtFrameDepth = gfx.GL.CreateRenderbuffer()
	gfx.GL.BindRenderbuffer(gl.RENDERBUFFER, crtFrameDepth)
	gfx.GL.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, 1024, 1024)
	gfx.GL.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, crtFrameDepth)

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
}

func crtRelease() {
	gfx.GL.DeleteFramebuffer(crtFrameBuffer)
	gfx.GL.DeleteTexture(crtFrameTexture)
	gfx.GL.DeleteRenderbuffer(crtFrameDepth)

	crtFrameBuffer = gl.Framebuffer{}
	crtFrameDepth = gl.Renderbuffer{}
	crtFrameTexture = gl.Texture{}
}

var startTime = time.Now()

// Draw renders a frame.
func (c *CRT) Draw(render func()) {
	width, height := gfx.Size()

	if DisableCRT {
		gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
		gfx.GL.Viewport(0, 0, width, height)

		render()

		return
	}

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, crtFrameBuffer)
	gfx.GL.Viewport(0, 0, 1024, 1024)
	gfx.GL.Clear(gl.DEPTH_BUFFER_BIT)

	render()

	gfx.Lock.Lock()

	gfx.GL.BindFramebuffer(gl.FRAMEBUFFER, gl.Framebuffer{})
	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)
	gfx.GL.Viewport(0, 0, width, height)
	gfx.GL.UseProgram(crtProgram.Program)

	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, crtFrameTexture)

	gfx.GL.Uniform1i(crtMainTex.Uniform, 0)
	gfx.GL.Uniform4f(crtScreenParams.Uniform, float32(width), float32(height), 1+1/float32(width), 1+1/float32(height))
	gfx.GL.Uniform1f(crtTime.Uniform, float32(time.Since(startTime).Seconds()))
	gfx.GL.Uniform1f(crtNoiseX.Uniform, float32(c.NoiseX))
	gfx.GL.Uniform2f(crtOffset.Uniform, float32(c.Offset[0]), float32(c.Offset[1]))
	gfx.GL.Uniform1f(crtRGBNoise.Uniform, float32(c.RGBNoise))
	gfx.GL.Uniform1f(crtSineNoiseWidth.Uniform, float32(c.SineNoiseWidth))
	gfx.GL.Uniform1f(crtSineNoiseScale.Uniform, float32(c.SineNoiseScale))
	gfx.GL.Uniform1f(crtSineNoiseOffset.Uniform, float32(c.SineNoiseOffset))
	gfx.GL.Uniform1f(crtScanLineTail.Uniform, float32(c.ScanLineTail))
	gfx.GL.Uniform1f(crtScanLineSpeed.Uniform, float32(c.ScanLineSpeed))
	gfx.GL.Uniform1f(crtScreenSize.Uniform, float32(c.ScreenSize))
	gfx.GL.Uniform1f(crtDarkening.Uniform, float32(c.Darkening))
	gfx.GL.Uniform1f(crtScanLineDarker.Uniform, float32(c.ScanLineDarker))
	gfx.GL.Uniform1f(crtScanLineDarker2.Uniform, float32(c.ScanLineDarker2))
	gfx.GL.Uniform1f(crtFadeOut.Uniform, float32(c.FadeOut))

	gfx.GL.BindBuffer(gl.ARRAY_BUFFER, gfx.Square.Data)
	gfx.GL.VertexAttribPointer(posAttrib.Attrib, 4, gl.BYTE, false, 6, 0)
	gfx.GL.EnableVertexAttribArray(posAttrib.Attrib)
	gfx.GL.VertexAttribPointer(texAttrib.Attrib, 2, gl.BYTE, false, 6, 4)
	gfx.GL.EnableVertexAttribArray(texAttrib.Attrib)

	gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, gfx.Square.Element)
	gfx.GL.DrawElements(gl.TRIANGLES, gfx.Square.Count, gl.UNSIGNED_SHORT, 0)

	gfx.Lock.Unlock()
}
