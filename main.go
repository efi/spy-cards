// +build !headless

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/highscores"
	"git.lubar.me/ben/spy-cards/arcade/mainmenu"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/crt"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/match/visual"
	"git.lubar.me/ben/spy-cards/rng"
	"git.lubar.me/ben/spy-cards/room"
)

func main() {
	flag.BoolVar(&crt.DisableCRT, "disable-crt", false, "disable CRT effect (termacade only)")
	flag.BoolVar(&game.DrawButtons, "draw-buttons", false, "draw pressed buttons on screen (termacade only)")
	flag.BoolVar(&game.SuperSpeed, "super-speed", false, "run game at maximum tick rate (ignoring time) (termacade only)")
	flag.BoolVar(&game.AIEnabled, "use-ai", false, "use a neural network to decide game inputs (termacade only)")
	flag.StringVar(&rng.ForceSeed, "seed", "", "RNG seed (blank for random)")
	startState := flag.String("run", "MainMenu", "which sub-program to run")
	recording := flag.String("recording", "", "which recording to play (for sub-programs that play a recording)")
	mode := flag.String("mode", "", "Spy Cards game mode")
	customCards := flag.String("cards", "", "custom card data")
	variant := flag.Int("variant", 0, "card set variant")

	flag.Parse()

	ctx, ictx := input.NewContext(context.Background(), getInputs)

	go run(ctx, *startState, *recording, *mode, *customCards, *variant)

	appMain(ictx)
}

func run(ctx context.Context, startState, recording, mode, customCards string, variant int) {
	gfx.NextFrame()

	var err error

	seed := rng.ForceSeed
	if seed == "" {
		seed = rng.RandomSeed()
	}

	switch startState {
	default:
		fmt.Fprintf(os.Stderr, "unknown sub-program: %q\n", startState)

		os.Exit(2)
	case "MainMenu":
		log.Println("TODO: main menu")

		fallthrough
	case "TermacadeMenu":
		var mm mainmenu.MainMenu

		err = mm.Run(ctx)
	case "TermacadeRecording":
		var rec highscores.RecordingViewer

		rec.Code = recording

		err = rec.Run(ctx)
	case "HighScores":
		var hs highscores.HighScores

		err = hs.Run(ctx)
	case "MiteKnight":
		create := miteknight.New(arcade.DefaultMiteKnightRules)

		_, err = game.RunGame(ctx, create, seed)
	case "FlowerJourney":
		create := flowerjourney.New(arcade.DefaultFlowerJourneyRules)

		_, err = game.RunGame(ctx, create, seed)
	case "RoomRPC":
		err = room.RunRPC(ctx)
	case "SpyCards":
		var v *visual.Visual

		v, err = visual.New(ctx, &match.Init{
			Version: internal.Version,
			Mode:    mode,
			Custom:  customCards,
			Variant: uint64(variant),
		})
		if err != nil {
			break
		}

		if recording != "" {
			if err = v.SetRecording(ctx, recording); err != nil {
				break
			}
		}

		err = visual.Loop(ctx, v)
	case "CardEditor":
		var ce visual.CardEditor

		if err = ce.SetCards(customCards); err != nil {
			break
		}

		err = visual.Loop(ctx, &ce)
	}

	if err != nil {
		panic(err)
	}

	os.Exit(0)
}
