package card

import (
	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

func (cd *Def) unmarshalV4(r *format.Reader, loc *string, formatVersion uint64) (Def, error) {
	var card Def

	*loc = "reading card ID (v4)"
	card.ID = ID(r.UVarInt())
	card.Rank = card.ID.Rank()

	var tribes Tribe

	*loc = "reading card tribes (v4)"

	r.Read(&tribes)

	card.Tribes = []TribeDef{
		{
			Tribe: tribes >> 4,
		},
	}

	if tribes&15 != TribeNone {
		card.Tribes = append(card.Tribes, TribeDef{
			Tribe: tribes & 15,
		})
	}

	var special uint8

	*loc = "reading card special fields (v4)"

	r.Read(&special)

	for special>>4 != 0 {
		switch special >> 4 {
		case 1: // extra tribe
			if len(card.Tribes) < 2 {
				return Def{}, xerrors.New("card: special field ExtraTribe cannot appear on a card with only 1 existing tribe")
			}

			if Tribe(special&15) == TribeNone {
				return Def{}, xerrors.New("card: special field ExtraTribe cannot have tribe None")
			}

			t := TribeDef{
				Tribe: Tribe(special & 15),
			}

			if t.Tribe == TribeCustom {
				r.Read(&t.Red)
				r.Read(&t.Green)
				r.Read(&t.Blue)
				t.CustomName = r.String()
			}

			card.Tribes = append(card.Tribes, t)
		default:
			return Def{}, xerrors.Errorf("card: unknown special field: %d", special>>4)
		}

		r.Read(&special)
	}

	switch {
	case special <= 10:
		card.TP = int64(special)
		card.legacyUnpickable = false
	case special == 15:
		card.TP = 1
		card.legacyUnpickable = true
	default:
		return Def{}, xerrors.Errorf("card: invalid value for TP byte: %02x", special)
	}

	*loc = "reading card portrait byte (v4)"

	r.Read(&card.Portrait)

	if card.Portrait > 234 && card.Portrait < 254 {
		return Def{}, xerrors.Errorf("card: invalid value for portrait byte: %02x", card.Portrait)
	}

	*loc = "reading card name (v4)"

	card.Name = r.String()

	*loc = "reading card effect count (v4)"

	effectCount := r.UVarInt()
	effectBuf := make([]EffectDef, effectCount)
	card.Effects = make([]*EffectDef, effectCount)

	*loc = "reading card effects (v4)"

	for i := range card.Effects {
		card.Effects[i] = &effectBuf[i]
		if err := card.Effects[i].Unmarshal(r, formatVersion); err != nil {
			return Def{}, xerrors.Errorf("card: decoding effect %d: %w", i, err)
		}
	}

	for i := 0; i < len(card.Effects); i++ {
		if card.Effects[i].Type == EffectTP {
			card.TP -= card.Effects[i].Amount
			card.Effects = append(card.Effects[:i], card.Effects[i+1:]...)
			i--
		}
	}

	*loc = "reading card custom tribes (v4)"

	if len(card.Tribes) >= 1 && card.Tribes[0].Tribe == TribeCustom {
		r.Read(&card.Tribes[0].Red)
		r.Read(&card.Tribes[0].Green)
		r.Read(&card.Tribes[0].Blue)
		card.Tribes[0].CustomName = r.String()
	}

	if len(card.Tribes) >= 2 && card.Tribes[1].Tribe == TribeCustom {
		r.Read(&card.Tribes[1].Red)
		r.Read(&card.Tribes[1].Green)
		r.Read(&card.Tribes[1].Blue)
		card.Tribes[1].CustomName = r.String()
	}

	*loc = "reading card custom portrait (v4)"

	if card.Portrait == PortraitCustomEmbedded || card.Portrait == PortraitCustomExternal {
		customPortraitLength := r.UVarInt()
		if customPortraitLength > uint64(r.Len()) {
			return Def{}, xerrors.New("card: custom portrait length longer than remaining data")
		}

		card.CustomPortrait = r.Bytes(int(customPortraitLength))
	} else {
		card.CustomPortrait = nil
	}

	return card, nil
}
