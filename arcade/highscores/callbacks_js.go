// +build js,wasm
// +build !headless

package highscores

import (
	"net/url"
	"strconv"
	"strings"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
)

func (hs *HighScores) setParametersFromURL() {
	v, err := url.ParseQuery(strings.TrimPrefix(js.Global().Get("location").Get("search").String(), "?"))
	if err != nil {
		return
	}

	switch v.Get("highscores") {
	case "Recent":
		hs.Mode = Recent
	case "Weekly":
		hs.Mode = Weekly
	case "Quarterly":
		hs.Mode = Quarterly
	case "AllTime":
		hs.Mode = AllTime
	}

	switch v.Get("game") {
	case "FlowerJourney":
		hs.Game = arcade.FlowerJourney
	case "MiteKnight":
		hs.Game = arcade.MiteKnight
	}

	if n, err := strconv.Atoi(v.Get("page")); err == nil && n >= 1 {
		hs.Page = n - 1
	}
}

func (hs *HighScores) setURL() {
	js.Global().Get("history").Call("replaceState",
		js.Null(),
		js.Global().Get("document").Get("title"),
		js.Global().Get("location").Get("pathname").String()+"?highscores="+hs.Mode.String()+"&game="+hs.Game.String()+"&page="+strconv.Itoa(hs.Page+1),
	)
}

func (hs *HighScores) clearURL() {
	js.Global().Get("history").Call("replaceState",
		js.Null(),
		js.Global().Get("document").Get("title"),
		js.Global().Get("location").Get("pathname"),
	)
}

func (v *RecordingViewer) pushURL() {
	js.Global().Get("history").Call("pushState",
		js.Null(),
		js.Global().Get("document").Get("title"),
		js.Global().Get("location").Get("pathname").String()+"?recording="+v.Code,
	)
}

func (v *RecordingViewer) popURL() {
	query, err := url.ParseQuery(strings.TrimPrefix(js.Global().Get("location").Get("search").String(), "?"))
	if err != nil {
		return
	}

	if _, ok := query["recording"]; ok {
		js.Global().Get("history").Call("back")
	}
}

var (
	activeGame *game.State

	setExit = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		if activeGame != nil {
			activeGame.Exit = true
		}

		return js.Undefined()
	})
)

func (v *RecordingViewer) pushListener(s *game.State) {
	activeGame = s

	js.Global().Call("addEventListener", "popstate", setExit)
}

func (v *RecordingViewer) popListener() {
	activeGame = nil

	js.Global().Call("removeEventListener", "popstate", setExit)
}
