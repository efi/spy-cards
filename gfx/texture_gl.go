// +build !headless

package gfx

import (
	"log"

	"golang.org/x/mobile/gl"
)

func onNewTexture(t *Texture) {
	Lock.Lock()

	textures = append(textures, t)

	if GL != nil {
		t.init()
	}

	Lock.Unlock()
}

// Release deletes a texture handle.
func (t *Texture) Release() {
	Lock.Lock()

	if GL != nil {
		t.release()
	}

	for i := range textures {
		if textures[i] == t {
			textures = append(textures[:i], textures[i+1:]...)

			break
		}
	}

	Lock.Unlock()
}

func (t *Texture) init() {
	t.Texture = GL.CreateTexture()

	GL.BindTexture(gl.TEXTURE_2D, t.Texture)
	GL.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, t.img.Rect.Dx(), t.img.Rect.Dy(), gl.RGBA, gl.UNSIGNED_BYTE, t.img.Pix)

	if t.nearest {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	} else {
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
		GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST)
	}

	GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	GL.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	GL.GenerateMipmap(gl.TEXTURE_2D)
}

func (t *Texture) release() {
	GL.DeleteTexture(t.Texture)

	t.Texture = gl.Texture{}
}

func (a *AssetTexture) init() {
	img, mime, err := a.load()
	if err != nil {
		a.err = err
	} else {
		a.tex, a.err = newDataTexture(a.Name, img, mime, a.nearest)
	}

	if a.err != nil {
		log.Println("error loading texture", a.Name, a.err)
	}

	if a.cache {
		_, _ = texCache.Do(a.Name, func() (interface{}, error) {
			return a, nil
		})
	}

	close(a.loaded)
}
