---
title: Data Formats (Card v0 and v1)
...

(base64-encoded)

- Byte 0: Format version number (1)
- Byte 1: Global card ID to replace
- Byte 2 (most significant 4 bits): Tribe 1
- Byte 2 (least significant 4 bits): Tribe 2
- Byte 3 (most significant bit): (reserved)
- Byte 3 (bit 6): if set, and byte 4 is 0xff, custom image ID follows instead of raw PNG
- Byte 3 (bits 4-5): 0 = Attacker, 1 = Effect, 2 = Mini-Boss, 3 = Boss (this must match the original card)
- Byte 3 (least significant 4 bits): TP cost (range 0..10, 15 = infinite)
- Byte 4 (most significant 4 bits): Portrait Y coordinate
- Byte 4 (least significant 4 bits): Portrait X coordinate
- (If byte 4 is 0xff, a PNG follows the custom card.)
- Byte 5: Name length (0 for "no replacement")
- Bytes 6 to 6+n: Name (UTF-8)
- Byte 7+n: Number of effects
- Bytes 8+n to end: (repeated) effect definition
- If Tribe 1 is 14:
    - 3 bytes: RGB color
    - 1 byte: Tribe 1 name length
    - n bytes: Tribe 1 name
- If Tribe 2 is 14:
    - 3 bytes: RGB color
    - 1 byte: Tribe 2 name length
    - n bytes: Tribe 2 name

## Effect Definition

- Byte 0: Effect ID

For all effects:

- Amount = 0 means infinity
- If Tribe = 14, it is followed by:
    - Byte (name length)
    - UTF-8 bytes (name)

### 0: ATK (static)

- Byte 1: Amount

### 1: DEF (static)

- Byte 1: Amount

### 2: ATK (static, once per turn)

- Byte 1: Amount

### 3: ATK+n

- Byte 1: Amount

### 4: DEF+n

- Byte 1: Amount

### 5: Empower (card)

- Byte 1: Global Card ID
- Byte 2: Count

### 6: Empower (Tribe)

- Byte 1 (most significant 4 bits): (reserved)
- Byte 1 (least significant 4 bits): Tribe ID
- Byte 2: Count

### 7: Heal

- Byte 1: Amount

### 8: Lifesteal

- Byte 1: Amount

### 9: Numb

- Byte 1: Amount

### 10: Pierce

- Byte 1: Amount

### 11: Summon

- Byte 1: Global Card ID

### 12: Summon Rank

- Byte 1 (since version 1, most significant 6 bits): reserved (0)
- Byte 1 (since version 1, least significant 2 bits): 0 = attacker, 1 = effect, 2 = mini-boss, 3 = boss

### 13: Summon Tribe

- Byte 1 (most significant 4 bits): 0 = enemy, 1 = attacker, 2 = effect, 3 = mini-boss, 4 = boss
- Byte 1 (least significant 4 bits): Tribe ID
- Byte 2: Count

### 14: Unity

(if Tribe is (none), replace this effect with ATK when decoding)

- Byte 1 (most significant 4 bits): (reserved)
- Byte 1 (least significant 4 bits): Tribe ID
- Byte 2: Amount

### 15: TP

- Byte 1: Amount

### 16: Summon as Opponent

- Byte 1: Global Card ID

### 17: Multiply Healing

- Byte 1: Amount minus 1 (1 -> multiply by 2, -1 -> multiply by 0, etc.)

### 18: Flavor Text

- Byte 1 (most significant 7 bits): (reserved)
- Byte 1 (least significant bit): hide remaining effects
- Byte 2: text length
- Bytes 3-n: text (UTF-8)

### 128: Coin

- Byte 1 (since version 1): condition flags
- Byte 2 (most significant bit): 0=heads only, 1=heads or tails
- Byte 2 (least significant 7 bits): Count minus 1
- Bytes 3 to end: (possibly repeated) Effect Definition

### 129: If Card

- Byte 1 (since version 1): condition flags
- Byte 2: Global Card ID
- Byte 3 (since version 1): Count
- Bytes 4 to end: Effect Definition

### 130: Per Card

- Byte 1 (since version 1): condition flags
- Byte 2: Global Card ID
- Bytes 3 to end: Effect Definition

### 131: If Tribe

- Byte 1 (since version 1): condition flags
- Byte 2 (most significant 4 bits): (reserved)
- Byte 2 (least significant 4 bits): Tribe ID
- Byte 3: Count
- Bytes 4 to end: Effect Definition

### 132: VS Tribe

- Byte 1 (since version 1): condition flags
- Byte 2 (most significant 4 bits): (reserved)
- Byte 2 (least significant 4 bits): Tribe ID
- Byte 3 (since version 1): Count
- Bytes 4 to end: Effect Definition

### 133: If Stat

- Byte 1 (since version 1): condition flags
- Byte 2 (most significant bit): 0 = ATK, 1 = DEF
- Byte 2 (least significant 7 bits): Count
- Bytes 3 to end: Effect Definition

### 134: Setup

- Byte 1 (since version 1): condition flags
- Bytes 2 to end: Effect Definition

### 135: Limit

- Byte 1 (since version 1): condition flags
- Byte 2: Count
- Bytes 3 to end: Effect Definition

### 136: VS Card

- Byte 1: condition flags
- Byte 2: Global Card ID
- Byte 3: Count
- Bytes 4 to end: Effect Definition

### 137: If Rank

- Byte 1: condition flags
- Byte 2 (most significant 6 bits): reserved (0)
- Byte 2 (least significant 2 bits): 0 = attacker, 1 = effect, 2 = mini-boss, 3 = boss
- Byte 3: Count
- Bytes 4 to end: Effect Definition

### 138: VS Rank

- Byte 1: condition flags
- Byte 2 (most significant 6 bits): reserved (0)
- Byte 2 (least significant 2 bits): 0 = attacker, 1 = effect, 2 = mini-boss, 3 = boss
- Byte 3: Count
- Bytes 4 to end: Effect Definition

### 139: If Win

- Byte 1: condition flags
- Bytes 2 to end: Effect Definition

### 140: If Tie

- Byte 1: condition flags
- Bytes 2 to end: Effect Definition

### 141: If Stat (after modifiers)

- Byte 1: condition flags
- Byte 2 (most significant bit): 0 = ATK, 1 = DEF
- Byte 2 (least significant 7 bits): Count
- Bytes 3 to end: Effect Definition

### 142: VS Stat

- Byte 1: condition flags
- Byte 2 (most significant bit): 0 = ATK, 1 = DEF
- Byte 2 (least significant 7 bits): Count
- Bytes 3 to end: Effect Definition

### 143: VS Stat (after modifiers)

- Byte 1: condition flags
- Byte 2 (most significant bit): 0 = ATK, 1 = DEF
- Byte 2 (least significant 7 bits): Count
- Bytes 3 to end: Effect Definition

### 144: If HP

- Byte 1: condition flags
- Byte 2: Count
- Bytes 3 to end: Effect Definition

### 145: Per Tribe

- Byte 1: condition flags
- Byte 2 (most significant 4 bits): (reserved)
- Byte 2 (least significant 4 bits): Tribe ID
- Bytes 3 to end: Effect Definition

### 146: Per Rank

- Byte 1: condition flags
- Byte 2 (most significant 6 bits): reserved (0)
- Byte 2 (least significant 2 bits): 0 = attacker, 1 = effect, 2 = mini-boss, 3 = boss
- Bytes 3 to end: Effect Definition

### 147: VS HP

- Byte 1: condition flags
- Byte 2: Count
- Bytes 3 to end: Effect Definition

## Condition Flags

- Bits 1-7 (most significant 7 bits): reserved
- Bit 0 (least significant): invert
