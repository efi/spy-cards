// +build js,wasm
// +build !headless

package room

import (
	"context"
	"syscall/js"
	"time"

	"git.lubar.me/ben/spy-cards/gfx"
	"golang.org/x/mobile/gl"
)

// RunRPC implements the RPC compatibility layer for the JavaScript
// implementation of Spy Cards Online.
func RunRPC(ctx context.Context) error {
	gfx.GL.(js.Wrapper).JSValue().Get("canvas").Get("classList").Call("add", "inactive")

	setRoom := make(chan Room, 1)

	var (
		waitingForMine []js.Value
		handles        = []interface{}{nil}
	)

	js.Global().Get("SpyCards").Get("Native").Call("roomRPCInit", map[string]interface{}{
		"newStage": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			go func() {
				s := NewStage(ctx, nil)
				id := len(handles)
				handles = append(handles, s)

				args[0].Invoke(id)
			}()

			return js.Undefined()
		}),
		"newMine": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			go func() {
				m := NewMine(ctx, nil)
				id := len(handles)
				handles = append(handles, m)

				args[0].Invoke(id)
			}()

			return js.Undefined()
		}),
		"setRoom": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			setRoom <- handles[args[0].Int()].(Room)

			return js.Undefined()
		}),
		"onMineSelected": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			waitingForMine = append(waitingForMine, args[0])

			return js.Undefined()
		}),
		"setPlayer": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			p := NewPlayer(CharacterByName[args[1].String()])
			id := len(handles)
			handles = append(handles, p)
			handles[args[0].Int()].(Room).AddPlayer(p, args[2].Int())

			return id
		}),
		"setCurrentPlayer": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			handles[args[0].Int()].(Room).SetPlayer(args[1].Int())

			return js.Undefined()
		}),
		"setCharacter": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			handles[args[0].Int()].(*Player).Character = CharacterByName[args[1].String()]

			return js.Undefined()
		}),
		"createAudience": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			seed := make([]byte, args[0].Length())
			js.CopyBytesToGo(seed, args[0])
			a := NewAudience(seed, uint64(args[1].Int()))

			var ids []interface{}
			for _, member := range a {
				id := len(handles)
				handles = append(handles, member)
				ids = append(ids, id)
			}

			return ids
		}),
		"isAudienceBack": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			return handles[args[0].Int()].(*Audience).Back
		}),
		"addAudience": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			handles[args[0].Int()].(Room).AddAudience(handles[args[1].Int()].(*Audience))

			return js.Undefined()
		}),
		"startCheer": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			handles[args[0].Int()].(*Audience).StartCheer(time.Duration(args[1].Float() * float64(time.Second)))

			return js.Undefined()
		}),
		"becomeAngry": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			handles[args[0].Int()].(*Player).BecomeAngry(time.Duration(args[1].Float() * float64(time.Second)))

			return js.Undefined()
		}),
		"createAudienceMember": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			a := &Audience{
				Type:       AudienceType(args[0].Int()),
				X:          float32(args[1].Float()),
				Z:          float32(args[2].Float()),
				Scale:      1,
				Back:       args[3].Bool(),
				Flip:       args[4].Bool(),
				Hue:        float32(args[5].Float()),
				Color:      hueToColor(args[5].Float()),
				Excitement: float32(args[6].Float()),
			}
			id := len(handles)
			handles = append(handles, a)

			return id
		}),
		"exit": js.FuncOf(func(this js.Value, args []js.Value) interface{} {
			handles = handles[:1]
			setRoom <- nil

			return js.Undefined()
		}),
	})

	frameActive := false
	frameReady := make(chan struct{}, 1)
	nextFrame := func() {
		gfx.NextFrame()

		select {
		case frameReady <- struct{}{}:
		default:
		}
	}

	var room Room

	var nextTick time.Time

	for {
		select {
		case r := <-setRoom:
			room = r

			if !frameActive {
				frameActive = true
				nextTick = time.Now()

				gfx.GL.(js.Wrapper).JSValue().Get("canvas").Get("classList").Call("remove", "inactive")

				go nextFrame()
			}
		case <-frameReady:
			gfx.GL.ClearColor(0, 0, 0, 1)
			gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

			if room != nil {
				if time.Since(nextTick) > 5*time.Second {
					nextTick = time.Now()
				}

				outstandingFrames := 0
				for time.Now().After(nextTick) && outstandingFrames < 3 {
					room.Update()

					outstandingFrames++

					nextTick = nextTick.Add(time.Second / 60)

					if len(waitingForMine) != 0 {
						m, ok := room.(*mine)
						if ok && m.confirmed {
							for _, f := range waitingForMine {
								f.Invoke(m.players[m.selected].Character.Name)
							}

							waitingForMine = nil
						}
					}
				}
				room.Render()

				go nextFrame()
			} else {
				frameActive = false
				gfx.GL.(js.Wrapper).JSValue().Get("canvas").Get("classList").Call("add", "inactive")
			}
		}
	}
}
