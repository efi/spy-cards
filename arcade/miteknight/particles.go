package miteknight

import (
	"image/color"
	"math"
	"math/rand"
	"time"

	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

type particleDef struct {
	lifetime float32
	delay    float32
	size     float32
	speed    float32
	count    int
	color    color.RGBA
	colorMin color.RGBA
	additive bool
	hemi     bool
	radius   float32
	angular  [2]float32
	sprites  []*sprites.Sprite
	dampen   float32
	gravity  float32
	sizeFunc func(t float32) float32
}

type particleEffect struct {
	def             *particleDef
	start           time.Time
	end             time.Time
	color           color.RGBA
	sprite          *sprites.Sprite
	initialPosition coord3
	initialOffset   coord3
	velocity        coord3
	initialAngle    float32
	angular         float32
	size            float32
}

var deathSmoke = []*particleDef{
	{
		lifetime: 1,
		delay:    0.01,
		speed:    3,
		size:     1,
		count:    20,
		radius:   0.1,
		hemi:     true,
		color:    color.RGBA{191, 191, 191, 255},
		colorMin: color.RGBA{127, 127, 127, 255},
		angular:  [2]float32{-math.Pi / 4, math.Pi / 4},
		sprites:  []*sprites.Sprite{sprites.ParticleSmoke},
		dampen:   0.18,
		sizeFunc: func(t float32) float32 { return 1 - t*t },
		gravity:  -0.15 * 9.8,
	},
}

var hitPart = []*particleDef{
	{
		lifetime: 0.5,
		size:     0.6,
		speed:    5,
		count:    5,
		color:    color.RGBA{255, 255, 255, 255},
		colorMin: color.RGBA{255, 237, 0, 255},
		radius:   0.31,
		angular:  [2]float32{2 * math.Pi, 2 * math.Pi},
		sprites:  []*sprites.Sprite{sprites.ParticleStar},
		sizeFunc: func(t float32) float32 { return 1 - t },
		// size curve ease-in-out(?)
		gravity: 0.1 * 9.8,
	},
	{
		lifetime: 0.3,
		size:     3,
		speed:    10,
		count:    10,
		color:    color.RGBA{127, 89, 63, 255},
		colorMin: color.RGBA{127, 89, 63, 255},
		additive: true,
		radius:   0.21,
		angular:  [2]float32{5 / 9 * math.Pi, 5 / 9 * math.Pi},
		sprites:  []*sprites.Sprite{sprites.ParticleGrassPlaceholder},
		sizeFunc: func(t float32) float32 { return 1 - t },
		// size curve ease-in-out(?)
		gravity: 0,
	},
}

func (d *dungeonFloor) spawnParticleSystem(defs []*particleDef, x, y, z, scale float32) {
	scale *= 0.75
	y += 0.4

	if d.dungeon.particleRand == nil {
		/* #nosec */
		d.dungeon.particleRand = rand.New(rand.NewSource(int64(d.dungeon.state.TickCount)))
	}

	now := time.Now()

	for _, def := range defs {
		for i := 0; i < def.count; i++ {
			colorRand := d.dungeon.particleRand.Float64()
			color := color.RGBA{
				R: uint8(game.Lerp(float64(def.colorMin.R), float64(def.color.R), colorRand)),
				G: uint8(game.Lerp(float64(def.colorMin.G), float64(def.color.G), colorRand)),
				B: uint8(game.Lerp(float64(def.colorMin.B), float64(def.color.B), colorRand)),
				A: 255,
			}

			normal := coord3{
				d.dungeon.particleRand.Float32()*2 - 1,
				d.dungeon.particleRand.Float32()*2 - 1,
				d.dungeon.particleRand.Float32()*2 - 1,
			}

			if def.hemi && normal.y < 0 {
				normal.y = -normal.y
			}

			mag := float32(math.Sqrt(float64(normal.x*normal.x + normal.y*normal.y + normal.z*normal.z)))
			normal.x = normal.x / mag * scale
			normal.y = normal.y / mag * scale
			normal.z = normal.z / mag * scale

			d.dungeon.particles = append(d.dungeon.particles, &particleEffect{
				def:             def,
				color:           color,
				start:           now.Add(time.Duration(def.delay * float32(i) * float32(time.Second))),
				end:             now.Add(time.Duration((def.delay*float32(i) + def.lifetime) * float32(time.Second))),
				sprite:          def.sprites[d.dungeon.particleRand.Intn(len(def.sprites))],
				initialAngle:    d.dungeon.particleRand.Float32() * 2 * math.Pi,
				initialPosition: coord3{x, y, z},
				initialOffset:   coord3{normal.x * def.radius, normal.y * def.radius, normal.z * def.radius},
				velocity:        coord3{normal.x * def.speed * def.lifetime, normal.y * def.speed * def.lifetime, normal.z * def.speed * def.lifetime},
				size:            scale * def.size,
				angular:         game.Lerp32(def.angular[0], def.angular[1], d.dungeon.particleRand.Float32()),
			})
		}
	}
}
