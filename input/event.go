package input

import (
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
)

// OnSize handles screen size events.
func (c *Context) OnSize(e size.Event) {
	c.size = e
}

// OnTouch handles touchscreen events.
func (c *Context) OnTouch(e touch.Event) {
	c.touch = append(c.touch, e)
}

// OnMouse handles mouse events.
func (c *Context) OnMouse(e mouse.Event) {
	c.mouse = append(c.mouse, e)
}

// OnKey handles keyboard events.
func (c *Context) OnKey(e key.Event) {
	c.key = append(c.key, e)
}
