package match

import (
	"context"
	"log"
	"runtime"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match/matchnet"
)

type Recording struct {
	Recording *card.Recording
	match     *Match
	scg       *matchnet.SecureCardGame
	rounds    []recordingRound
	lock      sync.Mutex
}

type recordingRound struct {
	state *State
	ready card.TurnData
}

func NewRecording(ctx context.Context, rec *card.Recording) (*Recording, error) {
	r := &Recording{
		Recording: rec,
	}

	if err := r.initMatch(); err != nil {
		return nil, err
	}

	return r, nil
}

func (r *Recording) initMatch() error {
	r.match = &Match{
		Init: &Init{
			Mode:    r.Recording.ModeName,
			Version: r.Recording.Version,
			Custom:  r.Recording.CustomCardsRaw,
			Variant: uint64(r.Recording.CustomCards.Variant),
		},
		Rematches:   r.Recording.RematchCount,
		Perspective: r.Recording.Perspective,
		Cosmetic:    r.Recording.Cosmetic,
		Set:         new(card.Set),
	}

	if runtime.GOOS == "js" {
		r.match.Log.Debug = true
	}

	if r.Recording.Perspective != 2 {
		r.match.Perspective = 1
	}

	*r.match.Set = r.Recording.CustomCards
	if r.match.Set.Variant != -1 {
		r.match.Set.Mode, _ = r.match.Set.Mode.Variant(r.match.Set.Variant)
	}

	r.match.IndexSet()

	var err error

	r.scg, err = matchnet.NewSecureCardGame(&matchnet.SecureCardGameOptions{
		ForReplay: &r.Recording.Version,
	})
	if err != nil {
		return err
	}

	copy(r.scg.Seed(matchnet.RNGShared), r.Recording.SharedSeed[:])

	if r.match.Perspective == 2 {
		copy(r.scg.Seed(matchnet.RNGLocal), r.Recording.PrivateSeed[1][:])
		copy(r.scg.Seed(matchnet.RNGRemote), r.Recording.PrivateSeed[0][:])
	} else {
		copy(r.scg.Seed(matchnet.RNGLocal), r.Recording.PrivateSeed[0][:])
		copy(r.scg.Seed(matchnet.RNGRemote), r.Recording.PrivateSeed[1][:])
	}

	for player := range r.match.State.Sides {
		r.match.State.Sides[player].Deck = make([]*Card, len(r.Recording.InitialDeck[player]))

		for i, id := range r.Recording.InitialDeck[player] {
			c := r.match.Set.Card(id)

			r.match.State.Sides[player].Deck[i] = &Card{
				Set:  r.match.Set,
				Def:  c,
				Back: c.Rank.Back(),
			}
		}
	}

	r.rounds = make([]recordingRound, len(r.Recording.Rounds)+1)

	go r.process()

	return nil
}

// NumRounds returns the total number of rounds in the recording (the final
// round in the recording is after the winner of the match is determined).
func (r *Recording) NumRounds() int {
	return len(r.rounds)
}

// Round returns a processed round from the match recording.
func (r *Recording) Round(i uint64) (*State, *card.TurnData) {
	if i >= uint64(len(r.rounds)) {
		return nil, nil
	}

	r.lock.Lock()
	s := r.rounds[i]
	r.lock.Unlock()

	if s.state == nil {
		return nil, nil
	}

	if i == uint64(len(r.rounds))-1 {
		return s.state.clone(), nil
	}

	return s.state.clone(), &s.ready
}

// RoundProcessed returns true if the specified round has finished processing.
func (r *Recording) RoundProcessed(i uint64) bool {
	r.lock.Lock()
	s := r.rounds[i]
	r.lock.Unlock()

	return s.state != nil
}

func (r *Recording) process() {
	for i, round := range r.Recording.Rounds {
		local, remote := r.Recording.PrivateSeed[0][:], r.Recording.PrivateSeed[1][:]
		if r.Recording.Perspective == 2 {
			local, remote = remote, local
		}

		r.scg.InitTurnSeed(round.TurnSeed[:], local, remote)

		if i == 0 {
			r.match.InitState(r.scg.Rand(matchnet.RNGShared), nil)

			r.match.Start = r.Recording.Start
		}

		r.match.State.Round++

		r.match.ShuffleAndDraw(r.scg, true)
		r.match.ApplyInHand()

		ready := card.TurnData{
			Ready: round.Ready,
		}

		for player := range ready.Played {
			for i, c := range r.match.State.Sides[player].Hand {
				if ready.Ready[player]&(1<<i) != 0 {
					ready.Played[player] = append(ready.Played[player], c.Def.ID)
				}
			}
		}

		state := r.match.State.clone()

		r.lock.Lock()
		r.rounds[i].state = state
		r.rounds[i].ready = ready
		r.lock.Unlock()

		if r.Recording.FormatVersion >= 1 {
			r.scg.WhenConfirmedTurn(round.TurnSeed2[:])
		}

		r.match.BeginTurn(&ready)
		rng := r.scg.Rand(matchnet.RNGShared)

		for len(r.match.ProcessQueuedEffect(rng, nil)) != 0 {
			if runtime.GOOS == "js" {
				time.Sleep(time.Millisecond)
			} else {
				runtime.Gosched()
			}
		}
	}

	r.lock.Lock()
	r.rounds[len(r.Recording.Rounds)].state = r.match.State.clone()
	r.lock.Unlock()

	if r.match.Log.Debug {
		log.Printf("DEBUG: at end of match: Player 1 HP: %v // Player 2 HP: %v // Round winner: %v // Match winner: %v", r.match.State.Sides[0].HP, r.match.State.Sides[1].HP, r.match.State.RoundWinner, r.match.Winner())
	}
}
