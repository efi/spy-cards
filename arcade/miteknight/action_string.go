// Code generated by "stringer -type action -trimprefix action"; DO NOT EDIT.

package miteknight

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[actionNone-0]
	_ = x[actionAttacking-1]
	_ = x[actionTurning-2]
	_ = x[actionMoving-3]
}

const _action_name = "NoneAttackingTurningMoving"

var _action_index = [...]uint8{0, 4, 13, 20, 26}

func (i action) String() string {
	if i >= action(len(_action_index)-1) {
		return "action(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _action_name[_action_index[i]:_action_index[i+1]]
}
