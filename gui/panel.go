package gui

import (
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"image/color"
)

type Renderable interface {
	Render()
}

type Panel struct {
	Container
	Color color.RGBA
}

func NewPanel() *Panel {
	return &Panel{
		Container: *NewContainer(),
	}
}

func (p *Panel) Render() {
	batch := p.Container.Control.root.Batch
	batch.Append(sprites.Blank, p.Rect.x, p.Rect.y, 0, p.Rect.w, p.Rect.h, p.Color, 0, 0, 0, 0)
}
