// +build !headless

package game

const isHeadless = false

// SuperSpeed causes the game to run as many ticks as possible per rendered frame.
var SuperSpeed bool
