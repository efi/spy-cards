// Dummy file to allow linting to not parse the huge generated font data file.

// +build codeanalysis

package sprites

var (
	d3StreetismAdvance   map[rune]float32
	d3Streetism          map[rune][2]*Sprite
	bubblegumSansAdvance map[rune]float32
	bubblegumSans        map[rune][2]*Sprite
	arialAdvance         map[rune]float32
	arial                map[rune][2]*Sprite
	dejavuAdvance        map[rune]float32
	dejavu               map[rune][2]*Sprite
	kbSprites            [10]*Sprite

	_ = embeddedSheet
	_ = embeddedTextSheet
	_ = (*textSheetWrapper).sprite
)
