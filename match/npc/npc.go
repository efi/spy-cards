// Package npc implements computer-controlled Spy Cards Online players.
package npc

import (
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
	"golang.org/x/xerrors"
)

// NPC is a computer-controlled Spy Cards Online player.
type NPC interface {
	// PickPlayer returns a player character to represent this NPC.
	PickPlayer() *room.Character

	// CreateDeck returns a slice of cards to use as a deck.
	CreateDeck(set *card.Set) card.Deck

	// PlayRound returns a bitmask of cards from the player's hand to play.
	PlayRound(m *match.Match) uint64

	// AfterRound is called after a round's winner has been determined.
	AfterRound(m *match.Match)
}

func mustDeck(s string) card.Deck {
	var d card.Deck

	if err := d.UnmarshalText([]byte(s)); err != nil {
		panic(err)
	}

	return d
}

// Get returns an NPC for the specified identifier.
func Get(name string) (NPC, error) {
	if strings.HasPrefix(name, "cm-") {
		parts := strings.SplitN(name, "-", 3)

		if len(parts) < 3 {
			parts = append(parts, "")
		}

		var deck card.Deck

		if err := deck.UnmarshalText([]byte(parts[1])); err != nil {
			return nil, xerrors.Errorf("npc: decoding cm- NPC identifier: %w", err)
		}

		return &CardMaster{
			Character: room.CharacterByName[parts[2]],
			Deck:      deck,
		}, nil
	}

	switch name {
	case "janet":
		return &Janet{}, nil
	case "bu-gi", "johnny", "kage", "ritchee", "serene", "carmina":
		return &TourneyPlayer{Character: room.CharacterByName[name]}, nil
	case "saved-decks":
		return &SavedDecks{}, nil
	case "tutorial":
		return &CardMaster{
			Character: room.CharacterByName["carmina"],
			Deck:      mustDeck("01H00000000013HSMR"),
		}, nil
	case "carmina2":
		return &CardMaster{
			Character: room.CharacterByName["carmina"],
			Deck:      mustDeck("3P7T52H8MA5273HG842YF7KR"),
		}, nil
	case "chuck":
		return &CardMaster{
			Character: room.CharacterByName["chuck"],
			Deck:      mustDeck("4HH0000007VXYZFG84210GG8"),
		}, nil
	case "arie":
		return &CardMaster{
			Character: room.CharacterByName["arie"],
			Deck:      mustDeck("310J10G84212NANCPAD6K7VW"),
		}, nil
	case "shay":
		return &CardMaster{
			Character: room.CharacterByName["shay"],
			Deck:      mustDeck("511KHRWE631GRD6K9MMA5840"),
		}, nil
	case "crow":
		return &CardMaster{
			Character: room.CharacterByName["crow"],
			Deck:      mustDeck("101AXEPH8MCJ8G845G"),
		}, nil
	case "mender-spam":
		return &MenderSpam{
			Mothiva: time.Now().Unix()&1 == 0,
		}, nil
	case "mender-spam-mothiva":
		return &MenderSpam{
			Mothiva: true,
		}, nil
	case "mender-spam-kali":
		return &MenderSpam{
			Mothiva: false,
		}, nil
	case "tp2-generic":
		return &TourneyPlayer2{
			AdjustWeight: genericNPCWeight,
		}, nil
	case "tp2-janet":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["janet"],
			},
			AdjustWeight: janetWeight,
		}, nil
	case "tp2-bu-gi":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["bu-gi"],
			},
			AdjustWeight: buGiWeight,
		}, nil
	case "tp2-johnny":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["johnny"],
			},
			AdjustWeight: johnnyWeight,
		}, nil
	case "tp2-kage":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["kage"],
			},
			AdjustWeight: kageWeight,
		}, nil
	case "tp2-ritchee":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["ritchee"],
			},
			AdjustWeight: ritcheeWeight,
		}, nil
	case "tp2-serene":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["serene"],
			},
			AdjustWeight: sereneWeight,
		}, nil
	case "tp2-carmina":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["carmina"],
			},
			AdjustWeight: carminaWeight,
		}, nil
	case "tp2-chuck":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["chuck"],
			},
			AdjustWeight: chuckWeight,
		}, nil
	case "tp2-arie":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["arie"],
			},
			AdjustWeight: arieWeight,
		}, nil
	case "tp2-shay":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["shay"],
			},
			AdjustWeight: shayWeight,
		}, nil
	case "tp2-crow":
		return &TourneyPlayer2{
			TourneyPlayer: TourneyPlayer{
				Character: room.CharacterByName["crow"],
			},
			AdjustWeight: crowWeight,
		}, nil
	case "", "generic":
		return &GenericNPC{}, nil
	default:
		return nil, xerrors.Errorf("npc: unknown NPC identifier %q", name)
	}
}
