// +build !headless

package room

import (
	"context"
	"image/color"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/mobile/gl"
	"golang.org/x/xerrors"
)

const mineSlots = 15

type mine struct {
	ictx      *input.Context
	cam       gfx.Camera
	textCam   gfx.Camera
	stageLoad chan *customStage
	stage     *customStage
	batch     *sprites.Batch
	tb        *sprites.TextBatch

	players   []*Player
	player    int
	rot       float32
	blur      float32
	selected  int
	changed   bool
	confirmed bool
	disable3D bool
	onConfirm func(*Character)
}

func loadMine() (interface{}, error) {
	_ = sprites.Cerise1.Preload()
	_ = sprites.Tanjerin1.Preload()

	return loadStage(context.Background(), "img/room/mine.stage")
}

// PreloadMine loads the 3D model for the mine into cache.
func PreloadMine() error {
	_, err := stageCache.Do("img/room/mine.stage", loadMine)
	if err != nil {
		return xerrors.Errorf("room: loading mine: %w", err)
	}

	return nil
}

// NewMine returns a new character selection room.
func NewMine(ctx context.Context, onConfirm func(*Character)) Room {
	r := &mine{
		onConfirm: onConfirm,
	}
	r.cam.SetDefaults()
	r.cam.Position = gfx.Identity()
	r.cam.Offset = gfx.MultiplyMatrix(
		gfx.RotationX(0.0625*math.Pi),
		gfx.MultiplyMatrix(
			gfx.Translation(6.75, 0.25, -5.5),
			gfx.RotationY(-0.125*math.Pi),
		),
	)
	r.cam.Rotation = gfx.Identity()

	r.textCam.SetDefaults()
	r.textCam.Rotation = gfx.Identity()
	r.textCam.Position = gfx.Identity()
	r.textCam.Offset = gfx.Scale(-0.125, -0.125, -0.125)

	i := 0

	settings := internal.LoadSettings()
	initialCharacter := settings.Character

	found := false

	for _, c := range PlayerCharacters {
		if c.Hidden && initialCharacter != c.Name {
			continue
		}

		if c.Name == initialCharacter {
			r.player = i
			found = true
		}

		r.AddPlayer(NewPlayer(c), i)
		i++
	}

	r.ictx = input.GetContext(ctx)

	if !found {
		r.player = int(time.Now().UnixNano()) % len(r.players)
	}

	r.rot = -float32((r.player-1)%mineSlots) * math.Pi * 2 / (mineSlots + 1)

	r.disable3D = settings.Disable3D

	if !r.disable3D {
		for _, sound := range []*audio.Sound{audio.PageFlip, audio.CrowdGasp, audio.Buzzer} {
			go func(s *audio.Sound) { _ = s.Preload() }(sound)
		}

		r.stageLoad = make(chan *customStage, 1)

		go func() {
			m, err := stageCache.Do("img/room/mine.stage", loadMine)
			if err != nil {
				panic(err)
			}

			r.stageLoad <- m.(*customStage)
		}()
	}

	r.ictx.ConsumeWheel()

	return r
}

func (r *mine) AddAudience(a ...*Audience) {
	panic(xerrors.New("room: mine cannot have an audience"))
}

func (r *mine) AddPlayer(p *Player, i int) {
	if len(r.players) <= i {
		r.players = append(r.players, make([]*Player, i-len(r.players)+1)...)
	}

	r.players[i] = p
}

func (r *mine) SetPlayer(i int) {
	r.player = i
}

func (r *mine) Update() {
	playerMod := r.player % mineSlots
	if playerMod < 0 {
		playerMod += mineSlots
	}

	targetRot := -float32(playerMod) * math.Pi * 2 / (mineSlots + 1)
	moveRot := targetRot - r.rot

	for moveRot > math.Pi {
		moveRot -= math.Pi * 2
	}

	for moveRot < -math.Pi {
		moveRot += math.Pi * 2
	}

	r.blur = moveRot / (math.Pi * 2 / (mineSlots + 1)) * 0.125

	r.rot += moveRot * 0.1

	r.cam.Rotation = gfx.RotationY(r.rot)

	r.selected = r.player % len(r.players)
	if r.selected < 0 {
		r.selected += len(r.players)
	}

	if r.players[r.selected] != nil {
		if r.cam.Position[13] == 0 {
			r.cam.Position[13] = r.players[r.selected].Character.Sprites[0].Y1 * 0.75
		} else {
			r.cam.Position[13] = r.cam.Position[13]*0.95 + r.players[r.selected].Character.Sprites[0].Y1*0.0375
		}
	}

	for i := range r.players {
		if r.players[i] != nil {
			r.players[i].Update()
		}
	}

	w, h := gfx.Size()

	gfx.GL.Viewport(0, 0, w, h)

	aspect := float32(w) / float32(h)
	sx, sy := float32(1.0), float32(1.0)

	if aspect > 16.0/9.0 {
		sy = aspect * 9.0 / 16.0
		aspect = 16.0 / 9.0
	}

	r.cam.Perspective = gfx.MultiplyMatrix(
		gfx.Perspective(-60*math.Pi/180, aspect, 0.3, 150),
		gfx.Scale(sx, sy, 1),
	)

	r.textCam.Perspective = gfx.Scale(1, float32(w)/float32(h), 0.01)

	r.ictx.Tick()

	if !r.confirmed {
		switch wheel := r.ictx.ConsumeWheel(); {
		case wheel > 0:
			audio.PageFlip.PlaySound(0, 0, 0)
			r.player--
			r.changed = true
		case wheel < 0:
			audio.PageFlip.PlaySound(0, 0, 0)
			r.player++
			r.changed = true
		case r.ictx.Consume(arcade.BtnConfirm):
			if r.players[r.selected] != nil {
				r.confirmed = true
				for i, p := range r.players {
					if i != r.selected && p != nil {
						p.BecomeAngry(-1)
					}
				}

				if r.changed {
					s := internal.LoadSettings()
					s.Character = r.players[r.selected].Character.Name
					internal.SaveSettings(s)
				}

				audio.CrowdGasp.PlaySound(0, 0, 0)

				if r.onConfirm != nil {
					r.onConfirm(r.players[r.selected].Character)
				}
			} else {
				audio.Buzzer.PlaySound(0, 0, 0)
			}
		case r.ictx.ConsumeAllowRepeat(arcade.BtnLeft, 30):
			audio.PageFlip.PlaySound(0, 0, 0)
			r.player--
			r.changed = true
		case r.ictx.ConsumeAllowRepeat(arcade.BtnRight, 30):
			audio.PageFlip.PlaySound(0, 0, 0)
			r.player++
			r.changed = true
		}
	}
}

func (r *mine) Render() {
	if r.disable3D {
		gfx.GL.ClearColor(0.25, 0.25, 0.25, 1)
		gfx.GL.Clear(gl.COLOR_BUFFER_BIT)
	} else {
		gfx.GL.ClearColor(0, 0, 0, 1)
		gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

		if r.stage != nil {
			r.stage.Render(&r.cam)
		} else {
			select {
			case r.stage = <-r.stageLoad:
			default:
			}
		}

		gfx.GL.Enable(gl.DEPTH_TEST)

		if r.batch == nil {
			r.batch = sprites.NewBatch(&r.cam)
		} else {
			r.batch.Reset(&r.cam)
		}

		for i := mineSlots / 2; i >= -2; i-- {
			unclampedIndex := r.player + i

			index := unclampedIndex % len(r.players)
			if index < 0 {
				index += len(r.players)
			}

			if r.players[index] == nil {
				continue
			}

			slot := (unclampedIndex%mineSlots + mineSlots) % mineSlots
			spoke := float64(slot) * math.Pi * 2 / (mineSlots + 1)

			blur := float32(0.0)

			switch {
			case i < 0:
				blur = r.blur - float32(i)*0.125
				r.players[index].Flip = true
				r.players[index].Color = color.RGBA{191, 191, 191, 191}
			case i == 0:
				r.players[index].Flip = false
				r.players[index].Color = color.RGBA{255, 255, 255, 255}
			case i > 0:
				r.players[index].Flip = false
				r.players[index].Color = color.RGBA{127, 127, 127, 255}
			}

			r.players[index].X = 6.75 * float32(math.Cos(spoke))
			r.players[index].Z = 6.75 * float32(math.Sin(spoke))
			r.players[index].Render(r.batch, sprites.FlagNoDiscard, 0.125*math.Pi-r.rot, 0, blur)
		}

		r.batch.Render()

		gfx.GL.Disable(gl.DEPTH_TEST)
	}

	if r.tb == nil {
		r.tb = sprites.NewTextBatch(&r.textCam)
	} else {
		r.tb.Reset(&r.textCam)
	}

	if r.players[r.selected] != nil {
		border := sprites.FlagBorder

		if r.confirmed {
			border |= sprites.FlagRainbow
		}

		sprites.DrawTextCenteredFuncEx(r.tb, sprites.FontBubblegumSans, r.players[r.selected].Character.DisplayName, 3.5, 0, 0, 1.5, 1.5, func(rune) color.RGBA {
			if r.confirmed {
				return sprites.White
			}

			return sprites.Black
		}, false, border, 0, 0, 0)
		sprites.DrawTextCentered(r.tb, sprites.FontBubblegumSans, r.players[r.selected].Character.DisplayName, 3.5, 0, 0, 1.5, 1.5, sprites.White, false)

		if !r.confirmed {
			sprites.DrawTextCenteredFuncEx(r.tb, sprites.FontD3Streetism, "Select with "+sprites.Button(arcade.BtnConfirm), 3.5, -2.25, 0, 0.65, 0.65, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
			sprites.DrawTextCentered(r.tb, sprites.FontD3Streetism, "Select with "+sprites.Button(arcade.BtnConfirm), 3.5, -2.25, 0, 0.65, 0.65, sprites.White, false)
		}
	}

	if !r.confirmed {
		sprites.DrawTextCenteredFuncEx(r.tb, sprites.FontD3Streetism, "Rotate with "+sprites.Button(arcade.BtnLeft)+" "+sprites.Button(arcade.BtnRight), 3.5, -2.75, 0, 0.65, 0.65, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
		sprites.DrawTextCentered(r.tb, sprites.FontD3Streetism, "Rotate with "+sprites.Button(arcade.BtnLeft)+" "+sprites.Button(arcade.BtnRight), 3.5, -2.75, 0, 0.65, 0.65, sprites.White, false)
	}

	r.tb.Render()
}

func (r *mine) SetStage(context.Context, card.ContentIdentifier) error {
	return xerrors.New("room: cannot change stage of mine")
}
