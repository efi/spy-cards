module SpyCards.Effect {
	export interface OrderEntry {
		type: EffectType;

		negate?: boolean;
		opponent?: boolean;
		each?: boolean;
		late?: boolean;
		generic?: boolean;
		defense?: boolean;
		_reserved6?: boolean;
		_reserved7?: boolean;
	}

	export const order: {
		ignored: OrderEntry[];
		pre: OrderEntry[];
		main: OrderEntry[];
		post: OrderEntry[];
		discard: OrderEntry[];
	} = {
		// no effect during round
		ignored: [
			{ type: EffectType.TP },
			{ type: EffectType.FlavorText },
		],
		// before stats are displayed
		pre: [
			{ type: EffectType.CondApply, late: false },
			{ type: EffectType.CondLimit },
			{ type: EffectType.CondHP, late: false },
			{ type: EffectType.Summon, negate: true },
			{ type: EffectType.Summon, negate: false },
			{ type: EffectType.CondCoin },
			{ type: EffectType.CondCard, opponent: false, each: false },
			{ type: EffectType.Heal },
		],
		// main part of round
		main: [
			{ type: EffectType.Stat, opponent: false },
			{ type: EffectType.CondCard, opponent: true, each: false },
			{ type: EffectType.CondCard, each: true },
			{ type: EffectType.Empower },
			{ type: EffectType.CondStat, late: false },
			{ type: EffectType.Numb },
			{ type: EffectType.Stat, opponent: true },
			{ type: EffectType.CondStat, late: true },
		],
		// after winner is computed
		post: [
			{ type: EffectType.CondWinner },
			{ type: EffectType.CondHP, late: true },
			{ type: EffectType.CondApply, late: true },
		],
		// triggered by other effects; ignore if still present
		discard: [
			{ type: EffectType.CondOnNumb },
		],
	};

	function filterTargets(ctx: Context, card: CardInstance, effect: EffectDef, allowNumb: boolean = false): CardInstance[] {
		return ctx.state.played[effect.opponent ? 2 - card.player : card.player - 1].filter((c) => {
			if ((!allowNumb && c.numb) || c.setup) {
				return false;
			}

			if (ctx.defs.unfilter[c.card.id]) {
				return false;
			}

			if (!effect.generic) {
				return effect.card === c.card.id;
			}

			if (effect.rank !== Rank.None) {
				const rank = c.card.rank();
				if (effect.rank === Rank.Enemy) {
					if (rank !== Rank.Attacker && rank !== Rank.Effect) {
						return false;
					}
				} else if (effect.rank !== rank) {
					return false;
				}
			}

			if (effect.tribe === Tribe.None) {
				return true;
			}

			if (effect.tribe === Tribe.Custom) {
				return c.card.tribes.some((t) => t.tribe === Tribe.Custom && t.custom.name === effect.customTribe);
			}

			return c.card.tribes.some((t) => t.tribe === effect.tribe);
		});
	}

	export async function dispatch(ctx: Context, card: CardInstance, effect: EffectDef) {
		let ok: boolean = null;
		let stat: "ATK" | "DEF";
		let diff: number;
		let player: 1 | 2;
		let targets: CardInstance[];
		switch (effect.type) {
			case EffectType.FlavorText:
				// no effect
				break;
			case EffectType.Stat:
				stat = effect.defense ? "DEF" : "ATK";
				diff = effect.negate ? -effect.amount : effect.amount;
				player = <1 | 2>(effect.opponent ? 3 - card.player : card.player);
				await ctx.effect.modifyStat(card, effect.opponent || card.becomingNumb ? null : card, effect, player, stat, diff);
				break;
			case EffectType.Empower:
				stat = effect.defense ? "DEF" : "ATK";
				diff = effect.negate ? -effect.amount : effect.amount;
				player = <1 | 2>(effect.opponent ? 3 - card.player : card.player);
				targets = filterTargets(ctx, card, effect);
				for (let c of targets) {
					await ctx.effect.modifyStat(card, c, effect, player, stat, diff);
				}
				break;
			case EffectType.Summon:
				const special = effect.defense ? effect.negate ? "both-invisible" : "invisible" :
					effect.negate ? "replace" : null;
				player = <1 | 2>(effect.opponent ? 3 - card.player : card.player);
				const toSummon = [];
				for (let i = 0; i < effect.amount; i++) {
					if (effect.generic) {
						toSummon.push(await randomMatchingCard(ctx, effect));
					} else {
						toSummon.push(ctx.defs.cardsByID[effect.card]);
					}
				}
				if (effect.negate) {
					card.setup = true;
				}
				const toSummonWait = toSummon.filter(Boolean).map((c, i) => ({ c, w: ctx.fx.multiple(i) }));
				for (let { c, w } of toSummonWait) {
					await w;
					await ctx.effect.summonCard(player, card, c, special);
				}
				break;
			case EffectType.Heal:
				player = <1 | 2>(effect.opponent ? 3 - card.player : card.player);
				diff = effect.negate ? -effect.amount : effect.amount;
				if (effect.each) {
					await ctx.effect.multiplyHealing(card, effect, player, diff, effect.generic ? null : !effect.defense);
				} else {
					await ctx.effect.heal(card, effect, player, diff);
				}
				break;
			case EffectType.TP:
				// no effect
				break;
			case EffectType.Numb:
				for (let i = 0; i < effect.amount; i++) {
					let lowest = null;
					let lowestATK = 0;
					let lowestDEF = 0;
					let lowestMin = 0;
					let lowestMax = 0;

					targets = filterTargets(ctx, card, effect);
					for (let c of targets) {
						if (c.numb || c.setup) {
							continue;
						}

						const atk = c.modifiedATK || 0;
						const def = c.modifiedDEF || 0;
						const min = Math.min(atk, def);
						const max = Math.max(atk, def);

						if (lowest && atk === 0 && def === 0) {
							continue;
						}

						if (!lowest || (lowestATK === 0 && lowestDEF === 0 && (atk !== 0 || def !== 0))) {
							lowest = c;
							lowestATK = atk;
							lowestDEF = def;
							lowestMin = min;
							lowestMax = max;
							continue;
						}

						if (max > lowestMax) {
							continue;
						}

						if (max === lowestMax && min > lowestMin) {
							continue;
						}

						if (max === lowestMax && lowestMax == lowestATK) {
							continue;
						}

						lowest = c;
						lowestATK = atk;
						lowestDEF = def;
						lowestMin = min;
						lowestMax = max;
					}

					if (!lowest) {
						break;
					}

					lowest.becomingNumb = true;

					for (let i = 0; i < lowest.effects.length; i++) {
						if (lowest.effects[i].type === EffectType.CondOnNumb) {
							const trigger = lowest.effects.splice(i, 1)[0];
							ctx.state.gameLog.log("Processing effect " + EffectType[trigger.result.type] + " for player " + lowest.player + " card " + lowest.card.displayName() + " (due to numb)");

							await ctx.fx.beforeProcessEffect(lowest, trigger);

							await Effect.dispatch(ctx, lowest, trigger.result);

							await ctx.fx.afterProcessEffect(lowest, trigger);

							i--;
						}
					}

					lowest.numb = true;
					await ctx.effect.recalculateStats(<1 | 2>(3 - card.player));
					await ctx.fx.numb(card, lowest);
				}
				break;
			case EffectType.CondCard:
				targets = filterTargets(ctx, card, effect, true);
				ctx.state.gameLog.log("Matching cards (" + targets.length + "): " + targets.map((c) => c.card.displayName()).join(", "));
				if (effect.each) {
					for (let c of targets) {
						await ctx.fx.assist(card, c);
						await ctx.effect.applyResult(card, effect, effect.result);
					}
				} else {
					ok = effect.negate ? targets.length < effect.amount : targets.length >= effect.amount;
				}
				break;
			case EffectType.CondLimit:
				const count = ctx.state.once[card.player - 1].reduce((n, e) => e === effect ? n + 1 : n, 0);
				ok = effect.negate ?
					count >= effect.amount :
					count < effect.amount;
				ctx.state.once[card.player - 1].push(effect);
				break;
			case EffectType.CondWinner:
				if (effect.negate) {
					ok = effect.opponent ?
						!!ctx.state.winner :
						!ctx.state.winner;
				} else {
					ok = effect.opponent ?
						ctx.state.winner === 3 - card.player :
						ctx.state.winner === card.player;
				}
				break;
			case EffectType.CondApply:
				player = <1 | 2>(effect.opponent ? 3 - card.player : card.player);
				if (effect.late) {
					if (effect.negate) {
						let found = false;
						for (let s of ctx.state.setup[player - 1]) {
							if (s.sourceCard === card && s.originalDesc) {
								s.effects.push(effect.result);
								found = true;
								break;
							}
						}
						if (found) {
							break;
						}
					}
					ctx.state.setup[player - 1].push({
						card: card.card,
						effects: [effect.result],
						originalDesc: effect.negate,
						sourceCard: card
					});
					await ctx.fx.setup(card);
				} else {
					await ctx.effect.summonCard(player, card, card.card, effect.negate ? "setup-original" : "setup-effect", [effect.result]);
				}
				break;
			case EffectType.CondCoin:
				const coins: boolean[] = [];
				for (let i = 0; i < effect.amount; i++) {
					coins.push(await ctx.scg.rand(true, 2) !== 0);
				}
				for (let i = 0; i < coins.length; i++) {
					await ctx.fx.coin(card, effect.negate ? !coins[i] : coins[i], effect.defense, i, coins.length);
				}
				for (let flip of coins) {
					ctx.state.gameLog.log("Player " + card.player + " card " + card.card.displayName() + " coin lands on " + (flip ? "heads" : "tails"));

					if (flip) {
						await ctx.effect.applyResult(card, effect, effect.result);
					} else if (effect.generic) {
						await ctx.effect.applyResult(card, effect, effect.tailsResult);
					}
				}
				break;
			case EffectType.CondHP:
				const hp = ctx.state.hp[effect.opponent ? 2 - card.player : card.player - 1];
				ok = effect.negate ? hp < effect.amount : hp >= effect.amount;
				break;
			case EffectType.CondStat:
				stat = effect.defense ? "DEF" : "ATK";
				const value = ctx.state[stat][effect.opponent ? 2 - card.player : card.player - 1];
				ok = effect.negate ? value < effect.amount : value >= effect.amount;
				break;
			case EffectType.CondPriority:
				ctx.state.gameLog.log("Processing effect " + EffectType[effect.result.type] + " at priority of " + EffectType[effect.amount]);
				await Effect.dispatch(ctx, card, effect.result);
				break;
			case EffectType.CondOnNumb:
				// no effect when run from here
				break;
			default:
				throw new Error("unhandled effect type " + effect.type + " (" + EffectType[effect.type] + ")");
		}
		if (ok !== null) {
			ctx.state.gameLog.log("Condition is " + (ok ? "met." : "NOT met."));
		}
		if (ok) {
			await ctx.effect.applyResult(card, effect, effect.result);
		}
	}

	export function isHidden(card: CardDef, effect: EffectDef): boolean {
		const firstHide = card.effects.findIndex((e) => e.type === EffectType.FlavorText && e.negate) + 1;
		if (firstHide <= 0) {
			return false;
		}

		for (let i = firstHide; i < card.effects.length; i++) {
			if (isInTree(card.effects[i], effect)) {
				return true;
			}
		}

		return false;
	}

	function isInTree(tree: EffectDef, effect: EffectDef): boolean {
		if (tree === effect) {
			return true;
		}

		if (tree.result && isInTree(tree.result, effect)) {
			return true;
		}

		if (tree.tailsResult && isInTree(tree.tailsResult, effect)) {
			return true;
		}

		return false;
	}

	async function randomMatchingCard(ctx: Context, effect: EffectDef): Promise<CardDef> {
		let choices: CardDef[];
		if (effect.tribe === Tribe.None && effect.negate) {
			ctx.state.gameLog.log("Selecting a random " + (effect.rank === Rank.None ? "" : rankName(effect.rank)) + " card that does not have a Carmina-style summon...");
			choices = ctx.defs.nonRandom[effect.rank].filter((c) => !ctx.defs.banned[c.id]);
		} else {
			ctx.state.gameLog.log("Selecting a random card with filters: Rank: " + rankName(effect.rank) + " Tribe: " + tribeName(effect.tribe, effect.customTribe));
			choices = ctx.defs.getAvailableCards(effect.rank, effect.tribe, effect.customTribe);
		}

		if (!choices.length) {
			ctx.state.gameLog.log("(no cards are available for this filter)");
			return null;
		}

		const index = await ctx.scg.rand(true, choices.length);
		ctx.state.gameLog.log("Selected card " + (index + 1) + " of " + choices.length + ": " + choices[index].displayName());
		return choices[index];
	}
}
