// Package crt renders a screen-space CRT monitor effect.
package crt

// DisableCRT bypasses the CRT effect.
var DisableCRT bool

// CRT is a screen-space graphical filter.
type CRT struct {
	NoiseX          float64
	Offset          [2]float64
	RGBNoise        float64
	SineNoiseWidth  float64
	SineNoiseScale  float64
	SineNoiseOffset float64
	ScanLineTail    float64
	ScanLineSpeed   float64
	ScreenSize      float64
	Darkening       float64
	ScanLineDarker  float64
	ScanLineDarker2 float64
	FadeOut         float64
}

// SetDefaults sets the CRT settings to their default values.
func (c *CRT) SetDefaults() {
	c.NoiseX = 0
	c.Offset = [2]float64{0, 0}
	c.RGBNoise = 0
	c.SineNoiseWidth = 0
	c.SineNoiseScale = 0
	c.SineNoiseOffset = 0
	c.ScanLineTail = 2
	c.ScanLineSpeed = 100
	c.ScreenSize = 0.15
	c.Darkening = 0.1
	c.ScanLineDarker = 4
	c.ScanLineDarker2 = 0.5
	c.FadeOut = 1
}
