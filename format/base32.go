package format

import (
	"encoding/base32"
	"strings"
)

// Crockford32 is an implementation of Crockford Base 32.
// See https://www.crockford.com/base32.html
var Crockford32 = base32.NewEncoding("0123456789ABCDEFGHJKMNPQRSTVWXYZ").WithPadding(base32.NoPadding)

// Encode32 encodes a slice of bytes as a Crockford Base 32 string.
func Encode32(src []byte) string {
	return Crockford32.EncodeToString(src)
}

// Encode32Space encodes a slice of bytes as a Crockford Base 32 string with
// a space after every 4 characters.
func Encode32Space(src []byte) string {
	enc := Encode32(src)
	b := make([]byte, 0, len(enc)*5/4)

	for len(enc) > 4 {
		b = append(b, enc[:4]...)
		b = append(b, ' ')
		enc = enc[4:]
	}

	return string(append(b, enc...))
}

// Decode32 decodes a Crockford Base 32 string, applying the recommended
// transformations to fix typos.
func Decode32(src string) ([]byte, error) {
	src = strings.ToUpper(src)
	src = strings.ReplaceAll(src, "O", "0")
	src = strings.ReplaceAll(src, "I", "1")
	src = strings.ReplaceAll(src, "L", "1")
	src = strings.ReplaceAll(src, " ", "")
	src = strings.ReplaceAll(src, "-", "")
	src = strings.ReplaceAll(src, "_", "")

	return Crockford32.DecodeString(src)
}
