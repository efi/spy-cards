// +build !js !wasm
// +build !headless

package internal

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"

	"golang.org/x/xerrors"
)

func dataStoragePath() (string, error) {
	if t := os.TempDir(); runtime.GOOS == "android" && filepath.Base(t) == "cache" {
		// HACK
		return filepath.Join(filepath.Dir(t), "files"), nil
	}

	dir, err := os.UserConfigDir()
	if err != nil {
		return "", xerrors.Errorf("internal: cannot determine data storage path: %w", err)
	}

	dir = filepath.Join(dir, "spy-cards.lubar.me")

	return dir, nil
}

func LoadData(key string) ([]byte, error) {
	base, err := dataStoragePath()
	if err != nil {
		return nil, err
	}

	b, err := ioutil.ReadFile(filepath.Join(base, key+".dat")) /*#nosec*/
	if os.IsNotExist(err) {
		return nil, nil
	}

	return b, err
}

func StoreData(key string, val []byte) error {
	base, err := dataStoragePath()
	if err != nil {
		return err
	}

	if err := os.MkdirAll(base, 0700); err != nil {
		return err
	}

	return ioutil.WriteFile(filepath.Join(base, key+".dat"), val, 0600)
}
