"use strict";
var SpyCards;
(function (SpyCards) {
    function createUnknownCardEl(back) {
        const el = document.createElement("div");
        el.classList.add("card", "unknown", "flipped");
        el.classList.add("card-type-" + ["enemy", "mini-boss", "boss"][back]);
        const backEl = document.createElement("div");
        backEl.setAttribute("aria-label", "Back of " + ["an Enemy", "a Mini-Boss", "a Boss"][back] + " Card");
        backEl.classList.add("back");
        el.appendChild(backEl);
        return el;
    }
    SpyCards.createUnknownCardEl = createUnknownCardEl;
    function flipCard(el, force) {
        el.classList.toggle("flipped", force);
        const front = el.querySelector(":scope > .front");
        const back = el.querySelector(":scope > .back");
        if (el.classList.contains("flipped")) {
            if (front) {
                front.setAttribute("aria-hidden", "true");
            }
            if (back) {
                back.removeAttribute("aria-hidden");
            }
        }
        else {
            if (front) {
                front.removeAttribute("aria-hidden");
            }
            if (back) {
                back.setAttribute("aria-hidden", "true");
            }
        }
    }
    SpyCards.flipCard = flipCard;
    function createCardEl(defs, card) {
        const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
        const tpMul = (sg && sg.m && (sg.m & 16)) ? 2 : 1;
        const el = document.createElement("div");
        el.classList.add("card");
        switch (card.rank()) {
            case SpyCards.Rank.Attacker:
                el.classList.add("card-type-attacker");
                break;
            case SpyCards.Rank.Effect:
                el.classList.add("card-type-effect");
                break;
            case SpyCards.Rank.MiniBoss:
                el.classList.add("card-type-mini-boss");
                break;
            case SpyCards.Rank.Boss:
                el.classList.add("card-type-boss");
                break;
        }
        const front = document.createElement("div");
        front.classList.add("front");
        front.setAttribute("aria-label", SpyCards.rankName(card.rank()) + " card: " + card.displayName());
        el.appendChild(front);
        const back = document.createElement("div");
        back.classList.add("back");
        back.setAttribute("aria-hidden", "true");
        switch (card.rank()) {
            case SpyCards.Rank.Attacker:
            case SpyCards.Rank.Effect:
                back.setAttribute("aria-label", "Back of an Enemy Card");
                break;
            case SpyCards.Rank.MiniBoss:
                back.setAttribute("aria-label", "Back of a Mini-Boss Card");
                break;
            case SpyCards.Rank.Boss:
                back.setAttribute("aria-label", "Back of a Boss Card");
                break;
        }
        el.appendChild(back);
        const name = document.createElement("span");
        name.classList.add("card-name");
        const nameSquish = document.createElement("span");
        nameSquish.classList.add("squish");
        nameSquish.textContent = card.displayName();
        name.appendChild(nameSquish);
        front.appendChild(name);
        const tp = document.createElement("span");
        tp.classList.add("card-tp");
        const effectiveTP = card.effectiveTP() * tpMul;
        tp.title = effectiveTP < 0 ? "Gain " + (-effectiveTP) + " teamwork points on the turn you play this card." :
            isFinite(effectiveTP) ? "Teamwork Points Required: " + effectiveTP :
                "This card cannot be played through normal means.";
        tp.textContent = isNaN(effectiveTP) ? "NaN" : isFinite(effectiveTP) ? String(effectiveTP) : (effectiveTP < 0 ? "-∞" : "∞");
        if (effectiveTP < 0) {
            tp.setAttribute("data-negative", "");
        }
        if (effectiveTP === Infinity) {
            tp.setAttribute("data-infinity", "");
        }
        if (effectiveTP !== Infinity || !defs.banned[card.id]) {
            front.appendChild(tp);
        }
        const portrait = document.createElement("div");
        portrait.classList.add("portrait");
        portrait.setAttribute("data-x", String(card.portrait & 15));
        portrait.setAttribute("data-y", String(card.portrait >> 4));
        front.appendChild(portrait);
        if (card.customPortrait) {
            const customPortrait = document.createElement("img");
            if (card.portrait === 255) {
                customPortrait.src = user_image_base_url + CrockfordBase32.encode(card.customPortrait) + ".png";
            }
            else {
                customPortrait.src = "data:image/png;base64," + Base64.encode(card.customPortrait);
            }
            customPortrait.crossOrigin = "anonymous";
            customPortrait.width = 128;
            customPortrait.height = 128;
            portrait.appendChild(customPortrait);
        }
        const descWrapper = document.createElement("div");
        descWrapper.classList.add("card-desc-wrapper");
        const desc = document.createElement("div");
        desc.setAttribute("role", "list");
        desc.classList.add("card-desc", "squishy");
        descWrapper.appendChild(desc);
        front.appendChild(descWrapper);
        const tribes = document.createElement("div");
        tribes.classList.add("card-tribe-placeholders");
        tribes.style.setProperty("--numtribes", String(card.tribes.filter((t) => !t.custom || t.custom.name.indexOf("_") !== 0).length));
        front.appendChild(tribes);
        for (let i = 0; i < card.tribes.length; i++) {
            if (card.tribes[i].custom && card.tribes[i].custom.name.charAt(0) === "_") {
                continue;
            }
            const tribeEl = document.createElement("span");
            tribeEl.classList.add("card-tribe-placeholder");
            tribeEl.setAttribute("data-tribe", card.tribeName(i));
            tribeEl.title = "Tribe: " + (card.tribeName(i) === "???" ? "Unknown" : card.tribeName(i));
            const tribeSquish = document.createElement("span");
            tribeSquish.classList.add("squish");
            tribeSquish.textContent = card.tribeName(i);
            tribeEl.appendChild(tribeSquish);
            if (card.tribes[i].custom) {
                tribeEl.style.setProperty("--custom-color", "#" + (0x1000000 + card.tribes[i].custom.rgb).toString(16).substr(1));
            }
            tribes.appendChild(tribeEl);
        }
        for (let effect of card.effects) {
            const effectEl = effect.createEl(defs, card);
            effectEl.setAttribute("role", "listitem");
            desc.appendChild(effectEl);
        }
        if (!disableDoSquish) {
            requestAnimationFrame(() => doSquish(el));
        }
        return el;
    }
    SpyCards.createCardEl = createCardEl;
    function createEffectEl(defs, card, effect, childOf) {
        const el = document.createElement("span");
        if (!effect || effect.type === null) {
            el.classList.add("card-null");
            return el;
        }
        switch (effect.type) {
            case SpyCards.EffectType.FlavorText:
                el.classList.add("card-flavor-text");
                el.classList.toggle("hide-remaining-effects", effect.negate);
                el.title = "Flavor text: " + effect.text;
                el.textContent = effect.text;
                break;
            case SpyCards.EffectType.Stat:
                el.classList.add((childOf ? "card-effect-" : "card-") + (effect.defense ? "def" : "atk"));
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                if (!effect.opponent) {
                    const stat = effect.defense ? "Defense" : "Attack";
                    el.title = (childOf ? "Increase " + stat + " by " : "Card " + stat + ": ") + (effect.negate ? "-" : "") + effect.amount;
                }
                el.textContent = isFinite(effect.amount) ? String(effect.amount) : "∞";
                break;
            case SpyCards.EffectType.Empower:
                if (childOf && childOf.classList.contains("card-condition-limit") && effect.generic && effect.rank === SpyCards.Rank.None && effect.tribe !== SpyCards.Tribe.None && !effect.opponent && !effect.defense) {
                    childOf.classList.add("possibly-unity");
                }
                el.classList.add("card-effect-empower");
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                el.setAttribute("data-stat", effect.defense ? "DEF" : "ATK");
                el.setAttribute("data-count", String(effect.amount));
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                    }
                }
                else {
                    el.setAttribute("data-card", defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?");
                }
                break;
            case SpyCards.EffectType.Summon:
                el.classList.add(effect.negate ?
                    (effect.generic ? "card-effect-replace-random" : "card-effect-replace-card") :
                    (effect.generic ? "card-effect-summon-random" : "card-effect-summon-card"));
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                el.setAttribute("data-count", String(effect.amount));
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                    }
                }
                else {
                    el.setAttribute("data-card", defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?");
                    if (effect.card === card.id) {
                        el.setAttribute("data-same-card", "");
                    }
                }
                break;
            case SpyCards.EffectType.Heal:
                el.classList.add(effect.each ? "card-effect-multiply-healing" : "card-effect-heal");
                if (childOf && childOf.classList.contains("card-condition-winner") && !effect.each && !effect.opponent) {
                    childOf.classList.add("possibly-lifesteal");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                if (effect.generic) {
                    el.setAttribute("data-generic", "");
                }
                if (effect.defense) {
                    el.setAttribute("data-defense", "");
                }
                el.textContent = isFinite(effect.amount) ? String(effect.amount) : "∞";
                break;
            case SpyCards.EffectType.TP:
                const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
                const tpMul = (sg && sg.m && (sg.m & 16)) ? 2 : 1;
                el.classList.add("card-effect-tp");
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                el.textContent = isFinite(effect.amount) ? String(effect.amount * tpMul) : "∞";
                break;
            case SpyCards.EffectType.Numb:
                el.classList.add("card-effect-numb");
                el.textContent = isFinite(effect.amount) ? String(effect.amount) : "∞";
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                el.title = "Numb " + (isFinite(effect.amount) ? "cards (limit " + effect.amount + ")" : "all cards");
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                        el.title += " with rank: " + SpyCards.rankName(effect.rank);
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                        el.title += " with tribe: " + SpyCards.tribeName(effect.tribe, effect.customTribe);
                    }
                }
                else {
                    const cardName = defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?";
                    el.setAttribute("data-card", cardName);
                    el.title += " with name: " + cardName;
                }
                break;
            case SpyCards.EffectType.CondCard:
                el.classList.add("card-condition-card");
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                    }
                }
                else {
                    el.setAttribute("data-card", defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?");
                }
                el.setAttribute("data-count", String(effect.amount));
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.each) {
                    el.setAttribute("data-each", "");
                }
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondLimit:
                el.classList.add("card-condition-limit");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                el.setAttribute("data-count", String(effect.amount));
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondWinner:
                el.classList.add("card-condition-winner");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondApply:
                // negate flag used internally
                el.classList.add(effect.late ? "card-condition-setup" : "card-condition-apply");
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.late) {
                    let count = 1;
                    for (let child = effect.result; child.type === SpyCards.EffectType.CondApply; child = child.result) {
                        if (effect.late === child.late && !child.opponent && effect.negate === child.negate) {
                            count++;
                        }
                        else {
                            break;
                        }
                    }
                    el.setAttribute("data-count", String(count));
                }
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondCoin:
                el.classList.add("card-condition-coin");
                el.setAttribute("data-count", String(effect.amount));
                const heads = document.createElement("span");
                heads.classList.add("card-coin-heads");
                const headsEffect = createEffectEl(defs, card, effect.result, el);
                heads.appendChild(headsEffect);
                heads.title = "Apply effect if coin lands on Heads";
                el.appendChild(heads);
                if (effect.generic) {
                    const tails = document.createElement("span");
                    tails.classList.add("card-coin-tails");
                    const tailsEffect = createEffectEl(defs, card, effect.tailsResult, el);
                    tails.appendChild(tailsEffect);
                    tails.title = "Apply effect if coin lands on Tails";
                    el.appendChild(tails);
                }
                break;
            case SpyCards.EffectType.CondHP:
                el.classList.add("card-condition-hp");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.late) {
                    el.setAttribute("data-late", "");
                }
                el.setAttribute("data-count", String(effect.amount));
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondStat:
                el.classList.add("card-condition-stat");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.late) {
                    el.setAttribute("data-late", "");
                }
                el.setAttribute("data-stat", effect.defense ? "DEF" : "ATK");
                el.setAttribute("data-count", String(effect.amount));
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondPriority:
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondOnNumb:
                el.classList.add("card-condition-on-numb");
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            default:
                throw new Error("unhandled effect type: " + effect.type + " (" + SpyCards.EffectType[effect.type] + ")");
        }
        return el;
    }
    SpyCards.createEffectEl = createEffectEl;
})(SpyCards || (SpyCards = {}));
