package spoilerguard

import (
	"encoding/json"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
)

type QuestProgress int8

const (
	QuestUnavailable QuestProgress = -1
	QuestAvailable   QuestProgress = 0
	QuestTaken       QuestProgress = 1
	QuestComplete    QuestProgress = 2
)

type Data struct {
	Quest            QuestProgress `json:"q"`
	MetCarmina       bool          `json:"t"`
	CarminaApproved  bool          `json:"a"`
	Deck             card.Deck     `json:"d"`
	SeenSpiedEnemies []uint8       `json:"s"`
	Menu             uint32        `json:"m,omitempty"`
}

const localStorageKey = "spy-cards-spoiler-guard-v0"

func LoadData() *Data {
	b, err := internal.LoadData(localStorageKey)
	if err != nil {
		panic(err)
	}

	if len(b) == 0 {
		return nil
	}

	var d Data

	if err := json.Unmarshal(b, &d); err != nil {
		panic(err)
	}

	return &d
}

func SaveData(d *Data) {
	if d == nil {
		if err := internal.StoreData(localStorageKey, nil); err != nil {
			panic(err)
		}

		return
	}

	b, err := json.Marshal(d)
	if err != nil {
		panic(err)
	}

	if err := internal.StoreData(localStorageKey, b); err != nil {
		panic(err)
	}
}

/*
	TODO:
	export enum GuardState {
		QuestLocked,
		QuestNotAccepted,
		QuestNotCompleted,
		NotMetCarmina,
		CardsNotApproved,
		NotAllSeen,
		NotAllSpied,
		Disabled
	}

	const cannotPlayState: { [state: number]: boolean } = {
		[GuardState.QuestLocked]: true,
		[GuardState.QuestNotAccepted]: true,
		[GuardState.QuestNotCompleted]: true,
		[GuardState.NotMetCarmina]: true,
		[GuardState.CardsNotApproved]: true
	}

	export function getSpoilerGuardState(): GuardState {
		const save = getSpoilerGuardData();
		if (!save) {
			return GuardState.Disabled;
		}

		if (save.q !== 2) {
			return [
				GuardState.QuestLocked,
				GuardState.QuestNotAccepted,
				GuardState.QuestNotCompleted
			][save.q + 1];
		}
		if (!save.t) {
			return GuardState.NotMetCarmina;
		}
		if (!save.a) {
			return GuardState.CardsNotApproved;
		}

		let seenAllEnemies = true;
		let spiedAllEnemies = true;
		const enemyBitmap = Base64.decode(save.s);
		for (let card of cardEnemyIDs) {
			const i = card >> 2;
			const j = (card & 3) << 1;
			if (!(enemyBitmap[i] & (1 << j))) {
				seenAllEnemies = false;
			}
			if (!(enemyBitmap[i] & (2 << j))) {
				spiedAllEnemies = false;
			}
		}

		if (!seenAllEnemies) {
			return GuardState.NotAllSeen;
		}
		if (!spiedAllEnemies) {
			return GuardState.NotAllSpied;
		}
		return GuardState.Disabled;
	}
*/
