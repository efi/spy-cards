"use strict";
var SpyCards;
(function (SpyCards) {
    var Audio;
    (function (Audio) {
        const audioDebug = new URLSearchParams(location.search).has("audioDebug") ? (...message) => console.debug("AUDIO:", ...message) : (...message) => { };
        Audio.actx = new (window.AudioContext || window.webkitAudioContext)();
        const musicGain = Audio.actx.createGain();
        musicGain.connect(Audio.actx.destination);
        Audio.musicFFT = Audio.actx.createAnalyser();
        Audio.musicFFT.connect(musicGain);
        const soundsGain = Audio.actx.createGain();
        soundsGain.connect(Audio.actx.destination);
        addEventListener("click", function () {
            if (Audio.actx.state === "suspended") {
                Audio.actx.resume();
            }
        }, true);
        Audio.musicVolume = 0;
        Audio.soundsVolume = 0;
        Audio.ignoreAudio = 0;
        let activeMusic;
        let forcedMusic;
        function adoptForcedMusic() {
            if (activeMusic) {
                activeMusic.stop();
            }
            activeMusic = forcedMusic;
            forcedMusic = null;
            if (!Audio.musicVolume) {
                stopMusic();
            }
        }
        Audio.adoptForcedMusic = adoptForcedMusic;
        function playingMusic() {
            return !!activeMusic;
        }
        Audio.playingMusic = playingMusic;
        const fetchAudioPromise = new Promise((resolve) => Audio.startFetchAudio = resolve);
        function setVolume(music, sounds) {
            Audio.musicVolume = music;
            Audio.soundsVolume = sounds;
            const settings = SpyCards.loadSettings();
            settings.audio.music = music;
            settings.audio.sounds = sounds;
            SpyCards.saveSettings(settings);
            musicGain.gain.setValueAtTime(music, Audio.actx.currentTime);
            soundsGain.gain.setValueAtTime(sounds, Audio.actx.currentTime);
            if (!music) {
                stopMusic();
            }
        }
        Audio.setVolume = setVolume;
        addEventListener("spy-cards-settings-changed", () => {
            const s = SpyCards.loadSettings().audio;
            Audio.musicVolume = s.music;
            Audio.soundsVolume = s.sounds;
            musicGain.gain.setValueAtTime(Audio.musicVolume, Audio.actx.currentTime);
            soundsGain.gain.setValueAtTime(Audio.soundsVolume, Audio.actx.currentTime);
        });
        function showUI(form) {
            const initialSettings = SpyCards.loadSettings().audio;
            Audio.musicVolume = initialSettings.music;
            Audio.soundsVolume = initialSettings.sounds;
            musicGain.gain.setValueAtTime(Audio.musicVolume, Audio.actx.currentTime);
            soundsGain.gain.setValueAtTime(Audio.soundsVolume, Audio.actx.currentTime);
            const reenableAudio = SpyCards.UI.button("Enable Audio", ["enable-audio", "audio-error"], async () => {
                await Audio.actx.resume();
                updateUI();
                Audio.Sounds.Confirm.play();
            });
            const enableAudio = SpyCards.UI.button("Enable Audio", ["enable-audio"], async () => {
                await Audio.actx.resume();
                setVolume(0.6, 0.6);
                updateUI();
                Audio.Sounds.Confirm.play();
            });
            const enableAudioMusic = SpyCards.UI.button("Music", ["enable-audio", "split", "split-1"], () => {
                setVolume(Audio.musicVolume ? 0 : 0.6, Audio.soundsVolume);
                updateUI();
            });
            const enableAudioSound = SpyCards.UI.button("Sounds", ["enable-audio", "split", "split-2"], () => {
                setVolume(Audio.musicVolume, Audio.soundsVolume ? 0 : 0.6);
                updateUI();
                Audio.Sounds.Confirm.play();
            });
            function updateUI() {
                SpyCards.UI.remove(reenableAudio);
                SpyCards.UI.remove(enableAudio);
                SpyCards.UI.remove(enableAudioMusic);
                SpyCards.UI.remove(enableAudioSound);
                if (Audio.actx.state === "suspended" && (Audio.musicVolume || Audio.soundsVolume)) {
                    form.appendChild(reenableAudio);
                    Audio.actx.resume().then(updateUI);
                }
                else if (Audio.musicVolume || Audio.soundsVolume) {
                    if (window.requestIdleCallback) {
                        requestIdleCallback(Audio.startFetchAudio);
                    }
                    enableAudioMusic.classList.toggle("enabled", Audio.musicVolume !== 0);
                    enableAudioMusic.setAttribute("aria-pressed", String(Audio.musicVolume !== 0));
                    enableAudioSound.classList.toggle("enabled", Audio.soundsVolume !== 0);
                    enableAudioSound.setAttribute("aria-pressed", String(Audio.soundsVolume !== 0));
                    form.appendChild(enableAudioMusic);
                    form.appendChild(enableAudioSound);
                }
                else {
                    enableAudio.setAttribute("aria-pressed", "false");
                    form.appendChild(enableAudio);
                }
            }
            updateUI();
        }
        Audio.showUI = showUI;
        async function fetchAudio(name) {
            audioDebug("waiting to fetch audio: ", name);
            await fetchAudioPromise;
            audioDebug("fetching audio: ", name);
            const response = await fetch(name.indexOf("/") === -1 ? "audio/" + name + ".opus" : name);
            audioDebug("received response for audio: ", name);
            const body = await response.arrayBuffer();
            audioDebug("finished downloading audio: ", name);
            audioDebug("audio context state before decoding ", name, ": ", Audio.actx.state);
            if (Audio.actx.state === "suspended") {
                audioDebug("waiting to resume audio context for: ", name);
                await Audio.actx.resume();
                audioDebug("resumed audio context for: ", name);
            }
            const bufferPromise = Audio.actx.decodeAudioData(body);
            audioDebug("audio decoding promise for ", name, ": ", bufferPromise);
            const buffer = await bufferPromise;
            audioDebug("decoded audio: ", name);
            return buffer;
        }
        class Sound {
            constructor(name, pitch = 1, volume = 1) {
                this.name = name;
                this.buffer = fetchAudio(name);
                this.pitch = pitch;
                this.volume = volume;
            }
            async createSource() {
                Audio.startFetchAudio();
                const source = Audio.actx.createBufferSource();
                source.buffer = await this.buffer;
                source.playbackRate.setValueAtTime(this.pitch, Audio.actx.currentTime);
                return source;
            }
            async preload() {
                Audio.startFetchAudio();
                await this.buffer;
            }
            async play(delay = 0, overridePitch, overrideVolume) {
                if (!Audio.soundsVolume) {
                    return;
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                let dest = soundsGain;
                if (this.volume !== 1) {
                    if (!this.gain) {
                        this.gain = Audio.actx.createGain();
                        this.gain.connect(dest);
                        this.gain.gain.setValueAtTime(this.volume, Audio.actx.currentTime);
                    }
                    dest = this.gain;
                }
                if (overrideVolume) {
                    const gain = Audio.actx.createGain();
                    gain.connect(dest);
                    gain.gain.setValueAtTime(overrideVolume, Audio.actx.currentTime);
                    dest = gain;
                }
                const src = await this.createSource();
                if (overridePitch) {
                    src.playbackRate.setValueAtTime(overridePitch * this.pitch, Audio.actx.currentTime);
                }
                src.connect(dest);
                src.start(Audio.actx.currentTime + delay);
            }
            async playAsMusic(delay = 0) {
                if (!Audio.musicVolume) {
                    return;
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                let dest = Audio.musicFFT;
                if (this.volume !== 1) {
                    if (!this.musicGain) {
                        this.musicGain = Audio.actx.createGain();
                        this.musicGain.connect(dest);
                        this.musicGain.gain.setValueAtTime(this.volume, Audio.actx.currentTime);
                    }
                    dest = this.musicGain;
                }
                stopMusic();
                const src = await this.createSource();
                src.connect(dest);
                src.start(Audio.actx.currentTime + delay);
                activeMusic = src;
            }
        }
        Audio.Sound = Sound;
        class Music extends Sound {
            constructor(name, start, end) {
                super(name);
                this.start = start;
                this.end = end;
            }
            async createSource() {
                const source = await super.createSource();
                source.loop = true;
                source.loopStart = this.start;
                source.loopEnd = this.end;
                return source;
            }
            async play(delay = 0) {
                if (!Audio.musicVolume) {
                    return;
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                stopMusic();
                const src = await this.createSource();
                src.connect(Audio.musicFFT);
                src.start(Audio.actx.currentTime + delay);
                activeMusic = src;
            }
            async forcePlay(delay = 0) {
                if (forcedMusic) {
                    forcedMusic.stop();
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                const src = await this.createSource();
                src.connect(Audio.musicFFT);
                src.start(Audio.actx.currentTime + delay);
                forcedMusic = src;
            }
        }
        Audio.Music = Music;
        function stopMusic() {
            if (activeMusic) {
                activeMusic.stop();
            }
            activeMusic = null;
        }
        Audio.stopMusic = stopMusic;
        Audio.Sounds = {
            AtkFail: new Sound("AtkFail"),
            AtkSuccess: new Sound("AtkSuccess"),
            BattleStart0: new Sound("BattleStart0"),
            Buzzer: new Sound("Buzzer"),
            CardSound2: new Sound("CardSound2", 1.2, 0.5),
            Charge: new Sound("Charge"),
            Coin: new Sound("Coin"),
            Confirm: new Sound("Confirm"),
            Confirm1: new Sound("Confirm1", 0.4, 0.5),
            CrowdCheer2: new Sound("CrowdCheer2", 1.2),
            CrowdClap: new Sound("CrowdClap"),
            CrowdGasp: new Sound("CrowdGasp"),
            CrowdGaspSlow: new Sound("CrowdGasp", 0.75),
            Damage0: new Sound("Damage0"),
            Death3: new Sound("Death3"),
            Fail: new Sound("Fail"),
            Heal: new Sound("Heal"),
            Lazer: new Sound("Lazer"),
            PageFlip: new Sound("PageFlip"),
            PageFlipCancel: new Sound("PageFlip", 0.7),
            Toss11: new Sound("Toss11"),
            FBCountdown: new Sound("FBCountdown"),
            FBDeath: new Sound("FBDeath"),
            FBFlower: new Sound("FBFlower"),
            FBGameOver: new Sound("FBGameOver"),
            FBPoint: new Sound("FBPoint"),
            FBStart: new Sound("FBStart"),
            MiteKnightIntro: new Sound("MiteKnightIntro"),
            MKDeath: new Sound("MKDeath"),
            MKGameOver: new Sound("MKGameOver"),
            MKHit: new Sound("MKHit"),
            MKHit2: new Sound("MKHit2"),
            MKKey: new Sound("MKKey"),
            MKOpen: new Sound("MKOpen"),
            MKPotion: new Sound("MKPotion"),
            MKShield: new Sound("MKShield"),
            MKStairs: new Sound("MKStairs"),
            MKWalk: new Sound("MKWalk"),
            PeacockSpiderNPCSummonSuccess: new Sound("PeacockSpiderNPCSummonSuccess"),
            Shot2: new Sound("Shot2"),
        };
        Audio.Songs = {
            Miniboss: new Music("Miniboss", 20.55, 87),
            Bounty: new Music("Bounty", 6.7, 52.5),
            Inside2: new Music("Inside2", 10.3, 72),
            FlyingBee: new Music("FlyingBee", 999, 999),
            MiteKnight: new Music("MiteKnight", 999, 999),
            TermiteLoop: new Music("TermiteLoop", 999, 999),
        };
    })(Audio = SpyCards.Audio || (SpyCards.Audio = {}));
})(SpyCards || (SpyCards = {}));
