package flowerjourney

import (
	"fmt"
	"math"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// RenderAttract implements game.Interface.
func (fj *FlowerJourney) RenderAttract(progress int, fadeIn, fadeOut bool) {
	if fj.batch == nil {
		fj.batch = sprites.NewBatch(&fj.state.Camera)
	} else {
		fj.batch.Reset(&fj.state.Camera)
	}

	fj.batch.Append(sprites.AttractFJ, 0.9, 2.25, 0, 2.5, 2.5, sprites.White, 0, 0, 0, 0)
	fj.batch.Render()

	if fj.tb == nil {
		fj.tb = sprites.NewTextBatch(&fj.state.Camera)
	} else {
		fj.tb.Reset(&fj.state.Camera)
	}

	sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "Flower Journey", 0, 4, 0, 2, 2, sprites.White, true)
	sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(arcade.BtnConfirm)+" TO START!", 0, 0, 0, 1, 1, sprites.Rainbow, true)

	fj.tb.Render()
}

// Render implements game.Interface.
func (fj *FlowerJourney) Render() {
	if fj.batch == nil {
		fj.batch = sprites.NewBatch(&fj.state.Camera)
	} else {
		fj.batch.Reset(&fj.state.Camera)
	}

	if fj.gameState == stateEnding {
		deadAnimationProgress := float32(math.Min(math.Max((float64(fj.state.TickCount)+30-float64(fj.gameOverReady))/60, 0), 1))
		fj.state.Camera.Position = gfx.Translation(0, 70+deadAnimationProgress*20, 3*(1-deadAnimationProgress))

		fj.batch.Append(sprites.FJBack, 0.9, 2.25, 0, 10000, 10000, fj.backColor, 0, 0, 0, 0) // -10
	} else {
		fj.batch.Append(sprites.FJBack, 0.9, 2.25, 0, 200, 200, fj.backColor, 0, 0, 0, 0) // -10
	}

	first := true

	for _, cloud := range fj.clouds {
		if !fj.passedHive && first {
			fj.batch.Append(sprites.FJHive, float32(cloud.x), float32(cloud.y), 0, cloud.scale, cloud.scale, sprites.White, 0, 0, 0, 0) // -9

			first = false

			continue
		}

		fj.batch.Append(sprites.FJCloud[cloud.sprite], float32(cloud.x), float32(cloud.y), 0, cloud.scale, cloud.scale, sprites.White, 0, 0, 0, 0) // -9
	}

	for x := -15 + math.Mod(fj.gpos1, fj.gwidth1); x <= 15; x += fj.gwidth1 {
		fj.batch.Append(sprites.FJGround[1], float32(x), 0.3, 0, 2, 2.25, sprites.White, 0, 0, 0, 0) // -8
	}

	for _, wall := range fj.walls {
		fj.batch.Append(sprites.FJWall, float32(wall.x), float32(wall.y), 0, 2, 2.5, sprites.White, 0, 0, 0, 0)          // -7
		fj.batch.Append(sprites.FJWall, float32(wall.x), float32(wall.y+wall.gap), 0, 2, 2.5, sprites.White, 0, 0, 0, 0) // -7
	}

	for x := -15 + math.Mod(fj.gpos0, fj.gwidth0); x <= 15; x += fj.gwidth0 {
		fj.batch.Append(sprites.FJGround[0], float32(x), -0.4, 0, 2, 2.25, sprites.White, 0, 0, 0, 0) // -6
	}

	if fj.drawPlayer {
		fj.batch.Append(sprites.FJBee[fj.playerSprite], float32(fj.playerX), float32(fj.playerY), 0, 2, 2, sprites.White, 0, 0, fj.playerAngle, 0) // -5
	}

	if fj.itemEnabled {
		sx := float32(2)
		if fj.flipItemX {
			sx = -sx
		}

		if fj.itemType == itemFlower {
			fj.batch.Append(sprites.FJFlower, float32(fj.itemX), float32(fj.itemY), 0, sx, 2, sprites.White, 0, 0, 0, 0)
		} else {
			fj.batch.Append(sprites.FJHoney, float32(fj.itemX), float32(fj.itemY), 0, sx, 2, sprites.White, 0, 0, 0, 0)
		}
	}

	if fj.enemyEnabled {
		fj.batch.Append(sprites.FJWasp[fj.enemySprite], float32(fj.enemyX), float32(fj.enemyY), 0, 2, 2, sprites.White, 0, 0, 0, 0)
	}

	fj.batch.Render()

	if fj.tb == nil {
		fj.tb = sprites.NewTextBatch(&fj.state.Camera)
	} else {
		fj.tb.Reset(&fj.state.Camera)
	}

	sprites.DrawText(fj.tb, sprites.FontD3Streetism, fj.scoreText, -5, -0.75, 0, 1, 1, sprites.White)

	fj.state.DrawButtons(fj.tb, -5, 0, 0, 1)

	if fj.state.Playback != nil {
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "Recorded by "+fj.state.Playback.PlayerName, 3, -0.35, 0, 0.5, 0.5, sprites.White, false)
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, fj.state.Playback.Start.Format("Jan 2, 2006"), 3, -0.75, 0, 0.5, 0.5, sprites.White, false)
	}

	if fj.countdown == 0 {
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "GO!", 0, 2, 0, 2, 2, sprites.Rainbow, true)
	} else if fj.countdown > 0 {
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, fmt.Sprintf("%.0f", fj.countdown), 0, 2, 0, 2, 2, sprites.White, true)
	}

	if fj.lastCombo > fj.state.TickCount-game.FrameRate/2 {
		color := sprites.White
		if fj.combo >= 5 {
			color = sprites.Rainbow
		}

		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, fmt.Sprintf("x%d", fj.combo), float32(fj.playerX), float32(fj.comboY)+float32(fj.state.TickCount-fj.lastCombo)/(game.FrameRate/2), 0, 1, 1, color, true)
	}

	if fj.gameState == statePaused {
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "PAUSED", 0, 3.45, 0, 1, 1, sprites.White, true)
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(arcade.BtnConfirm)+" TO CONTINUE", 0, 2.05, 0, 1, 1, sprites.White, true)
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(arcade.BtnCancel)+" TO EXIT", 0, 1, 0, 1, 1, sprites.White, true)
	}

	if fj.gameState == stateEnding {
		sprites.DrawTextCentered(fj.tb, sprites.FontD3Streetism, "GAME OVER", 0, 22, 0, 2, 2, sprites.White, true)
		sprites.DrawTextShadow(fj.tb, sprites.FontD3Streetism, "Seed: "+fj.state.Seed, -7, 18, 0, 0.7, 0.7, sprites.Gray)
	}

	fj.tb.Render()
}
