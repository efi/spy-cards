// +build !headless

package visual

import (
	"context"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

// Layout handles arranging a grid of cards on the screen.
type Layout struct {
	Cards []*Card

	MinPerRow       int
	MaxPerRow       int
	OnSelect        func(*Card)
	InputFocus      *interface{}
	InputFocusLeft  func()
	InputFocusRight func()
	InputFocusUp    func()
	InputFocusDown  func()
	MinScale        float32
	BaseScale       float32
	MaxScale        float32
	HoverScale      float32
	MarginTop       float32
	MarginLeft      float32
	MarginBottom    float32
	MarginRight     float32
	Padding         float32
	GrowDx, GrowDy  float32

	scale    float32
	rx, ry   float32
	aspect   float32
	scroll   float32
	wheel    float32
	momentum float32

	CenterLastRow bool
	AllowScroll   bool
	Reverse       bool
	PinCorner     uint8
	Slide         bool
	fromKB        bool

	cam  gfx.Camera
	ictx *input.Context

	perRow  int
	active  int
	clicked int
}

// NewLayout creates a new card layout.
func NewLayout(ctx context.Context) *Layout {
	go func() { _ = sprites.CardFront.Preload() }()
	go func() { _ = sprites.CardFrontWindow.Preload() }()
	go func() { _ = sprites.Portraits[0].Preload() }()

	l := &Layout{
		MinPerRow:   3,
		MaxPerRow:   5,
		MinScale:    1.75,
		BaseScale:   2,
		MaxScale:    3.5,
		HoverScale:  1,
		Padding:     0.05,
		AllowScroll: true,

		ictx:    input.GetContext(ctx),
		active:  -1,
		clicked: -1,
	}

	l.cam.SetDefaults()
	l.cam.Position = gfx.Identity()
	l.cam.Rotation = gfx.Identity()
	l.cam.Offset = gfx.Identity()

	return l
}

// Focus attaches the input focus to this Layout.
func (l *Layout) Focus() {
	if l.InputFocus != nil {
		*l.InputFocus = l
	}

	if l.active < 0 || l.active >= len(l.Cards) {
		l.active = 0
		l.fromKB = true
	}
}

// Update processes one frame of input.
func (l *Layout) Update(modTP map[card.ID]int64) {
	if l.active >= len(l.Cards) {
		l.active = -1
	}

	realW, realH := gfx.Size()
	l.aspect = float32(realW) / float32(realH)
	l.cam.Perspective = gfx.Scale(-64.0/float32(realW), -64.0/float32(realH), -0.05)

	w := int(float32(realW) * (1 - l.MarginLeft - l.MarginRight))
	h := int(float32(realH) * (1 - l.MarginTop - l.MarginBottom))

	l.perRow = int(float32(w) / 100 / l.BaseScale)
	l.scale = float32(w) / 100

	switch {
	case l.perRow <= l.MinPerRow:
		l.perRow = l.MinPerRow
		l.scale /= float32(l.perRow)
	case l.perRow >= l.MaxPerRow:
		l.perRow = l.MaxPerRow
		l.scale /= float32(l.perRow)
	default:
		l.scale = l.BaseScale
	}

	if l.scale > l.MaxScale {
		l.scale = l.MaxScale
	}

	if l.scale < l.MinScale {
		l.scale = l.MinScale
	}

	l.ictx.Tick()

	mx, my, click := l.ictx.Mouse()
	lastX, lastY, lastClick := l.ictx.LastMouse()

	allowClick := true

	if my < l.MarginTop {
		allowClick = false
	}

	if my > 1-l.MarginBottom {
		allowClick = false
	}

	if mx < l.MarginLeft {
		allowClick = false
	}

	if mx > 1-l.MarginRight {
		allowClick = false
	}

	l.rx = mx
	l.ry = my

	if mx != lastX || my != lastY {
		if click && lastClick {
			amount := (lastY - my) * float32(h) / 32 / l.scale / (cardHeight + cardWidth*l.Padding)

			if l.AllowScroll {
				l.scroll += amount
				l.momentum += amount
			}
		}

		l.fromKB = false
	}

	if l.InputFocus == nil || *l.InputFocus == l {
		switch wheel := l.ictx.ConsumeWheel(); {
		case wheel != 0:
			if l.AllowScroll {
				l.wheel += wheel * 5 / cardHeight / l.scale
			}
		case !l.Reverse && l.ictx.ConsumeAllowRepeat(arcade.BtnLeft, 30),
			l.Reverse && l.ictx.ConsumeAllowRepeat(arcade.BtnRight, 30):
			switch {
			case l.active > 0:
				l.active--
			case !l.Reverse && l.InputFocusLeft != nil:
				l.InputFocusLeft()
			case l.Reverse && l.InputFocusRight != nil:
				l.InputFocusRight()
			}

			l.fromKB = true
		case !l.Reverse && l.ictx.ConsumeAllowRepeat(arcade.BtnRight, 30),
			l.Reverse && l.ictx.ConsumeAllowRepeat(arcade.BtnLeft, 30):
			switch {
			case l.active+1 < len(l.Cards):
				l.active++
			case !l.Reverse && l.InputFocusRight != nil:
				l.InputFocusRight()
			case l.Reverse && l.InputFocusLeft != nil:
				l.InputFocusLeft()
			}

			l.fromKB = true
		case l.ictx.ConsumeAllowRepeat(arcade.BtnUp, 30):
			switch {
			case l.active > l.perRow:
				l.active -= l.perRow
			case l.active > 0:
				l.active = 0
			case l.InputFocusUp != nil:
				l.InputFocusUp()
			}

			l.fromKB = true
		case l.ictx.ConsumeAllowRepeat(arcade.BtnDown, 30):
			switch {
			case l.active+l.perRow < len(l.Cards):
				if l.active < 0 {
					l.active = 0
				}

				l.active += l.perRow
			case l.active < len(l.Cards)-1:
				l.active = len(l.Cards) - 1
			case l.InputFocusDown != nil:
				l.InputFocusDown()
			}

			l.fromKB = true
		case l.ictx.Consume(arcade.BtnConfirm):
			if l.active >= 0 && l.OnSelect != nil {
				l.fromKB = true
				l.OnSelect(l.Cards[l.active])
			}
		}
	}

	var (
		hoveredCard      *Card
		hoveredCardIndex int
	)

	cw := cardWidth * l.scale * (1 + l.Padding)
	ch := l.scale * (cardHeight + cardWidth*l.Padding)

	for i, c := range l.Cards {
		if c.Def != nil {
			c.ModTP = modTP[c.Def.ID]
		} else {
			c.ModTP = 0
		}

		numInRow := l.perRow
		if l.CenterLastRow && i/l.perRow == (len(l.Cards)-1)/l.perRow && len(l.Cards)%l.perRow != 0 {
			numInRow = len(l.Cards) % l.perRow
		}

		var baseX, baseY float32

		rowWidth := float32(numInRow-1) * cw

		switch l.PinCorner {
		case 0:
			// no pin
			centerX := (l.MarginLeft + (1 - l.MarginRight)) - 1
			baseX = -rowWidth/2 + float32(realW)/64*centerX
			baseY = l.scroll*ch + float32(h)/64 - ch/2 - float32(realH)/64*l.MarginTop + float32(realH)/64*l.MarginBottom
		case 1:
			// top left
			baseX = -float32(realW)/64 + cw/2
			baseY = float32(realH)/64 - ch/2
		case 2:
			// top right
			baseX = float32(realW)/64 - rowWidth - cw/2
			baseY = float32(realH)/64 - ch/2
		case 3:
			// bottom right
			baseX = float32(realW)/64 - rowWidth - cw/2
			baseY = -float32(realH)/64 + ch/2
		case 4:
			// bottom left
			baseX = -float32(realW)/64 + cw/2
			baseY = -float32(realH)/64 + ch/2
		}

		if c.hover == nil {
			c.hover = &HoverTilt{}
			c.hover.x0 = sprites.CardFront.X0
			c.hover.y0 = sprites.CardFront.Y0
			c.hover.x1 = sprites.CardFront.X1
			c.hover.y1 = sprites.CardFront.Y1
		}

		dx := i % l.perRow
		if l.Reverse {
			dx = numInRow - dx - 1
		}

		cx := float32(dx)*cw + baseX + c.hover.dx*cw
		cy := -float32(i/l.perRow)*ch + baseY + c.hover.dy*ch
		scale := l.scale + float32(c.hover.hover)*l.HoverScale/255

		if l.Slide && (c.hover.x != 0 || c.hover.y != 0) {
			c.hover.x = c.hover.x*0.9 + cx*0.1
			c.hover.y = c.hover.y*0.9 + cy*0.1

			if c.hover.hover == 0 {
				c.hover.scale = c.hover.scale*0.9 + scale*0.1
			} else {
				c.hover.scale = scale
			}
		} else {
			c.hover.x = cx
			c.hover.y = cy
			c.hover.scale = scale
		}

		c.hover.computeCursor(&l.cam, l.rx, l.ry)
		dist := c.hover.falloff

		if c.hover.hover != 0 && (l.GrowDx != 0 || l.GrowDy != 0) {
			c.hover.x += float32(c.hover.hover) * l.GrowDx * cw / 255
			c.hover.y += float32(c.hover.hover) * l.GrowDy * ch / 255
			c.hover.computeCursor(&l.cam, l.rx, l.ry)
		}

		if l.fromKB {
			c.hover.offX = 0
			c.hover.offY = 0
		}

		if lastClick && !click && l.clicked == l.active && l.clicked == i && dist < cardNearThreshold && c.hover.hover != 0 && !l.ictx.IsMouseDrag(10) && l.OnSelect != nil && allowClick {
			l.OnSelect(c)
		}

		if l.fromKB {
			dist = 1000

			if i == l.active && (l.InputFocus == nil || *l.InputFocus == l) {
				dist = 1
			}
		}

		if dist >= cardNearThreshold {
			if c.hover.hover < cardHoverOutSpeed {
				c.hover.hover = 0
			} else {
				c.hover.hover -= cardHoverOutSpeed
			}
		} else if hoveredCard == nil || hoveredCard.hover.hover == 0 {
			hoveredCard = c
			hoveredCardIndex = i
		}

		switch {
		case c.flipDir < 0 && c.flip < uint8(-c.flipDir):
			c.flipDir = 0
			c.flip = 0
		case c.flipDir > 0 && c.flip > 255-uint8(c.flipDir):
			c.flipDir = 0
			c.flip = 255
		default:
			c.flip += uint8(c.flipDir)
		}

		var visibleCoins int

		for _, coin := range c.coins {
			if coin.invisibleTimer != 0 {
				coin.invisibleTimer--
			}

			if coin.invisibleTimer == 0 {
				visibleCoins++
			}

			if coin.revealTimer != 0 {
				coin.revealTimer--
			}
		}

		var positionedCoins int

		for _, coin := range c.coins {
			if coin.invisibleTimer != 0 {
				continue
			}

			cols := 3

			if visibleCoins <= 4 {
				cols = 2
			}

			rows := (visibleCoins + cols - 1) / cols

			col := positionedCoins % cols
			row := positionedCoins / cols

			if row == rows-1 && visibleCoins%cols != 0 {
				cols = visibleCoins % cols
			}

			dx := float32(col) - float32(cols)/2 + 0.5
			dy := -(float32(row) - float32(rows)/2 + 0.5)

			coin.dx = coin.dx*0.9 + dx*0.1
			coin.dy = coin.dy*0.9 + dy*0.1

			positionedCoins++
		}
	}

	if !click {
		l.clicked = -1
	}

	if hoveredCard != nil {
		if l.InputFocus != nil && allowClick {
			*l.InputFocus = l
		}

		if hoveredCard.hover.hover > 255-cardHoverInSpeed {
			hoveredCard.hover.hover = 255
		} else {
			hoveredCard.hover.hover += cardHoverInSpeed
			l.active = hoveredCardIndex
		}

		if l.clicked == -1 && click {
			l.clicked = l.active
		}
	}

	if click {
		l.momentum *= 0.9
	} else {
		l.scroll += l.momentum * 0.1
		l.momentum *= 0.95
	}

	l.scroll += l.wheel * 0.1
	l.wheel *= 0.9

	if l.fromKB && l.AllowScroll {
		l.scroll = l.scroll*0.9 + (-float32(h)/32/ch/2+0.5+float32(l.active/l.perRow))*0.1
	}

	if !click || !l.AllowScroll {
		if l.scroll < 0 {
			l.scroll *= 0.9
		}

		if bottom := float32((len(l.Cards)+l.perRow-1)/l.perRow) - float32(h)/32/ch; l.scroll > bottom {
			l.scroll = (l.scroll-bottom)*0.9 + bottom
		}
	}
}

// Render draws the card layout to the screen.
func (l *Layout) Render(ctx context.Context, pass int) {
	for _, c := range l.Cards {
		if c.hover.mins[0] > 1.5 || c.hover.maxs[0] < -1.5 || c.hover.mins[1] > 1.5 || c.hover.maxs[1] < -1.5 {
			continue
		}

		switch {
		case c.hover.falloff >= cardNearThreshold && c.hover.hover == 0:
			// render inactive cards on pass 0
			if pass == 0 {
				l.renderCard(ctx, c)
			}
		case c.hover.falloff < cardNearThreshold && c.hover.hover == 0:
			// render card behind currently active card
			if pass == 1 {
				l.renderCard(ctx, c)
			}
		case c.hover.falloff >= cardNearThreshold && c.hover.hover != 0:
			// render card in front of inactive cards
			if pass == 1 {
				l.renderCard(ctx, c)
			}
		case c.hover.falloff < cardNearThreshold:
			// render active card last
			if pass == 2 {
				l.renderCard(ctx, c)
			}
		}
	}
}

func (l *Layout) renderCard(ctx context.Context, c *Card) {
	c.hover.Render(&l.cam, func(cam *gfx.Camera) {
		var e *card.EffectDef
		if c.Active != nil {
			e = c.Active.ActiveEffect
		}

		c.Render(ctx, cam, e)
	})
}
