// +build js,wasm

package internal

import "syscall/js"

// Await implements the JavaScript await operator.
func Await(promise js.Value) (js.Value, error) {
	type resultPair struct {
		v   js.Value
		err error
	}

	ch := make(chan resultPair, 1)

	thenFunc := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		ch <- resultPair{args[0], nil}

		return js.Undefined()
	})
	defer thenFunc.Release()

	catchFunc := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		ch <- resultPair{js.Undefined(), js.Error{Value: args[0]}}

		return js.Undefined()
	})
	defer catchFunc.Release()

	promise.Call("then", thenFunc, catchFunc)

	result := <-ch

	return result.v, result.err
}
