//go:generate stringer -type FilterType -trimprefix Filter

package card

import (
	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

// Filter is a card filter, introduced in card format 5.
type Filter []FilterComponent

// FilterType is a type of card filter.
type FilterType uint8

// Constants for FilterType.
const (
	FilterSingleCard FilterType = 1
	FilterTribe      FilterType = 16
	FilterNotTribe   FilterType = 32
	FilterRank       FilterType = 48
)

// FilterComponent is a single component of a card filter.
type FilterComponent struct {
	Type        FilterType
	Rank        Rank
	Tribe       Tribe
	CustomTribe string
	CardID      ID
}

// Marshal encodes the card filter.
func (f *Filter) Marshal(w *format.Writer) error {
	if len(*f) == 1 && (*f)[0].Type == FilterSingleCard {
		w.Write(FilterSingleCard)
		w.UVarInt(uint64((*f)[0].CardID))

		return nil
	}

	for _, e := range *f {
		switch e.Type {
		case FilterTribe, FilterNotTribe:
			w.Write(uint8(e.Type) | uint8(e.Tribe))

			if e.Tribe == TribeCustom {
				w.String(e.CustomTribe)
			} else if e.Tribe >= 15 {
				return xerrors.Errorf("card: invalid filter tribe: %v", e.Tribe)
			}
		case FilterRank:
			w.Write(uint8(e.Type) | uint8(e.Rank))

			if e.Rank >= 6 {
				return xerrors.Errorf("card: invalid filter rank: %v", e.Rank)
			}
		default:
			return xerrors.Errorf("card: invalid filter component type: %v", e.Type)
		}
	}

	w.Write(uint8(0))

	return nil
}

// Unmarshal decodes a card filter.
func (f *Filter) Unmarshal(r *format.Reader) (err error) {
	defer format.Catch(&err)

	var filter Filter

	for {
		switch ty := FilterType(r.Byte()); {
		case ty == 0:
			*f = filter

			return nil
		case ty == FilterSingleCard:
			if filter != nil {
				return xerrors.New("card: SingleCard cannot be a filter component")
			}

			*f = []FilterComponent{
				{
					Type:   FilterSingleCard,
					CardID: ID(r.UVarInt()),
				},
			}

			return nil
		case ty&0xf0 == FilterTribe || ty&0xf0 == FilterNotTribe:
			filter = append(filter, FilterComponent{
				Type:  ty & 0xf0,
				Tribe: Tribe(ty & 0xf),
			})

			switch filter[len(filter)-1].Tribe {
			case TribeCustom:
				filter[len(filter)-1].CustomTribe = r.String()
			case TribeNone:
				return xerrors.Errorf("card: invalid filter tribe: %v", filter[len(filter)-1].Tribe)
			}
		case ty&0xf8 == FilterRank:
			filter = append(filter, FilterComponent{
				Type: ty & 0xf8,
				Rank: Rank(ty & 0x7),
			})

			if filter[len(filter)-1].Rank > Enemy {
				return xerrors.Errorf("card: invalid filter rank: %v", filter[len(filter)-1].Rank)
			}
		}
	}
}

// IsSingleCard returns true if this filter is a single card filter.
func (f Filter) IsSingleCard() bool {
	return len(f) == 1 && f[0].Type == FilterSingleCard
}
