package matchnet

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"math/bits"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
	"github.com/pion/webrtc/v3"
	"golang.org/x/xerrors"
)

func (gc *GameConn) onSecureMessage(msg webrtc.DataChannelMessage) {
	if msg.IsString {
		log.Println("ERROR: received secure message of type string")

		return
	}

	select {
	case gc.secureBuf <- msg.Data:
	default:
		log.Println("ERROR: secure message buffer is full; dropping message")
	}
}

func (gc *GameConn) onControlMessage(msg webrtc.DataChannelMessage) {
	if msg.IsString {
		log.Println("ERROR: received control message of type string")

		return
	}

	if len(msg.Data) == 0 {
		log.Println("ERROR: received control message with no body")

		return
	}

	switch controlMessageType(msg.Data[0]) {
	case msgCustomCards:
		select {
		case gc.customCardData <- msg.Data[1:]:
		case <-time.After(time.Minute):
			log.Println("WARNING: discarding custom card data")
		}
	case msgSpoilerGuard:
		select {
		case gc.spoilerGuardData <- msg.Data[1:]:
		case <-time.After(time.Minute):
			log.Println("WARNING: discarding spoiler guard data")
		}
	case msgCosmeticData:
		gc.stateLock.Lock()
		if gc.recvCosmetic != nil {
			log.Println("WARNING: discarding cosmetic data")
		} else {
			err := json.Unmarshal(msg.Data[1:], &gc.recvCosmetic)
			if err != nil {
				log.Println("ERROR: decoding cosmetic data:", err)
			}
		}
		gc.stateLock.Unlock()
	case msgVersion:
		if !gc.isHost {
			if i := bytes.IndexByte(msg.Data, '-'); i != -1 {
				*gc.mode = string(msg.Data[i+1:])
				msg.Data = msg.Data[:i]
			}
		}

		if vn := internal.VersionNumber(); !bytes.Equal(msg.Data[1:], vn) {
			// TODO: display error to user
			log.Println("ERROR: version mismatch:", string(vn), "!=", string(msg.Data[1:]))
		}
	case msgReady:
		if gc.recvReady {
			log.Println("ERROR: duplicate msgReady from opponent")

			return
		}

		if err := gc.SCG.PromiseTurn(msg.Data[1:]); err != nil {
			log.Println("ERROR: receiving opponent ready message:", err)
		}

		gc.checkReady(&gc.recvReady)
	case msgGameQuit:
		log.Println("TODO: opponent sent msgQuit") // TODO
	case msgRematch:
		log.Println("TODO: rematch") // TODO
	case msgInHand:
		gc.recvInHand <- msg.Data[1:]
	case msgModifyCards:
		gc.recvModifyCards <- msg.Data[1:]
	default:
		log.Println("ERROR: unhandled control message", msg.Data)
	}
}

func (gc *GameConn) exchangeData(b []byte) ([]byte, error) {
	var err error

	if cs, _ := gc.State(); cs == StateUsingRelay {
		err = gc.sc.sendRelayedMessage(&relayedMessage{
			Type:    msgRelay,
			Channel: "secure",
			Payload: b,
		})

		if err != nil {
			gc.setState(StateRelayFailed)
		}
	} else {
		err = gc.secure.Send(b)
	}

	if err != nil {
		return nil, xerrors.Errorf("matchnet: sending SCG message: %w", err)
	}

	return <-gc.secureBuf, nil
}

func (gc *GameConn) send(ty controlMessageType, b []byte) error {
	b = append([]byte{byte(ty)}, b...)

	if cs, _ := gc.State(); cs == StateUsingRelay {
		err := gc.sc.sendRelayedMessage(&relayedMessage{
			Type:    msgRelay,
			Channel: "control",
			Payload: b,
		})
		if err != nil {
			gc.setState(StateRelayFailed)

			return xerrors.Errorf("matchnet: sending relayed control message failed: %w", err)
		}

		return nil
	}

	return gc.control.Send(b)
}

func (gc *GameConn) SendDeck(ctx context.Context, deck card.Deck) {
	go func() {
		backs := make(card.UnknownDeck, len(deck))
		for i, c := range deck {
			backs[i] = gc.cards.Card(c).Rank.Back()
		}

		opponentBacks, err := gc.SCG.SetDeck(gc.exchangeData, deck, backs)
		if err != nil {
			panic(err)
		}

		gc.opponentDeck <- opponentBacks
	}()
}

func (gc *GameConn) RecvDeck() (card.UnknownDeck, bool) {
	select {
	case d := <-gc.opponentDeck:
		return d, true
	default:
		return nil, false
	}
}

func (gc *GameConn) WaitRecvDeck() card.UnknownDeck {
	return <-gc.opponentDeck
}

func (gc *GameConn) SendCosmeticData(c card.CosmeticData) error {
	b, err := json.Marshal(c)
	if err != nil {
		return xerrors.Errorf("matchnet: encoding cosmetic data: %w", err)
	}

	return gc.send(msgCosmeticData, b)
}

func (gc *GameConn) RecvCosmeticData() *card.CosmeticData {
	gc.stateLock.Lock()
	c := gc.recvCosmetic
	gc.stateLock.Unlock()

	return c
}

func (gc *GameConn) SendReady(turnData *card.TurnData, perspective uint8) {
	var tap format.Writer

	tap.UVarInt(turnData.Ready[perspective-1])

	for _, id := range turnData.Played[perspective-1] {
		tap.UVarInt(uint64(id))
	}

	promise, err := gc.SCG.PrepareTurn(tap.Data())
	if err != nil {
		log.Println("matchnet: preparing ready:", err)

		return
	}

	if err := gc.send(msgReady, promise); err != nil {
		log.Println("matchnet: sending ready:", err)
	}

	gc.checkReady(&gc.sentReady)
}

func (gc *GameConn) RecvReady(turnData *card.TurnData, perspective uint8) bool {
	select {
	case b := <-gc.opponentReady:
		gc.decodeReady(turnData, perspective, b)

		return true
	default:
		return false
	}
}

func (gc *GameConn) WaitRecvReady(turnData *card.TurnData, perspective uint8) {
	b := <-gc.opponentReady

	gc.decodeReady(turnData, perspective, b)
}

func (gc *GameConn) decodeReady(turnData *card.TurnData, perspective uint8, b []byte) {
	var r format.Reader

	r.Init(b)

	turnData.Ready[2-perspective] = r.UVarInt()
	turnData.Played[2-perspective] = make([]card.ID, bits.OnesCount64(turnData.Ready[2-perspective]))

	for i := range turnData.Played[2-perspective] {
		turnData.Played[2-perspective][i] = card.ID(r.UVarInt())
	}
}

func (gc *GameConn) checkReady(b *bool) {
	gc.stateLock.Lock()

	*b = true

	if gc.recvReady && gc.sentReady {
		gc.sentReady, gc.recvReady = false, false

		go func() {
			data, err := gc.SCG.ConfirmTurn(gc.exchangeData)
			if err != nil {
				log.Println("ERROR: confirming turn:", err)
			}

			gc.opponentReady <- data
		}()
	}

	gc.stateLock.Unlock()
}

func (gc *GameConn) OpponentReady() bool {
	gc.stateLock.Lock()
	r := gc.recvReady
	gc.stateLock.Unlock()

	return r
}

func (gc *GameConn) ExchangeInHand(data []byte) []byte {
	if err := gc.send(msgInHand, data); err != nil {
		log.Println("ERROR: failed to send InHand message:", err)
	}

	return <-gc.recvInHand
}

func (gc *GameConn) SendModifiedCards(m []card.ModifiedCardPosition) {
	var w format.Writer

	w.UVarInt(uint64(len(m)))

	for _, c := range m {
		if c.InHand {
			w.SVarInt(int64(c.Position))
		} else {
			w.SVarInt(^int64(c.Position))
		}

		w.UVarInt(uint64(c.CardID))
	}

	gc.send(msgModifyCards, w.Data())
}

func (gc *GameConn) RecvModifiedCards() []card.ModifiedCardPosition {
	b := <-gc.recvModifyCards

	var r format.Reader

	r.Init(b)

	m := make([]card.ModifiedCardPosition, r.UVarInt())

	for i := range m {
		pos := r.SVarInt()

		if pos >= 0 {
			m[i].InHand = true
			m[i].Position = uint64(pos)
		} else {
			pos = ^pos
			m[i].InHand = false
			m[i].Position = uint64(pos)
		}

		m[i].CardID = card.ID(r.UVarInt())
	}

	return m
}

type (
	VerifyInitFunc = func(card.Deck, card.Deck, card.UnknownDeck, []byte, []byte, []byte) error
	VerifyTurnFunc = func(seed, seed2 []byte, turn *card.TurnData) error
)

func (gc *GameConn) FinalizeMatch(verifyInit VerifyInitFunc, verifyTurn VerifyTurnFunc) error {
	return gc.SCG.Finalize(gc.exchangeData, func(deck card.Deck, backs card.UnknownDeck) error {
		return verifyInit(gc.SCG.localDeckInitial, deck, backs, gc.SCG.Seed(RNGShared), gc.SCG.Seed(RNGLocal), gc.SCG.Seed(RNGRemote))
	}, func(t *scgTurn) error {
		return verifyTurn(t.seed, t.seed2, &t.data)
	})
}
