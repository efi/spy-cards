"use strict";
var SpyCards;
(function (SpyCards) {
    async function selectCharacter(ctx) {
        if (ctx.matchData.selectedCharacter) {
            return ctx.matchData.selectedCharacter;
        }
        const roomRPC = await SpyCards.Native.roomRPC();
        if (roomRPC == null) {
            return SpyCards.loadSettings().character || "tanjerin";
        }
        const mine = await new Promise((resolve) => roomRPC.newMine(resolve));
        const selected = new Promise((resolve) => roomRPC.onMineSelected(resolve));
        roomRPC.setRoom(mine);
        return await Promise.race([
            async function () {
                const c = await selected;
                await sleep(500);
                return c;
            }(),
            async function () {
                await ctx.ui.fatalErrorPromise;
                roomRPC.exit();
                return null;
            }()
        ]);
    }
    async function runSpyCardsGame(ctx) {
        try {
            ctx.net.cgc.sendVersion(spyCardsVersion + spyCardsVersionVariableSuffix);
            SpyCards.UI.remove(ctx.ui.h1);
            ctx.ui.status.textContent = "Connected to opponent. Setting up card game...";
            await ctx.net.cgc.initSecure(ctx.player);
            SpyCards.Audio.Songs.Inside2.play();
            const stage = new SpyCards.TheRoom.Stage();
            let npc = null;
            try {
                const params = new URLSearchParams(location.search);
                if (params.has("npc")) {
                    npc = SpyCards.AI.getNPC(params.get("npc"));
                }
            }
            catch (ex) {
                debugger;
            }
            ctx.ui.form.style.display = "none";
            const alreadySelected = !!ctx.matchData.selectedCharacter;
            const selectedCharacter = alreadySelected ? ctx.matchData.selectedCharacter :
                (npc ? (await npc.pickPlayer()).name : await selectCharacter(ctx));
            ctx.ui.form.style.display = "";
            if (!selectedCharacter) {
                // we had a fatal error before a character was selected
                return;
            }
            ctx.matchData.selectedCharacter = selectedCharacter;
            await stage.init();
            ctx.ui.fatalErrorPromise.then(() => {
                // make sure the stage goes away if we have a fatal error
                return stage.deinit();
            });
            stage.setFlipped(ctx.player === 2);
            const audience = await stage.createAudience(ctx.scg.seed, ctx.matchData.rematchCount);
            const myPlayer = new SpyCards.TheRoom.Player(ctx.player, selectedCharacter);
            stage.setPlayer(myPlayer);
            if (!alreadySelected) {
                ctx.net.cgc.sendCosmeticData({
                    character: selectedCharacter
                });
            }
            ctx.state = new SpyCards.MatchState(ctx.defs, ctx.npcCtx ? ctx.npcCtx.state : null);
            ctx.state.roundSetupComplete = false;
            ctx.state.roundSetupDelayed = [];
            ctx.state.players[ctx.player - 1] = myPlayer;
            ctx.net.cgc.recvCosmeticData.then((cosmetic) => {
                const p = new SpyCards.TheRoom.Player((3 - ctx.player), cosmetic.character);
                ctx.state.players[2 - ctx.player] = p;
                stage.setPlayer(p);
            });
            ctx.ui.status.textContent = "Waiting for opponent to send Spoiler Guard data...";
            const remoteSpoilerGuard = await ctx.net.cgc.recvSpoilerGuard;
            await SpyCards.SpoilerGuard.banSpoilerCards(ctx.defs, remoteSpoilerGuard);
            ctx.ui.status.textContent = "Select your deck!";
            const deck = npc ? await npc.createDeck(ctx.defs) : await SpyCards.Decks.selectDeck(ctx.ui.status, ctx.defs);
            ctx.ui.status.textContent = "Waiting for opponent to select their deck...";
            const opponentCardBacks = SpyCards.Decks.decodeBacks(await ctx.net.cgc.initDeck(SpyCards.Decks.encodeBinary(ctx.defs, deck), SpyCards.Decks.encodeBacks(deck.map((c) => c.getBackID()))));
            if (opponentCardBacks.length !== ctx.defs.rules.cardsPerDeck) {
                throw new Error("opponent sent deck with " + opponentCardBacks.length + " cards, but game rules require " + ctx.defs.rules.cardsPerDeck);
            }
            for (let i = 0; i < opponentCardBacks.length; i++) {
                let expected;
                if (i < ctx.defs.rules.bossCards) {
                    expected = 2;
                }
                else if (i < ctx.defs.rules.bossCards + ctx.defs.rules.miniBossCards) {
                    expected = 1;
                }
                else {
                    expected = 0;
                }
                if (opponentCardBacks[i] !== expected) {
                    throw new Error("opponent sent invalid deck (wrong card back at position " + (i + 1) + ")");
                }
            }
            SpyCards.Audio.stopMusic();
            SpyCards.Audio.Sounds.BattleStart0.play();
            if (spyCardsVersionSuffix === "-custom")
                SpyCards.Audio.Songs.Bounty.play(0.5);
            else
                SpyCards.Audio.Songs.Miniboss.play(0.5);
            SpyCards.UI.html.classList.add("in-match");
            document.scrollingElement.scrollTop = 0;
            document.querySelectorAll(".game-log").forEach((el) => {
                SpyCards.UI.remove(el);
            });
            document.body.appendChild(ctx.state.gameLog.el);
            ctx.ui.h1.classList.remove("hidden");
            ctx.state.deck[ctx.player - 1] = deck.map((c) => ({
                card: c,
                back: c.getBackID(),
                el: c.createEl(ctx.defs),
                player: ctx.player
            }));
            ctx.state.backs[ctx.player - 1] = deck.map((c) => c.getBackID());
            ctx.state.backs[2 - ctx.player] = [].slice.call(opponentCardBacks, 0);
            ctx.state.deck[2 - ctx.player] = ctx.state.backs[2 - ctx.player].map(function (back) {
                return {
                    card: null,
                    back: back,
                    el: SpyCards.createUnknownCardEl(back),
                    player: (3 - ctx.player)
                };
            });
            SpyCards.Fx.tpEl.classList.remove("hidden");
            SpyCards.Fx.myHPEl.classList.remove("hidden");
            SpyCards.Fx.theirHPEl.classList.remove("hidden");
            document.body.appendChild(SpyCards.Fx.tpEl);
            document.body.appendChild(SpyCards.Fx.myHPEl);
            document.body.appendChild(SpyCards.Fx.theirHPEl);
            const hpEl = [
                ctx.player === 1 ? SpyCards.Fx.myHPEl : SpyCards.Fx.theirHPEl,
                ctx.player === 2 ? SpyCards.Fx.myHPEl : SpyCards.Fx.theirHPEl
            ];
            const readyButton = document.createElement("button");
            readyButton.textContent = "Ready";
            readyButton.classList.add("btn1");
            ctx.ui.form.appendChild(readyButton);
            const opponentButton = document.createElement("button");
            opponentButton.textContent = "Waiting...";
            opponentButton.classList.add("btn2");
            opponentButton.disabled = true;
            ctx.ui.form.appendChild(opponentButton);
            const updateTP = function () {
                if (ctx.net.cgc.opponentForfeit || ctx.ui.hadFatalError) {
                    return;
                }
                let tp = ctx.state.tp[ctx.player - 1];
                for (let card of ctx.state.hand[ctx.player - 1]) {
                    if (card.el.classList.contains("tapped")) {
                        tp -= card.card.effectiveTP();
                    }
                }
                const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
                const tpMul = (sg && sg.m && (sg.m & 16)) ? 2 : 1;
                SpyCards.Fx.tpEl.textContent = String(Math.abs(tp * tpMul));
                SpyCards.Fx.tpEl.classList.toggle("negative", tp < 0);
                for (let player = 0; player < 2; player++) {
                    hpEl[player].textContent = String(ctx.state.hp[player]);
                }
                if (ctx.state.ready[ctx.player - 1] && !ctx.state.ready[2 - ctx.player]) {
                    ctx.ui.status.textContent = "Waiting for opponent...";
                }
                else if (ctx.state.ready[ctx.player - 1] && ctx.state.ready[2 - ctx.player]) {
                    ctx.ui.status.textContent = "Ending turn...";
                    ctx.state.bothReady();
                }
                else if (tp < 0) {
                    ctx.ui.status.textContent = "Not enough TP!";
                }
                else {
                    ctx.ui.status.textContent = "Select your cards, then click End Turn.";
                }
                readyButton.disabled = !ctx.state.roundSetupComplete || ctx.state.ready[ctx.player - 1] || tp < 0;
                readyButton.textContent = ctx.state.ready[ctx.player - 1] ? "Ready!" : "End Turn";
                opponentButton.textContent = ctx.state.ready[2 - ctx.player] ? "Ready!" : "Waiting...";
            };
            let timerTick = null;
            readyButton.addEventListener("click", function (e) {
                e.preventDefault();
                if (readyButton.disabled) {
                    // just in case a browser does a bad
                    return;
                }
                clearTimeout(timerTick);
                SpyCards.Audio.Sounds.Confirm1.play();
                let tapReady = 0;
                const tap = [];
                for (let i = 0; i < ctx.state.hand[ctx.player - 1].length; i++) {
                    if (ctx.state.hand[ctx.player - 1][i].el.classList.contains("tapped")) {
                        tapReady = SpyCards.Binary.setBit(tapReady, i, 1);
                        SpyCards.Binary.pushUVarInt(tap, ctx.state.hand[ctx.player - 1][i].card.id);
                    }
                }
                const tapReadyEnc = [];
                SpyCards.Binary.pushUVarInt(tapReadyEnc, tapReady);
                tap.unshift(...tapReadyEnc);
                ctx.state.turnData.ready[ctx.player - 1] = tapReady;
                ctx.state.ready[ctx.player - 1] = true;
                ctx.net.cgc.sendReady(new Uint8Array(tap));
                updateTP();
            });
            ctx.net.cgc.onReady = function (promise) {
                if (!ctx.state.roundSetupComplete) {
                    ctx.state.roundSetupDelayed.push(function () {
                        ctx.net.cgc.onReady(promise);
                    });
                    return;
                }
                this.scg.promiseTurn(promise);
                ctx.state.ready[2 - ctx.player] = true;
                updateTP();
            };
            const theirHandEl = document.createElement("div");
            theirHandEl.classList.add("their-hand");
            document.body.appendChild(theirHandEl);
            const myHandEl = document.createElement("div");
            myHandEl.classList.add("my-hand");
            document.body.appendChild(myHandEl);
            myHandEl.setAttribute("role", "listbox");
            myHandEl.setAttribute("aria-orientation", "horizontal");
            myHandEl.setAttribute("aria-label", "Cards in your Hand");
            myHandEl.setAttribute("aria-multiselectable", "true");
            myHandEl.addEventListener("focus", function (e) {
                let target = e.target;
                while (target && target.classList && !target.classList.contains("card")) {
                    target = target.parentElement;
                }
                if (!target || !target.classList) {
                    return;
                }
                if (!target.id) {
                    target.id = SpyCards.UI.uniqueID();
                }
                myHandEl.setAttribute("aria-activedescendant", target.id);
            }, true);
            myHandEl.addEventListener("click", function (e) {
                if (ctx.state.ready[ctx.player - 1]) {
                    return;
                }
                let target = e.target;
                while (target && target.classList && !target.classList.contains("card")) {
                    target = target.parentElement;
                }
                if (!target || !target.classList) {
                    return;
                }
                target.classList.toggle("tapped");
                target.setAttribute("aria-selected", String(target.classList.contains("tapped")));
                updateTP();
                if (!target.classList.contains("tapped")) {
                    SpyCards.Audio.Sounds.PageFlipCancel.play();
                }
                else if (SpyCards.Fx.tpEl.classList.contains("negative")) {
                    SpyCards.Audio.Sounds.Buzzer.play();
                }
                else {
                    SpyCards.Audio.Sounds.Confirm.play();
                }
            });
            addEventListener("keydown", function f(e) {
                if (myHandEl.parentNode !== document.body) {
                    removeEventListener("keydown", f);
                    return;
                }
                if (SpyCards.disableKeyboard) {
                    return;
                }
                const selectedCard = document.getElementById(myHandEl.getAttribute("aria-activedescendant"));
                switch (e.code) {
                    case "ArrowLeft":
                        e.preventDefault();
                        if (selectedCard && selectedCard.previousElementSibling) {
                            selectedCard.previousElementSibling.focus();
                        }
                        else {
                            myHandEl.lastElementChild.focus();
                        }
                        break;
                    case "ArrowRight":
                        e.preventDefault();
                        if (selectedCard && selectedCard.nextElementSibling) {
                            selectedCard.nextElementSibling.focus();
                        }
                        else {
                            myHandEl.firstElementChild.focus();
                        }
                        break;
                    case "KeyC":
                    case "KeyX":
                        e.preventDefault();
                        if (selectedCard && (e.code === "KeyX") === selectedCard.classList.contains("tapped")) {
                            selectedCard.click();
                        }
                        break;
                    case "Space":
                        e.preventDefault();
                        if (selectedCard) {
                            selectedCard.click();
                        }
                        break;
                    case "KeyZ":
                    case "Enter":
                        e.preventDefault();
                        readyButton.click();
                        break;
                }
            });
            ctx.ui.form.appendChild(ctx.ui.h1);
            const processor = new SpyCards.Processor(ctx, SpyCards.Fx.getCallbacks(ctx, audience));
            ctx.effect = processor.effect;
            ctx.fx = SpyCards.Fx.fx;
            const timerSettings = ctx.defs.mode && ctx.defs.mode.get(SpyCards.GameModeFieldType.Timer);
            let timerStored = timerSettings ? timerSettings.startTime : 0;
            let timerTurn = 0;
            let timerTimeText = null;
            let timerOtherText = null;
            while (!processor.checkWinner()) {
                if (ctx.ui.hadFatalError) {
                    return;
                }
                ctx.state.turn++;
                myHandEl.classList.remove("offscreen");
                theirHandEl.classList.remove("offscreen");
                ctx.ui.form.classList.remove("offscreen");
                ctx.ui.h1.textContent = "Round " + ctx.state.turn;
                ctx.ui.status.textContent = "Shuffling decks...";
                readyButton.textContent = "...";
                opponentButton.textContent = "...";
                const waitTimer = setInterval(function () {
                    if (getComputedStyle(ctx.ui.form).transitionDuration === "0s") {
                        // prefers-reduced-motion
                        return;
                    }
                    switch (readyButton.textContent.length) {
                        case 1:
                            readyButton.textContent = "..";
                            opponentButton.textContent = "..";
                            break;
                        default:
                            readyButton.textContent = "...";
                            opponentButton.textContent = "...";
                            break;
                        case 3:
                            readyButton.textContent = ".";
                            opponentButton.textContent = ".";
                            break;
                    }
                }, 1500);
                ctx.state.winner = 0;
                ctx.state.turnData = await ctx.net.cgc.beginTurn();
                clearInterval(waitTimer);
                ctx.state.turnData.ready = [null, null];
                await processor.shuffleAndDraw(false);
                if (timerSettings) {
                    timerStored += timerTurn;
                    timerStored = Math.min(timerStored, timerSettings.maxTime);
                    timerTurn = Math.min(timerStored, timerSettings.maxPerTurn);
                    timerStored -= timerTurn;
                    timerStored += timerSettings.perTurn;
                    timerTick = setTimeout(function f() {
                        timerTurn--;
                        if (timerTurn > 0) {
                            if (ctx.ui.status.textContent !== timerTimeText) {
                                timerOtherText = ctx.ui.status.textContent;
                            }
                            timerTimeText = "Time Remaining: " + Math.floor(timerTurn / 60) + (timerTurn % 60 < 10 ? ":0" : ":") + (timerTurn % 60);
                            ctx.ui.status.textContent = timerTurn % 10 < 5 ? timerTimeText : timerOtherText;
                            timerTick = setTimeout(f, 1000);
                            return;
                        }
                        if (readyButton.disabled) {
                            // we're out of time; just play nothing if we can't play these cards.
                            myHandEl.querySelectorAll(".tapped").forEach((el) => el.click());
                        }
                        readyButton.click();
                    }, 1000);
                }
                if (ctx.state.turn === 1) {
                    // show game rules-summoned cards
                    for (let player = 0; player < 2; player++) {
                        for (let setup of ctx.state.setup[player]) {
                            const el = setup.card.createEl(ctx.defs);
                            el.classList.add("setup");
                            (player === ctx.player - 1 ? SpyCards.Fx.myFieldSetup : SpyCards.Fx.theirFieldSetup).appendChild(el);
                        }
                    }
                    document.body.appendChild(SpyCards.Fx.myFieldSetup);
                    document.body.appendChild(SpyCards.Fx.theirFieldSetup);
                }
                SpyCards.UI.clear(myHandEl);
                for (let card of ctx.state.hand[ctx.player - 1]) {
                    SpyCards.flipCard(card.el, false);
                    card.el.classList.remove("tapped", "setup");
                    card.el.tabIndex = 0;
                    card.el.setAttribute("role", "option");
                    card.el.setAttribute("aria-selected", "false");
                    myHandEl.appendChild(card.el);
                }
                doSquish(myHandEl);
                SpyCards.UI.clear(theirHandEl);
                for (let card of ctx.state.hand[2 - ctx.player]) {
                    card.el.classList.remove("tapped");
                    theirHandEl.insertBefore(card.el, theirHandEl.firstChild);
                }
                ctx.state.ready[0] = ctx.state.ready[1] = false;
                requestAnimationFrame(function () {
                    for (let player = 0; player < 2; player++) {
                        for (let card of ctx.state.hand[player]) {
                            card.el.classList.add("in-hand");
                        }
                    }
                });
                ctx.state.roundSetupComplete = true;
                while (ctx.state.roundSetupDelayed.length) {
                    ctx.state.roundSetupDelayed.shift()();
                }
                updateTP();
                const minimumRoundTime = sleep(1000);
                await new Promise(function (resolve) {
                    ctx.state.bothReady = resolve;
                    if (npc) {
                        npc.playRound(ctx).then((choices) => {
                            const myHand = ctx.state.hand[ctx.player - 1];
                            for (let i = 0; i < myHand.length; i++) {
                                if (SpyCards.Binary.getBit(choices, i)) {
                                    myHand[i].el.classList.add("tapped");
                                }
                            }
                            readyButton.click();
                        });
                    }
                });
                await minimumRoundTime;
                ctx.state.roundSetupComplete = false;
                ctx.state.played[0].length = 0;
                ctx.state.played[1].length = 0;
                const confirmedTurn = await ctx.net.cgc.confirmTurn();
                for (let card of ctx.state.hand[ctx.player - 1]) {
                    if (card.el.classList.contains("tapped")) {
                        ctx.state.played[ctx.player - 1].push({
                            card: card.card,
                            back: card.back,
                            el: card.el,
                            player: ctx.player,
                            effects: card.card.effects.slice(0)
                        });
                    }
                }
                ctx.state.turnData.played = [null, null];
                ctx.state.turnData.played[ctx.player - 1] = ctx.state.played[ctx.player - 1].map(function (card) {
                    return card.card.id;
                });
                myHandEl.classList.add("offscreen");
                theirHandEl.classList.add("offscreen");
                await sleep(500);
                myHandEl.classList.add("hidden");
                theirHandEl.classList.add("hidden");
                const confirmedBuf = SpyCards.toArray(confirmedTurn);
                ctx.state.turnData.ready[2 - ctx.player] = SpyCards.Binary.shiftUVarInt(confirmedBuf);
                for (let i = 0; i < ctx.state.hand[2 - ctx.player].length; i++) {
                    if (SpyCards.Binary.getBit(ctx.state.turnData.ready[2 - ctx.player], i)) {
                        ctx.state.hand[2 - ctx.player][i].el.classList.add("tapped");
                    }
                }
                ctx.state.turnData.played[2 - ctx.player] = [];
                while (confirmedBuf.length) {
                    ctx.state.turnData.played[2 - ctx.player].push(SpyCards.Binary.shiftUVarInt(confirmedBuf));
                }
                for (let id of ctx.state.turnData.played[2 - ctx.player]) {
                    if (ctx.defs.banned[id]) {
                        throw new Error("opponent played banned card " + ctx.defs.cardsByID[id].displayName());
                    }
                }
                for (let player = 0; player < 2; player++) {
                    for (let card of ctx.state.discard[player]) {
                        ctx.state.deck[player].push(card);
                        ctx.state.backs[player].push(card.back);
                    }
                    ctx.state.discard[player] = [];
                    for (let i = 0; i < ctx.state.hand[player].length; i++) {
                        const card = ctx.state.hand[player][i];
                        if (card.el.classList.contains("tapped")) {
                            card.el.classList.remove("in-hand");
                            card.el.removeAttribute("role");
                            card.el.removeAttribute("aria-selected");
                            card.el.tabIndex = null;
                            ctx.state.discard[player].push(card);
                            ctx.state.hand[player].splice(i, 1);
                            i--;
                        }
                    }
                }
                let theirTP = 0;
                ctx.state.played[2 - ctx.player] = ctx.state.turnData.played[2 - ctx.player].map((id) => {
                    const card = ctx.defs.cardsByID[id];
                    theirTP += card.effectiveTP();
                    return {
                        card: card,
                        back: card.getBackID(),
                        el: card.createEl(ctx.defs),
                        player: (3 - ctx.player),
                        effects: card.effects.slice(0)
                    };
                });
                if (theirTP > ctx.state.tp[2 - ctx.player]) {
                    throw new Error("opponent played " + theirTP + " TP of cards with a pool of " + ctx.state.tp[2 - ctx.player] + " TP");
                }
                for (let player = 0; player < 2; player++) {
                    while (ctx.state.setup[player].length) {
                        const setup = ctx.state.setup[player].shift();
                        const el = setup.card.createEl(ctx.defs);
                        el.classList.add("setup");
                        if (!setup.originalDesc) {
                            el.querySelectorAll(".card-desc > *").forEach((e) => SpyCards.UI.remove(e));
                            el.querySelector(".card-desc").appendChild(setup.effects[0].createEl(ctx.defs, setup.card));
                        }
                        ctx.state.played[player].unshift({
                            card: setup.card,
                            back: setup.card.getBackID(),
                            el: el,
                            setup: true,
                            originalDesc: setup.originalDesc,
                            player: (player + 1),
                            effects: setup.effects.slice(0)
                        });
                    }
                }
                ctx.ui.form.classList.add("match-ready", "offscreen");
                await processor.processRound();
                myHandEl.classList.remove("hidden");
                theirHandEl.classList.remove("hidden");
                if (npc) {
                    await npc.afterRound(ctx);
                }
            }
            SpyCards.Fx.tpEl.classList.add("hidden");
            SpyCards.Fx.myHPEl.classList.add("hidden");
            SpyCards.Fx.theirHPEl.classList.add("hidden");
            ctx.ui.form.classList.remove("offscreen", "hidden");
            ctx.ui.h1.classList.add("hidden");
            SpyCards.UI.remove(readyButton);
            SpyCards.UI.remove(opponentButton);
            await stage.deinit();
            const oldOnQuit = ctx.net.cgc.onQuit;
            const quitPromise = new Promise((resolve) => {
                ctx.net.cgc.onQuit = () => {
                    resolve();
                    oldOnQuit();
                };
            });
            const rematchPromise = new Promise((resolve) => {
                ctx.net.cgc.onOfferRematch = resolve;
            });
            const minVerifyWait = sleep(250);
            ctx.ui.status.textContent = "Verifying match...";
            const verifyContext = new SpyCards.MatchState(ctx.defs);
            const verifyProcessor = new SpyCards.Processor({
                net: null,
                scg: ctx.scg,
                ui: null,
                player: ctx.player,
                defs: ctx.defs,
                matchData: ctx.matchData,
                state: verifyContext
            }, new SpyCards.ProcessorCallbacks());
            const initialDecks = [null, null];
            const recording = [];
            await ctx.net.cgc.finalizeMatch(async function (initialDeck, cardBacks) {
                verifyContext.deck[ctx.player - 1] = SpyCards.Decks.decodeBinary(ctx.defs, ctx.scg.localDeckInitial).map((c) => ({
                    card: c,
                    back: c.getBackID(),
                    player: ctx.player
                }));
                verifyContext.backs[ctx.player - 1] = verifyContext.deck[ctx.player - 1].map((c) => c.card.getBackID());
                verifyContext.deck[2 - ctx.player] = SpyCards.Decks.decodeBinary(ctx.defs, initialDeck).map((c) => ({
                    card: c,
                    back: c.getBackID(),
                    player: (3 - ctx.player)
                }));
                verifyContext.backs[2 - ctx.player] = SpyCards.Decks.decodeBacks(cardBacks);
                initialDecks[0] = verifyContext.deck[0].slice(0).map((c) => c.card);
                initialDecks[1] = verifyContext.deck[1].slice(0).map((c) => c.card);
                // deck size and number of each type already verified at start of match
                for (let player = 0; player < 2; player++) {
                    let miniBoss = null;
                    for (let i = 0; i < verifyContext.deck[player].length; i++) {
                        const card = verifyContext.deck[player][i].card;
                        if (!card) {
                            throw new Error("Verification failed for player " + (player + 1) + ": invalid card number " + (i + 1) + " (ID " + initialDeck[i] + ")");
                        }
                        if (card.getBackID() !== verifyContext.backs[player][i]) {
                            throw new Error("Verification failed for player " + (player + 1) + ": invalid card back for card " + (i + 1) + " (" + card.displayName() + ")");
                        }
                        if (card.getBackID() === 1) {
                            if (miniBoss === card) {
                                throw new Error("Verification failed for player " + (player + 1) + ": duplicate mini-boss card " + card.displayName());
                            }
                            miniBoss = card;
                        }
                    }
                    if (ctx.defs.mode) {
                        for (let filter of ctx.defs.mode.getAll(SpyCards.GameModeFieldType.DeckLimitFilter)) {
                            if (verifyContext.deck[player].filter((c) => filter.match(c.card)).length > filter.count) {
                                if (filter.rank === SpyCards.Rank.None && filter.tribe === SpyCards.Tribe.None) {
                                    throw new Error("Verification failed for player " + (player + 1) + ": too many of card " + ctx.defs.cardsByID[filter.card].displayName() + " in deck");
                                }
                                else {
                                    throw new Error("Verification failed for player " + (player + 1) + ": too many cards matching Rank=" + SpyCards.rankName(filter.rank) + " Tribe=" + SpyCards.tribeName(filter.tribe, filter.customTribe) + " in deck");
                                }
                            }
                        }
                    }
                }
                SpyCards.Binary.pushUVarInt(recording, 1); // format version
                const spyCardsVersionNum = spyCardsVersionPrefix.split(/\./g).map((n) => parseInt(n, 10));
                SpyCards.Binary.pushUVarInt(recording, spyCardsVersionNum[0]);
                SpyCards.Binary.pushUVarInt(recording, spyCardsVersionNum[1]);
                SpyCards.Binary.pushUVarInt(recording, spyCardsVersionNum[2]);
                SpyCards.Binary.pushUTF8String1(recording, ctx.matchData.customModeName);
                recording.push(ctx.player);
                for (let player = 0; player < 2; player++) {
                    SpyCards.Binary.pushUTF8String1(recording, ctx.state.players[player] ? ctx.state.players[player].name : "");
                }
                SpyCards.Binary.pushUVarInt(recording, ctx.matchData.rematchCount);
                recording.push(...SpyCards.toArray(ctx.scg.seed));
                recording.push(...SpyCards.toArray(ctx.player === 1 ? ctx.scg.localRandSeed : ctx.scg.remoteRandSeed));
                recording.push(...SpyCards.toArray(ctx.player === 2 ? ctx.scg.localRandSeed : ctx.scg.remoteRandSeed));
                SpyCards.Binary.pushUVarInt(recording, ctx.defs.customCardsRaw.length);
                for (let card of ctx.defs.customCardsRaw) {
                    const buf = Base64.decode(card);
                    SpyCards.Binary.pushUVarInt(recording, buf.length);
                    recording.push(...SpyCards.toArray(buf));
                }
                for (let player = 0; player < 2; player++) {
                    const buf = CrockfordBase32.decode(SpyCards.Decks.encode(ctx.defs, initialDecks[player]));
                    SpyCards.Binary.pushUVarInt(recording, buf.length);
                    recording.push(...SpyCards.toArray(buf));
                }
                SpyCards.Binary.pushUVarInt(recording, ctx.state.turn);
            }, async function (turn) {
                verifyContext.turn++;
                recording.push(...SpyCards.toArray(turn.seed));
                recording.push(...SpyCards.toArray(turn.seed2));
                SpyCards.Binary.pushUVarInt(recording, turn.data.ready[0]);
                SpyCards.Binary.pushUVarInt(recording, turn.data.ready[1]);
                await verifyProcessor.shuffleAndDraw(true);
                for (let player = 0; player < 2; player++) {
                    if (turn.data.ready[player] >= SpyCards.Binary.bit(verifyContext.hand[player].length)) {
                        throw new Error("Verification failed for player " + (player + 1) + " on turn " + verifyContext.turn + ": cards played outside of hand");
                    }
                    const hand = verifyContext.hand[player].slice(0);
                    for (let card of verifyContext.discard[player]) {
                        verifyContext.deck[player].push(card);
                        verifyContext.backs[player].push(card.card.getBackID());
                    }
                    verifyContext.discard[player] = [];
                    let played = [];
                    for (let i = 0, j = 0; i < hand.length; i++, j++) {
                        if (SpyCards.Binary.getBit(turn.data.ready[player], i)) {
                            const card = hand[i];
                            verifyContext.discard[player].push(card);
                            played.push(card);
                            verifyContext.hand[player].splice(j, 1);
                            j--;
                        }
                    }
                    if (played.length !== turn.data.played[player].length) {
                        throw new Error("Verification failed for player " + (player + 1) + " on turn " + verifyContext.turn + ": number of cards played (" + turn.data.played[player].length + ") does not match number of cards removed from hand (" + played.length + ")");
                    }
                    for (let i = 0; i < played.length; i++) {
                        if (played[i].card.id !== turn.data.played[player][i]) {
                            throw new Error("Verification failed for player " + (player + 1) + " on turn " + verifyContext.turn + ": played card " + (i + 1) + " (" + ctx.defs.cardsByID[turn.data.played[player][i]].displayName() + ") does not match card removed from hand (" + played[i].card.displayName() + ")");
                        }
                    }
                }
                // The result of the cards being played was computed locally and can be trusted.
            });
            await minVerifyWait;
            SpyCards.UI.remove(SpyCards.Fx.tpEl);
            SpyCards.UI.remove(hpEl[0]);
            SpyCards.UI.remove(hpEl[1]);
            SpyCards.UI.remove(myHandEl);
            SpyCards.UI.remove(theirHandEl);
            SpyCards.UI.html.classList.remove("in-match");
            const won = processor.checkWinner() === ctx.player;
            SpyCards.Audio.stopMusic();
            if (won) {
                SpyCards.Audio.Sounds.CrowdClap.play();
                ctx.matchData.wins++;
            }
            else {
                SpyCards.Audio.Sounds.CrowdGaspSlow.play();
                ctx.matchData.losses++;
            }
            ctx.ui.h1.textContent = won ? "You win!" : "Opponent wins!";
            ctx.ui.form.appendChild(ctx.ui.h1);
            const recordingBuf = new Uint8Array(recording);
            ctx.ui.code.value = Base64.encode(recordingBuf);
            localStorage["spy-cards-latest-recording-v0"] = ctx.ui.code.value;
            ctx.ui.form.appendChild(ctx.ui.code);
            ctx.ui.status.textContent = "Copy this code if you want to save a recording of this match to watch with ";
            const viewerLink = document.createElement("a");
            viewerLink.target = "_blank";
            viewerLink.href = "viewer.html";
            viewerLink.textContent = "the match viewer";
            ctx.ui.status.appendChild(viewerLink);
            ctx.ui.status.appendChild(document.createTextNode(", or "));
            const uploadMatchButton = document.createElement("button");
            uploadMatchButton.textContent = "Upload Recording";
            uploadMatchButton.addEventListener("click", function (e) {
                e.preventDefault();
                uploadMatchButton.disabled = true;
                uploadMatchButton.textContent = "Uploading...";
                const xhr = new XMLHttpRequest();
                xhr.open("PUT", match_recording_base_url + "upload");
                xhr.addEventListener("load", function (e) {
                    if (xhr.status === 201) {
                        ctx.ui.code.value = xhr.responseText;
                        const recordingLink = document.createElement("a");
                        recordingLink.textContent = "Match recording";
                        recordingLink.target = "_blank";
                        recordingLink.href = "viewer.html#" + xhr.responseText;
                        ctx.ui.status.textContent = "";
                        ctx.ui.status.appendChild(recordingLink);
                        ctx.ui.status.appendChild(document.createTextNode(" successfully uploaded. "));
                        if (!npc) {
                            const didAuto = SpyCards.loadSettings().autoUploadRecording;
                            const btn = SpyCards.UI.button(didAuto ? "Stop Uploading Automatically" : "Always Upload Recordings", [], () => {
                                SpyCards.UI.remove(btn);
                                const settings = SpyCards.loadSettings();
                                settings.autoUploadRecording = !didAuto;
                                SpyCards.saveSettings(settings);
                            });
                            ctx.ui.status.appendChild(btn);
                        }
                        return;
                    }
                    ctx.ui.status.textContent = "Error uploading match: " + xhr.responseText + " (" + xhr.status + ")";
                });
                xhr.addEventListener("error", function (e) {
                    ctx.ui.status.textContent = "Error uploading match!";
                });
                xhr.send(recordingBuf);
            });
            ctx.ui.status.appendChild(uploadMatchButton);
            ctx.ui.status.appendChild(document.createTextNode(" for a shorter code."));
            ctx.ui.form.appendChild(ctx.ui.status);
            if (!npc && SpyCards.loadSettings().autoUploadRecording) {
                uploadMatchButton.click();
            }
            const decksDiv = document.createElement("div");
            decksDiv.classList.add("deck-selector");
            ctx.ui.form.appendChild(decksDiv);
            const yourDeckHeader = document.createElement("h2");
            yourDeckHeader.textContent = "Your Deck";
            const yourDeck = SpyCards.Decks.createDisplay(ctx.defs, initialDecks[ctx.player - 1]);
            yourDeck.classList.add("deck", "no-flip");
            yourDeck.style.cursor = "pointer";
            yourDeck.addEventListener("click", function () {
                window.open("decks.html#" + SpyCards.Decks.encode(ctx.defs, initialDecks[ctx.player - 1]));
            });
            decksDiv.appendChild(yourDeckHeader);
            decksDiv.appendChild(yourDeck);
            const opponentsDeckHeader = document.createElement("h2");
            opponentsDeckHeader.textContent = "Opponent's Deck";
            const opponentsDeck = SpyCards.Decks.createDisplay(ctx.defs, initialDecks[2 - ctx.player]);
            opponentsDeck.classList.add("deck", "no-flip");
            opponentsDeck.style.cursor = "pointer";
            opponentsDeck.addEventListener("click", function () {
                window.open("decks.html#" + SpyCards.Decks.encode(ctx.defs, initialDecks[2 - ctx.player]));
            });
            decksDiv.appendChild(opponentsDeckHeader);
            decksDiv.appendChild(opponentsDeck);
            ctx.net.cgc.onRematch = () => {
                ctx.net.cgc.onQuit = oldOnQuit;
                ctx.net.cgc.onRematch = null;
                SpyCards.UI.clear(ctx.ui.form);
                ctx.ui.form.appendChild(ctx.ui.h1);
                ctx.ui.form.appendChild(ctx.ui.status);
                ctx.scg = ctx.net.cgc.scg;
                ctx.matchData.rematchCount++;
                runSpyCardsGame(ctx);
            };
            const rematchButton = document.createElement("button");
            rematchButton.classList.add("btn1");
            rematchButton.textContent = "Offer Rematch";
            rematchButton.addEventListener("click", function (e) {
                e.preventDefault();
                rematchButton.disabled = true;
                rematchButton.textContent = "Offered Rematch...";
                ctx.net.cgc.offerRematch();
            });
            rematchPromise.then(() => {
                if (!rematchButton.disabled) {
                    rematchButton.textContent = "Accept Rematch";
                }
            });
            quitPromise.then(() => {
                rematchButton.textContent = "Opponent Left";
                rematchButton.disabled = true;
            });
            const exitButton = document.createElement("button");
            exitButton.classList.add("btn2");
            exitButton.textContent = "Exit";
            exitButton.addEventListener("click", function (e) {
                e.preventDefault();
                location.reload();
            });
            ctx.ui.form.appendChild(rematchButton);
            ctx.ui.form.appendChild(exitButton);
            try {
                const params = new URLSearchParams(location.search);
                if (params.has("autoRematch")) {
                    rematchButton.click();
                }
            }
            catch (ex) {
                debugger;
            }
        }
        catch (ex) {
            errorHandler(ex, ctx.state);
        }
    }
    SpyCards.runSpyCardsGame = runSpyCardsGame;
})(SpyCards || (SpyCards = {}));
