---
title: Spy Cards Specification
...

# Cards

## Rank

Cards have four possible ranks:

- Boss (gold front, purple back)
- Mini-Boss (silver front, silver back)
- Attacker (green front, green back)
- Effect (orange front, green back)

Attacker and Effect cards together are called Enemy cards.
Enemy cards share a back, while Mini-Boss and Boss cards have unique backs.

## Attributes

Each card has:

- a name
- a portrait
- one or two tribes
- a TP cost
- a description

There are 13 tribes in the base game, but expansions can add additional tribes.

Descriptions contain one or more effects (expansions may add cards with zero effects).

## Attacker Cards

Attacker cards in the base game have one effect, ATK.
Expansion attacker cards can have either ATK, DEF, or both.

Attacker cards are the only cards susceptible to Numb.

# Effects

In the following sections, the placeholders used in descriptions are:

- `n`: an integer (or, in some cases, infinity); if not explicity specified, `n` is 1
- `Unless`: negate the condition. this is the logical negation; that is, "at least X" turns into "less than X", not "at most X"
- `[card]`: the name of a card. for the purposes of expansions, cards have an internal name that can be used to differentiate between two different cards with the same name printed on them
- `[tribe]`: the name of one of the 13 tribes used in the base game, or the name of a tribe added by an expansion
- `[rank]`: Boss, Mini-Boss, Attacker, or Effect
- `[-n]`: the negation of `n`; used when there is a negative value for `n` that does not have a sign in the description
- `-n`: a negative integer (the same as `n` for negative `n`)
- `[effect]` or `[X effect]`: a nested effect (used in conditions), with an optional label (X) to differentiate it from other effects in the same description
- `[A/B/C]`: either `A`, `B`, or `C`. In cases where it has a defined meaning, this can also mean `[A]`, `[B]`, or `[C]`

## Stats

### ATK

*(Written as `ATK: n` or `ATK+n`; expansions also use `ATK-n`)*

Increase the player's ATK by n. If this card is numbed, the player's ATK is decreased by n, for a net ATK of 0.

### DEF

*(Written as `DEF: n` or `DEF+n`; expansions also use `DEF-n`)*

Increase the player's DEF by n. If this card is numbed, the player's DEF is decreased by n, for a net DEF of 0.

### Pierce

*(Written as `Pierce (n)`)*

Decrease the opponent's DEF by n, to a minimum of 0.

### Lifesteal

*(Written as `Lifesteal (n)`)*

If the player wins the round, increase the player's HP by n, to a maximum of 5.

Equivalent to `On Win: Heal n`.

### Heal

*(Written as `Heal n`; expansions also use `Take [-n] Damage`)*

Increase the player's HP by n, to a maximum of 5.

### Multiply Healing

*(Expansion only; Written as `Multiply Healing by n`)*

For this entire round, all changes to the player's HP (other than from the opponent winning the round) are multiplied by n.

### TP

*(Expansion only; Written as `TP+n` or `TP-n`)*

Can appear in two contexts:

If TP is listed directly in a card's description, it is subtracted from the card's TP cost, allowing cards with TP costs that are negative or higher than 10.

If TP is listed as the result of a Setup condition, modify the amount of TP available to the player in the next round.

## Actions

### Numb

*(Written as `Numb (n)` online; Written as `Numb` for `Numb (1)` or `Numb All Attackers` for `Numb (∞)` in Bug Fables)*

Flip the first face-up attacker card played by the opponent to face-down, reverting its modification to the opponent's ATK and DEF.

"First" is determined differently in Bug Fables than in the online version.

In Bug Fables, cards are played in a specific order. NPCs always play cards from left to right in their hand, and players play cards in the order they select them.

In the online version, the "first" attacker is the one with the lowest stat,
where the stat used is either ATK or DEF, whichever is highest on the card.
If cards have equal ATK and DEF, the card with ATK is considered lower than the card with DEF.

### Summon

*(Written as `Summon [card]`, `Summon n [tribe]`, or `Turns into a random [rank] Card`; expansions also use `Summon [card] as Opponent` and `Summon n [tribe] [rank]`)*

Add the specified card, or n random cards fitting the specified conditions, to the player's played cards.
If no rank is specified, the random selection uses enemy cards.

Effects on the summoned card that would have already been applied are applied when the card is summoned.

### Unity

*(Written as `Unity (n, [tribe])`; some cards in Bug Fables are written as `Unity (n)`)*

For each unique instance of this effect (based on the card and the position in the card's description), increase the ATK of every card in the specified tribe by n.

Equivalent to `Empower +n ([tribe]) (only once)`.

### Empower

*(Written as `Empower +n ([tribe])` or `Empower +n ([card])`; online replaces the latter with `Empower Card +n ([card])` to avoid confusion)*

Increase the ATK of every card in the specified tribe, or every instance of the specified card, by n.

## Conditions

Conditions are effects that apply another effect based on criteria.
If a condition applies an effect that should have already been processed, it is processed at the time of application.

### Setup

*(Written as `Setup ([effect])`)*

The effect occurs in the following round. That is, if a card with `Setup (X)` is played in round 3, X occurs in round 4.

### Limit

*(Written as `[effect] (only once)`; expansions also use `[effect] (max. n times)`, `[effect] (except the first time)`, or `[effect] (except the first n times)`)*

The effect (unique, determined by its card and its position on the card) can only happen a limited number of times per player per round.
Expansions may invert this condition, causing an effect to *not* occur a limited number of times per round and occur normally once the limit has been reached.

### Coin

*(Written as `Coin (n) [heads effect]` or `Coin (n) [heads effect] or [tails effect]`)*

Flip n coins. For every coin that lands on heads, apply the heads effect.
For every coin that lands on tails, apply the tails effect (if applicable).

### Stat

*(Written as `If [stat] (n): [effect]`; expansions add various forms of `[If/Unless/VS/Unless VS] [Final/] [stat] (n): [effect]`)*

If the player's stat (ATK, DEF, or HP) is at least n, apply the effect.

For "Final ATK" or "Final DEF, this condition is checked after Numb and Pierce are applied.
Otherwise, this condition occurs before Numb and Pierce.

### If/VS

*(Written as `If [card]: [effect]`, `If [tribe] (n): [effect]`, or `VS [tribe]: [effect]`; expansions add various forms of `[If/VS] [card/tribe/rank] (n): [effect]` and `Unless [VS/] [card/tribe/rank] (n): [effect]`)*

If a sufficient number of matching cards are present in the player's (or in the case of VS, opponent's) played cards, apply the effect.

### Per

*(Written as `[effect] per [card]`; expansions add `[effect] per [tribe]` and `[effect] per [rank]`)*

For each matching card in the player's played cards, apply the effect.

### Winner

*(Expansion only; Written as `On Win: [effect]`, `On Loss: [effect]`, `On Tie: [effect]`, or `Unless Tie: [effect]`)*

If the player wins/loses/ties the round, apply the effect.

# Deck Building

A deck consists of:

- 1 boss card (see Appendix A for a list)
- 2 unique mini-boss cards (see Appendix A)
- 12 enemy cards (allowing repeats, see appendix A)

Apart from the number of each rank of card and the two mini-boss cards being different,
the only other restriction on the cards chosen is that cards that cost infinite TP cannot be selected.
(This only occurs in expansions.)

# Match Structure

## Start of Match

Both players start the match with:

- a 15-card deck (as described above)
- an empty hand
- an empty discard pile
- 1 max TP
- 5 HP

## Start of Round

For every round, including the first,

- Each player gains 1 max TP (to a limit of 10 max TP).
- Each player's TP is refilled to the maximum.
- Setup TP effects are applied (these may go above the maximum).
- If a player has negative TP as a result of Setup effects, set TP to 0.
- Each player shuffles their deck and draws cards based on their current hand size.
    - 0: draw 3 cards.
    - 1: draw 2 cards.
    - 2: draw 2 cards.
    - 3: draw 2 cards.
    - 4: draw 1 card.
    - 5: draw 0 cards.

The backs of the cards in each player's hand can be seen by the other player.

## Playing Cards

Both players select cards from their hand to play.
The TP cost of the cards must not exceed the player's TP.
The cards are played at the same time;
neither player can see which cards the other player intends to play.

## Effect Processing

Effects are processed in the following order:

1. Independent Conditions
    - Limit
    - Stat (HP)
    - Coin
2. Summon
3. Card-Based Conditions
    - If/VS
    - Per
3. Self Stat Changes
    - Multiply Healing
    - Heal
    - ATK
    - DEF
    - Unity
    - Empower
4. Stat Conditions
    - Stat (ATK)
    - Stat (DEF)
5. Opponent Stat Changes
    - Numb
    - Pierce
6. Final Stat Conditions
    - Stat (Final ATK)
    - Stat (Final DEF)

## End of Round

If DEF is negative, set DEF to 0.
Subtract DEF from the opponent's ATK.
If ATK is now negative, set ATK to 0.

The winner of the round is the player with higher ATK.
If both players have the same ATK, the round is a tie.

After the winner of the round is decided,
the remaining effects and conditions are applied:

- Lifesteal
- Winner
- Setup

Cards that are currently in the discard pile are added back into the deck.
Cards that were played this round are put into the discard pile.

## End of Match

The match ends after a round when one player has zero or lower HP,
or when both players have zero or lower HP and a player wins a round.

The winner of the match is the player with positive HP,
or the player who won the last round if neither player has positive HP.

# Appendix A: Cards

This section lists cards in the base game (vanilla).

(warning: this section contains Bug Fables spoilers)

## Boss

### Spider

3 TP Spider

- ATK: 2
- Summon Inichas
- Summon Jellyshroom

### Venus' Guardian

4 TP Plant

- ATK: 2
- ATK+3 per Venus' Bud

### Heavy Drone B-33

5 TP Bot

- DEF: 2
- Empower +2 (Bot)

### The Watcher

5 TP Zombie

- ATK: 1
- Summon Krawler
- Summon Warden

### The Beast

5 TP Bug

- ATK: 3
- If Kabbu: ATK+4

### ULTIMAX Tank

7 TP Bot

- ATK: 8

### Mother Chomper

4 TP Plant/Chomper

- Lifesteal (2)
- Empower +2 (Chomper)

### Broodmother

4 TP Bug

- ATK: 2
- Empower Card +2 (Midge)

### Zommoth

4 TP Fungi/Zombie

- Empower +2 (Fungi)

### Seedling King

4 TP Seedling/Plant

- Empower +2 (Seedling)

### Tidal Wyrm

5 TP ???

- ATK: 2
- Numb (∞)

### Peacock Spider

5 TP Spider

- Empower +3 (Spider)

### Devourer

4 TP Plant

- ATK: 3
- VS Bug (ATK+3)

### False Monarch

5 TP Bug/Mothfly

- Empower +3 (Mothfly)

### Maki

6 TP Bug

- ATK: 6

### Wasp King

7 TP Bug

- Summon 2 Wasp

### The Everlasting King

9 TP Plant/Bug

- ATK: ∞

## Mini-Boss

### Ahoneynation

5 TP ???

- Coin (2): Summon Abomihoney

### Acolyte Aria

3 TP Bug

- ATK: 1
- Coin (3): Summon Venus' Bud

### Mothiva

2 TP Bug

- ATK: 2
- If Zasp: Heal 1

### Zasp

3 TP Bug

- ATK: 3
- If Mothiva: ATK+2

### Astotheles

5 TP Bug/Thug

- Empower +3 (Thug)

### Dune Scorpion

7 TP Bug

- ATK: 7

### Primal Weevil

6 TP Bug

- ATK: 3
- Empower Card +2 (Weevil)

### Cross

3 TP Bug

- ATK: 2
- If Poi: ATK+2

### Poi

3 TP Bug

- DEF: 1
- If Cross: DEF+3

### General Ultimax

3 TP Bug/Wasp

- ATK: 1
- Unity (1, Wasp)

### Cenn

3 TP Bug

- ATK: 2
- If Pisci: Numb (1)

### Pisci

3 TP Bug

- DEF: 2
- If Cenn: DEF+4

### Monsieur Scarlet

3 TP Bug

- ATK: 3
- If ATK (7): Heal 1

### Kabbu

2 TP Bug

- ATK: 1
- Pierce (3)

### Kali

4 TP Bug

- DEF: 2
- If Kabbu: Heal 3

### Carmina

4 TP Bug

- Turns into a random Mini-Boss Card.

### Riz

3 TP Bug

- ATK: 1
- Setup (ATK+2)

### Kina

4 TP Bug

- ATK: 2
- If Maki: ATK+3

### Yin

3 TP Bug

- DEF: 1
- If Maki: Heal 2

### Dead Lander α

4 TP Dead Lander

- ATK: 3
- DEF: 1

### Dead Lander β

4 TP Dead Lander

- ATK: 2
- Coin (1): ATK+2 or DEF+2

### Dead Lander γ

6 TP Dead Lander

- ATK: 3
- DEF: 3

## Attacker

### Seedling

1 TP Plant/Seedling

- ATK: 1

### Underling

3 TP Seedling/Plant

- ATK: 3

### Golden Seedling

9 TP Seedling/Plant

- ATK: 9

### Zombiant

1 TP Fungi/Zombie

- ATK: 1

### Zombee

3 TP Fungi/Zombie

- ATK: 3

### Zombeetle

5 TP Fungi/Zombie

- ATK: 5

### Jellyshroom

1 TP Fungi

- ATK: 1

### Bloatshroom

4 TP Fungi

- ATK: 4

### Chomper

2 TP Plant/Chomper

- ATK: 2

### Chomper Brute

4 TP Plant/Chomper

- ATK: 4

### Psicorp

2 TP Bug

- ATK: 2

### Arrow Worm

3 TP ???

- ATK: 3

### Thief

2 TP Bug/Thug

- ATK: 2

### Bandit

3 TP Bug/Thug

- ATK: 3

### Burglar

4 TP Bug/Thug

- ATK: 4

### Ruffian

6 TP Bug/Thug

- ATK: 6

### Security Turret

2 TP Bot

- ATK: 2

### Abomihoney

4 TP ???

- ATK: 4

### Krawler

2 TP Bot

- ATK: 2

### Warden

3 TP Bot

- ATK: 3

### Haunted Cloth

4 TP ???

- ATK: 4

### Mantidfly

4 TP Bug

- ATK: 4

### Jumping Spider

3 TP Spider

- ATK: 3

### Mimic Spider

5 TP Spider

- ATK: 5

### Diving Spider

3 TP Spider

- ATK: 3

### Water Strider

3 TP Bug

- ATK: 3

### Belostoss

6 TP Bug

- ATK: 6

### Mothfly

1 TP Bug/Mothfly

- ATK: 1

### Mothfly Cluster

3 TP Bug/Mothfly

- ATK: 3

### Wasp Scout

2 TP Bug/Wasp

- ATK: 2

### Wasp Trooper

4 TP Wasp/Bug

- ATK: 4

## Effect

### Acornling

2 TP Plant/Seedling

- Coin (1): DEF+3

### Cactiling

3 TP Seedling/Plant

- Coin (1): DEF+4

### Flowerling

1 TP Plant/Seedling

- Lifesteal (1)

### Plumpling

6 TP Plant/Seedling

- Coin (1): DEF+6

### Inichas

1 TP Bug

- Coin (1): DEF+2

### Denmuki

3 TP Bug

- ATK: 1
- Coin (1): Numb (1)

### Madesphy

5 TP Bug

- Coin (2): DEF+3

### Numbnail

2 TP ???

- Numb (1)

### Ironnail

3 TP ???

- DEF: 1
- Numb (1)

### Midge

2 TP Bug

- ATK: 1 (only once)
- ATK: 1

*Note that this card has an incorrect description in Bug Fables 1.0.5. The description given here is correct for both Bug Fables and the online version.*

### Wild Chomper

4 TP Plant/Chomper

- ATK: 1
- Coin (1): Summon Chomper

### Weevil

- ATK: 1
- VS Plant (ATK+2)

### Bee-Boop

1 TP Bot

- Coin (1): ATK+1 or DEF+1

### Mender

1 TP Bot

- Bot (5): Heal 1

*Note that this card has an incorrect description in Bug Fables 1.0.5. The description given here is correct for both Bug Fables and the online version.*

### Leafbug Ninja

3 TP Bug/Leafbug

- Unity (2, Leafbug)

### Leafbug Archer

2 TP Bug/Leafbug

- Unity (1, Leafbug)

### Leafbug Clubber

4 TP Bug/Leafbug

- ATK: 2
- Leafbug (3): Numb (1)

### Wasp Bomber

5 TP Bug/Wasp

- ATK: 2
- Coin (1): ATK+1
- Coin (1): Numb (1)

### Wasp Driller

6 TP Bug/Wasp

- ATK: 4
- Pierce (2)

### Venus' Bud

1 TP Plant

- Lifesteal (1)
