"use strict";
var SpyCards;
(function (SpyCards) {
    var Audio;
    (function (Audio) {
        const audioDebug = new URLSearchParams(location.search).has("audioDebug") ? (...message) => console.debug("AUDIO:", ...message) : (...message) => { };
        Audio.actx = new (window.AudioContext || window.webkitAudioContext)();
        const musicGain = Audio.actx.createGain();
        musicGain.connect(Audio.actx.destination);
        Audio.musicFFT = Audio.actx.createAnalyser();
        Audio.musicFFT.connect(musicGain);
        const soundsGain = Audio.actx.createGain();
        soundsGain.connect(Audio.actx.destination);
        addEventListener("click", function () {
            if (Audio.actx.state === "suspended") {
                Audio.actx.resume();
            }
        }, true);
        Audio.musicVolume = 0;
        Audio.soundsVolume = 0;
        Audio.ignoreAudio = 0;
        let activeMusic;
        let forcedMusic;
        function adoptForcedMusic() {
            if (activeMusic) {
                activeMusic.stop();
            }
            activeMusic = forcedMusic;
            forcedMusic = null;
            if (!Audio.musicVolume) {
                stopMusic();
            }
        }
        Audio.adoptForcedMusic = adoptForcedMusic;
        function playingMusic() {
            return !!activeMusic;
        }
        Audio.playingMusic = playingMusic;
        const fetchAudioPromise = new Promise((resolve) => Audio.startFetchAudio = resolve);
        function setVolume(music, sounds) {
            Audio.musicVolume = music;
            Audio.soundsVolume = sounds;
            const settings = SpyCards.loadSettings();
            settings.audio.music = music;
            settings.audio.sounds = sounds;
            SpyCards.saveSettings(settings);
            musicGain.gain.setValueAtTime(music, Audio.actx.currentTime);
            soundsGain.gain.setValueAtTime(sounds, Audio.actx.currentTime);
            if (!music) {
                stopMusic();
            }
        }
        Audio.setVolume = setVolume;
        addEventListener("spy-cards-settings-changed", () => {
            const s = SpyCards.loadSettings().audio;
            Audio.musicVolume = s.music;
            Audio.soundsVolume = s.sounds;
            musicGain.gain.setValueAtTime(Audio.musicVolume, Audio.actx.currentTime);
            soundsGain.gain.setValueAtTime(Audio.soundsVolume, Audio.actx.currentTime);
        });
        function showUI(form) {
            const initialSettings = SpyCards.loadSettings().audio;
            Audio.musicVolume = initialSettings.music;
            Audio.soundsVolume = initialSettings.sounds;
            musicGain.gain.setValueAtTime(Audio.musicVolume, Audio.actx.currentTime);
            soundsGain.gain.setValueAtTime(Audio.soundsVolume, Audio.actx.currentTime);
            const reenableAudio = SpyCards.UI.button("Enable Audio", ["enable-audio", "audio-error"], async () => {
                await Audio.actx.resume();
                updateUI();
                Audio.Sounds.Confirm.play();
            });
            const enableAudio = SpyCards.UI.button("Enable Audio", ["enable-audio"], async () => {
                await Audio.actx.resume();
                setVolume(0.6, 0.6);
                updateUI();
                Audio.Sounds.Confirm.play();
            });
            const enableAudioMusic = SpyCards.UI.button("Music", ["enable-audio", "split", "split-1"], () => {
                setVolume(Audio.musicVolume ? 0 : 0.6, Audio.soundsVolume);
                updateUI();
            });
            const enableAudioSound = SpyCards.UI.button("Sounds", ["enable-audio", "split", "split-2"], () => {
                setVolume(Audio.musicVolume, Audio.soundsVolume ? 0 : 0.6);
                updateUI();
                Audio.Sounds.Confirm.play();
            });
            function updateUI() {
                SpyCards.UI.remove(reenableAudio);
                SpyCards.UI.remove(enableAudio);
                SpyCards.UI.remove(enableAudioMusic);
                SpyCards.UI.remove(enableAudioSound);
                if (Audio.actx.state === "suspended" && (Audio.musicVolume || Audio.soundsVolume)) {
                    form.appendChild(reenableAudio);
                    Audio.actx.resume().then(updateUI);
                }
                else if (Audio.musicVolume || Audio.soundsVolume) {
                    if (window.requestIdleCallback) {
                        requestIdleCallback(Audio.startFetchAudio);
                    }
                    enableAudioMusic.classList.toggle("enabled", Audio.musicVolume !== 0);
                    enableAudioMusic.setAttribute("aria-pressed", String(Audio.musicVolume !== 0));
                    enableAudioSound.classList.toggle("enabled", Audio.soundsVolume !== 0);
                    enableAudioSound.setAttribute("aria-pressed", String(Audio.soundsVolume !== 0));
                    form.appendChild(enableAudioMusic);
                    form.appendChild(enableAudioSound);
                }
                else {
                    enableAudio.setAttribute("aria-pressed", "false");
                    form.appendChild(enableAudio);
                }
            }
            updateUI();
        }
        Audio.showUI = showUI;
        async function fetchAudio(name) {
            audioDebug("waiting to fetch audio: ", name);
            await fetchAudioPromise;
            audioDebug("fetching audio: ", name);
            const response = await fetch(name.indexOf("/") === -1 ? "audio/" + name + ".opus" : name);
            audioDebug("received response for audio: ", name);
            const body = await response.arrayBuffer();
            audioDebug("finished downloading audio: ", name);
            audioDebug("audio context state before decoding ", name, ": ", Audio.actx.state);
            if (Audio.actx.state === "suspended") {
                audioDebug("waiting to resume audio context for: ", name);
                await Audio.actx.resume();
                audioDebug("resumed audio context for: ", name);
            }
            const bufferPromise = Audio.actx.decodeAudioData(body);
            audioDebug("audio decoding promise for ", name, ": ", bufferPromise);
            const buffer = await bufferPromise;
            audioDebug("decoded audio: ", name);
            return buffer;
        }
        class Sound {
            constructor(name, pitch = 1, volume = 1) {
                this.name = name;
                this.buffer = fetchAudio(name);
                this.pitch = pitch;
                this.volume = volume;
            }
            async createSource() {
                Audio.startFetchAudio();
                const source = Audio.actx.createBufferSource();
                source.buffer = await this.buffer;
                source.playbackRate.setValueAtTime(this.pitch, Audio.actx.currentTime);
                return source;
            }
            async preload() {
                Audio.startFetchAudio();
                await this.buffer;
            }
            async play(delay = 0, overridePitch, overrideVolume) {
                if (!Audio.soundsVolume) {
                    return;
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                let dest = soundsGain;
                if (this.volume !== 1) {
                    if (!this.gain) {
                        this.gain = Audio.actx.createGain();
                        this.gain.connect(dest);
                        this.gain.gain.setValueAtTime(this.volume, Audio.actx.currentTime);
                    }
                    dest = this.gain;
                }
                if (overrideVolume) {
                    const gain = Audio.actx.createGain();
                    gain.connect(dest);
                    gain.gain.setValueAtTime(overrideVolume, Audio.actx.currentTime);
                    dest = gain;
                }
                const src = await this.createSource();
                if (overridePitch) {
                    src.playbackRate.setValueAtTime(overridePitch * this.pitch, Audio.actx.currentTime);
                }
                src.connect(dest);
                src.start(Audio.actx.currentTime + delay);
            }
            async playAsMusic(delay = 0) {
                if (!Audio.musicVolume) {
                    return;
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                let dest = Audio.musicFFT;
                if (this.volume !== 1) {
                    if (!this.musicGain) {
                        this.musicGain = Audio.actx.createGain();
                        this.musicGain.connect(dest);
                        this.musicGain.gain.setValueAtTime(this.volume, Audio.actx.currentTime);
                    }
                    dest = this.musicGain;
                }
                stopMusic();
                const src = await this.createSource();
                src.connect(dest);
                src.start(Audio.actx.currentTime + delay);
                activeMusic = src;
            }
        }
        Audio.Sound = Sound;
        class Music extends Sound {
            constructor(name, start, end) {
                super(name);
                this.start = start;
                this.end = end;
            }
            async createSource() {
                const source = await super.createSource();
                source.loop = true;
                source.loopStart = this.start;
                source.loopEnd = this.end;
                return source;
            }
            async play(delay = 0) {
                if (!Audio.musicVolume) {
                    return;
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                stopMusic();
                const src = await this.createSource();
                src.connect(Audio.musicFFT);
                src.start(Audio.actx.currentTime + delay);
                activeMusic = src;
            }
            async forcePlay(delay = 0) {
                if (forcedMusic) {
                    forcedMusic.stop();
                }
                if (Audio.ignoreAudio) {
                    return;
                }
                const src = await this.createSource();
                src.connect(Audio.musicFFT);
                src.start(Audio.actx.currentTime + delay);
                forcedMusic = src;
            }
        }
        Audio.Music = Music;
        function stopMusic() {
            if (activeMusic) {
                activeMusic.stop();
            }
            activeMusic = null;
        }
        Audio.stopMusic = stopMusic;
        Audio.Sounds = {
            AtkFail: new Sound("AtkFail"),
            AtkSuccess: new Sound("AtkSuccess"),
            BattleStart0: new Sound("BattleStart0"),
            Buzzer: new Sound("Buzzer"),
            CardSound2: new Sound("CardSound2", 1.2, 0.5),
            Charge: new Sound("Charge"),
            Coin: new Sound("Coin"),
            Confirm: new Sound("Confirm"),
            Confirm1: new Sound("Confirm1", 0.4, 0.5),
            CrowdCheer2: new Sound("CrowdCheer2", 1.2),
            CrowdClap: new Sound("CrowdClap"),
            CrowdGasp: new Sound("CrowdGasp"),
            CrowdGaspSlow: new Sound("CrowdGasp", 0.75),
            Damage0: new Sound("Damage0"),
            Death3: new Sound("Death3"),
            Fail: new Sound("Fail"),
            Heal: new Sound("Heal"),
            Lazer: new Sound("Lazer"),
            PageFlip: new Sound("PageFlip"),
            PageFlipCancel: new Sound("PageFlip", 0.7),
            Toss11: new Sound("Toss11"),
            FBCountdown: new Sound("FBCountdown"),
            FBDeath: new Sound("FBDeath"),
            FBFlower: new Sound("FBFlower"),
            FBGameOver: new Sound("FBGameOver"),
            FBPoint: new Sound("FBPoint"),
            FBStart: new Sound("FBStart"),
            MiteKnightIntro: new Sound("MiteKnightIntro"),
            MKDeath: new Sound("MKDeath"),
            MKGameOver: new Sound("MKGameOver"),
            MKHit: new Sound("MKHit"),
            MKHit2: new Sound("MKHit2"),
            MKKey: new Sound("MKKey"),
            MKOpen: new Sound("MKOpen"),
            MKPotion: new Sound("MKPotion"),
            MKShield: new Sound("MKShield"),
            MKStairs: new Sound("MKStairs"),
            MKWalk: new Sound("MKWalk"),
            PeacockSpiderNPCSummonSuccess: new Sound("PeacockSpiderNPCSummonSuccess"),
            Shot2: new Sound("Shot2"),
        };
        Audio.Songs = {
            Miniboss: new Music("Miniboss", 20.55, 87),
            Bounty: new Music("Bounty", 6.7, 52.5),
            Inside2: new Music("Inside2", 10.3, 72),
            FlyingBee: new Music("FlyingBee", 999, 999),
            MiteKnight: new Music("MiteKnight", 999, 999),
            TermiteLoop: new Music("TermiteLoop", 999, 999),
        };
    })(Audio = SpyCards.Audio || (SpyCards.Audio = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
/**
 * Implementation of Crockford Base 32
 * https://www.crockford.com/base32.html
 */
var CrockfordBase32;
(function (CrockfordBase32) {
    const alphabet = "0123456789ABCDEFGHJKMNPQRSTVWXYZ";
    function normalize(s) {
        s = s.toUpperCase();
        s = s.replace(/O/g, "0");
        s = s.replace(/[IL]/g, "1");
        s = s.replace(/[ \-_]/g, "");
        return s;
    }
    function encode(src) {
        const dst = [];
        let bits = 0, n = 0;
        for (let i = 0; i < src.length; i++) {
            n <<= 8;
            n |= src[i];
            bits += 8;
            while (bits >= 5) {
                bits -= 5;
                dst.push(alphabet.charAt(n >> bits));
                n &= (1 << bits) - 1;
            }
        }
        if (bits) {
            while (bits < 5) {
                n <<= 1;
                bits++;
            }
            dst.push(alphabet.charAt(n));
        }
        return dst.join("");
    }
    CrockfordBase32.encode = encode;
    function decode(src) {
        src = normalize(src);
        const dst = new Uint8Array(Math.floor(src.length / 8 * 5));
        let bits = 0, n = 0;
        for (let i = 0, j = 0; i < src.length; i++) {
            const k = alphabet.indexOf(src.charAt(i));
            if (k === -1) {
                throw new Error("invalid character in string");
            }
            n <<= 5;
            n |= k;
            bits += 5;
            while (bits >= 8) {
                bits -= 8;
                dst[j++] = n >> bits;
                n &= (1 << bits) - 1;
            }
        }
        return dst;
    }
    CrockfordBase32.decode = decode;
})(CrockfordBase32 || (CrockfordBase32 = {}));
// Because atob and btoa are weird, fiddly, and don't properly support nul bytes.
var Base64;
(function (Base64) {
    const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    function encode(src) {
        const dst = [];
        let bits = 0, n = 0;
        for (let i = 0; i < src.length; i++) {
            n <<= 8;
            n |= src[i];
            bits += 8;
            while (bits >= 6) {
                bits -= 6;
                dst.push(alphabet.charAt(n >> bits));
                n &= (1 << bits) - 1;
            }
        }
        if (bits) {
            while (bits < 6) {
                n <<= 1;
                bits++;
            }
            dst.push(alphabet.charAt(n));
        }
        while (dst.length % 4) {
            dst.push("=");
        }
        return dst.join("");
    }
    Base64.encode = encode;
    function decode(src) {
        src = src.replace(/[=]+$/, "");
        const dst = new Uint8Array(Math.floor(src.length / 8 * 6));
        let bits = 0, n = 0;
        for (let i = 0, j = 0; i < src.length; i++) {
            const k = alphabet.indexOf(src.charAt(i));
            if (k === -1) {
                throw new Error("invalid character in string");
            }
            n <<= 6;
            n |= k;
            bits += 6;
            while (bits >= 8) {
                bits -= 8;
                dst[j++] = n >> bits;
                n &= (1 << bits) - 1;
            }
        }
        return dst;
    }
    Base64.decode = decode;
})(Base64 || (Base64 = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var CardData;
    (function (CardData) {
        let GlobalCardID;
        (function (GlobalCardID) {
            GlobalCardID[GlobalCardID["Zombiant"] = 0] = "Zombiant";
            GlobalCardID[GlobalCardID["Jellyshroom"] = 1] = "Jellyshroom";
            GlobalCardID[GlobalCardID["Spider"] = 2] = "Spider";
            GlobalCardID[GlobalCardID["Zasp"] = 3] = "Zasp";
            GlobalCardID[GlobalCardID["Cactiling"] = 4] = "Cactiling";
            GlobalCardID[GlobalCardID["Psicorp"] = 5] = "Psicorp";
            GlobalCardID[GlobalCardID["Thief"] = 6] = "Thief";
            GlobalCardID[GlobalCardID["Bandit"] = 7] = "Bandit";
            GlobalCardID[GlobalCardID["Inichas"] = 8] = "Inichas";
            GlobalCardID[GlobalCardID["Seedling"] = 9] = "Seedling";
            GlobalCardID[GlobalCardID["Numbnail"] = 14] = "Numbnail";
            GlobalCardID[GlobalCardID["Mothiva"] = 15] = "Mothiva";
            GlobalCardID[GlobalCardID["Acornling"] = 16] = "Acornling";
            GlobalCardID[GlobalCardID["Weevil"] = 17] = "Weevil";
            GlobalCardID[GlobalCardID["VenusBud"] = 19] = "VenusBud";
            GlobalCardID[GlobalCardID["Chomper"] = 20] = "Chomper";
            GlobalCardID[GlobalCardID["AcolyteAria"] = 21] = "AcolyteAria";
            GlobalCardID[GlobalCardID["Kabbu"] = 23] = "Kabbu";
            GlobalCardID[GlobalCardID["VenusGuardian"] = 24] = "VenusGuardian";
            GlobalCardID[GlobalCardID["WaspTrooper"] = 25] = "WaspTrooper";
            GlobalCardID[GlobalCardID["WaspBomber"] = 26] = "WaspBomber";
            GlobalCardID[GlobalCardID["WaspDriller"] = 27] = "WaspDriller";
            GlobalCardID[GlobalCardID["WaspScout"] = 28] = "WaspScout";
            GlobalCardID[GlobalCardID["Midge"] = 29] = "Midge";
            GlobalCardID[GlobalCardID["Underling"] = 30] = "Underling";
            GlobalCardID[GlobalCardID["MonsieurScarlet"] = 31] = "MonsieurScarlet";
            GlobalCardID[GlobalCardID["GoldenSeedling"] = 32] = "GoldenSeedling";
            GlobalCardID[GlobalCardID["ArrowWorm"] = 33] = "ArrowWorm";
            GlobalCardID[GlobalCardID["Carmina"] = 34] = "Carmina";
            GlobalCardID[GlobalCardID["SeedlingKing"] = 35] = "SeedlingKing";
            GlobalCardID[GlobalCardID["Broodmother"] = 36] = "Broodmother";
            GlobalCardID[GlobalCardID["Plumpling"] = 37] = "Plumpling";
            GlobalCardID[GlobalCardID["Flowerling"] = 38] = "Flowerling";
            GlobalCardID[GlobalCardID["Burglar"] = 39] = "Burglar";
            GlobalCardID[GlobalCardID["Astotheles"] = 40] = "Astotheles";
            GlobalCardID[GlobalCardID["MotherChomper"] = 41] = "MotherChomper";
            GlobalCardID[GlobalCardID["Ahoneynation"] = 42] = "Ahoneynation";
            GlobalCardID[GlobalCardID["BeeBoop"] = 43] = "BeeBoop";
            GlobalCardID[GlobalCardID["SecurityTurret"] = 44] = "SecurityTurret";
            GlobalCardID[GlobalCardID["Denmuki"] = 45] = "Denmuki";
            GlobalCardID[GlobalCardID["HeavyDroneB33"] = 46] = "HeavyDroneB33";
            GlobalCardID[GlobalCardID["Mender"] = 47] = "Mender";
            GlobalCardID[GlobalCardID["Abomihoney"] = 48] = "Abomihoney";
            GlobalCardID[GlobalCardID["DuneScorpion"] = 49] = "DuneScorpion";
            GlobalCardID[GlobalCardID["TidalWyrm"] = 50] = "TidalWyrm";
            GlobalCardID[GlobalCardID["Kali"] = 51] = "Kali";
            GlobalCardID[GlobalCardID["Zombee"] = 52] = "Zombee";
            GlobalCardID[GlobalCardID["Zombeetle"] = 53] = "Zombeetle";
            GlobalCardID[GlobalCardID["TheWatcher"] = 54] = "TheWatcher";
            GlobalCardID[GlobalCardID["PeacockSpider"] = 55] = "PeacockSpider";
            GlobalCardID[GlobalCardID["Bloatshroom"] = 56] = "Bloatshroom";
            GlobalCardID[GlobalCardID["Krawler"] = 57] = "Krawler";
            GlobalCardID[GlobalCardID["HauntedCloth"] = 58] = "HauntedCloth";
            GlobalCardID[GlobalCardID["Warden"] = 61] = "Warden";
            GlobalCardID[GlobalCardID["JumpingSpider"] = 63] = "JumpingSpider";
            GlobalCardID[GlobalCardID["MimicSpider"] = 64] = "MimicSpider";
            GlobalCardID[GlobalCardID["LeafbugNinja"] = 65] = "LeafbugNinja";
            GlobalCardID[GlobalCardID["LeafbugArcher"] = 66] = "LeafbugArcher";
            GlobalCardID[GlobalCardID["LeafbugClubber"] = 67] = "LeafbugClubber";
            GlobalCardID[GlobalCardID["Madesphy"] = 68] = "Madesphy";
            GlobalCardID[GlobalCardID["TheBeast"] = 69] = "TheBeast";
            GlobalCardID[GlobalCardID["ChomperBrute"] = 70] = "ChomperBrute";
            GlobalCardID[GlobalCardID["Mantidfly"] = 71] = "Mantidfly";
            GlobalCardID[GlobalCardID["GeneralUltimax"] = 72] = "GeneralUltimax";
            GlobalCardID[GlobalCardID["WildChomper"] = 73] = "WildChomper";
            GlobalCardID[GlobalCardID["Cross"] = 74] = "Cross";
            GlobalCardID[GlobalCardID["Poi"] = 75] = "Poi";
            GlobalCardID[GlobalCardID["PrimalWeevil"] = 76] = "PrimalWeevil";
            GlobalCardID[GlobalCardID["FalseMonarch"] = 77] = "FalseMonarch";
            GlobalCardID[GlobalCardID["Mothfly"] = 78] = "Mothfly";
            GlobalCardID[GlobalCardID["MothflyCluster"] = 79] = "MothflyCluster";
            GlobalCardID[GlobalCardID["Ironnail"] = 80] = "Ironnail";
            GlobalCardID[GlobalCardID["Belostoss"] = 81] = "Belostoss";
            GlobalCardID[GlobalCardID["Ruffian"] = 82] = "Ruffian";
            GlobalCardID[GlobalCardID["WaterStrider"] = 83] = "WaterStrider";
            GlobalCardID[GlobalCardID["DivingSpider"] = 84] = "DivingSpider";
            GlobalCardID[GlobalCardID["Cenn"] = 85] = "Cenn";
            GlobalCardID[GlobalCardID["Pisci"] = 86] = "Pisci";
            GlobalCardID[GlobalCardID["DeadLander\u03B1"] = 87] = "DeadLander\u03B1";
            GlobalCardID[GlobalCardID["DeadLander\u03B2"] = 88] = "DeadLander\u03B2";
            GlobalCardID[GlobalCardID["DeadLander\u03B3"] = 89] = "DeadLander\u03B3";
            GlobalCardID[GlobalCardID["WaspKing"] = 90] = "WaspKing";
            GlobalCardID[GlobalCardID["TheEverlastingKing"] = 91] = "TheEverlastingKing";
            GlobalCardID[GlobalCardID["Maki"] = 92] = "Maki";
            GlobalCardID[GlobalCardID["Kina"] = 93] = "Kina";
            GlobalCardID[GlobalCardID["Yin"] = 94] = "Yin";
            GlobalCardID[GlobalCardID["UltimaxTank"] = 95] = "UltimaxTank";
            GlobalCardID[GlobalCardID["Zommoth"] = 96] = "Zommoth";
            GlobalCardID[GlobalCardID["Riz"] = 97] = "Riz";
            GlobalCardID[GlobalCardID["Devourer"] = 98] = "Devourer";
        })(GlobalCardID = CardData.GlobalCardID || (CardData.GlobalCardID = {}));
        let BossCardOrder;
        (function (BossCardOrder) {
            BossCardOrder[BossCardOrder["Spider"] = 0] = "Spider";
            BossCardOrder[BossCardOrder["VenusGuardian"] = 1] = "VenusGuardian";
            BossCardOrder[BossCardOrder["HeavyDroneB33"] = 2] = "HeavyDroneB33";
            BossCardOrder[BossCardOrder["TheWatcher"] = 3] = "TheWatcher";
            BossCardOrder[BossCardOrder["TheBeast"] = 4] = "TheBeast";
            BossCardOrder[BossCardOrder["UltimaxTank"] = 5] = "UltimaxTank";
            BossCardOrder[BossCardOrder["MotherChomper"] = 6] = "MotherChomper";
            BossCardOrder[BossCardOrder["Broodmother"] = 7] = "Broodmother";
            BossCardOrder[BossCardOrder["Zommoth"] = 8] = "Zommoth";
            BossCardOrder[BossCardOrder["SeedlingKing"] = 9] = "SeedlingKing";
            BossCardOrder[BossCardOrder["TidalWyrm"] = 10] = "TidalWyrm";
            BossCardOrder[BossCardOrder["PeacockSpider"] = 11] = "PeacockSpider";
            BossCardOrder[BossCardOrder["Devourer"] = 12] = "Devourer";
            BossCardOrder[BossCardOrder["FalseMonarch"] = 13] = "FalseMonarch";
            BossCardOrder[BossCardOrder["Maki"] = 14] = "Maki";
            BossCardOrder[BossCardOrder["WaspKing"] = 15] = "WaspKing";
            BossCardOrder[BossCardOrder["TheEverlastingKing"] = 16] = "TheEverlastingKing";
        })(BossCardOrder = CardData.BossCardOrder || (CardData.BossCardOrder = {}));
        let MiniBossCardOrder;
        (function (MiniBossCardOrder) {
            MiniBossCardOrder[MiniBossCardOrder["Ahoneynation"] = 0] = "Ahoneynation";
            MiniBossCardOrder[MiniBossCardOrder["AcolyteAria"] = 1] = "AcolyteAria";
            MiniBossCardOrder[MiniBossCardOrder["Mothiva"] = 2] = "Mothiva";
            MiniBossCardOrder[MiniBossCardOrder["Zasp"] = 3] = "Zasp";
            MiniBossCardOrder[MiniBossCardOrder["Astotheles"] = 4] = "Astotheles";
            MiniBossCardOrder[MiniBossCardOrder["DuneScorpion"] = 5] = "DuneScorpion";
            MiniBossCardOrder[MiniBossCardOrder["PrimalWeevil"] = 6] = "PrimalWeevil";
            MiniBossCardOrder[MiniBossCardOrder["Cross"] = 7] = "Cross";
            MiniBossCardOrder[MiniBossCardOrder["Poi"] = 8] = "Poi";
            MiniBossCardOrder[MiniBossCardOrder["GeneralUltimax"] = 9] = "GeneralUltimax";
            MiniBossCardOrder[MiniBossCardOrder["Cenn"] = 10] = "Cenn";
            MiniBossCardOrder[MiniBossCardOrder["Pisci"] = 11] = "Pisci";
            MiniBossCardOrder[MiniBossCardOrder["MonsieurScarlet"] = 12] = "MonsieurScarlet";
            MiniBossCardOrder[MiniBossCardOrder["Kabbu"] = 13] = "Kabbu";
            MiniBossCardOrder[MiniBossCardOrder["Kali"] = 14] = "Kali";
            MiniBossCardOrder[MiniBossCardOrder["Carmina"] = 15] = "Carmina";
            MiniBossCardOrder[MiniBossCardOrder["Riz"] = 16] = "Riz";
            MiniBossCardOrder[MiniBossCardOrder["Kina"] = 17] = "Kina";
            MiniBossCardOrder[MiniBossCardOrder["Yin"] = 18] = "Yin";
            MiniBossCardOrder[MiniBossCardOrder["DeadLander\u03B1"] = 19] = "DeadLander\u03B1";
            MiniBossCardOrder[MiniBossCardOrder["DeadLander\u03B2"] = 20] = "DeadLander\u03B2";
            MiniBossCardOrder[MiniBossCardOrder["DeadLander\u03B3"] = 21] = "DeadLander\u03B3";
        })(MiniBossCardOrder = CardData.MiniBossCardOrder || (CardData.MiniBossCardOrder = {}));
        let AttackerCardOrder;
        (function (AttackerCardOrder) {
            AttackerCardOrder[AttackerCardOrder["Seedling"] = 0] = "Seedling";
            AttackerCardOrder[AttackerCardOrder["Underling"] = 1] = "Underling";
            AttackerCardOrder[AttackerCardOrder["GoldenSeedling"] = 2] = "GoldenSeedling";
            AttackerCardOrder[AttackerCardOrder["Zombiant"] = 3] = "Zombiant";
            AttackerCardOrder[AttackerCardOrder["Zombee"] = 4] = "Zombee";
            AttackerCardOrder[AttackerCardOrder["Zombeetle"] = 5] = "Zombeetle";
            AttackerCardOrder[AttackerCardOrder["Jellyshroom"] = 6] = "Jellyshroom";
            AttackerCardOrder[AttackerCardOrder["Bloatshroom"] = 7] = "Bloatshroom";
            AttackerCardOrder[AttackerCardOrder["Chomper"] = 8] = "Chomper";
            AttackerCardOrder[AttackerCardOrder["ChomperBrute"] = 9] = "ChomperBrute";
            AttackerCardOrder[AttackerCardOrder["Psicorp"] = 10] = "Psicorp";
            AttackerCardOrder[AttackerCardOrder["ArrowWorm"] = 11] = "ArrowWorm";
            AttackerCardOrder[AttackerCardOrder["Thief"] = 12] = "Thief";
            AttackerCardOrder[AttackerCardOrder["Bandit"] = 13] = "Bandit";
            AttackerCardOrder[AttackerCardOrder["Burglar"] = 14] = "Burglar";
            AttackerCardOrder[AttackerCardOrder["Ruffian"] = 15] = "Ruffian";
            AttackerCardOrder[AttackerCardOrder["SecurityTurret"] = 16] = "SecurityTurret";
            AttackerCardOrder[AttackerCardOrder["Abomihoney"] = 17] = "Abomihoney";
            AttackerCardOrder[AttackerCardOrder["Krawler"] = 18] = "Krawler";
            AttackerCardOrder[AttackerCardOrder["Warden"] = 19] = "Warden";
            AttackerCardOrder[AttackerCardOrder["HauntedCloth"] = 20] = "HauntedCloth";
            AttackerCardOrder[AttackerCardOrder["Mantidfly"] = 21] = "Mantidfly";
            AttackerCardOrder[AttackerCardOrder["JumpingSpider"] = 22] = "JumpingSpider";
            AttackerCardOrder[AttackerCardOrder["MimicSpider"] = 23] = "MimicSpider";
            AttackerCardOrder[AttackerCardOrder["DivingSpider"] = 24] = "DivingSpider";
            AttackerCardOrder[AttackerCardOrder["WaterStrider"] = 25] = "WaterStrider";
            AttackerCardOrder[AttackerCardOrder["Belostoss"] = 26] = "Belostoss";
            AttackerCardOrder[AttackerCardOrder["Mothfly"] = 27] = "Mothfly";
            AttackerCardOrder[AttackerCardOrder["MothflyCluster"] = 28] = "MothflyCluster";
            AttackerCardOrder[AttackerCardOrder["WaspScout"] = 29] = "WaspScout";
            AttackerCardOrder[AttackerCardOrder["WaspTrooper"] = 30] = "WaspTrooper";
        })(AttackerCardOrder = CardData.AttackerCardOrder || (CardData.AttackerCardOrder = {}));
        let EffectCardOrder;
        (function (EffectCardOrder) {
            EffectCardOrder[EffectCardOrder["Acornling"] = 0] = "Acornling";
            EffectCardOrder[EffectCardOrder["Cactiling"] = 1] = "Cactiling";
            EffectCardOrder[EffectCardOrder["Flowerling"] = 2] = "Flowerling";
            EffectCardOrder[EffectCardOrder["Plumpling"] = 3] = "Plumpling";
            EffectCardOrder[EffectCardOrder["Inichas"] = 4] = "Inichas";
            EffectCardOrder[EffectCardOrder["Denmuki"] = 5] = "Denmuki";
            EffectCardOrder[EffectCardOrder["Madesphy"] = 6] = "Madesphy";
            EffectCardOrder[EffectCardOrder["Numbnail"] = 7] = "Numbnail";
            EffectCardOrder[EffectCardOrder["Ironnail"] = 8] = "Ironnail";
            EffectCardOrder[EffectCardOrder["Midge"] = 9] = "Midge";
            EffectCardOrder[EffectCardOrder["WildChomper"] = 10] = "WildChomper";
            EffectCardOrder[EffectCardOrder["Weevil"] = 11] = "Weevil";
            EffectCardOrder[EffectCardOrder["BeeBoop"] = 12] = "BeeBoop";
            EffectCardOrder[EffectCardOrder["Mender"] = 13] = "Mender";
            EffectCardOrder[EffectCardOrder["LeafbugNinja"] = 14] = "LeafbugNinja";
            EffectCardOrder[EffectCardOrder["LeafbugArcher"] = 15] = "LeafbugArcher";
            EffectCardOrder[EffectCardOrder["LeafbugClubber"] = 16] = "LeafbugClubber";
            EffectCardOrder[EffectCardOrder["WaspBomber"] = 17] = "WaspBomber";
            EffectCardOrder[EffectCardOrder["WaspDriller"] = 18] = "WaspDriller";
            EffectCardOrder[EffectCardOrder["VenusBud"] = 19] = "VenusBud";
        })(EffectCardOrder = CardData.EffectCardOrder || (CardData.EffectCardOrder = {}));
        CardData.CardNameOverride = {
            [GlobalCardID.VenusBud]: "Venus' Bud",
            [GlobalCardID.AcolyteAria]: "Acolyte Aria",
            [GlobalCardID.VenusGuardian]: "Venus' Guardian",
            [GlobalCardID.WaspTrooper]: "Wasp Trooper",
            [GlobalCardID.WaspBomber]: "Wasp Bomber",
            [GlobalCardID.WaspDriller]: "Wasp Driller",
            [GlobalCardID.WaspScout]: "Wasp Scout",
            [GlobalCardID.MonsieurScarlet]: "Monsieur Scarlet",
            [GlobalCardID.GoldenSeedling]: "Golden Seedling",
            [GlobalCardID.ArrowWorm]: "Arrow Worm",
            [GlobalCardID.SeedlingKing]: "Seedling King",
            [GlobalCardID.MotherChomper]: "Mother Chomper",
            [GlobalCardID.BeeBoop]: "Bee-Boop",
            [GlobalCardID.SecurityTurret]: "Security Turret",
            [GlobalCardID.HeavyDroneB33]: "Heavy Drone B-33",
            [GlobalCardID.DuneScorpion]: "Dune Scorpion",
            [GlobalCardID.TidalWyrm]: "Tidal Wyrm",
            [GlobalCardID.TheWatcher]: "The Watcher",
            [GlobalCardID.PeacockSpider]: "Peacock Spider",
            [GlobalCardID.HauntedCloth]: "Haunted Cloth",
            [GlobalCardID.JumpingSpider]: "Jumping Spider",
            [GlobalCardID.MimicSpider]: "Mimic Spider",
            [GlobalCardID.LeafbugNinja]: "Leafbug Ninja",
            [GlobalCardID.LeafbugArcher]: "Leafbug Archer",
            [GlobalCardID.LeafbugClubber]: "Leafbug Clubber",
            [GlobalCardID.TheBeast]: "The Beast",
            [GlobalCardID.ChomperBrute]: "Chomper Brute",
            [GlobalCardID.GeneralUltimax]: "General Ultimax",
            [GlobalCardID.WildChomper]: "Wild Chomper",
            [GlobalCardID.PrimalWeevil]: "Primal Weevil",
            [GlobalCardID.FalseMonarch]: "False Monarch",
            [GlobalCardID.MothflyCluster]: "Mothfly Cluster",
            [GlobalCardID.WaterStrider]: "Water Strider",
            [GlobalCardID.DivingSpider]: "Diving Spider",
            [GlobalCardID.DeadLanderα]: "Dead Lander α",
            [GlobalCardID.DeadLanderβ]: "Dead Lander β",
            [GlobalCardID.DeadLanderγ]: "Dead Lander γ",
            [GlobalCardID.WaspKing]: "Wasp King",
            [GlobalCardID.TheEverlastingKing]: "The Everlasting King",
            [GlobalCardID.UltimaxTank]: "ULTIMAX Tank"
        };
        const vanillaCardByName = {};
        for (let card = 0; card < 128; card++) {
            if (!GlobalCardID[card]) {
                continue;
            }
            if (CardData.CardNameOverride[card]) {
                vanillaCardByName[CardData.CardNameOverride[card]] = card;
            }
            else {
                vanillaCardByName[GlobalCardID[card]] = card;
            }
        }
        // typo fixes
        vanillaCardByName["Belosstoss"] = GlobalCardID.Belostoss;
        function cardByName(name) {
            if (name.startsWith("Custom ")) {
                let rank;
                let index;
                if (name.startsWith("Custom Attacker #")) {
                    rank = SpyCards.Rank.Attacker;
                    index = parseInt(name.substr("Custom Attacker #".length), 10);
                }
                else if (name.startsWith("Custom Effect #")) {
                    rank = SpyCards.Rank.Effect;
                    index = parseInt(name.substr("Custom Effect #".length), 10);
                }
                else if (name.startsWith("Custom Mini-Boss #")) {
                    rank = SpyCards.Rank.MiniBoss;
                    index = parseInt(name.substr("Custom Mini-Boss #".length), 10);
                }
                else if (name.startsWith("Custom Boss #")) {
                    rank = SpyCards.Rank.Boss;
                    index = parseInt(name.substr("Custom Boss #".length), 10);
                }
                else {
                    throw new Error("unknown card name: " + name);
                }
                if (index >= 1) {
                    index--;
                    return 128 + ((index & ~31) << 2) | (rank << 5) | (index & 31);
                }
            }
            if (vanillaCardByName.hasOwnProperty(name)) {
                return vanillaCardByName[name];
            }
            throw new Error("unknown card name: " + name);
        }
        CardData.cardByName = cardByName;
    })(CardData = SpyCards.CardData || (SpyCards.CardData = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    let Rank;
    (function (Rank) {
        Rank[Rank["Attacker"] = 0] = "Attacker";
        Rank[Rank["Effect"] = 1] = "Effect";
        Rank[Rank["MiniBoss"] = 2] = "MiniBoss";
        Rank[Rank["Boss"] = 3] = "Boss";
        Rank[Rank["Enemy"] = 4] = "Enemy";
        Rank[Rank["_reserved5"] = 5] = "_reserved5";
        Rank[Rank["_reserved6"] = 6] = "_reserved6";
        Rank[Rank["None"] = 7] = "None";
    })(Rank = SpyCards.Rank || (SpyCards.Rank = {}));
    function rankName(rank) {
        switch (rank) {
            case Rank.MiniBoss:
                return "Mini-Boss";
            default:
                return Rank[rank];
        }
    }
    SpyCards.rankName = rankName;
    let Tribe;
    (function (Tribe) {
        Tribe[Tribe["Seedling"] = 0] = "Seedling";
        Tribe[Tribe["Wasp"] = 1] = "Wasp";
        Tribe[Tribe["Fungi"] = 2] = "Fungi";
        Tribe[Tribe["Zombie"] = 3] = "Zombie";
        Tribe[Tribe["Plant"] = 4] = "Plant";
        Tribe[Tribe["Bug"] = 5] = "Bug";
        Tribe[Tribe["Bot"] = 6] = "Bot";
        Tribe[Tribe["Thug"] = 7] = "Thug";
        Tribe[Tribe["Unknown"] = 8] = "Unknown";
        Tribe[Tribe["Chomper"] = 9] = "Chomper";
        Tribe[Tribe["Leafbug"] = 10] = "Leafbug";
        Tribe[Tribe["DeadLander"] = 11] = "DeadLander";
        Tribe[Tribe["Mothfly"] = 12] = "Mothfly";
        Tribe[Tribe["Spider"] = 13] = "Spider";
        Tribe[Tribe["Custom"] = 14] = "Custom";
        Tribe[Tribe["None"] = 15] = "None";
    })(Tribe = SpyCards.Tribe || (SpyCards.Tribe = {}));
    function tribeName(tribe, customName) {
        switch (tribe) {
            case Tribe.Unknown:
                return "???";
            case Tribe.DeadLander:
                return "Dead Lander";
            case Tribe.Custom:
                return customName;
            default:
                return Tribe[tribe];
        }
    }
    SpyCards.tribeName = tribeName;
    class CustomTribeData {
        constructor() {
            this.rgb = 0x808080;
            this.name = "";
        }
        marshal(buf) {
            buf.push((this.rgb >> 16) & 255);
            buf.push((this.rgb >> 8) & 255);
            buf.push(this.rgb & 255);
            SpyCards.Binary.pushUTF8StringVar(buf, this.name);
        }
        unmarshal(buf) {
            const r = buf.shift();
            const g = buf.shift();
            const b = buf.shift();
            this.rgb = (r << 16) | (g << 8) | b;
            this.name = SpyCards.Binary.shiftUTF8StringVar(buf);
        }
        unmarshalV1(buf) {
            const r = buf.shift();
            const g = buf.shift();
            const b = buf.shift();
            this.rgb = (r << 16) | (g << 8) | b;
            this.name = SpyCards.Binary.shiftUTF8String1(buf);
        }
    }
    SpyCards.CustomTribeData = CustomTribeData;
    let SpecialField;
    (function (SpecialField) {
        SpecialField[SpecialField["TP"] = 0] = "TP";
        SpecialField[SpecialField["ExtraTribe"] = 1] = "ExtraTribe";
    })(SpecialField || (SpecialField = {}));
    class CardDef {
        constructor() {
            this.name = "";
            this.tp = 1;
            this.portrait = 0x80;
            this.tribes = [{ tribe: Tribe.Unknown, custom: null }];
            this.effects = [];
        }
        rank() {
            if (this.id < 128) {
                const cardName = SpyCards.CardData.GlobalCardID[this.id];
                if (cardName in SpyCards.CardData.AttackerCardOrder) {
                    return Rank.Attacker;
                }
                if (cardName in SpyCards.CardData.EffectCardOrder) {
                    return Rank.Effect;
                }
                if (cardName in SpyCards.CardData.MiniBossCardOrder) {
                    return Rank.MiniBoss;
                }
                if (cardName in SpyCards.CardData.BossCardOrder) {
                    return Rank.Boss;
                }
                throw new Error("unknown card ID " + this.id + " (" + cardName + ")");
            }
            return (this.id >> 5) & 3;
        }
        getBackID() {
            switch (this.rank()) {
                case Rank.Boss:
                    return 2;
                case Rank.MiniBoss:
                    return 1;
                default:
                    return 0;
            }
        }
        originalName() {
            if (this.id >= 128) {
                const index = ((((this.id - 128) & ~127) >> 2) | (this.id & 31)) + 1;
                const rank = this.rank();
                if (rank === Rank.MiniBoss) {
                    return "Custom Mini-Boss #" + index;
                }
                return "Custom " + Rank[rank] + " #" + index;
            }
            return SpyCards.CardData.CardNameOverride[this.id] || SpyCards.CardData.GlobalCardID[this.id] || ("MissingCard?" + this.id + "?");
        }
        displayName() {
            return this.name || this.originalName();
        }
        effectiveTP() {
            let tp = this.tp;
            for (let effect of this.effects) {
                if (effect.type === EffectType.TP) {
                    tp -= effect.negate ? -effect.amount : effect.amount;
                }
            }
            return tp;
        }
        tribeName(n) {
            return this.tribes[n] ? tribeName(this.tribes[n].tribe, this.tribes[n].custom && this.tribes[n].custom.name) : "None";
        }
        marshal(buf) {
            SpyCards.Binary.pushUVarInt(buf, 4); // format version
            SpyCards.Binary.pushUVarInt(buf, this.id);
            if (this.tribes.length === 0) {
                buf.push((Tribe.Unknown << 4) | Tribe.None);
            }
            else if (this.tribes.length === 1) {
                buf.push((this.tribes[0].tribe << 4) | Tribe.None);
            }
            else {
                buf.push((this.tribes[0].tribe << 4) | this.tribes[1].tribe);
            }
            for (let i = 2; i < this.tribes.length; i++) {
                buf.push((SpecialField.ExtraTribe << 4) | this.tribes[i].tribe);
                if (this.tribes[i].tribe === Tribe.Custom) {
                    this.tribes[i].custom.marshal(buf);
                }
            }
            let overflowTP = null;
            if (this.tp === Infinity) {
                buf.push(15);
            }
            else if (this.tp < 0) {
                overflowTP = this.tp;
                buf.push(0);
            }
            else if (this.tp > 10) {
                overflowTP = this.tp - 10;
                buf.push(10);
            }
            else {
                if ((this.tp === 0 || this.tp === 10) && this.effects.length && this.effects[0].type === EffectType.TP) {
                    overflowTP = 0;
                }
                buf.push(this.tp);
            }
            buf.push(this.portrait);
            SpyCards.Binary.pushUTF8StringVar(buf, this.name === this.originalName() ? "" : this.name);
            const filteredEffects = this.effects.filter((e) => e.isValid());
            if (overflowTP !== null) {
                const tpEffect = new EffectDef();
                tpEffect.type = EffectType.TP;
                tpEffect.negate = overflowTP > 0;
                tpEffect.amount = Math.abs(overflowTP);
                filteredEffects.unshift(tpEffect);
            }
            SpyCards.Binary.pushUVarInt(buf, filteredEffects.length);
            for (let effect of filteredEffects) {
                effect.marshal(buf);
            }
            if (this.tribes.length >= 1 && this.tribes[0].tribe === Tribe.Custom) {
                this.tribes[0].custom.marshal(buf);
            }
            if (this.tribes.length >= 2 && this.tribes[1].tribe === Tribe.Custom) {
                this.tribes[1].custom.marshal(buf);
            }
            if (this.portrait === 254 || this.portrait === 255) {
                SpyCards.Binary.pushUVarInt(buf, this.customPortrait.length);
                for (let i = 0; i < this.customPortrait.length; i += 128) {
                    const sub = this.customPortrait.subarray(i);
                    buf.push(...SpyCards.toArray(sub.length > 128 ? sub.subarray(0, 128) : sub));
                }
            }
        }
        unmarshal(buf) {
            const formatVersion = SpyCards.Binary.shiftUVarInt(buf);
            if (formatVersion === 0 || formatVersion === 1) {
                this.unmarshalV1(buf, formatVersion);
                return;
            }
            if (formatVersion !== 2 && formatVersion !== 4) {
                throw new Error("unexpected format version for card definition: " + formatVersion);
            }
            this.id = SpyCards.Binary.shiftUVarInt(buf);
            const tribes = buf.shift();
            this.tribes = [
                {
                    tribe: tribes >> 4,
                    custom: null
                }
            ];
            if ((tribes & 15) !== Tribe.None) {
                this.tribes.push({
                    tribe: tribes & 15,
                    custom: null
                });
            }
            let special = buf.shift();
            while (special >> 4 !== SpecialField.TP) {
                switch (special >> 4) {
                    case SpecialField.ExtraTribe:
                        if (this.tribes.length < 2) {
                            throw new Error("special field ExtraTribe cannot appear on a card with only 1 existing tribe");
                        }
                        if ((special & 15) === Tribe.None) {
                            throw new Error("special field ExtraTribe cannot have tribe None");
                        }
                        const t = {
                            tribe: special & 15,
                            custom: null
                        };
                        if (t.tribe === Tribe.Custom) {
                            t.custom = new CustomTribeData();
                            t.custom.unmarshal(buf);
                        }
                        this.tribes.push(t);
                        break;
                    default:
                        throw new Error("unknown special field: " + (special >> 4));
                }
                special = buf.shift();
            }
            if (special <= 10) {
                this.tp = special;
            }
            else if (special === 15) {
                this.tp = Infinity;
            }
            else {
                throw new Error("invalid value for TP byte: " + special);
            }
            this.portrait = buf.shift();
            if (this.portrait > 234 && this.portrait < 254) {
                throw new Error("invalid value for portrait byte: " + this.portrait);
            }
            this.name = SpyCards.Binary.shiftUTF8StringVar(buf);
            const effectCount = SpyCards.Binary.shiftUVarInt(buf);
            this.effects = [];
            for (let i = 0; i < effectCount; i++) {
                const effect = new EffectDef();
                effect.unmarshal(buf, formatVersion);
                this.effects.push(effect);
            }
            if ((this.tp === 0 || this.tp === 10) && effectCount && this.effects[0].type === EffectType.TP) {
                const tpEffect = this.effects.shift();
                this.tp -= tpEffect.negate ? -tpEffect.amount : tpEffect.amount;
            }
            if (this.tribes.length >= 1 && this.tribes[0].tribe === Tribe.Custom) {
                this.tribes[0].custom = new CustomTribeData();
                this.tribes[0].custom.unmarshal(buf);
            }
            if (this.tribes.length >= 2 && this.tribes[1].tribe === Tribe.Custom) {
                this.tribes[1].custom = new CustomTribeData();
                this.tribes[1].custom.unmarshal(buf);
            }
            if (this.portrait === 254 || this.portrait === 255) {
                const customPortraitLength = SpyCards.Binary.shiftUVarInt(buf);
                if (customPortraitLength > buf.length) {
                    throw new Error("custom portrait length longer than remaining data");
                }
                this.customPortrait = new Uint8Array(buf.splice(0, customPortraitLength));
            }
            else {
                this.customPortrait = null;
            }
            if (buf.length) {
                throw new Error("extra data after end of card code");
            }
        }
        unmarshalV1(buf, formatVersion) {
            this.id = buf.shift();
            const tribes = buf.shift();
            this.tribes = [
                {
                    tribe: tribes >> 4,
                    custom: null
                }
            ];
            if ((tribes & 15) !== Tribe.None) {
                this.tribes.push({
                    tribe: tribes & 15,
                    custom: null
                });
            }
            let customPortraitID = 254;
            const tp = buf.shift();
            if (tp & 0x40) {
                customPortraitID = 255;
            }
            const codeRank = (tp >> 4) & 3;
            const actualRank = this.rank();
            if (codeRank !== actualRank) {
                throw new Error("expected rank " + Rank[actualRank] + " but card code specifies " + Rank[codeRank]);
            }
            if ((tp & 15) <= 10) {
                this.tp = tp & 15;
            }
            else if ((tp & 15) === 15) {
                this.tp = Infinity;
            }
            else {
                throw new Error("invalid value for TP byte: " + tp);
            }
            this.portrait = buf.shift();
            if (this.portrait > 234 && this.portrait !== 255) {
                throw new Error("invalid value for portrait byte: " + this.portrait);
            }
            if (this.portrait === 255) {
                this.portrait = customPortraitID;
            }
            else if (customPortraitID === 255) {
                throw new Error("external portrait bit is set but portrait is not custom");
            }
            this.name = SpyCards.Binary.shiftUTF8String1(buf);
            if (formatVersion === 0 && this.rank() === Rank.Attacker && (buf.length === 0 || buf[0] === 137)) {
                this.effects = [new EffectDef()];
                this.effects[0].type = EffectType.Stat;
                this.effects[0].amount = this.tp;
            }
            else {
                const effectCount = buf.shift();
                this.effects = [];
                for (let i = 0; i < effectCount; i++) {
                    const effect = new EffectDef();
                    effect.unmarshal(buf, formatVersion);
                    this.effects.push(effect);
                }
            }
            if (this.tribes.length >= 1 && this.tribes[0].tribe === Tribe.Custom) {
                this.tribes[0].custom = new CustomTribeData();
                this.tribes[0].custom.unmarshalV1(buf);
            }
            if (this.tribes.length >= 2 && this.tribes[1].tribe === Tribe.Custom) {
                this.tribes[1].custom = new CustomTribeData();
                this.tribes[1].custom.unmarshalV1(buf);
            }
            if (this.portrait === 254 || this.portrait === 255) {
                this.customPortrait = new Uint8Array(buf.splice(0, buf.length));
            }
            else {
                this.customPortrait = null;
            }
            if (buf.length) {
                throw new Error("extra data after end of card code");
            }
        }
        createEl(defs) {
            return SpyCards.createCardEl(defs, this);
        }
    }
    SpyCards.CardDef = CardDef;
    let EffectType;
    (function (EffectType) {
        EffectType[EffectType["FlavorText"] = 0] = "FlavorText";
        EffectType[EffectType["Stat"] = 1] = "Stat";
        EffectType[EffectType["Empower"] = 2] = "Empower";
        EffectType[EffectType["Summon"] = 3] = "Summon";
        EffectType[EffectType["Heal"] = 4] = "Heal";
        EffectType[EffectType["TP"] = 5] = "TP";
        EffectType[EffectType["Numb"] = 6] = "Numb";
        EffectType[EffectType["CondCard"] = 128] = "CondCard";
        EffectType[EffectType["CondLimit"] = 129] = "CondLimit";
        EffectType[EffectType["CondWinner"] = 130] = "CondWinner";
        EffectType[EffectType["CondApply"] = 131] = "CondApply";
        EffectType[EffectType["CondCoin"] = 132] = "CondCoin";
        EffectType[EffectType["CondHP"] = 133] = "CondHP";
        EffectType[EffectType["CondStat"] = 134] = "CondStat";
        EffectType[EffectType["CondPriority"] = 135] = "CondPriority";
        EffectType[EffectType["CondOnNumb"] = 136] = "CondOnNumb";
    })(EffectType = SpyCards.EffectType || (SpyCards.EffectType = {}));
    class EffectDef {
        constructor() {
            this.type = null;
            this.text = "";
            this.amount = 1;
            this.rank = Rank.None;
            this.tribe = Tribe.None;
        }
        marshal(buf) {
            buf.push(this.type);
            let flags = 0;
            flags |= this.negate ? 1 << 0 : 0;
            flags |= this.opponent ? 1 << 1 : 0;
            flags |= this.each ? 1 << 2 : 0;
            flags |= this.late ? 1 << 3 : 0;
            flags |= this.generic ? 1 << 4 : 0;
            flags |= this.defense ? 1 << 5 : 0;
            flags |= this._reserved6 ? 1 << 6 : 0;
            flags |= this._reserved7 ? 1 << 7 : 0;
            buf.push(flags);
            switch (this.type) {
                case EffectType.FlavorText:
                    SpyCards.Binary.pushUTF8StringVar(buf, this.text);
                    break;
                case EffectType.Stat:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    break;
                case EffectType.Empower:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    this.marshalFilter(buf);
                    break;
                case EffectType.Summon:
                    buf.push(this.amount - 1);
                    this.marshalFilter(buf);
                    break;
                case EffectType.Heal:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    break;
                case EffectType.TP:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    break;
                case EffectType.Numb:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    this.marshalFilter(buf);
                    break;
                case EffectType.CondCard:
                    if (!this.each) {
                        buf.push(this.amount - 1);
                    }
                    this.marshalFilter(buf);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondLimit:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondWinner:
                    this.result.marshal(buf);
                    break;
                case EffectType.CondApply:
                    this.result.marshal(buf);
                    break;
                case EffectType.CondCoin:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    if (this.generic) {
                        this.tailsResult.marshal(buf);
                    }
                    break;
                case EffectType.CondHP:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondStat:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondPriority:
                    buf.push(this.amount);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondOnNumb:
                    this.result.marshal(buf);
                    break;
                default:
                    throw new Error("unhandled effect type: " + this.type + " (" + EffectType[this.type] + ")");
            }
        }
        marshalFilter(buf) {
            if (this.generic) {
                buf.push((this.rank << 4) | this.tribe);
                if (this.tribe === Tribe.Custom) {
                    SpyCards.Binary.pushUTF8StringVar(buf, this.customTribe);
                }
            }
            else {
                SpyCards.Binary.pushUVarInt(buf, this.card);
            }
        }
        unmarshal(buf, formatVersion) {
            if (formatVersion === 0 || formatVersion === 1) {
                this.unmarshalV1(buf, formatVersion);
                return;
            }
            this.type = buf.shift();
            const flags = buf.shift();
            this.negate = !!((flags >> 0) & 1);
            this.opponent = !!((flags >> 1) & 1);
            this.each = !!((flags >> 2) & 1);
            this.late = !!((flags >> 3) & 1);
            this.generic = !!((flags >> 4) & 1);
            this.defense = !!((flags >> 5) & 1);
            this._reserved6 = !!((flags >> 6) & 1);
            this._reserved7 = !!((flags >> 7) & 1);
            this.text = "";
            this.amount = 1;
            this.card = null;
            this.rank = Rank.None;
            this.tribe = Tribe.None;
            this.customTribe = null;
            this.result = null;
            this.tailsResult = null;
            function shiftAmount(buf) {
                const amount = buf.shift();
                return amount === 255 ? Infinity : amount;
            }
            function shiftCount(buf) {
                const count = buf.shift();
                return count + 1;
            }
            switch (this.type) {
                case EffectType.FlavorText:
                    this.text = SpyCards.Binary.shiftUTF8StringVar(buf);
                    break;
                case EffectType.Stat:
                    this.amount = shiftAmount(buf);
                    break;
                case EffectType.Empower:
                    this.amount = shiftAmount(buf);
                    this.unmarshalFilter(buf);
                    break;
                case EffectType.Summon:
                    this.amount = shiftCount(buf);
                    this.unmarshalFilter(buf);
                    break;
                case EffectType.Heal:
                    this.amount = shiftAmount(buf);
                    break;
                case EffectType.TP:
                    this.amount = shiftAmount(buf);
                    break;
                case EffectType.Numb:
                    this.amount = shiftAmount(buf);
                    if (formatVersion >= 4) {
                        this.unmarshalFilter(buf);
                    }
                    else {
                        this.opponent = true;
                        this.generic = true;
                        this.rank = Rank.Attacker;
                        this.tribe = Tribe.None;
                    }
                    break;
                case EffectType.CondCard:
                    if (!this.each) {
                        this.amount = shiftCount(buf);
                    }
                    this.unmarshalFilter(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondLimit:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondWinner:
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondApply:
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondCoin:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    if (this.generic) {
                        this.tailsResult = new EffectDef();
                        this.tailsResult.unmarshal(buf, formatVersion);
                    }
                    break;
                case EffectType.CondHP:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondStat:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondPriority:
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondOnNumb:
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                default:
                    throw new Error("unhandled effect type: " + this.type + " (" + EffectType[this.type] + ")");
            }
        }
        unmarshalFilter(buf) {
            if (this.generic) {
                const rankTribe = buf.shift();
                this.rank = rankTribe >> 4;
                this.tribe = rankTribe & 15;
                if (this.tribe === Tribe.Custom) {
                    this.customTribe = SpyCards.Binary.shiftUTF8StringVar(buf);
                }
            }
            else {
                this.card = SpyCards.Binary.shiftUVarInt(buf);
            }
        }
        unmarshalV1(buf, formatVersion) {
            this.negate = false;
            this.opponent = false;
            this.each = false;
            this.late = false;
            this.generic = false;
            this.defense = false;
            this._reserved6 = false;
            this._reserved7 = false;
            this.text = "";
            this.amount = 1;
            this.card = null;
            this.rank = Rank.None;
            this.tribe = Tribe.None;
            this.customTribe = null;
            this.result = null;
            this.tailsResult = null;
            function shiftAmount(buf) {
                const n = buf.shift();
                if (n === 0) {
                    return [Infinity, false];
                }
                if (n >= 128) {
                    return [256 - n, true];
                }
                return [n, false];
            }
            function shiftTribe(buf) {
                const n = buf.shift();
                if ((n & 15) !== Tribe.Custom) {
                    return [n, null];
                }
                return [n, SpyCards.Binary.shiftUTF8String1(buf)];
            }
            const legacyType = buf.shift();
            switch (legacyType) {
                case 0: // ATK (static)
                    this.type = EffectType.Stat;
                    this.defense = false;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 1: // DEF (static)
                    this.type = EffectType.Stat;
                    this.defense = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 2: // ATK (static, once per turn)
                    this.type = EffectType.CondLimit;
                    this.negate = false;
                    this.amount = 1;
                    this.result = new EffectDef();
                    this.result.type = EffectType.Stat;
                    [this.result.amount, this.result.negate] = shiftAmount(buf);
                    break;
                case 3: // ATK+n
                    this.type = EffectType.Stat;
                    this.defense = false;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 4: // DEF+n
                    this.type = EffectType.Stat;
                    this.defense = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 5: // Empower (card)
                    this.type = EffectType.Empower;
                    this.generic = false;
                    this.defense = false;
                    this.card = buf.shift();
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 6: // Empower (tribe)
                    this.type = EffectType.Empower;
                    this.generic = true;
                    this.defense = false;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 7: // Heal
                    this.type = EffectType.Heal;
                    this.opponent = false;
                    this.each = false;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 8: // Lifesteal
                    this.type = EffectType.CondWinner;
                    this.negate = false;
                    this.opponent = false;
                    this.result = new EffectDef();
                    this.result.type = EffectType.Heal;
                    [this.result.amount, this.result.negate] = shiftAmount(buf);
                    break;
                case 9: // Numb
                    this.type = EffectType.Numb;
                    [this.amount] = shiftAmount(buf);
                    this.opponent = true;
                    this.generic = true;
                    this.rank = Rank.Attacker;
                    this.tribe = Tribe.None;
                    break;
                case 10: // Pierce
                    this.type = EffectType.Stat;
                    this.defense = true;
                    this.opponent = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    this.negate = !this.negate;
                    break;
                case 11: // Summon
                    this.type = EffectType.Summon;
                    this.negate = false;
                    this.opponent = false;
                    this.generic = false;
                    this.amount = 1;
                    this.card = buf.shift();
                    break;
                case 12: // Summon Rank
                    this.type = EffectType.Summon;
                    this.negate = true;
                    this.opponent = false;
                    this.generic = true;
                    this.amount = 1;
                    if (formatVersion === 0) {
                        this.rank = Rank.MiniBoss;
                        this.tribe = Tribe.None;
                    }
                    else {
                        this.rank = buf.shift();
                        this.tribe = Tribe.None;
                    }
                    break;
                case 13: // Summon Tribe
                    this.type = EffectType.Summon;
                    this.negate = false;
                    this.opponent = false;
                    this.generic = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    if (this.tribe >> 4 === 0) {
                        this.rank = Rank.Enemy;
                    }
                    else {
                        this.rank = (this.tribe >> 4) - 1;
                        this.tribe &= 15;
                    }
                    this.amount = buf.shift();
                    break;
                case 14: // Unity
                    this.type = EffectType.CondLimit;
                    this.negate = false;
                    this.amount = 1;
                    this.result = new EffectDef();
                    this.result.type = EffectType.Empower;
                    this.result.generic = true;
                    [this.result.tribe, this.result.customTribe] = shiftTribe(buf);
                    [this.result.amount, this.result.negate] = shiftAmount(buf);
                    break;
                case 15: // TP
                    this.type = EffectType.TP;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 16: // Summon as Opponent
                    this.type = EffectType.Summon;
                    this.negate = false;
                    this.opponent = true;
                    this.generic = false;
                    this.amount = 1;
                    this.card = buf.shift();
                    break;
                case 17: // Multiply Healing
                    this.type = EffectType.Heal;
                    this.opponent = false;
                    this.each = true;
                    this.generic = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    if (this.negate) {
                        this.amount--;
                        if (this.amount === 0) {
                            this.negate = false;
                        }
                    }
                    else {
                        this.amount++;
                    }
                    break;
                case 18: // Flavor Text
                    this.type = EffectType.FlavorText;
                    this.negate = !!(buf.shift() & 1);
                    this.text = SpyCards.Binary.shiftUTF8String1(buf);
                    break;
                case 128: // Coin
                    this.type = EffectType.CondCoin;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    const count = buf.shift();
                    this.generic = !!(count & 0x80);
                    this.amount = (count & 0x7f) + 1;
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    if (this.generic) {
                        this.tailsResult = new EffectDef();
                        this.tailsResult.unmarshal(buf, formatVersion);
                    }
                    this.defense = this.generic &&
                        this.result.type === EffectType.Stat &&
                        !this.result.defense &&
                        this.tailsResult.type === EffectType.Stat &&
                        this.tailsResult.defense;
                    break;
                case 129: // If Card
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.card = buf.shift();
                    if (formatVersion === 1) {
                        this.amount = buf.shift();
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 130: // Per Card
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.each = true;
                    this.card = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 131: // If Tribe
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 132: // VS Tribe
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.opponent = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    if (formatVersion === 1) {
                        this.amount = buf.shift();
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 133: // If Stat
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 134: // Setup
                    this.type = EffectType.CondApply;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.late = true;
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 135: // Limit
                    this.type = EffectType.CondLimit;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 136: // VS Card
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.card = buf.shift();
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 137: // If Rank
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.rank = buf.shift();
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 138: // VS Rank
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.opponent = true;
                    this.rank = buf.shift();
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 139: // If Win
                    this.type = EffectType.CondWinner;
                    if (formatVersion === 1) {
                        this.opponent = !!(buf.shift() & 1);
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 140: // If Tie
                    this.type = EffectType.CondWinner;
                    if (formatVersion === 1) {
                        this.opponent = !!(buf.shift() & 1);
                    }
                    this.negate = true;
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 141: // If Stat (late)
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.late = true;
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 142: // VS Stat
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 143: // VS Stat (late)
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.late = true;
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 144: // If HP
                    this.type = EffectType.CondHP;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 145: // Per Tribe
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.each = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 146: // Per Rank
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.each = true;
                    this.rank = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 147: // VS HP
                    this.type = EffectType.CondHP;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                default:
                    throw new Error("unexpected effect type ID " + legacyType);
            }
        }
        isValid() {
            if (this.type === null) {
                return false;
            }
            if (this.type >= 128 && (!this.result || !this.result.isValid())) {
                return false;
            }
            if (this.type === EffectType.CondCoin && this.generic && (!this.tailsResult || !this.tailsResult.isValid())) {
                return false;
            }
            return true;
        }
        createEl(defs, card) {
            return SpyCards.createEffectEl(defs, card, this);
        }
    }
    SpyCards.EffectDef = EffectDef;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    class CardDefs {
        constructor(overrides) {
            this.cardBacks = CardDefs.cardBacks;
            this.allCards = [];
            this.cardsByID = {};
            this.cardsByBack = [[], [], []];
            this.nonRandom = {
                [SpyCards.Rank.None]: [],
                [SpyCards.Rank.Enemy]: [],
                [SpyCards.Rank.Attacker]: [],
                [SpyCards.Rank.Effect]: [],
                [SpyCards.Rank.MiniBoss]: [],
                [SpyCards.Rank.Boss]: []
            };
            this.byTribe = {
                [SpyCards.Rank.None]: {},
                [SpyCards.Rank.Enemy]: {},
                [SpyCards.Rank.Attacker]: {},
                [SpyCards.Rank.Effect]: {},
                [SpyCards.Rank.MiniBoss]: {},
                [SpyCards.Rank.Boss]: {},
            };
            this.banned = {};
            this.unpickable = {};
            this.unfilter = {};
            this.rules = new SpyCards.GameModeGameRules();
            this.allCards = SpyCards.CardData.createVanillaCardDefs(overrides ? overrides.packedVersion : null);
            if (overrides) {
                if (overrides.mode) {
                    this.mode = overrides.mode;
                    for (let banList of overrides.mode.getAll(SpyCards.GameModeFieldType.BannedCards)) {
                        if (banList.bannedCards.length === 0) {
                            for (let card of this.allCards) {
                                this.banned[card.id] = true;
                            }
                        }
                        else {
                            for (let id of banList.bannedCards) {
                                this.banned[id] = true;
                            }
                        }
                    }
                    for (let unfilter of overrides.mode.getAll(SpyCards.GameModeFieldType.UnfilterCard)) {
                        this.unfilter[unfilter.card] = true;
                    }
                    this.rules = overrides.mode.get(SpyCards.GameModeFieldType.GameRules) || new SpyCards.GameModeGameRules();
                }
                else {
                    this.mode = null;
                }
                this.allCards = this.allCards.filter((c) => !overrides.cards.some((o) => o.id === c.id));
                this.allCards.push(...overrides.cards);
                this.customCardsRaw = overrides.customCardsRaw;
            }
            else {
                this.customCardsRaw = [];
                this.mode = null;
            }
            for (let cd of this.allCards) {
                const rank = cd.rank();
                this.cardsByID[cd.id] = cd;
                if (cd.effectiveTP() !== Infinity) { // negative infinity is okay
                    this.cardsByBack[cd.getBackID()].push(cd);
                }
                if (!cd.effects.some((effect) => effect.type === SpyCards.EffectType.Summon && effect.negate)) {
                    this.nonRandom[SpyCards.Rank.None].push(cd);
                    if (rank === SpyCards.Rank.Attacker || rank === SpyCards.Rank.Effect) {
                        this.nonRandom[SpyCards.Rank.Enemy].push(cd);
                    }
                    this.nonRandom[rank].push(cd);
                }
                this.indexCard(cd, SpyCards.Rank.None, SpyCards.Tribe.None);
                if (rank === SpyCards.Rank.Attacker || rank === SpyCards.Rank.Effect) {
                    this.indexCard(cd, SpyCards.Rank.Enemy, SpyCards.Tribe.None);
                }
                this.indexCard(cd, rank, SpyCards.Tribe.None);
                for (let t of cd.tribes) {
                    this.indexCard(cd, SpyCards.Rank.None, t.tribe, t.custom);
                    if (rank === SpyCards.Rank.Attacker || rank === SpyCards.Rank.Effect) {
                        this.indexCard(cd, SpyCards.Rank.Enemy, t.tribe, t.custom);
                    }
                    this.indexCard(cd, rank, t.tribe, t.custom);
                }
            }
        }
        indexCard(card, rank, tribe, custom) {
            const tribeName = tribe === SpyCards.Tribe.Custom ? "?" + custom.name : SpyCards.Tribe[tribe];
            if (!this.byTribe[rank][tribeName]) {
                this.byTribe[rank][tribeName] = [];
            }
            this.byTribe[rank][tribeName].push(card);
        }
        getAvailableCards(rank, tribe, customTribe) {
            const tribeName = tribe === SpyCards.Tribe.Custom ? "?" + customTribe : SpyCards.Tribe[tribe];
            return (this.byTribe[rank][tribeName] || []).filter((c) => !this.banned[c.id]);
        }
    }
    CardDefs.cardBacks = ["enemy", "mini-boss", "boss"];
    SpyCards.CardDefs = CardDefs;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    function createUnknownCardEl(back) {
        const el = document.createElement("div");
        el.classList.add("card", "unknown", "flipped");
        el.classList.add("card-type-" + ["enemy", "mini-boss", "boss"][back]);
        const backEl = document.createElement("div");
        backEl.setAttribute("aria-label", "Back of " + ["an Enemy", "a Mini-Boss", "a Boss"][back] + " Card");
        backEl.classList.add("back");
        el.appendChild(backEl);
        return el;
    }
    SpyCards.createUnknownCardEl = createUnknownCardEl;
    function flipCard(el, force) {
        el.classList.toggle("flipped", force);
        const front = el.querySelector(":scope > .front");
        const back = el.querySelector(":scope > .back");
        if (el.classList.contains("flipped")) {
            if (front) {
                front.setAttribute("aria-hidden", "true");
            }
            if (back) {
                back.removeAttribute("aria-hidden");
            }
        }
        else {
            if (front) {
                front.removeAttribute("aria-hidden");
            }
            if (back) {
                back.setAttribute("aria-hidden", "true");
            }
        }
    }
    SpyCards.flipCard = flipCard;
    function createCardEl(defs, card) {
        const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
        const tpMul = (sg && sg.m && (sg.m & 16)) ? 2 : 1;
        const el = document.createElement("div");
        el.classList.add("card");
        switch (card.rank()) {
            case SpyCards.Rank.Attacker:
                el.classList.add("card-type-attacker");
                break;
            case SpyCards.Rank.Effect:
                el.classList.add("card-type-effect");
                break;
            case SpyCards.Rank.MiniBoss:
                el.classList.add("card-type-mini-boss");
                break;
            case SpyCards.Rank.Boss:
                el.classList.add("card-type-boss");
                break;
        }
        const front = document.createElement("div");
        front.classList.add("front");
        front.setAttribute("aria-label", SpyCards.rankName(card.rank()) + " card: " + card.displayName());
        el.appendChild(front);
        const back = document.createElement("div");
        back.classList.add("back");
        back.setAttribute("aria-hidden", "true");
        switch (card.rank()) {
            case SpyCards.Rank.Attacker:
            case SpyCards.Rank.Effect:
                back.setAttribute("aria-label", "Back of an Enemy Card");
                break;
            case SpyCards.Rank.MiniBoss:
                back.setAttribute("aria-label", "Back of a Mini-Boss Card");
                break;
            case SpyCards.Rank.Boss:
                back.setAttribute("aria-label", "Back of a Boss Card");
                break;
        }
        el.appendChild(back);
        const name = document.createElement("span");
        name.classList.add("card-name");
        const nameSquish = document.createElement("span");
        nameSquish.classList.add("squish");
        nameSquish.textContent = card.displayName();
        name.appendChild(nameSquish);
        front.appendChild(name);
        const tp = document.createElement("span");
        tp.classList.add("card-tp");
        const effectiveTP = card.effectiveTP() * tpMul;
        tp.title = effectiveTP < 0 ? "Gain " + (-effectiveTP) + " teamwork points on the turn you play this card." :
            isFinite(effectiveTP) ? "Teamwork Points Required: " + effectiveTP :
                "This card cannot be played through normal means.";
        tp.textContent = isNaN(effectiveTP) ? "NaN" : isFinite(effectiveTP) ? String(effectiveTP) : (effectiveTP < 0 ? "-∞" : "∞");
        if (effectiveTP < 0) {
            tp.setAttribute("data-negative", "");
        }
        if (effectiveTP === Infinity) {
            tp.setAttribute("data-infinity", "");
        }
        if (effectiveTP !== Infinity || !defs.banned[card.id]) {
            front.appendChild(tp);
        }
        const portrait = document.createElement("div");
        portrait.classList.add("portrait");
        portrait.setAttribute("data-x", String(card.portrait & 15));
        portrait.setAttribute("data-y", String(card.portrait >> 4));
        front.appendChild(portrait);
        if (card.customPortrait) {
            const customPortrait = document.createElement("img");
            if (card.portrait === 255) {
                customPortrait.src = user_image_base_url + CrockfordBase32.encode(card.customPortrait) + ".png";
            }
            else {
                customPortrait.src = "data:image/png;base64," + Base64.encode(card.customPortrait);
            }
            customPortrait.crossOrigin = "anonymous";
            customPortrait.width = 128;
            customPortrait.height = 128;
            portrait.appendChild(customPortrait);
        }
        const descWrapper = document.createElement("div");
        descWrapper.classList.add("card-desc-wrapper");
        const desc = document.createElement("div");
        desc.setAttribute("role", "list");
        desc.classList.add("card-desc", "squishy");
        descWrapper.appendChild(desc);
        front.appendChild(descWrapper);
        const tribes = document.createElement("div");
        tribes.classList.add("card-tribe-placeholders");
        tribes.style.setProperty("--numtribes", String(card.tribes.filter((t) => !t.custom || t.custom.name.indexOf("_") !== 0).length));
        front.appendChild(tribes);
        for (let i = 0; i < card.tribes.length; i++) {
            if (card.tribes[i].custom && card.tribes[i].custom.name.charAt(0) === "_") {
                continue;
            }
            const tribeEl = document.createElement("span");
            tribeEl.classList.add("card-tribe-placeholder");
            tribeEl.setAttribute("data-tribe", card.tribeName(i));
            tribeEl.title = "Tribe: " + (card.tribeName(i) === "???" ? "Unknown" : card.tribeName(i));
            const tribeSquish = document.createElement("span");
            tribeSquish.classList.add("squish");
            tribeSquish.textContent = card.tribeName(i);
            tribeEl.appendChild(tribeSquish);
            if (card.tribes[i].custom) {
                tribeEl.style.setProperty("--custom-color", "#" + (0x1000000 + card.tribes[i].custom.rgb).toString(16).substr(1));
            }
            tribes.appendChild(tribeEl);
        }
        for (let effect of card.effects) {
            const effectEl = effect.createEl(defs, card);
            effectEl.setAttribute("role", "listitem");
            desc.appendChild(effectEl);
        }
        if (!disableDoSquish) {
            requestAnimationFrame(() => doSquish(el));
        }
        return el;
    }
    SpyCards.createCardEl = createCardEl;
    function createEffectEl(defs, card, effect, childOf) {
        const el = document.createElement("span");
        if (!effect || effect.type === null) {
            el.classList.add("card-null");
            return el;
        }
        switch (effect.type) {
            case SpyCards.EffectType.FlavorText:
                el.classList.add("card-flavor-text");
                el.classList.toggle("hide-remaining-effects", effect.negate);
                el.title = "Flavor text: " + effect.text;
                el.textContent = effect.text;
                break;
            case SpyCards.EffectType.Stat:
                el.classList.add((childOf ? "card-effect-" : "card-") + (effect.defense ? "def" : "atk"));
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                if (!effect.opponent) {
                    const stat = effect.defense ? "Defense" : "Attack";
                    el.title = (childOf ? "Increase " + stat + " by " : "Card " + stat + ": ") + (effect.negate ? "-" : "") + effect.amount;
                }
                el.textContent = isFinite(effect.amount) ? String(effect.amount) : "∞";
                break;
            case SpyCards.EffectType.Empower:
                if (childOf && childOf.classList.contains("card-condition-limit") && effect.generic && effect.rank === SpyCards.Rank.None && effect.tribe !== SpyCards.Tribe.None && !effect.opponent && !effect.defense) {
                    childOf.classList.add("possibly-unity");
                }
                el.classList.add("card-effect-empower");
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                el.setAttribute("data-stat", effect.defense ? "DEF" : "ATK");
                el.setAttribute("data-count", String(effect.amount));
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                    }
                }
                else {
                    el.setAttribute("data-card", defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?");
                }
                break;
            case SpyCards.EffectType.Summon:
                el.classList.add(effect.negate ?
                    (effect.generic ? "card-effect-replace-random" : "card-effect-replace-card") :
                    (effect.generic ? "card-effect-summon-random" : "card-effect-summon-card"));
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                el.setAttribute("data-count", String(effect.amount));
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                    }
                }
                else {
                    el.setAttribute("data-card", defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?");
                    if (effect.card === card.id) {
                        el.setAttribute("data-same-card", "");
                    }
                }
                break;
            case SpyCards.EffectType.Heal:
                el.classList.add(effect.each ? "card-effect-multiply-healing" : "card-effect-heal");
                if (childOf && childOf.classList.contains("card-condition-winner") && !effect.each && !effect.opponent) {
                    childOf.classList.add("possibly-lifesteal");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                if (effect.generic) {
                    el.setAttribute("data-generic", "");
                }
                if (effect.defense) {
                    el.setAttribute("data-defense", "");
                }
                el.textContent = isFinite(effect.amount) ? String(effect.amount) : "∞";
                break;
            case SpyCards.EffectType.TP:
                const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
                const tpMul = (sg && sg.m && (sg.m & 16)) ? 2 : 1;
                el.classList.add("card-effect-tp");
                if (effect.negate) {
                    el.setAttribute("data-negative", "");
                }
                el.textContent = isFinite(effect.amount) ? String(effect.amount * tpMul) : "∞";
                break;
            case SpyCards.EffectType.Numb:
                el.classList.add("card-effect-numb");
                el.textContent = isFinite(effect.amount) ? String(effect.amount) : "∞";
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                el.title = "Numb " + (isFinite(effect.amount) ? "cards (limit " + effect.amount + ")" : "all cards");
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                        el.title += " with rank: " + SpyCards.rankName(effect.rank);
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                        el.title += " with tribe: " + SpyCards.tribeName(effect.tribe, effect.customTribe);
                    }
                }
                else {
                    const cardName = defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?";
                    el.setAttribute("data-card", cardName);
                    el.title += " with name: " + cardName;
                }
                break;
            case SpyCards.EffectType.CondCard:
                el.classList.add("card-condition-card");
                if (effect.generic) {
                    if (effect.rank !== SpyCards.Rank.None) {
                        el.setAttribute("data-rank", SpyCards.rankName(effect.rank));
                    }
                    if (effect.tribe !== SpyCards.Tribe.None) {
                        el.setAttribute("data-tribe", SpyCards.tribeName(effect.tribe, effect.customTribe));
                    }
                }
                else {
                    el.setAttribute("data-card", defs.cardsByID[effect.card] ? defs.cardsByID[effect.card].displayName() : "?" + effect.card + "?");
                }
                el.setAttribute("data-count", String(effect.amount));
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.each) {
                    el.setAttribute("data-each", "");
                }
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondLimit:
                el.classList.add("card-condition-limit");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                el.setAttribute("data-count", String(effect.amount));
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondWinner:
                el.classList.add("card-condition-winner");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondApply:
                // negate flag used internally
                el.classList.add(effect.late ? "card-condition-setup" : "card-condition-apply");
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.late) {
                    let count = 1;
                    for (let child = effect.result; child.type === SpyCards.EffectType.CondApply; child = child.result) {
                        if (effect.late === child.late && !child.opponent && effect.negate === child.negate) {
                            count++;
                        }
                        else {
                            break;
                        }
                    }
                    el.setAttribute("data-count", String(count));
                }
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondCoin:
                el.classList.add("card-condition-coin");
                el.setAttribute("data-count", String(effect.amount));
                const heads = document.createElement("span");
                heads.classList.add("card-coin-heads");
                const headsEffect = createEffectEl(defs, card, effect.result, el);
                heads.appendChild(headsEffect);
                heads.title = "Apply effect if coin lands on Heads";
                el.appendChild(heads);
                if (effect.generic) {
                    const tails = document.createElement("span");
                    tails.classList.add("card-coin-tails");
                    const tailsEffect = createEffectEl(defs, card, effect.tailsResult, el);
                    tails.appendChild(tailsEffect);
                    tails.title = "Apply effect if coin lands on Tails";
                    el.appendChild(tails);
                }
                break;
            case SpyCards.EffectType.CondHP:
                el.classList.add("card-condition-hp");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.late) {
                    el.setAttribute("data-late", "");
                }
                el.setAttribute("data-count", String(effect.amount));
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondStat:
                el.classList.add("card-condition-stat");
                if (effect.negate) {
                    el.setAttribute("data-negate", "");
                }
                if (effect.opponent) {
                    el.setAttribute("data-opponent", "");
                }
                if (effect.late) {
                    el.setAttribute("data-late", "");
                }
                el.setAttribute("data-stat", effect.defense ? "DEF" : "ATK");
                el.setAttribute("data-count", String(effect.amount));
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondPriority:
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            case SpyCards.EffectType.CondOnNumb:
                el.classList.add("card-condition-on-numb");
                el.appendChild(createEffectEl(defs, card, effect.result, el));
                break;
            default:
                throw new Error("unhandled effect type: " + effect.type + " (" + SpyCards.EffectType[effect.type] + ")");
        }
        return el;
    }
    SpyCards.createEffectEl = createEffectEl;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var CardData;
    (function (CardData) {
        // version is a Spy Cards Online version number packed into a 24-bit integer
        function createVanillaCardDefs(version) {
            return [
                "AgLfAwIAAwEAAgMAAAgDAAAB",
                "AhhPBBUAAgEAAoAEEwEAAw==",
                "Ai5vBSAAAgEgAgIQAnY=",
                "AjY/BSsAAwEAAQMAADkDAAA9",
                "AkVfBTUAAgEAA4AAABcBAAQ=",
                "Al9vB5IAAQEACA==",
                "AilJBBkAAoIABAACAhACeQ==",
                "AiRfBEUAAgEAAgIAAh0=",
                "AmAjBH4AAQIQAnI=",
                "AiMEBCwAAQIQAnA=",
                "AjKPBXoAAgEAAgYA/w==",
                "AjffBX8AAQIQA30=",
                "AmJPBLkAAgEAA4ASAHUBAAM=",
                "Ak1cBUIAAQIQA3w=",
                "AlxfBkYAAQEABg==",
                "AlpfB0gAAQMQAUE=",
                "AltFCa4AAQEA/w==",
                "AiqPBSMAAYQAAQMAADA=",
                "AhVfA2QAAgEAAYQAAgMAABM=",
                "Ag9fAg8AAgEAAoAAAAMEAAE=",
                "AgNfAwMAAgEAA4AAAA8BAAI=",
                "AihXBSYAAQIQA3c=",
                "AjFfByUAAQEABw==",
                "AkxfBj4AAgEAAwIAAhE=",
                "AkpfAz8AAgEAAoAAAEsBAAI=",
                "AktfA0AAAgEgAYAAAEoBIAM=",
                "AkhRAzgAAgEAAYEAAAIQAXE=",
                "AlVfA00AAgEAAoAAAFYGAAE=",
                "AlZfA04AAgEgAoAAAFUBIAQ=",
                "Ah9fAxcAAgEAA4YABgQAAQ==",
                "AhdfAhEAAgEAAQEjAw==",
                "AjNfBCcAAgEgAoAAABcEAAM=",
                "AiJfBB0AAQMRAC8=",
                "AmFfA3wAAgEAAYMIAQAC",
                "Al1fBEcAAgEAAoAAAFwBAAM=",
                "Al5fA7gAAgEgAYAAAFwEAAI=",
                "Ale/BKAAAgEAAwEgAQ==",
                "Ali/BKEAAgEAAoQwAAEAAgEgAg==",
                "Alm/BqIAAgEAAwEgAw==",
                "AglAAQkAAQEAAQ==",
                "Ah4EAxYAAQEAAw==",
                "AiAECRgAAQEACQ==",
                "AgAjAQAAAQEAAQ==",
                "AjQjAzsAAQEAAw==",
                "AjUjBTwAAQEABQ==",
                "AgEvAQEAAQEAAQ==",
                "AjgvBD0AAQEABA==",
                "AhRJAhAAAQEAAg==",
                "AkZJBDYAAQEABA==",
                "AgVfAgUAAQEAAg==",
                "AiGPAyIAAQEAAw==",
                "AgZXAgYAAQEAAg==",
                "AgdXAwcAAQEAAw==",
                "AidXBBoAAQEABA==",
                "AlJXBk8AAQEABg==",
                "AixvAh4AAQEAAg==",
                "AjCPBBwAAQEABA==",
                "AjlvAigAAQEAAg==",
                "Aj1vAyoAAQEAAw==",
                "AjqPBCkAAQEABA==",
                "AkdfBEEAAQEABA==",
                "Aj/fAy8AAQEAAw==",
                "AkDfBTQAAQEABQ==",
                "AlTfA0wAAQEAAw==",
                "AlNfA0sAAQEAAw==",
                "AlFfBkoAAQEABg==",
                "Ak5cAUMAAQEAAQ==",
                "Ak9cA0QAAQEAAw==",
                "AhxRAiEAAQEAAg==",
                "AhkVBBMAAQEABA==",
                "AhBAAgsAAYQAAAEgAw==",
                "AgQEAwQAAYQAAAEgBA==",
                "AiZAAS0AAYIABAAB",
                "AiVABi4AAYQAAAEgBg==",
                "AghfAQgAAYQAAAEgAg==",
                "Ai1fAx8AAgEAAYQAAAYAAQ==",
                "AkRfBTMAAYQAAQEgAw==",
                "Ag6PAg4AAQYAAQ==",
                "AlCPA0kAAgEgAQYAAQ==",
                "BB1fAhQAAoEAAAEAAYEAAAIAAR0=",
                "AklJBDoAAgEAAYQAAAMAABQ=",
                "AhFfAgwAAgEAAYASAHQBAAI=",
                "AitvARsAAYQwAAEAAQEgAQ==",
                "Ai9vASQAAYAQBHYEAAE=",
                "AkFaAzAAAYEAAAIQAno=",
                "AkJaAjEAAYEAAAIQAXo=",
                "AkNaBDIAAgEAAoAQAnoGAAE=",
                "AhpRBTkAAwEAAoQAAAEAAYQAAAYAAQ==",
                "AhtRBjcAAgEABAEjAg==",
                "AhNPAQ0AAYIABAAB"
            ].map((code, i) => {
                const card = new SpyCards.CardDef();
                card.unmarshal(SpyCards.toArray(Base64.decode(code)));
                return card;
            });
        }
        CardData.createVanillaCardDefs = createVanillaCardDefs;
    })(CardData = SpyCards.CardData || (SpyCards.CardData = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
class SignalingConnection {
    constructor(fake = false) {
        this.onCloseCode = (code) => { debugger; };
        if (!fake) {
            this.fake = null;
            this.ws = new WebSocket(matchmaking_server);
            this.ws.addEventListener("message", this.onMessage.bind(this));
            this.ws.addEventListener("error", this.onError.bind(this));
            this.ws.addEventListener("close", this.onClose.bind(this));
            this.sessionID = new Promise((resolve, reject) => {
                this.sessionIDResolve = resolve;
                this.ws.addEventListener("close", function (e) {
                    reject(e);
                });
            });
            this.keepAlive = setInterval(() => this.ws.send("k"), 45000);
        }
        else {
            this.fake = fake === true ? new SignalingConnection(this) : fake;
            this.sessionID = new Promise((resolve) => {
                this.sessionIDResolve = resolve;
            });
        }
    }
    close() {
        clearInterval(this.keepAlive);
        this.keepAlive = null;
        this.onCloseCode = function () { };
        if (this.fake) {
            return;
        }
        if (this.ws.readyState === WebSocket.OPEN)
            this.ws.send("q");
        this.ws.close();
    }
    onMessage(e) {
        if (!e.data.length) {
            return;
        }
        switch (e.data.charAt(0)) {
            case "s":
                // session id
                this.sessionIDResolve(e.data.substr(1));
                break;
            case "k":
                // keep-alive
                break;
            case "p":
                // handshake
                this.onHandshake();
                break;
            case "r":
                // relayed message
                this.onRelayedMessage(JSON.parse(e.data.substr(1)));
                break;
            case "q":
                // quit
                if (!this.connectionReady) {
                    this.onCloseCode(3002);
                }
                this.close();
                break;
            default:
                console.log("SIGNALING SERVER MESSAGE:", e);
                debugger;
                break;
        }
    }
    onError(e) {
        console.log("SIGNALING SERVER ERROR:", e);
        debugger;
    }
    onClose(e) {
        clearTimeout(this.keepAlive);
        this.keepAlive = null;
        if (this.onCloseCode) {
            this.onCloseCode(e.code);
            return;
        }
        console.log("SIGNALING SERVER CLOSE:", e);
        debugger;
    }
    initPlayer(playerNumber) {
        if (this.fake) {
            switch (playerNumber) {
                case "q":
                    throw new Error("quick join not implemented in mock signaling connection");
                case 1:
                    this.onMessage(new MessageEvent("message", { data: "sFAKE FAKE FAKE FAKE FAKE FAKE" }));
                    break;
                case 2:
                    break;
            }
            return;
        }
        if (this.ws.readyState === WebSocket.CONNECTING) {
            this.ws.addEventListener("open", () => {
                this.ws.send("p" + playerNumber + spyCardsVersionSuffix);
            });
            return;
        }
        this.ws.send("p" + playerNumber + spyCardsVersionSuffix);
    }
    setSessionID(sessionID) {
        this.sessionIDResolve(sessionID);
        if (this.fake) {
            this.onMessage(new MessageEvent("message", { data: "p" }));
            this.fake.onMessage(new MessageEvent("message", { data: "p" }));
            return;
        }
        if (this.ws.readyState === WebSocket.CONNECTING) {
            this.ws.addEventListener("open", () => {
                this.ws.send("s" + sessionID);
            });
            return;
        }
        this.ws.send("s" + sessionID);
    }
    sendRelayedMessage(message) {
        if (this.fake) {
            this.fake.onMessage(new MessageEvent("message", { data: "r" + JSON.stringify(message) }));
            return;
        }
        if (this.ws.readyState === WebSocket.OPEN) {
            this.ws.send("r" + JSON.stringify(message));
        }
    }
}
const defaultICEServers = function () {
    if (window.RTCPeerConnection && RTCPeerConnection.getDefaultIceServers) {
        const servers = RTCPeerConnection.getDefaultIceServers();
        if (servers.length) {
            return servers;
        }
    }
    return [
        { urls: ["stun:stun.stunprotocol.org"] },
        { urls: ["stun:stun.sipnet.net"] }
    ];
}();
class CardGameConnection {
    constructor(config) {
        this.logID = "(conn-" + (CardGameConnection.nextLogID++) + ")";
        this.conn = new RTCPeerConnection(config || {
            iceServers: defaultICEServers,
            iceCandidatePoolSize: 4
        });
        this.conn.addEventListener("negotiationneeded", (e) => console.log("NEGOTIATION NEEDED", this.logID));
        this.conn.addEventListener("connectionstatechange", (e) => console.log("CONNECTION STATE CHANGE:", this.conn.connectionState, this.logID));
        this.conn.addEventListener("iceconnectionstatechange", (e) => console.log("ICE CONNECTION STATE CHANGE:", this.conn.iceConnectionState, this.logID));
        this.conn.addEventListener("icegatheringstatechange", (e) => console.log("ICE GATHERING STATE CHANGE:", this.conn.iceGatheringState, this.logID));
        this.conn.addEventListener("signalingstatechange", (e) => console.log("SIGNALING STATE CHANGE:", this.conn.signalingState, this.logID));
        this.conn.addEventListener("connectionstatechange", (e) => {
            if (this.weWantRelay || this.opponentWantsRelay || !this.onConnectionStateChange) {
                return;
            }
            this.onConnectionStateChange(this.conn.connectionState);
        });
        this.scg = new SecureCardGame();
        this.secure = this.conn.createDataChannel("secure", {
            negotiated: true,
            id: 0
        });
        this.secure.binaryType = "arraybuffer";
        this.queuedSecureMessages = [];
        this.secureMessage = this.createSecureMessagePromise();
        this.secure.addEventListener("message", (e) => {
            if (this.secureMessageResolve) {
                this.secureMessageResolve(e.data);
            }
            else {
                this.queuedSecureMessages.push(e.data);
            }
        });
        this.recvCustomCards = new Promise((resolve) => this.customCardsResolve = resolve);
        this.recvCosmeticData = new Promise((resolve) => this.cosmeticDataResolve = resolve);
        this.recvSpoilerGuard = new Promise((resolve) => this.spoilerGuardResolve = resolve);
        this.control = this.conn.createDataChannel("control", {
            negotiated: true,
            id: 1
        });
        this.control.binaryType = "arraybuffer";
        this.control.addEventListener("message", (e) => {
            const data = new Uint8Array(e.data, 0);
            switch (String.fromCharCode(data[0])) {
                case "v":
                    if (this.onVersion) {
                        this.onVersion(String.fromCharCode.apply(String, SpyCards.toArray(data.subarray(1))));
                    }
                    break;
                case "q":
                    this.onQuit();
                    break;
                case "r":
                    if (this.opponentReady) {
                        console.warn("spurious READY from opponent");
                        break;
                    }
                    this.opponentReady = true;
                    this.onReady(data.slice(1));
                    break;
                case "a":
                    if (this.weWantRematch) {
                        this.setupRematch();
                    }
                    else if (!this.opponentWantsRematch) {
                        this.opponentWantsRematch = true;
                        if (this.onOfferRematch) {
                            this.onOfferRematch();
                        }
                    }
                    break;
                case "c":
                    this.customCardsResolve([].map.call(data.subarray(1), (ch) => String.fromCharCode(ch)).join(""));
                    break;
                case "o":
                    this.cosmeticDataResolve(JSON.parse([].map.call(data.subarray(1), (ch) => String.fromCharCode(ch)).join("")));
                    break;
                case "g":
                    this.spoilerGuardResolve(data.slice(1));
                    break;
                default:
                    console.log("CONTROL CHANNEL MESSAGE:", data);
                    debugger;
            }
        });
    }
    async rand(shared, max) {
        return this.scg.rand(shared, max);
    }
    async shuffle(shared, items) {
        return this.scg.shuffle(shared, items);
    }
    async publicShuffle(cards, cardBacks) {
        return this.scg.publicShuffle(cards, cardBacks);
    }
    async privateShuffle(cards, cardBacks, remote) {
        return this.scg.privateShuffle(cards, cardBacks, remote);
    }
    setOnConnectionStateChange(callback) {
        this.onConnectionStateChange = callback;
        if (callback) {
            callback(this.conn.connectionState);
        }
    }
    createSecureMessagePromise() {
        if (this.queuedSecureMessages.length) {
            return Promise.resolve(this.queuedSecureMessages.shift());
        }
        return new Promise((resolve) => {
            this.secureMessageResolve = (data) => {
                this.secureMessageResolve = null;
                resolve(data);
            };
        });
    }
    useSignalingServer(signalConn, playerNumber) {
        this.sc = signalConn;
        this.conn.addEventListener("icecandidate", (e) => {
            if (this.weWantRelay) {
                return;
            }
            this.sc.sendRelayedMessage({
                type: "ice-candidate",
                candidate: e.candidate
            });
        });
        this.sc.onRelayedMessage = async (msg) => {
            switch (msg.type) {
                case "player1-offer":
                    if (playerNumber === 2) {
                        const answer = await this.initPlayer2(msg.offer);
                        this.sc.sendRelayedMessage({
                            type: "player2-answer",
                            answer: answer
                        });
                    }
                    else {
                        console.log("unexpected player1-offer as player 1");
                        debugger;
                    }
                    break;
                case "player2-answer":
                    if (playerNumber === 1) {
                        await this.acceptPlayer2(msg.answer);
                    }
                    else {
                        console.log("unexpected player2-answer as player 2");
                        debugger;
                    }
                    break;
                case "ice-candidate":
                    // Google Chrome doesn't like null candidates here.
                    if (msg.candidate) {
                        await this.conn.addIceCandidate(msg.candidate);
                    }
                    break;
                case "use-relay":
                    if (!this.opponentWantsRelay && !this.weWantRelay && this.onConnectionStateChange) {
                        this.onConnectionStateChange("want-relay");
                    }
                    this.opponentWantsRelay = true;
                    if (!this.usingRelay && this.weWantRelay) {
                        this.setupRelay();
                    }
                    break;
                case "r":
                    if (!this.usingRelay) {
                        console.log("unexpected relayed game message");
                        debugger;
                    }
                    switch (msg.c) {
                        case "control":
                            this.control.dispatchEvent(new MessageEvent("message", {
                                data: Base64.decode(msg.p).buffer
                            }));
                            break;
                        case "secure":
                            this.secure.dispatchEvent(new MessageEvent("message", {
                                data: Base64.decode(msg.p).buffer
                            }));
                            break;
                        default:
                            console.log("unexpected channel in relayed message:", msg.c, msg);
                            break;
                    }
                    break;
                default:
                    console.log("RELAYED MESSAGE:", msg);
                    debugger;
                    break;
            }
        };
        this.sc.onHandshake = async () => {
            try {
                if (new URLSearchParams(location.search).has("forceRelay") || SpyCards.loadSettings().forceRelay) {
                    this.weWantRelay = true;
                    if (this.onConnectionStateChange) {
                        this.onConnectionStateChange("connecting");
                    }
                    if (!this.usingRelay && this.opponentWantsRelay) {
                        this.setupRelay();
                    }
                    this.sc.sendRelayedMessage({ type: "use-relay" });
                }
            }
            catch (ex) {
                debugger;
            }
            if (playerNumber === 1) {
                const offer = await this.initPlayer1();
                this.sc.sendRelayedMessage({
                    type: "player1-offer",
                    offer: offer
                });
            }
        };
    }
    async closeSignalingConnection(sc) {
        sc.connectionReady = true;
        if (this.usingRelay) {
            return;
        }
        await Promise.all([
            this.ensureChannelOpen(this.secure),
            this.ensureChannelOpen(this.control)
        ]);
        sc.close();
    }
    async close() {
        try {
            await this.sendMessage(this.control, new Uint8Array(["q".charCodeAt(0)]));
        }
        catch (ex) {
            // ignore
        }
        this.conn.close();
    }
    useRelay() {
        this.weWantRelay = true;
        this.sc.sendRelayedMessage({ type: "use-relay" });
        if (!this.usingRelay && this.opponentWantsRelay) {
            this.setupRelay();
        }
    }
    setupRelay() {
        this.usingRelay = true;
        this.control.dispatchEvent(new Event("use-relay"));
        this.secure.dispatchEvent(new Event("use-relay"));
        if (this.onConnectionStateChange) {
            this.onConnectionStateChange("connected");
        }
    }
    offerRematch() {
        if (this.weWantRematch) {
            return;
        }
        this.weWantRematch = true;
        this.sendMessage(this.control, new Uint8Array(["a".charCodeAt(0)]));
        if (this.opponentWantsRematch) {
            this.setupRematch();
        }
    }
    setupRematch() {
        this.weWantRematch = false;
        this.opponentWantsRematch = false;
        this.finalized = false;
        this.opponentForfeit = false;
        this.scg = new SecureCardGame();
        this.onRematch();
    }
    async initPlayer1() {
        if (this.weWantRelay) {
            return null;
        }
        this.logID += " player-1";
        const offer = await this.conn.createOffer();
        await this.conn.setLocalDescription(offer);
        return this.conn.localDescription;
    }
    async initPlayer2(player1Offer) {
        if (!player1Offer || this.weWantRelay) {
            return null;
        }
        this.logID += " player-2";
        if (player1Offer.type !== "offer" || !player1Offer.sdp) {
            throw new Error("invalid offer");
        }
        await this.conn.setRemoteDescription(player1Offer);
        const answer = await this.conn.createAnswer();
        await this.conn.setLocalDescription(answer);
        return this.conn.localDescription;
    }
    async acceptPlayer2(player2Answer) {
        if (!player2Answer) {
            return;
        }
        if (player2Answer.type !== "answer" || !player2Answer.sdp) {
            throw new Error("invalid answer");
        }
        await this.conn.setRemoteDescription(player2Answer);
    }
    async ensureChannelOpen(channel) {
        while (!this.usingRelay && channel.readyState !== "open" && channel.readyState !== "closed") {
            await new Promise((resolve) => {
                function f() {
                    channel.removeEventListener("open", f);
                    channel.removeEventListener("closed", f);
                    channel.removeEventListener("use-relay", f);
                    resolve();
                }
                channel.addEventListener("open", f);
                channel.addEventListener("closed", f);
                channel.addEventListener("use-relay", f);
            });
        }
        if (this.usingRelay) {
            return {
                send: (data) => {
                    this.sc.sendRelayedMessage({
                        type: "r",
                        c: channel.label,
                        p: Base64.encode(new Uint8Array(data.buffer, 0)),
                    });
                }
            };
        }
        if (channel.readyState === "open") {
            return channel;
        }
        throw new Error(channel.label + " channel " + channel.readyState);
    }
    async sendMessage(channel, data) {
        (await this.ensureChannelOpen(channel)).send(data);
    }
    async exchangeDataAsync(input) {
        try {
            await this.sendMessage(this.secure, input);
        }
        catch (ex) {
            if (ex.message === "secure channel closed") {
                // another error message will appear first, as the connection has been
                // terminated. don't activate the crash handler in this situation.
                await new Promise(() => { }); // never resolve
            }
            throw ex;
        }
        const output = new Uint8Array(await this.secureMessage, 0);
        this.secureMessage = this.createSecureMessagePromise();
        return output;
    }
    initSecure(playerNumber) {
        return this.scg.init(playerNumber, this.exchangeDataAsync.bind(this));
    }
    initDeck(deck, cardBacks) {
        return this.scg.setDeck(this.exchangeDataAsync.bind(this), deck, cardBacks);
    }
    beginTurn() {
        this.opponentReady = false;
        this.confirmedTurn = false;
        return this.scg.beginTurn(this.exchangeDataAsync.bind(this));
    }
    sendVersion(version) {
        const versionData = ("v" + version).split("").map((ch) => ch.charCodeAt(0));
        this.sendMessage(this.control, new Uint8Array(versionData));
    }
    sendCustomCards(customCards) {
        const data = ("c" + customCards).split("").map((ch) => ch.charCodeAt(0));
        this.sendMessage(this.control, new Uint8Array(data));
    }
    sendCosmeticData(cosmetic) {
        const data = ("o" + JSON.stringify(cosmetic)).split("").map((ch) => ch.charCodeAt(0));
        this.sendMessage(this.control, new Uint8Array(data));
    }
    sendSpoilerGuardData(guardData) {
        const guard = Base64.decode(guardData);
        const data = new Uint8Array(guard.length + 1);
        data[0] = "g".charCodeAt(0);
        data.set(guard, 1);
        this.sendMessage(this.control, data);
    }
    async sendReady(data) {
        const promise = await this.scg.prepareTurn(data);
        const ready = new Uint8Array(promise.length + 1);
        ready[0] = "r".charCodeAt(0);
        ready.set(promise, 1);
        this.sendMessage(this.control, ready);
    }
    async confirmTurn() {
        this.confirmedTurn = true;
        return await this.scg.confirmTurn(this.exchangeDataAsync.bind(this));
    }
    async finalizeMatch(verifyDeckAsync, verifyTurnAsync) {
        this.finalized = true;
        return await this.scg.finalize(this.exchangeDataAsync.bind(this), verifyDeckAsync, verifyTurnAsync);
    }
}
CardGameConnection.nextLogID = 0;
"use strict";
var SpyCards;
(function (SpyCards) {
    class MatchData {
        constructor() {
            this.customModeName = "";
            this.rematchCount = 0;
            this.losses = 0;
            this.wins = 0;
        }
    }
    SpyCards.MatchData = MatchData;
    class MatchState {
        constructor(defs, npcState) {
            this.hp = [0, 0];
            this.tp = [0, 0];
            this.rawATK = [0, 0];
            this.rawDEF = [0, 0];
            this.ATK = [0, 0];
            this.DEF = [0, 0];
            this.healTotalN = [0, 0];
            this.healTotalP = [0, 0];
            this.healMultiplierN = [1, 1];
            this.healMultiplierP = [1, 1];
            this.turn = 0;
            this.deck = [[], []];
            this.backs = [[], []];
            this.hand = [[], []];
            this.discard = [[], []];
            this.once = [[], []];
            this.setup = [[], []];
            this.ready = [false, false];
            this.played = [[], []];
            this.winner = 0;
            this.players = [null, null];
            this.effectBacklog = [];
            this.gameLog = new SpyCards.GameLog();
            this.hp[0] = this.hp[1] = defs.rules.maxHP;
            this.npcState = npcState;
        }
    }
    SpyCards.MatchState = MatchState;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var Decks;
    (function (Decks) {
        function decode(defs, code) {
            return decodeBinary(defs, CrockfordBase32.decode(code));
        }
        Decks.decode = decode;
        function decodeBinary(defs, buf) {
            if (buf.length < 2) {
                throw new Error("invalid deck code (too short)");
            }
            if (!(buf[0] & 0x80)) {
                return toDeck(defs, decodeShort(buf));
            }
            if (buf[0] === 0x80) {
                return toDeck(defs, decodeLongV0(buf));
            }
            if (buf[0] === 0x81) {
                return toDeck(defs, decodeLongV1(buf));
            }
            if (buf[0] === 0x82) {
                return toDeck(defs, decodeLongV2(buf));
            }
            throw new Error("invalid deck code (unrecognized first byte " + buf[0] + ")");
        }
        Decks.decodeBinary = decodeBinary;
        function decodeShort(buf) {
            const names = [];
            names.push(SpyCards.CardData.BossCardOrder[buf[0] >> 2]);
            names.push(SpyCards.CardData.MiniBossCardOrder[((buf[0] & 3) << 3) | (buf[1] >> 5)]);
            names.push(SpyCards.CardData.MiniBossCardOrder[buf[1] & 31]);
            function pushEnemyCard(index) {
                const attackerCardCount = 31;
                const name = index < attackerCardCount ?
                    SpyCards.CardData.AttackerCardOrder[index] :
                    SpyCards.CardData.EffectCardOrder[index - attackerCardCount];
                names.push(name);
            }
            for (let i = 2; i < buf.length;) {
                pushEnemyCard(buf[i] >> 2);
                i++;
                if (i >= buf.length) {
                    break;
                }
                pushEnemyCard(((buf[i - 1] & 3) << 4) | (buf[i] >> 4));
                i++;
                if (i >= buf.length) {
                    break;
                }
                pushEnemyCard(((buf[i - 1] & 15) << 2) | (buf[i] >> 6));
                i++;
                if (i === buf.length && (buf[i - 1] & 63) === 63) {
                    break;
                }
                pushEnemyCard(buf[i - 1] & 63);
            }
            return names.map((name) => SpyCards.CardData.GlobalCardID[name]);
        }
        function decodeLongV0(buf) {
            return [].slice.call(buf, 1);
        }
        function decodeLongV1(buf) {
            const buf2 = [].slice.call(buf, 1);
            const ids = [];
            while (buf2.length) {
                ids.push(SpyCards.Binary.shiftUVarInt(buf2));
            }
            return ids;
        }
        function decodeLongV2(buf) {
            const buf2 = [].slice.call(buf, 1);
            const ids = [];
            while (buf2.length) {
                ids.push(SpyCards.Binary.shiftUVarInt(buf2) + 128);
            }
            return ids;
        }
        function toDeck(defs, ids) {
            return ids.map((id) => {
                if (defs.cardsByID[id]) {
                    return defs.cardsByID[id];
                }
                const placeholder = new SpyCards.CardDef();
                placeholder.id = id;
                return placeholder;
            });
        }
        function loadSaved(defs, validOnly) {
            const storedSerialized = localStorage["spy-cards-decks-v0"];
            if (!storedSerialized) {
                return [];
            }
            const storedDecks = JSON.parse(storedSerialized).filter((d) => Array.isArray(d) && d.indexOf("MissingCard?undefined?") === -1);
            const decks = storedDecks.map((s) => toDeck(defs, s.map((name) => SpyCards.CardData.cardByName(name))));
            return decks.filter((deck) => {
                if (!deck || !deck.length) {
                    return false;
                }
                if (validOnly && deck.length !== defs.rules.cardsPerDeck) {
                    return false;
                }
                let i = 0;
                for (let card of deck) {
                    if (!card) {
                        return false;
                    }
                    if (validOnly && !defs.cardsByID[card.id]) {
                        return false;
                    }
                    if (validOnly && (defs.cardsByID[card.id].effectiveTP() === Infinity || defs.banned[card.id] || defs.unpickable[card.id])) {
                        return false;
                    }
                    const expectedBack = i < 3 ? i === 0 ? 2 : 1 : 0;
                    if (card.getBackID() !== expectedBack) {
                        return false;
                    }
                    i++;
                }
                if (validOnly && defs.mode) {
                    for (let filter of defs.mode.getAll(SpyCards.GameModeFieldType.DeckLimitFilter)) {
                        if (deck.filter((c) => filter.match(c)).length > filter.count) {
                            return false;
                        }
                    }
                }
                return deck[1] !== deck[2];
            });
        }
        Decks.loadSaved = loadSaved;
        function encode(defs, deck) {
            return CrockfordBase32.encode(encodeBinary(defs, deck));
        }
        Decks.encode = encode;
        function encodeBacks(backs) {
            const buf = new Uint8Array(Math.floor((backs.length + 3) / 4));
            for (let i = 0; i < backs.length; i++) {
                switch (i & 3) {
                    case 0:
                        buf[i >> 2] = backs[i] << 6;
                        break;
                    case 1:
                        buf[i >> 2] |= backs[i] << 4;
                        break;
                    case 2:
                        buf[i >> 2] |= backs[i] << 2;
                        break;
                    case 3:
                        buf[i >> 2] |= backs[i];
                        break;
                }
            }
            switch (backs.length & 3) {
                case 1:
                    buf[buf.length - 1] |= 0x3f;
                    break;
                case 2:
                    buf[buf.length - 1] |= 0xf;
                    break;
                case 3:
                    buf[buf.length - 1] |= 0x3;
                    break;
            }
            return buf;
        }
        Decks.encodeBacks = encodeBacks;
        function decodeBacks(buf) {
            const d = [];
            for (let i = 0; i < buf.length; i++) {
                for (let shift = 6; shift >= 0; shift -= 2) {
                    switch ((buf[i] >> shift) & 3) {
                        case 0:
                            d.push(0);
                            break;
                        case 1:
                            d.push(1);
                            break;
                        case 2:
                            d.push(2);
                            break;
                        case 3:
                            if (i !== buf.length - 1) {
                                throw new Error("card: invalid unknown deck: ends at index " + i + " of " + buf.length);
                            }
                            for (let shift2 = shift; shift2 >= 0; shift2 -= 2) {
                                const v = (buf[i] >> shift2) & 3;
                                if (v !== 3) {
                                    throw new Error("card: invalid unknown deck: ends before non-padding value " + v);
                                }
                            }
                            return d;
                    }
                }
            }
            return d;
        }
        Decks.decodeBacks = decodeBacks;
        function encodeBinary(defs, deck) {
            if (!deck.length) {
                throw new Error("cannot encode empty deck");
            }
            if (deck.length >= 3 && deck.every((c) => c.id < 128) &&
                deck[0].rank() === SpyCards.Rank.Boss &&
                deck[1].rank() === SpyCards.Rank.MiniBoss &&
                deck[2].rank() === SpyCards.Rank.MiniBoss &&
                deck.slice(3).every((c) => c.getBackID() === 0)) {
                return encodeShort(deck);
            }
            if (deck.every((c) => c.id >= 128)) {
                const buf = [0x82];
                for (let card of deck) {
                    SpyCards.Binary.pushUVarInt(buf, card.id - 128);
                }
                return new Uint8Array(buf);
            }
            if (deck.every((c) => c.id < 256)) {
                const buf = [0x80];
                for (let card of deck) {
                    buf.push(card.id);
                }
                return new Uint8Array(buf);
            }
            const buf = [0x81];
            for (let card of deck) {
                SpyCards.Binary.pushUVarInt(buf, card.id);
            }
            return new Uint8Array(buf);
        }
        Decks.encodeBinary = encodeBinary;
        function encodeShort(deck) {
            const buf = [];
            let bitsRemaining = 0;
            function appendBits(x, n) {
                while (bitsRemaining < n) {
                    if (bitsRemaining) {
                        n -= bitsRemaining;
                        buf[buf.length - 1] |= x >> n;
                        x &= (1 << n) - 1;
                        bitsRemaining = 0;
                    }
                    else {
                        buf.push(0);
                        bitsRemaining += 8;
                    }
                }
                bitsRemaining -= n;
                buf[buf.length - 1] |= x << bitsRemaining;
            }
            function cardIndex(card) {
                const name = SpyCards.CardData.GlobalCardID[card.id];
                switch (card.rank()) {
                    case SpyCards.Rank.Boss:
                        return SpyCards.CardData.BossCardOrder[name];
                    case SpyCards.Rank.MiniBoss:
                        return SpyCards.CardData.MiniBossCardOrder[name];
                    case SpyCards.Rank.Attacker:
                        return SpyCards.CardData.AttackerCardOrder[name];
                    case SpyCards.Rank.Effect:
                        return 31 + SpyCards.CardData.EffectCardOrder[name];
                }
            }
            appendBits(0, 1);
            appendBits(cardIndex(deck[0]), 5);
            appendBits(cardIndex(deck[1]), 5);
            appendBits(cardIndex(deck[2]), 5);
            for (let i = 3; i < deck.length; i++) {
                appendBits(cardIndex(deck[i]), 6);
            }
            if (bitsRemaining >= 6) {
                appendBits(63, 6);
            }
            return new Uint8Array(buf);
        }
        function createDisplay(defs, deck) {
            const el = document.createElement("div");
            el.classList.add("deck-display");
            el.setAttribute("role", "list");
            for (let card of deck) {
                const cardEl = card.createEl(defs);
                cardEl.setAttribute("role", "listitem");
                el.appendChild(cardEl);
            }
            return el;
        }
        Decks.createDisplay = createDisplay;
        function saveNewDeck(defs, deck) {
            let decks = loadSaved(defs);
            decks.unshift(deck);
            let decksToSave = decks.map(function (toSave) {
                return toSave.map(function (card) {
                    return card.originalName();
                });
            });
            decksToSave = decksToSave.filter(function (v) {
                const normalized = v.slice(0).sort().toString();
                for (let otherDeck of decksToSave) {
                    if (otherDeck === v) {
                        return true;
                    }
                    const other = otherDeck.slice(0).sort().toString();
                    if (normalized === other) {
                        return false;
                    }
                }
                throw new Error("should be unreachable");
            });
            localStorage["spy-cards-decks-v0"] = JSON.stringify(decksToSave);
        }
        Decks.saveNewDeck = saveNewDeck;
        function removeSavedDeck(defs, deck) {
            let decks = loadSaved(defs).map(function (toSave) {
                return toSave.map(function (card) {
                    return card.originalName();
                });
            });
            const normalized = deck.map(function (card) {
                return card.originalName();
            }).sort().toString();
            decks = decks.filter(function (d) {
                const other = d.slice(0).sort().toString();
                return normalized !== other;
            });
            localStorage["spy-cards-decks-v0"] = JSON.stringify(decks);
        }
        Decks.removeSavedDeck = removeSavedDeck;
        function confirmDeck(status, defs, deck, question, yesText, noText, allowEdit) {
            status.textContent = question;
            const btn1 = document.createElement("button");
            btn1.classList.add("btn1");
            btn1.textContent = yesText;
            const btn2 = document.createElement("button");
            btn2.classList.add("btn2");
            btn2.textContent = noText;
            const deckDisplay = createDisplay(defs, deck);
            const done = new Promise(function (resolve) {
                function cleanUp() {
                    SpyCards.UI.remove(deckDisplay);
                    SpyCards.UI.remove(btn1);
                    SpyCards.UI.remove(btn2);
                }
                if (allowEdit) {
                    deckDisplay.classList.add("allow-edit", "no-flip");
                    deckDisplay.addEventListener("click", function (e) {
                        let target = e.target;
                        while (target && target.classList && !target.classList.contains("card")) {
                            target = target.parentNode;
                        }
                        if (!target || !target.classList) {
                            return;
                        }
                        let i = [].indexOf.call(deckDisplay.children, target);
                        if (i === -1) {
                            return;
                        }
                        cleanUp();
                        deck = deck.slice(0);
                        deck[i] = null;
                        resolve(editDeck(status, defs, deck).then((editedDeck) => confirmDeck(status, defs, editedDeck, question, yesText, noText, allowEdit)));
                    });
                    for (let card of [].slice.call(deckDisplay.children, 0)) {
                        card.setAttribute("role", "button");
                        card.setAttribute("aria-label", "Remove " + card.getAttribute("aria-label"));
                        card.tabIndex = 0;
                        card.addEventListener("keydown", function (e) {
                            if (SpyCards.disableKeyboard) {
                                return;
                            }
                            if (e.code === "Space" || e.code === "Enter") {
                                e.preventDefault();
                                cleanUp();
                                let i = [].indexOf.call(deckDisplay.children, this);
                                if (i === -1) {
                                    return;
                                }
                                cleanUp();
                                deck = deck.slice(0);
                                deck[i] = null;
                                resolve(editDeck(status, defs, deck).then((editedDeck) => confirmDeck(status, defs, editedDeck, question, yesText, noText, allowEdit)));
                            }
                        });
                    }
                }
                btn1.addEventListener("click", function (e) {
                    e.preventDefault();
                    cleanUp();
                    resolve(deck);
                });
                btn2.addEventListener("click", function (e) {
                    e.preventDefault();
                    cleanUp();
                    resolve(null);
                });
            });
            status.parentNode.appendChild(deckDisplay);
            status.parentNode.appendChild(btn1);
            status.parentNode.appendChild(btn2);
            return done;
        }
        Decks.confirmDeck = confirmDeck;
        async function editDeck(status, defs, deck) {
            deck = deck.slice(0);
            for (let i = 0; i < deck.length; i++) {
                // normalize deck (replace undefined with null)
                if (!deck[i]) {
                    deck[i] = null;
                }
            }
            const buildingDeckDisplay = document.createElement("div");
            buildingDeckDisplay.classList.add("building-deck-display", "no-flip");
            status.parentNode.appendChild(buildingDeckDisplay);
            for (let i = 0; i < deck.length; i++) {
                if (deck[i]) {
                    buildingDeckDisplay.appendChild(deck[i].createEl(defs));
                }
                else {
                    if (i < defs.rules.bossCards) {
                        buildingDeckDisplay.appendChild(SpyCards.createUnknownCardEl(2));
                    }
                    else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
                        buildingDeckDisplay.appendChild(SpyCards.createUnknownCardEl(1));
                    }
                    else {
                        buildingDeckDisplay.appendChild(SpyCards.createUnknownCardEl(0));
                    }
                }
            }
            let cardRemovedResolve;
            let cardRemoved = new Promise((resolve) => cardRemovedResolve = resolve);
            buildingDeckDisplay.addEventListener("click", function (e) {
                let target = e.target;
                while (target && target.classList && !target.classList.contains("card")) {
                    target = target.parentNode;
                }
                if (!target || !target.classList) {
                    return;
                }
                let i = [].indexOf.call(buildingDeckDisplay.children, target);
                if (deck[i]) {
                    deck[i] = null;
                    target.classList.add("flip-exception");
                    SpyCards.flipCard(target, true);
                    cardRemovedResolve(null);
                    cardRemoved = new Promise((resolve) => cardRemovedResolve = resolve);
                }
            });
            function addBuiltCard(card, i) {
                if (!card) {
                    return;
                }
                deck[i] = card;
                const el = card.createEl(defs);
                el.classList.add("flip-exception");
                SpyCards.flipCard(el, true);
                const pointer = buildingDeckDisplay.children[i];
                SpyCards.UI.replace(pointer, el);
                requestAnimationFrame(function () {
                    SpyCards.flipCard(el, false);
                    doSquish(el);
                    setTimeout(function () {
                        el.classList.remove("flip-exception");
                    }, 1000);
                });
            }
            if (deck.indexOf(null) === -1) {
                status.textContent = "Select a card to replace.";
                await cardRemoved;
            }
            function unique(defs, used) {
                return defs.filter((c) => used.indexOf(c) === -1);
            }
            while (true) {
                let i = deck.indexOf(null);
                if (i === -1) {
                    break;
                }
                if (i < defs.rules.bossCards) {
                    status.textContent = "Select a boss card.";
                    const card = await selectCard(status, defs, unique(defs.cardsByBack[2], deck.slice(0, defs.rules.bossCards)), cardRemoved, deck);
                    addBuiltCard(card, i);
                }
                else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
                    const chosenMiniBoss = deck.slice(defs.rules.bossCards).slice(0, defs.rules.miniBossCards);
                    status.textContent = chosenMiniBoss.some(Boolean) ? "Select another mini-boss card." : "Select a mini-boss card.";
                    const card = await selectCard(status, defs, unique(defs.cardsByBack[1], chosenMiniBoss), cardRemoved, deck);
                    addBuiltCard(card, i);
                }
                else {
                    let remaining = 0;
                    for (let c of deck) {
                        if (!c) {
                            remaining++;
                        }
                    }
                    if (remaining === 1) {
                        status.textContent = "Select 1 more card.";
                    }
                    else {
                        status.textContent = "Select " + remaining + " more cards.";
                    }
                    const card = await selectCard(status, defs, defs.cardsByBack[0], cardRemoved, deck);
                    addBuiltCard(card, i);
                }
            }
            SpyCards.UI.remove(buildingDeckDisplay);
            return deck;
        }
        Decks.editDeck = editDeck;
        async function confirmSelectDeck(status, defs, deck, startOver) {
            deck = await confirmDeck(status, defs, deck, "Is this deck okay?", spyCardsVersionSuffix === "-custom" ? "I'M SO SORRY" : "Yes", "Start Over", true);
            if (deck) {
                saveNewDeck(defs, deck);
                return deck;
            }
            return startOver();
        }
        Decks.confirmSelectDeck = confirmSelectDeck;
        async function selectDeck(status, defs, skipLoad, startOver) {
            startOver = startOver || selectDeck.bind(null, status, defs, skipLoad, startOver);
            if (!skipLoad) {
                let decks = loadSaved(defs, true);
                const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
                if (sg && sg.m && (sg.m & 1)) {
                    decks = [];
                    const npc = new SpyCards.AI.GenericNPC2();
                    for (let i = 0; i < 10; i++) {
                        decks.push(await npc.createDeck(defs));
                    }
                }
                if (decks.length) {
                    status.textContent = "Select a deck.";
                    const savedDeck = await selectSavedDeck(status, defs, decks);
                    if (savedDeck) {
                        return await confirmSelectDeck(status, defs, savedDeck, startOver);
                    }
                }
            }
            const builtDeck = await editDeck(status, defs, new Array(defs.rules.cardsPerDeck));
            return await confirmSelectDeck(status, defs, builtDeck, startOver);
        }
        Decks.selectDeck = selectDeck;
        async function selectSavedDeck(status, defs, decks, showDelete) {
            const selector = document.createElement("div");
            if (!status.id) {
                status.id = SpyCards.UI.uniqueID();
            }
            selector.setAttribute("aria-labelledby", status.id);
            selector.classList.add("deck-selector", "no-flip");
            selector.setAttribute("role", "listbox");
            if (showDelete) {
                selector.classList.add("has-delete-button");
            }
            status.parentNode.insertBefore(selector, status.nextSibling);
            return new Promise(function (resolve) {
                const createNewDeck = document.createElement("div");
                createNewDeck.id = SpyCards.UI.uniqueID();
                createNewDeck.classList.add("deck");
                createNewDeck.appendChild(SpyCards.createUnknownCardEl(2));
                createNewDeck.appendChild(SpyCards.createUnknownCardEl(1));
                createNewDeck.appendChild(SpyCards.createUnknownCardEl(1));
                for (let i = 0; i < 12; i++) {
                    createNewDeck.appendChild(SpyCards.createUnknownCardEl(0));
                }
                const createNewDeckHint = document.createElement("div");
                createNewDeckHint.classList.add("create-new-deck-hint");
                createNewDeckHint.textContent = "Create New Deck";
                createNewDeckHint.id = SpyCards.UI.uniqueID();
                createNewDeck.appendChild(createNewDeckHint);
                createNewDeck.setAttribute("aria-labelledby", createNewDeckHint.id);
                createNewDeck.addEventListener("click", function () {
                    SpyCards.UI.remove(selector);
                    resolve(null);
                });
                createNewDeck.setAttribute("role", "option");
                createNewDeck.setAttribute("aria-selected", "false");
                createNewDeck.tabIndex = 0;
                selector.appendChild(createNewDeck);
                createNewDeck.addEventListener("focus", () => {
                    createNewDeck.setAttribute("aria-selected", "true");
                    selector.setAttribute("aria-activedescendant", createNewDeck.id);
                });
                createNewDeck.addEventListener("blur", () => {
                    createNewDeck.setAttribute("aria-selected", "false");
                });
                createNewDeck.focus();
                let firstDeckEl;
                let lastDeckEl;
                createNewDeck.addEventListener("keydown", function (e) {
                    if (SpyCards.disableKeyboard) {
                        return;
                    }
                    if (e.code === "Space" || e.code === "Enter") {
                        e.preventDefault();
                        SpyCards.UI.remove(selector);
                        resolve(null);
                        return;
                    }
                    if (e.code === "Home") {
                        e.preventDefault();
                        return;
                    }
                    if (e.code === "End") {
                        e.preventDefault();
                        lastDeckEl.focus();
                        return;
                    }
                    if (e.code === "ArrowUp") {
                        e.preventDefault();
                        return;
                    }
                    if (e.code === "ArrowDown") {
                        e.preventDefault();
                        firstDeckEl.focus();
                        return;
                    }
                });
                const deckEls = [];
                for (let deck of decks) {
                    const deckEl = document.createElement("div");
                    deckEl.classList.add("deck");
                    deckEl.setAttribute("aria-label", "Deck: " + deck.map((c) => c.displayName()).join(", "));
                    for (let card of deck) {
                        const cardEl = card.createEl(defs);
                        cardEl.setAttribute("role", "presentation");
                        deckEl.appendChild(cardEl);
                    }
                    if (showDelete) {
                        const deleteButton = document.createElement("button");
                        deleteButton.classList.add("delete");
                        deleteButton.textContent = "Delete";
                        deleteButton.addEventListener("click", (function (deck) {
                            return function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                const savedStatus = status.textContent;
                                SpyCards.UI.remove(selector);
                                confirmDeck(status, defs, deck, "Really delete this deck?", "Yes", "Go Back").then(function (deck) {
                                    if (deck) {
                                        removeSavedDeck(defs, deck);
                                        SpyCards.UI.remove(deckEl);
                                    }
                                    status.textContent = savedStatus;
                                    status.parentNode.appendChild(selector);
                                });
                            };
                        })(deck));
                        deckEl.appendChild(deleteButton);
                    }
                    if (deck.some(c => c.effectiveTP() === Infinity || !defs.cardsByID[c.id])) {
                        deckEl.setAttribute("role", "option");
                        deckEl.setAttribute("aria-disabled", "true");
                        deckEl.classList.add("no-select");
                    }
                    else {
                        deckEls.push(deckEl);
                        if (!firstDeckEl) {
                            firstDeckEl = deckEl;
                        }
                        lastDeckEl = deckEl;
                        deckEl.id = SpyCards.UI.uniqueID();
                        deckEl.setAttribute("role", "option");
                        deckEl.setAttribute("aria-selected", "false");
                        deckEl.tabIndex = 0;
                        deckEl.addEventListener("focus", () => {
                            deckEl.setAttribute("aria-selected", "true");
                            selector.setAttribute("aria-activedescendant", deckEl.id);
                        });
                        deckEl.addEventListener("blur", () => {
                            deckEl.setAttribute("aria-selected", "false");
                        });
                        deckEl.addEventListener("click", (function (deck) {
                            return function () {
                                SpyCards.UI.remove(selector);
                                resolve(deck);
                            };
                        })(deck));
                        deckEl.addEventListener("keydown", (function (deck) {
                            return function (e) {
                                if (SpyCards.disableKeyboard) {
                                    return;
                                }
                                if (e.code === "Space" || e.code === "Enter") {
                                    e.preventDefault();
                                    SpyCards.UI.remove(selector);
                                    resolve(deck);
                                    return;
                                }
                                if (e.code === "Home") {
                                    e.preventDefault();
                                    createNewDeck.focus();
                                    return;
                                }
                                if (e.code === "End") {
                                    e.preventDefault();
                                    lastDeckEl.focus();
                                    return;
                                }
                                if (e.code === "ArrowUp") {
                                    e.preventDefault();
                                    const prev = deckEls[deckEls.indexOf(deckEl) - 1];
                                    if (prev) {
                                        prev.focus();
                                    }
                                    else {
                                        createNewDeck.focus();
                                    }
                                    return;
                                }
                                if (e.code === "ArrowDown") {
                                    e.preventDefault();
                                    const next = deckEls[deckEls.indexOf(deckEl) + 1];
                                    if (next) {
                                        next.focus();
                                    }
                                    return;
                                }
                            };
                        })(deck));
                    }
                    selector.appendChild(deckEl);
                }
            });
        }
        Decks.selectSavedDeck = selectSavedDeck;
        let lastSelCardPrev = null;
        async function selectCard(status, defs, choices, cancel, alreadySelected) {
            const selector = document.createElement("div");
            selector.classList.add("card-selector", "no-flip");
            selector.setAttribute("role", "listbox");
            const selected = new Promise(function (resolve) {
                disableDoSquish = true;
                choices.filter((card) => {
                    return !defs.banned[card.id] && !defs.unpickable[card.id];
                }).filter((card) => {
                    if (!defs.mode || !alreadySelected) {
                        return true;
                    }
                    for (let filter of defs.mode.getAll(SpyCards.GameModeFieldType.DeckLimitFilter)) {
                        if (!filter.match(card)) {
                            continue;
                        }
                        if (alreadySelected.filter((c) => c && filter.match(c)).length >= filter.count) {
                            return false;
                        }
                    }
                    return true;
                }).forEach(function (card, i, filteredChoices) {
                    if (i === 0) {
                        requestAnimationFrame(() => {
                            const nextEl = selector.children[filteredChoices.indexOf(lastSelCardPrev) + 1];
                            if (nextEl) {
                                nextEl.focus();
                            }
                            else if (selector.children.length) {
                                selector.children[selector.children.length - 1].focus();
                            }
                        });
                    }
                    const el = card.createEl(defs);
                    el.id = SpyCards.UI.uniqueID();
                    el.setAttribute("role", "option");
                    el.setAttribute("aria-selected", "false");
                    el.tabIndex = 0;
                    el.addEventListener("focus", () => {
                        el.setAttribute("aria-selected", "true");
                        selector.setAttribute("aria-activedescendant", el.id);
                    });
                    el.addEventListener("blur", () => el.setAttribute("aria-selected", "false"));
                    selector.appendChild(el);
                    el.addEventListener("click", () => {
                        resolve(card);
                        lastSelCardPrev = filteredChoices[i - 1];
                    });
                    el.addEventListener("keydown", (e) => {
                        if (SpyCards.disableKeyboard) {
                            return;
                        }
                        if (e.code === "ArrowDown" || e.code === "ArrowRight") {
                            e.preventDefault();
                            if (el.nextSibling) {
                                el.nextSibling.focus();
                            }
                            else {
                                selector.firstChild.focus();
                            }
                            return;
                        }
                        if (e.code === "ArrowUp" || e.code === "ArrowLeft") {
                            e.preventDefault();
                            if (el.previousSibling) {
                                el.previousSibling.focus();
                            }
                            else {
                                selector.lastChild.focus();
                            }
                            return;
                        }
                        if (e.code === "Space" || e.code === "Enter") {
                            e.preventDefault();
                            el.click();
                            return;
                        }
                    });
                });
                requestAnimationFrame(function () {
                    disableDoSquish = false;
                    doSquish(selector);
                });
            });
            status.parentNode.appendChild(selector);
            const promises = [selected];
            if (cancel) {
                promises.push(cancel);
            }
            const selectedCard = await Promise.race(promises);
            SpyCards.UI.remove(selector);
            return selectedCard;
        }
        Decks.selectCard = selectCard;
    })(Decks = SpyCards.Decks || (SpyCards.Decks = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var Effect;
    (function (Effect) {
        Effect.order = {
            // no effect during round
            ignored: [
                { type: SpyCards.EffectType.TP },
                { type: SpyCards.EffectType.FlavorText },
            ],
            // before stats are displayed
            pre: [
                { type: SpyCards.EffectType.CondApply, late: false },
                { type: SpyCards.EffectType.CondLimit },
                { type: SpyCards.EffectType.CondHP, late: false },
                { type: SpyCards.EffectType.Summon, negate: true },
                { type: SpyCards.EffectType.Summon, negate: false },
                { type: SpyCards.EffectType.CondCoin },
                { type: SpyCards.EffectType.CondCard, opponent: false, each: false },
                { type: SpyCards.EffectType.Heal },
            ],
            // main part of round
            main: [
                { type: SpyCards.EffectType.Stat, opponent: false },
                { type: SpyCards.EffectType.CondCard, opponent: true, each: false },
                { type: SpyCards.EffectType.CondCard, each: true },
                { type: SpyCards.EffectType.Empower },
                { type: SpyCards.EffectType.CondStat, late: false },
                { type: SpyCards.EffectType.Numb },
                { type: SpyCards.EffectType.Stat, opponent: true },
                { type: SpyCards.EffectType.CondStat, late: true },
            ],
            // after winner is computed
            post: [
                { type: SpyCards.EffectType.CondWinner },
                { type: SpyCards.EffectType.CondHP, late: true },
                { type: SpyCards.EffectType.CondApply, late: true },
            ],
            // triggered by other effects; ignore if still present
            discard: [
                { type: SpyCards.EffectType.CondOnNumb },
            ],
        };
        function filterTargets(ctx, card, effect, allowNumb = false) {
            return ctx.state.played[effect.opponent ? 2 - card.player : card.player - 1].filter((c) => {
                if ((!allowNumb && c.numb) || c.setup) {
                    return false;
                }
                if (ctx.defs.unfilter[c.card.id]) {
                    return false;
                }
                if (!effect.generic) {
                    return effect.card === c.card.id;
                }
                if (effect.rank !== SpyCards.Rank.None) {
                    const rank = c.card.rank();
                    if (effect.rank === SpyCards.Rank.Enemy) {
                        if (rank !== SpyCards.Rank.Attacker && rank !== SpyCards.Rank.Effect) {
                            return false;
                        }
                    }
                    else if (effect.rank !== rank) {
                        return false;
                    }
                }
                if (effect.tribe === SpyCards.Tribe.None) {
                    return true;
                }
                if (effect.tribe === SpyCards.Tribe.Custom) {
                    return c.card.tribes.some((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === effect.customTribe);
                }
                return c.card.tribes.some((t) => t.tribe === effect.tribe);
            });
        }
        async function dispatch(ctx, card, effect) {
            let ok = null;
            let stat;
            let diff;
            let player;
            let targets;
            switch (effect.type) {
                case SpyCards.EffectType.FlavorText:
                    // no effect
                    break;
                case SpyCards.EffectType.Stat:
                    stat = effect.defense ? "DEF" : "ATK";
                    diff = effect.negate ? -effect.amount : effect.amount;
                    player = (effect.opponent ? 3 - card.player : card.player);
                    await ctx.effect.modifyStat(card, effect.opponent || card.becomingNumb ? null : card, effect, player, stat, diff);
                    break;
                case SpyCards.EffectType.Empower:
                    stat = effect.defense ? "DEF" : "ATK";
                    diff = effect.negate ? -effect.amount : effect.amount;
                    player = (effect.opponent ? 3 - card.player : card.player);
                    targets = filterTargets(ctx, card, effect);
                    for (let c of targets) {
                        await ctx.effect.modifyStat(card, c, effect, player, stat, diff);
                    }
                    break;
                case SpyCards.EffectType.Summon:
                    const special = effect.defense ? effect.negate ? "both-invisible" : "invisible" :
                        effect.negate ? "replace" : null;
                    player = (effect.opponent ? 3 - card.player : card.player);
                    const toSummon = [];
                    for (let i = 0; i < effect.amount; i++) {
                        if (effect.generic) {
                            toSummon.push(await randomMatchingCard(ctx, effect));
                        }
                        else {
                            toSummon.push(ctx.defs.cardsByID[effect.card]);
                        }
                    }
                    if (effect.negate) {
                        card.setup = true;
                    }
                    const toSummonWait = toSummon.filter(Boolean).map((c, i) => ({ c, w: ctx.fx.multiple(i) }));
                    for (let { c, w } of toSummonWait) {
                        await w;
                        await ctx.effect.summonCard(player, card, c, special);
                    }
                    break;
                case SpyCards.EffectType.Heal:
                    player = (effect.opponent ? 3 - card.player : card.player);
                    diff = effect.negate ? -effect.amount : effect.amount;
                    if (effect.each) {
                        await ctx.effect.multiplyHealing(card, effect, player, diff, effect.generic ? null : !effect.defense);
                    }
                    else {
                        await ctx.effect.heal(card, effect, player, diff);
                    }
                    break;
                case SpyCards.EffectType.TP:
                    // no effect
                    break;
                case SpyCards.EffectType.Numb:
                    for (let i = 0; i < effect.amount; i++) {
                        let lowest = null;
                        let lowestATK = 0;
                        let lowestDEF = 0;
                        let lowestMin = 0;
                        let lowestMax = 0;
                        targets = filterTargets(ctx, card, effect);
                        for (let c of targets) {
                            if (c.numb || c.setup) {
                                continue;
                            }
                            const atk = c.modifiedATK || 0;
                            const def = c.modifiedDEF || 0;
                            const min = Math.min(atk, def);
                            const max = Math.max(atk, def);
                            if (lowest && atk === 0 && def === 0) {
                                continue;
                            }
                            if (!lowest || (lowestATK === 0 && lowestDEF === 0 && (atk !== 0 || def !== 0))) {
                                lowest = c;
                                lowestATK = atk;
                                lowestDEF = def;
                                lowestMin = min;
                                lowestMax = max;
                                continue;
                            }
                            if (max > lowestMax) {
                                continue;
                            }
                            if (max === lowestMax && min > lowestMin) {
                                continue;
                            }
                            if (max === lowestMax && lowestMax == lowestATK) {
                                continue;
                            }
                            lowest = c;
                            lowestATK = atk;
                            lowestDEF = def;
                            lowestMin = min;
                            lowestMax = max;
                        }
                        if (!lowest) {
                            break;
                        }
                        lowest.becomingNumb = true;
                        for (let i = 0; i < lowest.effects.length; i++) {
                            if (lowest.effects[i].type === SpyCards.EffectType.CondOnNumb) {
                                const trigger = lowest.effects.splice(i, 1)[0];
                                ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[trigger.result.type] + " for player " + lowest.player + " card " + lowest.card.displayName() + " (due to numb)");
                                await ctx.fx.beforeProcessEffect(lowest, trigger);
                                await Effect.dispatch(ctx, lowest, trigger.result);
                                await ctx.fx.afterProcessEffect(lowest, trigger);
                                i--;
                            }
                        }
                        lowest.numb = true;
                        await ctx.effect.recalculateStats((3 - card.player));
                        await ctx.fx.numb(card, lowest);
                    }
                    break;
                case SpyCards.EffectType.CondCard:
                    targets = filterTargets(ctx, card, effect, true);
                    ctx.state.gameLog.log("Matching cards (" + targets.length + "): " + targets.map((c) => c.card.displayName()).join(", "));
                    if (effect.each) {
                        for (let c of targets) {
                            await ctx.fx.assist(card, c);
                            await ctx.effect.applyResult(card, effect, effect.result);
                        }
                    }
                    else {
                        ok = effect.negate ? targets.length < effect.amount : targets.length >= effect.amount;
                    }
                    break;
                case SpyCards.EffectType.CondLimit:
                    const count = ctx.state.once[card.player - 1].reduce((n, e) => e === effect ? n + 1 : n, 0);
                    ok = effect.negate ?
                        count >= effect.amount :
                        count < effect.amount;
                    ctx.state.once[card.player - 1].push(effect);
                    break;
                case SpyCards.EffectType.CondWinner:
                    if (effect.negate) {
                        ok = effect.opponent ?
                            !!ctx.state.winner :
                            !ctx.state.winner;
                    }
                    else {
                        ok = effect.opponent ?
                            ctx.state.winner === 3 - card.player :
                            ctx.state.winner === card.player;
                    }
                    break;
                case SpyCards.EffectType.CondApply:
                    player = (effect.opponent ? 3 - card.player : card.player);
                    if (effect.late) {
                        if (effect.negate) {
                            let found = false;
                            for (let s of ctx.state.setup[player - 1]) {
                                if (s.sourceCard === card && s.originalDesc) {
                                    s.effects.push(effect.result);
                                    found = true;
                                    break;
                                }
                            }
                            if (found) {
                                break;
                            }
                        }
                        ctx.state.setup[player - 1].push({
                            card: card.card,
                            effects: [effect.result],
                            originalDesc: effect.negate,
                            sourceCard: card
                        });
                        await ctx.fx.setup(card);
                    }
                    else {
                        await ctx.effect.summonCard(player, card, card.card, effect.negate ? "setup-original" : "setup-effect", [effect.result]);
                    }
                    break;
                case SpyCards.EffectType.CondCoin:
                    const coins = [];
                    for (let i = 0; i < effect.amount; i++) {
                        coins.push(await ctx.scg.rand(true, 2) !== 0);
                    }
                    for (let i = 0; i < coins.length; i++) {
                        await ctx.fx.coin(card, effect.negate ? !coins[i] : coins[i], effect.defense, i, coins.length);
                    }
                    for (let flip of coins) {
                        ctx.state.gameLog.log("Player " + card.player + " card " + card.card.displayName() + " coin lands on " + (flip ? "heads" : "tails"));
                        if (flip) {
                            await ctx.effect.applyResult(card, effect, effect.result);
                        }
                        else if (effect.generic) {
                            await ctx.effect.applyResult(card, effect, effect.tailsResult);
                        }
                    }
                    break;
                case SpyCards.EffectType.CondHP:
                    const hp = ctx.state.hp[effect.opponent ? 2 - card.player : card.player - 1];
                    ok = effect.negate ? hp < effect.amount : hp >= effect.amount;
                    break;
                case SpyCards.EffectType.CondStat:
                    stat = effect.defense ? "DEF" : "ATK";
                    const value = ctx.state[stat][effect.opponent ? 2 - card.player : card.player - 1];
                    ok = effect.negate ? value < effect.amount : value >= effect.amount;
                    break;
                case SpyCards.EffectType.CondPriority:
                    ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[effect.result.type] + " at priority of " + SpyCards.EffectType[effect.amount]);
                    await Effect.dispatch(ctx, card, effect.result);
                    break;
                case SpyCards.EffectType.CondOnNumb:
                    // no effect when run from here
                    break;
                default:
                    throw new Error("unhandled effect type " + effect.type + " (" + SpyCards.EffectType[effect.type] + ")");
            }
            if (ok !== null) {
                ctx.state.gameLog.log("Condition is " + (ok ? "met." : "NOT met."));
            }
            if (ok) {
                await ctx.effect.applyResult(card, effect, effect.result);
            }
        }
        Effect.dispatch = dispatch;
        function isHidden(card, effect) {
            const firstHide = card.effects.findIndex((e) => e.type === SpyCards.EffectType.FlavorText && e.negate) + 1;
            if (firstHide <= 0) {
                return false;
            }
            for (let i = firstHide; i < card.effects.length; i++) {
                if (isInTree(card.effects[i], effect)) {
                    return true;
                }
            }
            return false;
        }
        Effect.isHidden = isHidden;
        function isInTree(tree, effect) {
            if (tree === effect) {
                return true;
            }
            if (tree.result && isInTree(tree.result, effect)) {
                return true;
            }
            if (tree.tailsResult && isInTree(tree.tailsResult, effect)) {
                return true;
            }
            return false;
        }
        async function randomMatchingCard(ctx, effect) {
            let choices;
            if (effect.tribe === SpyCards.Tribe.None && effect.negate) {
                ctx.state.gameLog.log("Selecting a random " + (effect.rank === SpyCards.Rank.None ? "" : SpyCards.rankName(effect.rank)) + " card that does not have a Carmina-style summon...");
                choices = ctx.defs.nonRandom[effect.rank].filter((c) => !ctx.defs.banned[c.id]);
            }
            else {
                ctx.state.gameLog.log("Selecting a random card with filters: Rank: " + SpyCards.rankName(effect.rank) + " Tribe: " + SpyCards.tribeName(effect.tribe, effect.customTribe));
                choices = ctx.defs.getAvailableCards(effect.rank, effect.tribe, effect.customTribe);
            }
            if (!choices.length) {
                ctx.state.gameLog.log("(no cards are available for this filter)");
                return null;
            }
            const index = await ctx.scg.rand(true, choices.length);
            ctx.state.gameLog.log("Selected card " + (index + 1) + " of " + choices.length + ": " + choices[index].displayName());
            return choices[index];
        }
    })(Effect = SpyCards.Effect || (SpyCards.Effect = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
addEventListener("error", function (e) {
    if (SpyCards.disableErrorHandler) {
        return;
    }
    if (e.error && e.error.message === "Go program has already exited") {
        // don't mask real error
        return;
    }
    if (!e.error && e.target instanceof HTMLScriptElement) {
        const scriptName = e.target.src.replace(/^[^\?]*\/|\?.*$/g, "");
        console.debug("TODO: error screen for script that failed to load:", scriptName);
    }
    else if (e.error) {
        errorHandler(e.error);
    }
    else if (e.message) {
        errorHandler(new Error(e.message + " at " + e.filename + ":" + e.lineno + ":" + e.colno));
    }
    else {
        console.error("unhandled type of ErrorEvent", e);
        debugger;
    }
}, true);
var SpyCards;
(function (SpyCards) {
    // prevent keyboard inputs from being eaten
    SpyCards.disableKeyboard = false;
    SpyCards.disableErrorHandler = false;
    SpyCards.serviceWorkerVersion = "";
    fetch("/service-worker-status").then((r) => r.text()).then((v) => SpyCards.serviceWorkerVersion = v);
})(SpyCards || (SpyCards = {}));
function errorHandler(ex, state) {
    console.error("Error handler triggered:", ex, ex.goStack || ex.stack);
    debugger;
    if (document.documentElement.classList.contains("fatal-error")) {
        return;
    }
    SpyCards.disableKeyboard = true;
    if (SpyCards.Audio) {
        SpyCards.Audio.stopMusic();
    }
    const form = document.createElement("form");
    form.classList.add("error-report");
    const h1 = document.createElement("h1");
    h1.textContent = "Uh oh!";
    form.appendChild(h1);
    const flavor = document.createElement("p");
    flavor.classList.add("flavor");
    flavor.textContent = "It looks like Spy Cards Online crashed due to a bug! But that's impossible because Spy Cards Online contains no... oh.";
    form.appendChild(flavor);
    const message = document.createElement("p");
    message.classList.add("message");
    if (/^PANIC:/.test(ex.message)) {
        message.textContent = ex.message;
    }
    else {
        message.textContent = "Error: " + ex.message;
    }
    form.appendChild(message);
    const h2 = document.createElement("h2");
    h2.textContent = "Report Error";
    form.appendChild(h2);
    const commentField = document.createElement("textarea");
    commentField.name = "u";
    const commentLabel = document.createElement("label");
    commentLabel.textContent = "What happened right before the crash?";
    commentLabel.appendChild(commentField);
    form.appendChild(commentLabel);
    const submit = document.createElement("button");
    submit.type = "submit";
    submit.textContent = "Submit Report";
    form.appendChild(submit);
    const disclaimer = document.createElement("p");
    disclaimer.classList.add("disclaimer");
    disclaimer.textContent = "Submitting this form will send the following data:";
    form.appendChild(disclaimer);
    const dataList = document.createElement("ul");
    dataList.classList.add("disclaimer");
    function addItem(text) {
        const item = document.createElement("li");
        item.textContent = text;
        dataList.appendChild(item);
    }
    addItem("The version of Spy Cards Online (" + spyCardsVersion + ")");
    addItem("The error message (" + ex.message + ")");
    addItem("The location in the code where the error occurred");
    if (state) {
        addItem("The state of the match just before the error");
    }
    if (location.hash && location.hash.length > 1) {
        addItem("Custom card definitions (from the address bar)");
    }
    addItem("Your comment entered above (if any)");
    form.appendChild(dataList);
    form.addEventListener("submit", function (e) {
        e.preventDefault();
        submit.disabled = true;
        submit.textContent = "Sending...";
        const data = new FormData(form);
        data.append("v", spyCardsVersion);
        data.append("v2", SpyCards.serviceWorkerVersion);
        data.append("m", ex.message);
        data.append("t", ex.goStack || ex.stack);
        data.append("s", JSON.stringify(state || null));
        data.append("c", location.hash ? location.hash.substr(1) : "");
        function closeForm(message) {
            form.textContent = "";
            const thanks = document.createElement("p");
            thanks.classList.add("thanks");
            thanks.textContent = message;
            form.appendChild(thanks);
        }
        const xhr = new XMLHttpRequest();
        xhr.open("POST", issue_report_handler);
        xhr.addEventListener("load", function () {
            if (xhr.status === 202) {
                closeForm("Report submitted. Thanks for helping to make Spy Cards Online a little less buggy. Or more buggy.");
                return;
            }
            closeForm("Form submission failed due to an error. Oh, the irony! (remote code " + xhr.status + ")");
        });
        xhr.addEventListener("error", function () {
            closeForm("Form submission failed due to an error. Oh, the irony! (local code " + xhr.status + ")");
        });
        xhr.send(data);
    });
    document.documentElement.classList.add("fatal-error");
    document.body.appendChild(form);
    document.scrollingElement.scrollTop = 0;
}
if ("serviceWorker" in navigator) {
    if (navigator.serviceWorker.controller) {
        navigator.serviceWorker.addEventListener("controllerchange", () => {
            const updateDialog = document.createElement("div");
            updateDialog.classList.add("update-available");
            updateDialog.setAttribute("role", "alertdialog");
            const updateTitle = document.createElement("h1");
            updateTitle.id = "service-worker-update-available-title";
            updateDialog.setAttribute("aria-labelledby", updateTitle.id);
            updateTitle.textContent = "Update Available";
            updateDialog.appendChild(updateTitle);
            const updateMessage = document.createElement("p");
            updateMessage.id = "service-worker-update-available-message";
            updateDialog.setAttribute("aria-describedby", updateMessage.id);
            updateMessage.textContent = "An update to Spy Cards Online is available.";
            updateDialog.appendChild(updateMessage);
            const updateAccept = document.createElement("button");
            updateAccept.textContent = "Apply Now";
            updateAccept.addEventListener("click", (e) => {
                e.preventDefault();
                location.reload();
            });
            updateDialog.appendChild(updateAccept);
            const updateDecline = document.createElement("button");
            updateDecline.textContent = "Ignore";
            updateDecline.addEventListener("click", (e) => {
                e.preventDefault();
                if (updateDialog.parentNode) {
                    updateDialog.parentNode.removeChild(updateDialog);
                }
            });
            updateDialog.appendChild(updateDecline);
            document.querySelectorAll(".update-available").forEach((el) => {
                if (el.parentNode) {
                    el.parentNode.removeChild(el);
                }
            });
            const showUpdateDialog = () => {
                document.body.appendChild(updateDialog);
                updateAccept.focus();
            };
            if (document.documentElement.classList.contains("in-match")) {
                const matchWaiter = setInterval(() => {
                    if (document.documentElement.classList.contains("in-match")) {
                        return;
                    }
                    clearInterval(matchWaiter);
                    showUpdateDialog();
                }, 1000);
            }
            else {
                showUpdateDialog();
            }
        });
    }
    const hadController = !!navigator.serviceWorker.controller;
    navigator.serviceWorker.register("/service-worker.js").then((reg) => {
        reg.addEventListener("updatefound", (e) => {
            console.log("serviceworker update available");
            if (!hadController) {
                // we didn't load via a serviceworker, so we don't need to tell the user
                return;
            }
            const updateDialog = document.createElement("div");
            updateDialog.classList.add("update-available");
            updateDialog.setAttribute("role", "alert");
            const updateTitle = document.createElement("h1");
            updateTitle.id = "service-worker-update-found-title";
            updateDialog.setAttribute("aria-labelledby", updateTitle.id);
            updateTitle.textContent = "Update Found";
            updateDialog.appendChild(updateTitle);
            const updateMessage = document.createElement("p");
            updateMessage.id = "service-worker-update-found-message";
            updateDialog.setAttribute("aria-describedby", updateMessage.id);
            updateMessage.textContent = "An update to Spy Cards Online is being downloaded…";
            updateDialog.appendChild(updateMessage);
            document.body.appendChild(updateDialog);
        });
        let sw;
        if (reg.installing) {
            sw = reg.installing;
        }
        else if (reg.waiting) {
            sw = reg.waiting;
        }
        else if (reg.active) {
            sw = reg.active;
        }
        if (sw) {
            console.log("serviceworker initial state", sw.state);
            sw.addEventListener("statechange", (e) => {
                console.log("serviceworker state change", sw.state);
            });
        }
    });
    navigator.serviceWorker.addEventListener("message", (e) => {
        switch (e.data.type) {
            case "settings-changed":
                window.dispatchEvent(new Event("spy-cards-settings-changed"));
                break;
            default:
                debugger;
                break;
        }
    });
    navigator.serviceWorker.startMessages();
}
"use strict";
var SpyCards;
(function (SpyCards) {
    var Fx;
    (function (Fx) {
        Fx.tpEl = document.createElement("span");
        Fx.tpEl.classList.add("tp-ui");
        Fx.tpEl.setAttribute("aria-label", "Your Teamwork Points");
        Fx.myHPEl = document.createElement("span");
        Fx.myHPEl.classList.add("hp-ui", "my-hp");
        Fx.myHPEl.setAttribute("aria-label", "Your Health");
        Fx.theirHPEl = document.createElement("span");
        Fx.theirHPEl.classList.add("hp-ui", "their-hp");
        Fx.theirHPEl.setAttribute("aria-label", "Their Health");
        Fx.myATK = document.createElement("div");
        Fx.myATK.classList.add("stat-atk", "my-atk");
        Fx.myATK.setAttribute("aria-label", "Your Attack Stat");
        Fx.theirATK = document.createElement("div");
        Fx.theirATK.classList.add("stat-atk", "their-atk");
        Fx.theirATK.setAttribute("aria-label", "Their Attack Stat");
        Fx.myDEF = document.createElement("div");
        Fx.myDEF.classList.add("stat-def", "my-def");
        Fx.myDEF.setAttribute("aria-label", "Your Defense Stat");
        Fx.theirDEF = document.createElement("div");
        Fx.theirDEF.classList.add("stat-def", "their-def");
        Fx.theirDEF.setAttribute("aria-label", "Their Defense Stat");
        Fx.myField = document.createElement("div");
        Fx.myField.classList.add("my-field");
        Fx.myField.setAttribute("aria-label", "Your Played Cards");
        Fx.theirField = document.createElement("div");
        Fx.theirField.classList.add("their-field");
        Fx.theirField.setAttribute("aria-label", "Their Played Cards");
        Fx.myFieldSetup = document.createElement("div");
        Fx.myFieldSetup.classList.add("my-field", "setup");
        Fx.myFieldSetup.setAttribute("aria-label", "Your Setup Cards");
        Fx.theirFieldSetup = document.createElement("div");
        Fx.theirFieldSetup.classList.add("their-field", "setup");
        Fx.theirFieldSetup.setAttribute("aria-label", "Their Setup Cards");
        if (window.requestIdleCallback) {
            requestIdleCallback(() => {
                // start loading the room RPC sooner than we need it.
                SpyCards.Native.roomRPC();
            });
        }
        else {
            setTimeout(() => SpyCards.Native.roomRPC(), 1);
        }
        let fieldEl;
        let realFxSleep = function (ms) {
            const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
            if (sg && sg.m && (sg.m & 4)) {
                realFxSleep = () => Promise.resolve();
            }
            else {
                realFxSleep = (ms) => new Promise((resolve) => setTimeout(() => resolve(), ms));
            }
            return realFxSleep(ms);
        };
        let fxSleep = realFxSleep;
        let lastFlavor = null;
        function findEffectRecursive(path, effects, effect) {
            for (let i = 0; i < effects.length; i++) {
                path.push(":nth-child(" + (i + 1) + ")");
                if (effects[i] === effect) {
                    return true;
                }
                if (effects[i].type === SpyCards.EffectType.CondCoin) {
                    path.push(".card-coin-heads");
                    if (findEffectRecursive(path, [effects[i].result], effect)) {
                        return true;
                    }
                    path.pop();
                    path.push(".card-coin-tails");
                    if (findEffectRecursive(path, effects[i].tailsResult ? [effects[i].tailsResult] : [], effect)) {
                        return true;
                    }
                    path.pop();
                }
                else {
                    if (effects[i].result && findEffectRecursive(path, [effects[i].result], effect)) {
                        return true;
                    }
                }
                path.pop();
            }
            return false;
        }
        function findEffect(desc, effects, effect) {
            const path = [":scope"];
            if (!findEffectRecursive(path, effects, effect)) {
                throw new Error("cannot find effect");
            }
            const selector = path.join(" > ");
            const el = desc.querySelector(selector);
            if (!el) {
                throw new Error("cannot find effect for selector " + selector);
            }
            return el;
        }
        function getCallbacks(ctx, audience) {
            fieldEl = [
                ctx.player === 1 ? Fx.myField : Fx.theirField,
                ctx.player === 2 ? Fx.myField : Fx.theirField
            ];
            const setupFieldEl = [
                ctx.player === 1 ? Fx.myFieldSetup : Fx.theirFieldSetup,
                ctx.player === 2 ? Fx.myFieldSetup : Fx.theirFieldSetup
            ];
            const hpEl = [
                ctx.player === 1 ? Fx.myHPEl : Fx.theirHPEl,
                ctx.player === 2 ? Fx.myHPEl : Fx.theirHPEl
            ];
            let hpDelay = Promise.resolve();
            return {
                async beforeRound() {
                    Fx.tpEl.classList.add("hidden");
                    for (let player = 0; player < 2; player++) {
                        fieldEl[player].classList.remove("won-round", "lost-round");
                        fieldEl[player].classList.add("offscreen");
                        SpyCards.UI.clear(fieldEl[player]);
                        SpyCards.UI.clear(setupFieldEl[player]);
                        for (let card of ctx.state.played[player]) {
                            if (card.setup) {
                                if (!card.setupPlaceholder) {
                                    card.setupPlaceholder = card.el.cloneNode(true);
                                }
                                setupFieldEl[player].appendChild(card.setupPlaceholder);
                                card.el.style.visibility = "hidden";
                            }
                            else if (setupFieldEl[player].firstChild) {
                                const placeholder = document.createElement("div");
                                placeholder.classList.add("card");
                                placeholder.style.visibility = "hidden";
                                setupFieldEl[player].appendChild(placeholder);
                            }
                            fieldEl[player].appendChild(card.el);
                        }
                        document.body.appendChild(setupFieldEl[player]);
                        document.body.appendChild(fieldEl[player]);
                        doSquish(setupFieldEl[player]);
                        doSquish(fieldEl[player]);
                    }
                    await fxSleep(125);
                    Fx.myField.classList.remove("offscreen");
                    Fx.theirField.classList.remove("offscreen");
                    await fxSleep(250);
                    ctx.ui.form.classList.add("hidden");
                    for (let player = 0; player < 2; player++) {
                        for (let card of ctx.state.played[player]) {
                            if (card.setupPlaceholder) {
                                SpyCards.UI.remove(card.setupPlaceholder);
                                card.el.style.visibility = "";
                            }
                        }
                    }
                    lastFlavor = null;
                },
                async afterRound() {
                    await fxSleep(250);
                    Fx.myField.classList.add(ctx.state.winner === ctx.player ? "won-round" : "lost-round");
                    Fx.theirField.classList.add(ctx.state.winner === 3 - ctx.player ? "won-round" : "lost-round");
                    for (let player = 0; player < 2; player++) {
                        SpyCards.UI.remove(setupFieldEl[player]);
                        setupFieldEl[player].classList.add("offscreen");
                        SpyCards.UI.clear(setupFieldEl[player]);
                        document.body.appendChild(setupFieldEl[player]);
                        for (let card of ctx.state.played[player]) {
                            if (card.setup) {
                                card.el.style.visibility = "";
                            }
                        }
                        for (let setup of ctx.state.setup[player]) {
                            const el = setup.card.createEl(ctx.defs);
                            el.classList.add("setup");
                            if (!setup.originalDesc) {
                                el.querySelectorAll(".card-desc > *").forEach((e) => {
                                    SpyCards.UI.remove(e);
                                });
                                el.querySelector(".card-desc").appendChild(setup.effects[0].createEl(ctx.defs, setup.card));
                            }
                            setupFieldEl[player].insertBefore(el, setupFieldEl[player].firstChild);
                        }
                    }
                    await fxSleep(500);
                    for (let player = 0; player < 2; player++) {
                        setupFieldEl[player].classList.remove("offscreen");
                    }
                    await fxSleep(250);
                    removeCoins();
                    SpyCards.UI.remove(Fx.myField);
                    SpyCards.UI.remove(Fx.theirField);
                    SpyCards.UI.remove(Fx.myATK);
                    SpyCards.UI.remove(Fx.theirATK);
                    SpyCards.UI.remove(Fx.myDEF);
                    SpyCards.UI.remove(Fx.theirDEF);
                    Fx.tpEl.classList.remove("hidden");
                    ctx.ui.form.classList.remove("hidden");
                },
                async showStats() {
                    Fx.myATK.classList.remove("stat-ready");
                    Fx.theirATK.classList.remove("stat-ready");
                    Fx.myDEF.classList.remove("offscreen");
                    Fx.theirDEF.classList.remove("offscreen");
                    Fx.myATK.textContent = "0";
                    Fx.theirATK.textContent = "0";
                    Fx.myDEF.textContent = "0";
                    Fx.theirDEF.textContent = "0";
                    document.body.appendChild(Fx.myATK);
                    document.body.appendChild(Fx.theirATK);
                    document.body.appendChild(Fx.myDEF);
                    document.body.appendChild(Fx.theirDEF);
                },
                async beforeComputeWinner() {
                    if (ctx.state.played[0].length || ctx.state.played[1].length) {
                        SpyCards.Audio.Sounds.Toss11.play();
                    }
                    await fxSleep(500);
                    Fx.myDEF.classList.add("offscreen");
                    Fx.theirDEF.classList.add("offscreen");
                    await fxSleep(250);
                },
                async afterComputeWinner() {
                    Fx.myATK.classList.add("stat-ready");
                    Fx.theirATK.classList.add("stat-ready");
                    await fxSleep(750);
                    if (ctx.state.winner) {
                        if (ctx.state.players[2 - ctx.state.winner]) {
                            ctx.state.players[2 - ctx.state.winner].becomeAngry();
                        }
                    }
                    if (ctx.state.winner) {
                        hpDelay = fxSleep(800);
                    }
                    else {
                        hpDelay = Promise.resolve();
                    }
                    if (ctx.state.winner || ctx.state.hp[0] < ctx.defs.rules.maxHP / 2 || ctx.state.hp[1] < ctx.defs.rules.maxHP / 2) {
                        for (let a of audience) {
                            a.startCheer();
                        }
                    }
                    if (ctx.state.winner === ctx.player) {
                        SpyCards.Audio.Sounds.CrowdCheer2.play();
                        SpyCards.Audio.Sounds.Damage0.play(0.5);
                    }
                    else if (ctx.state.winner) {
                        SpyCards.Audio.Sounds.CrowdGasp.play();
                        SpyCards.Audio.Sounds.Death3.play(0.5);
                    }
                    else {
                        SpyCards.Audio.Sounds.Fail.play();
                    }
                },
                async drawCard(player, card) {
                },
                async hpChange(player, amountBefore, amountChanged) {
                    hpDelay.then(async function () {
                        hpEl[player - 1].textContent = String(Math.min(amountBefore + amountChanged, ctx.defs.rules.maxHP));
                        if (amountChanged > 0) {
                            hpEl[player - 1].classList.add("healed");
                            SpyCards.Audio.Sounds.Heal.play();
                            await fxSleep(10);
                            hpEl[player - 1].classList.remove("healed");
                            await fxSleep(90);
                        }
                        else if (amountChanged !== 0) {
                            hpEl[player - 1].classList.add("took-hit");
                            await fxSleep(10);
                            hpEl[player - 1].classList.remove("took-hit");
                            await fxSleep(90);
                        }
                    });
                    await fxSleep(100);
                },
                async statChange(player, stat, amountBefore, amountChanged, amountAfter) {
                    let newAmount;
                    if (amountAfter < 0) {
                        newAmount = "0";
                    }
                    else if (isFinite(amountAfter)) {
                        newAmount = String(amountAfter);
                    }
                    else {
                        newAmount = "∞";
                    }
                    if (player == ctx.player) {
                        if (stat === "ATK") {
                            Fx.myATK.textContent = newAmount;
                        }
                        else {
                            Fx.myDEF.textContent = newAmount;
                        }
                    }
                    else {
                        if (stat === "ATK") {
                            Fx.theirATK.textContent = newAmount;
                        }
                        else {
                            Fx.theirDEF.textContent = newAmount;
                        }
                    }
                    await fxSleep(100);
                },
                async cardSummoned(summoner, summoned, special, effects) {
                    summoned.el = summoned.card.createEl(ctx.defs);
                    if (special === "setup-original" || special === "setup-effect") {
                        summoned.setup = true;
                        summoned.el.classList.add("setup");
                        if (special === "setup-effect") {
                            summoned.el.querySelectorAll(".card-desc > *").forEach((e) => {
                                SpyCards.UI.remove(e);
                            });
                            summoned.el.querySelector(".card-desc").appendChild(effects[0].createEl(ctx.defs, summoned.card));
                        }
                        else {
                            summoned.originalDesc = true;
                        }
                    }
                    const oldParent = summoner.el.parentNode;
                    const oldNext = summoner.el.nextSibling;
                    if (special === "replace" && !oldParent) {
                        special = null;
                    }
                    if (special === "invisible" || special === "both-invisible") {
                        if (special === "both-invisible") {
                            summoned.el.classList.add("active");
                            SpyCards.flipCard(summoned.el, true);
                            summoner.el.classList.add("fast-flip");
                            SpyCards.flipCard(summoner.el, true);
                            await fxSleep(250);
                            SpyCards.UI.remove(summoner.el);
                        }
                        summoned.invisible = true;
                    }
                    else if (special === "replace") {
                        summoned.el.classList.add("active");
                        SpyCards.flipCard(summoned.el, true);
                        summoner.el.classList.add("fast-flip");
                        SpyCards.flipCard(summoner.el, true);
                        await fxSleep(250);
                        oldParent.insertBefore(summoned.el, oldNext);
                        SpyCards.UI.remove(summoner.el);
                        await fxSleep(10);
                        SpyCards.flipCard(summoned.el, false);
                    }
                    else {
                        summoned.el.classList.add("offscreen");
                        if (summoner.el.parentNode) {
                            summoner.el.parentNode.insertBefore(summoned.el, summoner.el.nextSibling);
                        }
                        else {
                            fieldEl[summoned.player - 1].appendChild(summoned.el);
                        }
                        await fxSleep(10);
                        summoned.el.classList.remove("offscreen");
                    }
                    if (summoner.player !== summoned.player && !summoned.invisible) {
                        fieldEl[summoned.player - 1].appendChild(summoned.el);
                    }
                    doSquish(summoned.el);
                },
                async beforeAddEffects(card, effects, delayProcessing) {
                },
                async addEffect(card, effect, alreadyProcessed, fromSummon) {
                }
            };
        }
        Fx.getCallbacks = getCallbacks;
        const positionHolders = [];
        const highlightedEffect = [];
        const coinsToCleanUp = [];
        Fx.fx = {
            async coin(card, heads, isStat, index, total) {
                let coinsEl = card.el.querySelector(".coins");
                if (!coinsEl) {
                    coinsEl = document.createElement("div");
                    coinsEl.classList.add("coins");
                    coinsToCleanUp.push(coinsEl);
                    card.el.appendChild(coinsEl);
                }
                const coinEl = document.createElement("span");
                coinEl.classList.add("coin", heads ? "heads" : "tails", "unknown");
                if (isStat) {
                    coinEl.classList.add("atk-def");
                }
                coinEl.style.setProperty("--rand", String(Math.random()));
                coinsEl.appendChild(coinEl);
                await fxSleep(index === 0 ? 250 : 50);
                SpyCards.Audio.Sounds.Coin.play();
                coinEl.classList.remove("unknown");
                if (!isStat) {
                    if (heads) {
                        SpyCards.Audio.Sounds.AtkSuccess.play(0.75);
                    }
                    else {
                        SpyCards.Audio.Sounds.AtkFail.play(0.75);
                    }
                }
                if (index === total - 1) {
                    await fxSleep(750);
                }
            },
            async numb(card, target) {
                SpyCards.flipCard(target.el, true);
                SpyCards.Audio.Sounds.Lazer.play();
                await fxSleep(250);
            },
            async setup(card) {
                SpyCards.Audio.Sounds.CardSound2.play();
            },
            async multiple(index) {
                await fxSleep(150 * index);
            },
            async assist() {
            },
            async beforeProcessEffect(card, effect, nested) {
                if (card.invisible) {
                    if (!nested) {
                        SpyCards.Audio.ignoreAudio++;
                        fxSleep = () => Promise.resolve();
                    }
                    return;
                }
                let playSound = true;
                const isHidden = (!card.setup || card.originalDesc) && SpyCards.Effect.isHidden(card.card, effect);
                if (isHidden) {
                    const flavor = card.card.effects.find((e) => e.type === SpyCards.EffectType.FlavorText && e.negate);
                    if (lastFlavor !== flavor) {
                        lastFlavor = flavor;
                    }
                    else {
                        playSound = false;
                    }
                }
                else if (lastFlavor) {
                    lastFlavor = null;
                    await fxSleep(250);
                }
                const el = card.el;
                if (!el.parentNode) {
                    // removed by an effect like summon-rank; re-add as pseudo-setup card.
                    el.classList.add("setup");
                    SpyCards.flipCard(el, false);
                    el.classList.remove("fast-flip");
                    fieldEl[card.player - 1].appendChild(el);
                }
                const effectEl = card.setup && !card.originalDesc ? el.querySelector(".card-desc > *") : findEffect(el.querySelector(".card-desc"), card.card.effects, effect);
                effectEl.classList.add("highlight");
                highlightedEffect.push(effectEl);
                if (!nested) {
                    const positionHolderEl = document.createElement("div");
                    positionHolderEl.classList.add("card");
                    positionHolderEl.style.visibility = "hidden";
                    positionHolders.push(positionHolderEl);
                    el.parentNode.insertBefore(positionHolderEl, el);
                    el.classList.add("active");
                    if (playSound) {
                        SpyCards.Audio.Sounds.CardSound2.play();
                    }
                }
            },
            async afterProcessEffect(card, effect, nested) {
                if (card.invisible) {
                    if (!nested) {
                        SpyCards.Audio.ignoreAudio--;
                        fxSleep = realFxSleep;
                    }
                    return;
                }
                if (!nested) {
                    if (!lastFlavor) {
                        await fxSleep(200);
                    }
                    const el = card.el;
                    const positionHolderEl = positionHolders.pop();
                    if (!el.parentNode && positionHolderEl.nextSibling) {
                        // handle Carmina's replacement
                        positionHolderEl.nextSibling.classList.remove("active");
                    }
                    SpyCards.UI.remove(positionHolderEl);
                    el.classList.remove("active");
                }
                const effectEl = highlightedEffect.pop();
                if (!lastFlavor) {
                    await fxSleep(50);
                }
                effectEl.classList.remove("highlight");
            },
        };
        function removeCoins() {
            for (let el of coinsToCleanUp) {
                SpyCards.UI.remove(el);
            }
            coinsToCleanUp.length = 0;
        }
        Fx.removeCoins = removeCoins;
    })(Fx = SpyCards.Fx || (SpyCards.Fx = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    async function selectCharacter(ctx) {
        if (ctx.matchData.selectedCharacter) {
            return ctx.matchData.selectedCharacter;
        }
        const roomRPC = await SpyCards.Native.roomRPC();
        if (roomRPC == null) {
            return SpyCards.loadSettings().character || "tanjerin";
        }
        const mine = await new Promise((resolve) => roomRPC.newMine(resolve));
        const selected = new Promise((resolve) => roomRPC.onMineSelected(resolve));
        roomRPC.setRoom(mine);
        return await Promise.race([
            async function () {
                const c = await selected;
                await sleep(500);
                return c;
            }(),
            async function () {
                await ctx.ui.fatalErrorPromise;
                roomRPC.exit();
                return null;
            }()
        ]);
    }
    async function runSpyCardsGame(ctx) {
        try {
            ctx.net.cgc.sendVersion(spyCardsVersion + spyCardsVersionVariableSuffix);
            SpyCards.UI.remove(ctx.ui.h1);
            ctx.ui.status.textContent = "Connected to opponent. Setting up card game...";
            await ctx.net.cgc.initSecure(ctx.player);
            SpyCards.Audio.Songs.Inside2.play();
            const stage = new SpyCards.TheRoom.Stage();
            let npc = null;
            try {
                const params = new URLSearchParams(location.search);
                if (params.has("npc")) {
                    npc = SpyCards.AI.getNPC(params.get("npc"));
                }
            }
            catch (ex) {
                debugger;
            }
            ctx.ui.form.style.display = "none";
            const alreadySelected = !!ctx.matchData.selectedCharacter;
            const selectedCharacter = alreadySelected ? ctx.matchData.selectedCharacter :
                (npc ? (await npc.pickPlayer()).name : await selectCharacter(ctx));
            ctx.ui.form.style.display = "";
            if (!selectedCharacter) {
                // we had a fatal error before a character was selected
                return;
            }
            ctx.matchData.selectedCharacter = selectedCharacter;
            await stage.init();
            ctx.ui.fatalErrorPromise.then(() => {
                // make sure the stage goes away if we have a fatal error
                return stage.deinit();
            });
            stage.setFlipped(ctx.player === 2);
            const audience = await stage.createAudience(ctx.scg.seed, ctx.matchData.rematchCount);
            const myPlayer = new SpyCards.TheRoom.Player(ctx.player, selectedCharacter);
            stage.setPlayer(myPlayer);
            if (!alreadySelected) {
                ctx.net.cgc.sendCosmeticData({
                    character: selectedCharacter
                });
            }
            ctx.state = new SpyCards.MatchState(ctx.defs, ctx.npcCtx ? ctx.npcCtx.state : null);
            ctx.state.roundSetupComplete = false;
            ctx.state.roundSetupDelayed = [];
            ctx.state.players[ctx.player - 1] = myPlayer;
            ctx.net.cgc.recvCosmeticData.then((cosmetic) => {
                const p = new SpyCards.TheRoom.Player((3 - ctx.player), cosmetic.character);
                ctx.state.players[2 - ctx.player] = p;
                stage.setPlayer(p);
            });
            ctx.ui.status.textContent = "Waiting for opponent to send Spoiler Guard data...";
            const remoteSpoilerGuard = await ctx.net.cgc.recvSpoilerGuard;
            await SpyCards.SpoilerGuard.banSpoilerCards(ctx.defs, remoteSpoilerGuard);
            ctx.ui.status.textContent = "Select your deck!";
            const deck = npc ? await npc.createDeck(ctx.defs) : await SpyCards.Decks.selectDeck(ctx.ui.status, ctx.defs);
            ctx.ui.status.textContent = "Waiting for opponent to select their deck...";
            const opponentCardBacks = SpyCards.Decks.decodeBacks(await ctx.net.cgc.initDeck(SpyCards.Decks.encodeBinary(ctx.defs, deck), SpyCards.Decks.encodeBacks(deck.map((c) => c.getBackID()))));
            if (opponentCardBacks.length !== ctx.defs.rules.cardsPerDeck) {
                throw new Error("opponent sent deck with " + opponentCardBacks.length + " cards, but game rules require " + ctx.defs.rules.cardsPerDeck);
            }
            for (let i = 0; i < opponentCardBacks.length; i++) {
                let expected;
                if (i < ctx.defs.rules.bossCards) {
                    expected = 2;
                }
                else if (i < ctx.defs.rules.bossCards + ctx.defs.rules.miniBossCards) {
                    expected = 1;
                }
                else {
                    expected = 0;
                }
                if (opponentCardBacks[i] !== expected) {
                    throw new Error("opponent sent invalid deck (wrong card back at position " + (i + 1) + ")");
                }
            }
            SpyCards.Audio.stopMusic();
            SpyCards.Audio.Sounds.BattleStart0.play();
            if (spyCardsVersionSuffix === "-custom")
                SpyCards.Audio.Songs.Bounty.play(0.5);
            else
                SpyCards.Audio.Songs.Miniboss.play(0.5);
            SpyCards.UI.html.classList.add("in-match");
            document.scrollingElement.scrollTop = 0;
            document.querySelectorAll(".game-log").forEach((el) => {
                SpyCards.UI.remove(el);
            });
            document.body.appendChild(ctx.state.gameLog.el);
            ctx.ui.h1.classList.remove("hidden");
            ctx.state.deck[ctx.player - 1] = deck.map((c) => ({
                card: c,
                back: c.getBackID(),
                el: c.createEl(ctx.defs),
                player: ctx.player
            }));
            ctx.state.backs[ctx.player - 1] = deck.map((c) => c.getBackID());
            ctx.state.backs[2 - ctx.player] = [].slice.call(opponentCardBacks, 0);
            ctx.state.deck[2 - ctx.player] = ctx.state.backs[2 - ctx.player].map(function (back) {
                return {
                    card: null,
                    back: back,
                    el: SpyCards.createUnknownCardEl(back),
                    player: (3 - ctx.player)
                };
            });
            SpyCards.Fx.tpEl.classList.remove("hidden");
            SpyCards.Fx.myHPEl.classList.remove("hidden");
            SpyCards.Fx.theirHPEl.classList.remove("hidden");
            document.body.appendChild(SpyCards.Fx.tpEl);
            document.body.appendChild(SpyCards.Fx.myHPEl);
            document.body.appendChild(SpyCards.Fx.theirHPEl);
            const hpEl = [
                ctx.player === 1 ? SpyCards.Fx.myHPEl : SpyCards.Fx.theirHPEl,
                ctx.player === 2 ? SpyCards.Fx.myHPEl : SpyCards.Fx.theirHPEl
            ];
            const readyButton = document.createElement("button");
            readyButton.textContent = "Ready";
            readyButton.classList.add("btn1");
            ctx.ui.form.appendChild(readyButton);
            const opponentButton = document.createElement("button");
            opponentButton.textContent = "Waiting...";
            opponentButton.classList.add("btn2");
            opponentButton.disabled = true;
            ctx.ui.form.appendChild(opponentButton);
            const updateTP = function () {
                if (ctx.net.cgc.opponentForfeit || ctx.ui.hadFatalError) {
                    return;
                }
                let tp = ctx.state.tp[ctx.player - 1];
                for (let card of ctx.state.hand[ctx.player - 1]) {
                    if (card.el.classList.contains("tapped")) {
                        tp -= card.card.effectiveTP();
                    }
                }
                const sg = SpyCards.SpoilerGuard.getSpoilerGuardData();
                const tpMul = (sg && sg.m && (sg.m & 16)) ? 2 : 1;
                SpyCards.Fx.tpEl.textContent = String(Math.abs(tp * tpMul));
                SpyCards.Fx.tpEl.classList.toggle("negative", tp < 0);
                for (let player = 0; player < 2; player++) {
                    hpEl[player].textContent = String(ctx.state.hp[player]);
                }
                if (ctx.state.ready[ctx.player - 1] && !ctx.state.ready[2 - ctx.player]) {
                    ctx.ui.status.textContent = "Waiting for opponent...";
                }
                else if (ctx.state.ready[ctx.player - 1] && ctx.state.ready[2 - ctx.player]) {
                    ctx.ui.status.textContent = "Ending turn...";
                    ctx.state.bothReady();
                }
                else if (tp < 0) {
                    ctx.ui.status.textContent = "Not enough TP!";
                }
                else {
                    ctx.ui.status.textContent = "Select your cards, then click End Turn.";
                }
                readyButton.disabled = !ctx.state.roundSetupComplete || ctx.state.ready[ctx.player - 1] || tp < 0;
                readyButton.textContent = ctx.state.ready[ctx.player - 1] ? "Ready!" : "End Turn";
                opponentButton.textContent = ctx.state.ready[2 - ctx.player] ? "Ready!" : "Waiting...";
            };
            let timerTick = null;
            readyButton.addEventListener("click", function (e) {
                e.preventDefault();
                if (readyButton.disabled) {
                    // just in case a browser does a bad
                    return;
                }
                clearTimeout(timerTick);
                SpyCards.Audio.Sounds.Confirm1.play();
                let tapReady = 0;
                const tap = [];
                for (let i = 0; i < ctx.state.hand[ctx.player - 1].length; i++) {
                    if (ctx.state.hand[ctx.player - 1][i].el.classList.contains("tapped")) {
                        tapReady = SpyCards.Binary.setBit(tapReady, i, 1);
                        SpyCards.Binary.pushUVarInt(tap, ctx.state.hand[ctx.player - 1][i].card.id);
                    }
                }
                const tapReadyEnc = [];
                SpyCards.Binary.pushUVarInt(tapReadyEnc, tapReady);
                tap.unshift(...tapReadyEnc);
                ctx.state.turnData.ready[ctx.player - 1] = tapReady;
                ctx.state.ready[ctx.player - 1] = true;
                ctx.net.cgc.sendReady(new Uint8Array(tap));
                updateTP();
            });
            ctx.net.cgc.onReady = function (promise) {
                if (!ctx.state.roundSetupComplete) {
                    ctx.state.roundSetupDelayed.push(function () {
                        ctx.net.cgc.onReady(promise);
                    });
                    return;
                }
                this.scg.promiseTurn(promise);
                ctx.state.ready[2 - ctx.player] = true;
                updateTP();
            };
            const theirHandEl = document.createElement("div");
            theirHandEl.classList.add("their-hand");
            document.body.appendChild(theirHandEl);
            const myHandEl = document.createElement("div");
            myHandEl.classList.add("my-hand");
            document.body.appendChild(myHandEl);
            myHandEl.setAttribute("role", "listbox");
            myHandEl.setAttribute("aria-orientation", "horizontal");
            myHandEl.setAttribute("aria-label", "Cards in your Hand");
            myHandEl.setAttribute("aria-multiselectable", "true");
            myHandEl.addEventListener("focus", function (e) {
                let target = e.target;
                while (target && target.classList && !target.classList.contains("card")) {
                    target = target.parentElement;
                }
                if (!target || !target.classList) {
                    return;
                }
                if (!target.id) {
                    target.id = SpyCards.UI.uniqueID();
                }
                myHandEl.setAttribute("aria-activedescendant", target.id);
            }, true);
            myHandEl.addEventListener("click", function (e) {
                if (ctx.state.ready[ctx.player - 1]) {
                    return;
                }
                let target = e.target;
                while (target && target.classList && !target.classList.contains("card")) {
                    target = target.parentElement;
                }
                if (!target || !target.classList) {
                    return;
                }
                target.classList.toggle("tapped");
                target.setAttribute("aria-selected", String(target.classList.contains("tapped")));
                updateTP();
                if (!target.classList.contains("tapped")) {
                    SpyCards.Audio.Sounds.PageFlipCancel.play();
                }
                else if (SpyCards.Fx.tpEl.classList.contains("negative")) {
                    SpyCards.Audio.Sounds.Buzzer.play();
                }
                else {
                    SpyCards.Audio.Sounds.Confirm.play();
                }
            });
            addEventListener("keydown", function f(e) {
                if (myHandEl.parentNode !== document.body) {
                    removeEventListener("keydown", f);
                    return;
                }
                if (SpyCards.disableKeyboard) {
                    return;
                }
                const selectedCard = document.getElementById(myHandEl.getAttribute("aria-activedescendant"));
                switch (e.code) {
                    case "ArrowLeft":
                        e.preventDefault();
                        if (selectedCard && selectedCard.previousElementSibling) {
                            selectedCard.previousElementSibling.focus();
                        }
                        else {
                            myHandEl.lastElementChild.focus();
                        }
                        break;
                    case "ArrowRight":
                        e.preventDefault();
                        if (selectedCard && selectedCard.nextElementSibling) {
                            selectedCard.nextElementSibling.focus();
                        }
                        else {
                            myHandEl.firstElementChild.focus();
                        }
                        break;
                    case "KeyC":
                    case "KeyX":
                        e.preventDefault();
                        if (selectedCard && (e.code === "KeyX") === selectedCard.classList.contains("tapped")) {
                            selectedCard.click();
                        }
                        break;
                    case "Space":
                        e.preventDefault();
                        if (selectedCard) {
                            selectedCard.click();
                        }
                        break;
                    case "KeyZ":
                    case "Enter":
                        e.preventDefault();
                        readyButton.click();
                        break;
                }
            });
            ctx.ui.form.appendChild(ctx.ui.h1);
            const processor = new SpyCards.Processor(ctx, SpyCards.Fx.getCallbacks(ctx, audience));
            ctx.effect = processor.effect;
            ctx.fx = SpyCards.Fx.fx;
            const timerSettings = ctx.defs.mode && ctx.defs.mode.get(SpyCards.GameModeFieldType.Timer);
            let timerStored = timerSettings ? timerSettings.startTime : 0;
            let timerTurn = 0;
            let timerTimeText = null;
            let timerOtherText = null;
            while (!processor.checkWinner()) {
                if (ctx.ui.hadFatalError) {
                    return;
                }
                ctx.state.turn++;
                myHandEl.classList.remove("offscreen");
                theirHandEl.classList.remove("offscreen");
                ctx.ui.form.classList.remove("offscreen");
                ctx.ui.h1.textContent = "Round " + ctx.state.turn;
                ctx.ui.status.textContent = "Shuffling decks...";
                readyButton.textContent = "...";
                opponentButton.textContent = "...";
                const waitTimer = setInterval(function () {
                    if (getComputedStyle(ctx.ui.form).transitionDuration === "0s") {
                        // prefers-reduced-motion
                        return;
                    }
                    switch (readyButton.textContent.length) {
                        case 1:
                            readyButton.textContent = "..";
                            opponentButton.textContent = "..";
                            break;
                        default:
                            readyButton.textContent = "...";
                            opponentButton.textContent = "...";
                            break;
                        case 3:
                            readyButton.textContent = ".";
                            opponentButton.textContent = ".";
                            break;
                    }
                }, 1500);
                ctx.state.winner = 0;
                ctx.state.turnData = await ctx.net.cgc.beginTurn();
                clearInterval(waitTimer);
                ctx.state.turnData.ready = [null, null];
                await processor.shuffleAndDraw(false);
                if (timerSettings) {
                    timerStored += timerTurn;
                    timerStored = Math.min(timerStored, timerSettings.maxTime);
                    timerTurn = Math.min(timerStored, timerSettings.maxPerTurn);
                    timerStored -= timerTurn;
                    timerStored += timerSettings.perTurn;
                    timerTick = setTimeout(function f() {
                        timerTurn--;
                        if (timerTurn > 0) {
                            if (ctx.ui.status.textContent !== timerTimeText) {
                                timerOtherText = ctx.ui.status.textContent;
                            }
                            timerTimeText = "Time Remaining: " + Math.floor(timerTurn / 60) + (timerTurn % 60 < 10 ? ":0" : ":") + (timerTurn % 60);
                            ctx.ui.status.textContent = timerTurn % 10 < 5 ? timerTimeText : timerOtherText;
                            timerTick = setTimeout(f, 1000);
                            return;
                        }
                        if (readyButton.disabled) {
                            // we're out of time; just play nothing if we can't play these cards.
                            myHandEl.querySelectorAll(".tapped").forEach((el) => el.click());
                        }
                        readyButton.click();
                    }, 1000);
                }
                if (ctx.state.turn === 1) {
                    // show game rules-summoned cards
                    for (let player = 0; player < 2; player++) {
                        for (let setup of ctx.state.setup[player]) {
                            const el = setup.card.createEl(ctx.defs);
                            el.classList.add("setup");
                            (player === ctx.player - 1 ? SpyCards.Fx.myFieldSetup : SpyCards.Fx.theirFieldSetup).appendChild(el);
                        }
                    }
                    document.body.appendChild(SpyCards.Fx.myFieldSetup);
                    document.body.appendChild(SpyCards.Fx.theirFieldSetup);
                }
                SpyCards.UI.clear(myHandEl);
                for (let card of ctx.state.hand[ctx.player - 1]) {
                    SpyCards.flipCard(card.el, false);
                    card.el.classList.remove("tapped", "setup");
                    card.el.tabIndex = 0;
                    card.el.setAttribute("role", "option");
                    card.el.setAttribute("aria-selected", "false");
                    myHandEl.appendChild(card.el);
                }
                doSquish(myHandEl);
                SpyCards.UI.clear(theirHandEl);
                for (let card of ctx.state.hand[2 - ctx.player]) {
                    card.el.classList.remove("tapped");
                    theirHandEl.insertBefore(card.el, theirHandEl.firstChild);
                }
                ctx.state.ready[0] = ctx.state.ready[1] = false;
                requestAnimationFrame(function () {
                    for (let player = 0; player < 2; player++) {
                        for (let card of ctx.state.hand[player]) {
                            card.el.classList.add("in-hand");
                        }
                    }
                });
                ctx.state.roundSetupComplete = true;
                while (ctx.state.roundSetupDelayed.length) {
                    ctx.state.roundSetupDelayed.shift()();
                }
                updateTP();
                const minimumRoundTime = sleep(1000);
                await new Promise(function (resolve) {
                    ctx.state.bothReady = resolve;
                    if (npc) {
                        npc.playRound(ctx).then((choices) => {
                            const myHand = ctx.state.hand[ctx.player - 1];
                            for (let i = 0; i < myHand.length; i++) {
                                if (SpyCards.Binary.getBit(choices, i)) {
                                    myHand[i].el.classList.add("tapped");
                                }
                            }
                            readyButton.click();
                        });
                    }
                });
                await minimumRoundTime;
                ctx.state.roundSetupComplete = false;
                ctx.state.played[0].length = 0;
                ctx.state.played[1].length = 0;
                const confirmedTurn = await ctx.net.cgc.confirmTurn();
                for (let card of ctx.state.hand[ctx.player - 1]) {
                    if (card.el.classList.contains("tapped")) {
                        ctx.state.played[ctx.player - 1].push({
                            card: card.card,
                            back: card.back,
                            el: card.el,
                            player: ctx.player,
                            effects: card.card.effects.slice(0)
                        });
                    }
                }
                ctx.state.turnData.played = [null, null];
                ctx.state.turnData.played[ctx.player - 1] = ctx.state.played[ctx.player - 1].map(function (card) {
                    return card.card.id;
                });
                myHandEl.classList.add("offscreen");
                theirHandEl.classList.add("offscreen");
                await sleep(500);
                myHandEl.classList.add("hidden");
                theirHandEl.classList.add("hidden");
                const confirmedBuf = SpyCards.toArray(confirmedTurn);
                ctx.state.turnData.ready[2 - ctx.player] = SpyCards.Binary.shiftUVarInt(confirmedBuf);
                for (let i = 0; i < ctx.state.hand[2 - ctx.player].length; i++) {
                    if (SpyCards.Binary.getBit(ctx.state.turnData.ready[2 - ctx.player], i)) {
                        ctx.state.hand[2 - ctx.player][i].el.classList.add("tapped");
                    }
                }
                ctx.state.turnData.played[2 - ctx.player] = [];
                while (confirmedBuf.length) {
                    ctx.state.turnData.played[2 - ctx.player].push(SpyCards.Binary.shiftUVarInt(confirmedBuf));
                }
                for (let id of ctx.state.turnData.played[2 - ctx.player]) {
                    if (ctx.defs.banned[id]) {
                        throw new Error("opponent played banned card " + ctx.defs.cardsByID[id].displayName());
                    }
                }
                for (let player = 0; player < 2; player++) {
                    for (let card of ctx.state.discard[player]) {
                        ctx.state.deck[player].push(card);
                        ctx.state.backs[player].push(card.back);
                    }
                    ctx.state.discard[player] = [];
                    for (let i = 0; i < ctx.state.hand[player].length; i++) {
                        const card = ctx.state.hand[player][i];
                        if (card.el.classList.contains("tapped")) {
                            card.el.classList.remove("in-hand");
                            card.el.removeAttribute("role");
                            card.el.removeAttribute("aria-selected");
                            card.el.tabIndex = null;
                            ctx.state.discard[player].push(card);
                            ctx.state.hand[player].splice(i, 1);
                            i--;
                        }
                    }
                }
                let theirTP = 0;
                ctx.state.played[2 - ctx.player] = ctx.state.turnData.played[2 - ctx.player].map((id) => {
                    const card = ctx.defs.cardsByID[id];
                    theirTP += card.effectiveTP();
                    return {
                        card: card,
                        back: card.getBackID(),
                        el: card.createEl(ctx.defs),
                        player: (3 - ctx.player),
                        effects: card.effects.slice(0)
                    };
                });
                if (theirTP > ctx.state.tp[2 - ctx.player]) {
                    throw new Error("opponent played " + theirTP + " TP of cards with a pool of " + ctx.state.tp[2 - ctx.player] + " TP");
                }
                for (let player = 0; player < 2; player++) {
                    while (ctx.state.setup[player].length) {
                        const setup = ctx.state.setup[player].shift();
                        const el = setup.card.createEl(ctx.defs);
                        el.classList.add("setup");
                        if (!setup.originalDesc) {
                            el.querySelectorAll(".card-desc > *").forEach((e) => SpyCards.UI.remove(e));
                            el.querySelector(".card-desc").appendChild(setup.effects[0].createEl(ctx.defs, setup.card));
                        }
                        ctx.state.played[player].unshift({
                            card: setup.card,
                            back: setup.card.getBackID(),
                            el: el,
                            setup: true,
                            originalDesc: setup.originalDesc,
                            player: (player + 1),
                            effects: setup.effects.slice(0)
                        });
                    }
                }
                ctx.ui.form.classList.add("match-ready", "offscreen");
                await processor.processRound();
                myHandEl.classList.remove("hidden");
                theirHandEl.classList.remove("hidden");
                if (npc) {
                    await npc.afterRound(ctx);
                }
            }
            SpyCards.Fx.tpEl.classList.add("hidden");
            SpyCards.Fx.myHPEl.classList.add("hidden");
            SpyCards.Fx.theirHPEl.classList.add("hidden");
            ctx.ui.form.classList.remove("offscreen", "hidden");
            ctx.ui.h1.classList.add("hidden");
            SpyCards.UI.remove(readyButton);
            SpyCards.UI.remove(opponentButton);
            await stage.deinit();
            const oldOnQuit = ctx.net.cgc.onQuit;
            const quitPromise = new Promise((resolve) => {
                ctx.net.cgc.onQuit = () => {
                    resolve();
                    oldOnQuit();
                };
            });
            const rematchPromise = new Promise((resolve) => {
                ctx.net.cgc.onOfferRematch = resolve;
            });
            const minVerifyWait = sleep(250);
            ctx.ui.status.textContent = "Verifying match...";
            const verifyContext = new SpyCards.MatchState(ctx.defs);
            const verifyProcessor = new SpyCards.Processor({
                net: null,
                scg: ctx.scg,
                ui: null,
                player: ctx.player,
                defs: ctx.defs,
                matchData: ctx.matchData,
                state: verifyContext
            }, new SpyCards.ProcessorCallbacks());
            const initialDecks = [null, null];
            const recording = [];
            await ctx.net.cgc.finalizeMatch(async function (initialDeck, cardBacks) {
                verifyContext.deck[ctx.player - 1] = SpyCards.Decks.decodeBinary(ctx.defs, ctx.scg.localDeckInitial).map((c) => ({
                    card: c,
                    back: c.getBackID(),
                    player: ctx.player
                }));
                verifyContext.backs[ctx.player - 1] = verifyContext.deck[ctx.player - 1].map((c) => c.card.getBackID());
                verifyContext.deck[2 - ctx.player] = SpyCards.Decks.decodeBinary(ctx.defs, initialDeck).map((c) => ({
                    card: c,
                    back: c.getBackID(),
                    player: (3 - ctx.player)
                }));
                verifyContext.backs[2 - ctx.player] = SpyCards.Decks.decodeBacks(cardBacks);
                initialDecks[0] = verifyContext.deck[0].slice(0).map((c) => c.card);
                initialDecks[1] = verifyContext.deck[1].slice(0).map((c) => c.card);
                // deck size and number of each type already verified at start of match
                for (let player = 0; player < 2; player++) {
                    let miniBoss = null;
                    for (let i = 0; i < verifyContext.deck[player].length; i++) {
                        const card = verifyContext.deck[player][i].card;
                        if (!card) {
                            throw new Error("Verification failed for player " + (player + 1) + ": invalid card number " + (i + 1) + " (ID " + initialDeck[i] + ")");
                        }
                        if (card.getBackID() !== verifyContext.backs[player][i]) {
                            throw new Error("Verification failed for player " + (player + 1) + ": invalid card back for card " + (i + 1) + " (" + card.displayName() + ")");
                        }
                        if (card.getBackID() === 1) {
                            if (miniBoss === card) {
                                throw new Error("Verification failed for player " + (player + 1) + ": duplicate mini-boss card " + card.displayName());
                            }
                            miniBoss = card;
                        }
                    }
                    if (ctx.defs.mode) {
                        for (let filter of ctx.defs.mode.getAll(SpyCards.GameModeFieldType.DeckLimitFilter)) {
                            if (verifyContext.deck[player].filter((c) => filter.match(c.card)).length > filter.count) {
                                if (filter.rank === SpyCards.Rank.None && filter.tribe === SpyCards.Tribe.None) {
                                    throw new Error("Verification failed for player " + (player + 1) + ": too many of card " + ctx.defs.cardsByID[filter.card].displayName() + " in deck");
                                }
                                else {
                                    throw new Error("Verification failed for player " + (player + 1) + ": too many cards matching Rank=" + SpyCards.rankName(filter.rank) + " Tribe=" + SpyCards.tribeName(filter.tribe, filter.customTribe) + " in deck");
                                }
                            }
                        }
                    }
                }
                SpyCards.Binary.pushUVarInt(recording, 1); // format version
                const spyCardsVersionNum = spyCardsVersionPrefix.split(/\./g).map((n) => parseInt(n, 10));
                SpyCards.Binary.pushUVarInt(recording, spyCardsVersionNum[0]);
                SpyCards.Binary.pushUVarInt(recording, spyCardsVersionNum[1]);
                SpyCards.Binary.pushUVarInt(recording, spyCardsVersionNum[2]);
                SpyCards.Binary.pushUTF8String1(recording, ctx.matchData.customModeName);
                recording.push(ctx.player);
                for (let player = 0; player < 2; player++) {
                    SpyCards.Binary.pushUTF8String1(recording, ctx.state.players[player] ? ctx.state.players[player].name : "");
                }
                SpyCards.Binary.pushUVarInt(recording, ctx.matchData.rematchCount);
                recording.push(...SpyCards.toArray(ctx.scg.seed));
                recording.push(...SpyCards.toArray(ctx.player === 1 ? ctx.scg.localRandSeed : ctx.scg.remoteRandSeed));
                recording.push(...SpyCards.toArray(ctx.player === 2 ? ctx.scg.localRandSeed : ctx.scg.remoteRandSeed));
                SpyCards.Binary.pushUVarInt(recording, ctx.defs.customCardsRaw.length);
                for (let card of ctx.defs.customCardsRaw) {
                    const buf = Base64.decode(card);
                    SpyCards.Binary.pushUVarInt(recording, buf.length);
                    recording.push(...SpyCards.toArray(buf));
                }
                for (let player = 0; player < 2; player++) {
                    const buf = CrockfordBase32.decode(SpyCards.Decks.encode(ctx.defs, initialDecks[player]));
                    SpyCards.Binary.pushUVarInt(recording, buf.length);
                    recording.push(...SpyCards.toArray(buf));
                }
                SpyCards.Binary.pushUVarInt(recording, ctx.state.turn);
            }, async function (turn) {
                verifyContext.turn++;
                recording.push(...SpyCards.toArray(turn.seed));
                recording.push(...SpyCards.toArray(turn.seed2));
                SpyCards.Binary.pushUVarInt(recording, turn.data.ready[0]);
                SpyCards.Binary.pushUVarInt(recording, turn.data.ready[1]);
                await verifyProcessor.shuffleAndDraw(true);
                for (let player = 0; player < 2; player++) {
                    if (turn.data.ready[player] >= SpyCards.Binary.bit(verifyContext.hand[player].length)) {
                        throw new Error("Verification failed for player " + (player + 1) + " on turn " + verifyContext.turn + ": cards played outside of hand");
                    }
                    const hand = verifyContext.hand[player].slice(0);
                    for (let card of verifyContext.discard[player]) {
                        verifyContext.deck[player].push(card);
                        verifyContext.backs[player].push(card.card.getBackID());
                    }
                    verifyContext.discard[player] = [];
                    let played = [];
                    for (let i = 0, j = 0; i < hand.length; i++, j++) {
                        if (SpyCards.Binary.getBit(turn.data.ready[player], i)) {
                            const card = hand[i];
                            verifyContext.discard[player].push(card);
                            played.push(card);
                            verifyContext.hand[player].splice(j, 1);
                            j--;
                        }
                    }
                    if (played.length !== turn.data.played[player].length) {
                        throw new Error("Verification failed for player " + (player + 1) + " on turn " + verifyContext.turn + ": number of cards played (" + turn.data.played[player].length + ") does not match number of cards removed from hand (" + played.length + ")");
                    }
                    for (let i = 0; i < played.length; i++) {
                        if (played[i].card.id !== turn.data.played[player][i]) {
                            throw new Error("Verification failed for player " + (player + 1) + " on turn " + verifyContext.turn + ": played card " + (i + 1) + " (" + ctx.defs.cardsByID[turn.data.played[player][i]].displayName() + ") does not match card removed from hand (" + played[i].card.displayName() + ")");
                        }
                    }
                }
                // The result of the cards being played was computed locally and can be trusted.
            });
            await minVerifyWait;
            SpyCards.UI.remove(SpyCards.Fx.tpEl);
            SpyCards.UI.remove(hpEl[0]);
            SpyCards.UI.remove(hpEl[1]);
            SpyCards.UI.remove(myHandEl);
            SpyCards.UI.remove(theirHandEl);
            SpyCards.UI.html.classList.remove("in-match");
            const won = processor.checkWinner() === ctx.player;
            SpyCards.Audio.stopMusic();
            if (won) {
                SpyCards.Audio.Sounds.CrowdClap.play();
                ctx.matchData.wins++;
            }
            else {
                SpyCards.Audio.Sounds.CrowdGaspSlow.play();
                ctx.matchData.losses++;
            }
            ctx.ui.h1.textContent = won ? "You win!" : "Opponent wins!";
            ctx.ui.form.appendChild(ctx.ui.h1);
            const recordingBuf = new Uint8Array(recording);
            ctx.ui.code.value = Base64.encode(recordingBuf);
            localStorage["spy-cards-latest-recording-v0"] = ctx.ui.code.value;
            ctx.ui.form.appendChild(ctx.ui.code);
            ctx.ui.status.textContent = "Copy this code if you want to save a recording of this match to watch with ";
            const viewerLink = document.createElement("a");
            viewerLink.target = "_blank";
            viewerLink.href = "viewer.html";
            viewerLink.textContent = "the match viewer";
            ctx.ui.status.appendChild(viewerLink);
            ctx.ui.status.appendChild(document.createTextNode(", or "));
            const uploadMatchButton = document.createElement("button");
            uploadMatchButton.textContent = "Upload Recording";
            uploadMatchButton.addEventListener("click", function (e) {
                e.preventDefault();
                uploadMatchButton.disabled = true;
                uploadMatchButton.textContent = "Uploading...";
                const xhr = new XMLHttpRequest();
                xhr.open("PUT", match_recording_base_url + "upload");
                xhr.addEventListener("load", function (e) {
                    if (xhr.status === 201) {
                        ctx.ui.code.value = xhr.responseText;
                        const recordingLink = document.createElement("a");
                        recordingLink.textContent = "Match recording";
                        recordingLink.target = "_blank";
                        recordingLink.href = "viewer.html#" + xhr.responseText;
                        ctx.ui.status.textContent = "";
                        ctx.ui.status.appendChild(recordingLink);
                        ctx.ui.status.appendChild(document.createTextNode(" successfully uploaded. "));
                        if (!npc) {
                            const didAuto = SpyCards.loadSettings().autoUploadRecording;
                            const btn = SpyCards.UI.button(didAuto ? "Stop Uploading Automatically" : "Always Upload Recordings", [], () => {
                                SpyCards.UI.remove(btn);
                                const settings = SpyCards.loadSettings();
                                settings.autoUploadRecording = !didAuto;
                                SpyCards.saveSettings(settings);
                            });
                            ctx.ui.status.appendChild(btn);
                        }
                        return;
                    }
                    ctx.ui.status.textContent = "Error uploading match: " + xhr.responseText + " (" + xhr.status + ")";
                });
                xhr.addEventListener("error", function (e) {
                    ctx.ui.status.textContent = "Error uploading match!";
                });
                xhr.send(recordingBuf);
            });
            ctx.ui.status.appendChild(uploadMatchButton);
            ctx.ui.status.appendChild(document.createTextNode(" for a shorter code."));
            ctx.ui.form.appendChild(ctx.ui.status);
            if (!npc && SpyCards.loadSettings().autoUploadRecording) {
                uploadMatchButton.click();
            }
            const decksDiv = document.createElement("div");
            decksDiv.classList.add("deck-selector");
            ctx.ui.form.appendChild(decksDiv);
            const yourDeckHeader = document.createElement("h2");
            yourDeckHeader.textContent = "Your Deck";
            const yourDeck = SpyCards.Decks.createDisplay(ctx.defs, initialDecks[ctx.player - 1]);
            yourDeck.classList.add("deck", "no-flip");
            yourDeck.style.cursor = "pointer";
            yourDeck.addEventListener("click", function () {
                window.open("decks.html#" + SpyCards.Decks.encode(ctx.defs, initialDecks[ctx.player - 1]));
            });
            decksDiv.appendChild(yourDeckHeader);
            decksDiv.appendChild(yourDeck);
            const opponentsDeckHeader = document.createElement("h2");
            opponentsDeckHeader.textContent = "Opponent's Deck";
            const opponentsDeck = SpyCards.Decks.createDisplay(ctx.defs, initialDecks[2 - ctx.player]);
            opponentsDeck.classList.add("deck", "no-flip");
            opponentsDeck.style.cursor = "pointer";
            opponentsDeck.addEventListener("click", function () {
                window.open("decks.html#" + SpyCards.Decks.encode(ctx.defs, initialDecks[2 - ctx.player]));
            });
            decksDiv.appendChild(opponentsDeckHeader);
            decksDiv.appendChild(opponentsDeck);
            ctx.net.cgc.onRematch = () => {
                ctx.net.cgc.onQuit = oldOnQuit;
                ctx.net.cgc.onRematch = null;
                SpyCards.UI.clear(ctx.ui.form);
                ctx.ui.form.appendChild(ctx.ui.h1);
                ctx.ui.form.appendChild(ctx.ui.status);
                ctx.scg = ctx.net.cgc.scg;
                ctx.matchData.rematchCount++;
                runSpyCardsGame(ctx);
            };
            const rematchButton = document.createElement("button");
            rematchButton.classList.add("btn1");
            rematchButton.textContent = "Offer Rematch";
            rematchButton.addEventListener("click", function (e) {
                e.preventDefault();
                rematchButton.disabled = true;
                rematchButton.textContent = "Offered Rematch...";
                ctx.net.cgc.offerRematch();
            });
            rematchPromise.then(() => {
                if (!rematchButton.disabled) {
                    rematchButton.textContent = "Accept Rematch";
                }
            });
            quitPromise.then(() => {
                rematchButton.textContent = "Opponent Left";
                rematchButton.disabled = true;
            });
            const exitButton = document.createElement("button");
            exitButton.classList.add("btn2");
            exitButton.textContent = "Exit";
            exitButton.addEventListener("click", function (e) {
                e.preventDefault();
                location.reload();
            });
            ctx.ui.form.appendChild(rematchButton);
            ctx.ui.form.appendChild(exitButton);
            try {
                const params = new URLSearchParams(location.search);
                if (params.has("autoRematch")) {
                    rematchButton.click();
                }
            }
            catch (ex) {
                debugger;
            }
        }
        catch (ex) {
            errorHandler(ex, ctx.state);
        }
    }
    SpyCards.runSpyCardsGame = runSpyCardsGame;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    class GameModeData {
        constructor() {
            this.fields = [];
        }
        get(type) {
            return this.fields.find((f) => f.getType() === type);
        }
        getAll(type) {
            return this.fields.filter((f) => f.getType() === type);
        }
        marshal(buf) {
            SpyCards.Binary.pushUVarInt(buf, 3); // format version
            SpyCards.Binary.pushUVarInt(buf, this.fields.length);
            for (let field of this.fields) {
                field.marshal(buf);
            }
        }
        unmarshal(buf) {
            const formatVersion = SpyCards.Binary.shiftUVarInt(buf);
            if (formatVersion !== 3) {
                throw new Error("unexpected game mode format version: " + formatVersion);
            }
            const fieldCount = SpyCards.Binary.shiftUVarInt(buf);
            this.fields = new Array(fieldCount);
            for (let i = 0; i < fieldCount; i++) {
                this.fields[i] = GameModeField.unmarshal(buf);
            }
            if (buf.length) {
                throw new Error("extra data after end of game mode");
            }
        }
    }
    SpyCards.GameModeData = GameModeData;
    let GameModeFieldType;
    (function (GameModeFieldType) {
        GameModeFieldType[GameModeFieldType["Metadata"] = 0] = "Metadata";
        GameModeFieldType[GameModeFieldType["BannedCards"] = 1] = "BannedCards";
        GameModeFieldType[GameModeFieldType["GameRules"] = 2] = "GameRules";
        GameModeFieldType[GameModeFieldType["SummonCard"] = 3] = "SummonCard";
        GameModeFieldType[GameModeFieldType["Variant"] = 4] = "Variant";
        GameModeFieldType[GameModeFieldType["UnfilterCard"] = 5] = "UnfilterCard";
        GameModeFieldType[GameModeFieldType["DeckLimitFilter"] = 6] = "DeckLimitFilter";
        GameModeFieldType[GameModeFieldType["Timer"] = 7] = "Timer";
    })(GameModeFieldType = SpyCards.GameModeFieldType || (SpyCards.GameModeFieldType = {}));
    class GameModeField {
        marshal(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.getType());
            const data = [];
            this.marshalData(data);
            SpyCards.Binary.pushUVarInt(buf, data.length);
            buf.push(...data);
        }
        static unmarshal(buf) {
            const type = SpyCards.Binary.shiftUVarInt(buf);
            if (!SpyCards.GameModeFieldConstructors[type]) {
                throw new Error("unknown game mode field type " + type);
            }
            const field = SpyCards.GameModeFieldConstructors[type]();
            const dataLength = SpyCards.Binary.shiftUVarInt(buf);
            if (buf.length < dataLength) {
                throw new Error("game mode field extends past end of buffer");
            }
            const data = buf.splice(0, dataLength);
            field.unmarshalData(data);
            if (data.length) {
                throw new Error("game mode field (" + GameModeFieldType[type] + ") did not decode full buffer");
            }
            return field;
        }
    }
    SpyCards.GameModeField = GameModeField;
    class GameModeMetadata extends GameModeField {
        constructor() {
            super(...arguments);
            this.title = "";
            this.author = "";
            this.description = "";
            this.latestChanges = "";
        }
        getType() {
            return GameModeFieldType.Metadata;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUTF8StringVar(buf, this.title);
            SpyCards.Binary.pushUTF8StringVar(buf, this.author);
            SpyCards.Binary.pushUTF8StringVar(buf, this.description);
            SpyCards.Binary.pushUTF8StringVar(buf, this.latestChanges);
        }
        unmarshalData(buf) {
            this.title = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.author = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.description = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.latestChanges = SpyCards.Binary.shiftUTF8StringVar(buf);
        }
        replaceCardID(from, to) { }
    }
    SpyCards.GameModeMetadata = GameModeMetadata;
    class GameModeBannedCards extends GameModeField {
        constructor() {
            super(...arguments);
            this.bannedCards = [];
        }
        getType() {
            return GameModeFieldType.BannedCards;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.bannedCards.length);
            for (let card of this.bannedCards) {
                SpyCards.Binary.pushUVarInt(buf, card);
            }
        }
        unmarshalData(buf) {
            const count = SpyCards.Binary.shiftUVarInt(buf);
            this.bannedCards = new Array(count);
            for (let i = 0; i < count; i++) {
                this.bannedCards[i] = SpyCards.Binary.shiftUVarInt(buf);
            }
        }
        replaceCardID(from, to) {
            this.bannedCards = this.bannedCards.map((c) => c === from ? to : c);
        }
    }
    SpyCards.GameModeBannedCards = GameModeBannedCards;
    let GameRuleType;
    (function (GameRuleType) {
        GameRuleType[GameRuleType["None"] = 0] = "None";
        GameRuleType[GameRuleType["MaxHP"] = 1] = "MaxHP";
        GameRuleType[GameRuleType["HandMinSize"] = 2] = "HandMinSize";
        GameRuleType[GameRuleType["HandMaxSize"] = 3] = "HandMaxSize";
        GameRuleType[GameRuleType["DrawPerTurn"] = 4] = "DrawPerTurn";
        GameRuleType[GameRuleType["CardsPerDeck"] = 5] = "CardsPerDeck";
        GameRuleType[GameRuleType["MinTP"] = 6] = "MinTP";
        GameRuleType[GameRuleType["MaxTP"] = 7] = "MaxTP";
        GameRuleType[GameRuleType["TPPerTurn"] = 8] = "TPPerTurn";
        GameRuleType[GameRuleType["BossCards"] = 9] = "BossCards";
        GameRuleType[GameRuleType["MiniBossCards"] = 10] = "MiniBossCards";
    })(GameRuleType = SpyCards.GameRuleType || (SpyCards.GameRuleType = {}));
    class GameModeGameRules extends GameModeField {
        constructor() {
            super(...arguments);
            this.maxHP = 5;
            this.handMinSize = 3;
            this.handMaxSize = 5;
            this.drawPerTurn = 2;
            this.cardsPerDeck = 15;
            this.minTP = 2;
            this.maxTP = 10;
            this.tpPerTurn = 1;
            this.bossCards = 1;
            this.miniBossCards = 2;
        }
        static typeName(type) {
            return GameRuleType[type].replace(/([a-z])([A-Z])|([A-Z])([A-Z][a-z])/g, "$1$3 $2$4");
        }
        getType() {
            return GameModeFieldType.GameRules;
        }
        marshalData(buf) {
            const defaults = new GameModeGameRules();
            for (let { type, get, signed } of GameModeGameRules.rules) {
                if (get(this) === get(defaults)) {
                    continue;
                }
                SpyCards.Binary.pushUVarInt(buf, type);
                if (signed) {
                    SpyCards.Binary.pushSVarInt(buf, get(this));
                }
                else {
                    SpyCards.Binary.pushUVarInt(buf, get(this));
                }
            }
            SpyCards.Binary.pushUVarInt(buf, GameRuleType.None);
        }
        unmarshalData(buf) {
            const defaults = new GameModeGameRules();
            for (let { get, set } of GameModeGameRules.rules) {
                set(this, get(defaults));
            }
            for (;;) {
                const type = SpyCards.Binary.shiftUVarInt(buf);
                if (type === GameRuleType.None) {
                    break;
                }
                const rule = GameModeGameRules.rules.find((r) => r.type === type);
                if (!rule) {
                    throw new Error("unknown game rule ID: " + type);
                }
                const { get, set, signed, max } = rule;
                if (signed) {
                    set(this, SpyCards.Binary.shiftSVarInt(buf));
                }
                else {
                    set(this, SpyCards.Binary.shiftUVarInt(buf));
                }
                if (max && get(this) > max) {
                    set(this, max);
                }
            }
        }
        replaceCardID(from, to) { }
    }
    GameModeGameRules.rules = [
        { type: GameRuleType.MaxHP, get: (r) => r.maxHP, set: (r, n) => r.maxHP = n },
        { type: GameRuleType.HandMinSize, get: (r) => r.handMinSize, set: (r, n) => r.handMinSize = n },
        { type: GameRuleType.HandMaxSize, get: (r) => r.handMaxSize, set: (r, n) => r.handMaxSize = n, max: 50 },
        { type: GameRuleType.DrawPerTurn, get: (r) => r.drawPerTurn, set: (r, n) => r.drawPerTurn = n },
        { type: GameRuleType.CardsPerDeck, get: (r) => r.cardsPerDeck, set: (r, n) => r.cardsPerDeck = n },
        { type: GameRuleType.MinTP, get: (r) => r.minTP, set: (r, n) => r.minTP = n },
        { type: GameRuleType.MaxTP, get: (r) => r.maxTP, set: (r, n) => r.maxTP = n },
        { type: GameRuleType.TPPerTurn, get: (r) => r.tpPerTurn, set: (r, n) => r.tpPerTurn = n },
        { type: GameRuleType.BossCards, get: (r) => r.bossCards, set: (r, n) => r.bossCards = n },
        { type: GameRuleType.MiniBossCards, get: (r) => r.miniBossCards, set: (r, n) => r.miniBossCards = n },
    ];
    SpyCards.GameModeGameRules = GameModeGameRules;
    let SummonCardFlags;
    (function (SummonCardFlags) {
        SummonCardFlags[SummonCardFlags["BothPlayers"] = 1] = "BothPlayers";
    })(SummonCardFlags = SpyCards.SummonCardFlags || (SpyCards.SummonCardFlags = {}));
    class GameModeSummonCard extends GameModeField {
        constructor() {
            super(...arguments);
            this.flags = SummonCardFlags.BothPlayers;
            this.card = SpyCards.CardData.GlobalCardID.Seedling;
        }
        getType() {
            return GameModeFieldType.SummonCard;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.flags);
            SpyCards.Binary.pushUVarInt(buf, this.card);
        }
        unmarshalData(buf) {
            this.flags = SpyCards.Binary.shiftUVarInt(buf);
            this.card = SpyCards.Binary.shiftUVarInt(buf);
        }
        replaceCardID(from, to) {
            if (this.card === from) {
                this.card = to;
            }
        }
    }
    SpyCards.GameModeSummonCard = GameModeSummonCard;
    class GameModeVariant extends GameModeField {
        constructor() {
            super(...arguments);
            this.title = "";
            this.npc = "";
            this.rules = [];
        }
        getType() {
            return GameModeFieldType.Variant;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUTF8StringVar(buf, this.title);
            SpyCards.Binary.pushUTF8StringVar(buf, this.npc);
            SpyCards.Binary.pushUVarInt(buf, this.rules.length);
            for (let rule of this.rules) {
                rule.marshal(buf);
            }
        }
        unmarshalData(buf) {
            this.title = SpyCards.Binary.shiftUTF8StringVar(buf);
            this.npc = SpyCards.Binary.shiftUTF8StringVar(buf);
            const numRules = SpyCards.Binary.shiftUVarInt(buf);
            this.rules.length = numRules;
            for (let i = 0; i < numRules; i++) {
                this.rules[i] = GameModeField.unmarshal(buf);
            }
        }
        replaceCardID(from, to) {
            for (let rule of this.rules) {
                rule.replaceCardID(from, to);
            }
        }
    }
    SpyCards.GameModeVariant = GameModeVariant;
    class GameModeUnfilterCard extends GameModeField {
        constructor() {
            super(...arguments);
            this.flags = 0;
            this.card = SpyCards.CardData.GlobalCardID.Seedling;
        }
        getType() {
            return GameModeFieldType.UnfilterCard;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.flags);
            SpyCards.Binary.pushUVarInt(buf, this.card);
        }
        unmarshalData(buf) {
            this.flags = SpyCards.Binary.shiftUVarInt(buf);
            this.card = SpyCards.Binary.shiftUVarInt(buf);
        }
        replaceCardID(from, to) {
            if (this.card === from) {
                this.card = to;
            }
        }
    }
    SpyCards.GameModeUnfilterCard = GameModeUnfilterCard;
    class GameModeDeckLimitFilter extends GameModeField {
        constructor() {
            super(...arguments);
            this.count = 0;
            this.rank = SpyCards.Rank.Attacker;
            this.tribe = SpyCards.Tribe.Seedling;
            this.customTribe = "";
            this.card = SpyCards.CardData.GlobalCardID.Zombiant;
        }
        getType() {
            return GameModeFieldType.DeckLimitFilter;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.count);
            buf.push((this.rank << 4) | this.tribe);
            if (this.tribe === SpyCards.Tribe.Custom) {
                SpyCards.Binary.pushUTF8StringVar(buf, this.customTribe);
            }
            if (this.rank === SpyCards.Rank.None && this.tribe === SpyCards.Tribe.None) {
                SpyCards.Binary.pushUVarInt(buf, this.card);
            }
        }
        unmarshalData(buf) {
            this.count = SpyCards.Binary.shiftUVarInt(buf);
            const rankTribe = buf.shift();
            this.rank = rankTribe >> 4;
            this.tribe = rankTribe & 15;
            if (this.tribe === SpyCards.Tribe.Custom) {
                this.customTribe = SpyCards.Binary.shiftUTF8StringVar(buf);
            }
            else {
                this.customTribe = "";
            }
            if (this.rank === SpyCards.Rank.None && this.tribe === SpyCards.Tribe.None) {
                this.card = SpyCards.Binary.shiftUVarInt(buf);
            }
            else {
                this.card = SpyCards.CardData.GlobalCardID.Zombiant;
            }
        }
        replaceCardID(from, to) {
            if (this.card === from) {
                this.card = to;
            }
        }
        match(card) {
            if (!card) {
                return false;
            }
            if (this.rank === SpyCards.Rank.None && this.tribe === SpyCards.Tribe.None) {
                return card.id === this.card;
            }
            if (this.rank === SpyCards.Rank.Enemy) {
                if (card.rank() !== SpyCards.Rank.Attacker && card.rank() !== SpyCards.Rank.Effect) {
                    return false;
                }
            }
            else if (this.rank !== SpyCards.Rank.None && card.rank() !== this.rank) {
                return false;
            }
            if (this.tribe === SpyCards.Tribe.Custom) {
                if (!card.tribes.some((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === this.customTribe)) {
                    return false;
                }
            }
            else if (this.tribe !== SpyCards.Tribe.None) {
                if (!card.tribes.some((t) => t.tribe === this.tribe)) {
                    return false;
                }
            }
            return true;
        }
    }
    SpyCards.GameModeDeckLimitFilter = GameModeDeckLimitFilter;
    class GameModeTimer extends GameModeField {
        constructor() {
            super(...arguments);
            this.startTime = 150;
            this.maxTime = 300;
            this.perTurn = 10;
            this.maxPerTurn = 90;
        }
        getType() {
            return GameModeFieldType.Timer;
        }
        marshalData(buf) {
            SpyCards.Binary.pushUVarInt(buf, this.startTime);
            SpyCards.Binary.pushUVarInt(buf, this.maxTime);
            SpyCards.Binary.pushUVarInt(buf, this.perTurn);
            SpyCards.Binary.pushUVarInt(buf, this.maxPerTurn);
        }
        unmarshalData(buf) {
            this.startTime = SpyCards.Binary.shiftUVarInt(buf);
            this.maxTime = SpyCards.Binary.shiftUVarInt(buf);
            this.perTurn = SpyCards.Binary.shiftUVarInt(buf);
            this.maxPerTurn = SpyCards.Binary.shiftUVarInt(buf);
        }
        replaceCardID(from, to) {
            // no card IDs in this field
        }
    }
    SpyCards.GameModeTimer = GameModeTimer;
    SpyCards.GameModeFieldConstructors = {
        [GameModeFieldType.Metadata]: () => new GameModeMetadata(),
        [GameModeFieldType.BannedCards]: () => new GameModeBannedCards(),
        [GameModeFieldType.GameRules]: () => new GameModeGameRules(),
        [GameModeFieldType.SummonCard]: () => new GameModeSummonCard(),
        [GameModeFieldType.Variant]: () => new GameModeVariant(),
        [GameModeFieldType.UnfilterCard]: () => new GameModeUnfilterCard(),
        [GameModeFieldType.DeckLimitFilter]: () => new GameModeDeckLimitFilter(),
        [GameModeFieldType.Timer]: () => new GameModeTimer(),
    };
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    let Button;
    (function (Button) {
        Button[Button["Up"] = 0] = "Up";
        Button[Button["Down"] = 1] = "Down";
        Button[Button["Left"] = 2] = "Left";
        Button[Button["Right"] = 3] = "Right";
        Button[Button["Confirm"] = 4] = "Confirm";
        Button[Button["Cancel"] = 5] = "Cancel";
        Button[Button["Switch"] = 6] = "Switch";
        Button[Button["Toggle"] = 7] = "Toggle";
        Button[Button["Pause"] = 8] = "Pause";
        Button[Button["Help"] = 9] = "Help";
    })(Button = SpyCards.Button || (SpyCards.Button = {}));
    SpyCards.defaultKeyButton = {
        [Button.Up]: "ArrowUp",
        [Button.Down]: "ArrowDown",
        [Button.Left]: "ArrowLeft",
        [Button.Right]: "ArrowRight",
        [Button.Confirm]: "KeyC",
        [Button.Cancel]: "KeyX",
        [Button.Switch]: "KeyZ",
        [Button.Toggle]: "KeyV",
        [Button.Pause]: "Escape",
        [Button.Help]: "Enter",
    };
    // see https://w3c.github.io/gamepad/standard_gamepad.svg
    SpyCards.standardGamepadButton = {
        [Button.Up]: 12,
        [Button.Down]: 13,
        [Button.Left]: 14,
        [Button.Right]: 15,
        [Button.Confirm]: 0,
        [Button.Cancel]: 1,
        [Button.Switch]: 2,
        [Button.Toggle]: 3,
        [Button.Pause]: 9,
        [Button.Help]: 8,
    };
    const buttonPressed = {};
    const buttonWasPressed = {};
    SpyCards.buttonHeld = {};
    const keyHeld = {};
    let lastKeyboard = 0;
    let lastGamepad = 0;
    let inputTicks = 0;
    let ButtonStyle;
    (function (ButtonStyle) {
        ButtonStyle[ButtonStyle["Keyboard"] = 0] = "Keyboard";
        ButtonStyle[ButtonStyle["GenericGamepad"] = 1] = "GenericGamepad";
    })(ButtonStyle = SpyCards.ButtonStyle || (SpyCards.ButtonStyle = {}));
    let lastGamepadStyle = ButtonStyle.GenericGamepad;
    function getButtonStyle() {
        if (!lastGamepad || lastGamepad < lastKeyboard) {
            return ButtonStyle.Keyboard;
        }
        return lastGamepadStyle;
    }
    SpyCards.getButtonStyle = getButtonStyle;
    function saveInputState() {
        const p = {};
        const w = {};
        const h = {};
        for (let btn = Button.Up; btn <= Button.Help; btn++) {
            p[btn] = buttonPressed[btn];
            w[btn] = buttonWasPressed[btn];
            h[btn] = SpyCards.buttonHeld[btn];
        }
        return { t: inputTicks, p, w, h };
    }
    SpyCards.saveInputState = saveInputState;
    function loadInputState(s) {
        inputTicks = s.t;
        for (let btn = Button.Up; btn <= Button.Help; btn++) {
            buttonPressed[btn] = s.p[btn];
            buttonWasPressed[btn] = s.w[btn];
            SpyCards.buttonHeld[btn] = s.h[btn];
        }
    }
    SpyCards.loadInputState = loadInputState;
    function updateButtons(force) {
        if (force) {
            force(SpyCards.buttonHeld);
        }
        else if (SpyCards.updateAIButtons) {
            SpyCards.updateAIButtons(SpyCards.buttonHeld);
        }
        else {
            updateHeldButtons();
        }
        inputTicks++;
        for (let button = Button.Up; button <= Button.Help; button++) {
            if (!SpyCards.buttonHeld[button]) {
                buttonPressed[button] = false;
                buttonWasPressed[button] = 0;
            }
            else if (buttonWasPressed[button] < inputTicks) {
                buttonWasPressed[button] = Infinity;
                buttonPressed[button] = true;
            }
        }
    }
    SpyCards.updateButtons = updateButtons;
    function updateHeldButtons() {
        const controlsSettings = SpyCards.loadSettings().controls || {};
        const gamepadSettings = controlsSettings.gamepad || {};
        let keyboardMap = SpyCards.defaultKeyButton;
        if (controlsSettings.customKB && controlsSettings.keyboard > 0 && controlsSettings.customKB[controlsSettings.keyboard - 1]) {
            keyboardMap = controlsSettings.customKB[controlsSettings.keyboard - 1].code;
        }
        for (let button = Button.Up; button <= Button.Help; button++) {
            SpyCards.buttonHeld[button] = false;
            if (keyHeld[keyboardMap[button]]) {
                SpyCards.buttonHeld[button] = true;
                lastKeyboard = Date.now();
            }
            for (let gp of navigator.getGamepads()) {
                if (!gp) {
                    continue;
                }
                let mapping = SpyCards.standardGamepadButton;
                let style = ButtonStyle.GenericGamepad;
                if (controlsSettings.customGP && gamepadSettings[gp.id] > 0 && controlsSettings.customGP[gamepadSettings[gp.id] - 1]) {
                    mapping = controlsSettings.customGP[gamepadSettings[gp.id] - 1].button;
                    style = controlsSettings.customGP[gamepadSettings[gp.id] - 1].style;
                }
                else if (gp.mapping !== "standard") {
                    continue;
                }
                const mapped = mapping[button];
                let pressed;
                if (Array.isArray(mapped)) {
                    const axis = gp.axes[mapped[0]];
                    if (mapped[1]) {
                        pressed = axis > 0.5;
                    }
                    else {
                        pressed = axis < -0.5;
                    }
                }
                else {
                    const gpButton = gp.buttons[mapped];
                    pressed = gpButton && gpButton.pressed;
                }
                if (pressed) {
                    SpyCards.buttonHeld[button] = true;
                    lastGamepad = Date.now();
                    lastGamepadStyle = style;
                }
            }
        }
    }
    function consumeButton(btn) {
        if (buttonPressed[btn]) {
            buttonPressed[btn] = false;
            return true;
        }
        return false;
    }
    SpyCards.consumeButton = consumeButton;
    function consumeButtonAllowRepeat(btn, delay = 0.1) {
        if (buttonPressed[btn]) {
            buttonPressed[btn] = false;
            buttonWasPressed[btn] = inputTicks + delay * 60;
            return true;
        }
        return false;
    }
    SpyCards.consumeButtonAllowRepeat = consumeButtonAllowRepeat;
    function isAllowedKeyCode(code) {
        // don't grab OS/Browser special keys
        if (code.startsWith("Alt") ||
            code.startsWith("Control") ||
            code.startsWith("Shift") ||
            code.startsWith("Meta") ||
            code.startsWith("OS") ||
            code.startsWith("Browser") ||
            code.startsWith("Launch") ||
            code.startsWith("Audio") ||
            code.startsWith("Media") ||
            code.startsWith("Volume") ||
            code.startsWith("Lang") ||
            code.startsWith("Page") ||
            code.endsWith("Mode") ||
            code.endsWith("Lock") ||
            /^F[0-9]+$/.test(code) ||
            code === "" ||
            code === "Unidentified" ||
            code === "PrintScreen" ||
            code === "Power" ||
            code === "Pause" ||
            code === "ContextMenu" ||
            code === "Help" ||
            code === "Fn" ||
            code === "Tab" ||
            code === "Home" ||
            code === "End") {
            return false;
        }
        // allow letters, numbers, and some symbols.
        if (/^Key[A-Z]$|^Digit[0-9]$|^Numpad|^Intl|Up$|Down$|Left$|Right$/.test(code)) {
            return true;
        }
        // additional symbols and special keys listed by name:
        switch (code) {
            case "Escape":
            case "Minus":
            case "Equal":
            case "Backspace":
            case "Enter":
            case "Semicolon":
            case "Quote":
            case "Backquote":
            case "Backslash":
            case "Comma":
            case "Period":
            case "Slash":
            case "Space":
            case "Insert":
            case "Delete":
                return true;
            default:
                console.warn("Unhandled keycode " + code + "; ignoring for safety.");
                debugger;
                return false;
        }
    }
    const awaitingKeyPress = [];
    addEventListener("keydown", (e) => {
        if (SpyCards.disableKeyboard) {
            return;
        }
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return;
        }
        if ("value" in e.target) {
            return;
        }
        if (isAllowedKeyCode(e.code)) {
            e.preventDefault();
            keyHeld[e.code] = true;
            for (let resolve of awaitingKeyPress) {
                resolve(e.code);
            }
            awaitingKeyPress.length = 0;
        }
    });
    addEventListener("keyup", (e) => {
        if (SpyCards.disableKeyboard) {
            return;
        }
        if (e.ctrlKey || e.altKey || e.metaKey) {
            return;
        }
        if ("value" in e.target) {
            return;
        }
        if (isAllowedKeyCode(e.code)) {
            e.preventDefault();
            keyHeld[e.code] = false;
        }
    });
    function nextPressedKey() {
        return new Promise((resolve) => awaitingKeyPress.push(resolve));
    }
    SpyCards.nextPressedKey = nextPressedKey;
    function nextPressedButton() {
        const alreadyHeld = [];
        for (let gp of navigator.getGamepads()) {
            for (let i = 0; i < gp.buttons.length; i++) {
                if (gp.buttons[i].pressed) {
                    alreadyHeld.push(gp.id + ":" + i);
                }
            }
            for (let i = 0; i < gp.axes.length; i++) {
                if (gp.axes[i] > 0.5) {
                    alreadyHeld.push(gp.id + ":+" + i);
                }
                if (gp.axes[i] < -0.5) {
                    alreadyHeld.push(gp.id + ":-" + i);
                }
            }
        }
        return new Promise((resolve) => {
            requestAnimationFrame(function check() {
                for (let gp of navigator.getGamepads()) {
                    for (let i = 0; i < gp.buttons.length; i++) {
                        const already = alreadyHeld.indexOf(gp.id + ":" + i);
                        if (already === -1 && gp.buttons[i].pressed) {
                            return resolve(i);
                        }
                        if (already !== -1 && !gp.buttons[i].pressed) {
                            alreadyHeld.splice(already, 1);
                        }
                    }
                    for (let i = 0; i < gp.axes.length; i++) {
                        const alreadyP = alreadyHeld.indexOf(gp.id + ":+" + i);
                        const alreadyN = alreadyHeld.indexOf(gp.id + ":-" + i);
                        if (alreadyP === -1 && gp.axes[i] > 0.5) {
                            return resolve([i, true]);
                        }
                        if (alreadyN === -1 && gp.axes[i] < -0.5) {
                            return resolve([i, false]);
                        }
                        if (alreadyP !== -1 && gp.axes[i] <= 0.5) {
                            alreadyHeld.splice(alreadyP, 1);
                        }
                        if (alreadyN !== -1 && gp.axes[i] >= -0.5) {
                            alreadyHeld.splice(alreadyN, 1);
                        }
                    }
                }
                requestAnimationFrame(check);
            });
        });
    }
    SpyCards.nextPressedButton = nextPressedButton;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var Minimal;
    (function (Minimal) {
        function exchangeDataPipe() {
            let resolve;
            let promise;
            return function (data) {
                if (promise) {
                    const toReturn = promise;
                    resolve(data);
                    resolve = null;
                    promise = null;
                    return toReturn;
                }
                promise = Promise.resolve(data);
                return new Promise((r) => resolve = r);
            };
        }
        function newContext(player, defs) {
            return {
                net: null,
                ui: null,
                scg: null,
                defs: defs,
                player: player,
                matchData: new SpyCards.MatchData(),
                fx: {
                    async coin() { },
                    async numb() { },
                    async setup() { },
                    async multiple() { },
                    async assist() { },
                    async beforeProcessEffect() { },
                    async afterProcessEffect() { },
                },
            };
        }
        async function playMatches(p1, p2, count = 1, maxRounds = null, defs = new SpyCards.CardDefs(), preview, modeName) {
            preview = preview || function () { };
            const p1ctx = newContext(1, defs);
            const p2ctx = newContext(2, defs);
            p1ctx.matchData.customModeName = modeName || "";
            p2ctx.matchData.customModeName = modeName || "";
            const xchg = exchangeDataPipe();
            const matches = [];
            [p1ctx.matchData.selectedCharacter, p2ctx.matchData.selectedCharacter] = await Promise.all([
                p1.pickPlayer().then((p) => p.name),
                p2.pickPlayer().then((p) => p.name),
            ]);
            while (matches.length < count) {
                p1ctx.scg = new SecureCardGame();
                p2ctx.scg = new SecureCardGame();
                p1ctx.state = new SpyCards.MatchState(defs);
                p2ctx.state = new SpyCards.MatchState(defs);
                const match = [];
                SpyCards.Binary.pushUVarInt(match, 1); // format version
                const spyCardsVersionNum = spyCardsVersionPrefix.split(/\./g).map((n) => parseInt(n, 10));
                SpyCards.Binary.pushUVarInt(match, spyCardsVersionNum[0]);
                SpyCards.Binary.pushUVarInt(match, spyCardsVersionNum[1]);
                SpyCards.Binary.pushUVarInt(match, spyCardsVersionNum[2]);
                SpyCards.Binary.pushUTF8String1(match, p1ctx.matchData.customModeName);
                match.push(0); // not from either player's perspective
                SpyCards.Binary.pushUTF8String1(match, p1ctx.matchData.selectedCharacter);
                SpyCards.Binary.pushUTF8String1(match, p2ctx.matchData.selectedCharacter);
                SpyCards.Binary.pushUVarInt(match, matches.length);
                await Promise.all([
                    p1ctx.scg.init(1, xchg),
                    p2ctx.scg.init(2, xchg)
                ]);
                match.push(...SpyCards.toArray(p1ctx.scg.seed));
                match.push(...SpyCards.toArray(p1ctx.scg.localRandSeed));
                match.push(...SpyCards.toArray(p2ctx.scg.localRandSeed));
                SpyCards.Binary.pushUVarInt(match, p1ctx.defs.customCardsRaw.length);
                for (let card of p1ctx.defs.customCardsRaw) {
                    const buf = Base64.decode(card);
                    SpyCards.Binary.pushUVarInt(match, buf.length);
                    match.push(...SpyCards.toArray(buf));
                }
                const [deck1, deck2] = await Promise.all([
                    p1.createDeck(defs),
                    p2.createDeck(defs)
                ]);
                for (let deck of [deck1, deck2]) {
                    const buf = CrockfordBase32.decode(SpyCards.Decks.encode(defs, deck));
                    SpyCards.Binary.pushUVarInt(match, buf.length);
                    match.push(...SpyCards.toArray(buf));
                }
                p1ctx.state.deck[0] = deck1.map((c) => ({ card: c, back: c.getBackID(), player: 1 }));
                p2ctx.state.deck[1] = deck2.map((c) => ({ card: c, back: c.getBackID(), player: 2 }));
                p1ctx.state.deck[1] = deck2.map((c) => ({ card: null, back: c.getBackID(), player: 2 }));
                p2ctx.state.deck[0] = deck1.map((c) => ({ card: null, back: c.getBackID(), player: 1 }));
                p1ctx.state.backs[0] = deck1.map((c) => c.getBackID());
                p1ctx.state.backs[1] = deck2.map((c) => c.getBackID());
                p2ctx.state.backs[0] = deck1.map((c) => c.getBackID());
                p2ctx.state.backs[1] = deck2.map((c) => c.getBackID());
                await Promise.all([
                    p1ctx.scg.setDeck(xchg, SpyCards.Decks.encodeBinary(defs, deck1), new Uint8Array(deck1.map(c => c.getBackID()))),
                    p2ctx.scg.setDeck(xchg, SpyCards.Decks.encodeBinary(defs, deck2), new Uint8Array(deck2.map(c => c.getBackID())))
                ]);
                const p1proc = new SpyCards.Processor(p1ctx, new SpyCards.ProcessorCallbacks());
                p1ctx.effect = p1proc.effect;
                const p2proc = new SpyCards.Processor(p2ctx, new SpyCards.ProcessorCallbacks());
                p2ctx.effect = p2proc.effect;
                while (!p1proc.checkWinner()) {
                    p1ctx.state.turn++;
                    p2ctx.state.turn++;
                    const [turnData] = await Promise.all([
                        p1ctx.scg.beginTurn(xchg),
                        p2ctx.scg.beginTurn(xchg)
                    ]);
                    await Promise.all([
                        p1proc.shuffleAndDraw(false),
                        p2proc.shuffleAndDraw(false)
                    ]);
                    turnData.ready = await Promise.all([
                        p1.playRound(p1ctx),
                        p2.playRound(p2ctx)
                    ]);
                    for (let ctx of [p1ctx, p2ctx]) {
                        for (let player = 0; player < 2; player++) {
                            while (ctx.state.discard[player].length) {
                                const card = ctx.state.discard[player].shift();
                                ctx.state.deck[player].push({
                                    card: card.card,
                                    back: card.back,
                                    player: card.player,
                                });
                                ctx.state.backs[player].push(card.back);
                            }
                            for (let i = 4; i >= 0; i--) {
                                if (turnData.ready[player] & (1 << i)) {
                                    const [card] = ctx.state.hand[player].splice(i, 1);
                                    ctx.state.discard[player].unshift(card);
                                }
                            }
                        }
                    }
                    p1ctx.state.played[0] = p1ctx.state.discard[0].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: 1,
                        effects: c.card.effects.slice(0)
                    }));
                    p2ctx.state.played[1] = p2ctx.state.discard[1].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: 2,
                        effects: c.card.effects.slice(0)
                    }));
                    p1ctx.state.played[1] = p2ctx.state.played[1].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: c.player,
                        effects: c.effects.slice(0)
                    }));
                    p2ctx.state.played[0] = p1ctx.state.played[0].map(c => ({
                        card: c.card,
                        back: c.back,
                        player: c.player,
                        effects: c.effects.slice(0)
                    }));
                    await Promise.all([
                        p1ctx.scg.prepareTurn(new Uint8Array(0)).then((x) => p2ctx.scg.promiseTurn(x)),
                        p2ctx.scg.prepareTurn(new Uint8Array(0)).then((x) => p1ctx.scg.promiseTurn(x))
                    ]);
                    await Promise.all([
                        p1ctx.scg.confirmTurn(xchg),
                        p2ctx.scg.confirmTurn(xchg)
                    ]);
                    await Promise.all([
                        p1proc.processRound(),
                        p2proc.processRound()
                    ]);
                    await Promise.all([
                        p1.afterRound(p1ctx),
                        p2.afterRound(p2ctx)
                    ]);
                    if (maxRounds && p1ctx.state.turn >= maxRounds) {
                        break;
                    }
                }
                let winner;
                if (p1ctx.state.hp[0] > 0) {
                    winner = 1;
                }
                else if (p1ctx.state.hp[1] > 0) {
                    winner = 2;
                }
                else if (p1ctx.state.winner) {
                    winner = p1ctx.state.winner;
                }
                else {
                    winner = 0;
                }
                p1ctx.matchData.rematchCount++;
                p2ctx.matchData.rematchCount++;
                if (winner === 1) {
                    p1ctx.matchData.wins++;
                    p2ctx.matchData.losses++;
                }
                else if (winner === 2) {
                    p1ctx.matchData.losses++;
                    p2ctx.matchData.wins++;
                }
                SpyCards.Binary.pushUVarInt(match, p1ctx.state.turn);
                await Promise.all([
                    p1ctx.scg.finalize(xchg, async () => { }, async (turn) => {
                        match.push(...SpyCards.toArray(turn.seed));
                        match.push(...SpyCards.toArray(turn.seed2));
                        match.push(turn.data.ready[0]);
                        match.push(turn.data.ready[1]);
                    }),
                    p2ctx.scg.finalize(xchg, async () => { }, async () => { })
                ]);
                const summary = {
                    winner: winner,
                    rounds: p1ctx.state.turn,
                    recording: new Uint8Array(match)
                };
                preview(summary);
                matches.push(summary);
            }
            return matches;
        }
        Minimal.playMatches = playMatches;
        async function playMatch(ctx, npc) {
            ctx.net.cgc.onVersion = () => { };
            ctx.net.cgc.onQuit = () => { };
            ctx.net.cgc.sendSpoilerGuardData("");
            ctx.net.cgc.sendVersion(spyCardsVersion + spyCardsVersionVariableSuffix);
            await ctx.net.cgc.initSecure(ctx.player);
            ctx.matchData.selectedCharacter = (await npc.pickPlayer()).name;
            ctx.net.cgc.sendCosmeticData({
                character: ctx.matchData.selectedCharacter,
            });
            ctx.state = new SpyCards.MatchState(ctx.defs);
            ctx.state.roundSetupComplete = false;
            ctx.state.roundSetupDelayed = [];
            const remoteSpoilerGuard = await ctx.net.cgc.recvSpoilerGuard;
            await SpyCards.SpoilerGuard.banSpoilerCards(ctx.defs, remoteSpoilerGuard);
            const deck = await npc.createDeck(ctx.defs);
            const opponentCardBacks = await ctx.net.cgc.initDeck(SpyCards.Decks.encodeBinary(ctx.defs, deck), SpyCards.Decks.encodeBacks(deck.map((c) => c.getBackID())));
            ctx.state.deck[ctx.player - 1] = deck.map((c) => ({
                card: c,
                back: c.getBackID(),
                player: ctx.player,
            }));
            ctx.state.backs[ctx.player - 1] = deck.map((c) => c.getBackID());
            ctx.state.backs[2 - ctx.player] = SpyCards.Decks.decodeBacks(opponentCardBacks);
            ctx.state.deck[2 - ctx.player] = ctx.state.backs[2 - ctx.player].map(function (back) {
                return {
                    card: null,
                    back: back,
                    player: (3 - ctx.player),
                };
            });
            const processor = new SpyCards.Processor(ctx, new SpyCards.ProcessorCallbacks());
            ctx.effect = processor.effect;
            let turnReady;
            ctx.net.cgc.onReady = function (promise) {
                if (!ctx.state.roundSetupComplete) {
                    ctx.state.roundSetupDelayed.push(function () {
                        ctx.net.cgc.onReady(promise);
                    });
                    return;
                }
                this.scg.promiseTurn(promise);
                ctx.state.ready[2 - ctx.player] = true;
                turnReady();
            };
            while (!processor.checkWinner()) {
                ctx.state.turn++;
                ctx.state.winner = 0;
                ctx.state.turnData = await ctx.net.cgc.beginTurn();
                ctx.state.turnData.ready = [null, null];
                await processor.shuffleAndDraw(false);
                ctx.state.ready[0] = ctx.state.ready[1] = false;
                ctx.state.roundSetupComplete = true;
                const waitReady = new Promise((resolve) => turnReady = resolve);
                while (ctx.state.roundSetupDelayed.length) {
                    ctx.state.roundSetupDelayed.shift()();
                }
                ctx.state.played[0].length = 0;
                ctx.state.played[1].length = 0;
                const ready = await npc.playRound(ctx);
                ctx.state.turnData.ready[ctx.player - 1] = ready;
                ctx.state.ready[ctx.player - 1] = true;
                const tap = [];
                const tapped = [[], []];
                const untapped = [[], []];
                SpyCards.Binary.pushUVarInt(tap, ready);
                for (let i = 0; i < ctx.state.hand[ctx.player - 1].length; i++) {
                    const card = ctx.state.hand[ctx.player - 1][i];
                    if (SpyCards.Binary.getBit(ready, i)) {
                        SpyCards.Binary.pushUVarInt(tap, card.card.id);
                        tapped[ctx.player - 1].push(card);
                    }
                    else {
                        untapped[ctx.player - 1].push(card);
                    }
                }
                ctx.net.cgc.sendReady(new Uint8Array(tap));
                await waitReady;
                ctx.state.roundSetupComplete = false;
                const confirmedTurn = await ctx.net.cgc.confirmTurn();
                ctx.state.turnData.played = [null, null];
                ctx.state.turnData.played[ctx.player - 1] = ctx.state.played[ctx.player - 1].map(function (card) {
                    return card.card.id;
                });
                const confirmedBuf = SpyCards.toArray(confirmedTurn);
                ctx.state.turnData.ready[2 - ctx.player] = SpyCards.Binary.shiftUVarInt(confirmedBuf);
                for (let i = 0; i < ctx.state.hand[2 - ctx.player].length; i++) {
                    const card = ctx.state.hand[2 - ctx.player][i];
                    if (SpyCards.Binary.getBit(ctx.state.turnData.ready[2 - ctx.player], i)) {
                        tapped[2 - ctx.player].push(card);
                    }
                    else {
                        untapped[2 - ctx.player].push(card);
                    }
                }
                ctx.state.turnData.played[2 - ctx.player] = [];
                let i = 0;
                while (confirmedBuf.length) {
                    const cardID = SpyCards.Binary.shiftUVarInt(confirmedBuf);
                    ctx.state.turnData.played[2 - ctx.player].push(cardID);
                    tapped[2 - ctx.player][i++].card = ctx.defs.cardsByID[cardID];
                }
                for (let player = 0; player < 2; player++) {
                    for (let card of ctx.state.discard[player]) {
                        ctx.state.deck[player].push({
                            card: card.card,
                            back: card.back,
                            player: card.player,
                        });
                        ctx.state.backs[player].push(card.back);
                    }
                    ctx.state.discard[player] = tapped[player];
                    ctx.state.hand[player] = untapped[player];
                    tapped[player] = tapped[player].slice(0);
                    for (let c of tapped[player]) {
                        c.effects = c.card.effects.slice(0);
                    }
                    ctx.state.played[player] = tapped[player];
                }
                for (let player = 0; player < 2; player++) {
                    while (ctx.state.setup[player].length) {
                        const setup = ctx.state.setup[player].shift();
                        ctx.state.played[player].unshift({
                            card: setup.card,
                            back: setup.card.getBackID(),
                            setup: true,
                            originalDesc: setup.originalDesc,
                            player: (player + 1),
                            effects: setup.effects.slice(0)
                        });
                    }
                }
                await processor.processRound();
                await npc.afterRound(ctx);
            }
            await ctx.net.cgc.finalizeMatch(async () => { }, async () => { });
            setTimeout(() => ctx.net.cgc.close(), 15000);
        }
        Minimal.playMatch = playMatch;
    })(Minimal = SpyCards.Minimal || (SpyCards.Minimal = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var AI;
    (function (AI) {
        // Rules for NPCs:
        // - MUST NOT modify arguments or any value reachable from arguments.
        // - MUST NOT modify Spy Cards code or global data.
        // - Promises MUST take a reasonable, finite amount of time to resolve.
        // - MAY read any value reachable from arguments.
        // - SHOULD return a valid response (this will be caught in online games, but not in simulations).
        // - MAY store, access, and modify data local to the NPC.
        class NPC {
            // afterRound(SpyCards.Context): optional, called after a round's winner has been determined.
            async afterRound(ctx) { }
        }
        AI.NPC = NPC;
        // Generic NPC:
        // - picks a random player
        // - creates a random 15 card deck (no ELK)
        // - plays cards, if possible, from left to right
        class GenericNPC extends NPC {
            async pickPlayer() {
                const choices = SpyCards.TheRoom.Player.characters;
                return choices[Math.floor(Math.random() * choices.length)];
            }
            async createDeck(defs) {
                function pick(arr, not) {
                    let card;
                    do {
                        card = arr[Math.floor(Math.random() * arr.length)];
                    } while (defs.banned[card.id] || defs.unpickable[card.id] || (not && not.indexOf(card) !== -1));
                    return card;
                }
                const deck = [];
                for (let i = 0; i < defs.rules.cardsPerDeck; i++) {
                    if (i < defs.rules.bossCards) {
                        deck.push(pick(defs.cardsByBack[2], [defs.cardsByID[91]].concat(deck)));
                    }
                    else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
                        deck.push(pick(defs.cardsByBack[1], deck.slice(defs.rules.bossCards)));
                    }
                    else {
                        deck.push(pick(defs.cardsByBack[0]));
                    }
                }
                return deck;
            }
            async playRound(ctx) {
                let choices = 0;
                const myHand = ctx.state.hand[ctx.player - 1];
                let tp = ctx.state.tp[ctx.player - 1];
                for (let i = 0; i < myHand.length; i++) {
                    const cardTP = myHand[i].card.effectiveTP();
                    if (cardTP <= tp) {
                        tp -= cardTP;
                        choices = SpyCards.Binary.setBit(choices, i, 1);
                    }
                }
                return choices;
            }
        }
        AI.GenericNPC = GenericNPC;
        // Card Master:
        // - has a predefined deck and player
        class CardMaster extends GenericNPC {
            constructor(name, deck) {
                super();
                this.name = name;
                this.deck = deck;
            }
            async pickPlayer() {
                return this.name ?
                    SpyCards.TheRoom.Player.characters.find((p) => p.name === this.name) :
                    super.pickPlayer();
            }
            async createDeck(defs) {
                return SpyCards.Decks.decode(defs, this.deck);
            }
        }
        AI.CardMaster = CardMaster;
        // Tourney Player:
        // - has a predefined player
        class TourneyPlayer extends GenericNPC {
            constructor(name) {
                super();
                this.name = name;
            }
            async pickPlayer() {
                return SpyCards.TheRoom.Player.characters.find((p) => p.name === this.name);
            }
        }
        AI.TourneyPlayer = TourneyPlayer;
        // Janet:
        // - janet
        class Janet extends TourneyPlayer {
            constructor() {
                super("janet");
            }
            async createDeck(defs) {
                const deck = await super.createDeck(defs);
                for (let i = 0; i < defs.rules.cardsPerDeck && i < defs.rules.bossCards; i++) {
                    if (!defs.banned[91] && defs.cardsByID[91].effectiveTP() !== Infinity && Math.random() >= 0.5) {
                        deck[i] = defs.cardsByID[91];
                        break;
                    }
                }
                return deck;
            }
        }
        AI.Janet = Janet;
        // Saved Decks:
        // - chooses a random valid local deck if one is available
        class SavedDecks extends GenericNPC {
            async createDeck(defs) {
                const decks = SpyCards.Decks.loadSaved(defs, true);
                if (decks.length) {
                    return decks[Math.floor(Math.random() * decks.length)];
                }
                return await super.createDeck(defs);
            }
        }
        AI.SavedDecks = SavedDecks;
        class MenderSpam extends NPC {
            constructor(mothiva = true) {
                super();
                this.mothiva = mothiva;
            }
            async pickPlayer() {
                if (this.mothiva) {
                    return SpyCards.TheRoom.Player.characters.find((c) => c.name === "pibu");
                }
                return SpyCards.TheRoom.Player.characters.find((c) => c.name === "nero");
            }
            async createDeck(defs) {
                const deck = [];
                if (defs.rules.bossCards !== 1 || defs.rules.miniBossCards !== 2) {
                    throw new Error("cannot create mender deck with non-standard number of (mini-)boss cards");
                }
                deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.HeavyDroneB33]);
                if (this.mothiva) {
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Mothiva]);
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Zasp]);
                }
                else {
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Kali]);
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Kabbu]);
                }
                while (deck.length < defs.rules.cardsPerDeck) {
                    deck.push(defs.cardsByID[SpyCards.CardData.GlobalCardID.Mender]);
                }
                return deck;
            }
            async playRound(ctx) {
                const myHand = ctx.state.hand[ctx.player - 1];
                const mothiva = myHand.findIndex((c) => c.card.id === SpyCards.CardData.GlobalCardID.Mothiva || c.card.id === SpyCards.CardData.GlobalCardID.Kali);
                const zasp = myHand.findIndex((c) => c.card.id === SpyCards.CardData.GlobalCardID.Zasp || c.card.id === SpyCards.CardData.GlobalCardID.Kabbu);
                const b33 = myHand.findIndex((c) => c.card.id === SpyCards.CardData.GlobalCardID.HeavyDroneB33);
                let tp = ctx.state.tp[ctx.player - 1];
                if (mothiva !== -1 && zasp !== -1 && tp >= myHand[mothiva].card.effectiveTP() + myHand[zasp].card.effectiveTP()) {
                    return SpyCards.Binary.bit(mothiva) + SpyCards.Binary.bit(zasp);
                }
                if (myHand.length < 5) {
                    return 0;
                }
                const mothivaOrZasp = mothiva === -1 ? zasp : mothiva;
                if (mothivaOrZasp !== -1 && tp >= myHand[mothivaOrZasp].card.effectiveTP()) {
                    return SpyCards.Binary.bit(mothivaOrZasp);
                }
                if (ctx.state.hp[ctx.player - 1] > 2) {
                    return 0;
                }
                if (b33 !== -1 && tp < myHand[b33].card.effectiveTP()) {
                    return 0;
                }
                if (b33 === -1 && tp < 5) {
                    return 0;
                }
                let toPlay = 0;
                if (b33 !== -1) {
                    tp -= myHand[b33].card.effectiveTP();
                    toPlay = SpyCards.Binary.setBit(toPlay, b33, 1);
                }
                for (let i = 0; i < myHand.length; i++) {
                    if (SpyCards.Binary.getBit(toPlay, i)) {
                        continue;
                    }
                    if (tp >= myHand[i].card.effectiveTP()) {
                        tp -= myHand[i].card.effectiveTP();
                        toPlay = SpyCards.Binary.setBit(toPlay, i, 1);
                    }
                }
                return toPlay;
            }
        }
        AI.MenderSpam = MenderSpam;
        class TourneyPlayer2 extends TourneyPlayer {
            constructor(name) {
                super(name);
            }
            countSynergies(defs, deck, source, target, perCard, perTribe) {
                // for now, assume all synergies are good
                let count = 0;
                const cards = [target.id];
                const tribes = [];
                for (let tribe of target.tribes) {
                    if (tribe.tribe === SpyCards.Tribe.Custom) {
                        tribes.push(tribe.custom.name);
                    }
                    else {
                        tribes.push(tribe.tribe);
                    }
                }
                for (let effect of target.effects) {
                    if (effect.type === SpyCards.EffectType.CondCoin || effect.type === SpyCards.EffectType.CondLimit) {
                        effect = effect.result;
                    }
                    if (effect.type === SpyCards.EffectType.Summon && !effect.opponent) {
                        for (let i = 0; i < effect.amount; i++) {
                            if (effect.generic) {
                                if (effect.tribe === SpyCards.Tribe.Custom) {
                                    tribes.push(effect.customTribe);
                                }
                                else if (effect.tribe !== SpyCards.Tribe.None) {
                                    tribes.push(effect.tribe);
                                }
                            }
                            else {
                                cards.push(effect.card);
                                for (let tribe of defs.cardsByID[effect.card].tribes) {
                                    tribes.push(tribe.tribe === SpyCards.Tribe.Custom ? tribe.custom.name : tribe.tribe);
                                }
                            }
                        }
                    }
                }
                for (let effect of source.effects) {
                    let mul = 1;
                    if (effect.type === SpyCards.EffectType.CondLimit && !effect.negate && source.id !== target.id) {
                        effect = effect.result;
                    }
                    if ((effect.type === SpyCards.EffectType.Empower || effect.type === SpyCards.EffectType.CondCard) && !effect.opponent) {
                        if (effect.type === SpyCards.EffectType.CondCard) {
                            mul /= effect.amount;
                        }
                        if (effect.generic) {
                            if (effect.tribe === SpyCards.Tribe.Custom) {
                                for (let t of tribes) {
                                    if (t === effect.customTribe) {
                                        count += perTribe * mul;
                                    }
                                }
                            }
                            else if (effect.tribe !== SpyCards.Tribe.None) {
                                for (let t of tribes) {
                                    if (t === effect.tribe) {
                                        count += perTribe * mul;
                                    }
                                }
                            }
                        }
                        else {
                            for (let c of cards) {
                                if (c === effect.card) {
                                    count += perCard * mul;
                                }
                            }
                        }
                    }
                }
                return count;
            }
            async selectCard(deck, defs, choices) {
                const choiceWeights = choices.map((c) => {
                    if (c.id === SpyCards.CardData.GlobalCardID.TheEverlastingKing || defs.unpickable[c.id] || c.effectiveTP() > 10) {
                        return { card: c, weight: 0 };
                    }
                    return { card: c, weight: 1 };
                });
                for (let c of choiceWeights) {
                    let statTotal = 0;
                    const unprocessedEffects = c.card.effects.slice(0);
                    while (unprocessedEffects.length) {
                        const effect = unprocessedEffects.shift();
                        // numb is very powerful
                        if (effect.type === SpyCards.EffectType.Numb && effect.opponent) {
                            statTotal += Math.min(effect.amount * 2, 5);
                        }
                        if (effect.type === SpyCards.EffectType.Stat && !effect.opponent && !effect.negate) {
                            statTotal += effect.amount;
                        }
                        if (effect.type === SpyCards.EffectType.Empower && effect.generic && !effect.opponent && !effect.negate) {
                            statTotal += effect.amount;
                        }
                        if (effect.type === SpyCards.EffectType.Summon && !effect.generic && !effect.opponent) {
                            for (let i = 0; i < effect.amount; i++) {
                                unprocessedEffects.push(...defs.cardsByID[effect.card].effects);
                            }
                        }
                        if (effect.type === SpyCards.EffectType.CondCoin &&
                            effect.result.type === SpyCards.EffectType.Stat && !effect.result.opponent && !effect.result.negate &&
                            (!effect.generic || (effect.tailsResult.type === SpyCards.EffectType.Stat && !effect.tailsResult.opponent && !effect.tailsResult.negate))) {
                            statTotal += effect.result.amount * effect.amount / 2;
                            if (effect.generic) {
                                statTotal += effect.tailsResult.amount * effect.amount / 2;
                            }
                        }
                    }
                    // avoid division by 0
                    const tp = Math.max(0.1, c.card.effectiveTP());
                    // (vanilla) cards that have a nonzero raw stat total that is lower than their TP are weak
                    if (statTotal && isFinite(statTotal)) {
                        c.weight *= statTotal / tp;
                    }
                    // don't add too many cards with high TP
                    c.weight *= Math.min(1, 5 / tp);
                    for (let card of deck) {
                        // favor cards that have synergies with existing cards
                        let synergies = 0;
                        synergies += this.countSynergies(defs, deck, c.card, card, c.card.rank() === SpyCards.Rank.MiniBoss ? 3 : 1, card.rank() >= SpyCards.Rank.MiniBoss ? 2 : 1);
                        synergies += this.countSynergies(defs, deck, card, c.card, c.card.rank() === SpyCards.Rank.MiniBoss ? 3 : 1, card.rank() >= SpyCards.Rank.MiniBoss ? 2 : 1);
                        c.weight *= Math.pow(7.5, synergies);
                        // try not to pick too many cards with the same TP cost
                        if (card.effectiveTP() === c.card.effectiveTP()) {
                            c.weight *= 0.25;
                        }
                        // favor duplicates over non-duplicates
                        if (card.id === c.card.id) {
                            // one duplicate    = 2.50
                            // two duplicates   = 1.56
                            // three duplicates = 0.57
                            // four duplicates  = 0.15
                            c.weight *= 2.5 / deck.reduce((n, o) => o.id === c.card.id ? n + 1 : n, 0);
                        }
                    }
                }
                for (let c of choiceWeights) {
                    if (defs.banned[c.card.id]) {
                        c.weight = 0;
                    }
                    else {
                        c.weight = Math.max(this.adjustWeight(c.card, c.weight, deck, defs), 0);
                    }
                }
                const totalWeight = choiceWeights.reduce((t, w) => t + w.weight, 0);
                let choice = Math.random() * totalWeight;
                for (let { card, weight } of choiceWeights) {
                    choice -= weight;
                    if (choice < 0) {
                        return card;
                    }
                }
                // rounding error, probably
                return choices[choices.length - 1];
            }
            async createDeck(defs) {
                const deck = [];
                function unique(choices, except) {
                    return choices.filter((c) => except.indexOf(c) === -1);
                }
                for (let i = 0; i < defs.rules.cardsPerDeck; i++) {
                    if (i < defs.rules.bossCards) {
                        deck.push(await this.selectCard(deck, defs, unique(defs.cardsByBack[2], deck)));
                    }
                    else if (i < defs.rules.bossCards + defs.rules.miniBossCards) {
                        deck.push(await this.selectCard(deck, defs, unique(defs.cardsByBack[1], deck.slice(defs.rules.bossCards))));
                    }
                    else {
                        deck.push(await this.selectCard(deck, defs, defs.cardsByBack[0]));
                    }
                }
                const rankOrder = [SpyCards.Rank.Boss, SpyCards.Rank.MiniBoss, SpyCards.Rank.Attacker, SpyCards.Rank.Effect];
                deck.sort((a, b) => {
                    if (a.rank() !== b.rank()) {
                        return rankOrder.indexOf(a.rank()) - rankOrder.indexOf(b.rank());
                    }
                    return defs.allCards.indexOf(a) - defs.allCards.indexOf(b);
                });
                return deck;
            }
        }
        AI.TourneyPlayer2 = TourneyPlayer2;
        class GenericNPC2 extends TourneyPlayer2 {
            constructor() {
                const choices = SpyCards.TheRoom.Player.characters;
                super(choices[Math.floor(Math.random() * choices.length)].name);
            }
            adjustWeight(card, weight) {
                return weight;
            }
        }
        AI.GenericNPC2 = GenericNPC2;
        class BuGi2 extends TourneyPlayer2 {
            constructor() {
                super("bu-gi");
            }
            adjustWeight(card, weight) {
                // bu-gi likes attacker cards
                if (card.rank() === SpyCards.Rank.Attacker) {
                    return weight * 2.5;
                }
                return weight;
            }
        }
        AI.BuGi2 = BuGi2;
        class Carmina2 extends TourneyPlayer2 {
            constructor() {
                super("carmina");
            }
            adjustWeight(card, weight) {
                // carmina favors cards with random chances on them.
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.CondCoin || (effect.type === SpyCards.EffectType.Summon && effect.generic)) {
                        return weight * 10;
                    }
                }
                return weight;
            }
        }
        AI.Carmina2 = Carmina2;
        class Janet2 extends TourneyPlayer2 {
            constructor() {
                super("janet");
            }
            adjustWeight(card, weight, deck, defs) {
                // janet.
                if (card.id !== SpyCards.CardData.GlobalCardID.TheEverlastingKing) {
                    return weight;
                }
                if (!defs.banned[card.id] && card.effectiveTP() <= 10) {
                    return defs.cardsByBack[2].length;
                }
                return weight;
            }
        }
        AI.Janet2 = Janet2;
        class Johnny2 extends TourneyPlayer2 {
            constructor() {
                super("johnny");
            }
            adjustWeight(card, weight) {
                // johnny likes cards with non-random conditions, and (mini-)bosses with empower.
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.Empower && card.rank() >= SpyCards.Rank.MiniBoss) {
                        return weight * 5;
                    }
                    if (effect.type !== SpyCards.EffectType.CondCoin && effect.type !== SpyCards.EffectType.CondLimit && effect.type >= 128) {
                        if (effect.type === SpyCards.EffectType.CondCard) {
                            return weight * 5 / effect.amount;
                        }
                        return weight * 5;
                    }
                }
                return weight;
            }
        }
        AI.Johnny2 = Johnny2;
        class Kage2 extends TourneyPlayer2 {
            constructor() {
                super("kage");
            }
            adjustWeight(card, weight) {
                // kage likes cards with multiple tribes and cards with empower or unity on them.
                weight *= card.tribes.length / 2 + 0.5;
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.CondLimit && !effect.negate) {
                        effect = effect.result;
                    }
                    if (effect.type === SpyCards.EffectType.Empower && !effect.negate && !effect.opponent) {
                        return weight * 2;
                    }
                }
                return weight;
            }
        }
        AI.Kage2 = Kage2;
        class Ritchee2 extends TourneyPlayer2 {
            constructor() {
                super("ritchee");
            }
            adjustWeight(card, weight) {
                // ritchee prefers high-cost cards.
                return weight * Math.pow(card.effectiveTP(), 3);
            }
        }
        AI.Ritchee2 = Ritchee2;
        class Serene2 extends TourneyPlayer2 {
            constructor() {
                super("serene");
            }
            adjustWeight(card, weight) {
                // serene likes cards with low TP costs.
                if (card.effectiveTP() > 3) {
                    weight *= Math.pow(3 / card.effectiveTP(), 3);
                }
                return weight;
            }
        }
        AI.Serene2 = Serene2;
        class Chuck2 extends TourneyPlayer2 {
            constructor() {
                super("chuck");
            }
            adjustWeight(card, weight) {
                // chuck loves seedlings.
                if (card.tribes.some((t) => t.tribe === SpyCards.Tribe.Seedling)) {
                    return weight * 25;
                }
                return weight;
            }
        }
        AI.Chuck2 = Chuck2;
        class Arie2 extends TourneyPlayer2 {
            constructor() {
                super("arie");
            }
            adjustWeight(card, weight) {
                // arie favors cards that cost near to 2 TP, and dislikes cards
                // that are fungi, or that are bug plus another tribe.
                if (card.effectiveTP() === 2) {
                    weight *= 3;
                }
                else {
                    weight *= Math.pow(1 / (1 + Math.abs(2 - card.effectiveTP())), 4);
                }
                for (let t of card.tribes) {
                    if (t.tribe === SpyCards.Tribe.Fungi) {
                        weight /= 10;
                        break;
                    }
                    if (t.tribe === SpyCards.Tribe.Bug && card.tribes.length > 1) {
                        weight /= 10;
                        break;
                    }
                }
                return weight;
            }
        }
        AI.Arie2 = Arie2;
        class Shay2 extends TourneyPlayer2 {
            constructor() {
                super("shay");
            }
            adjustWeight(card, weight) {
                // shay likes Thugs and cards with numb.
                if (card.tribes.some((t) => t.tribe === SpyCards.Tribe.Thug)) {
                    return weight * (card.rank() >= SpyCards.Rank.MiniBoss ? 25 : 7.5);
                }
                for (let effect of card.effects) {
                    if (effect.type === SpyCards.EffectType.CondCoin) {
                        effect = effect.result;
                    }
                    if (effect.type === SpyCards.EffectType.Numb) {
                        return weight * 5;
                    }
                }
                return weight;
            }
        }
        AI.Shay2 = Shay2;
        class Crow2 extends TourneyPlayer2 {
            constructor() {
                super("crow");
            }
            adjustWeight(card, weight) {
                // crow likes (non-boss) ??? and (all) Bot cards below 6 TP,
                // and cards with both ATK and a non-card-based condition.
                if (card.effectiveTP() < 6) {
                    for (let t of card.tribes) {
                        if ((t.tribe === SpyCards.Tribe.Unknown && card.rank() !== SpyCards.Rank.Boss) || t.tribe === SpyCards.Tribe.Bot) {
                            return weight * 30;
                        }
                    }
                }
                if (card.rank() === SpyCards.Rank.Boss) {
                    return weight / 100;
                }
                const haveCondition = card.effects.some((e) => e.type >= 128 && e.type !== SpyCards.EffectType.CondCard);
                const haveATK = card.effects.some((e) => e.type === SpyCards.EffectType.Stat && !e.defense && !e.opponent && !e.negate);
                if (haveCondition && haveATK) {
                    return weight * 40;
                }
                return weight;
            }
        }
        AI.Crow2 = Crow2;
        function getNPC(name) {
            if (typeof name === "string" && name.startsWith("cm-")) {
                const parts = name.split("-", 3);
                return new CardMaster(parts[2], parts[1]);
            }
            switch (name) {
                case "janet":
                    return new Janet();
                case "bu-gi":
                case "johnny":
                case "kage":
                case "ritchee":
                case "serene":
                case "carmina":
                    return new TourneyPlayer(name);
                case "saved-decks":
                    return new SavedDecks();
                case "tutorial":
                    return new CardMaster("carmina", "01H00000000013HSMR");
                case "carmina2":
                    return new CardMaster("carmina", "3P7T52H8MA5273HG842YF7KR");
                case "chuck":
                    return new CardMaster("chuck", "4HH0000007VXYZFG84210GG8");
                case "arie":
                    return new CardMaster("arie", "310J10G84212NANCPAD6K7VW");
                case "shay":
                    return new CardMaster("shay", "511KHRWE631GRD6K9MMA5840");
                case "crow":
                    return new CardMaster("crow", "101AXEPH8MCJ8G845G");
                case "mender-spam":
                    return new MenderSpam(Math.random() >= 0.5);
                case "mender-spam-mothiva":
                    return new MenderSpam(true);
                case "mender-spam-kali":
                    return new MenderSpam(false);
                case "tp2-generic":
                    return new GenericNPC2();
                case "tp2-janet":
                    return new Janet2();
                case "tp2-bu-gi":
                    return new BuGi2();
                case "tp2-johnny":
                    return new Johnny2();
                case "tp2-kage":
                    return new Kage2();
                case "tp2-ritchee":
                    return new Ritchee2();
                case "tp2-serene":
                    return new Serene2();
                case "tp2-carmina":
                    return new Carmina2();
                case "tp2-chuck":
                    return new Chuck2();
                case "tp2-arie":
                    return new Arie2();
                case "tp2-shay":
                    return new Shay2();
                case "tp2-crow":
                    return new Crow2();
                default:
                    return new GenericNPC();
            }
        }
        AI.getNPC = getNPC;
    })(AI = SpyCards.AI || (SpyCards.AI = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
if (!NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}
var SpyCards;
(function (SpyCards) {
    // because we can't use [...arr] in old browsers
    function toArray(arr) {
        return [].slice.call(arr, 0);
    }
    SpyCards.toArray = toArray;
})(SpyCards || (SpyCards = {}));
if (!window.TextDecoder) {
    window.TextDecoder = function () {
        // from https://stackoverflow.com/a/59339612/2664560
        this.decode = function (array) {
            let out = "";
            const len = array.length;
            let i = 0;
            while (i < len) {
                const c = array[i++];
                switch (c >> 4) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        // 0xxxxxxx
                        out += String.fromCharCode(c);
                        break;
                    case 12:
                    case 13:
                        // 110x xxxx   10xx xxxx
                        const char1 = array[i++];
                        out += String.fromCharCode(((c & 0x1F) << 6) | (char1 & 0x3F));
                        break;
                    case 14:
                        // 1110 xxxx  10xx xxxx  10xx xxxx
                        const char2 = array[i++];
                        const char3 = array[i++];
                        out += String.fromCharCode(((c & 0x0F) << 12) |
                            ((char2 & 0x3F) << 6) |
                            ((char3 & 0x3F) << 0));
                        break;
                }
            }
            return out;
        };
    };
}
"use strict";
var SpyCards;
(function (SpyCards) {
    class GameLog {
        constructor() {
            this.entries = [];
            this.entryDepth = 0;
            this.el = document.createElement("details");
            const summary = document.createElement("summary");
            summary.textContent = "Game Log";
            this.el.appendChild(summary);
            this.el.classList.add("game-log");
            this.current = this.el;
        }
        startGroup(label, collapsed) {
            const group = document.createElement("details");
            group.open = !collapsed;
            const summary = document.createElement("summary");
            summary.textContent = label;
            group.appendChild(summary);
            this.current.appendChild(group);
            this.current = group;
            this.pushEntry([label]);
            this.entryDepth++;
        }
        endGroup() {
            this.current = this.current.parentNode;
            this.entryDepth--;
        }
        log(message) {
            const entry = document.createElement("div");
            entry.textContent = message;
            this.current.appendChild(entry);
            this.pushEntry(message);
        }
        pushEntry(entry) {
            let parent = this.entries;
            for (let i = 0; i < this.entryDepth; i++) {
                parent = parent[parent.length - 1];
            }
            parent.push(entry);
        }
    }
    SpyCards.GameLog = GameLog;
    class ProcessorCallbacks {
        async beforeRound() { }
        async afterRound() { }
        async drawCard(player, card) { }
        async showStats() { }
        async beforeComputeWinner() { }
        async afterComputeWinner() { }
        async hpChange(player, amountBefore, amountChanged) { }
        async statChange(player, stat, amountBefore, amountChanged, amountAfter) { }
        async cardSummoned(summoner, summoned, special, effects) { }
        async addEffect(card, effect, alreadyProcessed, fromSummon) { }
        async beforeAddEffects(card, effects, delayProcessing) { }
    }
    SpyCards.ProcessorCallbacks = ProcessorCallbacks;
    class Processor {
        constructor(ctx, callbacks) {
            this.overrideSong = null;
            this.effect = {
                heal: async (card, effect, player, amount) => {
                    amount *= (amount < 0 ? this.ctx.state.healMultiplierN : this.ctx.state.healMultiplierP)[player - 1];
                    (amount < 0 ? this.ctx.state.healTotalN : this.ctx.state.healTotalP)[player - 1] += amount;
                    await this.callbacks.hpChange(player, this.ctx.state.hp[player - 1], amount);
                    this.ctx.state.hp[player - 1] = Math.min(this.ctx.state.hp[player - 1] + amount, this.ctx.defs.rules.maxHP);
                },
                multiplyHealing: async (card, effect, player, amount, negative) => {
                    if (negative !== true) {
                        // positive healing
                        if (this.ctx.state.healMultiplierP[player - 1]) {
                            await this.ctx.effect.heal(card, effect, player, this.ctx.state.healTotalP[player - 1] * (amount - 1) / this.ctx.state.healMultiplierP[player - 1]);
                            this.ctx.state.healMultiplierP[player - 1] *= amount;
                        }
                    }
                    if (negative !== false) {
                        // negative healing
                        if (this.ctx.state.healMultiplierN[player - 1]) {
                            await this.ctx.effect.heal(card, effect, player, this.ctx.state.healTotalN[player - 1] * (amount - 1) / this.ctx.state.healMultiplierN[player - 1]);
                            this.ctx.state.healMultiplierN[player - 1] *= amount;
                        }
                    }
                },
                modifyStat: async (card, target, effect, player, stat, amount) => {
                    if (target) {
                        const key = ("modified" + stat);
                        target[key] = target[key] || 0;
                        target[key] += amount;
                    }
                    else {
                        const key = ("raw" + stat);
                        this.ctx.state[key][player - 1] += amount;
                    }
                    await this.callbacks.statChange(player, stat, this.ctx.state[stat][player - 1], amount, this.ctx.state[stat][player - 1] + amount);
                    this.ctx.state[stat][player - 1] += amount;
                    this.ctx.state.gameLog.log("Player " + player + " " + stat + " is now " + this.ctx.state[stat][player - 1] + " (changed by " + amount + ")");
                },
                recalculateStats: async (player) => {
                    const oldATK = this.ctx.state.ATK[player - 1];
                    const oldDEF = this.ctx.state.DEF[player - 1];
                    let newATK = this.ctx.state.rawATK[player - 1];
                    let newDEF = this.ctx.state.rawDEF[player - 1];
                    for (let card of this.ctx.state.played[player - 1]) {
                        if (card.numb) {
                            continue;
                        }
                        newATK += card.modifiedATK || 0;
                        newDEF += card.modifiedDEF || 0;
                    }
                    await this.callbacks.statChange(player, "ATK", oldATK, newATK - oldATK, newATK);
                    await this.callbacks.statChange(player, "DEF", oldDEF, newDEF - oldDEF, newDEF);
                    this.ctx.state.ATK[player - 1] = newATK;
                    this.ctx.state.DEF[player - 1] = newDEF;
                },
                summonCard: async (player, summoner, toSummon, special, effects) => {
                    const summonedCard = {
                        card: toSummon,
                        back: toSummon.getBackID(),
                        player: player,
                        effects: []
                    };
                    effects = effects || toSummon.effects;
                    await this.callbacks.cardSummoned(summoner, summonedCard, special, effects);
                    this.ctx.state.gameLog.log("Player " + summoner.player + " summoned card " + toSummon.displayName() + (player !== summoner.player ? " for player " + player : ""));
                    this.ctx.state.played[player - 1].push(summonedCard);
                    for (let effect of effects) {
                        await this.ctx.effect.applyResult(summonedCard, null, effect, true);
                    }
                },
                applyResult: async (card, effect, result, delayProcessing) => {
                    this.ctx.state.gameLog.log("Adding effects " + SpyCards.EffectType[result.type] + " to player " + card.player + " card " + card.card.displayName());
                    await this.callbacks.beforeAddEffects(card, [result], delayProcessing);
                    const resultType = result.type === SpyCards.EffectType.CondPriority ? result.amount : result.type;
                    const alreadyProcessed = this.processed[resultType] ? this.processed[resultType].some((f) => Processor.checkFilter(f.filter, result)) : false;
                    await this.callbacks.addEffect(card, result, alreadyProcessed, delayProcessing);
                    if (alreadyProcessed) {
                        if (delayProcessing) {
                            this.ctx.state.effectBacklog.push({ card: card, effect: result });
                        }
                        else {
                            this.ctx.state.currentCard = card;
                            this.ctx.state.currentEffect = result;
                            this.ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[result.type] + " for player " + card.player + " card " + card.card.displayName());
                            await this.ctx.fx.beforeProcessEffect(card, result, true);
                            await SpyCards.Effect.dispatch(this.ctx, card, result);
                            await this.ctx.fx.afterProcessEffect(card, result, true);
                        }
                        return;
                    }
                    card.effects.push(result);
                }
            };
            this.ctx = ctx;
            this.callbacks = callbacks;
        }
        checkWinner() {
            const p1Dead = this.ctx.state.hp[0] <= 0;
            const p2Dead = this.ctx.state.hp[1] <= 0;
            if (p1Dead != p2Dead) {
                return p1Dead ? 2 : 1;
            }
            if (p1Dead && p2Dead) {
                return this.ctx.state.winner;
            }
            return 0;
        }
        static checkFilter(filter, effect) {
            const effectType = effect.type === SpyCards.EffectType.CondPriority ? effect.amount : effect.type;
            if (filter.type !== effectType) {
                return false;
            }
            for (let [flag, expected] of [
                [effect.negate, filter.negate],
                [effect.opponent, filter.opponent],
                [effect.each, filter.each],
                [effect.late, filter.late],
                [effect.generic, filter.generic],
                [effect.defense, filter.defense],
                [effect._reserved6, filter._reserved6],
                [effect._reserved7, filter._reserved7]
            ]) {
                if (expected === false && flag) {
                    return false;
                }
                if (expected === true && !flag) {
                    return false;
                }
            }
            return true;
        }
        async processOne(card, effect, skipCallbacks) {
            this.ctx.state.currentCard = card;
            this.ctx.state.currentEffect = effect;
            if (card.numb && effect.type !== SpyCards.EffectType.Numb) {
                return;
            }
            this.ctx.state.gameLog.log("Processing effect " + SpyCards.EffectType[effect.type] + " for player " + card.player + " card " + card.card.displayName());
            if (!skipCallbacks) {
                await this.ctx.fx.beforeProcessEffect(card, effect);
            }
            await SpyCards.Effect.dispatch(this.ctx, card, effect);
            if (!skipCallbacks) {
                await this.ctx.fx.afterProcessEffect(card, effect);
            }
        }
        async processEffect(filter, noAnim) {
            this.processed[filter.type] = this.processed[filter.type] || [];
            this.processed[filter.type].push({ filter: filter, animate: !noAnim });
            for (let player = 0; player < 2; player++) {
                for (let card of this.ctx.state.played[player]) {
                    for (let i = 0; i < card.effects.length; i++) {
                        const effect = card.effects[i];
                        if (!Processor.checkFilter(filter, effect)) {
                            continue;
                        }
                        card.effects.splice(i, 1);
                        i--;
                        await this.processOne(card, effect, noAnim);
                    }
                }
            }
            while (this.ctx.state.effectBacklog.length) {
                const { card, effect } = this.ctx.state.effectBacklog.shift();
                await this.processOne(card, effect, !this.processed[effect.type] || !this.processed[effect.type].some((f) => f.animate));
            }
        }
        async shuffleAndDraw(haveRemoteSeed) {
            const rules = this.ctx.defs.rules;
            if (this.ctx.state.turn === 1) {
                if (this.ctx.defs.mode) {
                    for (let summon of this.ctx.defs.mode.getAll(SpyCards.GameModeFieldType.SummonCard)) {
                        const card = this.ctx.defs.cardsByID[summon.card];
                        if (!card) {
                            continue;
                        }
                        this.ctx.state.setup[0].push({
                            card: card,
                            effects: [...card.effects],
                            originalDesc: true,
                        });
                        if (summon.flags & SpyCards.SummonCardFlags.BothPlayers) {
                            this.ctx.state.setup[1].push({
                                card: card,
                                effects: [...card.effects],
                                originalDesc: true,
                            });
                        }
                    }
                }
            }
            for (let player = 0; player < 2; player++) {
                this.ctx.state.tp[player] = Math.min((this.ctx.state.turn - 1) * this.ctx.defs.rules.tpPerTurn + this.ctx.defs.rules.minTP, this.ctx.defs.rules.maxTP);
                for (let setup of this.ctx.state.setup[player]) {
                    for (let e of setup.effects) {
                        if (e.type === SpyCards.EffectType.TP) {
                            this.ctx.state.tp[player] += e.negate ? -e.amount : e.amount;
                        }
                    }
                }
                this.ctx.state.tp[player] = Math.max(this.ctx.state.tp[player], 0);
                if (haveRemoteSeed || this.ctx.player - 1 === player) {
                    await this.ctx.scg.privateShuffle(this.ctx.state.deck[player], this.ctx.state.backs[player], 2 - this.ctx.player === player);
                }
                else {
                    await this.ctx.scg.publicShuffle(this.ctx.state.deck[player], this.ctx.state.backs[player]);
                }
                for (let i = 0; (i < rules.drawPerTurn || this.ctx.state.hand[player].length < rules.handMinSize) && this.ctx.state.hand[player].length < rules.handMaxSize && this.ctx.state.deck[player].length; i++) {
                    const card = this.ctx.state.deck[player].pop();
                    await this.callbacks.drawCard((player + 1), card);
                    this.ctx.state.hand[player].push(card);
                    this.ctx.state.backs[player].pop();
                }
            }
            if (!this.overrideSong && this.ctx.matchData.customModeName && this.ctx.matchData.customModeName.startsWith("bossmode.") && this.ctx.fx === SpyCards.Fx.fx) {
                for (let c of this.ctx.state.setup[2 - this.ctx.player]) {
                    if (c.card.tribes.some((t) => t.tribe === SpyCards.Tribe.Custom && t.custom.name === "Boss")) {
                        switch (c.card.portrait) {
                            case 2:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle1.opus", 4.665, 71);
                                break;
                            case 21:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle2.opus", 1.8, 80);
                                break;
                            case 32:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle4.opus", 12.95, 83.68);
                                break;
                            case 43:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle5.opus", 1.7, 81.13);
                                break;
                            case 53:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle7.opus", 5.915, 67.4);
                                break;
                            case 72:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Final1.opus", 29.1, 169.54);
                                break;
                            case 126:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle8.opus", 5.52, 82.36);
                                break;
                            case 146:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Battle9.opus", 2.3, 101.9);
                                break;
                            case 174:
                                this.overrideSong = new SpyCards.Audio.Music("https://music.spy-cards.lubar.me/Final2.opus", 32.8, 183.6);
                                break;
                        }
                        if (this.overrideSong) {
                            await this.overrideSong.play();
                            break;
                        }
                    }
                }
            }
        }
        async processRound() {
            this.ctx.state.gameLog.startGroup("Round " + this.ctx.state.turn, true);
            this.ctx.state.gameLog.startGroup("Player 1 plays:");
            for (let c of this.ctx.state.played[0]) {
                this.ctx.state.gameLog.log(c.card.displayName() + (c.setup ? " (setup)" : ""));
            }
            this.ctx.state.gameLog.endGroup();
            this.ctx.state.gameLog.startGroup("Player 2 plays:");
            for (let c of this.ctx.state.played[1]) {
                this.ctx.state.gameLog.log(c.card.displayName() + (c.setup ? " (setup)" : ""));
            }
            this.ctx.state.gameLog.endGroup();
            this.ctx.state.rawATK[0] = this.ctx.state.rawATK[1] = 0;
            this.ctx.state.rawDEF[0] = this.ctx.state.rawDEF[1] = 0;
            this.ctx.state.ATK[0] = this.ctx.state.ATK[1] = 0;
            this.ctx.state.DEF[0] = this.ctx.state.DEF[1] = 0;
            this.ctx.state.healTotalN[0] = this.ctx.state.healTotalN[1] = 0;
            this.ctx.state.healTotalP[0] = this.ctx.state.healTotalP[1] = 0;
            this.ctx.state.healMultiplierN[0] = this.ctx.state.healMultiplierN[1] = 1;
            this.ctx.state.healMultiplierP[0] = this.ctx.state.healMultiplierP[1] = 1;
            this.ctx.state.winner = 0;
            this.ctx.state.effectBacklog.length = 0;
            this.ctx.state.once[0].length = 0;
            this.ctx.state.once[1].length = 0;
            this.processed = {};
            await this.callbacks.beforeRound();
            for (let filter of SpyCards.Effect.order.ignored) {
                await this.processEffect(filter, true);
            }
            for (let filter of SpyCards.Effect.order.pre) {
                await this.processEffect(filter);
            }
            await this.callbacks.showStats();
            for (let filter of SpyCards.Effect.order.main) {
                await this.processEffect(filter);
            }
            await this.callbacks.beforeComputeWinner();
            this.ctx.state.gameLog.log("Subtracting DEF from ATK...");
            this.ctx.state.DEF[0] = Math.max(this.ctx.state.DEF[0], 0);
            this.ctx.state.DEF[1] = Math.max(this.ctx.state.DEF[1], 0);
            await this.ctx.effect.modifyStat(null, null, null, 1, "ATK", -this.ctx.state.DEF[1]);
            await this.ctx.effect.modifyStat(null, null, null, 2, "ATK", -this.ctx.state.DEF[0]);
            this.ctx.state.ATK[0] = Math.max(this.ctx.state.ATK[0], 0);
            this.ctx.state.ATK[1] = Math.max(this.ctx.state.ATK[1], 0);
            if (this.ctx.state.ATK[0] < this.ctx.state.ATK[1]) {
                this.ctx.state.winner = 2;
            }
            else if (this.ctx.state.ATK[0] > this.ctx.state.ATK[1]) {
                this.ctx.state.winner = 1;
            }
            else {
                this.ctx.state.winner = 0;
            }
            if (this.ctx.state.winner) {
                this.ctx.state.gameLog.log("Player " + this.ctx.state.winner + " wins round!");
            }
            else {
                this.ctx.state.gameLog.log("Round was a tie!");
            }
            await this.callbacks.afterComputeWinner();
            if (this.ctx.state.winner === 1) {
                await this.callbacks.hpChange(2, this.ctx.state.hp[1], -1);
                this.ctx.state.hp[1]--;
            }
            else if (this.ctx.state.winner === 2) {
                await this.callbacks.hpChange(1, this.ctx.state.hp[0], -1);
                this.ctx.state.hp[0]--;
            }
            for (let filter of SpyCards.Effect.order.post) {
                await this.processEffect(filter);
            }
            for (let filter of SpyCards.Effect.order.discard) {
                await this.processEffect(filter, true);
            }
            const unhandledEffects = [];
            for (let player = 0; player < 2; player++) {
                for (let card of this.ctx.state.played[player]) {
                    if (card.effects.length) {
                        unhandledEffects.push(card);
                    }
                }
            }
            if (unhandledEffects.length) {
                debugger;
                throw new Error("unhandled effects: " + unhandledEffects.map((c) => {
                    return c.effects.map((e) => e.type);
                }));
            }
            await this.callbacks.afterRound();
            this.ctx.state.gameLog.endGroup();
        }
    }
    SpyCards.Processor = Processor;
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var TheRoom;
    (function (TheRoom) {
        class Stage {
            async init() {
                SpyCards.UI.html.classList.add("in-room");
                this.rpc = await SpyCards.Native.roomRPC();
                if (this.rpc) {
                    this.id = await new Promise((resolve) => this.rpc.newStage(resolve));
                    this.rpc.setRoom(this.id);
                }
            }
            async deinit() {
                if (this.rpc) {
                    this.rpc.exit();
                    this.id = 0;
                    this.rpc = null;
                    SpyCards.UI.html.classList.remove("in-room", "room-flipped");
                }
            }
            setFlipped(flipped) {
                if (this.rpc) {
                    this.rpc.setCurrentPlayer(this.id, flipped ? 2 : 1);
                }
            }
            setPlayer(player) {
                if (!this.rpc) {
                    return;
                }
                player.rpc = this.rpc;
                player.id = this.rpc.setPlayer(this.id, player.name, player.num);
            }
            addAudienceMember(member) {
                if (!this.rpc) {
                    return;
                }
                this.rpc.addAudience(this.id, member.id);
            }
            createAudienceMember(type, x, y, back, flip, color, excitement) {
                if (!this.rpc) {
                    return new Audience(null, 0);
                }
                return new Audience(this.rpc, this.rpc.createAudienceMember(type, x, y, back, flip, color, excitement));
            }
            async createAudience(seed, rematches) {
                if (!this.rpc) {
                    return [];
                }
                const a = this.rpc.createAudience(seed, rematches).map((a) => new Audience(this.rpc, a));
                for (let member of a) {
                    this.addAudienceMember(member);
                }
                return a;
            }
        }
        TheRoom.Stage = Stage;
        let PlayerFlags;
        (function (PlayerFlags) {
            PlayerFlags[PlayerFlags["Ant"] = 0] = "Ant";
            PlayerFlags[PlayerFlags["Bee"] = 1] = "Bee";
            PlayerFlags[PlayerFlags["Beefly"] = 2] = "Beefly";
            PlayerFlags[PlayerFlags["Beetle"] = 3] = "Beetle";
            PlayerFlags[PlayerFlags["Butterfly"] = 4] = "Butterfly";
            PlayerFlags[PlayerFlags["Cricket"] = 5] = "Cricket";
            PlayerFlags[PlayerFlags["Dragonfly"] = 6] = "Dragonfly";
            PlayerFlags[PlayerFlags["Firefly"] = 7] = "Firefly";
            PlayerFlags[PlayerFlags["Ladybug"] = 8] = "Ladybug";
            PlayerFlags[PlayerFlags["Mantis"] = 9] = "Mantis";
            PlayerFlags[PlayerFlags["Mosquito"] = 10] = "Mosquito";
            PlayerFlags[PlayerFlags["Moth"] = 11] = "Moth";
            PlayerFlags[PlayerFlags["Tangy"] = 12] = "Tangy";
            PlayerFlags[PlayerFlags["Wasp"] = 13] = "Wasp";
            PlayerFlags[PlayerFlags["NonAwakened"] = 14] = "NonAwakened";
            PlayerFlags[PlayerFlags["French"] = 15] = "French";
            PlayerFlags[PlayerFlags["Explorer"] = 16] = "Explorer";
            PlayerFlags[PlayerFlags["Snakemouth"] = 17] = "Snakemouth";
            PlayerFlags[PlayerFlags["Criminal"] = 18] = "Criminal";
            PlayerFlags[PlayerFlags["CardMaster"] = 19] = "CardMaster";
            PlayerFlags[PlayerFlags["MetalIsland"] = 20] = "MetalIsland";
            PlayerFlags[PlayerFlags["Developer"] = 21] = "Developer";
        })(PlayerFlags = TheRoom.PlayerFlags || (TheRoom.PlayerFlags = {}));
        function playerDisplayName(character) {
            return character.displayName || (character.name.substr(0, 1).toUpperCase() + character.name.substr(1));
        }
        TheRoom.playerDisplayName = playerDisplayName;
        class Player {
            constructor(num, name) {
                this.num = num;
                this.name = name;
            }
            setCharacter(character) {
                if (this.rpc) {
                    this.rpc.setCharacter(this.id, character.name);
                }
                this.name = character.name;
            }
            becomeAngry(ms = 2000) {
                if (this.rpc) {
                    this.rpc.becomeAngry(this.id, ms / 1000);
                }
            }
        }
        Player.characters = [
            {
                name: "amber",
                flags: [
                    PlayerFlags.Ant
                ]
            },
            {
                name: "aria",
                displayName: "Acolyte Aria",
                flags: [
                    PlayerFlags.Mantis
                ]
            },
            {
                name: "arie",
                flags: [
                    PlayerFlags.Butterfly,
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "astotheles",
                flags: [
                    PlayerFlags.Cricket,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "bomby",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "bu-gi",
                flags: [
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "carmina",
                flags: [
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "celia",
                flags: [
                    PlayerFlags.Ant,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "cenn",
                flags: [
                    PlayerFlags.Butterfly,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "cerise",
                flags: [
                    PlayerFlags.Tangy
                ]
            },
            {
                name: "chompy",
                flags: [
                    PlayerFlags.NonAwakened
                ]
            },
            {
                name: "chubee",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "chuck",
                flags: [
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "crisbee",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "crow",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "diana",
                flags: [
                    PlayerFlags.Ant
                ]
            },
            {
                name: "eremi",
                flags: [
                    PlayerFlags.Mantis
                ]
            },
            {
                name: "futes",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Developer
                ]
            },
            {
                name: "genow",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.Developer
                ]
            },
            {
                name: "honeycomb",
                displayName: "Professor Honeycomb",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "janet",
                flags: [
                    PlayerFlags.Ant,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "jaune",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.French
                ]
            },
            {
                name: "jayde",
                flags: [
                    PlayerFlags.Wasp
                ]
            },
            {
                name: "johnny",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "kabbu",
                flags: [
                    PlayerFlags.Beetle,
                    PlayerFlags.Explorer,
                    PlayerFlags.Snakemouth
                ]
            },
            {
                name: "kage",
                flags: [
                    PlayerFlags.Ladybug,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "kali",
                flags: [
                    PlayerFlags.Moth
                ]
            },
            {
                name: "kenny",
                flags: [
                    PlayerFlags.Beetle
                ]
            },
            {
                name: "kina",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "lanya",
                flags: [
                    PlayerFlags.Firefly
                ]
            },
            {
                name: "leif",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.Snakemouth,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "levi",
                flags: [
                    PlayerFlags.Ladybug,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "maki",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "malbee",
                flags: [
                    PlayerFlags.Bee
                ]
            },
            {
                name: "mar",
                flags: [
                    PlayerFlags.Mantis,
                    PlayerFlags.Developer
                ]
            },
            {
                name: "mothiva",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "neolith",
                displayName: "Professor Neolith",
                flags: [
                    PlayerFlags.Moth
                ]
            },
            {
                name: "nero",
                flags: [
                    PlayerFlags.NonAwakened
                ]
            },
            {
                name: "pibu",
                flags: [
                    PlayerFlags.NonAwakened
                ]
            },
            {
                name: "pisci",
                flags: [
                    PlayerFlags.Beetle,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "ritchee",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "riz",
                flags: [
                    PlayerFlags.Dragonfly
                ]
            },
            {
                name: "samira",
                flags: [
                    PlayerFlags.Beefly
                ]
            },
            {
                name: "scarlet",
                displayName: "Monsieur Scarlet",
                flags: [
                    PlayerFlags.Ant,
                    PlayerFlags.Criminal
                ]
            },
            {
                name: "serene",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.MetalIsland
                ]
            },
            {
                name: "shay",
                flags: [
                    PlayerFlags.Mosquito,
                    PlayerFlags.CardMaster
                ]
            },
            {
                name: "tanjerin",
                flags: [
                    PlayerFlags.Tangy
                ]
            },
            {
                name: "ultimax",
                displayName: "General Ultimax",
                flags: [
                    PlayerFlags.Wasp
                ]
            },
            {
                name: "vanessa",
                flags: [
                    PlayerFlags.Wasp
                ]
            },
            {
                name: "vi",
                flags: [
                    PlayerFlags.Bee,
                    PlayerFlags.Snakemouth,
                    PlayerFlags.Explorer,
                    PlayerFlags.French
                ]
            },
            {
                name: "yin",
                flags: [
                    PlayerFlags.Moth,
                    PlayerFlags.Explorer
                ]
            },
            {
                name: "zaryant",
                flags: [
                    PlayerFlags.Ant
                ]
            },
            {
                name: "zasp",
                flags: [
                    PlayerFlags.Wasp,
                    PlayerFlags.Explorer
                ]
            }
        ];
        TheRoom.Player = Player;
        class Audience {
            constructor(rpc, id) {
                this.rpc = rpc;
                this.id = id;
            }
            startCheer(ms = 5000) {
                if (!this.rpc) {
                    return;
                }
                this.rpc.startCheer(this.id, ms / 1000);
            }
            isBack() {
                if (!this.rpc) {
                    return false;
                }
                return this.rpc.isAudienceBack(this.id);
            }
        }
        TheRoom.Audience = Audience;
    })(TheRoom = SpyCards.TheRoom || (SpyCards.TheRoom = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var SyncPoint;
(function (SyncPoint) {
    SyncPoint[SyncPoint["InitialSeed"] = 0] = "InitialSeed";
    SyncPoint[SyncPoint["DeckPromise"] = 1] = "DeckPromise";
    SyncPoint[SyncPoint["TurnSeedPromise"] = 2] = "TurnSeedPromise";
    SyncPoint[SyncPoint["TurnSeed"] = 3] = "TurnSeed";
    SyncPoint[SyncPoint["ConfirmTurn"] = 4] = "ConfirmTurn";
    SyncPoint[SyncPoint["Finalize"] = 5] = "Finalize";
})(SyncPoint || (SyncPoint = {}));
async function doExchangeData(syncPoint, syncIndex, exchangeDataAsync, sendData) {
    let buf = [];
    SpyCards.Binary.pushUVarInt(buf, syncPoint);
    SpyCards.Binary.pushUVarInt(buf, syncIndex);
    buf.push(...SpyCards.toArray(sendData));
    try {
        const recvData = await exchangeDataAsync(new Uint8Array(buf));
        buf = SpyCards.toArray(recvData);
        const recvSyncPoint = SpyCards.Binary.shiftUVarInt(buf);
        const recvSyncIndex = SpyCards.Binary.shiftUVarInt(buf);
        if (recvSyncPoint !== syncPoint || recvSyncIndex !== syncIndex) {
            throw new Error("received data for " + SyncPoint[recvSyncPoint] + (recvSyncIndex ? "[" + recvSyncIndex + "]" : "") + ": " + Base64.encode(recvData));
        }
        return new Uint8Array(buf);
    }
    catch (ex) {
        throw new Error("while exchanging data for " + SyncPoint[syncPoint] + (syncIndex ? "[" + syncIndex + "]" : "") + ": " + (ex.message || ex));
    }
}
class SecureCardGame {
    constructor(config) {
        this.totalAdvanceCount = [0, 0, 0];
        config = config || {};
        this.hash = config.hash || "SHA-256";
        this.seedLength = config.seedLength || 16;
        this.randLength = config.randLength || 4;
        if (config.forReplay) {
            this.compat = config.forReplay;
            this.turns = [{ seed: null, data: null }];
            return;
        }
        this.player = 0;
        this.seed = new Uint8Array(this.seedLength * 2);
        this.localRandSeed = new Uint8Array(this.randLength);
        crypto.getRandomValues(this.seed.subarray(0, this.seedLength));
        crypto.getRandomValues(this.localRandSeed);
    }
    async init(playerNumber, exchangeDataAsync) {
        if (this.player !== 0) {
            console.error("init called multiple times on a single connection!");
            return;
        }
        if (playerNumber !== 1 && playerNumber !== 2) {
            throw new Error("playerNumber must be either 1 or 2");
        }
        this.player = playerNumber;
        const sendSeed = this.seed.subarray(0, this.seedLength);
        if (playerNumber === 2) {
            this.seed.set(sendSeed, this.seedLength);
        }
        const recvSeed = await doExchangeData(SyncPoint.InitialSeed, 0, exchangeDataAsync, sendSeed);
        if (!(recvSeed instanceof Uint8Array) || recvSeed.length !== this.seedLength) {
            throw new Error("initial handshake: expected " + this.seedLength + " bytes of data, but received " + recvSeed.length + " bytes");
        }
        if (playerNumber === 1) {
            this.seed.set(recvSeed, this.seedLength);
        }
        else {
            this.seed.set(recvSeed, 0);
        }
    }
    async setDeck(exchangeDataAsync, deck, cardBacks) {
        if (this.player === 0) {
            throw new Error("init must be called before setDeck");
        }
        if (this.localDeckInitial) {
            throw new Error("setDeck has already been called");
        }
        if (!deck) {
            throw new Error("missing deck");
        }
        const typedDeck = new Uint8Array(deck);
        this.localDeckInitial = typedDeck;
        const buffer = new Uint8Array(this.seed.length + this.randLength + this.localDeckInitial.byteLength);
        buffer.set(this.seed, 0);
        buffer.set(this.localRandSeed, this.seed.length);
        buffer.set(new Uint8Array(this.localDeckInitial.buffer, 0), this.seed.length + this.randLength);
        const bufferHash = new Uint8Array(await crypto.subtle.digest(this.hash, buffer));
        const sendData = cardBacks ? new Uint8Array(bufferHash.length + cardBacks.length) : bufferHash;
        if (cardBacks) {
            sendData.set(bufferHash, 0);
            sendData.set(cardBacks, bufferHash.length);
        }
        const recvData = await doExchangeData(SyncPoint.DeckPromise, 0, exchangeDataAsync, sendData);
        if (!(recvData instanceof Uint8Array) || recvData.length < bufferHash.length) {
            throw new Error("while exchanging deck information: expected at least " + bufferHash.length + " bytes of data, but received " + recvData.length + " bytes");
        }
        if (cardBacks) {
            this.remoteCardBacks = new Uint8Array(recvData.buffer, bufferHash.length);
        }
        else if (recvData.length !== bufferHash.length) {
            throw new Error("unexpectedly received card backs");
        }
        this.turns = [];
        return cardBacks ? new Uint8Array(this.remoteCardBacks) : undefined;
    }
    async beginTurn(exchangeDataAsync) {
        if (!this.turns) {
            throw new Error("init and setDeck must be called before beginTurn may be called");
        }
        if (this.finalized) {
            throw new Error("cannot begin turn on finalized game");
        }
        const turn = {
            seed: new Uint8Array(this.randLength * 2),
            data: {}
        };
        const sendSeed = turn.seed.subarray((this.player - 1) * this.randLength).subarray(0, this.randLength);
        crypto.getRandomValues(sendSeed);
        const hashInput = new Uint8Array(this.seed.length + this.randLength);
        hashInput.set(this.seed, 0);
        hashInput.set(sendSeed, this.seed.length);
        const sendHash = new Uint8Array(await crypto.subtle.digest(this.hash, hashInput));
        const recvHash = await doExchangeData(SyncPoint.TurnSeedPromise, this.turns.length + 1, exchangeDataAsync, sendHash);
        if (!(recvHash instanceof Uint8Array) || sendHash.length !== recvHash.length) {
            throw new Error("setting up turn (1): expected " + sendHash.length + " bytes of data but received " + recvHash.length + " bytes");
        }
        const recvSeed = await doExchangeData(SyncPoint.TurnSeed, this.turns.length + 1, exchangeDataAsync, sendSeed);
        if (!(recvSeed instanceof Uint8Array) || sendSeed.length !== recvSeed.length) {
            throw new Error("setting up turn (2): expected " + sendSeed.length + " bytes of data but received " + recvSeed.length + " bytes");
        }
        hashInput.set(recvSeed, this.seed.length);
        const verifyHash = new Uint8Array(await crypto.subtle.digest(this.hash, hashInput));
        if (!recvHash.every(function (b, i) { return b === verifyHash[i]; })) {
            throw new Error("remote hash did not match value. possible implementation error or cheating attempt.");
        }
        turn.seed.set(recvSeed, (2 - this.player) * this.randLength);
        this.turns.push(turn);
        await this.initTurnSeed(turn.seed, this.localRandSeed);
        return turn.data;
    }
    async prepareTurn(data) {
        if (!this.turns || !this.turns.length) {
            throw new Error("init, setDeck, and beginTurn must be called before prepareTurn may be called");
        }
        if (this.finalized) {
            throw new Error("cannot prepare turn on finalized game");
        }
        const turn = this.turns[this.turns.length - 1];
        if (!turn.seed2) {
            turn.seed2 = new Uint8Array(this.randLength * 2);
        }
        const confirm = new Uint8Array(this.seed.length + this.randLength + data.length);
        confirm.set(this.seed, 0);
        crypto.getRandomValues(confirm.subarray(this.seed.length, this.seed.length + this.randLength));
        turn.seed2.set(confirm.subarray(this.seed.length, this.seed.length + this.randLength), this.player === 2 ? this.randLength : 0);
        confirm.set(data, this.seed.length + this.randLength);
        turn[this.player === 2 ? "confirm2" : "confirm1"] = confirm.subarray(this.seed.length);
        const promise = new Uint8Array(await crypto.subtle.digest(this.hash, confirm), 0);
        return promise;
    }
    promiseTurn(promise) {
        if (!this.turns || !this.turns.length) {
            throw new Error("init, setDeck, and beginTurn must be called before promiseTurn may be called");
        }
        if (this.finalized) {
            throw new Error("cannot modify turn on finalized game");
        }
        const turn = this.turns[this.turns.length - 1];
        turn.promise = new Uint8Array(promise);
    }
    async confirmTurn(exchangeDataAsync) {
        if (!this.turns || !this.turns.length) {
            throw new Error("init, setDeck, and beginTurn must be called before confirmTurn may be called");
        }
        if (this.finalized) {
            throw new Error("cannot modify turn on finalized game");
        }
        const turn = this.turns[this.turns.length - 1];
        if (!turn.promise) {
            throw new Error("promiseTurn must be called before confirmTurn");
        }
        const confirmBuf = await doExchangeData(SyncPoint.ConfirmTurn, this.turns.length, exchangeDataAsync, turn[this.player === 2 ? "confirm2" : "confirm1"]);
        if (confirmBuf.length < this.randLength) {
            throw new Error("received turn confirmation buffer that is too short");
        }
        const confirmHashBuf = new Uint8Array(this.seed.length + confirmBuf.length);
        confirmHashBuf.set(this.seed, 0);
        confirmHashBuf.set(confirmBuf, this.seed.length);
        const confirmHash = new Uint8Array(await crypto.subtle.digest(this.hash, confirmHashBuf), 0);
        if (!confirmHash.every((b, i) => b === turn.promise[i])) {
            throw new Error("turn confirmation validation failed (implementation error or possible cheating)");
        }
        turn.confirm2 = confirmBuf;
        turn.seed2.set(confirmBuf.subarray(0, this.randLength), this.player === 2 ? 0 : this.randLength);
        await this.whenConfirmedTurn(turn.seed2);
        return confirmBuf.slice(this.randLength);
    }
    async whenConfirmedTurn(seed2) {
        this.sharedRandBuffer.set(seed2, 32 + this.seed.length);
        this.sharedRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.sharedRandBuffer.subarray(32)));
        this.sharedRandIdx = 0;
    }
    async finalize(exchangeDataAsync, verifyDeckAsync, verifyTurnAsync) {
        if (this.finalized) {
            throw new Error("finalize already called");
        }
        if (!this.turns) {
            throw new Error("finalize called on uninitialized SecureCardGame instance");
        }
        this.finalized = true;
        const sendData = new Uint8Array(this.randLength + this.localDeckInitial.byteLength);
        sendData.set(this.localRandSeed, 0);
        sendData.set(new Uint8Array(this.localDeckInitial.buffer, 0), this.randLength);
        const recvData = await doExchangeData(SyncPoint.Finalize, 0, exchangeDataAsync, sendData);
        if (!(recvData instanceof Uint8Array) || recvData.length < this.randLength || ((recvData.length - this.randLength) % this.localDeckInitial.BYTES_PER_ELEMENT) !== 0) {
            throw new Error("finalizing match: expected to receive at least " + this.randLength + " bytes of data, but received " + recvData.length + " bytes");
        }
        this.remoteRandSeed = new Uint8Array(recvData.buffer, 0, this.randLength);
        this.remoteDeckInitial = new Uint8Array(recvData.buffer, this.randLength);
        await verifyDeckAsync(this.remoteDeckInitial, this.remoteCardBacks);
        for (let turn of this.turns) {
            await this.initTurnSeed(turn.seed, this.localRandSeed, this.remoteRandSeed);
            await verifyTurnAsync(turn);
        }
    }
    async initTurnSeed(turnSeed, localSeed, remoteSeed) {
        if (!this.sharedRandBuffer) {
            this.sharedRandBuffer = new Uint8Array(32 + this.seed.length + this.randLength * 2);
            this.sharedRandBuffer.set(this.seed, 32);
        }
        this.sharedRandBuffer.set(turnSeed, 32 + this.seed.length);
        this.sharedRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.sharedRandBuffer.subarray(32)));
        this.sharedRandIdx = 0;
        const compat274 = this.compat && this.compat[0] === 0 && this.compat[1] === 2 && this.compat[2] <= 74;
        if (compat274) {
            if (!this.localRandBuffer) {
                this.localRandBuffer = new Uint8Array(32 + this.randLength);
            }
            this.localRandBuffer.set(this.sharedRandState, 0);
            this.localRandBuffer.set(localSeed, 32);
            this.localRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.localRandBuffer));
            this.localRandIdx = 0;
            if (remoteSeed) {
                this.localRandBuffer.set(remoteSeed, 32);
                this.remoteRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.localRandBuffer));
                this.remoteRandIdx = 0;
            }
            return;
        }
        if (!this.localRandBuffer) {
            this.localRandBuffer = new Uint8Array(32 + this.seed.length + this.randLength * 3);
            this.localRandBuffer.set(this.seed, 32);
        }
        this.localRandBuffer.set(turnSeed, 32 + this.seed.length);
        this.localRandBuffer.set(localSeed, 32 + this.seed.length + turnSeed.length);
        this.localRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.localRandBuffer.subarray(32)));
        this.localRandIdx = 0;
        if (remoteSeed) {
            if (!this.remoteRandBuffer) {
                this.remoteRandBuffer = new Uint8Array(32 + this.seed.length + this.randLength * 3);
                this.remoteRandBuffer.set(this.seed, 32);
            }
            this.remoteRandBuffer.set(turnSeed, 32 + this.seed.length);
            this.remoteRandBuffer.set(remoteSeed, 32 + this.seed.length + turnSeed.length);
            this.remoteRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.remoteRandBuffer.subarray(32)));
            this.remoteRandIdx = 0;
        }
    }
    async rand(shared, max) {
        if (!this.turns || !this.turns.length) {
            throw new Error("rand cannot be called before a turn is started");
        }
        if (max < 1) {
            throw new Error("rand cannot generate a number between 0 and " + (max - 1));
        }
        if (max !== (max | 0)) {
            throw new Error("rand max must be an integer");
        }
        let numBytes = 0;
        for (let i = max; i; i = i >> 8) {
            numBytes++;
        }
        const compat274 = this.compat && this.compat[0] === 0 && this.compat[1] === 2 && this.compat[2] <= 74;
        let maxRawValue = max;
        while (maxRawValue + max <= (compat274 ? (numBytes << 3) : (1 << (numBytes << 3)))) {
            maxRawValue += max;
        }
        let rawValue;
        do {
            rawValue = await this.advanceRNGMulti(shared, numBytes);
        } while (compat274 ? rawValue > maxRawValue : rawValue >= maxRawValue);
        return rawValue % max;
    }
    async advanceRNG(shared) {
        if (shared === 2) {
            let idx = this.remoteRandIdx;
            if (idx >= this.remoteRandState.length) {
                idx = 0;
                this.remoteRandState = await this.nextRandState(this.remoteRandState, this.remoteRandBuffer);
            }
            this.remoteRandIdx = idx + 1;
            this.totalAdvanceCount[2]++;
            return this.remoteRandState[idx];
        }
        else if (shared) {
            let idx = this.sharedRandIdx;
            if (idx >= this.sharedRandState.length) {
                idx = 0;
                this.sharedRandState = await this.nextRandState(this.sharedRandState, this.sharedRandBuffer);
            }
            this.sharedRandIdx = idx + 1;
            this.totalAdvanceCount[1]++;
            return this.sharedRandState[idx];
        }
        else {
            let idx = this.localRandIdx;
            if (idx >= this.localRandState.length) {
                idx = 0;
                this.localRandState = await this.nextRandState(this.localRandState, this.localRandBuffer);
            }
            this.localRandIdx = idx + 1;
            this.totalAdvanceCount[0]++;
            return this.localRandState[idx];
        }
    }
    async advanceRNGMulti(shared, count) {
        let val = 0;
        for (let i = 0; i < count; i++) {
            val = val | (await this.advanceRNG(shared) << (i << 3));
        }
        return val;
    }
    async nextRandState(currentState, updateBuffer) {
        const compat274 = this.compat && this.compat[0] === 0 && this.compat[1] === 2 && this.compat[2] <= 74;
        if (compat274) {
            updateBuffer = new Uint8Array(this.seed.length + currentState.length);
            updateBuffer.set(this.seed, 0);
            updateBuffer.set(currentState, this.seed.length);
        }
        else {
            updateBuffer.set(currentState, 0);
        }
        return new Uint8Array(await crypto.subtle.digest(this.hash, updateBuffer));
    }
    async shuffle(shared, items) {
        for (let i = 1; i < items.length; i++) {
            const j = await this.rand(shared, i + 1);
            const tmp = items[i];
            items[i] = items[j];
            items[j] = tmp;
        }
    }
    async publicShuffle(cards, cardBacks) {
        if (cards.length !== cardBacks.length) {
            throw new Error("cards and cardBacks must have the same length");
        }
        for (let i = 1; i < cards.length; i++) {
            const j = await this.rand(true, i + 1);
            const tmp = cards[i];
            cards[i] = cards[j];
            cards[j] = tmp;
            const tmp2 = cardBacks[i];
            cardBacks[i] = cardBacks[j];
            cardBacks[j] = tmp2;
        }
    }
    async privateShuffle(cards, cardBacks, remote) {
        if (cards.length !== cardBacks.length) {
            throw new Error("cards and cardBacks must have the same length");
        }
        const cardArrays = [];
        const byBack = {};
        for (let i = 0; i < cards.length; i++) {
            if (!byBack[cardBacks[i]]) {
                cardArrays.push(byBack[cardBacks[i]] = []);
            }
            byBack[cardBacks[i]].push(cards[i]);
        }
        await this.shuffle(true, cardBacks);
        for (let arr of cardArrays) {
            await this.shuffle(remote ? 2 : false, arr);
        }
        for (let i = 0; i < cards.length; i++) {
            cards[i] = byBack[cardBacks[i]].shift();
        }
    }
}
"use strict";
var SpyCards;
(function (SpyCards) {
    var SpoilerGuard;
    (function (SpoilerGuard) {
        const cardEnemyIDs = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15, 16, 17, 19, 20, 21, 23, 24, 25,
            26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
            44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 61, 63, 64,
            65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82,
            83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98
        ];
        const encCards = {
            "ȮȬ": 0, "ȭȭ": 1, "ȭȬ": 2, "ȯ": 3,
            "ȫȭ": 4, "ȫȬ": 5, "Ȯ": 6, "ȫȩ": 7,
            "Ȯȧ": 8, "Ȫȧ": 9, "Ȯȯ": 10, "Ȭȭ": 11,
            "ȮȮ": 12, "Ȯȭ": 13, "ȭȫ": 14, "ȨȮ": 15,
            "ȭȦ": 16, "ȭȨ": 17, "ȫȨ": 18, "ȫȦ": 19,
            "ȫȧ": 20, "ȪȦ": 21, "ȪȮ": 22, "Ȫȭ": 23,
            "ȨȬ": 24, "Ȩȭ": 25, "Ȩȯ": 26, "ȩȨ": 27,
            "ȩȧ": 28, "ȬȮ": 29, "ȭȯ": 30, "ȮȪ": 31,
            "Ȧ": 32, "ȧȦ": 33, "ȧȧ": 34, "ȧ": 35,
            "Ȭȯ": 36, "Ȫȩ": 37, "Ȯȫ": 38, "ȩȦ": 39,
            "ȭȮ": 40, "ȩȭ": 41, "Ȯȩ": 42, "ȭȪ": 43,
            "Ȭȫ": 44, "ȪȬ": 45, "Ȫȫ": 46, "ȪȪ": 47,
            "ȧȩ": 48, "ȧȨ": 49, "ȮȨ": 50, "ȬȬ": 0,
            "ȩ": 1, "Ȫ": 2, "ȫ": 3, "ȬȨ": 4,
            "Ȭȧ": 5, "ȩȪ": 6, "ȩȬ": 7, "ȩȫ": 8,
            "ȩȯ": 9, "Ȩȫ": 10, "ȨȪ": 11, "Ȩ": 12,
            "ȮȦ": 13, "ȫȮ": 14, "ȭȧ": 15, "ȧȫ": 16,
            "ȧȮ": 17, "ȧȭ": 18, "Ȩȩ": 19, "ȨȨ": 20,
            "Ȩȧ": 21, "ȭ": 0, "Ȭ": 1, "ȬȪ": 2,
            "ȫȫ": 3, "ȪȨ": 4, "ȩȮ": 5, "ȭȩ": 6,
            "Ȭȩ": 7, "ȧȬ": 8, "ȫȯ": 9, "ȬȦ": 10,
            "ȫȪ": 11, "ȧȪ": 12, "ȩȩ": 13, "ȧȯ": 14,
            "Ȫȯ": 15, "ȨȦ": 16
        };
        const sep0 = "ȳ";
        const sep1 = "ɟ";
        const sep2 = "ȕ";
        const sep3 = "ɣɌɏɓɖɋɣ";
        const nilval = "ȯ";
        const questID = "ȩ";
        const prDeny = [2, 5, 21, 27, 32, 37, 47, 53, 75, 76, 122, 127, 146, 162];
        let prtImg = null;
        const menu = [
            {
                high: 1381321031,
                low: 1162149888,
                flag: 1,
                shift: 16,
            },
            {
                high: 1212240452,
                low: 1163088896,
                flag: 2,
                shift: 8,
            },
            {
                high: 1179795789,
                low: 1162825285,
                flag: 4,
                shift: 0,
            },
            {
                high: 1347769160,
                low: 1380926283,
                flag: 8,
                shift: 0,
            },
            {
                high: 1297044037,
                low: 1178686029,
                flag: 16,
                shift: 0,
            },
            {
                high: 1297699668,
                low: 1163024703,
                flag: 32,
                shift: 0,
            },
            {
                high: 1414874694,
                low: 1112885075,
                flag: 23,
                shift: 0,
            },
        ];
        const menuHist = { high: 0, low: 0 };
        addEventListener("keydown", (e) => {
            const key = (e.key || "").toUpperCase();
            if (key.length !== 1) {
                return;
            }
            menuHist.high = (menuHist.high << 8) | (menuHist.low >> 24);
            menuHist.low = (menuHist.low << 8) | (key.charCodeAt(0) & 0xff);
            for (let { high, low, flag, shift } of menu) {
                const effHigh = (menuHist.high << shift) | (shift ? menuHist.low >> (32 - shift) : 0);
                const effLow = (menuHist.low << shift);
                if (high === effHigh && low === effLow) {
                    let data = getSpoilerGuardData();
                    if (!data) {
                        data = {
                            q: 2,
                            t: true,
                            a: true,
                            d: null,
                            s: Base64.encode(new Uint8Array(32).map(() => -1)),
                        };
                    }
                    data.m = data.m || 0;
                    if ((data.m & flag) !== flag) {
                        data.m |= flag;
                        SpyCards.Audio.startFetchAudio();
                        SpyCards.Audio.Sounds.AtkSuccess.play();
                        localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(data);
                    }
                }
            }
        });
        function flagMap(s) {
            return s.split(sep0).map((b) => !(b.length & 1));
        }
        function getSpoilerGuardData() {
            const val = localStorage["spy-cards-spoiler-guard-v0"];
            return val ? JSON.parse(val) : null;
        }
        SpoilerGuard.getSpoilerGuardData = getSpoilerGuardData;
        let GuardState;
        (function (GuardState) {
            GuardState[GuardState["QuestLocked"] = 0] = "QuestLocked";
            GuardState[GuardState["QuestNotAccepted"] = 1] = "QuestNotAccepted";
            GuardState[GuardState["QuestNotCompleted"] = 2] = "QuestNotCompleted";
            GuardState[GuardState["NotMetCarmina"] = 3] = "NotMetCarmina";
            GuardState[GuardState["CardsNotApproved"] = 4] = "CardsNotApproved";
            GuardState[GuardState["NotAllSeen"] = 5] = "NotAllSeen";
            GuardState[GuardState["NotAllSpied"] = 6] = "NotAllSpied";
            GuardState[GuardState["Disabled"] = 7] = "Disabled";
        })(GuardState = SpoilerGuard.GuardState || (SpoilerGuard.GuardState = {}));
        const cannotPlayState = {
            [GuardState.QuestLocked]: true,
            [GuardState.QuestNotAccepted]: true,
            [GuardState.QuestNotCompleted]: true,
            [GuardState.NotMetCarmina]: true,
            [GuardState.CardsNotApproved]: true
        };
        function getSpoilerGuardState() {
            const save = getSpoilerGuardData();
            if (!save) {
                return GuardState.Disabled;
            }
            if (save.q !== 2) {
                return [
                    GuardState.QuestLocked,
                    GuardState.QuestNotAccepted,
                    GuardState.QuestNotCompleted
                ][save.q + 1];
            }
            if (!save.t) {
                return GuardState.NotMetCarmina;
            }
            if (!save.a) {
                return GuardState.CardsNotApproved;
            }
            let seenAllEnemies = true;
            let spiedAllEnemies = true;
            const enemyBitmap = Base64.decode(save.s);
            for (let card of cardEnemyIDs) {
                const i = card >> 2;
                const j = (card & 3) << 1;
                if (!(enemyBitmap[i] & (1 << j))) {
                    seenAllEnemies = false;
                }
                if (!(enemyBitmap[i] & (2 << j))) {
                    spiedAllEnemies = false;
                }
            }
            if (!seenAllEnemies) {
                return GuardState.NotAllSeen;
            }
            if (!spiedAllEnemies) {
                return GuardState.NotAllSpied;
            }
            return GuardState.Disabled;
        }
        SpoilerGuard.getSpoilerGuardState = getSpoilerGuardState;
        async function banSpoilerCards(defs, remoteData) {
            const localGuard = getSpoilerGuardData();
            const localData = Base64.decode(localGuard ? localGuard.s : "");
            for (let card of cardEnemyIDs) {
                const i = card >> 2;
                const j = (card & 3) << 1;
                if (localData.length) {
                    if ((localData[i] & (3 << j)) === 1) {
                        defs.unpickable[card] = true;
                    }
                }
                const local = localData.length ? localData[i] : 255;
                const remote = remoteData.length ? remoteData[i] : 255;
                if (!(local & remote & (1 << j))) {
                    defs.banned[card] = true;
                }
            }
            if (defs.pr || !localGuard || !localGuard.m) {
                return;
            }
            if (localGuard.m & 8) {
                if (!prtImg) {
                    prtImg = fetch(user_image_base_url + "prt.json", { mode: "cors" }).then((r) => r.json());
                }
                defs.pr = true;
                for (let card of defs.allCards) {
                    const portrait = card.portrait;
                    if (portrait === 88 || (portrait === 255 && (await prtImg).indexOf(CrockfordBase32.encode(card.customPortrait)) !== -1)) {
                        card.portrait = 17;
                        card.customPortrait = null;
                    }
                    else if ((portrait === 255 && (await prtImg).indexOf("!" + CrockfordBase32.encode(card.customPortrait)) !== -1)) {
                        // external prDeny
                    }
                    else if (prDeny.indexOf(portrait) === -1) {
                        const id = "FTCZKAQ95KFP818";
                        card.portrait = 255;
                        card.customPortrait = CrockfordBase32.decode(id);
                    }
                }
            }
            if (localGuard.m & 32) {
                for (let byBack of defs.cardsByBack) {
                    for (let i = 1; i < byBack.length; i++) {
                        const t = byBack[i];
                        const j = Math.floor(Math.random() * (i + 1));
                        byBack[i] = byBack[j];
                        byBack[j] = t;
                    }
                }
            }
        }
        SpoilerGuard.banSpoilerCards = banSpoilerCards;
        async function surveySaveData(form) {
            async function ask(question, options) {
                const p = document.createElement("p");
                p.textContent = question;
                form.appendChild(p);
                const buttons = document.createElement("div");
                buttons.classList.add("buttons");
                form.appendChild(buttons);
                const optionPromises = options.map((opt, i) => {
                    return new Promise((resolve) => {
                        const btn = SpyCards.UI.button(opt, [], () => resolve(i));
                        buttons.appendChild(btn);
                    });
                });
                const response = await Promise.race(optionPromises);
                SpyCards.UI.remove(p);
                SpyCards.UI.remove(buttons);
                return response;
            }
            if (!await ask("Which do you want to do?", ["Upload Save File", "Answer Some Questions"])) {
                return null;
            }
            const chapterNumber = await ask("What is your current chapter number?", ["1", "2", "3", "4", "5", "6", "7"]) + 1;
            const questCompleted = chapterNumber > 2 && !await ask("Have you completed the sidequest \"Requesting Assistance\"?", ["Yes", "No"]);
            const enemyData = new Uint8Array(256 / 8);
            function seenSpied(id) {
                enemyData[id >> 2] |= 3 << ((id & 3) << 1);
            }
            if (questCompleted) {
                seenSpied(31); // Monsieur Scarlet
            }
            switch (chapterNumber) {
                case 7:
                    seenSpied(87); // Dead Lander α
                    seenSpied(88); // Dead Lander β
                    seenSpied(89); // Dead Lander γ
                    if (chapterNumber > 7 || (await ask("Which is the furthest area you have reached?", ["Dead Lands", "The Machine", "Sapling Plains"]) === 2 && !await ask("Has the sapling been destroyed?", ["Yes", "No"]))) {
                        seenSpied(90); // Wasp King
                        seenSpied(91); // The Everlasting King
                        if (chapterNumber > 7 || !await ask("Have you fought Team Maki? Answer yes even if you lost.", ["Yes", "No"])) {
                            seenSpied(92); // Maki
                            seenSpied(93); // Kina
                            seenSpied(94); // Yin
                        }
                    }
                // fallthrough
                case 6:
                    if (chapterNumber > 6 || !await ask("Have you crossed the Forsaken Lands?", ["Yes", "No"])) {
                        seenSpied(37); // Plumpling
                        seenSpied(64); // Mimic Spider
                        seenSpied(78); // Mothfly
                        seenSpied(79); // Mothfly Cluster
                        seenSpied(80); // Ironnail
                        if (chapterNumber > 6 || !await ask("Do you have the boat?", ["Yes", "No"])) {
                            seenSpied(74); // Cross
                            seenSpied(75); // Poi
                            seenSpied(76); // Primal Weevil
                            if (chapterNumber > 6 || !await ask("Have you reached Rubber Prison?", ["Yes", "No"])) {
                                seenSpied(82); // Ruffian
                                if (chapterNumber > 6) {
                                    seenSpied(95); // ULTIMAX Tank
                                }
                            }
                        }
                    }
                // fallthrough
                case 5:
                    if (chapterNumber > 5 || !await ask("Have you reached the swamp?", ["Yes", "No"])) {
                        seenSpied(38); // Flowerling
                        seenSpied(63); // Jumping Spider
                        seenSpied(71); // Mantidfly
                        seenSpied(73); // Wild Chomper
                        if (chapterNumber > 5 || !await ask("Have you reached the Wasp Kingdom?", ["Yes", "No"])) {
                            seenSpied(65); // Leafbug Ninja
                            seenSpied(66); // Leafbug Archer
                            seenSpied(67); // Leafbug Clubber
                            seenSpied(68); // Madesphy
                            seenSpied(69); // The Beast
                            if (chapterNumber > 5) {
                                seenSpied(26); // Wasp Bomber
                                seenSpied(27); // Wasp Driller
                                seenSpied(72); // General Ultimax
                                seenSpied(97); // Riz
                            }
                        }
                    }
                    if (!await ask("Have you discovered a use for the gem dropped by The Watcher?", ["Yes", "No"])) {
                        seenSpied(52); // Zombee
                        seenSpied(53); // Zombeetle
                        seenSpied(56); // Bloatshroom
                        seenSpied(96); // Zommoth
                    }
                // feedback
                case 4:
                    if (chapterNumber > 4 || !await ask("Have you obtained the Earth Key?", ["Yes", "No"])) {
                        seenSpied(40); // Astotheles
                        if (chapterNumber > 4 || !await ask("Have you reached the sand castle?", ["Yes", "No"])) {
                            seenSpied(49); // Dune Scorpion
                            seenSpied(81); // Belostoss
                            seenSpied(83); // Water Strider
                            seenSpied(84); // Diving Spider
                            if (chapterNumber > 4 || !await ask("Have you obtained the fourth artifact?", ["Yes", "No"])) {
                                seenSpied(54); // The Watcher
                                seenSpied(57); // Krawler
                                seenSpied(58); // Haunted Cloth
                                seenSpied(61); // Warden
                                if (chapterNumber > 4) {
                                    seenSpied(23); // Kabbu
                                    seenSpied(51); // Kali
                                    seenSpied(85); // Cenn
                                    seenSpied(86); // Pisci
                                }
                            }
                            if (!await ask("Have you discovered a use for the machine in Professor Honeycomb's office?", ["Yes", "No"])) {
                                seenSpied(41); // Mother Chomper
                                seenSpied(70); // Chomper Brute
                            }
                        }
                    }
                // fallthrough
                case 3:
                    if (chapterNumber > 3 || !await ask("Have you reached Defiant Root?", ["Yes", "No"])) {
                        seenSpied(4); // Cactiling
                        seenSpied(5); // Psicorp
                        seenSpied(6); // Thief
                        seenSpied(7); // Bandit
                        seenSpied(28); // Wasp Scout
                        seenSpied(33); // Arrow Worm
                        seenSpied(39); // Burglar
                        if (chapterNumber > 3 || !await ask("Have you found the Overseer?", ["Yes", "No"])) {
                            seenSpied(42); // Ahoneynation
                            seenSpied(43); // Bee-Boop
                            seenSpied(44); // Security Turret
                            seenSpied(45); // Denmuki
                            seenSpied(48); // Abomihoney
                            if (chapterNumber > 3) {
                                seenSpied(46); // Heavy Drone B-33
                                seenSpied(34); // Carmina
                                seenSpied(36); // Broodmother
                                seenSpied(47); // Mender
                            }
                        }
                    }
                // fallthrough
                case 2:
                    if (chapterNumber > 2 || !await ask("Have you reached Golden Settlement?", ["Yes", "No"])) {
                        seenSpied(14); // Numbnail
                        seenSpied(16); // Acornling
                        seenSpied(17); // Weevil
                        seenSpied(20); // Chomper
                        seenSpied(25); // Wasp Trooper
                        seenSpied(29); // Midge
                        seenSpied(30); // Underling
                        seenSpied(32); // Golden Seedling
                        if (chapterNumber > 2 || !await ask("Have you gained passage to Golden Hills?", ["Yes", "No"])) {
                            seenSpied(19); // Venus' Bud
                            seenSpied(21); // Acolyte Aria
                            if (chapterNumber > 2) {
                                seenSpied(3); // Zasp
                                seenSpied(15); // Mothiva
                                seenSpied(24); // Venus' Guardian
                            }
                        }
                    }
                // fallthrough
                case 1:
                    seenSpied(0); // Zombiant
                    seenSpied(1); // Jellyshroom
                    seenSpied(2); // Spider
                    seenSpied(8); // Inichas
                    seenSpied(9); // Seedling
            }
            if (chapterNumber >= 3 && !await ask("Have you completed the following bounty: Devourer", ["Yes", "No"])) {
                seenSpied(98);
            }
            if (chapterNumber >= 4 && !await ask("Have you completed the following bounty: Tidal Wyrm", ["Yes", "No"])) {
                seenSpied(50);
            }
            if (chapterNumber >= 5 && !await ask("Have you completed the following bounty: Seedling King", ["Yes", "No"])) {
                seenSpied(35);
            }
            if (chapterNumber >= 6 && !await ask("Have you completed the following bounty: False Monarch", ["Yes", "No"])) {
                seenSpied(77);
            }
            if (chapterNumber >= 6 && enemyData[76 >> 2] & (1 << (76 & 3)) && !await ask("Have you completed the following bounty: Peacock Spider", ["Yes", "No"])) {
                seenSpied(55);
            }
            return {
                q: chapterNumber <= 2 ? -1 : questCompleted ? 2 : 0,
                t: questCompleted,
                a: questCompleted,
                d: null,
                s: Base64.encode(enemyData),
                m: null
            };
        }
        SpoilerGuard.surveySaveData = surveySaveData;
        function parseSaveData(data) {
            // to avoid giving away how saves are encoded,
            // we're not actually decoding the save,
            // just grabbing the specific data we need.
            const sections = data.split(sep2);
            const flags = flagMap(sections[11]);
            const modes0 = [613, 614, 615, 616, 656, 681];
            const modes1 = modes0.map((n, i) => flags[n] ? 1 << i : 0);
            const modes2 = modes1.reduce((x, y) => x | y, 0);
            const questType = sections[5].split(sep1).map((q) => q.split(sep0)).findIndex((q) => q.indexOf(questID) !== -1);
            const deck0 = sections[12].split(sep3)[12];
            const deck1 = deck0.split(sep0).map((e) => encCards[e]);
            const deck2 = (deck1.length !== 15 || deck1.some((c) => typeof c !== "number")) ? null : new Uint8Array(11);
            if (deck2) {
                deck2[0] = (deck1[0] << 2) | (deck1[1] >> 3);
                deck2[1] = (deck1[1] << 5) | deck1[2];
                for (let i = 2, j = 3; j < deck1.length; i += 3, j += 4) {
                    deck2[i] = (deck1[j] << 2) | (deck1[j + 1] >> 4);
                    deck2[i + 1] = (deck1[j + 1] << 4) | (deck1[j + 2] >> 2);
                    deck2[i + 2] = (deck1[j + 2] << 6) | deck1[j + 3];
                }
            }
            const spyData = flagMap(sections[10].split(sep1)[1]);
            const enemyData0 = sections[17];
            const enemyData1 = enemyData0.split(sep1);
            const enemyData2 = enemyData1.map((e) => e.split(sep0));
            const enemyData3 = enemyData2.map((e) => e.map((c) => c !== nilval));
            const enemyData4 = new Uint8Array(256 / 8);
            let i = 0, b = 0;
            for (let j = 0; j < 128; j++) {
                if (enemyData3[j][0]) {
                    enemyData4[i] |= 1 << b;
                }
                b++;
                if (spyData[j]) {
                    enemyData4[i] |= 1 << b;
                }
                b++;
                if (b >= 8) {
                    i++;
                    b = 0;
                }
            }
            if (b || i !== enemyData4.length) {
                throw new Error("invalid data");
            }
            return {
                q: questType,
                t: flags[236],
                a: flags[237],
                d: deck2 ? CrockfordBase32.encode(deck2) : null,
                s: Base64.encode(enemyData4),
                m: modes2
            };
        }
        SpoilerGuard.parseSaveData = parseSaveData;
        function requestDataFile() {
            const upload = document.createElement("input");
            upload.type = "file";
            upload.accept = ".dat";
            const filePromise = new Promise((resolve, reject) => {
                upload.addEventListener("input", function (e) {
                    if (upload.files.length) {
                        resolve(upload.files[0]);
                    }
                    else {
                        reject(new Error("no file selected"));
                    }
                });
            });
            upload.click();
            return filePromise;
        }
        async function onFile(f) {
            if (["save0.dat", "save1.dat", "save2.dat"].indexOf(f.name) === -1) {
                // TODO: confirm user intended this file
                debugger;
            }
            const data = await f.text();
            const parsed = parseSaveData(data);
            localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(parsed);
            updateSpoilerGuardState();
        }
        const enableButtons = document.querySelectorAll(".enable-spoiler-guard");
        if (enableButtons.length) {
            document.addEventListener("dragover", function (e) {
                e.preventDefault();
                e.dataTransfer.dropEffect = "copy";
            });
            document.addEventListener("drop", function (e) {
                e.preventDefault();
                onFile(e.dataTransfer.files[0]);
            });
            enableButtons.forEach((btn) => {
                btn.addEventListener("click", function (e) {
                    e.preventDefault();
                    const form = document.createElement("div");
                    form.classList.add("readme", "spoiler-guard-form");
                    form.setAttribute("role", "form");
                    form.setAttribute("aria-live", "assertive");
                    document.body.appendChild(form);
                    surveySaveData(form).then((surveyData) => {
                        SpyCards.UI.remove(form);
                        if (surveyData === null) {
                            requestDataFile().then((f) => onFile(f));
                        }
                        else {
                            localStorage["spy-cards-spoiler-guard-v0"] = JSON.stringify(surveyData);
                            updateSpoilerGuardState();
                        }
                    });
                });
            });
        }
        const disableButton = document.querySelector(".disable-spoiler-guard");
        if (disableButton) {
            disableButton.addEventListener("click", function (e) {
                e.preventDefault();
                delete localStorage["spy-cards-spoiler-guard-v0"];
                updateSpoilerGuardState();
            });
        }
        else if (cannotPlayState[getSpoilerGuardState()]) {
            location.href = "spoiler-guard.html";
        }
        function updateSpoilerGuardState() {
            const data = getSpoilerGuardData();
            const isEnabled = data !== null;
            const state = getSpoilerGuardState();
            SpyCards.UI.html.classList.toggle("room-pr", data && data.m && (data.m & 8) !== 0);
            document.querySelectorAll(".spoiler-guard-disabled, .spoiler-guard-enabled, [data-spoiler-guard-state]").forEach((el) => {
                el.hidden = false;
                if (el.classList.contains("spoiler-guard-disabled")) {
                    el.hidden = isEnabled;
                }
                if (el.classList.contains("spoiler-guard-enabled")) {
                    el.hidden = !isEnabled;
                }
                if (el.hidden) {
                    return;
                }
                const stateRaw = el.getAttribute("data-spoiler-guard-state");
                if (!stateRaw) {
                    return;
                }
                const expectedState = parseInt(stateRaw, 10);
                el.hidden = expectedState !== state;
            });
            document.querySelectorAll(".spoiler-guard-deck").forEach((el) => {
                if (!data || !data.d) {
                    el.hidden = true;
                    return;
                }
                el.hidden = false;
                el.querySelectorAll("a").forEach((a) => {
                    a.href = "decks.html#" + data.d;
                });
            });
        }
        setTimeout(updateSpoilerGuardState, 1);
    })(SpoilerGuard = SpyCards.SpoilerGuard || (SpyCards.SpoilerGuard = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var SpyCards;
(function (SpyCards) {
    var UI;
    (function (UI) {
        UI.html = document.querySelector("html");
        let nextUniqueID = 0;
        function uniqueID() {
            return "_tmp_" + (nextUniqueID++);
        }
        UI.uniqueID = uniqueID;
        function replace(from, to) {
            if (from && from.parentNode && to) {
                from.parentNode.insertBefore(to, from);
                from.parentNode.removeChild(from);
            }
            else {
                remove(from);
                remove(to);
            }
        }
        UI.replace = replace;
        function remove(el) {
            if (el && el.parentNode) {
                el.parentNode.removeChild(el);
            }
        }
        UI.remove = remove;
        function clear(el) {
            if (!el) {
                return;
            }
            el.textContent = "";
        }
        UI.clear = clear;
        function button(label, classes, click) {
            const btn = document.createElement("button");
            btn.classList.add(...classes);
            btn.textContent = label;
            btn.addEventListener("click", (e) => {
                e.preventDefault();
                click();
            });
            return btn;
        }
        UI.button = button;
        function option(label, value) {
            const opt = document.createElement("option");
            opt.textContent = label;
            opt.value = value;
            return opt;
        }
        UI.option = option;
    })(UI = SpyCards.UI || (SpyCards.UI = {}));
})(SpyCards || (SpyCards = {}));
"use strict";
var disableDoSquish = false;
function doSquish(el) {
    if (disableDoSquish) {
        return;
    }
    el.querySelectorAll(".squish").forEach((s) => {
        const max = parseInt(getComputedStyle(s.parentElement).width.replace("px", ""), 10);
        s.style.transform = "";
        const current = s.clientWidth;
        if (current > max) {
            s.style.transform = "scaleX(calc(" + max + " / " + current + "))";
        }
    });
    el.querySelectorAll(".squishy").forEach((s) => {
        const max = parseInt(getComputedStyle(s.parentElement).height.replace("px", ""), 10);
        s.style.transform = "";
        const current = s.clientHeight;
        if (current > max) {
            s.style.transform = "scaleY(calc(" + max + " / " + current + "))";
        }
    });
}
addEventListener("load", function () {
    doSquish(document);
});
function sleep(ms) {
    return new Promise(function (resolve) {
        setTimeout(function () {
            resolve();
        }, ms);
    });
}
var SpyCards;
(function (SpyCards) {
    var Binary;
    (function (Binary) {
        // JavaScript bitwise operators work on signed 32-bit integers.
        // These helper functions work around this.
        function bit(n) {
            let base = 1;
            while (n > 30) {
                base *= 1 << 30;
                n -= 30;
            }
            return (1 << n) * base;
        }
        Binary.bit = bit;
        function getBit(x, n) {
            while (n > 30) {
                x /= 1 << 30;
                n -= 30;
            }
            return ((x >> n) & 1);
        }
        Binary.getBit = getBit;
        function setBit(x, n, b) {
            const ob = getBit(x, n);
            if (b === ob) {
                return x;
            }
            return b ? x + bit(n) : x - bit(n);
        }
        Binary.setBit = setBit;
        function pushUVarInt(buf, x) {
            while (x >= 0x80) {
                buf.push(x & 0xff | 0x80);
                x /= 1 << 7;
            }
            buf.push(x | 0);
        }
        Binary.pushUVarInt = pushUVarInt;
        function pushSVarInt(buf, x) {
            let ux = x * 2;
            if (x < 0) {
                ux = -ux + 1;
            }
            pushUVarInt(buf, ux);
        }
        Binary.pushSVarInt = pushSVarInt;
        function shiftUVarInt(buf) {
            let x = 0, s = 0;
            while (buf.length) {
                const b = buf.shift();
                x += (b & 0x7f) * Math.pow(2, s);
                s += 7;
                if (b < 0x80) {
                    return x;
                }
            }
            throw new Error("reached end of buffer");
        }
        Binary.shiftUVarInt = shiftUVarInt;
        function shiftSVarInt(buf) {
            const ux = shiftUVarInt(buf);
            let x = ux / 1;
            if (ux & 1) {
                x = -x - 0.5;
            }
            return x;
        }
        Binary.shiftSVarInt = shiftSVarInt;
        function pushUTF8String1(buf, s) {
            const b = new TextEncoder().encode(s || "");
            if (b.length > 255) {
                throw new Error("string length (" + b.length + " bytes) longer than maximum (255)");
            }
            buf.push(b.length);
            buf.push(...SpyCards.toArray(b));
        }
        Binary.pushUTF8String1 = pushUTF8String1;
        function shiftUTF8String1(buf) {
            const length = buf.shift();
            if (buf.length < length) {
                throw new Error("buffer too short to hold " + length + "-byte string");
            }
            const b = new Uint8Array(buf.splice(0, length));
            return new TextDecoder().decode(b);
        }
        Binary.shiftUTF8String1 = shiftUTF8String1;
        function pushUTF8StringVar(buf, s) {
            const b = new TextEncoder().encode(s || "");
            pushUVarInt(buf, b.length);
            buf.push(...SpyCards.toArray(b));
        }
        Binary.pushUTF8StringVar = pushUTF8StringVar;
        function shiftUTF8StringVar(buf) {
            const length = shiftUVarInt(buf);
            if (buf.length < length) {
                throw new Error("buffer too short to hold " + length + "-byte string");
            }
            const b = new Uint8Array(buf.splice(0, length));
            return new TextDecoder().decode(b);
        }
        Binary.shiftUTF8StringVar = shiftUTF8StringVar;
    })(Binary = SpyCards.Binary || (SpyCards.Binary = {}));
})(SpyCards || (SpyCards = {}));
(function (SpyCards) {
    async function fetchCustomCardSet(name, revision) {
        const url = revision ?
            (custom_card_api_base_url + "get-revision/" + encodeURIComponent(name) + "/" + encodeURIComponent(revision)) :
            (custom_card_api_base_url + "latest/" + encodeURIComponent(name));
        const response = await fetch(url, { mode: "cors" });
        if (response.status !== 200) {
            throw new Error("Server returned " + response.status + " " + response.statusText);
        }
        return await response.json();
    }
    SpyCards.fetchCustomCardSet = fetchCustomCardSet;
    async function parseCustomCards(codes, variant, allowInvalid) {
        let mode = null;
        const customCardsRaw = [];
        const cards = [];
        const usedIDs = [];
        for (let group of codes.split(";")) {
            if (group.indexOf(".") !== -1 && group.indexOf(",") === -1) {
                const [modeName, revisionStr] = group.split(/\./);
                const revision = parseInt(revisionStr, 10);
                const mode = await fetchCustomCardSet(modeName, revision);
                group = mode.Cards;
            }
            let groupMode = null;
            const groupCards = [];
            const groupUsedIDs = [];
            const idsToReplace = [];
            let first = true;
            let second = false;
            for (let code of group.split(",")) {
                const codeBuf = SpyCards.toArray(Base64.decode(code));
                if (first) {
                    first = false;
                    if (codeBuf[0] === 3) {
                        groupMode = new SpyCards.GameModeData();
                        groupMode.unmarshal(codeBuf);
                        if (mode) {
                            mode.fields = mode.fields.concat(groupMode.fields);
                        }
                        else {
                            mode = groupMode;
                        }
                        second = true;
                        continue;
                    }
                }
                if (second) {
                    second = false;
                    const codeBufClone = codeBuf.slice(0);
                    const encodedVariant = SpyCards.Binary.shiftUVarInt(codeBufClone);
                    if (codeBufClone.length === 0) {
                        if (variant === "parse-variant") {
                            variant = encodedVariant;
                        }
                        continue;
                    }
                }
                const card = new SpyCards.CardDef();
                card.unmarshal(codeBuf);
                if (groupUsedIDs.indexOf(card.id) !== -1) {
                    throw new Error("multiple cards replacing ID " + card.id + " (" + card.originalName() + ")");
                }
                groupUsedIDs.push(card.id);
                if (usedIDs.indexOf(card.id) !== -1) {
                    if (card.id < 128) {
                        if (allowInvalid) {
                            continue;
                        }
                        throw new Error("multiple card groups replacing ID " + card.id + " (" + card.originalName() + ")");
                    }
                    idsToReplace.push(card.id);
                }
                else {
                    usedIDs.push(card.id);
                }
                groupCards.push(card);
            }
            for (let id of idsToReplace) {
                let replacementIDIndex = -1;
                let replacementID;
                do {
                    replacementIDIndex++;
                    replacementID = 128 + ((replacementIDIndex & ~31) << 2) + (replacementIDIndex & 31) + (id & 96);
                } while (usedIDs.indexOf(replacementID) !== -1 || groupUsedIDs.indexOf(replacementID) !== -1);
                usedIDs.push(replacementID);
                if (groupMode) {
                    for (let field of groupMode.fields) {
                        field.replaceCardID(id, replacementID);
                    }
                }
                for (let card of groupCards) {
                    if (card.id === id) {
                        card.id = replacementID;
                    }
                    for (let effect of card.effects) {
                        replaceID(effect, id, replacementID);
                    }
                }
            }
            cards.push(...groupCards);
            if (customCardsRaw.length) {
                customCardsRaw.length = 0;
                if (mode) {
                    const buf = [];
                    mode.marshal(buf);
                    customCardsRaw.push(Base64.encode(new Uint8Array(buf)));
                }
                for (let card of cards) {
                    const buf = [];
                    card.marshal(buf);
                    customCardsRaw.push(Base64.encode(new Uint8Array(buf)));
                }
            }
            else {
                customCardsRaw.push(...group.split(","));
            }
        }
        const variants = mode ? mode.getAll(SpyCards.GameModeFieldType.Variant) : [];
        if (variant === "parse-variant") {
            variant = 0;
        }
        if (variant !== "ignore-variant" && variants.length) {
            const variantID = [];
            variant = Math.min(Math.max(variant || 0, 0), variants.length - 1);
            SpyCards.Binary.pushUVarInt(variantID, variant);
            customCardsRaw.splice(1, 0, Base64.encode(new Uint8Array(variantID)));
            mode.fields.push(...variants[variant].rules);
        }
        else {
            variant = null;
        }
        return { mode, cards, customCardsRaw, variant: variant };
        function replaceID(effect, oldID, newID) {
            if (effect.card === oldID) {
                effect.card = newID;
            }
            if (effect.result) {
                replaceID(effect.result, oldID, newID);
            }
            if (effect.tailsResult) {
                replaceID(effect.tailsResult, oldID, newID);
            }
        }
    }
    SpyCards.parseCustomCards = parseCustomCards;
    function loadSettings() {
        if (localStorage["spy-cards-settings-v0"]) {
            return JSON.parse(localStorage["spy-cards-settings-v0"]);
        }
        const settings = {
            audio: {
                music: 0,
                sounds: 0
            }
        };
        if (localStorage["spy-cards-audio-settings-v0"]) {
            const legacyAudio = JSON.parse(localStorage["spy-cards-audio-settings-v0"]);
            settings.audio.music = legacyAudio.enabled || legacyAudio.musicEnabled ? 0.6 : 0;
            settings.audio.sounds = legacyAudio.enabled || legacyAudio.sfxEnabled ? 0.6 : 0;
        }
        if (localStorage["spy-cards-player-sprite-v0"]) {
            settings.character = localStorage["spy-cards-player-sprite-v0"];
        }
        saveSettings(settings);
        delete localStorage["spy-cards-audio-settings-v0"];
        delete localStorage["spy-cards-player-sprite-v0"];
        return settings;
    }
    SpyCards.loadSettings = loadSettings;
    function saveSettings(settings) {
        localStorage["spy-cards-settings-v0"] = JSON.stringify(settings);
        if (navigator.serviceWorker && navigator.serviceWorker.controller) {
            navigator.serviceWorker.controller.postMessage({ type: "settings-changed" });
        }
        window.dispatchEvent(new Event("spy-cards-settings-changed"));
    }
    SpyCards.saveSettings = saveSettings;
})(SpyCards || (SpyCards = {}));
"use strict";
const spyCardsVersionPrefix = "0.2.80";
const spyCardsClientMode = location.pathname === "/play.html" ? new URLSearchParams(location.search).get("mode") : "";
const spyCardsVersionSuffix = (function () {
    const path = location.pathname.substr(1);
    if (spyCardsClientMode) {
        return "-" + spyCardsClientMode;
    }
    if (path.substr(path.length - ".html".length) === ".html") {
        return "-" + path.substr(0, path.length - 5);
    }
    return "";
})();
const spyCardsVersion = spyCardsVersionPrefix + spyCardsVersionSuffix;
var spyCardsVersionVariableSuffix = "";
// This file has been modified from the version that ships with Go.

// Copyright 2018 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

(() => {
	// Map multiple JavaScript environments to a single common API,
	// preferring web standards over Node.js API.
	//
	// Environments considered:
	// - Browsers
	// - Node.js
	// - Electron
	// - Parcel

	if (typeof global !== "undefined") {
		// global already exists
	} else if (typeof window !== "undefined") {
		window.global = window;
	} else if (typeof self !== "undefined") {
		self.global = self;
	} else {
		throw new Error("cannot export Go (neither global, window nor self is defined)");
	}

	if (!global.require && typeof require !== "undefined") {
		global.require = require;
	}

	if (!global.fs && global.require) {
		const fs = require("fs");
		if (Object.keys(fs) !== 0) {
			global.fs = fs;
		}
	}

	const enosys = () => {
		const err = new Error("not implemented");
		err.code = "ENOSYS";
		return err;
	};

	let panicBuf = "";

	if (!global.fs) {
		let outputBuf = "";
		global.fs = {
			constants: { O_WRONLY: -1, O_RDWR: -1, O_CREAT: -1, O_TRUNC: -1, O_APPEND: -1, O_EXCL: -1 }, // unused
			writeSync(fd, buf) {
				outputBuf += decoder.decode(buf);
				const nl = outputBuf.lastIndexOf("\n");
				if (nl != -1) {
					let command = "log";
					if (/^[^ ]+: WARNING:/.test(outputBuf)) {
						command = "warn";
					} else if (/^[^ ]+: (ERROR|TODO):/.test(outputBuf)) {
						command = "error";
					} else if (/^[^ ]+: DEBUG:/.test(outputBuf)) {
						command = "debug";
					}

					if (/^panic: /.test(outputBuf) || panicBuf) {
						panicBuf += outputBuf.substr(0, nl + 1);
					}

					console[command](outputBuf.substr(0, nl));
					outputBuf = outputBuf.substr(nl + 1);
				}
				return buf.length;
			},
			write(fd, buf, offset, length, position, callback) {
				if (offset !== 0 || length !== buf.length || position !== null) {
					callback(enosys());
					return;
				}
				const n = this.writeSync(fd, buf);
				callback(null, n);
			},
			chmod(path, mode, callback) { callback(enosys()); },
			chown(path, uid, gid, callback) { callback(enosys()); },
			close(fd, callback) { callback(enosys()); },
			fchmod(fd, mode, callback) { callback(enosys()); },
			fchown(fd, uid, gid, callback) { callback(enosys()); },
			fstat(fd, callback) { callback(enosys()); },
			fsync(fd, callback) { callback(null); },
			ftruncate(fd, length, callback) { callback(enosys()); },
			lchown(path, uid, gid, callback) { callback(enosys()); },
			link(path, link, callback) { callback(enosys()); },
			lstat(path, callback) { callback(enosys()); },
			mkdir(path, perm, callback) { callback(enosys()); },
			open(path, flags, mode, callback) { callback(enosys()); },
			read(fd, buffer, offset, length, position, callback) { callback(enosys()); },
			readdir(path, callback) { callback(enosys()); },
			readlink(path, callback) { callback(enosys()); },
			rename(from, to, callback) { callback(enosys()); },
			rmdir(path, callback) { callback(enosys()); },
			stat(path, callback) { callback(enosys()); },
			symlink(path, link, callback) { callback(enosys()); },
			truncate(path, length, callback) { callback(enosys()); },
			unlink(path, callback) { callback(enosys()); },
			utimes(path, atime, mtime, callback) { callback(enosys()); },
		};
	}

	if (!global.process) {
		global.process = {
			getuid() { return -1; },
			getgid() { return -1; },
			geteuid() { return -1; },
			getegid() { return -1; },
			getgroups() { throw enosys(); },
			pid: -1,
			ppid: -1,
			umask() { throw enosys(); },
			cwd() { throw enosys(); },
			chdir() { throw enosys(); },
		}
	}

	if (!global.crypto) {
		const nodeCrypto = require("crypto");
		global.crypto = {
			getRandomValues(b) {
				nodeCrypto.randomFillSync(b);
			},
		};
	}

	if (!global.performance) {
		global.performance = {
			now() {
				const [sec, nsec] = process.hrtime();
				return sec * 1000 + nsec / 1000000;
			},
		};
	}

	if (!global.TextEncoder) {
		global.TextEncoder = require("util").TextEncoder;
	}

	if (!global.TextDecoder) {
		global.TextDecoder = require("util").TextDecoder;
	}

	// End of polyfills for common API.

	const encoder = new TextEncoder("utf-8");
	const decoder = new TextDecoder("utf-8");

	global.Go = class {
		constructor() {
			this.argv = ["js"];
			this.env = {};
			this.exit = (code) => {
				if (code !== 0) {
					console.warn("exit code:", code);

					if (panicBuf) {
						const nl = panicBuf.indexOf("\n\n");
						const err = new Error(panicBuf.substr(0, nl).replace(/^panic:/, "PANIC:"));
						err.goStack = panicBuf.substr(nl+2);

						SpyCards.disableErrorHandler = false;
						errorHandler(err);
					}
				}
			};
			this._exitPromise = new Promise((resolve) => {
				this._resolveExitPromise = resolve;
			});
			this._pendingEvent = null;
			this._scheduledTimeouts = new Map();
			this._nextCallbackTimeoutID = 1;

			const setInt64 = (addr, v) => {
				this.mem.setUint32(addr + 0, v, true);
				this.mem.setUint32(addr + 4, Math.floor(v / 4294967296), true);
			}

			const getInt64 = (addr) => {
				const low = this.mem.getUint32(addr + 0, true);
				const high = this.mem.getInt32(addr + 4, true);
				return low + high * 4294967296;
			}

			const loadValue = (addr) => {
				const f = this.mem.getFloat64(addr, true);
				if (f === 0) {
					return undefined;
				}
				if (!isNaN(f)) {
					return f;
				}

				const id = this.mem.getUint32(addr, true);
				return this._values[id];
			}

			const storeValue = (addr, v) => {
				const nanHead = 0x7FF80000;

				if (typeof v === "number" && v !== 0) {
					if (isNaN(v)) {
						this.mem.setUint32(addr + 4, nanHead, true);
						this.mem.setUint32(addr, 0, true);
						return;
					}
					this.mem.setFloat64(addr, v, true);
					return;
				}

				if (v === undefined) {
					this.mem.setFloat64(addr, 0, true);
					return;
				}

				let id = this._ids.get(v);
				if (id === undefined) {
					id = this._idPool.pop();
					if (id === undefined) {
						id = this._values.length;
					}
					this._values[id] = v;
					this._goRefCounts[id] = 0;
					this._ids.set(v, id);
				}
				this._goRefCounts[id]++;
				let typeFlag = 0;
				switch (typeof v) {
					case "object":
						if (v !== null) {
							typeFlag = 1;
						}
						break;
					case "string":
						typeFlag = 2;
						break;
					case "symbol":
						typeFlag = 3;
						break;
					case "function":
						typeFlag = 4;
						break;
				}
				this.mem.setUint32(addr + 4, nanHead | typeFlag, true);
				this.mem.setUint32(addr, id, true);
			}

			const loadSlice = (addr) => {
				const array = getInt64(addr + 0);
				const len = getInt64(addr + 8);
				return new Uint8Array(this._inst.exports.mem.buffer, array, len);
			}

			const loadSliceOfValues = (addr) => {
				const array = getInt64(addr + 0);
				const len = getInt64(addr + 8);
				const a = new Array(len);
				for (let i = 0; i < len; i++) {
					a[i] = loadValue(array + i * 8);
				}
				return a;
			}

			const loadString = (addr) => {
				const saddr = getInt64(addr + 0);
				const len = getInt64(addr + 8);
				return decoder.decode(new DataView(this._inst.exports.mem.buffer, saddr, len));
			}

			const timeOrigin = Date.now() - performance.now();
			this.importObject = {
				go: {
					// Go's SP does not change as long as no Go code is running. Some operations (e.g. calls, getters and setters)
					// may synchronously trigger a Go event handler. This makes Go code get executed in the middle of the imported
					// function. A goroutine can switch to a new stack if the current stack is too small (see morestack function).
					// This changes the SP, thus we have to update the SP used by the imported function.

					// func wasmExit(code int32)
					"runtime.wasmExit": (sp) => {
						const code = this.mem.getInt32(sp + 8, true);
						this.exited = true;
						delete this._inst;
						delete this._values;
						delete this._goRefCounts;
						delete this._ids;
						delete this._idPool;
						this.exit(code);
					},

					// func wasmWrite(fd uintptr, p unsafe.Pointer, n int32)
					"runtime.wasmWrite": (sp) => {
						const fd = getInt64(sp + 8);
						const p = getInt64(sp + 16);
						const n = this.mem.getInt32(sp + 24, true);
						fs.writeSync(fd, new Uint8Array(this._inst.exports.mem.buffer, p, n));
					},

					// func resetMemoryDataView()
					"runtime.resetMemoryDataView": (sp) => {
						this.mem = new DataView(this._inst.exports.mem.buffer);
					},

					// func nanotime1() int64
					"runtime.nanotime1": (sp) => {
						setInt64(sp + 8, (timeOrigin + performance.now()) * 1000000);
					},

					// func walltime1() (sec int64, nsec int32)
					"runtime.walltime1": (sp) => {
						const msec = (new Date).getTime();
						setInt64(sp + 8, msec / 1000);
						this.mem.setInt32(sp + 16, (msec % 1000) * 1000000, true);
					},

					// func scheduleTimeoutEvent(delay int64) int32
					"runtime.scheduleTimeoutEvent": (sp) => {
						const id = this._nextCallbackTimeoutID;
						this._nextCallbackTimeoutID++;
						this._scheduledTimeouts.set(id, setTimeout(
							() => {
								this._resume();
								while (this._scheduledTimeouts.has(id)) {
									// for some reason Go failed to register the timeout event, log and try again
									// (temporary workaround for https://github.com/golang/go/issues/28975)
									console.warn("scheduleTimeoutEvent: missed timeout event");
									this._resume();
								}
							},
							getInt64(sp + 8) + 1, // setTimeout has been seen to fire up to 1 millisecond early
						));
						this.mem.setInt32(sp + 16, id, true);
					},

					// func clearTimeoutEvent(id int32)
					"runtime.clearTimeoutEvent": (sp) => {
						const id = this.mem.getInt32(sp + 8, true);
						clearTimeout(this._scheduledTimeouts.get(id));
						this._scheduledTimeouts.delete(id);
					},

					// func getRandomData(r []byte)
					"runtime.getRandomData": (sp) => {
						crypto.getRandomValues(loadSlice(sp + 8));
					},

					// func finalizeRef(v ref)
					"syscall/js.finalizeRef": (sp) => {
						const id = this.mem.getUint32(sp + 8, true);
						this._goRefCounts[id]--;
						if (this._goRefCounts[id] === 0) {
							const v = this._values[id];
							this._values[id] = null;
							this._ids.delete(v);
							this._idPool.push(id);
						}
					},

					// func stringVal(value string) ref
					"syscall/js.stringVal": (sp) => {
						storeValue(sp + 24, loadString(sp + 8));
					},

					// func valueGet(v ref, p string) ref
					"syscall/js.valueGet": (sp) => {
						const result = Reflect.get(loadValue(sp + 8), loadString(sp + 16));
						sp = this._inst.exports.getsp(); // see comment above
						storeValue(sp + 32, result);
					},

					// func valueSet(v ref, p string, x ref)
					"syscall/js.valueSet": (sp) => {
						Reflect.set(loadValue(sp + 8), loadString(sp + 16), loadValue(sp + 32));
					},

					// func valueDelete(v ref, p string)
					"syscall/js.valueDelete": (sp) => {
						Reflect.deleteProperty(loadValue(sp + 8), loadString(sp + 16));
					},

					// func valueIndex(v ref, i int) ref
					"syscall/js.valueIndex": (sp) => {
						storeValue(sp + 24, Reflect.get(loadValue(sp + 8), getInt64(sp + 16)));
					},

					// valueSetIndex(v ref, i int, x ref)
					"syscall/js.valueSetIndex": (sp) => {
						Reflect.set(loadValue(sp + 8), getInt64(sp + 16), loadValue(sp + 24));
					},

					// func valueCall(v ref, m string, args []ref) (ref, bool)
					"syscall/js.valueCall": (sp) => {
						try {
							const v = loadValue(sp + 8);
							const m = Reflect.get(v, loadString(sp + 16));
							const args = loadSliceOfValues(sp + 32);
							const result = Reflect.apply(m, v, args);
							sp = this._inst.exports.getsp(); // see comment above
							storeValue(sp + 56, result);
							this.mem.setUint8(sp + 64, 1);
						} catch (err) {
							storeValue(sp + 56, err);
							this.mem.setUint8(sp + 64, 0);
						}
					},

					// func valueInvoke(v ref, args []ref) (ref, bool)
					"syscall/js.valueInvoke": (sp) => {
						try {
							const v = loadValue(sp + 8);
							const args = loadSliceOfValues(sp + 16);
							const result = Reflect.apply(v, undefined, args);
							sp = this._inst.exports.getsp(); // see comment above
							storeValue(sp + 40, result);
							this.mem.setUint8(sp + 48, 1);
						} catch (err) {
							storeValue(sp + 40, err);
							this.mem.setUint8(sp + 48, 0);
						}
					},

					// func valueNew(v ref, args []ref) (ref, bool)
					"syscall/js.valueNew": (sp) => {
						try {
							const v = loadValue(sp + 8);
							const args = loadSliceOfValues(sp + 16);
							const result = Reflect.construct(v, args);
							sp = this._inst.exports.getsp(); // see comment above
							storeValue(sp + 40, result);
							this.mem.setUint8(sp + 48, 1);
						} catch (err) {
							storeValue(sp + 40, err);
							this.mem.setUint8(sp + 48, 0);
						}
					},

					// func valueLength(v ref) int
					"syscall/js.valueLength": (sp) => {
						setInt64(sp + 16, parseInt(loadValue(sp + 8).length));
					},

					// valuePrepareString(v ref) (ref, int)
					"syscall/js.valuePrepareString": (sp) => {
						const str = encoder.encode(String(loadValue(sp + 8)));
						storeValue(sp + 16, str);
						setInt64(sp + 24, str.length);
					},

					// valueLoadString(v ref, b []byte)
					"syscall/js.valueLoadString": (sp) => {
						const str = loadValue(sp + 8);
						loadSlice(sp + 16).set(str);
					},

					// func valueInstanceOf(v ref, t ref) bool
					"syscall/js.valueInstanceOf": (sp) => {
						this.mem.setUint8(sp + 24, (loadValue(sp + 8) instanceof loadValue(sp + 16)) ? 1 : 0);
					},

					// func copyBytesToGo(dst []byte, src ref) (int, bool)
					"syscall/js.copyBytesToGo": (sp) => {
						const dst = loadSlice(sp + 8);
						const src = loadValue(sp + 32);
						if (!(src instanceof Uint8Array || src instanceof Uint8ClampedArray)) {
							this.mem.setUint8(sp + 48, 0);
							return;
						}
						const toCopy = src.subarray(0, dst.length);
						dst.set(toCopy);
						setInt64(sp + 40, toCopy.length);
						this.mem.setUint8(sp + 48, 1);
					},

					// func copyBytesToJS(dst ref, src []byte) (int, bool)
					"syscall/js.copyBytesToJS": (sp) => {
						const dst = loadValue(sp + 8);
						const src = loadSlice(sp + 16);
						if (!(dst instanceof Uint8Array || dst instanceof Uint8ClampedArray)) {
							this.mem.setUint8(sp + 48, 0);
							return;
						}
						const toCopy = src.subarray(0, dst.length);
						dst.set(toCopy);
						setInt64(sp + 40, toCopy.length);
						this.mem.setUint8(sp + 48, 1);
					},

					"debug": (value) => {
						console.log(value);
					},
				}
			};
		}

		async run(instance) {
			this._inst = instance;
			this.mem = new DataView(this._inst.exports.mem.buffer);
			this._values = [ // JS values that Go currently has references to, indexed by reference id
				NaN,
				0,
				null,
				true,
				false,
				global,
				this,
			];
			this._goRefCounts = new Array(this._values.length).fill(Infinity); // number of references that Go has to a JS value, indexed by reference id
			this._ids = new Map([ // mapping from JS values to reference ids
				[0, 1],
				[null, 2],
				[true, 3],
				[false, 4],
				[global, 5],
				[this, 6],
			]);
			this._idPool = [];   // unused ids that have been garbage collected
			this.exited = false; // whether the Go program has exited

			// Pass command line arguments and environment variables to WebAssembly by writing them to the linear memory.
			let offset = 4096;

			const strPtr = (str) => {
				const ptr = offset;
				const bytes = encoder.encode(str + "\0");
				new Uint8Array(this.mem.buffer, offset, bytes.length).set(bytes);
				offset += bytes.length;
				if (offset % 8 !== 0) {
					offset += 8 - (offset % 8);
				}
				return ptr;
			};

			const argc = this.argv.length;

			const argvPtrs = [];
			this.argv.forEach((arg) => {
				argvPtrs.push(strPtr(arg));
			});
			argvPtrs.push(0);

			const keys = Object.keys(this.env).sort();
			keys.forEach((key) => {
				argvPtrs.push(strPtr(`${key}=${this.env[key]}`));
			});
			argvPtrs.push(0);

			const argv = offset;
			argvPtrs.forEach((ptr) => {
				this.mem.setUint32(offset, ptr, true);
				this.mem.setUint32(offset + 4, 0, true);
				offset += 8;
			});

			this._inst.exports.run(argc, argv);
			if (this.exited) {
				this._resolveExitPromise();
			}
			await this._exitPromise;
		}

		_resume() {
			if (this.exited) {
				throw new Error("Go program has already exited");
			}
			this._inst.exports.resume();
			if (this.exited) {
				this._resolveExitPromise();
			}
		}

		_makeFuncWrapper(id) {
			const go = this;
			return function () {
				const event = { id: id, this: this, args: arguments };
				go._pendingEvent = event;
				go._resume();
				return event.result;
			};
		}
	}

	if (
		global.require &&
		global.require.main === module &&
		global.process &&
		global.process.versions &&
		!global.process.versions.electron
	) {
		if (process.argv.length < 3) {
			console.error("usage: go_js_wasm_exec [wasm binary] [arguments]");
			process.exit(1);
		}

		const go = new Go();
		go.argv = process.argv.slice(2);
		go.env = Object.assign({ TMPDIR: require("os").tmpdir() }, process.env);
		go.exit = process.exit;
		WebAssembly.instantiate(fs.readFileSync(process.argv[2]), go.importObject).then((result) => {
			process.on("exit", (code) => { // Node.js exits if no event handler is pending
				if (code === 0 && !go.exited) {
					// deadlock, make Go print error and stack traces
					go._pendingEvent = { id: 0 };
					go._resume();
				}
			});
			return go.run(result.instance);
		}).catch((err) => {
			console.error(err);
			process.exit(1);
		});
	}
})();
"use strict";
var SpyCards;
(function (SpyCards) {
    var Native;
    (function (Native) {
        if (!WebAssembly.instantiateStreaming) { // polyfill
            WebAssembly.instantiateStreaming = async (resp, importObject) => {
                const source = await (await resp).arrayBuffer();
                return await WebAssembly.instantiate(source, importObject);
            };
        }
        let go = new window.Go();
        let inst;
        let mod;
        let ready = WebAssembly.instantiateStreaming(fetch("spy-cards.wasm"), go.importObject).then(function (result) {
            inst = result.instance;
            mod = result.module;
        });
        const roomRPCPromise = new Promise((resolve) => Native.roomRPCInit = resolve);
        let roomRPCProcess;
        async function roomRPC() {
            if (new URLSearchParams(location.search).has("no3d") || SpyCards.loadSettings().disable3D) {
                return null;
            }
            if (!roomRPCProcess) {
                roomRPCProcess = run("-run=RoomRPC");
            }
            return roomRPCPromise;
        }
        Native.roomRPC = roomRPC;
        async function run(...argv) {
            ready = ready.then(async () => {
                go.argv = ["js"].concat(argv);
                await go.run(inst);
                inst = await WebAssembly.instantiate(mod, go.importObject);
            });
            return ready;
        }
        Native.run = run;
    })(Native = SpyCards.Native || (SpyCards.Native = {}));
})(SpyCards || (SpyCards = {}));
(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.adapter = f()}})(function(){var define,module,exports;return (function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */

'use strict';

var _adapter_factory = require('./adapter_factory.js');

var adapter = (0, _adapter_factory.adapterFactory)({ window: typeof window === 'undefined' ? undefined : window });
module.exports = adapter; // this is the difference from adapter_core.

},{"./adapter_factory.js":2}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.adapterFactory = adapterFactory;

var _utils = require('./utils');

var utils = _interopRequireWildcard(_utils);

var _chrome_shim = require('./chrome/chrome_shim');

var chromeShim = _interopRequireWildcard(_chrome_shim);

var _edge_shim = require('./edge/edge_shim');

var edgeShim = _interopRequireWildcard(_edge_shim);

var _firefox_shim = require('./firefox/firefox_shim');

var firefoxShim = _interopRequireWildcard(_firefox_shim);

var _safari_shim = require('./safari/safari_shim');

var safariShim = _interopRequireWildcard(_safari_shim);

var _common_shim = require('./common_shim');

var commonShim = _interopRequireWildcard(_common_shim);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

// Shimming starts here.
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
function adapterFactory() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      window = _ref.window;

  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {
    shimChrome: true,
    shimFirefox: true,
    shimEdge: true,
    shimSafari: true
  };

  // Utils.
  var logging = utils.log;
  var browserDetails = utils.detectBrowser(window);

  var adapter = {
    browserDetails: browserDetails,
    commonShim: commonShim,
    extractVersion: utils.extractVersion,
    disableLog: utils.disableLog,
    disableWarnings: utils.disableWarnings
  };

  // Shim browser if found.
  switch (browserDetails.browser) {
    case 'chrome':
      if (!chromeShim || !chromeShim.shimPeerConnection || !options.shimChrome) {
        logging('Chrome shim is not included in this adapter release.');
        return adapter;
      }
      if (browserDetails.version === null) {
        logging('Chrome shim can not determine version, not shimming.');
        return adapter;
      }
      logging('adapter.js shimming chrome.');
      // Export to the adapter global object visible in the browser.
      adapter.browserShim = chromeShim;

      chromeShim.shimGetUserMedia(window);
      chromeShim.shimMediaStream(window);
      chromeShim.shimPeerConnection(window);
      chromeShim.shimOnTrack(window);
      chromeShim.shimAddTrackRemoveTrack(window);
      chromeShim.shimGetSendersWithDtmf(window);
      chromeShim.shimGetStats(window);
      chromeShim.shimSenderReceiverGetStats(window);
      chromeShim.fixNegotiationNeeded(window);

      commonShim.shimRTCIceCandidate(window);
      commonShim.shimConnectionState(window);
      commonShim.shimMaxMessageSize(window);
      commonShim.shimSendThrowTypeError(window);
      commonShim.removeAllowExtmapMixed(window);
      break;
    case 'firefox':
      if (!firefoxShim || !firefoxShim.shimPeerConnection || !options.shimFirefox) {
        logging('Firefox shim is not included in this adapter release.');
        return adapter;
      }
      logging('adapter.js shimming firefox.');
      // Export to the adapter global object visible in the browser.
      adapter.browserShim = firefoxShim;

      firefoxShim.shimGetUserMedia(window);
      firefoxShim.shimPeerConnection(window);
      firefoxShim.shimOnTrack(window);
      firefoxShim.shimRemoveStream(window);
      firefoxShim.shimSenderGetStats(window);
      firefoxShim.shimReceiverGetStats(window);
      firefoxShim.shimRTCDataChannel(window);
      firefoxShim.shimAddTransceiver(window);
      firefoxShim.shimGetParameters(window);
      firefoxShim.shimCreateOffer(window);
      firefoxShim.shimCreateAnswer(window);

      commonShim.shimRTCIceCandidate(window);
      commonShim.shimConnectionState(window);
      commonShim.shimMaxMessageSize(window);
      commonShim.shimSendThrowTypeError(window);
      break;
    case 'edge':
      if (!edgeShim || !edgeShim.shimPeerConnection || !options.shimEdge) {
        logging('MS edge shim is not included in this adapter release.');
        return adapter;
      }
      logging('adapter.js shimming edge.');
      // Export to the adapter global object visible in the browser.
      adapter.browserShim = edgeShim;

      edgeShim.shimGetUserMedia(window);
      edgeShim.shimGetDisplayMedia(window);
      edgeShim.shimPeerConnection(window);
      edgeShim.shimReplaceTrack(window);

      // the edge shim implements the full RTCIceCandidate object.

      commonShim.shimMaxMessageSize(window);
      commonShim.shimSendThrowTypeError(window);
      break;
    case 'safari':
      if (!safariShim || !options.shimSafari) {
        logging('Safari shim is not included in this adapter release.');
        return adapter;
      }
      logging('adapter.js shimming safari.');
      // Export to the adapter global object visible in the browser.
      adapter.browserShim = safariShim;

      safariShim.shimRTCIceServerUrls(window);
      safariShim.shimCreateOfferLegacy(window);
      safariShim.shimCallbacksAPI(window);
      safariShim.shimLocalStreamsAPI(window);
      safariShim.shimRemoteStreamsAPI(window);
      safariShim.shimTrackEventTransceiver(window);
      safariShim.shimGetUserMedia(window);
      safariShim.shimAudioContext(window);

      commonShim.shimRTCIceCandidate(window);
      commonShim.shimMaxMessageSize(window);
      commonShim.shimSendThrowTypeError(window);
      commonShim.removeAllowExtmapMixed(window);
      break;
    default:
      logging('Unsupported browser!');
      break;
  }

  return adapter;
}

// Browser shims.

},{"./chrome/chrome_shim":3,"./common_shim":6,"./edge/edge_shim":12,"./firefox/firefox_shim":7,"./safari/safari_shim":10,"./utils":11}],3:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shimGetDisplayMedia = exports.shimGetUserMedia = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _getusermedia = require('./getusermedia');

Object.defineProperty(exports, 'shimGetUserMedia', {
  enumerable: true,
  get: function get() {
    return _getusermedia.shimGetUserMedia;
  }
});

var _getdisplaymedia = require('./getdisplaymedia');

Object.defineProperty(exports, 'shimGetDisplayMedia', {
  enumerable: true,
  get: function get() {
    return _getdisplaymedia.shimGetDisplayMedia;
  }
});
exports.shimMediaStream = shimMediaStream;
exports.shimOnTrack = shimOnTrack;
exports.shimGetSendersWithDtmf = shimGetSendersWithDtmf;
exports.shimGetStats = shimGetStats;
exports.shimSenderReceiverGetStats = shimSenderReceiverGetStats;
exports.shimAddTrackRemoveTrackWithNative = shimAddTrackRemoveTrackWithNative;
exports.shimAddTrackRemoveTrack = shimAddTrackRemoveTrack;
exports.shimPeerConnection = shimPeerConnection;
exports.fixNegotiationNeeded = fixNegotiationNeeded;

var _utils = require('../utils.js');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function shimMediaStream(window) {
  window.MediaStream = window.MediaStream || window.webkitMediaStream;
}

function shimOnTrack(window) {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection && !('ontrack' in window.RTCPeerConnection.prototype)) {
    Object.defineProperty(window.RTCPeerConnection.prototype, 'ontrack', {
      get: function get() {
        return this._ontrack;
      },
      set: function set(f) {
        if (this._ontrack) {
          this.removeEventListener('track', this._ontrack);
        }
        this.addEventListener('track', this._ontrack = f);
      },

      enumerable: true,
      configurable: true
    });
    var origSetRemoteDescription = window.RTCPeerConnection.prototype.setRemoteDescription;
    window.RTCPeerConnection.prototype.setRemoteDescription = function setRemoteDescription() {
      var _this = this;

      if (!this._ontrackpoly) {
        this._ontrackpoly = function (e) {
          // onaddstream does not fire when a track is added to an existing
          // stream. But stream.onaddtrack is implemented so we use that.
          e.stream.addEventListener('addtrack', function (te) {
            var receiver = void 0;
            if (window.RTCPeerConnection.prototype.getReceivers) {
              receiver = _this.getReceivers().find(function (r) {
                return r.track && r.track.id === te.track.id;
              });
            } else {
              receiver = { track: te.track };
            }

            var event = new Event('track');
            event.track = te.track;
            event.receiver = receiver;
            event.transceiver = { receiver: receiver };
            event.streams = [e.stream];
            _this.dispatchEvent(event);
          });
          e.stream.getTracks().forEach(function (track) {
            var receiver = void 0;
            if (window.RTCPeerConnection.prototype.getReceivers) {
              receiver = _this.getReceivers().find(function (r) {
                return r.track && r.track.id === track.id;
              });
            } else {
              receiver = { track: track };
            }
            var event = new Event('track');
            event.track = track;
            event.receiver = receiver;
            event.transceiver = { receiver: receiver };
            event.streams = [e.stream];
            _this.dispatchEvent(event);
          });
        };
        this.addEventListener('addstream', this._ontrackpoly);
      }
      return origSetRemoteDescription.apply(this, arguments);
    };
  } else {
    // even if RTCRtpTransceiver is in window, it is only used and
    // emitted in unified-plan. Unfortunately this means we need
    // to unconditionally wrap the event.
    utils.wrapPeerConnectionEvent(window, 'track', function (e) {
      if (!e.transceiver) {
        Object.defineProperty(e, 'transceiver', { value: { receiver: e.receiver } });
      }
      return e;
    });
  }
}

function shimGetSendersWithDtmf(window) {
  // Overrides addTrack/removeTrack, depends on shimAddTrackRemoveTrack.
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection && !('getSenders' in window.RTCPeerConnection.prototype) && 'createDTMFSender' in window.RTCPeerConnection.prototype) {
    var shimSenderWithDtmf = function shimSenderWithDtmf(pc, track) {
      return {
        track: track,
        get dtmf() {
          if (this._dtmf === undefined) {
            if (track.kind === 'audio') {
              this._dtmf = pc.createDTMFSender(track);
            } else {
              this._dtmf = null;
            }
          }
          return this._dtmf;
        },
        _pc: pc
      };
    };

    // augment addTrack when getSenders is not available.
    if (!window.RTCPeerConnection.prototype.getSenders) {
      window.RTCPeerConnection.prototype.getSenders = function getSenders() {
        this._senders = this._senders || [];
        return this._senders.slice(); // return a copy of the internal state.
      };
      var origAddTrack = window.RTCPeerConnection.prototype.addTrack;
      window.RTCPeerConnection.prototype.addTrack = function addTrack(track, stream) {
        var sender = origAddTrack.apply(this, arguments);
        if (!sender) {
          sender = shimSenderWithDtmf(this, track);
          this._senders.push(sender);
        }
        return sender;
      };

      var origRemoveTrack = window.RTCPeerConnection.prototype.removeTrack;
      window.RTCPeerConnection.prototype.removeTrack = function removeTrack(sender) {
        origRemoveTrack.apply(this, arguments);
        var idx = this._senders.indexOf(sender);
        if (idx !== -1) {
          this._senders.splice(idx, 1);
        }
      };
    }
    var origAddStream = window.RTCPeerConnection.prototype.addStream;
    window.RTCPeerConnection.prototype.addStream = function addStream(stream) {
      var _this2 = this;

      this._senders = this._senders || [];
      origAddStream.apply(this, [stream]);
      stream.getTracks().forEach(function (track) {
        _this2._senders.push(shimSenderWithDtmf(_this2, track));
      });
    };

    var origRemoveStream = window.RTCPeerConnection.prototype.removeStream;
    window.RTCPeerConnection.prototype.removeStream = function removeStream(stream) {
      var _this3 = this;

      this._senders = this._senders || [];
      origRemoveStream.apply(this, [stream]);

      stream.getTracks().forEach(function (track) {
        var sender = _this3._senders.find(function (s) {
          return s.track === track;
        });
        if (sender) {
          // remove sender
          _this3._senders.splice(_this3._senders.indexOf(sender), 1);
        }
      });
    };
  } else if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection && 'getSenders' in window.RTCPeerConnection.prototype && 'createDTMFSender' in window.RTCPeerConnection.prototype && window.RTCRtpSender && !('dtmf' in window.RTCRtpSender.prototype)) {
    var origGetSenders = window.RTCPeerConnection.prototype.getSenders;
    window.RTCPeerConnection.prototype.getSenders = function getSenders() {
      var _this4 = this;

      var senders = origGetSenders.apply(this, []);
      senders.forEach(function (sender) {
        return sender._pc = _this4;
      });
      return senders;
    };

    Object.defineProperty(window.RTCRtpSender.prototype, 'dtmf', {
      get: function get() {
        if (this._dtmf === undefined) {
          if (this.track.kind === 'audio') {
            this._dtmf = this._pc.createDTMFSender(this.track);
          } else {
            this._dtmf = null;
          }
        }
        return this._dtmf;
      }
    });
  }
}

function shimGetStats(window) {
  if (!window.RTCPeerConnection) {
    return;
  }

  var origGetStats = window.RTCPeerConnection.prototype.getStats;
  window.RTCPeerConnection.prototype.getStats = function getStats() {
    var _this5 = this;

    var _arguments = Array.prototype.slice.call(arguments),
        selector = _arguments[0],
        onSucc = _arguments[1],
        onErr = _arguments[2];

    // If selector is a function then we are in the old style stats so just
    // pass back the original getStats format to avoid breaking old users.


    if (arguments.length > 0 && typeof selector === 'function') {
      return origGetStats.apply(this, arguments);
    }

    // When spec-style getStats is supported, return those when called with
    // either no arguments or the selector argument is null.
    if (origGetStats.length === 0 && (arguments.length === 0 || typeof selector !== 'function')) {
      return origGetStats.apply(this, []);
    }

    var fixChromeStats_ = function fixChromeStats_(response) {
      var standardReport = {};
      var reports = response.result();
      reports.forEach(function (report) {
        var standardStats = {
          id: report.id,
          timestamp: report.timestamp,
          type: {
            localcandidate: 'local-candidate',
            remotecandidate: 'remote-candidate'
          }[report.type] || report.type
        };
        report.names().forEach(function (name) {
          standardStats[name] = report.stat(name);
        });
        standardReport[standardStats.id] = standardStats;
      });

      return standardReport;
    };

    // shim getStats with maplike support
    var makeMapStats = function makeMapStats(stats) {
      return new Map(Object.keys(stats).map(function (key) {
        return [key, stats[key]];
      }));
    };

    if (arguments.length >= 2) {
      var successCallbackWrapper_ = function successCallbackWrapper_(response) {
        onSucc(makeMapStats(fixChromeStats_(response)));
      };

      return origGetStats.apply(this, [successCallbackWrapper_, selector]);
    }

    // promise-support
    return new Promise(function (resolve, reject) {
      origGetStats.apply(_this5, [function (response) {
        resolve(makeMapStats(fixChromeStats_(response)));
      }, reject]);
    }).then(onSucc, onErr);
  };
}

function shimSenderReceiverGetStats(window) {
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection && window.RTCRtpSender && window.RTCRtpReceiver)) {
    return;
  }

  // shim sender stats.
  if (!('getStats' in window.RTCRtpSender.prototype)) {
    var origGetSenders = window.RTCPeerConnection.prototype.getSenders;
    if (origGetSenders) {
      window.RTCPeerConnection.prototype.getSenders = function getSenders() {
        var _this6 = this;

        var senders = origGetSenders.apply(this, []);
        senders.forEach(function (sender) {
          return sender._pc = _this6;
        });
        return senders;
      };
    }

    var origAddTrack = window.RTCPeerConnection.prototype.addTrack;
    if (origAddTrack) {
      window.RTCPeerConnection.prototype.addTrack = function addTrack() {
        var sender = origAddTrack.apply(this, arguments);
        sender._pc = this;
        return sender;
      };
    }
    window.RTCRtpSender.prototype.getStats = function getStats() {
      var sender = this;
      return this._pc.getStats().then(function (result) {
        return (
          /* Note: this will include stats of all senders that
           *   send a track with the same id as sender.track as
           *   it is not possible to identify the RTCRtpSender.
           */
          utils.filterStats(result, sender.track, true)
        );
      });
    };
  }

  // shim receiver stats.
  if (!('getStats' in window.RTCRtpReceiver.prototype)) {
    var origGetReceivers = window.RTCPeerConnection.prototype.getReceivers;
    if (origGetReceivers) {
      window.RTCPeerConnection.prototype.getReceivers = function getReceivers() {
        var _this7 = this;

        var receivers = origGetReceivers.apply(this, []);
        receivers.forEach(function (receiver) {
          return receiver._pc = _this7;
        });
        return receivers;
      };
    }
    utils.wrapPeerConnectionEvent(window, 'track', function (e) {
      e.receiver._pc = e.srcElement;
      return e;
    });
    window.RTCRtpReceiver.prototype.getStats = function getStats() {
      var receiver = this;
      return this._pc.getStats().then(function (result) {
        return utils.filterStats(result, receiver.track, false);
      });
    };
  }

  if (!('getStats' in window.RTCRtpSender.prototype && 'getStats' in window.RTCRtpReceiver.prototype)) {
    return;
  }

  // shim RTCPeerConnection.getStats(track).
  var origGetStats = window.RTCPeerConnection.prototype.getStats;
  window.RTCPeerConnection.prototype.getStats = function getStats() {
    if (arguments.length > 0 && arguments[0] instanceof window.MediaStreamTrack) {
      var track = arguments[0];
      var sender = void 0;
      var receiver = void 0;
      var err = void 0;
      this.getSenders().forEach(function (s) {
        if (s.track === track) {
          if (sender) {
            err = true;
          } else {
            sender = s;
          }
        }
      });
      this.getReceivers().forEach(function (r) {
        if (r.track === track) {
          if (receiver) {
            err = true;
          } else {
            receiver = r;
          }
        }
        return r.track === track;
      });
      if (err || sender && receiver) {
        return Promise.reject(new DOMException('There are more than one sender or receiver for the track.', 'InvalidAccessError'));
      } else if (sender) {
        return sender.getStats();
      } else if (receiver) {
        return receiver.getStats();
      }
      return Promise.reject(new DOMException('There is no sender or receiver for the track.', 'InvalidAccessError'));
    }
    return origGetStats.apply(this, arguments);
  };
}

function shimAddTrackRemoveTrackWithNative(window) {
  // shim addTrack/removeTrack with native variants in order to make
  // the interactions with legacy getLocalStreams behave as in other browsers.
  // Keeps a mapping stream.id => [stream, rtpsenders...]
  window.RTCPeerConnection.prototype.getLocalStreams = function getLocalStreams() {
    var _this8 = this;

    this._shimmedLocalStreams = this._shimmedLocalStreams || {};
    return Object.keys(this._shimmedLocalStreams).map(function (streamId) {
      return _this8._shimmedLocalStreams[streamId][0];
    });
  };

  var origAddTrack = window.RTCPeerConnection.prototype.addTrack;
  window.RTCPeerConnection.prototype.addTrack = function addTrack(track, stream) {
    if (!stream) {
      return origAddTrack.apply(this, arguments);
    }
    this._shimmedLocalStreams = this._shimmedLocalStreams || {};

    var sender = origAddTrack.apply(this, arguments);
    if (!this._shimmedLocalStreams[stream.id]) {
      this._shimmedLocalStreams[stream.id] = [stream, sender];
    } else if (this._shimmedLocalStreams[stream.id].indexOf(sender) === -1) {
      this._shimmedLocalStreams[stream.id].push(sender);
    }
    return sender;
  };

  var origAddStream = window.RTCPeerConnection.prototype.addStream;
  window.RTCPeerConnection.prototype.addStream = function addStream(stream) {
    var _this9 = this;

    this._shimmedLocalStreams = this._shimmedLocalStreams || {};

    stream.getTracks().forEach(function (track) {
      var alreadyExists = _this9.getSenders().find(function (s) {
        return s.track === track;
      });
      if (alreadyExists) {
        throw new DOMException('Track already exists.', 'InvalidAccessError');
      }
    });
    var existingSenders = this.getSenders();
    origAddStream.apply(this, arguments);
    var newSenders = this.getSenders().filter(function (newSender) {
      return existingSenders.indexOf(newSender) === -1;
    });
    this._shimmedLocalStreams[stream.id] = [stream].concat(newSenders);
  };

  var origRemoveStream = window.RTCPeerConnection.prototype.removeStream;
  window.RTCPeerConnection.prototype.removeStream = function removeStream(stream) {
    this._shimmedLocalStreams = this._shimmedLocalStreams || {};
    delete this._shimmedLocalStreams[stream.id];
    return origRemoveStream.apply(this, arguments);
  };

  var origRemoveTrack = window.RTCPeerConnection.prototype.removeTrack;
  window.RTCPeerConnection.prototype.removeTrack = function removeTrack(sender) {
    var _this10 = this;

    this._shimmedLocalStreams = this._shimmedLocalStreams || {};
    if (sender) {
      Object.keys(this._shimmedLocalStreams).forEach(function (streamId) {
        var idx = _this10._shimmedLocalStreams[streamId].indexOf(sender);
        if (idx !== -1) {
          _this10._shimmedLocalStreams[streamId].splice(idx, 1);
        }
        if (_this10._shimmedLocalStreams[streamId].length === 1) {
          delete _this10._shimmedLocalStreams[streamId];
        }
      });
    }
    return origRemoveTrack.apply(this, arguments);
  };
}

function shimAddTrackRemoveTrack(window) {
  if (!window.RTCPeerConnection) {
    return;
  }
  var browserDetails = utils.detectBrowser(window);
  // shim addTrack and removeTrack.
  if (window.RTCPeerConnection.prototype.addTrack && browserDetails.version >= 65) {
    return shimAddTrackRemoveTrackWithNative(window);
  }

  // also shim pc.getLocalStreams when addTrack is shimmed
  // to return the original streams.
  var origGetLocalStreams = window.RTCPeerConnection.prototype.getLocalStreams;
  window.RTCPeerConnection.prototype.getLocalStreams = function getLocalStreams() {
    var _this11 = this;

    var nativeStreams = origGetLocalStreams.apply(this);
    this._reverseStreams = this._reverseStreams || {};
    return nativeStreams.map(function (stream) {
      return _this11._reverseStreams[stream.id];
    });
  };

  var origAddStream = window.RTCPeerConnection.prototype.addStream;
  window.RTCPeerConnection.prototype.addStream = function addStream(stream) {
    var _this12 = this;

    this._streams = this._streams || {};
    this._reverseStreams = this._reverseStreams || {};

    stream.getTracks().forEach(function (track) {
      var alreadyExists = _this12.getSenders().find(function (s) {
        return s.track === track;
      });
      if (alreadyExists) {
        throw new DOMException('Track already exists.', 'InvalidAccessError');
      }
    });
    // Add identity mapping for consistency with addTrack.
    // Unless this is being used with a stream from addTrack.
    if (!this._reverseStreams[stream.id]) {
      var newStream = new window.MediaStream(stream.getTracks());
      this._streams[stream.id] = newStream;
      this._reverseStreams[newStream.id] = stream;
      stream = newStream;
    }
    origAddStream.apply(this, [stream]);
  };

  var origRemoveStream = window.RTCPeerConnection.prototype.removeStream;
  window.RTCPeerConnection.prototype.removeStream = function removeStream(stream) {
    this._streams = this._streams || {};
    this._reverseStreams = this._reverseStreams || {};

    origRemoveStream.apply(this, [this._streams[stream.id] || stream]);
    delete this._reverseStreams[this._streams[stream.id] ? this._streams[stream.id].id : stream.id];
    delete this._streams[stream.id];
  };

  window.RTCPeerConnection.prototype.addTrack = function addTrack(track, stream) {
    var _this13 = this;

    if (this.signalingState === 'closed') {
      throw new DOMException('The RTCPeerConnection\'s signalingState is \'closed\'.', 'InvalidStateError');
    }
    var streams = [].slice.call(arguments, 1);
    if (streams.length !== 1 || !streams[0].getTracks().find(function (t) {
      return t === track;
    })) {
      // this is not fully correct but all we can manage without
      // [[associated MediaStreams]] internal slot.
      throw new DOMException('The adapter.js addTrack polyfill only supports a single ' + ' stream which is associated with the specified track.', 'NotSupportedError');
    }

    var alreadyExists = this.getSenders().find(function (s) {
      return s.track === track;
    });
    if (alreadyExists) {
      throw new DOMException('Track already exists.', 'InvalidAccessError');
    }

    this._streams = this._streams || {};
    this._reverseStreams = this._reverseStreams || {};
    var oldStream = this._streams[stream.id];
    if (oldStream) {
      // this is using odd Chrome behaviour, use with caution:
      // https://bugs.chromium.org/p/webrtc/issues/detail?id=7815
      // Note: we rely on the high-level addTrack/dtmf shim to
      // create the sender with a dtmf sender.
      oldStream.addTrack(track);

      // Trigger ONN async.
      Promise.resolve().then(function () {
        _this13.dispatchEvent(new Event('negotiationneeded'));
      });
    } else {
      var newStream = new window.MediaStream([track]);
      this._streams[stream.id] = newStream;
      this._reverseStreams[newStream.id] = stream;
      this.addStream(newStream);
    }
    return this.getSenders().find(function (s) {
      return s.track === track;
    });
  };

  // replace the internal stream id with the external one and
  // vice versa.
  function replaceInternalStreamId(pc, description) {
    var sdp = description.sdp;
    Object.keys(pc._reverseStreams || []).forEach(function (internalId) {
      var externalStream = pc._reverseStreams[internalId];
      var internalStream = pc._streams[externalStream.id];
      sdp = sdp.replace(new RegExp(internalStream.id, 'g'), externalStream.id);
    });
    return new RTCSessionDescription({
      type: description.type,
      sdp: sdp
    });
  }
  function replaceExternalStreamId(pc, description) {
    var sdp = description.sdp;
    Object.keys(pc._reverseStreams || []).forEach(function (internalId) {
      var externalStream = pc._reverseStreams[internalId];
      var internalStream = pc._streams[externalStream.id];
      sdp = sdp.replace(new RegExp(externalStream.id, 'g'), internalStream.id);
    });
    return new RTCSessionDescription({
      type: description.type,
      sdp: sdp
    });
  }
  ['createOffer', 'createAnswer'].forEach(function (method) {
    var nativeMethod = window.RTCPeerConnection.prototype[method];
    var methodObj = _defineProperty({}, method, function () {
      var _this14 = this;

      var args = arguments;
      var isLegacyCall = arguments.length && typeof arguments[0] === 'function';
      if (isLegacyCall) {
        return nativeMethod.apply(this, [function (description) {
          var desc = replaceInternalStreamId(_this14, description);
          args[0].apply(null, [desc]);
        }, function (err) {
          if (args[1]) {
            args[1].apply(null, err);
          }
        }, arguments[2]]);
      }
      return nativeMethod.apply(this, arguments).then(function (description) {
        return replaceInternalStreamId(_this14, description);
      });
    });
    window.RTCPeerConnection.prototype[method] = methodObj[method];
  });

  var origSetLocalDescription = window.RTCPeerConnection.prototype.setLocalDescription;
  window.RTCPeerConnection.prototype.setLocalDescription = function setLocalDescription() {
    if (!arguments.length || !arguments[0].type) {
      return origSetLocalDescription.apply(this, arguments);
    }
    arguments[0] = replaceExternalStreamId(this, arguments[0]);
    return origSetLocalDescription.apply(this, arguments);
  };

  // TODO: mangle getStats: https://w3c.github.io/webrtc-stats/#dom-rtcmediastreamstats-streamidentifier

  var origLocalDescription = Object.getOwnPropertyDescriptor(window.RTCPeerConnection.prototype, 'localDescription');
  Object.defineProperty(window.RTCPeerConnection.prototype, 'localDescription', {
    get: function get() {
      var description = origLocalDescription.get.apply(this);
      if (description.type === '') {
        return description;
      }
      return replaceInternalStreamId(this, description);
    }
  });

  window.RTCPeerConnection.prototype.removeTrack = function removeTrack(sender) {
    var _this15 = this;

    if (this.signalingState === 'closed') {
      throw new DOMException('The RTCPeerConnection\'s signalingState is \'closed\'.', 'InvalidStateError');
    }
    // We can not yet check for sender instanceof RTCRtpSender
    // since we shim RTPSender. So we check if sender._pc is set.
    if (!sender._pc) {
      throw new DOMException('Argument 1 of RTCPeerConnection.removeTrack ' + 'does not implement interface RTCRtpSender.', 'TypeError');
    }
    var isLocal = sender._pc === this;
    if (!isLocal) {
      throw new DOMException('Sender was not created by this connection.', 'InvalidAccessError');
    }

    // Search for the native stream the senders track belongs to.
    this._streams = this._streams || {};
    var stream = void 0;
    Object.keys(this._streams).forEach(function (streamid) {
      var hasTrack = _this15._streams[streamid].getTracks().find(function (track) {
        return sender.track === track;
      });
      if (hasTrack) {
        stream = _this15._streams[streamid];
      }
    });

    if (stream) {
      if (stream.getTracks().length === 1) {
        // if this is the last track of the stream, remove the stream. This
        // takes care of any shimmed _senders.
        this.removeStream(this._reverseStreams[stream.id]);
      } else {
        // relying on the same odd chrome behaviour as above.
        stream.removeTrack(sender.track);
      }
      this.dispatchEvent(new Event('negotiationneeded'));
    }
  };
}

function shimPeerConnection(window) {
  var browserDetails = utils.detectBrowser(window);

  if (!window.RTCPeerConnection && window.webkitRTCPeerConnection) {
    // very basic support for old versions.
    window.RTCPeerConnection = window.webkitRTCPeerConnection;
  }
  if (!window.RTCPeerConnection) {
    return;
  }

  var addIceCandidateNullSupported = window.RTCPeerConnection.prototype.addIceCandidate.length === 0;

  // shim implicit creation of RTCSessionDescription/RTCIceCandidate
  if (browserDetails.version < 53) {
    ['setLocalDescription', 'setRemoteDescription', 'addIceCandidate'].forEach(function (method) {
      var nativeMethod = window.RTCPeerConnection.prototype[method];
      var methodObj = _defineProperty({}, method, function () {
        arguments[0] = new (method === 'addIceCandidate' ? window.RTCIceCandidate : window.RTCSessionDescription)(arguments[0]);
        return nativeMethod.apply(this, arguments);
      });
      window.RTCPeerConnection.prototype[method] = methodObj[method];
    });
  }

  // support for addIceCandidate(null or undefined)
  var nativeAddIceCandidate = window.RTCPeerConnection.prototype.addIceCandidate;
  window.RTCPeerConnection.prototype.addIceCandidate = function addIceCandidate() {
    if (!addIceCandidateNullSupported && !arguments[0]) {
      if (arguments[1]) {
        arguments[1].apply(null);
      }
      return Promise.resolve();
    }
    // Firefox 68+ emits and processes {candidate: "", ...}, ignore
    // in older versions. Native support planned for Chrome M77.
    if (browserDetails.version < 78 && arguments[0] && arguments[0].candidate === '') {
      return Promise.resolve();
    }
    return nativeAddIceCandidate.apply(this, arguments);
  };
}

// Attempt to fix ONN in plan-b mode.
function fixNegotiationNeeded(window) {
  var browserDetails = utils.detectBrowser(window);
  utils.wrapPeerConnectionEvent(window, 'negotiationneeded', function (e) {
    var pc = e.target;
    if (browserDetails.version < 72 || pc.getConfiguration && pc.getConfiguration().sdpSemantics === 'plan-b') {
      if (pc.signalingState !== 'stable') {
        return;
      }
    }
    return e;
  });
}

},{"../utils.js":11,"./getdisplaymedia":4,"./getusermedia":5}],4:[function(require,module,exports){
/*
 *  Copyright (c) 2018 The adapter.js project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shimGetDisplayMedia = shimGetDisplayMedia;
function shimGetDisplayMedia(window, getSourceId) {
  if (window.navigator.mediaDevices && 'getDisplayMedia' in window.navigator.mediaDevices) {
    return;
  }
  if (!window.navigator.mediaDevices) {
    return;
  }
  // getSourceId is a function that returns a promise resolving with
  // the sourceId of the screen/window/tab to be shared.
  if (typeof getSourceId !== 'function') {
    console.error('shimGetDisplayMedia: getSourceId argument is not ' + 'a function');
    return;
  }
  window.navigator.mediaDevices.getDisplayMedia = function getDisplayMedia(constraints) {
    return getSourceId(constraints).then(function (sourceId) {
      var widthSpecified = constraints.video && constraints.video.width;
      var heightSpecified = constraints.video && constraints.video.height;
      var frameRateSpecified = constraints.video && constraints.video.frameRate;
      constraints.video = {
        mandatory: {
          chromeMediaSource: 'desktop',
          chromeMediaSourceId: sourceId,
          maxFrameRate: frameRateSpecified || 3
        }
      };
      if (widthSpecified) {
        constraints.video.mandatory.maxWidth = widthSpecified;
      }
      if (heightSpecified) {
        constraints.video.mandatory.maxHeight = heightSpecified;
      }
      return window.navigator.mediaDevices.getUserMedia(constraints);
    });
  };
}

},{}],5:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.shimGetUserMedia = shimGetUserMedia;

var _utils = require('../utils.js');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var logging = utils.log;

function shimGetUserMedia(window) {
  var navigator = window && window.navigator;

  if (!navigator.mediaDevices) {
    return;
  }

  var browserDetails = utils.detectBrowser(window);

  var constraintsToChrome_ = function constraintsToChrome_(c) {
    if ((typeof c === 'undefined' ? 'undefined' : _typeof(c)) !== 'object' || c.mandatory || c.optional) {
      return c;
    }
    var cc = {};
    Object.keys(c).forEach(function (key) {
      if (key === 'require' || key === 'advanced' || key === 'mediaSource') {
        return;
      }
      var r = _typeof(c[key]) === 'object' ? c[key] : { ideal: c[key] };
      if (r.exact !== undefined && typeof r.exact === 'number') {
        r.min = r.max = r.exact;
      }
      var oldname_ = function oldname_(prefix, name) {
        if (prefix) {
          return prefix + name.charAt(0).toUpperCase() + name.slice(1);
        }
        return name === 'deviceId' ? 'sourceId' : name;
      };
      if (r.ideal !== undefined) {
        cc.optional = cc.optional || [];
        var oc = {};
        if (typeof r.ideal === 'number') {
          oc[oldname_('min', key)] = r.ideal;
          cc.optional.push(oc);
          oc = {};
          oc[oldname_('max', key)] = r.ideal;
          cc.optional.push(oc);
        } else {
          oc[oldname_('', key)] = r.ideal;
          cc.optional.push(oc);
        }
      }
      if (r.exact !== undefined && typeof r.exact !== 'number') {
        cc.mandatory = cc.mandatory || {};
        cc.mandatory[oldname_('', key)] = r.exact;
      } else {
        ['min', 'max'].forEach(function (mix) {
          if (r[mix] !== undefined) {
            cc.mandatory = cc.mandatory || {};
            cc.mandatory[oldname_(mix, key)] = r[mix];
          }
        });
      }
    });
    if (c.advanced) {
      cc.optional = (cc.optional || []).concat(c.advanced);
    }
    return cc;
  };

  var shimConstraints_ = function shimConstraints_(constraints, func) {
    if (browserDetails.version >= 61) {
      return func(constraints);
    }
    constraints = JSON.parse(JSON.stringify(constraints));
    if (constraints && _typeof(constraints.audio) === 'object') {
      var remap = function remap(obj, a, b) {
        if (a in obj && !(b in obj)) {
          obj[b] = obj[a];
          delete obj[a];
        }
      };
      constraints = JSON.parse(JSON.stringify(constraints));
      remap(constraints.audio, 'autoGainControl', 'googAutoGainControl');
      remap(constraints.audio, 'noiseSuppression', 'googNoiseSuppression');
      constraints.audio = constraintsToChrome_(constraints.audio);
    }
    if (constraints && _typeof(constraints.video) === 'object') {
      // Shim facingMode for mobile & surface pro.
      var face = constraints.video.facingMode;
      face = face && ((typeof face === 'undefined' ? 'undefined' : _typeof(face)) === 'object' ? face : { ideal: face });
      var getSupportedFacingModeLies = browserDetails.version < 66;

      if (face && (face.exact === 'user' || face.exact === 'environment' || face.ideal === 'user' || face.ideal === 'environment') && !(navigator.mediaDevices.getSupportedConstraints && navigator.mediaDevices.getSupportedConstraints().facingMode && !getSupportedFacingModeLies)) {
        delete constraints.video.facingMode;
        var matches = void 0;
        if (face.exact === 'environment' || face.ideal === 'environment') {
          matches = ['back', 'rear'];
        } else if (face.exact === 'user' || face.ideal === 'user') {
          matches = ['front'];
        }
        if (matches) {
          // Look for matches in label, or use last cam for back (typical).
          return navigator.mediaDevices.enumerateDevices().then(function (devices) {
            devices = devices.filter(function (d) {
              return d.kind === 'videoinput';
            });
            var dev = devices.find(function (d) {
              return matches.some(function (match) {
                return d.label.toLowerCase().includes(match);
              });
            });
            if (!dev && devices.length && matches.includes('back')) {
              dev = devices[devices.length - 1]; // more likely the back cam
            }
            if (dev) {
              constraints.video.deviceId = face.exact ? { exact: dev.deviceId } : { ideal: dev.deviceId };
            }
            constraints.video = constraintsToChrome_(constraints.video);
            logging('chrome: ' + JSON.stringify(constraints));
            return func(constraints);
          });
        }
      }
      constraints.video = constraintsToChrome_(constraints.video);
    }
    logging('chrome: ' + JSON.stringify(constraints));
    return func(constraints);
  };

  var shimError_ = function shimError_(e) {
    if (browserDetails.version >= 64) {
      return e;
    }
    return {
      name: {
        PermissionDeniedError: 'NotAllowedError',
        PermissionDismissedError: 'NotAllowedError',
        InvalidStateError: 'NotAllowedError',
        DevicesNotFoundError: 'NotFoundError',
        ConstraintNotSatisfiedError: 'OverconstrainedError',
        TrackStartError: 'NotReadableError',
        MediaDeviceFailedDueToShutdown: 'NotAllowedError',
        MediaDeviceKillSwitchOn: 'NotAllowedError',
        TabCaptureError: 'AbortError',
        ScreenCaptureError: 'AbortError',
        DeviceCaptureError: 'AbortError'
      }[e.name] || e.name,
      message: e.message,
      constraint: e.constraint || e.constraintName,
      toString: function toString() {
        return this.name + (this.message && ': ') + this.message;
      }
    };
  };

  var getUserMedia_ = function getUserMedia_(constraints, onSuccess, onError) {
    shimConstraints_(constraints, function (c) {
      navigator.webkitGetUserMedia(c, onSuccess, function (e) {
        if (onError) {
          onError(shimError_(e));
        }
      });
    });
  };
  navigator.getUserMedia = getUserMedia_.bind(navigator);

  // Even though Chrome 45 has navigator.mediaDevices and a getUserMedia
  // function which returns a Promise, it does not accept spec-style
  // constraints.
  if (navigator.mediaDevices.getUserMedia) {
    var origGetUserMedia = navigator.mediaDevices.getUserMedia.bind(navigator.mediaDevices);
    navigator.mediaDevices.getUserMedia = function (cs) {
      return shimConstraints_(cs, function (c) {
        return origGetUserMedia(c).then(function (stream) {
          if (c.audio && !stream.getAudioTracks().length || c.video && !stream.getVideoTracks().length) {
            stream.getTracks().forEach(function (track) {
              track.stop();
            });
            throw new DOMException('', 'NotFoundError');
          }
          return stream;
        }, function (e) {
          return Promise.reject(shimError_(e));
        });
      });
    };
  }
}

},{"../utils.js":11}],6:[function(require,module,exports){
/*
 *  Copyright (c) 2017 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.shimRTCIceCandidate = shimRTCIceCandidate;
exports.shimMaxMessageSize = shimMaxMessageSize;
exports.shimSendThrowTypeError = shimSendThrowTypeError;
exports.shimConnectionState = shimConnectionState;
exports.removeAllowExtmapMixed = removeAllowExtmapMixed;

var _sdp = require('sdp');

var _sdp2 = _interopRequireDefault(_sdp);

var _utils = require('./utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function shimRTCIceCandidate(window) {
  // foundation is arbitrarily chosen as an indicator for full support for
  // https://w3c.github.io/webrtc-pc/#rtcicecandidate-interface
  if (!window.RTCIceCandidate || window.RTCIceCandidate && 'foundation' in window.RTCIceCandidate.prototype) {
    return;
  }

  var NativeRTCIceCandidate = window.RTCIceCandidate;
  window.RTCIceCandidate = function RTCIceCandidate(args) {
    // Remove the a= which shouldn't be part of the candidate string.
    if ((typeof args === 'undefined' ? 'undefined' : _typeof(args)) === 'object' && args.candidate && args.candidate.indexOf('a=') === 0) {
      args = JSON.parse(JSON.stringify(args));
      args.candidate = args.candidate.substr(2);
    }

    if (args.candidate && args.candidate.length) {
      // Augment the native candidate with the parsed fields.
      var nativeCandidate = new NativeRTCIceCandidate(args);
      var parsedCandidate = _sdp2.default.parseCandidate(args.candidate);
      var augmentedCandidate = Object.assign(nativeCandidate, parsedCandidate);

      // Add a serializer that does not serialize the extra attributes.
      augmentedCandidate.toJSON = function toJSON() {
        return {
          candidate: augmentedCandidate.candidate,
          sdpMid: augmentedCandidate.sdpMid,
          sdpMLineIndex: augmentedCandidate.sdpMLineIndex,
          usernameFragment: augmentedCandidate.usernameFragment
        };
      };
      return augmentedCandidate;
    }
    return new NativeRTCIceCandidate(args);
  };
  window.RTCIceCandidate.prototype = NativeRTCIceCandidate.prototype;

  // Hook up the augmented candidate in onicecandidate and
  // addEventListener('icecandidate', ...)
  utils.wrapPeerConnectionEvent(window, 'icecandidate', function (e) {
    if (e.candidate) {
      Object.defineProperty(e, 'candidate', {
        value: new window.RTCIceCandidate(e.candidate),
        writable: 'false'
      });
    }
    return e;
  });
}

function shimMaxMessageSize(window) {
  if (!window.RTCPeerConnection) {
    return;
  }
  var browserDetails = utils.detectBrowser(window);

  if (!('sctp' in window.RTCPeerConnection.prototype)) {
    Object.defineProperty(window.RTCPeerConnection.prototype, 'sctp', {
      get: function get() {
        return typeof this._sctp === 'undefined' ? null : this._sctp;
      }
    });
  }

  var sctpInDescription = function sctpInDescription(description) {
    if (!description || !description.sdp) {
      return false;
    }
    var sections = _sdp2.default.splitSections(description.sdp);
    sections.shift();
    return sections.some(function (mediaSection) {
      var mLine = _sdp2.default.parseMLine(mediaSection);
      return mLine && mLine.kind === 'application' && mLine.protocol.indexOf('SCTP') !== -1;
    });
  };

  var getRemoteFirefoxVersion = function getRemoteFirefoxVersion(description) {
    // TODO: Is there a better solution for detecting Firefox?
    var match = description.sdp.match(/mozilla...THIS_IS_SDPARTA-(\d+)/);
    if (match === null || match.length < 2) {
      return -1;
    }
    var version = parseInt(match[1], 10);
    // Test for NaN (yes, this is ugly)
    return version !== version ? -1 : version;
  };

  var getCanSendMaxMessageSize = function getCanSendMaxMessageSize(remoteIsFirefox) {
    // Every implementation we know can send at least 64 KiB.
    // Note: Although Chrome is technically able to send up to 256 KiB, the
    //       data does not reach the other peer reliably.
    //       See: https://bugs.chromium.org/p/webrtc/issues/detail?id=8419
    var canSendMaxMessageSize = 65536;
    if (browserDetails.browser === 'firefox') {
      if (browserDetails.version < 57) {
        if (remoteIsFirefox === -1) {
          // FF < 57 will send in 16 KiB chunks using the deprecated PPID
          // fragmentation.
          canSendMaxMessageSize = 16384;
        } else {
          // However, other FF (and RAWRTC) can reassemble PPID-fragmented
          // messages. Thus, supporting ~2 GiB when sending.
          canSendMaxMessageSize = 2147483637;
        }
      } else if (browserDetails.version < 60) {
        // Currently, all FF >= 57 will reset the remote maximum message size
        // to the default value when a data channel is created at a later
        // stage. :(
        // See: https://bugzilla.mozilla.org/show_bug.cgi?id=1426831
        canSendMaxMessageSize = browserDetails.version === 57 ? 65535 : 65536;
      } else {
        // FF >= 60 supports sending ~2 GiB
        canSendMaxMessageSize = 2147483637;
      }
    }
    return canSendMaxMessageSize;
  };

  var getMaxMessageSize = function getMaxMessageSize(description, remoteIsFirefox) {
    // Note: 65536 bytes is the default value from the SDP spec. Also,
    //       every implementation we know supports receiving 65536 bytes.
    var maxMessageSize = 65536;

    // FF 57 has a slightly incorrect default remote max message size, so
    // we need to adjust it here to avoid a failure when sending.
    // See: https://bugzilla.mozilla.org/show_bug.cgi?id=1425697
    if (browserDetails.browser === 'firefox' && browserDetails.version === 57) {
      maxMessageSize = 65535;
    }

    var match = _sdp2.default.matchPrefix(description.sdp, 'a=max-message-size:');
    if (match.length > 0) {
      maxMessageSize = parseInt(match[0].substr(19), 10);
    } else if (browserDetails.browser === 'firefox' && remoteIsFirefox !== -1) {
      // If the maximum message size is not present in the remote SDP and
      // both local and remote are Firefox, the remote peer can receive
      // ~2 GiB.
      maxMessageSize = 2147483637;
    }
    return maxMessageSize;
  };

  var origSetRemoteDescription = window.RTCPeerConnection.prototype.setRemoteDescription;
  window.RTCPeerConnection.prototype.setRemoteDescription = function setRemoteDescription() {
    this._sctp = null;
    // Chrome decided to not expose .sctp in plan-b mode.
    // As usual, adapter.js has to do an 'ugly worakaround'
    // to cover up the mess.
    if (browserDetails.browser === 'chrome' && browserDetails.version >= 76) {
      var _getConfiguration = this.getConfiguration(),
          sdpSemantics = _getConfiguration.sdpSemantics;

      if (sdpSemantics === 'plan-b') {
        Object.defineProperty(this, 'sctp', {
          get: function get() {
            return typeof this._sctp === 'undefined' ? null : this._sctp;
          },

          enumerable: true,
          configurable: true
        });
      }
    }

    if (sctpInDescription(arguments[0])) {
      // Check if the remote is FF.
      var isFirefox = getRemoteFirefoxVersion(arguments[0]);

      // Get the maximum message size the local peer is capable of sending
      var canSendMMS = getCanSendMaxMessageSize(isFirefox);

      // Get the maximum message size of the remote peer.
      var remoteMMS = getMaxMessageSize(arguments[0], isFirefox);

      // Determine final maximum message size
      var maxMessageSize = void 0;
      if (canSendMMS === 0 && remoteMMS === 0) {
        maxMessageSize = Number.POSITIVE_INFINITY;
      } else if (canSendMMS === 0 || remoteMMS === 0) {
        maxMessageSize = Math.max(canSendMMS, remoteMMS);
      } else {
        maxMessageSize = Math.min(canSendMMS, remoteMMS);
      }

      // Create a dummy RTCSctpTransport object and the 'maxMessageSize'
      // attribute.
      var sctp = {};
      Object.defineProperty(sctp, 'maxMessageSize', {
        get: function get() {
          return maxMessageSize;
        }
      });
      this._sctp = sctp;
    }

    return origSetRemoteDescription.apply(this, arguments);
  };
}

function shimSendThrowTypeError(window) {
  if (!(window.RTCPeerConnection && 'createDataChannel' in window.RTCPeerConnection.prototype)) {
    return;
  }

  // Note: Although Firefox >= 57 has a native implementation, the maximum
  //       message size can be reset for all data channels at a later stage.
  //       See: https://bugzilla.mozilla.org/show_bug.cgi?id=1426831

  function wrapDcSend(dc, pc) {
    var origDataChannelSend = dc.send;
    dc.send = function send() {
      var data = arguments[0];
      var length = data.length || data.size || data.byteLength;
      if (dc.readyState === 'open' && pc.sctp && length > pc.sctp.maxMessageSize) {
        throw new TypeError('Message too large (can send a maximum of ' + pc.sctp.maxMessageSize + ' bytes)');
      }
      return origDataChannelSend.apply(dc, arguments);
    };
  }
  var origCreateDataChannel = window.RTCPeerConnection.prototype.createDataChannel;
  window.RTCPeerConnection.prototype.createDataChannel = function createDataChannel() {
    var dataChannel = origCreateDataChannel.apply(this, arguments);
    wrapDcSend(dataChannel, this);
    return dataChannel;
  };
  utils.wrapPeerConnectionEvent(window, 'datachannel', function (e) {
    wrapDcSend(e.channel, e.target);
    return e;
  });
}

/* shims RTCConnectionState by pretending it is the same as iceConnectionState.
 * See https://bugs.chromium.org/p/webrtc/issues/detail?id=6145#c12
 * for why this is a valid hack in Chrome. In Firefox it is slightly incorrect
 * since DTLS failures would be hidden. See
 * https://bugzilla.mozilla.org/show_bug.cgi?id=1265827
 * for the Firefox tracking bug.
 */
function shimConnectionState(window) {
  if (!window.RTCPeerConnection || 'connectionState' in window.RTCPeerConnection.prototype) {
    return;
  }
  var proto = window.RTCPeerConnection.prototype;
  Object.defineProperty(proto, 'connectionState', {
    get: function get() {
      return {
        completed: 'connected',
        checking: 'connecting'
      }[this.iceConnectionState] || this.iceConnectionState;
    },

    enumerable: true,
    configurable: true
  });
  Object.defineProperty(proto, 'onconnectionstatechange', {
    get: function get() {
      return this._onconnectionstatechange || null;
    },
    set: function set(cb) {
      if (this._onconnectionstatechange) {
        this.removeEventListener('connectionstatechange', this._onconnectionstatechange);
        delete this._onconnectionstatechange;
      }
      if (cb) {
        this.addEventListener('connectionstatechange', this._onconnectionstatechange = cb);
      }
    },

    enumerable: true,
    configurable: true
  });

  ['setLocalDescription', 'setRemoteDescription'].forEach(function (method) {
    var origMethod = proto[method];
    proto[method] = function () {
      if (!this._connectionstatechangepoly) {
        this._connectionstatechangepoly = function (e) {
          var pc = e.target;
          if (pc._lastConnectionState !== pc.connectionState) {
            pc._lastConnectionState = pc.connectionState;
            var newEvent = new Event('connectionstatechange', e);
            pc.dispatchEvent(newEvent);
          }
          return e;
        };
        this.addEventListener('iceconnectionstatechange', this._connectionstatechangepoly);
      }
      return origMethod.apply(this, arguments);
    };
  });
}

function removeAllowExtmapMixed(window) {
  /* remove a=extmap-allow-mixed for webrtc.org < M71 */
  if (!window.RTCPeerConnection) {
    return;
  }
  var browserDetails = utils.detectBrowser(window);
  if (browserDetails.browser === 'chrome' && browserDetails.version >= 71) {
    return;
  }
  if (browserDetails.browser === 'safari' && browserDetails.version >= 605) {
    return;
  }
  var nativeSRD = window.RTCPeerConnection.prototype.setRemoteDescription;
  window.RTCPeerConnection.prototype.setRemoteDescription = function setRemoteDescription(desc) {
    if (desc && desc.sdp && desc.sdp.indexOf('\na=extmap-allow-mixed') !== -1) {
      desc.sdp = desc.sdp.split('\n').filter(function (line) {
        return line.trim() !== 'a=extmap-allow-mixed';
      }).join('\n');
    }
    return nativeSRD.apply(this, arguments);
  };
}

},{"./utils":11,"sdp":13}],7:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shimGetDisplayMedia = exports.shimGetUserMedia = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _getusermedia = require('./getusermedia');

Object.defineProperty(exports, 'shimGetUserMedia', {
  enumerable: true,
  get: function get() {
    return _getusermedia.shimGetUserMedia;
  }
});

var _getdisplaymedia = require('./getdisplaymedia');

Object.defineProperty(exports, 'shimGetDisplayMedia', {
  enumerable: true,
  get: function get() {
    return _getdisplaymedia.shimGetDisplayMedia;
  }
});
exports.shimOnTrack = shimOnTrack;
exports.shimPeerConnection = shimPeerConnection;
exports.shimSenderGetStats = shimSenderGetStats;
exports.shimReceiverGetStats = shimReceiverGetStats;
exports.shimRemoveStream = shimRemoveStream;
exports.shimRTCDataChannel = shimRTCDataChannel;
exports.shimAddTransceiver = shimAddTransceiver;
exports.shimGetParameters = shimGetParameters;
exports.shimCreateOffer = shimCreateOffer;
exports.shimCreateAnswer = shimCreateAnswer;

var _utils = require('../utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function shimOnTrack(window) {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCTrackEvent && 'receiver' in window.RTCTrackEvent.prototype && !('transceiver' in window.RTCTrackEvent.prototype)) {
    Object.defineProperty(window.RTCTrackEvent.prototype, 'transceiver', {
      get: function get() {
        return { receiver: this.receiver };
      }
    });
  }
}

function shimPeerConnection(window) {
  var browserDetails = utils.detectBrowser(window);

  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) !== 'object' || !(window.RTCPeerConnection || window.mozRTCPeerConnection)) {
    return; // probably media.peerconnection.enabled=false in about:config
  }
  if (!window.RTCPeerConnection && window.mozRTCPeerConnection) {
    // very basic support for old versions.
    window.RTCPeerConnection = window.mozRTCPeerConnection;
  }

  if (browserDetails.version < 53) {
    // shim away need for obsolete RTCIceCandidate/RTCSessionDescription.
    ['setLocalDescription', 'setRemoteDescription', 'addIceCandidate'].forEach(function (method) {
      var nativeMethod = window.RTCPeerConnection.prototype[method];
      var methodObj = _defineProperty({}, method, function () {
        arguments[0] = new (method === 'addIceCandidate' ? window.RTCIceCandidate : window.RTCSessionDescription)(arguments[0]);
        return nativeMethod.apply(this, arguments);
      });
      window.RTCPeerConnection.prototype[method] = methodObj[method];
    });
  }

  // support for addIceCandidate(null or undefined)
  // as well as ignoring {sdpMid, candidate: ""}
  if (browserDetails.version < 68) {
    var nativeAddIceCandidate = window.RTCPeerConnection.prototype.addIceCandidate;
    window.RTCPeerConnection.prototype.addIceCandidate = function addIceCandidate() {
      if (!arguments[0]) {
        if (arguments[1]) {
          arguments[1].apply(null);
        }
        return Promise.resolve();
      }
      // Firefox 68+ emits and processes {candidate: "", ...}, ignore
      // in older versions.
      if (arguments[0] && arguments[0].candidate === '') {
        return Promise.resolve();
      }
      return nativeAddIceCandidate.apply(this, arguments);
    };
  }

  var modernStatsTypes = {
    inboundrtp: 'inbound-rtp',
    outboundrtp: 'outbound-rtp',
    candidatepair: 'candidate-pair',
    localcandidate: 'local-candidate',
    remotecandidate: 'remote-candidate'
  };

  var nativeGetStats = window.RTCPeerConnection.prototype.getStats;
  window.RTCPeerConnection.prototype.getStats = function getStats() {
    var _arguments = Array.prototype.slice.call(arguments),
        selector = _arguments[0],
        onSucc = _arguments[1],
        onErr = _arguments[2];

    return nativeGetStats.apply(this, [selector || null]).then(function (stats) {
      if (browserDetails.version < 53 && !onSucc) {
        // Shim only promise getStats with spec-hyphens in type names
        // Leave callback version alone; misc old uses of forEach before Map
        try {
          stats.forEach(function (stat) {
            stat.type = modernStatsTypes[stat.type] || stat.type;
          });
        } catch (e) {
          if (e.name !== 'TypeError') {
            throw e;
          }
          // Avoid TypeError: "type" is read-only, in old versions. 34-43ish
          stats.forEach(function (stat, i) {
            stats.set(i, Object.assign({}, stat, {
              type: modernStatsTypes[stat.type] || stat.type
            }));
          });
        }
      }
      return stats;
    }).then(onSucc, onErr);
  };
}

function shimSenderGetStats(window) {
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection && window.RTCRtpSender)) {
    return;
  }
  if (window.RTCRtpSender && 'getStats' in window.RTCRtpSender.prototype) {
    return;
  }
  var origGetSenders = window.RTCPeerConnection.prototype.getSenders;
  if (origGetSenders) {
    window.RTCPeerConnection.prototype.getSenders = function getSenders() {
      var _this = this;

      var senders = origGetSenders.apply(this, []);
      senders.forEach(function (sender) {
        return sender._pc = _this;
      });
      return senders;
    };
  }

  var origAddTrack = window.RTCPeerConnection.prototype.addTrack;
  if (origAddTrack) {
    window.RTCPeerConnection.prototype.addTrack = function addTrack() {
      var sender = origAddTrack.apply(this, arguments);
      sender._pc = this;
      return sender;
    };
  }
  window.RTCRtpSender.prototype.getStats = function getStats() {
    return this.track ? this._pc.getStats(this.track) : Promise.resolve(new Map());
  };
}

function shimReceiverGetStats(window) {
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection && window.RTCRtpSender)) {
    return;
  }
  if (window.RTCRtpSender && 'getStats' in window.RTCRtpReceiver.prototype) {
    return;
  }
  var origGetReceivers = window.RTCPeerConnection.prototype.getReceivers;
  if (origGetReceivers) {
    window.RTCPeerConnection.prototype.getReceivers = function getReceivers() {
      var _this2 = this;

      var receivers = origGetReceivers.apply(this, []);
      receivers.forEach(function (receiver) {
        return receiver._pc = _this2;
      });
      return receivers;
    };
  }
  utils.wrapPeerConnectionEvent(window, 'track', function (e) {
    e.receiver._pc = e.srcElement;
    return e;
  });
  window.RTCRtpReceiver.prototype.getStats = function getStats() {
    return this._pc.getStats(this.track);
  };
}

function shimRemoveStream(window) {
  if (!window.RTCPeerConnection || 'removeStream' in window.RTCPeerConnection.prototype) {
    return;
  }
  window.RTCPeerConnection.prototype.removeStream = function removeStream(stream) {
    var _this3 = this;

    utils.deprecated('removeStream', 'removeTrack');
    this.getSenders().forEach(function (sender) {
      if (sender.track && stream.getTracks().includes(sender.track)) {
        _this3.removeTrack(sender);
      }
    });
  };
}

function shimRTCDataChannel(window) {
  // rename DataChannel to RTCDataChannel (native fix in FF60):
  // https://bugzilla.mozilla.org/show_bug.cgi?id=1173851
  if (window.DataChannel && !window.RTCDataChannel) {
    window.RTCDataChannel = window.DataChannel;
  }
}

function shimAddTransceiver(window) {
  // https://github.com/webrtcHacks/adapter/issues/998#issuecomment-516921647
  // Firefox ignores the init sendEncodings options passed to addTransceiver
  // https://bugzilla.mozilla.org/show_bug.cgi?id=1396918
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection)) {
    return;
  }
  var origAddTransceiver = window.RTCPeerConnection.prototype.addTransceiver;
  if (origAddTransceiver) {
    window.RTCPeerConnection.prototype.addTransceiver = function addTransceiver() {
      this.setParametersPromises = [];
      var initParameters = arguments[1];
      var shouldPerformCheck = initParameters && 'sendEncodings' in initParameters;
      if (shouldPerformCheck) {
        // If sendEncodings params are provided, validate grammar
        initParameters.sendEncodings.forEach(function (encodingParam) {
          if ('rid' in encodingParam) {
            var ridRegex = /^[a-z0-9]{0,16}$/i;
            if (!ridRegex.test(encodingParam.rid)) {
              throw new TypeError('Invalid RID value provided.');
            }
          }
          if ('scaleResolutionDownBy' in encodingParam) {
            if (!(parseFloat(encodingParam.scaleResolutionDownBy) >= 1.0)) {
              throw new RangeError('scale_resolution_down_by must be >= 1.0');
            }
          }
          if ('maxFramerate' in encodingParam) {
            if (!(parseFloat(encodingParam.maxFramerate) >= 0)) {
              throw new RangeError('max_framerate must be >= 0.0');
            }
          }
        });
      }
      var transceiver = origAddTransceiver.apply(this, arguments);
      if (shouldPerformCheck) {
        // Check if the init options were applied. If not we do this in an
        // asynchronous way and save the promise reference in a global object.
        // This is an ugly hack, but at the same time is way more robust than
        // checking the sender parameters before and after the createOffer
        // Also note that after the createoffer we are not 100% sure that
        // the params were asynchronously applied so we might miss the
        // opportunity to recreate offer.
        var sender = transceiver.sender;

        var params = sender.getParameters();
        if (!('encodings' in params) ||
        // Avoid being fooled by patched getParameters() below.
        params.encodings.length === 1 && Object.keys(params.encodings[0]).length === 0) {
          params.encodings = initParameters.sendEncodings;
          sender.sendEncodings = initParameters.sendEncodings;
          this.setParametersPromises.push(sender.setParameters(params).then(function () {
            delete sender.sendEncodings;
          }).catch(function () {
            delete sender.sendEncodings;
          }));
        }
      }
      return transceiver;
    };
  }
}

function shimGetParameters(window) {
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCRtpSender)) {
    return;
  }
  var origGetParameters = window.RTCRtpSender.prototype.getParameters;
  if (origGetParameters) {
    window.RTCRtpSender.prototype.getParameters = function getParameters() {
      var params = origGetParameters.apply(this, arguments);
      if (!('encodings' in params)) {
        params.encodings = [].concat(this.sendEncodings || [{}]);
      }
      return params;
    };
  }
}

function shimCreateOffer(window) {
  // https://github.com/webrtcHacks/adapter/issues/998#issuecomment-516921647
  // Firefox ignores the init sendEncodings options passed to addTransceiver
  // https://bugzilla.mozilla.org/show_bug.cgi?id=1396918
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection)) {
    return;
  }
  var origCreateOffer = window.RTCPeerConnection.prototype.createOffer;
  window.RTCPeerConnection.prototype.createOffer = function createOffer() {
    var _this4 = this,
        _arguments2 = arguments;

    if (this.setParametersPromises && this.setParametersPromises.length) {
      return Promise.all(this.setParametersPromises).then(function () {
        return origCreateOffer.apply(_this4, _arguments2);
      }).finally(function () {
        _this4.setParametersPromises = [];
      });
    }
    return origCreateOffer.apply(this, arguments);
  };
}

function shimCreateAnswer(window) {
  // https://github.com/webrtcHacks/adapter/issues/998#issuecomment-516921647
  // Firefox ignores the init sendEncodings options passed to addTransceiver
  // https://bugzilla.mozilla.org/show_bug.cgi?id=1396918
  if (!((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCPeerConnection)) {
    return;
  }
  var origCreateAnswer = window.RTCPeerConnection.prototype.createAnswer;
  window.RTCPeerConnection.prototype.createAnswer = function createAnswer() {
    var _this5 = this,
        _arguments3 = arguments;

    if (this.setParametersPromises && this.setParametersPromises.length) {
      return Promise.all(this.setParametersPromises).then(function () {
        return origCreateAnswer.apply(_this5, _arguments3);
      }).finally(function () {
        _this5.setParametersPromises = [];
      });
    }
    return origCreateAnswer.apply(this, arguments);
  };
}

},{"../utils":11,"./getdisplaymedia":8,"./getusermedia":9}],8:[function(require,module,exports){
/*
 *  Copyright (c) 2018 The adapter.js project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shimGetDisplayMedia = shimGetDisplayMedia;
function shimGetDisplayMedia(window, preferredMediaSource) {
  if (window.navigator.mediaDevices && 'getDisplayMedia' in window.navigator.mediaDevices) {
    return;
  }
  if (!window.navigator.mediaDevices) {
    return;
  }
  window.navigator.mediaDevices.getDisplayMedia = function getDisplayMedia(constraints) {
    if (!(constraints && constraints.video)) {
      var err = new DOMException('getDisplayMedia without video ' + 'constraints is undefined');
      err.name = 'NotFoundError';
      // from https://heycam.github.io/webidl/#idl-DOMException-error-names
      err.code = 8;
      return Promise.reject(err);
    }
    if (constraints.video === true) {
      constraints.video = { mediaSource: preferredMediaSource };
    } else {
      constraints.video.mediaSource = preferredMediaSource;
    }
    return window.navigator.mediaDevices.getUserMedia(constraints);
  };
}

},{}],9:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.shimGetUserMedia = shimGetUserMedia;

var _utils = require('../utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function shimGetUserMedia(window) {
  var browserDetails = utils.detectBrowser(window);
  var navigator = window && window.navigator;
  var MediaStreamTrack = window && window.MediaStreamTrack;

  navigator.getUserMedia = function (constraints, onSuccess, onError) {
    // Replace Firefox 44+'s deprecation warning with unprefixed version.
    utils.deprecated('navigator.getUserMedia', 'navigator.mediaDevices.getUserMedia');
    navigator.mediaDevices.getUserMedia(constraints).then(onSuccess, onError);
  };

  if (!(browserDetails.version > 55 && 'autoGainControl' in navigator.mediaDevices.getSupportedConstraints())) {
    var remap = function remap(obj, a, b) {
      if (a in obj && !(b in obj)) {
        obj[b] = obj[a];
        delete obj[a];
      }
    };

    var nativeGetUserMedia = navigator.mediaDevices.getUserMedia.bind(navigator.mediaDevices);
    navigator.mediaDevices.getUserMedia = function (c) {
      if ((typeof c === 'undefined' ? 'undefined' : _typeof(c)) === 'object' && _typeof(c.audio) === 'object') {
        c = JSON.parse(JSON.stringify(c));
        remap(c.audio, 'autoGainControl', 'mozAutoGainControl');
        remap(c.audio, 'noiseSuppression', 'mozNoiseSuppression');
      }
      return nativeGetUserMedia(c);
    };

    if (MediaStreamTrack && MediaStreamTrack.prototype.getSettings) {
      var nativeGetSettings = MediaStreamTrack.prototype.getSettings;
      MediaStreamTrack.prototype.getSettings = function () {
        var obj = nativeGetSettings.apply(this, arguments);
        remap(obj, 'mozAutoGainControl', 'autoGainControl');
        remap(obj, 'mozNoiseSuppression', 'noiseSuppression');
        return obj;
      };
    }

    if (MediaStreamTrack && MediaStreamTrack.prototype.applyConstraints) {
      var nativeApplyConstraints = MediaStreamTrack.prototype.applyConstraints;
      MediaStreamTrack.prototype.applyConstraints = function (c) {
        if (this.kind === 'audio' && (typeof c === 'undefined' ? 'undefined' : _typeof(c)) === 'object') {
          c = JSON.parse(JSON.stringify(c));
          remap(c, 'autoGainControl', 'mozAutoGainControl');
          remap(c, 'noiseSuppression', 'mozNoiseSuppression');
        }
        return nativeApplyConstraints.apply(this, [c]);
      };
    }
  }
}

},{"../utils":11}],10:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.shimLocalStreamsAPI = shimLocalStreamsAPI;
exports.shimRemoteStreamsAPI = shimRemoteStreamsAPI;
exports.shimCallbacksAPI = shimCallbacksAPI;
exports.shimGetUserMedia = shimGetUserMedia;
exports.shimConstraints = shimConstraints;
exports.shimRTCIceServerUrls = shimRTCIceServerUrls;
exports.shimTrackEventTransceiver = shimTrackEventTransceiver;
exports.shimCreateOfferLegacy = shimCreateOfferLegacy;
exports.shimAudioContext = shimAudioContext;

var _utils = require('../utils');

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function shimLocalStreamsAPI(window) {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) !== 'object' || !window.RTCPeerConnection) {
    return;
  }
  if (!('getLocalStreams' in window.RTCPeerConnection.prototype)) {
    window.RTCPeerConnection.prototype.getLocalStreams = function getLocalStreams() {
      if (!this._localStreams) {
        this._localStreams = [];
      }
      return this._localStreams;
    };
  }
  if (!('addStream' in window.RTCPeerConnection.prototype)) {
    var _addTrack = window.RTCPeerConnection.prototype.addTrack;
    window.RTCPeerConnection.prototype.addStream = function addStream(stream) {
      var _this = this;

      if (!this._localStreams) {
        this._localStreams = [];
      }
      if (!this._localStreams.includes(stream)) {
        this._localStreams.push(stream);
      }
      // Try to emulate Chrome's behaviour of adding in audio-video order.
      // Safari orders by track id.
      stream.getAudioTracks().forEach(function (track) {
        return _addTrack.call(_this, track, stream);
      });
      stream.getVideoTracks().forEach(function (track) {
        return _addTrack.call(_this, track, stream);
      });
    };

    window.RTCPeerConnection.prototype.addTrack = function addTrack(track) {
      var _this2 = this;

      for (var _len = arguments.length, streams = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        streams[_key - 1] = arguments[_key];
      }

      if (streams) {
        streams.forEach(function (stream) {
          if (!_this2._localStreams) {
            _this2._localStreams = [stream];
          } else if (!_this2._localStreams.includes(stream)) {
            _this2._localStreams.push(stream);
          }
        });
      }
      return _addTrack.apply(this, arguments);
    };
  }
  if (!('removeStream' in window.RTCPeerConnection.prototype)) {
    window.RTCPeerConnection.prototype.removeStream = function removeStream(stream) {
      var _this3 = this;

      if (!this._localStreams) {
        this._localStreams = [];
      }
      var index = this._localStreams.indexOf(stream);
      if (index === -1) {
        return;
      }
      this._localStreams.splice(index, 1);
      var tracks = stream.getTracks();
      this.getSenders().forEach(function (sender) {
        if (tracks.includes(sender.track)) {
          _this3.removeTrack(sender);
        }
      });
    };
  }
}

function shimRemoteStreamsAPI(window) {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) !== 'object' || !window.RTCPeerConnection) {
    return;
  }
  if (!('getRemoteStreams' in window.RTCPeerConnection.prototype)) {
    window.RTCPeerConnection.prototype.getRemoteStreams = function getRemoteStreams() {
      return this._remoteStreams ? this._remoteStreams : [];
    };
  }
  if (!('onaddstream' in window.RTCPeerConnection.prototype)) {
    Object.defineProperty(window.RTCPeerConnection.prototype, 'onaddstream', {
      get: function get() {
        return this._onaddstream;
      },
      set: function set(f) {
        var _this4 = this;

        if (this._onaddstream) {
          this.removeEventListener('addstream', this._onaddstream);
          this.removeEventListener('track', this._onaddstreampoly);
        }
        this.addEventListener('addstream', this._onaddstream = f);
        this.addEventListener('track', this._onaddstreampoly = function (e) {
          e.streams.forEach(function (stream) {
            if (!_this4._remoteStreams) {
              _this4._remoteStreams = [];
            }
            if (_this4._remoteStreams.includes(stream)) {
              return;
            }
            _this4._remoteStreams.push(stream);
            var event = new Event('addstream');
            event.stream = stream;
            _this4.dispatchEvent(event);
          });
        });
      }
    });
    var origSetRemoteDescription = window.RTCPeerConnection.prototype.setRemoteDescription;
    window.RTCPeerConnection.prototype.setRemoteDescription = function setRemoteDescription() {
      var pc = this;
      if (!this._onaddstreampoly) {
        this.addEventListener('track', this._onaddstreampoly = function (e) {
          e.streams.forEach(function (stream) {
            if (!pc._remoteStreams) {
              pc._remoteStreams = [];
            }
            if (pc._remoteStreams.indexOf(stream) >= 0) {
              return;
            }
            pc._remoteStreams.push(stream);
            var event = new Event('addstream');
            event.stream = stream;
            pc.dispatchEvent(event);
          });
        });
      }
      return origSetRemoteDescription.apply(pc, arguments);
    };
  }
}

function shimCallbacksAPI(window) {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) !== 'object' || !window.RTCPeerConnection) {
    return;
  }
  var prototype = window.RTCPeerConnection.prototype;
  var origCreateOffer = prototype.createOffer;
  var origCreateAnswer = prototype.createAnswer;
  var setLocalDescription = prototype.setLocalDescription;
  var setRemoteDescription = prototype.setRemoteDescription;
  var addIceCandidate = prototype.addIceCandidate;

  prototype.createOffer = function createOffer(successCallback, failureCallback) {
    var options = arguments.length >= 2 ? arguments[2] : arguments[0];
    var promise = origCreateOffer.apply(this, [options]);
    if (!failureCallback) {
      return promise;
    }
    promise.then(successCallback, failureCallback);
    return Promise.resolve();
  };

  prototype.createAnswer = function createAnswer(successCallback, failureCallback) {
    var options = arguments.length >= 2 ? arguments[2] : arguments[0];
    var promise = origCreateAnswer.apply(this, [options]);
    if (!failureCallback) {
      return promise;
    }
    promise.then(successCallback, failureCallback);
    return Promise.resolve();
  };

  var withCallback = function withCallback(description, successCallback, failureCallback) {
    var promise = setLocalDescription.apply(this, [description]);
    if (!failureCallback) {
      return promise;
    }
    promise.then(successCallback, failureCallback);
    return Promise.resolve();
  };
  prototype.setLocalDescription = withCallback;

  withCallback = function withCallback(description, successCallback, failureCallback) {
    var promise = setRemoteDescription.apply(this, [description]);
    if (!failureCallback) {
      return promise;
    }
    promise.then(successCallback, failureCallback);
    return Promise.resolve();
  };
  prototype.setRemoteDescription = withCallback;

  withCallback = function withCallback(candidate, successCallback, failureCallback) {
    var promise = addIceCandidate.apply(this, [candidate]);
    if (!failureCallback) {
      return promise;
    }
    promise.then(successCallback, failureCallback);
    return Promise.resolve();
  };
  prototype.addIceCandidate = withCallback;
}

function shimGetUserMedia(window) {
  var navigator = window && window.navigator;

  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    // shim not needed in Safari 12.1
    var mediaDevices = navigator.mediaDevices;
    var _getUserMedia = mediaDevices.getUserMedia.bind(mediaDevices);
    navigator.mediaDevices.getUserMedia = function (constraints) {
      return _getUserMedia(shimConstraints(constraints));
    };
  }

  if (!navigator.getUserMedia && navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    navigator.getUserMedia = function getUserMedia(constraints, cb, errcb) {
      navigator.mediaDevices.getUserMedia(constraints).then(cb, errcb);
    }.bind(navigator);
  }
}

function shimConstraints(constraints) {
  if (constraints && constraints.video !== undefined) {
    return Object.assign({}, constraints, { video: utils.compactObject(constraints.video) });
  }

  return constraints;
}

function shimRTCIceServerUrls(window) {
  if (!window.RTCPeerConnection) {
    return;
  }
  // migrate from non-spec RTCIceServer.url to RTCIceServer.urls
  var OrigPeerConnection = window.RTCPeerConnection;
  window.RTCPeerConnection = function RTCPeerConnection(pcConfig, pcConstraints) {
    if (pcConfig && pcConfig.iceServers) {
      var newIceServers = [];
      for (var i = 0; i < pcConfig.iceServers.length; i++) {
        var server = pcConfig.iceServers[i];
        if (!server.hasOwnProperty('urls') && server.hasOwnProperty('url')) {
          utils.deprecated('RTCIceServer.url', 'RTCIceServer.urls');
          server = JSON.parse(JSON.stringify(server));
          server.urls = server.url;
          delete server.url;
          newIceServers.push(server);
        } else {
          newIceServers.push(pcConfig.iceServers[i]);
        }
      }
      pcConfig.iceServers = newIceServers;
    }
    return new OrigPeerConnection(pcConfig, pcConstraints);
  };
  window.RTCPeerConnection.prototype = OrigPeerConnection.prototype;
  // wrap static methods. Currently just generateCertificate.
  if ('generateCertificate' in OrigPeerConnection) {
    Object.defineProperty(window.RTCPeerConnection, 'generateCertificate', {
      get: function get() {
        return OrigPeerConnection.generateCertificate;
      }
    });
  }
}

function shimTrackEventTransceiver(window) {
  // Add event.transceiver member over deprecated event.receiver
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object' && window.RTCTrackEvent && 'receiver' in window.RTCTrackEvent.prototype && !('transceiver' in window.RTCTrackEvent.prototype)) {
    Object.defineProperty(window.RTCTrackEvent.prototype, 'transceiver', {
      get: function get() {
        return { receiver: this.receiver };
      }
    });
  }
}

function shimCreateOfferLegacy(window) {
  var origCreateOffer = window.RTCPeerConnection.prototype.createOffer;
  window.RTCPeerConnection.prototype.createOffer = function createOffer(offerOptions) {
    if (offerOptions) {
      if (typeof offerOptions.offerToReceiveAudio !== 'undefined') {
        // support bit values
        offerOptions.offerToReceiveAudio = !!offerOptions.offerToReceiveAudio;
      }
      var audioTransceiver = this.getTransceivers().find(function (transceiver) {
        return transceiver.receiver.track.kind === 'audio';
      });
      if (offerOptions.offerToReceiveAudio === false && audioTransceiver) {
        if (audioTransceiver.direction === 'sendrecv') {
          if (audioTransceiver.setDirection) {
            audioTransceiver.setDirection('sendonly');
          } else {
            audioTransceiver.direction = 'sendonly';
          }
        } else if (audioTransceiver.direction === 'recvonly') {
          if (audioTransceiver.setDirection) {
            audioTransceiver.setDirection('inactive');
          } else {
            audioTransceiver.direction = 'inactive';
          }
        }
      } else if (offerOptions.offerToReceiveAudio === true && !audioTransceiver) {
        this.addTransceiver('audio');
      }

      if (typeof offerOptions.offerToReceiveVideo !== 'undefined') {
        // support bit values
        offerOptions.offerToReceiveVideo = !!offerOptions.offerToReceiveVideo;
      }
      var videoTransceiver = this.getTransceivers().find(function (transceiver) {
        return transceiver.receiver.track.kind === 'video';
      });
      if (offerOptions.offerToReceiveVideo === false && videoTransceiver) {
        if (videoTransceiver.direction === 'sendrecv') {
          if (videoTransceiver.setDirection) {
            videoTransceiver.setDirection('sendonly');
          } else {
            videoTransceiver.direction = 'sendonly';
          }
        } else if (videoTransceiver.direction === 'recvonly') {
          if (videoTransceiver.setDirection) {
            videoTransceiver.setDirection('inactive');
          } else {
            videoTransceiver.direction = 'inactive';
          }
        }
      } else if (offerOptions.offerToReceiveVideo === true && !videoTransceiver) {
        this.addTransceiver('video');
      }
    }
    return origCreateOffer.apply(this, arguments);
  };
}

function shimAudioContext(window) {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) !== 'object' || window.AudioContext) {
    return;
  }
  window.AudioContext = window.webkitAudioContext;
}

},{"../utils":11}],11:[function(require,module,exports){
/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports.extractVersion = extractVersion;
exports.wrapPeerConnectionEvent = wrapPeerConnectionEvent;
exports.disableLog = disableLog;
exports.disableWarnings = disableWarnings;
exports.log = log;
exports.deprecated = deprecated;
exports.detectBrowser = detectBrowser;
exports.compactObject = compactObject;
exports.walkStats = walkStats;
exports.filterStats = filterStats;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var logDisabled_ = true;
var deprecationWarnings_ = true;

/**
 * Extract browser version out of the provided user agent string.
 *
 * @param {!string} uastring userAgent string.
 * @param {!string} expr Regular expression used as match criteria.
 * @param {!number} pos position in the version string to be returned.
 * @return {!number} browser version.
 */
function extractVersion(uastring, expr, pos) {
  var match = uastring.match(expr);
  return match && match.length >= pos && parseInt(match[pos], 10);
}

// Wraps the peerconnection event eventNameToWrap in a function
// which returns the modified event object (or false to prevent
// the event).
function wrapPeerConnectionEvent(window, eventNameToWrap, wrapper) {
  if (!window.RTCPeerConnection) {
    return;
  }
  var proto = window.RTCPeerConnection.prototype;
  var nativeAddEventListener = proto.addEventListener;
  proto.addEventListener = function (nativeEventName, cb) {
    if (nativeEventName !== eventNameToWrap) {
      return nativeAddEventListener.apply(this, arguments);
    }
    var wrappedCallback = function wrappedCallback(e) {
      var modifiedEvent = wrapper(e);
      if (modifiedEvent) {
        if (cb.handleEvent) {
          cb.handleEvent(modifiedEvent);
        } else {
          cb(modifiedEvent);
        }
      }
    };
    this._eventMap = this._eventMap || {};
    if (!this._eventMap[eventNameToWrap]) {
      this._eventMap[eventNameToWrap] = new Map();
    }
    this._eventMap[eventNameToWrap].set(cb, wrappedCallback);
    return nativeAddEventListener.apply(this, [nativeEventName, wrappedCallback]);
  };

  var nativeRemoveEventListener = proto.removeEventListener;
  proto.removeEventListener = function (nativeEventName, cb) {
    if (nativeEventName !== eventNameToWrap || !this._eventMap || !this._eventMap[eventNameToWrap]) {
      return nativeRemoveEventListener.apply(this, arguments);
    }
    if (!this._eventMap[eventNameToWrap].has(cb)) {
      return nativeRemoveEventListener.apply(this, arguments);
    }
    var unwrappedCb = this._eventMap[eventNameToWrap].get(cb);
    this._eventMap[eventNameToWrap].delete(cb);
    if (this._eventMap[eventNameToWrap].size === 0) {
      delete this._eventMap[eventNameToWrap];
    }
    if (Object.keys(this._eventMap).length === 0) {
      delete this._eventMap;
    }
    return nativeRemoveEventListener.apply(this, [nativeEventName, unwrappedCb]);
  };

  Object.defineProperty(proto, 'on' + eventNameToWrap, {
    get: function get() {
      return this['_on' + eventNameToWrap];
    },
    set: function set(cb) {
      if (this['_on' + eventNameToWrap]) {
        this.removeEventListener(eventNameToWrap, this['_on' + eventNameToWrap]);
        delete this['_on' + eventNameToWrap];
      }
      if (cb) {
        this.addEventListener(eventNameToWrap, this['_on' + eventNameToWrap] = cb);
      }
    },

    enumerable: true,
    configurable: true
  });
}

function disableLog(bool) {
  if (typeof bool !== 'boolean') {
    return new Error('Argument type: ' + (typeof bool === 'undefined' ? 'undefined' : _typeof(bool)) + '. Please use a boolean.');
  }
  logDisabled_ = bool;
  return bool ? 'adapter.js logging disabled' : 'adapter.js logging enabled';
}

/**
 * Disable or enable deprecation warnings
 * @param {!boolean} bool set to true to disable warnings.
 */
function disableWarnings(bool) {
  if (typeof bool !== 'boolean') {
    return new Error('Argument type: ' + (typeof bool === 'undefined' ? 'undefined' : _typeof(bool)) + '. Please use a boolean.');
  }
  deprecationWarnings_ = !bool;
  return 'adapter.js deprecation warnings ' + (bool ? 'disabled' : 'enabled');
}

function log() {
  if ((typeof window === 'undefined' ? 'undefined' : _typeof(window)) === 'object') {
    if (logDisabled_) {
      return;
    }
    if (typeof console !== 'undefined' && typeof console.log === 'function') {
      console.log.apply(console, arguments);
    }
  }
}

/**
 * Shows a deprecation warning suggesting the modern and spec-compatible API.
 */
function deprecated(oldMethod, newMethod) {
  if (!deprecationWarnings_) {
    return;
  }
  console.warn(oldMethod + ' is deprecated, please use ' + newMethod + ' instead.');
}

/**
 * Browser detector.
 *
 * @return {object} result containing browser and version
 *     properties.
 */
function detectBrowser(window) {
  // Returned result object.
  var result = { browser: null, version: null };

  // Fail early if it's not a browser
  if (typeof window === 'undefined' || !window.navigator) {
    result.browser = 'Not a browser.';
    return result;
  }

  var navigator = window.navigator;


  if (navigator.mozGetUserMedia) {
    // Firefox.
    result.browser = 'firefox';
    result.version = extractVersion(navigator.userAgent, /Firefox\/(\d+)\./, 1);
  } else if (navigator.webkitGetUserMedia || window.isSecureContext === false && window.webkitRTCPeerConnection && !window.RTCIceGatherer) {
    // Chrome, Chromium, Webview, Opera.
    // Version matches Chrome/WebRTC version.
    // Chrome 74 removed webkitGetUserMedia on http as well so we need the
    // more complicated fallback to webkitRTCPeerConnection.
    result.browser = 'chrome';
    result.version = extractVersion(navigator.userAgent, /Chrom(e|ium)\/(\d+)\./, 2);
  } else if (navigator.mediaDevices && navigator.userAgent.match(/Edge\/(\d+).(\d+)$/)) {
    // Edge.
    result.browser = 'edge';
    result.version = extractVersion(navigator.userAgent, /Edge\/(\d+).(\d+)$/, 2);
  } else if (window.RTCPeerConnection && navigator.userAgent.match(/AppleWebKit\/(\d+)\./)) {
    // Safari.
    result.browser = 'safari';
    result.version = extractVersion(navigator.userAgent, /AppleWebKit\/(\d+)\./, 1);
    result.supportsUnifiedPlan = window.RTCRtpTransceiver && 'currentDirection' in window.RTCRtpTransceiver.prototype;
  } else {
    // Default fallthrough: not supported.
    result.browser = 'Not a supported browser.';
    return result;
  }

  return result;
}

/**
 * Checks if something is an object.
 *
 * @param {*} val The something you want to check.
 * @return true if val is an object, false otherwise.
 */
function isObject(val) {
  return Object.prototype.toString.call(val) === '[object Object]';
}

/**
 * Remove all empty objects and undefined values
 * from a nested object -- an enhanced and vanilla version
 * of Lodash's `compact`.
 */
function compactObject(data) {
  if (!isObject(data)) {
    return data;
  }

  return Object.keys(data).reduce(function (accumulator, key) {
    var isObj = isObject(data[key]);
    var value = isObj ? compactObject(data[key]) : data[key];
    var isEmptyObject = isObj && !Object.keys(value).length;
    if (value === undefined || isEmptyObject) {
      return accumulator;
    }
    return Object.assign(accumulator, _defineProperty({}, key, value));
  }, {});
}

/* iterates the stats graph recursively. */
function walkStats(stats, base, resultSet) {
  if (!base || resultSet.has(base.id)) {
    return;
  }
  resultSet.set(base.id, base);
  Object.keys(base).forEach(function (name) {
    if (name.endsWith('Id')) {
      walkStats(stats, stats.get(base[name]), resultSet);
    } else if (name.endsWith('Ids')) {
      base[name].forEach(function (id) {
        walkStats(stats, stats.get(id), resultSet);
      });
    }
  });
}

/* filter getStats for a sender/receiver track. */
function filterStats(result, track, outbound) {
  var streamStatsType = outbound ? 'outbound-rtp' : 'inbound-rtp';
  var filteredResult = new Map();
  if (track === null) {
    return filteredResult;
  }
  var trackStats = [];
  result.forEach(function (value) {
    if (value.type === 'track' && value.trackIdentifier === track.id) {
      trackStats.push(value);
    }
  });
  trackStats.forEach(function (trackStat) {
    result.forEach(function (stats) {
      if (stats.type === streamStatsType && stats.trackId === trackStat.id) {
        walkStats(result, stats, filteredResult);
      }
    });
  });
  return filteredResult;
}

},{}],12:[function(require,module,exports){

},{}],13:[function(require,module,exports){
/* eslint-env node */
'use strict';

// SDP helpers.
var SDPUtils = {};

// Generate an alphanumeric identifier for cname or mids.
// TODO: use UUIDs instead? https://gist.github.com/jed/982883
SDPUtils.generateIdentifier = function() {
  return Math.random().toString(36).substr(2, 10);
};

// The RTCP CNAME used by all peerconnections from the same JS.
SDPUtils.localCName = SDPUtils.generateIdentifier();

// Splits SDP into lines, dealing with both CRLF and LF.
SDPUtils.splitLines = function(blob) {
  return blob.trim().split('\n').map(function(line) {
    return line.trim();
  });
};
// Splits SDP into sessionpart and mediasections. Ensures CRLF.
SDPUtils.splitSections = function(blob) {
  var parts = blob.split('\nm=');
  return parts.map(function(part, index) {
    return (index > 0 ? 'm=' + part : part).trim() + '\r\n';
  });
};

// returns the session description.
SDPUtils.getDescription = function(blob) {
  var sections = SDPUtils.splitSections(blob);
  return sections && sections[0];
};

// returns the individual media sections.
SDPUtils.getMediaSections = function(blob) {
  var sections = SDPUtils.splitSections(blob);
  sections.shift();
  return sections;
};

// Returns lines that start with a certain prefix.
SDPUtils.matchPrefix = function(blob, prefix) {
  return SDPUtils.splitLines(blob).filter(function(line) {
    return line.indexOf(prefix) === 0;
  });
};

// Parses an ICE candidate line. Sample input:
// candidate:702786350 2 udp 41819902 8.8.8.8 60769 typ relay raddr 8.8.8.8
// rport 55996"
SDPUtils.parseCandidate = function(line) {
  var parts;
  // Parse both variants.
  if (line.indexOf('a=candidate:') === 0) {
    parts = line.substring(12).split(' ');
  } else {
    parts = line.substring(10).split(' ');
  }

  var candidate = {
    foundation: parts[0],
    component: parseInt(parts[1], 10),
    protocol: parts[2].toLowerCase(),
    priority: parseInt(parts[3], 10),
    ip: parts[4],
    address: parts[4], // address is an alias for ip.
    port: parseInt(parts[5], 10),
    // skip parts[6] == 'typ'
    type: parts[7]
  };

  for (var i = 8; i < parts.length; i += 2) {
    switch (parts[i]) {
      case 'raddr':
        candidate.relatedAddress = parts[i + 1];
        break;
      case 'rport':
        candidate.relatedPort = parseInt(parts[i + 1], 10);
        break;
      case 'tcptype':
        candidate.tcpType = parts[i + 1];
        break;
      case 'ufrag':
        candidate.ufrag = parts[i + 1]; // for backward compability.
        candidate.usernameFragment = parts[i + 1];
        break;
      default: // extension handling, in particular ufrag
        candidate[parts[i]] = parts[i + 1];
        break;
    }
  }
  return candidate;
};

// Translates a candidate object into SDP candidate attribute.
SDPUtils.writeCandidate = function(candidate) {
  var sdp = [];
  sdp.push(candidate.foundation);
  sdp.push(candidate.component);
  sdp.push(candidate.protocol.toUpperCase());
  sdp.push(candidate.priority);
  sdp.push(candidate.address || candidate.ip);
  sdp.push(candidate.port);

  var type = candidate.type;
  sdp.push('typ');
  sdp.push(type);
  if (type !== 'host' && candidate.relatedAddress &&
      candidate.relatedPort) {
    sdp.push('raddr');
    sdp.push(candidate.relatedAddress);
    sdp.push('rport');
    sdp.push(candidate.relatedPort);
  }
  if (candidate.tcpType && candidate.protocol.toLowerCase() === 'tcp') {
    sdp.push('tcptype');
    sdp.push(candidate.tcpType);
  }
  if (candidate.usernameFragment || candidate.ufrag) {
    sdp.push('ufrag');
    sdp.push(candidate.usernameFragment || candidate.ufrag);
  }
  return 'candidate:' + sdp.join(' ');
};

// Parses an ice-options line, returns an array of option tags.
// a=ice-options:foo bar
SDPUtils.parseIceOptions = function(line) {
  return line.substr(14).split(' ');
};

// Parses an rtpmap line, returns RTCRtpCoddecParameters. Sample input:
// a=rtpmap:111 opus/48000/2
SDPUtils.parseRtpMap = function(line) {
  var parts = line.substr(9).split(' ');
  var parsed = {
    payloadType: parseInt(parts.shift(), 10) // was: id
  };

  parts = parts[0].split('/');

  parsed.name = parts[0];
  parsed.clockRate = parseInt(parts[1], 10); // was: clockrate
  parsed.channels = parts.length === 3 ? parseInt(parts[2], 10) : 1;
  // legacy alias, got renamed back to channels in ORTC.
  parsed.numChannels = parsed.channels;
  return parsed;
};

// Generate an a=rtpmap line from RTCRtpCodecCapability or
// RTCRtpCodecParameters.
SDPUtils.writeRtpMap = function(codec) {
  var pt = codec.payloadType;
  if (codec.preferredPayloadType !== undefined) {
    pt = codec.preferredPayloadType;
  }
  var channels = codec.channels || codec.numChannels || 1;
  return 'a=rtpmap:' + pt + ' ' + codec.name + '/' + codec.clockRate +
      (channels !== 1 ? '/' + channels : '') + '\r\n';
};

// Parses an a=extmap line (headerextension from RFC 5285). Sample input:
// a=extmap:2 urn:ietf:params:rtp-hdrext:toffset
// a=extmap:2/sendonly urn:ietf:params:rtp-hdrext:toffset
SDPUtils.parseExtmap = function(line) {
  var parts = line.substr(9).split(' ');
  return {
    id: parseInt(parts[0], 10),
    direction: parts[0].indexOf('/') > 0 ? parts[0].split('/')[1] : 'sendrecv',
    uri: parts[1]
  };
};

// Generates a=extmap line from RTCRtpHeaderExtensionParameters or
// RTCRtpHeaderExtension.
SDPUtils.writeExtmap = function(headerExtension) {
  return 'a=extmap:' + (headerExtension.id || headerExtension.preferredId) +
      (headerExtension.direction && headerExtension.direction !== 'sendrecv'
        ? '/' + headerExtension.direction
        : '') +
      ' ' + headerExtension.uri + '\r\n';
};

// Parses an ftmp line, returns dictionary. Sample input:
// a=fmtp:96 vbr=on;cng=on
// Also deals with vbr=on; cng=on
SDPUtils.parseFmtp = function(line) {
  var parsed = {};
  var kv;
  var parts = line.substr(line.indexOf(' ') + 1).split(';');
  for (var j = 0; j < parts.length; j++) {
    kv = parts[j].trim().split('=');
    parsed[kv[0].trim()] = kv[1];
  }
  return parsed;
};

// Generates an a=ftmp line from RTCRtpCodecCapability or RTCRtpCodecParameters.
SDPUtils.writeFmtp = function(codec) {
  var line = '';
  var pt = codec.payloadType;
  if (codec.preferredPayloadType !== undefined) {
    pt = codec.preferredPayloadType;
  }
  if (codec.parameters && Object.keys(codec.parameters).length) {
    var params = [];
    Object.keys(codec.parameters).forEach(function(param) {
      if (codec.parameters[param]) {
        params.push(param + '=' + codec.parameters[param]);
      } else {
        params.push(param);
      }
    });
    line += 'a=fmtp:' + pt + ' ' + params.join(';') + '\r\n';
  }
  return line;
};

// Parses an rtcp-fb line, returns RTCPRtcpFeedback object. Sample input:
// a=rtcp-fb:98 nack rpsi
SDPUtils.parseRtcpFb = function(line) {
  var parts = line.substr(line.indexOf(' ') + 1).split(' ');
  return {
    type: parts.shift(),
    parameter: parts.join(' ')
  };
};
// Generate a=rtcp-fb lines from RTCRtpCodecCapability or RTCRtpCodecParameters.
SDPUtils.writeRtcpFb = function(codec) {
  var lines = '';
  var pt = codec.payloadType;
  if (codec.preferredPayloadType !== undefined) {
    pt = codec.preferredPayloadType;
  }
  if (codec.rtcpFeedback && codec.rtcpFeedback.length) {
    // FIXME: special handling for trr-int?
    codec.rtcpFeedback.forEach(function(fb) {
      lines += 'a=rtcp-fb:' + pt + ' ' + fb.type +
      (fb.parameter && fb.parameter.length ? ' ' + fb.parameter : '') +
          '\r\n';
    });
  }
  return lines;
};

// Parses an RFC 5576 ssrc media attribute. Sample input:
// a=ssrc:3735928559 cname:something
SDPUtils.parseSsrcMedia = function(line) {
  var sp = line.indexOf(' ');
  var parts = {
    ssrc: parseInt(line.substr(7, sp - 7), 10)
  };
  var colon = line.indexOf(':', sp);
  if (colon > -1) {
    parts.attribute = line.substr(sp + 1, colon - sp - 1);
    parts.value = line.substr(colon + 1);
  } else {
    parts.attribute = line.substr(sp + 1);
  }
  return parts;
};

SDPUtils.parseSsrcGroup = function(line) {
  var parts = line.substr(13).split(' ');
  return {
    semantics: parts.shift(),
    ssrcs: parts.map(function(ssrc) {
      return parseInt(ssrc, 10);
    })
  };
};

// Extracts the MID (RFC 5888) from a media section.
// returns the MID or undefined if no mid line was found.
SDPUtils.getMid = function(mediaSection) {
  var mid = SDPUtils.matchPrefix(mediaSection, 'a=mid:')[0];
  if (mid) {
    return mid.substr(6);
  }
};

SDPUtils.parseFingerprint = function(line) {
  var parts = line.substr(14).split(' ');
  return {
    algorithm: parts[0].toLowerCase(), // algorithm is case-sensitive in Edge.
    value: parts[1]
  };
};

// Extracts DTLS parameters from SDP media section or sessionpart.
// FIXME: for consistency with other functions this should only
//   get the fingerprint line as input. See also getIceParameters.
SDPUtils.getDtlsParameters = function(mediaSection, sessionpart) {
  var lines = SDPUtils.matchPrefix(mediaSection + sessionpart,
    'a=fingerprint:');
  // Note: a=setup line is ignored since we use the 'auto' role.
  // Note2: 'algorithm' is not case sensitive except in Edge.
  return {
    role: 'auto',
    fingerprints: lines.map(SDPUtils.parseFingerprint)
  };
};

// Serializes DTLS parameters to SDP.
SDPUtils.writeDtlsParameters = function(params, setupType) {
  var sdp = 'a=setup:' + setupType + '\r\n';
  params.fingerprints.forEach(function(fp) {
    sdp += 'a=fingerprint:' + fp.algorithm + ' ' + fp.value + '\r\n';
  });
  return sdp;
};

// Parses a=crypto lines into
//   https://rawgit.com/aboba/edgertc/master/msortc-rs4.html#dictionary-rtcsrtpsdesparameters-members
SDPUtils.parseCryptoLine = function(line) {
  var parts = line.substr(9).split(' ');
  return {
    tag: parseInt(parts[0], 10),
    cryptoSuite: parts[1],
    keyParams: parts[2],
    sessionParams: parts.slice(3),
  };
};

SDPUtils.writeCryptoLine = function(parameters) {
  return 'a=crypto:' + parameters.tag + ' ' +
    parameters.cryptoSuite + ' ' +
    (typeof parameters.keyParams === 'object'
      ? SDPUtils.writeCryptoKeyParams(parameters.keyParams)
      : parameters.keyParams) +
    (parameters.sessionParams ? ' ' + parameters.sessionParams.join(' ') : '') +
    '\r\n';
};

// Parses the crypto key parameters into
//   https://rawgit.com/aboba/edgertc/master/msortc-rs4.html#rtcsrtpkeyparam*
SDPUtils.parseCryptoKeyParams = function(keyParams) {
  if (keyParams.indexOf('inline:') !== 0) {
    return null;
  }
  var parts = keyParams.substr(7).split('|');
  return {
    keyMethod: 'inline',
    keySalt: parts[0],
    lifeTime: parts[1],
    mkiValue: parts[2] ? parts[2].split(':')[0] : undefined,
    mkiLength: parts[2] ? parts[2].split(':')[1] : undefined,
  };
};

SDPUtils.writeCryptoKeyParams = function(keyParams) {
  return keyParams.keyMethod + ':'
    + keyParams.keySalt +
    (keyParams.lifeTime ? '|' + keyParams.lifeTime : '') +
    (keyParams.mkiValue && keyParams.mkiLength
      ? '|' + keyParams.mkiValue + ':' + keyParams.mkiLength
      : '');
};

// Extracts all SDES paramters.
SDPUtils.getCryptoParameters = function(mediaSection, sessionpart) {
  var lines = SDPUtils.matchPrefix(mediaSection + sessionpart,
    'a=crypto:');
  return lines.map(SDPUtils.parseCryptoLine);
};

// Parses ICE information from SDP media section or sessionpart.
// FIXME: for consistency with other functions this should only
//   get the ice-ufrag and ice-pwd lines as input.
SDPUtils.getIceParameters = function(mediaSection, sessionpart) {
  var ufrag = SDPUtils.matchPrefix(mediaSection + sessionpart,
    'a=ice-ufrag:')[0];
  var pwd = SDPUtils.matchPrefix(mediaSection + sessionpart,
    'a=ice-pwd:')[0];
  if (!(ufrag && pwd)) {
    return null;
  }
  return {
    usernameFragment: ufrag.substr(12),
    password: pwd.substr(10),
  };
};

// Serializes ICE parameters to SDP.
SDPUtils.writeIceParameters = function(params) {
  return 'a=ice-ufrag:' + params.usernameFragment + '\r\n' +
      'a=ice-pwd:' + params.password + '\r\n';
};

// Parses the SDP media section and returns RTCRtpParameters.
SDPUtils.parseRtpParameters = function(mediaSection) {
  var description = {
    codecs: [],
    headerExtensions: [],
    fecMechanisms: [],
    rtcp: []
  };
  var lines = SDPUtils.splitLines(mediaSection);
  var mline = lines[0].split(' ');
  for (var i = 3; i < mline.length; i++) { // find all codecs from mline[3..]
    var pt = mline[i];
    var rtpmapline = SDPUtils.matchPrefix(
      mediaSection, 'a=rtpmap:' + pt + ' ')[0];
    if (rtpmapline) {
      var codec = SDPUtils.parseRtpMap(rtpmapline);
      var fmtps = SDPUtils.matchPrefix(
        mediaSection, 'a=fmtp:' + pt + ' ');
      // Only the first a=fmtp:<pt> is considered.
      codec.parameters = fmtps.length ? SDPUtils.parseFmtp(fmtps[0]) : {};
      codec.rtcpFeedback = SDPUtils.matchPrefix(
        mediaSection, 'a=rtcp-fb:' + pt + ' ')
        .map(SDPUtils.parseRtcpFb);
      description.codecs.push(codec);
      // parse FEC mechanisms from rtpmap lines.
      switch (codec.name.toUpperCase()) {
        case 'RED':
        case 'ULPFEC':
          description.fecMechanisms.push(codec.name.toUpperCase());
          break;
        default: // only RED and ULPFEC are recognized as FEC mechanisms.
          break;
      }
    }
  }
  SDPUtils.matchPrefix(mediaSection, 'a=extmap:').forEach(function(line) {
    description.headerExtensions.push(SDPUtils.parseExtmap(line));
  });
  // FIXME: parse rtcp.
  return description;
};

// Generates parts of the SDP media section describing the capabilities /
// parameters.
SDPUtils.writeRtpDescription = function(kind, caps) {
  var sdp = '';

  // Build the mline.
  sdp += 'm=' + kind + ' ';
  sdp += caps.codecs.length > 0 ? '9' : '0'; // reject if no codecs.
  sdp += ' UDP/TLS/RTP/SAVPF ';
  sdp += caps.codecs.map(function(codec) {
    if (codec.preferredPayloadType !== undefined) {
      return codec.preferredPayloadType;
    }
    return codec.payloadType;
  }).join(' ') + '\r\n';

  sdp += 'c=IN IP4 0.0.0.0\r\n';
  sdp += 'a=rtcp:9 IN IP4 0.0.0.0\r\n';

  // Add a=rtpmap lines for each codec. Also fmtp and rtcp-fb.
  caps.codecs.forEach(function(codec) {
    sdp += SDPUtils.writeRtpMap(codec);
    sdp += SDPUtils.writeFmtp(codec);
    sdp += SDPUtils.writeRtcpFb(codec);
  });
  var maxptime = 0;
  caps.codecs.forEach(function(codec) {
    if (codec.maxptime > maxptime) {
      maxptime = codec.maxptime;
    }
  });
  if (maxptime > 0) {
    sdp += 'a=maxptime:' + maxptime + '\r\n';
  }
  sdp += 'a=rtcp-mux\r\n';

  if (caps.headerExtensions) {
    caps.headerExtensions.forEach(function(extension) {
      sdp += SDPUtils.writeExtmap(extension);
    });
  }
  // FIXME: write fecMechanisms.
  return sdp;
};

// Parses the SDP media section and returns an array of
// RTCRtpEncodingParameters.
SDPUtils.parseRtpEncodingParameters = function(mediaSection) {
  var encodingParameters = [];
  var description = SDPUtils.parseRtpParameters(mediaSection);
  var hasRed = description.fecMechanisms.indexOf('RED') !== -1;
  var hasUlpfec = description.fecMechanisms.indexOf('ULPFEC') !== -1;

  // filter a=ssrc:... cname:, ignore PlanB-msid
  var ssrcs = SDPUtils.matchPrefix(mediaSection, 'a=ssrc:')
    .map(function(line) {
      return SDPUtils.parseSsrcMedia(line);
    })
    .filter(function(parts) {
      return parts.attribute === 'cname';
    });
  var primarySsrc = ssrcs.length > 0 && ssrcs[0].ssrc;
  var secondarySsrc;

  var flows = SDPUtils.matchPrefix(mediaSection, 'a=ssrc-group:FID')
    .map(function(line) {
      var parts = line.substr(17).split(' ');
      return parts.map(function(part) {
        return parseInt(part, 10);
      });
    });
  if (flows.length > 0 && flows[0].length > 1 && flows[0][0] === primarySsrc) {
    secondarySsrc = flows[0][1];
  }

  description.codecs.forEach(function(codec) {
    if (codec.name.toUpperCase() === 'RTX' && codec.parameters.apt) {
      var encParam = {
        ssrc: primarySsrc,
        codecPayloadType: parseInt(codec.parameters.apt, 10)
      };
      if (primarySsrc && secondarySsrc) {
        encParam.rtx = {ssrc: secondarySsrc};
      }
      encodingParameters.push(encParam);
      if (hasRed) {
        encParam = JSON.parse(JSON.stringify(encParam));
        encParam.fec = {
          ssrc: primarySsrc,
          mechanism: hasUlpfec ? 'red+ulpfec' : 'red'
        };
        encodingParameters.push(encParam);
      }
    }
  });
  if (encodingParameters.length === 0 && primarySsrc) {
    encodingParameters.push({
      ssrc: primarySsrc
    });
  }

  // we support both b=AS and b=TIAS but interpret AS as TIAS.
  var bandwidth = SDPUtils.matchPrefix(mediaSection, 'b=');
  if (bandwidth.length) {
    if (bandwidth[0].indexOf('b=TIAS:') === 0) {
      bandwidth = parseInt(bandwidth[0].substr(7), 10);
    } else if (bandwidth[0].indexOf('b=AS:') === 0) {
      // use formula from JSEP to convert b=AS to TIAS value.
      bandwidth = parseInt(bandwidth[0].substr(5), 10) * 1000 * 0.95
          - (50 * 40 * 8);
    } else {
      bandwidth = undefined;
    }
    encodingParameters.forEach(function(params) {
      params.maxBitrate = bandwidth;
    });
  }
  return encodingParameters;
};

// parses http://draft.ortc.org/#rtcrtcpparameters*
SDPUtils.parseRtcpParameters = function(mediaSection) {
  var rtcpParameters = {};

  // Gets the first SSRC. Note tha with RTX there might be multiple
  // SSRCs.
  var remoteSsrc = SDPUtils.matchPrefix(mediaSection, 'a=ssrc:')
    .map(function(line) {
      return SDPUtils.parseSsrcMedia(line);
    })
    .filter(function(obj) {
      return obj.attribute === 'cname';
    })[0];
  if (remoteSsrc) {
    rtcpParameters.cname = remoteSsrc.value;
    rtcpParameters.ssrc = remoteSsrc.ssrc;
  }

  // Edge uses the compound attribute instead of reducedSize
  // compound is !reducedSize
  var rsize = SDPUtils.matchPrefix(mediaSection, 'a=rtcp-rsize');
  rtcpParameters.reducedSize = rsize.length > 0;
  rtcpParameters.compound = rsize.length === 0;

  // parses the rtcp-mux attrіbute.
  // Note that Edge does not support unmuxed RTCP.
  var mux = SDPUtils.matchPrefix(mediaSection, 'a=rtcp-mux');
  rtcpParameters.mux = mux.length > 0;

  return rtcpParameters;
};

// parses either a=msid: or a=ssrc:... msid lines and returns
// the id of the MediaStream and MediaStreamTrack.
SDPUtils.parseMsid = function(mediaSection) {
  var parts;
  var spec = SDPUtils.matchPrefix(mediaSection, 'a=msid:');
  if (spec.length === 1) {
    parts = spec[0].substr(7).split(' ');
    return {stream: parts[0], track: parts[1]};
  }
  var planB = SDPUtils.matchPrefix(mediaSection, 'a=ssrc:')
    .map(function(line) {
      return SDPUtils.parseSsrcMedia(line);
    })
    .filter(function(msidParts) {
      return msidParts.attribute === 'msid';
    });
  if (planB.length > 0) {
    parts = planB[0].value.split(' ');
    return {stream: parts[0], track: parts[1]};
  }
};

// SCTP
// parses draft-ietf-mmusic-sctp-sdp-26 first and falls back
// to draft-ietf-mmusic-sctp-sdp-05
SDPUtils.parseSctpDescription = function(mediaSection) {
  var mline = SDPUtils.parseMLine(mediaSection);
  var maxSizeLine = SDPUtils.matchPrefix(mediaSection, 'a=max-message-size:');
  var maxMessageSize;
  if (maxSizeLine.length > 0) {
    maxMessageSize = parseInt(maxSizeLine[0].substr(19), 10);
  }
  if (isNaN(maxMessageSize)) {
    maxMessageSize = 65536;
  }
  var sctpPort = SDPUtils.matchPrefix(mediaSection, 'a=sctp-port:');
  if (sctpPort.length > 0) {
    return {
      port: parseInt(sctpPort[0].substr(12), 10),
      protocol: mline.fmt,
      maxMessageSize: maxMessageSize
    };
  }
  var sctpMapLines = SDPUtils.matchPrefix(mediaSection, 'a=sctpmap:');
  if (sctpMapLines.length > 0) {
    var parts = SDPUtils.matchPrefix(mediaSection, 'a=sctpmap:')[0]
      .substr(10)
      .split(' ');
    return {
      port: parseInt(parts[0], 10),
      protocol: parts[1],
      maxMessageSize: maxMessageSize
    };
  }
};

// SCTP
// outputs the draft-ietf-mmusic-sctp-sdp-26 version that all browsers
// support by now receiving in this format, unless we originally parsed
// as the draft-ietf-mmusic-sctp-sdp-05 format (indicated by the m-line
// protocol of DTLS/SCTP -- without UDP/ or TCP/)
SDPUtils.writeSctpDescription = function(media, sctp) {
  var output = [];
  if (media.protocol !== 'DTLS/SCTP') {
    output = [
      'm=' + media.kind + ' 9 ' + media.protocol + ' ' + sctp.protocol + '\r\n',
      'c=IN IP4 0.0.0.0\r\n',
      'a=sctp-port:' + sctp.port + '\r\n'
    ];
  } else {
    output = [
      'm=' + media.kind + ' 9 ' + media.protocol + ' ' + sctp.port + '\r\n',
      'c=IN IP4 0.0.0.0\r\n',
      'a=sctpmap:' + sctp.port + ' ' + sctp.protocol + ' 65535\r\n'
    ];
  }
  if (sctp.maxMessageSize !== undefined) {
    output.push('a=max-message-size:' + sctp.maxMessageSize + '\r\n');
  }
  return output.join('');
};

// Generate a session ID for SDP.
// https://tools.ietf.org/html/draft-ietf-rtcweb-jsep-20#section-5.2.1
// recommends using a cryptographically random +ve 64-bit value
// but right now this should be acceptable and within the right range
SDPUtils.generateSessionId = function() {
  return Math.random().toString().substr(2, 21);
};

// Write boilder plate for start of SDP
// sessId argument is optional - if not supplied it will
// be generated randomly
// sessVersion is optional and defaults to 2
// sessUser is optional and defaults to 'thisisadapterortc'
SDPUtils.writeSessionBoilerplate = function(sessId, sessVer, sessUser) {
  var sessionId;
  var version = sessVer !== undefined ? sessVer : 2;
  if (sessId) {
    sessionId = sessId;
  } else {
    sessionId = SDPUtils.generateSessionId();
  }
  var user = sessUser || 'thisisadapterortc';
  // FIXME: sess-id should be an NTP timestamp.
  return 'v=0\r\n' +
      'o=' + user + ' ' + sessionId + ' ' + version +
        ' IN IP4 127.0.0.1\r\n' +
      's=-\r\n' +
      't=0 0\r\n';
};

SDPUtils.writeMediaSection = function(transceiver, caps, type, stream) {
  var sdp = SDPUtils.writeRtpDescription(transceiver.kind, caps);

  // Map ICE parameters (ufrag, pwd) to SDP.
  sdp += SDPUtils.writeIceParameters(
    transceiver.iceGatherer.getLocalParameters());

  // Map DTLS parameters to SDP.
  sdp += SDPUtils.writeDtlsParameters(
    transceiver.dtlsTransport.getLocalParameters(),
    type === 'offer' ? 'actpass' : 'active');

  sdp += 'a=mid:' + transceiver.mid + '\r\n';

  if (transceiver.direction) {
    sdp += 'a=' + transceiver.direction + '\r\n';
  } else if (transceiver.rtpSender && transceiver.rtpReceiver) {
    sdp += 'a=sendrecv\r\n';
  } else if (transceiver.rtpSender) {
    sdp += 'a=sendonly\r\n';
  } else if (transceiver.rtpReceiver) {
    sdp += 'a=recvonly\r\n';
  } else {
    sdp += 'a=inactive\r\n';
  }

  if (transceiver.rtpSender) {
    // spec.
    var msid = 'msid:' + stream.id + ' ' +
        transceiver.rtpSender.track.id + '\r\n';
    sdp += 'a=' + msid;

    // for Chrome.
    sdp += 'a=ssrc:' + transceiver.sendEncodingParameters[0].ssrc +
        ' ' + msid;
    if (transceiver.sendEncodingParameters[0].rtx) {
      sdp += 'a=ssrc:' + transceiver.sendEncodingParameters[0].rtx.ssrc +
          ' ' + msid;
      sdp += 'a=ssrc-group:FID ' +
          transceiver.sendEncodingParameters[0].ssrc + ' ' +
          transceiver.sendEncodingParameters[0].rtx.ssrc +
          '\r\n';
    }
  }
  // FIXME: this should be written by writeRtpDescription.
  sdp += 'a=ssrc:' + transceiver.sendEncodingParameters[0].ssrc +
      ' cname:' + SDPUtils.localCName + '\r\n';
  if (transceiver.rtpSender && transceiver.sendEncodingParameters[0].rtx) {
    sdp += 'a=ssrc:' + transceiver.sendEncodingParameters[0].rtx.ssrc +
        ' cname:' + SDPUtils.localCName + '\r\n';
  }
  return sdp;
};

// Gets the direction from the mediaSection or the sessionpart.
SDPUtils.getDirection = function(mediaSection, sessionpart) {
  // Look for sendrecv, sendonly, recvonly, inactive, default to sendrecv.
  var lines = SDPUtils.splitLines(mediaSection);
  for (var i = 0; i < lines.length; i++) {
    switch (lines[i]) {
      case 'a=sendrecv':
      case 'a=sendonly':
      case 'a=recvonly':
      case 'a=inactive':
        return lines[i].substr(2);
      default:
        // FIXME: What should happen here?
    }
  }
  if (sessionpart) {
    return SDPUtils.getDirection(sessionpart);
  }
  return 'sendrecv';
};

SDPUtils.getKind = function(mediaSection) {
  var lines = SDPUtils.splitLines(mediaSection);
  var mline = lines[0].split(' ');
  return mline[0].substr(2);
};

SDPUtils.isRejected = function(mediaSection) {
  return mediaSection.split(' ', 2)[1] === '0';
};

SDPUtils.parseMLine = function(mediaSection) {
  var lines = SDPUtils.splitLines(mediaSection);
  var parts = lines[0].substr(2).split(' ');
  return {
    kind: parts[0],
    port: parseInt(parts[1], 10),
    protocol: parts[2],
    fmt: parts.slice(3).join(' ')
  };
};

SDPUtils.parseOLine = function(mediaSection) {
  var line = SDPUtils.matchPrefix(mediaSection, 'o=')[0];
  var parts = line.substr(2).split(' ');
  return {
    username: parts[0],
    sessionId: parts[1],
    sessionVersion: parseInt(parts[2], 10),
    netType: parts[3],
    addressType: parts[4],
    address: parts[5]
  };
};

// a very naive interpretation of a valid SDP.
SDPUtils.isValidSDP = function(blob) {
  if (typeof blob !== 'string' || blob.length === 0) {
    return false;
  }
  var lines = SDPUtils.splitLines(blob);
  for (var i = 0; i < lines.length; i++) {
    if (lines[i].length < 2 || lines[i].charAt(1) !== '=') {
      return false;
    }
    // TODO: check the modifier a bit more.
  }
  return true;
};

// Expose public methods.
if (typeof module === 'object') {
  module.exports = SDPUtils;
}

},{}]},{},[1])(1)
});
