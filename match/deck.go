package match

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/internal"
)

const deckStorageKey = "spy-cards-decks-v0"

// LoadDecks loads the player's stored decks.
func LoadDecks() []card.Deck {
	b, err := internal.LoadData(deckStorageKey)
	if err != nil {
		panic(err)
	}

	if len(b) == 0 {
		return nil
	}

	var names [][]string

	if err := json.Unmarshal(b, &names); err != nil {
		log.Println("error decoding decks:", err)

		return nil
	}

	decks := make([]card.Deck, 0, len(names))

	for _, deck := range names {
		if d := decodeDeck(deck); d != nil {
			decks = append(decks, d)
		}
	}

	return decks
}

// SaveDeck adds a deck to the top of the player's deck storage.
func SaveDeck(d card.Deck) {
	b, err := internal.LoadData(deckStorageKey)
	if err != nil {
		panic(err)
	}

	var names [][]string

	if len(b) != 0 {
		if err := json.Unmarshal(b, &names); err != nil {
			log.Println("error decoding decks:", err)

			return
		}
	}

	deck := make([]string, len(d))

	for i, c := range d {
		deck[i] = c.String()
	}

	for i := 0; i < len(names); i++ {
		same := len(names[i]) == len(deck)

		for j := 0; j < len(deck) && same; j++ {
			same = names[i][j] == deck[j]
		}

		if same {
			names = append(names[:i], names[i+1:]...)
			i--
		}
	}

	names = append([][]string{deck}, names...)

	b, err = json.Marshal(&names)
	if err != nil {
		log.Println("error encoding decks:", err)

		return
	}

	err = internal.StoreData(deckStorageKey, b)
	if err != nil {
		log.Println("error saving decks:", err)

		return
	}
}

var byNameVanilla = func() map[string]card.ID {
	m := make(map[string]card.ID)

	for i := card.ID(0); i < 128; i++ {
		if i.BasicIndex() >= -1 {
			m[i.String()] = i
		}
	}

	m["Belosstoss"] = card.Belostoss

	return m
}()

func decodeDeck(names []string) card.Deck {
	d := make(card.Deck, len(names))

	for i, s := range names {
		var ok bool

		d[i], ok = byNameVanilla[s]
		if ok {
			continue
		}

		for j, prefix := range []string{
			"Custom Attacker #",
			"Custom Effect #",
			"Custom Mini-Boss #",
			"Custom Boss #",
		} {
			if !strings.HasPrefix(s, prefix) {
				continue
			}

			index, err := strconv.ParseUint(s[len(prefix):], 10, 64)
			if err == nil {
				index--
				indexEnd := index & 31
				indexStart := index>>5 + 1

				index = indexStart<<7 | uint64(j)<<5 | indexEnd

				d[i] = card.ID(index)
				ok = true

				break
			}
		}

		if !ok {
			return nil
		}
	}

	return d
}
