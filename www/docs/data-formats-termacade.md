---
title: Data Formats (Termacade)
...

# Termacade Recording

- varint FormatVersion = 1
- varint\[3\] SpyCardsOnlineVersion
- varint GameType
    - 1: Mite Knight
    - 2: Flower Journey
- varint GameRuleOverridesLength
- byte\[GameRuleOverridesLength\] GameRuleOverrides (described below)
- varint SeedLength
- byte\[SeedLength\] Seed (UTF-8)
- varint StartPlayingTimestamp (milliseconds since unix epoch)
- varint GameDataLength
- byte\[GameDataLength\] GameData (see below)
- varint RandomIndex (final index of the deterministic random number generator)
- varint PlayerNameLength
- byte\[PlayerNameLength\] PlayerName (UTF-8)

## Game Rule Overrides

Repeated:

- varint GameRuleID (game-specific)
- one of: (depending on the game rule)
    - uint8 BooleanValue (0 or 1)
    - float64 NumberValue
    - float64\[2\] CoordinateValue

See Appendix B for a list of rules for each game.

## Game Data

### Shared (v1)

Repeated:

- varint ButtonsHeld (bitfield)
    - bit 0: Up
    - bit 1: Down
    - bit 2: Left
    - bit 3: Right
    - bit 4: Confirm
    - bit 5: Cancel
    - bit 6: Switch
    - bit 7: Toggle
    - bit 8: Pause
    - bit 9: Help
- varint AdditionalTicks (number of ticks this set of buttons is held, minus 1)

The recording starts on tick 0. Game logic starts on tick 1. This means that the first tick of input is dropped.
The reference implementation records tick 0's input as all buttons released.

### Mite Knight (v0)

Repeated:

- uint8 Buttons (bitfield)
    - Up (least significant bit)
    - Down
    - Left
    - Right
    - Shield
    - most significant 3 bits: reserved (0)
- varint FramesMinusOne (number of one-sixtieth second intervals until the next state, minus 1 - buttons are always held for at least one frame)

### Flower Journey (v0)

Repeated:

- varint TicksSincePreviousFlap (number of one-sixtieth second intervals since the last time the bee flapped its wings, or since the countdown finished if this is the first flap. the last of these items is the timestamp of the bee colliding with something.)
