---
title: Data Formats (Card v3 - Game Mode)
...

Since game modes and custom cards are shared using the same mechanism,
game mode format versions are disjoint with custom card versions.

(base64-encoded)

- varint FormatVersion (3)
- varint FieldCount
- GameModeField\[FieldCount\] Fields

# GameModeField

- varint Type
- varint DataLength
- byte\[DataLength\] Data

## 0: Metadata

- varint TitleLength
- byte\[TitleLength\] Title (UTF-8)
- varint AuthorLength
- byte\[AuthorLength\] Author (UTF-8)
- varint DescriptionLength
- byte\[DescriptionLength\] Description (UTF-8)
- varint LatestChangesLength
- byte\[LatestChangesLength\] LatestChanges (UTF-8)

## 1: BannedCards

If BannedCards is present and BannedCardCount is 0, all vanilla cards are banned.

- uint8 Padding = 0
    - If this is not the first byte, or if the encoded data is exactly 1 byte in length, ignore this field and the Flags field.
- uvarint Flags
    - 0: cards are unpickable but can still be random-summoned
- uvarint BannedCardCount
- uvarint\[BannedCardCount\] BannedCardIDs

## 2: GameRules

Repeated:

- varint Rule
- varint Value

If Rule is None, there is no value and this field ends.

Rules that have their default value are not encoded.

Rules and their default values are as follows:

- 0: None (no value)
- 1: MaxHP (5)
- 2: HandMinSize (3)
- 3: HandMaxSize (5) (max value 50)
- 4: DrawPerTurn (2)
- 5: CardsPerDeck (15)
- 6: MinTP (2)
- 7: MaxTP (10)
- 8: TPPerTurn (1)
- 9: BossCards (1)
- 10: MiniBossCards (2)

## 3: SummonCard

- varint Flags
    - bit 0: both players (if not set, summon for player 1 only)
- varint CardID

## 4: Variant

- string Title
- string NPC
- varint FieldCount
- Field[FieldCount] Fields

## 5: UnfilterCard

- varint Flags
- varint CardID

## 6: DeckLimitFilter

- uvarint Count
- uint8 RankTribe (high 4 bits rank, low 4 bits tribe)
- If RankTribe is 128:
    - [CardFilter (v5)](data-formats-card-5.html#cardfilter) Filter
- If RankTribe is 255:
    - uvarint CardID
- Otherwise:
    - (only RankTribe is 14 mod 16) string CustomTribe

## 7: Timer

- uvarint StartTime
- uvarint MaxTime
- uvarint PerTurn
- uvarint MaxPerTurn

## 8: Turn0Effect

- uvarint Flags
- uvarint CardFormatVersion
- EffectDef Effect
