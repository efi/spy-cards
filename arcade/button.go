//go:generate stringer -type Button -trimprefix Btn

package arcade

// Button represents a gamepad button.
type Button uint8

// Constants for Button.
const (
	BtnUp      Button = 0
	BtnDown    Button = 1
	BtnLeft    Button = 2
	BtnRight   Button = 3
	BtnConfirm Button = 4
	BtnCancel  Button = 5
	BtnSwitch  Button = 6
	BtnToggle  Button = 7
	BtnPause   Button = 8
	BtnHelp    Button = 9
)

// PackButtons combines the specified buttons into a bitmask.
// Order is not preserved, and any repeated buttons are only stored once.
func PackButtons(buttons ...Button) uint64 {
	var n uint64

	for _, btn := range buttons {
		n |= 1 << btn
	}

	return n
}

// UnpackButtons extracts the buttons from a given bitmask.
func UnpackButtons(mask uint64) []Button {
	var buttons []Button

	for i := 0; mask != 0; i, mask = i+1, mask>>1 {
		if mask&1 != 0 {
			buttons = append(buttons, Button(i))
		}
	}

	return buttons
}
