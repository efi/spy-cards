package server

import (
	"crypto/rand"
	"database/sql"
	"html/template"
	"log"
	"math/big"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/format"
	"github.com/davecgh/go-spew/spew"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/xerrors"
)

var (
	tmplLanding = template.Must(template.New("landing").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href="..">&larr; Back</a></p>
<ul>
<li><a href="portraits?page=1">Card Portraits</a></li>
<li><a href="reports?page=1">Issue Reports</a></li>
<li><a href="recordings?page=1">Recordings (Spy Cards)</a></li>
<li><a href="arcade-recordings?page=1">Recordings (Termacade)</a></li>
<li><a href="modes?page=1">Game Modes</a></li>
<li><a href="users?page=1">Users</a></li>
</ul>
<p>Outstanding matchmaking codes: {{len .MatchCodes}}</p>
<ul>
{{range .MatchCodes}}
<li>{{.}}</li>
{{end}}
</ul>
</body>
</html>
`))
	tmplPortraits = template.Must(template.New("portraits").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Portraits (page {{.CurrentPage}} of {{.MaxPage}}) | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
{{range .Portraits}}
<img src="{{.ImageURL}}" height="128" width="128" data-id="{{.ID.ID}}">
{{end}}
<form method="get">
<label>Page <input type="number" name="page" min="1" max="{{.MaxPage}}" value="{{.CurrentPage}}"> of {{.MaxPage}}</label>
<input type="submit" value="Go!">
</form>
</body>
</html>
`))
	tmplReports = template.Must(template.New("reports").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Reports (page {{.CurrentPage}} of {{.MaxPage}}) | Spy Cards Admin</title>
<meta charset="utf-8">
<style>
pre {
	white-space: pre-wrap;
}
.has-dev-comment {
	color: #808080;
}
</style>
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<table border="1">
<thead>
<th>ID</th>
<th>Version</th>
<th>Error</th>
<th>Game Data</th>
<th>User Comment</th>
<th>Developer Comment</th>
</thead>
<tbody>
{{range .Reports}}
<tr id="report-{{.ID}}" class="{{if .DevComment.Valid}}has-dev-comment{{end}}">
<td>{{.ID}}</td>
<td><details><summary>{{.Version}}</summary><br>{{.CacheVersion}}<br>UA: {{.UserAgent}}</details></td>
<td><pre><details><summary><strong>{{.Message}}</strong></summary>
{{.Stack}}</details></pre></td>
<td><details><summary>State</summary><pre class="state-data">{{.State}}</pre></details>
{{with .Custom}}<details><summary>Custom Card Data</summary><pre class="custom-card-data">{{.}}</pre></details>{{end}}</td>
<td>{{with .UserComment}}<pre class="user-comment">{{.}}</pre>{{else}}<em>(no comment attached)</em>{{end}}</td>
<td><form method="post" action="#report-{{.ID}}"><textarea class="dev-comment" name="comment">{{.DevComment.String}}</textarea><input type="hidden" name="id" value="{{.ID}}"><button type="submit">Save</button><input type="hidden" name="csrf-token" value="{{$.CSRFToken}}"></form></td>
</tr>
{{end}}
</tbody>
</table>
<form method="get">
<label>Page <input type="number" name="page" min="1" max="{{.MaxPage}}" value="{{.CurrentPage}}"> of {{.MaxPage}}</label>
<input type="submit" value="Go!">
</form>
<script>
function removeCardNulls(obj, isCard) {
	if (Array.isArray(obj)) {
		for (let c of obj) {
			removeCardNulls(c, isCard);
		}
		return;
	}

	if (typeof obj !== "object" || !obj) {
		return;
	}

	for (let key of Object.keys(obj)) {
		if (isCard && obj[key] === null) {
			delete obj[key];
			continue;
		}
		removeCardNulls(obj[key], isCard || key === "card");
	}

	return obj;
}
document.querySelectorAll(".state-data").forEach(function(el) {
	const obj = removeCardNulls(JSON.parse(el.textContent), false);
	const btn = document.createElement("button");
	btn.textContent = "Log to console";
	btn.addEventListener("click", function(e) {
		e.preventDefault();

		console.log(obj);
	});
	el.parentNode.insertBefore(btn, el);
	el.textContent = JSON.stringify(obj, null, "\t");
});
document.querySelectorAll(".custom-card-data").forEach(function(el) {
	el.textContent = el.textContent.split(/,/g).join("\n");
});
document.querySelectorAll(".dev-comment").forEach(function(el) {
	el.addEventListener("input", function() {
		el.parentNode.style.backgroundColor = "#ff0";
	});
});
</script>
</body>
</html>
`))
	tmplRecordings = template.Must(template.New("recordings").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Spy Cards Recordings (page {{.CurrentPage}} of {{.MaxPage}}) | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<table border="1">
<thead>
<th>ID</th>
<th>Code</th>
<th>Version</th>
<th>Mode</th>
<th>Date</th>
</thead>
<tbody>
{{range .Recordings}}
<tr id="recording-{{.ID.ID}}">
<td>{{.ID.ID}}</td>
<td><a href="https://spy-cards.lubar.me/viewer.html#{{.Code}}">{{.Code}}</a></td>
<td>{{.VersionMajor}}.{{.VersionMinor}}.{{.VersionPatch}}</td>
<td>{{.ModePrefix}}{{if .ModeSuffix.Valid}}.{{.ModeSuffix.String}}{{end}}</td>
<td>{{.UploadedAt.Format "2006-01-02T15:04:05Z07:00"}}</td>
</tr>
{{end}}
</tbody>
</table>
<form method="get">
<label>Page <input type="number" name="page" min="1" max="{{.MaxPage}}" value="{{.CurrentPage}}"> of {{.MaxPage}}</label>
<input type="submit" value="Go!">
</form>
</body>
</html>
`))
	tmplArcadeRecordings = template.Must(template.New("arcade-recordings").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Termacade Recordings (page {{.CurrentPage}} of {{.MaxPage}}) | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<table border="1">
<thead>
<th>ID</th>
<th>Code</th>
<th>Date</th>
<th>Game</th>
<th>Player</th>
<th>Score</th>
<th>Status</th>
</thead>
<tbody>
{{range .Recordings}}
<tr id="recording-{{.ID.ID}}">
<td>{{.ID.ID}}</td>
<td><a href="/arcade.html?recording={{.Code}}">{{.Code}}</a></td>
<td>{{.UploadedAt.Format "2006-01-02T15:04:05Z07:00"}}</td>
<td>{{if .Game.Valid}}{{.Game.String}} {{if .Vanilla.Valid}}{{if .Vanilla.Bool}}(vanilla){{else}}(custom){{end}}{{else}}(unknown){{end}}{{else}}<em>(unknown)</em>{{end}}</td>
<td>{{if .Player.Valid}}{{.Player.String}}{{else}}<em>(unknown)</em>{{end}}</td>
<td>{{if .Score.Valid}}{{.Score.Int64}}{{else}}<em>(unknown)</em>{{end}}</td>
<td><pre>{{if .Failed.Valid}}decoding failed: <em>{{.Failed.String}}</em>{{else}}<em>valid</em>{{end}}</pre>
<form method="post" action="#recording-{{.ID.ID}}"><input type="hidden" name="retry" value="{{.ID.ID}}"><button type="submit">Retry</button><input type="hidden" name="csrf-token" value="{{$.CSRFToken}}"></form></td>
</tr>
{{end}}
</tbody>
</table>
<form method="get">
<label>Page <input type="number" name="page" min="1" max="{{.MaxPage}}" value="{{.CurrentPage}}"> of {{.MaxPage}}</label>
<input type="submit" value="Go!">
</form>
</body>
</html>
`))
	tmplModes = template.Must(template.New("modes").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Game Modes (page {{.CurrentPage}} of {{.MaxPage}}) | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<table border="1">
<thead>
<th>ID</th>
<th>URL Slug</th>
<th>Quick Join</th>
<th>Name</th>
<th>Revisions</th>
</thead>
<tbody>
{{range .Modes}}
<tr id="game-mode-{{.ID}}">
<td>{{.ID}}</td>
<td><a href="https://spy-cards.lubar.me/play.html?mode={{.Client}}">{{.Client}}</a></td>
<td>{{if .Prefix.Valid}}<code>{{.Prefix.String}}</code>{{else}}<em>(no quick join)</em>{{end}}</td>
<td>{{.Name}}</td>
<td>{{.Revisions}}</td>
</tr>
{{end}}
</tbody>
</table>
<form method="get">
<label>Page <input type="number" name="page" min="1" max="{{.MaxPage}}" value="{{.CurrentPage}}"> of {{.MaxPage}}</label>
<input type="submit" value="Go!">
</form>
</body>
</html>
`))
	tmplUsers = template.Must(template.New("users").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Users (page {{.CurrentPage}} of {{.MaxPage}}) | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<p><a href="new-user">New User</a></p>
<table border="1">
<thead>
<th>ID</th>
<th>Login Name</th>
<th>Flags</th>
</thead>
<tbody>
{{range .Users}}
<tr id="user-{{.ID}}">
<td>{{.ID}}</td>
<td><a href="user?id={{.ID}}">{{.LoginName}}</a></td>
<td>
{{if eq .SessionReset 0}}DEFAULT_PASSWORD{{end}}
{{if .IsAdmin}}ADMIN{{end}}
</td>
</tr>
{{end}}
</tbody>
</table>
<form method="get">
<label>Page <input type="number" name="page" min="1" max="{{.MaxPage}}" value="{{.CurrentPage}}"> of {{.MaxPage}}</label>
<input type="submit" value="Go!">
</form>
</body>
</html>
`))
	tmplNewUser = template.Must(template.New("new-user").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>Create New User | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<form method="post">
{{if .Error}}<p class="error">{{.Error}}</p>{{end}}
<p><label>Login Name:<br><input type="text" name="username" value="{{.Username}}" required></label></p>
<p><label>Password:<br><input type="text" name="password" value="{{.Password}}" required></label></p>
<p><button type="submit">Create</button></p>
<input type="hidden" name="csrf-token" value="{{$.CSRFToken}}">
</form>
</body>
</html>
`))
	tmplUser = template.Must(template.New("user").Parse(`<!DOCTYPE html>
<html lang="en">
<head>
<title>User {{.User.ID}} | Spy Cards Admin</title>
<meta charset="utf-8">
</head>
<body>
<p><a href=".">&larr; Back</a></p>
<dl>
<dt>User ID</dt>
<dd>{{.User.ID}}</dd>
<dt>Login Name</dt>
<dd>{{.User.LoginName}}</dd>
<dt>Session Reset Count</dt>
<dd>{{.User.SessionReset}}</dd>
<dt>Is Admin</dt>
<dd>{{.User.IsAdmin}}</dd>
</dl>
<table border="1">
<thead>
<th>ID</th>
<th>URL Slug</th>
<th>Name</th>
</thead>
<tbody>
{{range .Modes}}
<tr id="game-mode-{{.ID}}">
<td>{{.ID}}</td>
<td><a href="https://spy-cards.lubar.me/play.html?mode={{.Client}}">{{.Client}}</a></td>
<td>{{.Name}}</td>
</tr>
{{end}}
</tbody>
</table>
</body>
</html>
`))
)

var adminRouter http.ServeMux

func adminMiddleware(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	u, err := getCurrentUser(&r)
	if err != nil {
		log.Println("gcu admin", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if u == nil {
		http.SetCookie(w, &http.Cookie{
			Name:     "spy_cards_login_redirect",
			Value:    r.URL.RequestURI(),
			SameSite: http.SameSiteStrictMode,
			HttpOnly: true,
			Secure:   true,
		})
		http.Redirect(w, r, "/spy-cards/log-in", http.StatusFound)

		return
	}

	if !u.IsAdmin {
		http.NotFound(w, r)

		return
	}

	adminRouter.ServeHTTP(w, r)
}

func adminLanding(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/spy-cards/admin/" {
		http.NotFound(w, r)

		return
	}

	var data struct {
		MatchCodes []string
	}

	sessions.Lock()

	for k := range sessions.m {
		data.MatchCodes = append(data.MatchCodes, format.Encode32Space(k[:]))
	}

	sessions.Unlock()

	sort.Strings(data.MatchCodes)

	w.Header().Add("Content-Type", "text/html")
	_ = tmplLanding.Execute(w, &data)
}

func adminPortraits(w http.ResponseWriter, r *http.Request) {
	type PortraitData struct {
		ID       format.FileID
		ImageURL string
	}

	var data struct {
		Portraits   []PortraitData
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	err := getPortraitCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting portrait count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 249) / 250

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getPortraitList.Query((data.CurrentPage-1)*250, 250)
	if err != nil {
		http.Error(w, "getting portraits: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			portrait PortraitData
			hash     []byte
		)

		if err = rows.Scan(&portrait.ID.ID, &hash); err != nil {
			http.Error(w, "getting portraits: "+err.Error(), http.StatusInternalServerError)

			return
		}

		copy(portrait.ID.Hash[:], hash)

		b, _ := portrait.ID.MarshalText()
		portrait.ImageURL = "../user-img/" + string(b) + ".png"
		data.Portraits = append(data.Portraits, portrait)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting portraits: "+err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplPortraits.Execute(w, &data)
}

func adminReports(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		_, err = updateReportDevComment.Exec(r.FormValue("id"), r.FormValue("comment"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		http.Redirect(w, r, r.URL.String(), http.StatusFound)

		return
	}

	type ReportData struct {
		ID           int64
		Version      string
		CacheVersion string
		UserAgent    string
		Message      string
		Stack        string
		State        string
		Custom       string
		UserComment  string
		DevComment   sql.NullString
	}

	var data struct {
		Reports     []ReportData
		CurrentPage int
		MaxPage     int
		TotalCount  int
		CSRFToken   string
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	err = getReportCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting report count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 24) / 25

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getReportList.Query((data.CurrentPage-1)*25, 25)
	if err != nil {
		http.Error(w, "getting reports: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var report ReportData
		if err = rows.Scan(&report.ID, &report.Version, &report.CacheVersion, &report.UserAgent, &report.Message, &report.Stack, &report.State, &report.Custom, &report.UserComment, &report.DevComment); err != nil {
			http.Error(w, "getting reports: "+err.Error(), http.StatusInternalServerError)

			return
		}

		data.Reports = append(data.Reports, report)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting reports: "+err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplReports.Execute(w, &data)
}

func adminRecordings(w http.ResponseWriter, r *http.Request) {
	type RecordingData struct {
		ID           format.FileID
		UploadedAt   time.Time
		VersionMajor int64
		VersionMinor int64
		VersionPatch int64
		ModePrefix   string
		ModeSuffix   sql.NullString
		Code         string
	}

	var data struct {
		Recordings  []RecordingData
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	err := getRecordingCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting recording count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 249) / 250

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getRecordingList.Query((data.CurrentPage-1)*250, 250)
	if err != nil {
		http.Error(w, "getting recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			recording RecordingData
			hash      []byte
		)

		if err = rows.Scan(&recording.ID.ID, &recording.UploadedAt, &hash, &recording.VersionMajor, &recording.VersionMinor, &recording.VersionPatch, &recording.ModePrefix, &recording.ModeSuffix); err != nil {
			http.Error(w, "getting recordings: "+err.Error(), http.StatusInternalServerError)

			return
		}

		copy(recording.ID.Hash[:], hash)

		code, err := recording.ID.MarshalText()
		if err != nil {
			http.Error(w, "marshaling recording ID: "+err.Error(), http.StatusInternalServerError)

			return
		}

		recording.Code = string(code)

		data.Recordings = append(data.Recordings, recording)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplRecordings.Execute(w, &data)
}

// GameMode is a community-owned Spy Cards Online custom game mode.
type GameMode struct {
	ID        int64
	Client    string
	Prefix    sql.NullString
	Name      string
	Revisions int
}

func adminModes(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Modes       []GameMode
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	err := getCustomGameModeCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting game mode count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 49) / 50

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getCustomGameModeList.Query((data.CurrentPage-1)*50, 50)
	if err != nil {
		http.Error(w, "getting game modes: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var mode GameMode
		if err = rows.Scan(&mode.ID, &mode.Client, &mode.Prefix, &mode.Name, &mode.Revisions); err != nil {
			http.Error(w, "getting game modes: "+err.Error(), http.StatusInternalServerError)

			return
		}

		data.Modes = append(data.Modes, mode)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting game modes: "+err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplModes.Execute(w, &data)
}

func adminUsers(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Users       []User
		CurrentPage int
		MaxPage     int
		TotalCount  int
	}

	err := getUserCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting user count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 49) / 50

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getUserList.Query((data.CurrentPage-1)*50, 50)
	if err != nil {
		http.Error(w, "getting users: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var user User
		if err = rows.Scan(&user.ID, &user.LoginName, &user.SessionReset, &user.IsAdmin); err != nil {
			http.Error(w, "getting users: "+err.Error(), http.StatusInternalServerError)

			return
		}

		data.Users = append(data.Users, user)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting users: "+err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplUsers.Execute(w, &data)
}

func adminNewUser(w http.ResponseWriter, r *http.Request) {
	var data struct {
		Error     string
		Username  string
		Password  string
		CSRFToken string
	}

	data.Username = r.PostFormValue("username")
	data.Password = r.PostFormValue("password")

	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		switch {
		case data.Username == "":
			data.Error = "Missing username."
		case data.Password == "":
			data.Error = "Missing password."
		case url.QueryEscape(data.Username) != data.Username, strings.ToLower(data.Username) != data.Username:
			data.Error = "Invalid username."
		}

		if data.Error == "" {
			var userID int64

			cryptPass, err := bcrypt.GenerateFromPassword([]byte(data.Password), bcrypt.DefaultCost)
			if err == nil {
				err = createUser.QueryRow(data.Username, cryptPass).Scan(&userID)
			}

			if err != nil {
				data.Error = err.Error()
			} else {
				http.Redirect(w, r, "user?id="+strconv.FormatInt(userID, 10), http.StatusFound)
			}
		}
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	if data.Password == "" {
		max := big.NewInt(10000)

		n, err := rand.Int(rand.Reader, max)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		n.Add(n, max)
		data.Password = "changeme" + n.String()[1:]
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplNewUser.Execute(w, &data)
}

func adminUser(w http.ResponseWriter, r *http.Request) {
	var data struct {
		User  User
		Modes []GameMode
	}

	var err error

	data.User.ID, err = strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)

		return
	}

	err = getUserByID.QueryRow(data.User.ID).Scan(&data.User.LoginName, &data.User.SessionReset, &data.User.IsAdmin)
	if xerrors.Is(err, sql.ErrNoRows) {
		http.NotFound(w, r)

		return
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	modes, err := getCustomGameModesForOwner.Query(data.User.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}
	defer modes.Close()

	for modes.Next() {
		var mode GameMode
		if err = modes.Scan(&mode.ID, &mode.Client, &mode.Name); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}

		data.Modes = append(data.Modes, mode)
	}

	if err = modes.Err(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplUser.Execute(w, &data)
}

func adminArcadeRecordings(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		csrfKey, err := getCSRFKey(w, r, false)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		if !verifyCSRFToken(csrfKey, r.FormValue("csrf-token")) {
			http.Error(w, "request validation failed (try again)", http.StatusBadRequest)

			return
		}

		id, err := strconv.ParseInt(r.PostFormValue("retry"), 10, 64)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)

			return
		}

		decodeArcadeRecording(id)

		http.Redirect(w, r, r.RequestURI, http.StatusFound)

		return
	}

	type RecordingData struct {
		ID         format.FileID
		UploadedAt time.Time
		Game       sql.NullString
		Player     sql.NullString
		Score      sql.NullInt64
		Vanilla    sql.NullBool
		Failed     sql.NullString
		Code       string
	}

	var data struct {
		Recordings  []RecordingData
		CurrentPage int
		MaxPage     int
		TotalCount  int
		CSRFToken   string
	}

	err := getArcadeRecordingCount.QueryRow().Scan(&data.TotalCount)
	if err != nil {
		http.Error(w, "getting arcade recording count: "+err.Error(), http.StatusInternalServerError)

		return
	}

	data.MaxPage = (data.TotalCount + 249) / 250

	data.CurrentPage, err = strconv.Atoi(r.FormValue("page"))
	if err != nil || data.CurrentPage <= 0 {
		data.CurrentPage = 1
	}

	rows, err := getArcadeRecordingList.Query((data.CurrentPage-1)*250, 250)
	if err != nil {
		http.Error(w, "getting arcade recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	for rows.Next() {
		var (
			recording RecordingData
			hash      []byte
		)

		if err = rows.Scan(&recording.ID.ID, &recording.UploadedAt, &hash, &recording.Game, &recording.Player, &recording.Score, &recording.Vanilla, &recording.Failed); err != nil {
			http.Error(w, "getting arcade recordings: "+err.Error(), http.StatusInternalServerError)

			return
		}

		copy(recording.ID.Hash[:], hash)

		code, err := recording.ID.MarshalText()
		if err != nil {
			http.Error(w, "marshaling recording ID: "+err.Error(), http.StatusInternalServerError)

			return
		}

		recording.Code = string(code)

		data.Recordings = append(data.Recordings, recording)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, "getting arcade recordings: "+err.Error(), http.StatusInternalServerError)

		return
	}

	csrfKey, err := getCSRFKey(w, r, true)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	data.CSRFToken, err = createCSRFToken(csrfKey)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/html")
	_ = tmplArcadeRecordings.Execute(w, &data)
}

func handleIssueReportPost(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPost)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPost {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPost {
		w.Header().Add("Allow", "POST, OPTIONS")
		http.Error(w, "only POST is allowed", http.StatusMethodNotAllowed)

		return
	}

	err := r.ParseMultipartForm(1 << 20)
	if err != nil {
		http.Error(w, "bad request", http.StatusBadRequest)

		return
	}

	_, err = insertIssueReport.Exec(
		r.Header.Get("X-Forwarded-For"),
		r.UserAgent(),
		r.Form.Get("v"),
		r.Form.Get("v2"),
		r.Form.Get("m"),
		r.Form.Get("t"),
		r.Form.Get("s"),
		r.Form.Get("c"),
		r.Form.Get("u"),
	)
	if err != nil {
		log.Println("error submitting report", err)
		spew.Dump(r.Form)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	w.WriteHeader(http.StatusAccepted)
}
