// +build headless

package flowerjourney_test

import (
	"context"
	"io/ioutil"
	"path/filepath"
	"testing"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/input"
)

type recording struct {
	FileID string
	Player string
	Score  uint64
}

var recordings = []*recording{
	{"4FN777Y793GA848", "SMA", 7765},
	{"91J65ZWY66M3P40", "MBI", 8830},
	{"BS25G0E8NSZCGGR", "SMA", 11130},
	{"G4RNZ8DBRNMA47R", "MBI", 16570},
}

func TestFlowerJourney(t *testing.T) {
	ctx, _ := input.NewContext(context.Background(), func() ([]arcade.Button, float32, float32, bool) {
		return nil, -1, -1, false
	})

	for i := range recordings {
		r := recordings[i]

		t.Run(r.FileID, func(t *testing.T) {
			b, err := ioutil.ReadFile(filepath.Join("testdata", r.FileID))
			if err != nil {
				t.Fatal(err)
			}

			var rec arcade.Recording

			err = rec.UnmarshalBinary(b)
			if err != nil {
				t.Fatal(err)
			}

			state, err := game.PlayRecording(ctx, flowerjourney.New(*rec.Rules.(*arcade.FlowerJourneyRules)), &rec)
			if err != nil {
				t.Fatal(err)
			}

			if uint64(state.Score) != r.Score {
				t.Errorf("expected score: %v actual score: %v", r.Score, state.Score)
			}

			if state.Playback.PlayerName != r.Player {
				t.Errorf("expected player name: %v actual player name: %v", r.Player, state.Playback.PlayerName)
			}
		})
	}
}
