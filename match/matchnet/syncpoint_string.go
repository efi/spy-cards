// Code generated by "stringer -type syncPoint -trimprefix sync"; DO NOT EDIT.

package matchnet

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[syncInitialSeed-0]
	_ = x[syncDeckPromise-1]
	_ = x[syncTurnSeedPromise-2]
	_ = x[syncTurnSeed-3]
	_ = x[syncConfirmTurn-4]
	_ = x[syncFinalize-5]
}

const _syncPoint_name = "InitialSeedDeckPromiseTurnSeedPromiseTurnSeedConfirmTurnFinalize"

var _syncPoint_index = [...]uint8{0, 11, 22, 37, 45, 56, 64}

func (i syncPoint) String() string {
	if i >= syncPoint(len(_syncPoint_index)-1) {
		return "syncPoint(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _syncPoint_name[_syncPoint_index[i]:_syncPoint_index[i+1]]
}
