package spoilerguard

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"sync"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
)

var (
	prDeny = map[uint8]bool{
		2:   true,
		5:   true,
		21:  true,
		27:  true,
		32:  true,
		37:  true,
		47:  true,
		53:  true,
		75:  true,
		76:  true,
		122: true,
		127: true,
		146: true,
		162: true,
	}
	prtImg     = make(map[string]bool)
	prtImgOnce sync.Once

	cardEnemyIDs = []card.ID{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 14, 15, 16, 17, 19, 20, 21, 23, 24, 25,
		26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
		44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 61, 63, 64,
		65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82,
		83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98,
	}
)

// Apply applies card restrictions and menu codes to the set.
func Apply(ctx context.Context, set *card.Set, remoteData []uint8) {
	var localData []uint8

	localGuard := LoadData()
	if localGuard != nil {
		localData = localGuard.SeenSpiedEnemies
	}

	if len(localData) != 0 || len(remoteData) != 0 {
		if len(remoteData) != 256/8 {
			remoteData = make([]uint8, 256/8)

			for i := range remoteData {
				remoteData[i] = 255
			}
		} else {
			set.SpoilerGuard[1] = new([32]byte)
			copy(set.SpoilerGuard[1][:], remoteData)
		}

		if len(localData) != 256/8 {
			localData = make([]uint8, 256/8)

			for i := range localData {
				localData[i] = 255
			}
		} else {
			set.SpoilerGuard[0] = new([32]byte)
			copy(set.SpoilerGuard[0][:], localData)
		}

		var banned, unpickable card.BannedCards

		unpickable.Flags |= card.BannedCardRandomSummonable

		for _, card := range cardEnemyIDs {
			i := card >> 2
			j := (card & 3) << 1

			if localData[i]&(3<<j) == 1 {
				unpickable.Cards = append(unpickable.Cards, card)
			}

			if localData[i]&remoteData[i]&(1<<j) == 0 {
				banned.Cards = append(banned.Cards, card)
			}
		}

		if len(banned.Cards) != 0 {
			set.Spoiler = append(set.Spoiler, &banned)
		}

		if len(unpickable.Cards) != 0 {
			set.Spoiler = append(set.Spoiler, &unpickable)
		}

		if set.AppliedPR || localGuard == nil || localGuard.Menu == 0 {
			return
		}

		if localGuard.Menu&8 != 0 {
			prtImgOnce.Do(func() {
				initPrtImg(ctx)
			})

			set.AppliedPR = true

			for _, id := range cardEnemyIDs {
				var found bool

				for _, c := range set.Cards {
					if c.ID == id {
						found = true

						break
					}
				}

				if !found {
					set.Cards = append(set.Cards, card.Vanilla(id))
				}
			}

			for _, c := range set.Cards {
				if c.Portrait == card.PortraitCustomExternal {
					if prt, ok := prtImg[format.Encode32(c.CustomPortrait)]; ok && prt {
						c.Portrait = 17
						c.CustomPortrait = nil

						continue
					} else if ok {
						continue
					}
				}

				if c.Portrait == 88 {
					c.Portrait = 17

					continue
				}

				if prDeny[c.Portrait] {
					continue
				}

				c.Portrait = card.PortraitCustomExternal
				c.CustomPortrait, _ = format.Decode32("FTCZKAQ95KFP818")
			}
		}

		set.RandomByBackOrder = localGuard.Menu&32 != 0
	}
}

func initPrtImg(ctx context.Context) {
	prtImg = make(map[string]bool)

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, internal.GetConfig(ctx).UserImageBaseURL+"prt.json", nil)
	if err != nil {
		log.Println("ERROR: could not request prt.json:", err)

		return
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println("ERROR: could not load prt.json:", err)

		return
	}
	defer resp.Body.Close()

	var prt []string
	if err := json.NewDecoder(resp.Body).Decode(&prt); err != nil {
		log.Println("ERROR: could not parse prt.json:", err)

		return
	}

	for _, s := range prt {
		if strings.HasPrefix(s, "!") {
			prtImg[s[1:]] = false
		} else {
			prtImg[s] = true
		}
	}
}
