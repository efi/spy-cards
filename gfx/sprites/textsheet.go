package sprites

import (
	"git.lubar.me/ben/spy-cards/gfx"
)

func embeddedTextSheet(unitPerPx float32, nearest bool, dx, dy int, mask []byte) *textSheetWrapper {
	return &textSheetWrapper{
		sheet: &spriteSheet{
			AssetTexture: gfx.NewEmbeddedTexture("(fonts)", nearest, func() ([]byte, string, error) {
				return mask, "image/png", nil
			}),

			width:     dx,
			height:    dy,
			unitPerPx: unitPerPx,
		},
	}
}

type textSheetWrapper struct {
	sheet *spriteSheet
}

func (t *textSheetWrapper) sprite(yOff int, x, y, w, h, px, py float32) [2]*Sprite {
	y += float32(t.sheet.height - yOff)

	return [2]*Sprite{
		t.sheet.sprite(x, y, w, h, px, py),
		t.sheet.sprite(x+float32(t.sheet.width/2), y, w, h, px, py),
	}
}

func embeddedSheet(name string, unitPerPx float32, nearest bool, dx, dy int, src []byte) *spriteSheet {
	return &spriteSheet{
		AssetTexture: gfx.NewEmbeddedTexture(name, nearest, func() ([]byte, string, error) {
			return src, "image/png", nil
		}),

		width:     dx,
		height:    dy,
		unitPerPx: unitPerPx,
	}
}
