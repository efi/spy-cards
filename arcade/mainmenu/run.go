// Package mainmenu handles the Termacade main menu.
package mainmenu

import (
	"context"
	"runtime"
	"sync"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/crt"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/internal"
)

func preload(funcs ...func() error) (<-chan float64, <-chan error) {
	progress := make(chan float64, 1)
	result := make(chan error, 1)

	var (
		lock   sync.Mutex
		loaded float64
		total  = float64(len(funcs))
	)

	run := func(async bool, f func()) {
		// invert async on android
		if runtime.GOOS == "android" {
			async = !async
		}

		if async {
			go f()
		} else {
			f()
		}
	}

	run(false, func() {
		for i := range funcs {
			fn := funcs[i]

			run(true, func() {
				err := fn()
				if err != nil {
					select {
					case result <- err:
					default:
					}

					return
				}

				lock.Lock()

				loaded++
				select {
				case <-progress:
				default:
				}
				progress <- loaded / total

				if loaded == total {
					close(result)
				}

				lock.Unlock()
			})
		}
	})

	return progress, result
}

// MainMenu is the Termacade main menu.
type MainMenu struct {
	CRT       crt.CRT
	Camera    gfx.Camera
	Selection int

	batch *sprites.Batch
	tb    *sprites.TextBatch

	LastSelectionChange time.Time
	neonLoaded          bool
}

// Run runs the Termacade main menu.
func (mm *MainMenu) Run(ctx context.Context) error {
	internal.SetTitle("Termacade")

	for i := 0; i < len(Options); i++ {
		if Options[i].Hidden {
			Options = append(Options[:i], Options[i+1:]...)
			i--
		}
	}

	ictx := input.GetContext(ctx)

	mm.CRT.SetDefaults()
	mm.Camera.SetDefaults()
	mm.Camera.PushTransform(gfx.Translation(0, 70, 3))

	mm.Selection = internal.LoadSettings().LastTermacadeOption
	if mm.Selection >= len(Options) {
		mm.Selection = len(Options) - 1
	}

	neonLoaded := make(chan error, 1)
	musicPlaying := make(chan struct{})

	go func() {
		neonLoaded <- sprites.NeonFJBee.Preload()
	}()

	var (
		preloadProgress, preloadDone = preload(
			// Sprites
			sprites.Blank.Preload,
			sprites.GamepadButton[0][0].Preload,
			sprites.FJBack.Preload,
			sprites.NeonFJBee.Preload,
			sprites.AttractFJ.Preload,
			sprites.AttractMK.Preload,
			sprites.ParticleGrassPlaceholder.Preload,
			sprites.ParticleSmoke.Preload,
			sprites.ParticleStar.Preload,

			// Songs
			audio.TermiteLoop.Preload,
			audio.MiteKnight.Preload,
			audio.FlyingBee.Preload,

			// Main Menu
			audio.Buzzer.Preload,
			audio.Confirm.Preload,
			audio.Confirm1.Preload,

			// Mite Knight
			audio.MiteKnightIntro.Preload,
			audio.PeacockSpiderNPCSummonSuccess.Preload,
			audio.Shot2.Preload,
			audio.MKDeath.Preload,
			audio.MKGameOver.Preload,
			audio.MKHit.Preload,
			audio.MKHit2.Preload,
			audio.MKKey.Preload,
			audio.MKOpen.Preload,
			audio.MKPotion.Preload,
			audio.MKStairs.Preload,
			audio.MKWalk.Preload,

			// Flower Journey
			audio.FBCountdown.Preload,
			audio.FBDeath.Preload,
			audio.FBFlower.Preload,
			audio.FBGameOver.Preload,
			audio.FBPoint.Preload,
			audio.FBStart.Preload,
		)
		progress     float64
		preloadReady bool
		songReady    bool
	)

	go func() {
		audio.TermiteLoop.PlayMusic(0, false)
		close(musicPlaying)
	}()

	for !preloadReady || !mm.neonLoaded || !songReady {
		select {
		case err := <-neonLoaded:
			if err != nil {
				return err
			}

			mm.neonLoaded = true
		default:
		}

		select {
		case progress = <-preloadProgress:
		default:
		}

		select {
		case <-musicPlaying:
			songReady = true
		default:
		}

		if !preloadReady {
			select {
			case err := <-preloadDone:
				if err != nil {
					return err
				}

				preloadReady = true
			default:
			}
		}

		mm.CRT.RGBNoise = 0
		mm.CRT.NoiseX = 1 - progress

		mm.CRT.Draw(mm.render)

		gfx.NextFrame()
	}

	mm.CRT.RGBNoise = 0
	mm.CRT.NoiseX = 0

	mm.LastSelectionChange = time.Now()

	for {
		ictx.Tick()

		switch {
		case ictx.Consume(arcade.BtnConfirm):
			if fn := Options[mm.Selection].Func; fn != nil {
				audio.Confirm.PlaySound(0, 0, 0)

				if err := fn(ctx); err != nil {
					return err
				}

				internal.SetTitle("Termacade")
				audio.TermiteLoop.PlayMusic(0, false)
			} else {
				audio.Buzzer.PlaySound(0, 0, 0)
			}
		case ictx.ConsumeAllowRepeat(arcade.BtnUp, 30):
			mm.moveSelection(-1)
		case ictx.ConsumeAllowRepeat(arcade.BtnDown, 30):
			mm.moveSelection(1)
		}

		mm.CRT.Draw(mm.render)

		gfx.NextFrame()
	}
}

func (mm *MainMenu) moveSelection(direction int) {
	mm.Selection += direction

	audio.Confirm1.PlaySound(0, 0, 0)

	if mm.Selection < 0 {
		mm.Selection = len(Options) - 1
	}

	if mm.Selection >= len(Options) {
		mm.Selection = 0
	}

	settings := internal.LoadSettings()
	settings.LastTermacadeOption = mm.Selection
	internal.SaveSettings(settings)

	mm.LastSelectionChange = time.Now()
}
