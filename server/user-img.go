package server

import (
	"bytes"
	"crypto/sha256"
	"database/sql"
	"encoding/json"
	"image/png"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

func uploadUserImg(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPut)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPut {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPut {
		w.Header().Add("Allow", "PUT, OPTIONS")
		http.Error(w, "only PUT is allowed", http.StatusMethodNotAllowed)

		return
	}

	defer r.Body.Close()

	// 128 KiB max file size
	// (128x128 32-bit pixels without metadata is half of that)
	const maxSize = 128 << 10

	var buf bytes.Buffer

	_, err := io.CopyN(&buf, r.Body, maxSize+1)
	if err == nil {
		log.Println("user png too big")
		http.Error(w, "file too large", http.StatusRequestEntityTooLarge)

		return
	}

	if !xerrors.Is(err, io.EOF) {
		log.Println("upload err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)

		return
	}

	config, err := png.DecodeConfig(bytes.NewReader(buf.Bytes()))
	if err != nil {
		log.Println("failed to decode user png", strconv.Quote(r.Header.Get("Content-Type")), err)
		http.Error(w, "invalid PNG", http.StatusBadRequest)

		return
	}

	if config.Height > 512 || config.Width > 512 {
		log.Println("user png dims", config.Width, "x", config.Height)
		// for safety (file size is already checked)
		http.Error(w, "PNG dimensions too large", http.StatusBadRequest)

		return
	}

	hash := sha256.Sum256(buf.Bytes())

	var fileID format.FileID

	copy(fileID.Hash[:], hash[:])

	if err = getUserImageByHash.QueryRow(hash[:]).Scan(&fileID.ID); err != nil {
		err = insertUserImage.QueryRow(r.Header.Get("X-Forwarded-For"), hash[:], buf.Bytes()).Scan(&fileID.ID)
		if err != nil {
			log.Println("upload store err", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)

			return
		}
	}

	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(http.StatusCreated)

	b, _ := fileID.MarshalText()
	_, _ = w.Write(b)
}

func serveUserImg(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	if !strings.HasSuffix(r.URL.Path, ".png") {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	fileIDEnc := r.URL.Path[len("/spy-cards/user-img/"):]
	fileIDEnc = fileIDEnc[:len(fileIDEnc)-len(".png")]

	var fileID format.FileID

	err := fileID.UnmarshalText([]byte(fileIDEnc))
	if err != nil {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	var fileHash, fileData []byte

	err = getUserImage.QueryRow(fileID.ID).Scan(&fileHash, &fileData)
	if xerrors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=600")
		http.NotFound(w, r)

		return
	}

	if err != nil {
		log.Println("error getting", r.URL.Path, err)
		w.Header().Set("Cache-Control", "no-store")
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	if !bytes.Equal(fileID.Hash[:], fileHash[:8]) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", "image/png")
	w.Header().Set("Content-Length", strconv.Itoa(len(fileData)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(fileData)
	}
}

func getUserImgListScript(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	rows, err := getCommunityPortraits.Query()
	if err != nil {
		log.Println("whitelist get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var script bytes.Buffer

	_, _ = script.WriteString("user_images_whitelist.splice(0, user_images_whitelist.length")

	for rows.Next() {
		var (
			id   format.FileID
			hash []byte
		)

		err = rows.Scan(&hash, &id.ID)
		if err != nil {
			log.Println("whitelist get", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(id.Hash[:], hash)

		_, _ = script.WriteString(", \"")
		b, _ := id.MarshalText()
		_, _ = script.Write(b)
		_ = script.WriteByte('"')
	}

	_, _ = script.WriteString(");\n")

	if err := rows.Err(); err != nil {
		log.Println("whitelist get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	w.Header().Set("Cache-Control", "public, max-age=60, stale-while-revalidate=600")
	w.Header().Set("Content-Type", "application/javascript")
	w.Header().Set("Content-Length", strconv.Itoa(script.Len()))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = script.WriteTo(w)
	}
}

func getUserImgCommunityList(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	rows, err := getCommunityPortraits.Query()
	if err != nil {
		log.Println("community portrait list get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var script bytes.Buffer

	_, _ = script.WriteString("[")

	first := true

	for rows.Next() {
		var (
			id   format.FileID
			hash []byte
		)

		err = rows.Scan(&hash, &id.ID)
		if err != nil {
			log.Println("community portrait list get", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(id.Hash[:], hash)

		if first {
			first = false
		} else {
			_, _ = script.WriteString(", ")
		}

		_ = script.WriteByte('"')
		b, _ := id.MarshalText()
		_, _ = script.Write(b)
		_ = script.WriteByte('"')
	}

	_, _ = script.WriteString("]\n")

	if err := rows.Err(); err != nil {
		log.Println("community portrait list get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=86400")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(script.Len()))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = script.WriteTo(w)
	}
}

func getUserImgTanjerinList(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Access-Control-Allow-Origin", "*")

	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	if !validateOrigin(w, r) {
		return
	}

	rows, err := getTanjerinUserImages.Query()
	if err != nil {
		log.Println("tanjerin get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}
	defer rows.Close()

	var ids []string

	for rows.Next() {
		var (
			id       format.FileID
			hash     []byte
			tanjerin bool
		)

		err = rows.Scan(&hash, &id.ID, &tanjerin)
		if err != nil {
			log.Println("tanjerin get", err)
			http.Error(w, "database error", http.StatusInternalServerError)

			return
		}

		copy(id.Hash[:], hash)

		b, _ := id.MarshalText()
		formatted := string(b)

		if !tanjerin {
			formatted = "!" + formatted
		}

		ids = append(ids, formatted)
	}

	if err := rows.Err(); err != nil {
		log.Println("tanjerin get", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	b, err := json.Marshal(ids)
	if err != nil {
		panic(err)
	}

	w.Header().Set("Cache-Control", "public, max-age=3600, stale-while-revalidate=86400")
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Content-Length", strconv.Itoa(len(b)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(b)
	}
}
