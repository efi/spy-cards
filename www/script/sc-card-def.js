"use strict";
var SpyCards;
(function (SpyCards) {
    let Rank;
    (function (Rank) {
        Rank[Rank["Attacker"] = 0] = "Attacker";
        Rank[Rank["Effect"] = 1] = "Effect";
        Rank[Rank["MiniBoss"] = 2] = "MiniBoss";
        Rank[Rank["Boss"] = 3] = "Boss";
        Rank[Rank["Enemy"] = 4] = "Enemy";
        Rank[Rank["_reserved5"] = 5] = "_reserved5";
        Rank[Rank["_reserved6"] = 6] = "_reserved6";
        Rank[Rank["None"] = 7] = "None";
    })(Rank = SpyCards.Rank || (SpyCards.Rank = {}));
    function rankName(rank) {
        switch (rank) {
            case Rank.MiniBoss:
                return "Mini-Boss";
            default:
                return Rank[rank];
        }
    }
    SpyCards.rankName = rankName;
    let Tribe;
    (function (Tribe) {
        Tribe[Tribe["Seedling"] = 0] = "Seedling";
        Tribe[Tribe["Wasp"] = 1] = "Wasp";
        Tribe[Tribe["Fungi"] = 2] = "Fungi";
        Tribe[Tribe["Zombie"] = 3] = "Zombie";
        Tribe[Tribe["Plant"] = 4] = "Plant";
        Tribe[Tribe["Bug"] = 5] = "Bug";
        Tribe[Tribe["Bot"] = 6] = "Bot";
        Tribe[Tribe["Thug"] = 7] = "Thug";
        Tribe[Tribe["Unknown"] = 8] = "Unknown";
        Tribe[Tribe["Chomper"] = 9] = "Chomper";
        Tribe[Tribe["Leafbug"] = 10] = "Leafbug";
        Tribe[Tribe["DeadLander"] = 11] = "DeadLander";
        Tribe[Tribe["Mothfly"] = 12] = "Mothfly";
        Tribe[Tribe["Spider"] = 13] = "Spider";
        Tribe[Tribe["Custom"] = 14] = "Custom";
        Tribe[Tribe["None"] = 15] = "None";
    })(Tribe = SpyCards.Tribe || (SpyCards.Tribe = {}));
    function tribeName(tribe, customName) {
        switch (tribe) {
            case Tribe.Unknown:
                return "???";
            case Tribe.DeadLander:
                return "Dead Lander";
            case Tribe.Custom:
                return customName;
            default:
                return Tribe[tribe];
        }
    }
    SpyCards.tribeName = tribeName;
    class CustomTribeData {
        constructor() {
            this.rgb = 0x808080;
            this.name = "";
        }
        marshal(buf) {
            buf.push((this.rgb >> 16) & 255);
            buf.push((this.rgb >> 8) & 255);
            buf.push(this.rgb & 255);
            SpyCards.Binary.pushUTF8StringVar(buf, this.name);
        }
        unmarshal(buf) {
            const r = buf.shift();
            const g = buf.shift();
            const b = buf.shift();
            this.rgb = (r << 16) | (g << 8) | b;
            this.name = SpyCards.Binary.shiftUTF8StringVar(buf);
        }
        unmarshalV1(buf) {
            const r = buf.shift();
            const g = buf.shift();
            const b = buf.shift();
            this.rgb = (r << 16) | (g << 8) | b;
            this.name = SpyCards.Binary.shiftUTF8String1(buf);
        }
    }
    SpyCards.CustomTribeData = CustomTribeData;
    let SpecialField;
    (function (SpecialField) {
        SpecialField[SpecialField["TP"] = 0] = "TP";
        SpecialField[SpecialField["ExtraTribe"] = 1] = "ExtraTribe";
    })(SpecialField || (SpecialField = {}));
    class CardDef {
        constructor() {
            this.name = "";
            this.tp = 1;
            this.portrait = 0x80;
            this.tribes = [{ tribe: Tribe.Unknown, custom: null }];
            this.effects = [];
        }
        rank() {
            if (this.id < 128) {
                const cardName = SpyCards.CardData.GlobalCardID[this.id];
                if (cardName in SpyCards.CardData.AttackerCardOrder) {
                    return Rank.Attacker;
                }
                if (cardName in SpyCards.CardData.EffectCardOrder) {
                    return Rank.Effect;
                }
                if (cardName in SpyCards.CardData.MiniBossCardOrder) {
                    return Rank.MiniBoss;
                }
                if (cardName in SpyCards.CardData.BossCardOrder) {
                    return Rank.Boss;
                }
                throw new Error("unknown card ID " + this.id + " (" + cardName + ")");
            }
            return (this.id >> 5) & 3;
        }
        getBackID() {
            switch (this.rank()) {
                case Rank.Boss:
                    return 2;
                case Rank.MiniBoss:
                    return 1;
                default:
                    return 0;
            }
        }
        originalName() {
            if (this.id >= 128) {
                const index = ((((this.id - 128) & ~127) >> 2) | (this.id & 31)) + 1;
                const rank = this.rank();
                if (rank === Rank.MiniBoss) {
                    return "Custom Mini-Boss #" + index;
                }
                return "Custom " + Rank[rank] + " #" + index;
            }
            return SpyCards.CardData.CardNameOverride[this.id] || SpyCards.CardData.GlobalCardID[this.id] || ("MissingCard?" + this.id + "?");
        }
        displayName() {
            return this.name || this.originalName();
        }
        effectiveTP() {
            let tp = this.tp;
            for (let effect of this.effects) {
                if (effect.type === EffectType.TP) {
                    tp -= effect.negate ? -effect.amount : effect.amount;
                }
            }
            return tp;
        }
        tribeName(n) {
            return this.tribes[n] ? tribeName(this.tribes[n].tribe, this.tribes[n].custom && this.tribes[n].custom.name) : "None";
        }
        marshal(buf) {
            SpyCards.Binary.pushUVarInt(buf, 4); // format version
            SpyCards.Binary.pushUVarInt(buf, this.id);
            if (this.tribes.length === 0) {
                buf.push((Tribe.Unknown << 4) | Tribe.None);
            }
            else if (this.tribes.length === 1) {
                buf.push((this.tribes[0].tribe << 4) | Tribe.None);
            }
            else {
                buf.push((this.tribes[0].tribe << 4) | this.tribes[1].tribe);
            }
            for (let i = 2; i < this.tribes.length; i++) {
                buf.push((SpecialField.ExtraTribe << 4) | this.tribes[i].tribe);
                if (this.tribes[i].tribe === Tribe.Custom) {
                    this.tribes[i].custom.marshal(buf);
                }
            }
            let overflowTP = null;
            if (this.tp === Infinity) {
                buf.push(15);
            }
            else if (this.tp < 0) {
                overflowTP = this.tp;
                buf.push(0);
            }
            else if (this.tp > 10) {
                overflowTP = this.tp - 10;
                buf.push(10);
            }
            else {
                if ((this.tp === 0 || this.tp === 10) && this.effects.length && this.effects[0].type === EffectType.TP) {
                    overflowTP = 0;
                }
                buf.push(this.tp);
            }
            buf.push(this.portrait);
            SpyCards.Binary.pushUTF8StringVar(buf, this.name === this.originalName() ? "" : this.name);
            const filteredEffects = this.effects.filter((e) => e.isValid());
            if (overflowTP !== null) {
                const tpEffect = new EffectDef();
                tpEffect.type = EffectType.TP;
                tpEffect.negate = overflowTP > 0;
                tpEffect.amount = Math.abs(overflowTP);
                filteredEffects.unshift(tpEffect);
            }
            SpyCards.Binary.pushUVarInt(buf, filteredEffects.length);
            for (let effect of filteredEffects) {
                effect.marshal(buf);
            }
            if (this.tribes.length >= 1 && this.tribes[0].tribe === Tribe.Custom) {
                this.tribes[0].custom.marshal(buf);
            }
            if (this.tribes.length >= 2 && this.tribes[1].tribe === Tribe.Custom) {
                this.tribes[1].custom.marshal(buf);
            }
            if (this.portrait === 254 || this.portrait === 255) {
                SpyCards.Binary.pushUVarInt(buf, this.customPortrait.length);
                for (let i = 0; i < this.customPortrait.length; i += 128) {
                    const sub = this.customPortrait.subarray(i);
                    buf.push(...SpyCards.toArray(sub.length > 128 ? sub.subarray(0, 128) : sub));
                }
            }
        }
        unmarshal(buf) {
            const formatVersion = SpyCards.Binary.shiftUVarInt(buf);
            if (formatVersion === 0 || formatVersion === 1) {
                this.unmarshalV1(buf, formatVersion);
                return;
            }
            if (formatVersion !== 2 && formatVersion !== 4) {
                throw new Error("unexpected format version for card definition: " + formatVersion);
            }
            this.id = SpyCards.Binary.shiftUVarInt(buf);
            const tribes = buf.shift();
            this.tribes = [
                {
                    tribe: tribes >> 4,
                    custom: null
                }
            ];
            if ((tribes & 15) !== Tribe.None) {
                this.tribes.push({
                    tribe: tribes & 15,
                    custom: null
                });
            }
            let special = buf.shift();
            while (special >> 4 !== SpecialField.TP) {
                switch (special >> 4) {
                    case SpecialField.ExtraTribe:
                        if (this.tribes.length < 2) {
                            throw new Error("special field ExtraTribe cannot appear on a card with only 1 existing tribe");
                        }
                        if ((special & 15) === Tribe.None) {
                            throw new Error("special field ExtraTribe cannot have tribe None");
                        }
                        const t = {
                            tribe: special & 15,
                            custom: null
                        };
                        if (t.tribe === Tribe.Custom) {
                            t.custom = new CustomTribeData();
                            t.custom.unmarshal(buf);
                        }
                        this.tribes.push(t);
                        break;
                    default:
                        throw new Error("unknown special field: " + (special >> 4));
                }
                special = buf.shift();
            }
            if (special <= 10) {
                this.tp = special;
            }
            else if (special === 15) {
                this.tp = Infinity;
            }
            else {
                throw new Error("invalid value for TP byte: " + special);
            }
            this.portrait = buf.shift();
            if (this.portrait > 234 && this.portrait < 254) {
                throw new Error("invalid value for portrait byte: " + this.portrait);
            }
            this.name = SpyCards.Binary.shiftUTF8StringVar(buf);
            const effectCount = SpyCards.Binary.shiftUVarInt(buf);
            this.effects = [];
            for (let i = 0; i < effectCount; i++) {
                const effect = new EffectDef();
                effect.unmarshal(buf, formatVersion);
                this.effects.push(effect);
            }
            if ((this.tp === 0 || this.tp === 10) && effectCount && this.effects[0].type === EffectType.TP) {
                const tpEffect = this.effects.shift();
                this.tp -= tpEffect.negate ? -tpEffect.amount : tpEffect.amount;
            }
            if (this.tribes.length >= 1 && this.tribes[0].tribe === Tribe.Custom) {
                this.tribes[0].custom = new CustomTribeData();
                this.tribes[0].custom.unmarshal(buf);
            }
            if (this.tribes.length >= 2 && this.tribes[1].tribe === Tribe.Custom) {
                this.tribes[1].custom = new CustomTribeData();
                this.tribes[1].custom.unmarshal(buf);
            }
            if (this.portrait === 254 || this.portrait === 255) {
                const customPortraitLength = SpyCards.Binary.shiftUVarInt(buf);
                if (customPortraitLength > buf.length) {
                    throw new Error("custom portrait length longer than remaining data");
                }
                this.customPortrait = new Uint8Array(buf.splice(0, customPortraitLength));
            }
            else {
                this.customPortrait = null;
            }
            if (buf.length) {
                throw new Error("extra data after end of card code");
            }
        }
        unmarshalV1(buf, formatVersion) {
            this.id = buf.shift();
            const tribes = buf.shift();
            this.tribes = [
                {
                    tribe: tribes >> 4,
                    custom: null
                }
            ];
            if ((tribes & 15) !== Tribe.None) {
                this.tribes.push({
                    tribe: tribes & 15,
                    custom: null
                });
            }
            let customPortraitID = 254;
            const tp = buf.shift();
            if (tp & 0x40) {
                customPortraitID = 255;
            }
            const codeRank = (tp >> 4) & 3;
            const actualRank = this.rank();
            if (codeRank !== actualRank) {
                throw new Error("expected rank " + Rank[actualRank] + " but card code specifies " + Rank[codeRank]);
            }
            if ((tp & 15) <= 10) {
                this.tp = tp & 15;
            }
            else if ((tp & 15) === 15) {
                this.tp = Infinity;
            }
            else {
                throw new Error("invalid value for TP byte: " + tp);
            }
            this.portrait = buf.shift();
            if (this.portrait > 234 && this.portrait !== 255) {
                throw new Error("invalid value for portrait byte: " + this.portrait);
            }
            if (this.portrait === 255) {
                this.portrait = customPortraitID;
            }
            else if (customPortraitID === 255) {
                throw new Error("external portrait bit is set but portrait is not custom");
            }
            this.name = SpyCards.Binary.shiftUTF8String1(buf);
            if (formatVersion === 0 && this.rank() === Rank.Attacker && (buf.length === 0 || buf[0] === 137)) {
                this.effects = [new EffectDef()];
                this.effects[0].type = EffectType.Stat;
                this.effects[0].amount = this.tp;
            }
            else {
                const effectCount = buf.shift();
                this.effects = [];
                for (let i = 0; i < effectCount; i++) {
                    const effect = new EffectDef();
                    effect.unmarshal(buf, formatVersion);
                    this.effects.push(effect);
                }
            }
            if (this.tribes.length >= 1 && this.tribes[0].tribe === Tribe.Custom) {
                this.tribes[0].custom = new CustomTribeData();
                this.tribes[0].custom.unmarshalV1(buf);
            }
            if (this.tribes.length >= 2 && this.tribes[1].tribe === Tribe.Custom) {
                this.tribes[1].custom = new CustomTribeData();
                this.tribes[1].custom.unmarshalV1(buf);
            }
            if (this.portrait === 254 || this.portrait === 255) {
                this.customPortrait = new Uint8Array(buf.splice(0, buf.length));
            }
            else {
                this.customPortrait = null;
            }
            if (buf.length) {
                throw new Error("extra data after end of card code");
            }
        }
        createEl(defs) {
            return SpyCards.createCardEl(defs, this);
        }
    }
    SpyCards.CardDef = CardDef;
    let EffectType;
    (function (EffectType) {
        EffectType[EffectType["FlavorText"] = 0] = "FlavorText";
        EffectType[EffectType["Stat"] = 1] = "Stat";
        EffectType[EffectType["Empower"] = 2] = "Empower";
        EffectType[EffectType["Summon"] = 3] = "Summon";
        EffectType[EffectType["Heal"] = 4] = "Heal";
        EffectType[EffectType["TP"] = 5] = "TP";
        EffectType[EffectType["Numb"] = 6] = "Numb";
        EffectType[EffectType["CondCard"] = 128] = "CondCard";
        EffectType[EffectType["CondLimit"] = 129] = "CondLimit";
        EffectType[EffectType["CondWinner"] = 130] = "CondWinner";
        EffectType[EffectType["CondApply"] = 131] = "CondApply";
        EffectType[EffectType["CondCoin"] = 132] = "CondCoin";
        EffectType[EffectType["CondHP"] = 133] = "CondHP";
        EffectType[EffectType["CondStat"] = 134] = "CondStat";
        EffectType[EffectType["CondPriority"] = 135] = "CondPriority";
        EffectType[EffectType["CondOnNumb"] = 136] = "CondOnNumb";
    })(EffectType = SpyCards.EffectType || (SpyCards.EffectType = {}));
    class EffectDef {
        constructor() {
            this.type = null;
            this.text = "";
            this.amount = 1;
            this.rank = Rank.None;
            this.tribe = Tribe.None;
        }
        marshal(buf) {
            buf.push(this.type);
            let flags = 0;
            flags |= this.negate ? 1 << 0 : 0;
            flags |= this.opponent ? 1 << 1 : 0;
            flags |= this.each ? 1 << 2 : 0;
            flags |= this.late ? 1 << 3 : 0;
            flags |= this.generic ? 1 << 4 : 0;
            flags |= this.defense ? 1 << 5 : 0;
            flags |= this._reserved6 ? 1 << 6 : 0;
            flags |= this._reserved7 ? 1 << 7 : 0;
            buf.push(flags);
            switch (this.type) {
                case EffectType.FlavorText:
                    SpyCards.Binary.pushUTF8StringVar(buf, this.text);
                    break;
                case EffectType.Stat:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    break;
                case EffectType.Empower:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    this.marshalFilter(buf);
                    break;
                case EffectType.Summon:
                    buf.push(this.amount - 1);
                    this.marshalFilter(buf);
                    break;
                case EffectType.Heal:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    break;
                case EffectType.TP:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    break;
                case EffectType.Numb:
                    buf.push(isFinite(this.amount) ? this.amount : 255);
                    this.marshalFilter(buf);
                    break;
                case EffectType.CondCard:
                    if (!this.each) {
                        buf.push(this.amount - 1);
                    }
                    this.marshalFilter(buf);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondLimit:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondWinner:
                    this.result.marshal(buf);
                    break;
                case EffectType.CondApply:
                    this.result.marshal(buf);
                    break;
                case EffectType.CondCoin:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    if (this.generic) {
                        this.tailsResult.marshal(buf);
                    }
                    break;
                case EffectType.CondHP:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondStat:
                    buf.push(this.amount - 1);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondPriority:
                    buf.push(this.amount);
                    this.result.marshal(buf);
                    break;
                case EffectType.CondOnNumb:
                    this.result.marshal(buf);
                    break;
                default:
                    throw new Error("unhandled effect type: " + this.type + " (" + EffectType[this.type] + ")");
            }
        }
        marshalFilter(buf) {
            if (this.generic) {
                buf.push((this.rank << 4) | this.tribe);
                if (this.tribe === Tribe.Custom) {
                    SpyCards.Binary.pushUTF8StringVar(buf, this.customTribe);
                }
            }
            else {
                SpyCards.Binary.pushUVarInt(buf, this.card);
            }
        }
        unmarshal(buf, formatVersion) {
            if (formatVersion === 0 || formatVersion === 1) {
                this.unmarshalV1(buf, formatVersion);
                return;
            }
            this.type = buf.shift();
            const flags = buf.shift();
            this.negate = !!((flags >> 0) & 1);
            this.opponent = !!((flags >> 1) & 1);
            this.each = !!((flags >> 2) & 1);
            this.late = !!((flags >> 3) & 1);
            this.generic = !!((flags >> 4) & 1);
            this.defense = !!((flags >> 5) & 1);
            this._reserved6 = !!((flags >> 6) & 1);
            this._reserved7 = !!((flags >> 7) & 1);
            this.text = "";
            this.amount = 1;
            this.card = null;
            this.rank = Rank.None;
            this.tribe = Tribe.None;
            this.customTribe = null;
            this.result = null;
            this.tailsResult = null;
            function shiftAmount(buf) {
                const amount = buf.shift();
                return amount === 255 ? Infinity : amount;
            }
            function shiftCount(buf) {
                const count = buf.shift();
                return count + 1;
            }
            switch (this.type) {
                case EffectType.FlavorText:
                    this.text = SpyCards.Binary.shiftUTF8StringVar(buf);
                    break;
                case EffectType.Stat:
                    this.amount = shiftAmount(buf);
                    break;
                case EffectType.Empower:
                    this.amount = shiftAmount(buf);
                    this.unmarshalFilter(buf);
                    break;
                case EffectType.Summon:
                    this.amount = shiftCount(buf);
                    this.unmarshalFilter(buf);
                    break;
                case EffectType.Heal:
                    this.amount = shiftAmount(buf);
                    break;
                case EffectType.TP:
                    this.amount = shiftAmount(buf);
                    break;
                case EffectType.Numb:
                    this.amount = shiftAmount(buf);
                    if (formatVersion >= 4) {
                        this.unmarshalFilter(buf);
                    }
                    else {
                        this.opponent = true;
                        this.generic = true;
                        this.rank = Rank.Attacker;
                        this.tribe = Tribe.None;
                    }
                    break;
                case EffectType.CondCard:
                    if (!this.each) {
                        this.amount = shiftCount(buf);
                    }
                    this.unmarshalFilter(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondLimit:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondWinner:
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondApply:
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondCoin:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    if (this.generic) {
                        this.tailsResult = new EffectDef();
                        this.tailsResult.unmarshal(buf, formatVersion);
                    }
                    break;
                case EffectType.CondHP:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondStat:
                    this.amount = shiftCount(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondPriority:
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case EffectType.CondOnNumb:
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                default:
                    throw new Error("unhandled effect type: " + this.type + " (" + EffectType[this.type] + ")");
            }
        }
        unmarshalFilter(buf) {
            if (this.generic) {
                const rankTribe = buf.shift();
                this.rank = rankTribe >> 4;
                this.tribe = rankTribe & 15;
                if (this.tribe === Tribe.Custom) {
                    this.customTribe = SpyCards.Binary.shiftUTF8StringVar(buf);
                }
            }
            else {
                this.card = SpyCards.Binary.shiftUVarInt(buf);
            }
        }
        unmarshalV1(buf, formatVersion) {
            this.negate = false;
            this.opponent = false;
            this.each = false;
            this.late = false;
            this.generic = false;
            this.defense = false;
            this._reserved6 = false;
            this._reserved7 = false;
            this.text = "";
            this.amount = 1;
            this.card = null;
            this.rank = Rank.None;
            this.tribe = Tribe.None;
            this.customTribe = null;
            this.result = null;
            this.tailsResult = null;
            function shiftAmount(buf) {
                const n = buf.shift();
                if (n === 0) {
                    return [Infinity, false];
                }
                if (n >= 128) {
                    return [256 - n, true];
                }
                return [n, false];
            }
            function shiftTribe(buf) {
                const n = buf.shift();
                if ((n & 15) !== Tribe.Custom) {
                    return [n, null];
                }
                return [n, SpyCards.Binary.shiftUTF8String1(buf)];
            }
            const legacyType = buf.shift();
            switch (legacyType) {
                case 0: // ATK (static)
                    this.type = EffectType.Stat;
                    this.defense = false;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 1: // DEF (static)
                    this.type = EffectType.Stat;
                    this.defense = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 2: // ATK (static, once per turn)
                    this.type = EffectType.CondLimit;
                    this.negate = false;
                    this.amount = 1;
                    this.result = new EffectDef();
                    this.result.type = EffectType.Stat;
                    [this.result.amount, this.result.negate] = shiftAmount(buf);
                    break;
                case 3: // ATK+n
                    this.type = EffectType.Stat;
                    this.defense = false;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 4: // DEF+n
                    this.type = EffectType.Stat;
                    this.defense = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 5: // Empower (card)
                    this.type = EffectType.Empower;
                    this.generic = false;
                    this.defense = false;
                    this.card = buf.shift();
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 6: // Empower (tribe)
                    this.type = EffectType.Empower;
                    this.generic = true;
                    this.defense = false;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 7: // Heal
                    this.type = EffectType.Heal;
                    this.opponent = false;
                    this.each = false;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 8: // Lifesteal
                    this.type = EffectType.CondWinner;
                    this.negate = false;
                    this.opponent = false;
                    this.result = new EffectDef();
                    this.result.type = EffectType.Heal;
                    [this.result.amount, this.result.negate] = shiftAmount(buf);
                    break;
                case 9: // Numb
                    this.type = EffectType.Numb;
                    [this.amount] = shiftAmount(buf);
                    this.opponent = true;
                    this.generic = true;
                    this.rank = Rank.Attacker;
                    this.tribe = Tribe.None;
                    break;
                case 10: // Pierce
                    this.type = EffectType.Stat;
                    this.defense = true;
                    this.opponent = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    this.negate = !this.negate;
                    break;
                case 11: // Summon
                    this.type = EffectType.Summon;
                    this.negate = false;
                    this.opponent = false;
                    this.generic = false;
                    this.amount = 1;
                    this.card = buf.shift();
                    break;
                case 12: // Summon Rank
                    this.type = EffectType.Summon;
                    this.negate = true;
                    this.opponent = false;
                    this.generic = true;
                    this.amount = 1;
                    if (formatVersion === 0) {
                        this.rank = Rank.MiniBoss;
                        this.tribe = Tribe.None;
                    }
                    else {
                        this.rank = buf.shift();
                        this.tribe = Tribe.None;
                    }
                    break;
                case 13: // Summon Tribe
                    this.type = EffectType.Summon;
                    this.negate = false;
                    this.opponent = false;
                    this.generic = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    if (this.tribe >> 4 === 0) {
                        this.rank = Rank.Enemy;
                    }
                    else {
                        this.rank = (this.tribe >> 4) - 1;
                        this.tribe &= 15;
                    }
                    this.amount = buf.shift();
                    break;
                case 14: // Unity
                    this.type = EffectType.CondLimit;
                    this.negate = false;
                    this.amount = 1;
                    this.result = new EffectDef();
                    this.result.type = EffectType.Empower;
                    this.result.generic = true;
                    [this.result.tribe, this.result.customTribe] = shiftTribe(buf);
                    [this.result.amount, this.result.negate] = shiftAmount(buf);
                    break;
                case 15: // TP
                    this.type = EffectType.TP;
                    [this.amount, this.negate] = shiftAmount(buf);
                    break;
                case 16: // Summon as Opponent
                    this.type = EffectType.Summon;
                    this.negate = false;
                    this.opponent = true;
                    this.generic = false;
                    this.amount = 1;
                    this.card = buf.shift();
                    break;
                case 17: // Multiply Healing
                    this.type = EffectType.Heal;
                    this.opponent = false;
                    this.each = true;
                    this.generic = true;
                    [this.amount, this.negate] = shiftAmount(buf);
                    if (this.negate) {
                        this.amount--;
                        if (this.amount === 0) {
                            this.negate = false;
                        }
                    }
                    else {
                        this.amount++;
                    }
                    break;
                case 18: // Flavor Text
                    this.type = EffectType.FlavorText;
                    this.negate = !!(buf.shift() & 1);
                    this.text = SpyCards.Binary.shiftUTF8String1(buf);
                    break;
                case 128: // Coin
                    this.type = EffectType.CondCoin;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    const count = buf.shift();
                    this.generic = !!(count & 0x80);
                    this.amount = (count & 0x7f) + 1;
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    if (this.generic) {
                        this.tailsResult = new EffectDef();
                        this.tailsResult.unmarshal(buf, formatVersion);
                    }
                    this.defense = this.generic &&
                        this.result.type === EffectType.Stat &&
                        !this.result.defense &&
                        this.tailsResult.type === EffectType.Stat &&
                        this.tailsResult.defense;
                    break;
                case 129: // If Card
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.card = buf.shift();
                    if (formatVersion === 1) {
                        this.amount = buf.shift();
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 130: // Per Card
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.each = true;
                    this.card = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 131: // If Tribe
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 132: // VS Tribe
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.opponent = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    if (formatVersion === 1) {
                        this.amount = buf.shift();
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 133: // If Stat
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 134: // Setup
                    this.type = EffectType.CondApply;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.late = true;
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 135: // Limit
                    this.type = EffectType.CondLimit;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 136: // VS Card
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.card = buf.shift();
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 137: // If Rank
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.rank = buf.shift();
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 138: // VS Rank
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.opponent = true;
                    this.rank = buf.shift();
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 139: // If Win
                    this.type = EffectType.CondWinner;
                    if (formatVersion === 1) {
                        this.opponent = !!(buf.shift() & 1);
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 140: // If Tie
                    this.type = EffectType.CondWinner;
                    if (formatVersion === 1) {
                        this.opponent = !!(buf.shift() & 1);
                    }
                    this.negate = true;
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 141: // If Stat (late)
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.late = true;
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 142: // VS Stat
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 143: // VS Stat (late)
                    this.type = EffectType.CondStat;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.late = true;
                    this.amount = buf.shift();
                    if (this.amount & 0x80) {
                        this.defense = true;
                        this.amount &= 0x7f;
                    }
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 144: // If HP
                    this.type = EffectType.CondHP;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 145: // Per Tribe
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.each = true;
                    [this.tribe, this.customTribe] = shiftTribe(buf);
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 146: // Per Rank
                    this.type = EffectType.CondCard;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.generic = true;
                    this.each = true;
                    this.rank = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                case 147: // VS HP
                    this.type = EffectType.CondHP;
                    if (formatVersion === 1) {
                        this.negate = !!(buf.shift() & 1);
                    }
                    this.opponent = true;
                    this.amount = buf.shift();
                    this.result = new EffectDef();
                    this.result.unmarshal(buf, formatVersion);
                    break;
                default:
                    throw new Error("unexpected effect type ID " + legacyType);
            }
        }
        isValid() {
            if (this.type === null) {
                return false;
            }
            if (this.type >= 128 && (!this.result || !this.result.isValid())) {
                return false;
            }
            if (this.type === EffectType.CondCoin && this.generic && (!this.tailsResult || !this.tailsResult.isValid())) {
                return false;
            }
            return true;
        }
        createEl(defs, card) {
            return SpyCards.createEffectEl(defs, card, this);
        }
    }
    SpyCards.EffectDef = EffectDef;
})(SpyCards || (SpyCards = {}));
