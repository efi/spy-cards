// +build js,wasm
// +build !headless

package internal

import "syscall/js"

var clipboardAPI = js.Global().Get("navigator").Get("clipboard")

// ReadClipboard returns the text in the system clipboard.
func ReadClipboard() (string, error) {
	v, err := Await(clipboardAPI.Call("readText"))
	if err != nil {
		return "", err
	}

	return v.String(), nil
}

// WriteClipboard puts text into the system clipboard.
func WriteClipboard(s string) error {
	_, err := Await(clipboardAPI.Call("writeText", s))

	return err
}
