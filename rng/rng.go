// Package rng implements a deterministic random number generator
// using cryptographic functions.
package rng

import (
	"crypto/rand"
	"hash"
	"math"

	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

// RNG is a deterministic random number generator.
type RNG struct {
	// Count is the number of bytes RNG has been advanced by.
	//
	// This number is not read by any RNG calculations, and is provided
	// for debugging only.
	Count uint64

	seed  []byte
	buf   []byte
	hash  hash.Hash
	index int

	// Flags to maintain compatibility with bugs in previous versions of RNG.
	SeparateUpdateSeed []byte
	ReverseFloat       bool
	MaxValueFencepost  bool
	MaxValueShift      bool
}

// ForceSeed is a user-specified seed to use.
var ForceSeed string

// RandomSeed returns a random 16-character string with 80 bits of entropy.
func RandomSeed() string {
	var b [10]byte

	_, err := rand.Read(b[:])
	if err != nil {
		panic(xerrors.Errorf("rng: cannot generate random seed: %w", err))
	}

	return format.Encode32(b[:])
}

// New constructs an RNG with a given hash function and seed.
func New(hash hash.Hash, seed string) *RNG {
	seedCopy := []byte(seed)

	hash.Reset()
	_, _ = hash.Write(seedCopy)

	return &RNG{
		Count: 0,

		seed:  seedCopy,
		buf:   hash.Sum(nil),
		index: 0,
		hash:  hash,
	}
}

func (r *RNG) nextMulti(numBytes int) uint64 {
	var val uint64

	for i := 0; i < numBytes; i++ {
		val |= uint64(r.next()) << (i << 3)
	}

	return val
}

func (r *RNG) next() uint8 {
	if r.index >= len(r.buf) {
		r.hash.Reset()

		if r.SeparateUpdateSeed != nil {
			_, _ = r.hash.Write(r.SeparateUpdateSeed)
			_, _ = r.hash.Write(r.buf)
		} else {
			_, _ = r.hash.Write(r.buf)
			_, _ = r.hash.Write(r.seed)
		}

		r.buf = r.hash.Sum(r.buf[:0])
		r.index = 0
	}

	n := r.buf[r.index]
	r.index++
	r.Count++

	return n
}

// Clone returns a full copy of this RNG.
//
// It is not safe to use both RNG instances concurrently,
// but changes to one will not be reflected in the other.
func (r *RNG) Clone() *RNG {
	return &RNG{
		Count: r.Count,

		seed:  r.seed,
		buf:   append([]byte(nil), r.buf...),
		index: r.index,
		hash:  r.hash,

		SeparateUpdateSeed: r.SeparateUpdateSeed,
		ReverseFloat:       r.ReverseFloat,
		MaxValueFencepost:  r.MaxValueFencepost,
		MaxValueShift:      r.MaxValueShift,
	}
}

// CheapFloat returns a real number in [0, 1),
// advancing the random state by 1 byte.
func (r *RNG) CheapFloat() float64 {
	return float64(r.next()) / 256.0
}

// Float returns a real number in [0, 1),
// advancing the random state by 4 bytes.
func (r *RNG) Float() float64 {
	if r.ReverseFloat {
		a := float64(r.next())
		b := float64(r.next())
		c := float64(r.next())
		d := float64(r.next())

		return (((a*256+b)*256+c)*256 + d) / 4294967296.0
	}

	var v uint32

	for i := 0; i < 4; i++ {
		v |= uint32(r.next()) << (i << 3)
	}

	return float64(v) / 4294967296.0
}

// RangeInt returns an integer in [min, max),
// advancing the random state by a variable amount.
func (r *RNG) RangeInt(min, max int) int {
	diff := max - min
	if diff <= 0 {
		panic("rng: invalid random range")
	}

	numBytes := 0
	for i := diff; i > 0; i >>= 8 {
		numBytes++
	}

	maxMaxValue := 1 << (numBytes << 3)
	if r.MaxValueShift {
		maxMaxValue = numBytes << 3
	}

	maxValue := diff
	for maxValue+diff <= maxMaxValue {
		maxValue += diff
	}

	value := int(r.nextMulti(numBytes))
	for value >= maxValue && (!r.MaxValueFencepost || value != maxValue) {
		value = int(r.nextMulti(numBytes))
	}

	return value%diff + min
}

// RangeIntFloat returns an integer in [min, max),
// advancing the random state by a variable amount.
//
// This function matches the behavior of RangeInt for integer min and max,
// and matches the behavior of JavaScript for non-integers.
//
// Non-integer min causes the result to have min's fractional part.
//
// A non-integer difference between min and max causes the last number to have
// a lower probability of occurring.
func (r *RNG) RangeIntFloat(min, max float64) float64 {
	diff := max - min
	if diff <= 0 {
		panic("rng: invalid random range")
	}

	numBytes := 0
	for i := diff; i >= 1; i /= 256 {
		numBytes++
	}

	maxValue := diff
	for maxValue+diff <= math.Pow(256, float64(numBytes)) {
		maxValue += diff
	}

	value := float64(r.nextMulti(numBytes))
	for value >= maxValue {
		value = float64(r.nextMulti(numBytes))
	}

	return math.Mod(value, diff) + min
}

// RangeFloat returns a real number in [min, max),
// advancing the random state by 4 bytes.
func (r *RNG) RangeFloat(min, max float64) float64 {
	diff := max - min
	if diff <= 0 {
		panic("rng: invalid random range")
	}

	return diff*r.Float() + min
}
