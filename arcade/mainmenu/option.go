package mainmenu

import (
	"context"
	"runtime"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/flowerjourney"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/arcade/highscores"
	"git.lubar.me/ben/spy-cards/arcade/miteknight"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/rng"
	"golang.org/x/xerrors"
)

// Option is a Termacade main menu option.
type Option struct {
	Hidden bool
	Game   arcade.Game
	Name   string
	Func   func(ctx context.Context) error
	Neon   []*Neon
	Action string
}

func runGame(ctx context.Context, create game.CreateFunc) error {
	seed := rng.ForceSeed
	if seed == "" {
		seed = rng.RandomSeed()
	}

	_, err := game.RunGame(ctx, create, seed)
	if err != nil {
		return xerrors.Errorf("mainmenu: running game: %w", err)
	}

	return nil
}

// Options are the default Termacade main menu options.
var Options = []*Option{
	{
		Game: arcade.FlowerJourney,
		Name: "Flower Journey",
		Func: func(ctx context.Context) error {
			return runGame(ctx, flowerjourney.New(arcade.DefaultFlowerJourneyRules))
		},
		Neon: neonFJ,
	},
	{
		Game: arcade.MiteKnight,
		Name: "Mite Knight",
		Func: func(ctx context.Context) error {
			return runGame(ctx, miteknight.New(arcade.DefaultMiteKnightRules))
		},
		Neon: neonMK,
	},
	{
		Name:   "Fortmite",
		Func:   nil,
		Hidden: time.Now().Month() != time.April || time.Now().Day() != 1,
	},
	{
		Name: "High Scores",
		Func: func(ctx context.Context) error {
			var hs highscores.HighScores

			return hs.Run(ctx)
		},
		Action: "Press " + sprites.Button(arcade.BtnConfirm) + " to view!",
		Neon:   neonHS,
	},
	{
		Name: "Settings",
		Func: func(ctx context.Context) error {
			settingsRedirect()

			return nil
		},
		Action: "Press " + sprites.Button(arcade.BtnConfirm) + " to edit",
		Hidden: runtime.GOOS != "js",
	},
}
