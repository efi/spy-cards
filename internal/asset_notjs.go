// +build !js !wasm

package internal

import (
	"context"
	"io"
	"net/http"
	"strings"

	"golang.org/x/mobile/asset"
	"golang.org/x/xerrors"
)

// OpenAsset opens a local asset or performs an HTTP request.
func OpenAsset(ctx context.Context, name string) (io.ReadCloser, error) {
	if strings.Contains(name, ":") {
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, name, nil)
		if err != nil {
			return nil, xerrors.Errorf("loading asset %q: %w", name, err)
		}

		resp, err := http.DefaultClient.Do(req) //nolint:bodyclose
		if err != nil {
			return nil, xerrors.Errorf("loading asset %q: %w", name, err)
		}

		return resp.Body, nil
	}

	f, err := asset.Open(name)
	if err != nil {
		return nil, xerrors.Errorf("loading asset %q: %w", name, err)
	}

	return f, nil
}
