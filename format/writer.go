// Package format provides readers and writers for various binary formats
// used by Spy Cards Online.
package format

import (
	"bytes"
	"encoding/binary"

	"golang.org/x/xerrors"
)

// Writer encodes data into a binary format.
//
// There should be a deferred call to Catch when Writer is in use.
type Writer struct {
	buf []byte
}

// Data returns the buffer held by the Writer.
//
// The buffer is not copied, so modifications to the buffer may result in
// modifications to the writer.
func (w *Writer) Data() []byte {
	return w.buf
}

// SVarInt appends a signed varint to the Writer.
func (w *Writer) SVarInt(n int64) {
	var buf [binary.MaxVarintLen64]byte
	count := binary.PutVarint(buf[:], n)
	w.buf = append(w.buf, buf[:count]...)
}

// UVarInt appends an unsigned varint to the Writer.
func (w *Writer) UVarInt(n uint64) {
	var buf [binary.MaxVarintLen64]byte
	count := binary.PutUvarint(buf[:], n)
	w.buf = append(w.buf, buf[:count]...)
}

// SubWriter returns a Writer that can be used to append a length-prefixed
// section of data.
//
// done must be called before any methods are called on w.
func (w *Writer) SubWriter() (sub *Writer, done func()) {
	startingLength := len(w.buf)
	sub = &Writer{}
	done = func() {
		if len(w.buf) != startingLength {
			Throw(xerrors.New("format: writer was modified while sub-writer was open"))
		}

		w.UVarInt(uint64(len(sub.buf)))
		w.Bytes(sub.buf)
	}

	return
}

// Bytes appends raw bytes to the Writer.
func (w *Writer) Bytes(b []byte) {
	w.buf = append(w.buf, b...)
}

// String appends a length-prefixed string to the Writer.
func (w *Writer) String(s string) {
	w.UVarInt(uint64(len(s)))
	w.Bytes([]byte(s))
}

// String1 appends a length-prefixed string to the Writer.
//
// The maximum length of a string in this legacy format is 255 characters.
func (w *Writer) String1(s string) {
	var length uint8
	if len(s) <= 255 {
		length = uint8(len(s))
	} else {
		Throw(xerrors.Errorf("format: cannot write %d byte string as string1 (max 255 bytes)", len(s)))
	}

	w.Write(&length)
	w.Bytes([]byte(s))
}

// Write appends a fixed-size data structure to the Writer.
func (w *Writer) Write(v interface{}) {
	size := binary.Size(v)
	if size == -1 {
		Throw(xerrors.Errorf("format: cannot write type %T: non-constant size", v))
	}

	var buf bytes.Buffer

	err := binary.Write(&buf, binary.LittleEndian, v)
	if err != nil {
		Throw(xerrors.Errorf("format: failed to write type %T: %w", v, err))
	}

	w.Bytes(buf.Bytes())
}
