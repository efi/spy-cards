package gui

import "golang.org/x/mobile/event/key"

type Event struct {
	key.Event
	x, y  float32
	click bool
}
