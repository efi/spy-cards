// +build !headless

package visual

import (
	"context"
	"image/color"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/match"
)

// DeckBuilder constructs or edits a deck.
type DeckBuilder struct {
	cam    gfx.Camera
	set    *card.Set
	rules  *card.GameRules
	picked *Card
	picker *Layout
	deck   *Layout
	tb     *sprites.TextBatch
	oldX   float32
	oldY   float32

	Confirmed bool
	reset     struct {
		x, y float32
	}
}

// NewDeckBuilder creates a deck builder.
func NewDeckBuilder(ctx context.Context, set *card.Set) *DeckBuilder {
	d := &DeckBuilder{}

	d.cam.SetDefaults()
	d.cam.Offset = gfx.Identity()
	d.cam.Rotation = gfx.Identity()
	d.cam.Position = gfx.Identity()

	d.set = set

	d.picker = NewLayout(ctx)
	d.picker.scroll = -1
	d.picker.MarginTop = 0.15
	d.picker.OnSelect = func(c *Card) {
		d.picked.Def = c.Def
		d.picked.SetFlip(false)

		d.resetPicker()
	}
	d.picker.InputFocus = new(interface{})
	*d.picker.InputFocus = d.picker

	d.deck = NewLayout(ctx)
	d.deck.MinPerRow = 8
	d.deck.MaxPerRow = 20
	d.deck.MinScale = 0.75
	d.deck.BaseScale = 0.75
	d.deck.MaxScale = 0.75
	d.deck.HoverScale = 0.5
	d.deck.CenterLastRow = true
	d.deck.AllowScroll = false
	d.deck.OnSelect = func(c *Card) {
		if c.Def == nil {
			return
		}

		c.SetFlip(true)
		c.Def = nil

		d.resetPicker()
	}
	d.deck.InputFocus = d.picker.InputFocus
	d.picker.InputFocusUp = func() {
		d.deck.Focus()
	}
	d.deck.InputFocusDown = func() {
		d.picker.Focus()
	}

	d.rules, _ = set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if d.rules == nil {
		d.rules = &card.DefaultGameRules
	}

	d.deck.Cards = make([]*Card, d.rules.CardsPerDeck)
	for i := range d.deck.Cards {
		c := &Card{Card: &match.Card{Set: set}}

		switch {
		case i < int(d.rules.BossCards):
			c.Back = card.Boss
		case i < int(d.rules.BossCards+d.rules.MiniBossCards):
			c.Back = card.MiniBoss
		default:
			c.Back = card.Enemy
		}

		d.deck.Cards[i] = c
	}

	d.resetPicker()

	return d
}

func (d *DeckBuilder) resetPicker() {
	d.picker.Cards = d.picker.Cards[:0]

	d.picked = nil

	for _, c := range d.deck.Cards {
		if c.Def == nil {
			d.picked = c

			break
		}
	}

	if d.picked != nil {
		pickedDeck := make(card.Deck, 0, len(d.deck.Cards))

		for _, c := range d.deck.Cards {
			if c.Card.Def != nil {
				pickedDeck = append(pickedDeck, c.Card.Def.ID)
			}
		}

		for _, c := range d.set.ByBack(d.picked.Back, pickedDeck) {
			d.picker.Cards = append(d.picker.Cards, &Card{
				Card: &match.Card{
					Set:  d.set,
					Def:  c,
					Back: c.Rank.Back(),
				},
			})
		}

		d.deck.MaxPerRow = 20
		d.deck.BaseScale = 0.75
		d.deck.MaxScale = 0.75
		d.deck.MarginBottom = 0.85
		d.picker.Focus()
		d.deck.InputFocusLeft = func() {
			*d.deck.InputFocus = &d.reset
		}
		d.deck.InputFocusUp = func() {
			*d.deck.InputFocus = &d.reset
		}
		d.deck.InputFocusDown = func() {
			d.picker.Focus()
		}
	} else {
		d.deck.MaxPerRow = 8
		d.deck.MarginBottom = 0.4
		d.deck.Focus()
		d.deck.InputFocusLeft = nil
		d.deck.InputFocusUp = nil
		d.deck.InputFocusDown = func() {
			*d.deck.InputFocus = d
		}
	}

	d.picker.Update(nil)
}

// Update processes one frame of input.
func (d *DeckBuilder) Update() {
	d.picker.Update(nil)
	d.deck.Update(nil)

	w, h := gfx.Size()
	d.cam.Perspective = gfx.Scale(-64/float32(w), -64/float32(h), 0.01)

	if d.picked == nil {
		d.deck.BaseScale = d.deck.BaseScale*0.9 + 1.5*0.1
		d.deck.MaxScale = d.deck.MaxScale*0.9 + 2*0.1

		if *d.deck.InputFocus == d {
			switch {
			case d.deck.ictx.Consume(arcade.BtnConfirm):
				d.Confirmed = true
			case d.deck.ictx.Consume(arcade.BtnUp):
				d.deck.Focus()
			}
		}

		x, y, click := d.deck.ictx.Mouse()
		if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Play With This Deck", 0, -7.1, 0, 3, 3, x, y, true) {
			if x != d.oldX || y != d.oldY {
				*d.deck.InputFocus = d
			}

			if click {
				d.Confirmed = true
			}
		} else if *d.deck.InputFocus == d && (x != d.oldX || y != d.oldY) {
			*d.deck.InputFocus = nil
		}

		d.oldX = x
		d.oldY = y
	} else {
		if *d.deck.InputFocus == &d.reset {
			switch {
			case d.deck.ictx.Consume(arcade.BtnConfirm):
				d.deck.Cards = nil
				d.Confirmed = true
			case d.deck.ictx.Consume(arcade.BtnDown):
				d.deck.Focus()
			case d.deck.ictx.Consume(arcade.BtnRight):
				d.deck.Focus()
			}
		}

		d.reset.x = -float32(w)/64 + 0.5
		d.reset.y = float32(h)/64 - 1.2

		x, y, click := d.deck.ictx.Mouse()

		if d.oldX != x || d.oldY != y {
			if sprites.TextHitTest(&d.cam, sprites.FontBubblegumSans, "Reset", d.reset.x, d.reset.y, 0, 1.5, 1.5, x, y, false) {
				*d.deck.InputFocus = &d.reset
			} else if *d.deck.InputFocus == &d.reset {
				*d.deck.InputFocus = nil
			}
		}

		if x < 0.1 && y < 0.05 && click {
			d.deck.Cards = nil
			d.Confirmed = true
		}

		d.oldX = x
		d.oldY = y
	}
}

// Render draws the deck builder to the screen.
func (d *DeckBuilder) Render(ctx context.Context) {
	for pass := 0; pass < 3; pass++ {
		d.picker.Render(ctx, pass)
	}

	for pass := 0; pass < 3; pass++ {
		d.deck.Render(ctx, pass)
	}

	if d.tb == nil {
		d.tb = sprites.NewTextBatch(&d.cam)
	} else {
		d.tb.Reset(&d.cam)
	}

	if d.picked == nil {
		sprites.DrawTextCenteredFuncEx(d.tb, sprites.FontBubblegumSans, "Tap a card to replace it, or", 0, -5, 0, 3, 3, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)
		sprites.DrawTextCentered(d.tb, sprites.FontBubblegumSans, "Tap a card to replace it, or", 0, -5, 0, 3, 3, sprites.White, false)

		border := sprites.FlagBorder
		borderColor := sprites.Black

		if *d.deck.InputFocus == d {
			border |= sprites.FlagRainbow
			borderColor = sprites.White
		}

		sprites.DrawTextCenteredFuncEx(d.tb, sprites.FontBubblegumSans, "Play With This Deck", 0, -7.1, 0, 3, 3, func(rune) color.RGBA { return borderColor }, false, border, 0, 0, 0)
		sprites.DrawTextCentered(d.tb, sprites.FontBubblegumSans, "Play With This Deck", 0, -7.1, 0, 3, 3, effectHighlightColor, false)
	} else {
		border := sprites.FlagBorder
		borderColor := sprites.Black
		textColor := sprites.Gray

		if *d.deck.InputFocus == &d.reset {
			border |= sprites.FlagRainbow
			borderColor = sprites.White
			textColor = sprites.White
		}

		sprites.DrawTextFuncEx(d.tb, sprites.FontBubblegumSans, "Reset", d.reset.x, d.reset.y, 0, 1.5, 1.5, func(rune) color.RGBA { return borderColor }, border, 0, 0, 0)
		sprites.DrawText(d.tb, sprites.FontBubblegumSans, "Reset", d.reset.x, d.reset.y, 0, 1.5, 1.5, textColor)
	}

	d.tb.Render()
}
