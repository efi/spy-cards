package card

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"math/rand"
	"net/http"
	"net/url"
	"time"

	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
)

// Set is a set of custom cards.
type Set struct {
	External string
	Mode     *GameMode
	Spoiler  []*BannedCards
	Variant  int
	Cards    []*Def
	vanilla  map[ID]*Def

	SpoilerGuard [2]*[32]byte

	AppliedPR         bool
	RandomByBackOrder bool
}

// Card returns the card with the given ID.
func (cs *Set) Card(id ID) *Def {
	for _, c := range cs.Cards {
		if id == c.ID {
			return c
		}
	}

	if c, ok := cs.vanilla[id]; ok {
		return c
	}

	if cs.vanilla == nil {
		cs.vanilla = make(map[ID]*Def)
	}

	c := Vanilla(id)
	cs.vanilla[id] = c

	return c
}

// MarshalText implements encoding.TextMarshaler.
func (cs *Set) MarshalText() ([]byte, error) {
	if cs.External != "" && cs.Cards == nil {
		return []byte(cs.External), nil
	}

	parts := make([][]byte, 1+len(cs.Cards))
	cards := parts[1:]

	if cs.Mode != nil {
		b, err := cs.Mode.MarshalText()
		if err != nil {
			return nil, xerrors.Errorf("card: encoding game mode: %w", err)
		}

		parts[0] = b
	} else {
		parts = parts[1:]
	}

	for i, c := range cs.Cards {
		b, err := c.MarshalText()
		if err != nil {
			return nil, xerrors.Errorf("card: encoding card at index %d: %w", i, err)
		}

		cards[i] = b
	}

	return bytes.Join(parts, []byte{','}), nil
}

// ExternalMode is a Spy Cards Online game mode as returned by the server.
type ExternalMode struct {
	Name      string
	Cards     string
	Revision  int
	QuickJoin bool
}

// FetchMode downloads a game mode from the server.
func FetchMode(ctx context.Context, name, revision string) (*ExternalMode, error) {
	u := "latest/" + url.PathEscape(name)
	if revision != "0" {
		u = "get-revision/" + url.PathEscape(name) + "/" + url.PathEscape(revision)
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, internal.GetConfig(ctx).CustomCardAPIBaseURL+u, nil)
	if err != nil {
		return nil, xerrors.Errorf("card: downloading game mode: %w", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, xerrors.Errorf("card: downloading game mode: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, xerrors.Errorf("card: server returned %q", resp.Status)
	}

	var data ExternalMode

	if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
		return nil, xerrors.Errorf("card: downloading game mode: %w", err)
	}

	return &data, nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (cs *Set) UnmarshalText(b []byte) error {
	cs.External = ""

	parts := bytes.Split(b, []byte{','})
	if len(parts) == 1 && bytes.Contains(parts[0], []byte{'.'}) {
		cs.External = string(parts[0])

		dot := bytes.IndexByte(parts[0], '.')
		name := string(parts[0][:dot])
		revision := string(parts[0][dot+1:])

		data, err := FetchMode(context.TODO(), name, revision)
		if err != nil {
			return err
		}

		b = []byte(data.Cards)
		parts = bytes.Split(b, []byte{','})
	}

	if len(parts) == 1 && len(parts[0]) == 0 {
		cs.Mode = nil
		cs.Variant = -1
		cs.Cards = nil

		return nil
	}

	var mode *GameMode

	cardDefs := make([]Def, len(parts))
	cards := make([]*Def, len(parts))

	for i, part := range parts {
		p, err := base64.StdEncoding.DecodeString(string(part))
		if err != nil {
			return xerrors.Errorf("card: decoding card at index %d: %w", i, err)
		}

		if i == 0 && len(p) != 0 && p[0] == 3 {
			mode = &GameMode{}

			if err = mode.UnmarshalBinary(p); err != nil {
				return xerrors.Errorf("card: parsing game mode data: %w", err)
			}

			continue
		}

		cards[i] = &cardDefs[i]

		if err = cards[i].UnmarshalBinary(p); err != nil {
			return xerrors.Errorf("card: parsing card at index %d: %w", i, err)
		}
	}

	cs.Mode = mode
	cs.Variant = -1

	if mode != nil {
		cs.Cards = cards[1:]
	} else {
		cs.Cards = cards
	}

	var unpickable *BannedCards

	for _, c := range cs.Cards {
		if c.legacyUnpickable {
			if unpickable == nil {
				if cs.Mode == nil {
					cs.Mode = &GameMode{}
				}

				unpickable = &BannedCards{
					Flags: BannedCardRandomSummonable,
				}
				cs.Mode.Fields = append(cs.Mode.Fields, unpickable)
			}

			unpickable.Cards = append(unpickable.Cards, c.ID)
		}
	}

	return nil
}

// ByBack returns the cards in this Set with the specified back design that
// are available for deck building.
func (cs *Set) ByBack(back Rank, d Deck) []*Def {
	unpickable := make(map[ID]bool)

	if cs.Mode != nil {
		for _, f := range cs.Mode.GetAll(FieldBannedCards) {
			bc := f.(*BannedCards)

			if len(bc.Cards) == 0 {
				for i := ID(0); i < 128; i++ {
					unpickable[i] = true
				}
			} else {
				for _, id := range bc.Cards {
					unpickable[id] = true
				}
			}
		}
	}

	for _, bc := range cs.Spoiler {
		for _, id := range bc.Cards {
			unpickable[id] = true
		}
	}

	var vanilla []ID

	switch back {
	case Enemy:
		vanilla = shortIndex[0]
	case MiniBoss:
		vanilla = shortIndex[1]
	case Boss:
		vanilla = shortIndex[2]
	}

	cards := make([]*Def, 0, len(vanilla))

	for _, id := range vanilla {
		if unpickable[id] {
			continue
		}

		cd := cs.Card(id)
		if cd != nil && cd.Rank.Back() == back {
			cards = append(cards, cd)
		}
	}

	for _, c := range cs.Cards {
		if c.Rank.Back() != back || unpickable[c.ID] {
			continue
		}

		if c.ID < 128 && c.ID.Rank().Back() == back {
			continue // already found
		}

		cards = append(cards, c)
	}

	limits := make(map[*DeckLimitFilter]uint64)
	for _, f := range cs.Mode.GetAll(FieldDeckLimitFilter) {
		limits[f.(*DeckLimitFilter)] = 0
	}

	for _, id := range d {
		for l := range limits {
			if l.IsMatch(cs.Card(id)) {
				limits[l]++
			}
		}
	}

	for l, count := range limits {
		if count < l.Count {
			continue
		}

		for i := 0; i < len(cards); i++ {
			if l.IsMatch(cards[i]) {
				cards = append(cards[:i], cards[i+1:]...)
				i--
			}
		}
	}

	if back != Enemy {
		// boss and mini-boss cards are unique
		for _, id := range d {
			for i := 0; i < len(cards); i++ {
				if cards[i].ID == id {
					cards = append(cards[:i], cards[i+1:]...)
					i--
				}
			}
		}
	}

	if cs.RandomByBackOrder {
		r := rand.New(rand.NewSource(time.Now().UnixNano())) /*#nosec*/

		r.Shuffle(len(cards), func(i, j int) {
			cards[i], cards[j] = cards[j], cards[i]
		})
	}

	return cards
}

// Sets is a combination of sets of cards.
type Sets []Set

// Apply creates a Set that is the combination of cards in these sets,
// replacing IDs to ensure they don't overlap.
func (cs Sets) Apply() (*Set, error) {
	if len(cs) == 1 {
		return &cs[0], nil
	}

	var combined Set

	usedID := make(map[ID]bool)

	for i := range cs {
		replaceID := make(map[ID]ID)

		for _, c := range cs[i].Cards {
			if !usedID[c.ID] {
				usedID[c.ID] = true

				continue
			}

			id := ID(128)

			for usedID[id] {
				id++
			}

			replaceID[c.ID] = id
			usedID[id] = true
		}

		for _, c := range cs[i].Cards {
			for from, to := range replaceID {
				c.UpdateID(from, to)
			}
		}

		combined.Cards = append(combined.Cards, cs[i].Cards...)

		if cs[i].Mode != nil {
			if combined.Mode == nil {
				combined.Mode = &GameMode{}
			}

			for _, f := range cs[i].Mode.Fields {
				for from, to := range replaceID {
					f.UpdateID(from, to)
				}
			}

			combined.Mode.Fields = append(combined.Mode.Fields, cs[i].Mode.Fields...)
		}
	}

	return &combined, nil
}

// MarshalText implements encoding.TextMarshaler.
func (cs Sets) MarshalText() ([]byte, error) {
	parts := make([][]byte, len(cs))

	for i, set := range cs {
		p, err := set.MarshalText()
		if err != nil {
			return nil, xerrors.Errorf("card: encoding card set %d: %w", i, err)
		}

		parts[i] = p
	}

	return bytes.Join(parts, []byte{';'}), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (cs *Sets) UnmarshalText(b []byte) error {
	parts := bytes.Split(b, []byte{';'})
	sets := make([]Set, len(parts))

	for i, p := range parts {
		if err := sets[i].UnmarshalText(p); err != nil {
			return xerrors.Errorf("card: parsing card set %d: %w", i, err)
		}
	}

	*cs = sets

	return nil
}
