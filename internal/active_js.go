// +build js,wasm
// +build !headless

package internal

import "syscall/js"

var document = js.Global().Get("document")

// SetActive sets the active flag for the UI, which hides some UI elements.
func SetActive(b bool) {
	html := document.Get("documentElement")
	html.Get("classList").Call("toggle", "active", b)
}

// SetTitle sets the window title.
func SetTitle(title string) {
	document.Set("title", title)
}
