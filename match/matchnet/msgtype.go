package matchnet

import (
	"github.com/pion/webrtc/v3"
)

type relayedMessageType string

const (
	msgIceCandidate  relayedMessageType = "ice-candidate"
	msgPlayer1Offer  relayedMessageType = "player1-offer"
	msgPlayer2Answer relayedMessageType = "player2-answer"
	msgUseRelay      relayedMessageType = "use-relay"
	msgRelay         relayedMessageType = "r"
)

type relayedMessage struct {
	Type      relayedMessageType         `json:"type"`
	Candidate *webrtc.ICECandidateInit   `json:"candidate,omitempty"`
	Offer     *webrtc.SessionDescription `json:"offer,omitempty"`
	Answer    *webrtc.SessionDescription `json:"answer,omitempty"`
	Channel   string                     `json:"c,omitempty"`
	Payload   []byte                     `json:"p,omitempty"`
}

type matchmakingMessageType uint8

const (
	msgSessionID matchmakingMessageType = 's'
	msgKeepAlive matchmakingMessageType = 'k'
	msgHandshake matchmakingMessageType = 'p'
	msgRelayed   matchmakingMessageType = 'r'
	msgQuit      matchmakingMessageType = 'q'
)

type controlMessageType uint8

const (
	msgVersion      controlMessageType = 'v'
	msgGameQuit     controlMessageType = 'q'
	msgReady        controlMessageType = 'r'
	msgRematch      controlMessageType = 'a'
	msgCustomCards  controlMessageType = 'c'
	msgCosmeticData controlMessageType = 'o'
	msgSpoilerGuard controlMessageType = 'g'
	msgInHand       controlMessageType = 'i'
	msgModifyCards  controlMessageType = 'm'
)
