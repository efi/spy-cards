// +build !js !wasm headless

package highscores

import "git.lubar.me/ben/spy-cards/arcade/game"

func (hs *HighScores) setParametersFromURL()        {}
func (hs *HighScores) setURL()                      {}
func (hs *HighScores) clearURL()                    {}
func (v *RecordingViewer) pushURL()                 {}
func (v *RecordingViewer) popURL()                  {}
func (v *RecordingViewer) pushListener(*game.State) {}
func (v *RecordingViewer) popListener()             {}
