if (!NodeList.prototype.forEach) {
	NodeList.prototype.forEach = Array.prototype.forEach as any;
}
module SpyCards {
	// because we can't use [...arr] in old browsers
	export function toArray(arr: Uint8Array): number[] {
		return [].slice.call(arr, 0);
	}
}
if (!window.TextDecoder) {
	(window as any).TextDecoder = function () {
		// from https://stackoverflow.com/a/59339612/2664560
		this.decode = function (array: Uint8Array): string {
			let out = "";
			const len = array.length;
			let i = 0;
			while (i < len) {
				const c = array[i++];
				switch (c >> 4) {
					case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
						// 0xxxxxxx
						out += String.fromCharCode(c);
						break;
					case 12: case 13:
						// 110x xxxx   10xx xxxx
						const char1 = array[i++];
						out += String.fromCharCode(((c & 0x1F) << 6) | (char1 & 0x3F));
						break;
					case 14:
						// 1110 xxxx  10xx xxxx  10xx xxxx
						const char2 = array[i++];
						const char3 = array[i++];
						out += String.fromCharCode(((c & 0x0F) << 12) |
							((char2 & 0x3F) << 6) |
							((char3 & 0x3F) << 0));
						break;
				}
			}

			return out;
		};
	};
}
