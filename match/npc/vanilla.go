package npc

import (
	"log"
	"math/rand"
	"time"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
)

// GenericNPC implements the Bug Fables Spy Cards AI.
type GenericNPC struct {
	r *rand.Rand
}

func (g *GenericNPC) rng() *rand.Rand {
	if g.r == nil {
		g.r = rand.New(rand.NewSource(time.Now().UnixNano())) /*#nosec*/
	}

	return g.r
}

func (g *GenericNPC) randn(n int) int {
	return g.rng().Intn(n)
}

// PickPlayer picks a random player character.
func (g *GenericNPC) PickPlayer() *room.Character {
	c := room.PlayerCharacters[g.randn(len(room.PlayerCharacters))]

	for c.Hidden {
		c = room.PlayerCharacters[g.randn(len(room.PlayerCharacters))]
	}

	return c
}

// CreateDeck creates a completely random deck, avoiding ELK.
func (g *GenericNPC) CreateDeck(set *card.Set) card.Deck {
	rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	d := make(card.Deck, rules.CardsPerDeck)

	for i := range d {
		var available []*card.Def

		switch {
		case i < int(rules.BossCards):
			available = set.ByBack(card.Boss, append(card.Deck{card.TheEverlastingKing}, d[:i]...))
		case i < int(rules.BossCards+rules.MiniBossCards):
			available = set.ByBack(card.MiniBoss, d[:i])
		default:
			available = set.ByBack(card.Enemy, d[:i])
		}

		if len(available) == 0 {
			log.Printf("ERROR: cannot pick %d-th card: no cards available", i)

			return nil
		}

		d[i] = available[g.randn(len(available))].ID
	}

	return d
}

// PlayRound plays cards, if possible, from left to right.
func (g *GenericNPC) PlayRound(m *match.Match) uint64 {
	var choices uint64

	hand := m.State.Sides[m.Perspective-1].Hand
	tp := m.State.Sides[m.Perspective-1].TP

	if tp.NaN {
		return 0
	}

	if tp.AmountInf > 0 {
		return (1 << len(hand)) - 1
	}

	for i, c := range hand {
		if c.Def.TP+m.State.Sides[m.Perspective-1].ModTP[c.Def.ID] <= tp.Amount {
			tp.Amount -= c.Def.TP + m.State.Sides[m.Perspective-1].ModTP[c.Def.ID]
			choices |= 1 << i
		}
	}

	return choices
}

// AfterRound does nothing.
func (g *GenericNPC) AfterRound(m *match.Match) {}

// CardMaster is a generic NPC with a predefined deck and character.
type CardMaster struct {
	GenericNPC

	Character *room.Character
	Deck      card.Deck
}

// PickPlayer returns a predefined player character.
func (cm *CardMaster) PickPlayer() *room.Character {
	if cm.Character != nil {
		return cm.Character
	}

	return cm.GenericNPC.PickPlayer()
}

// CreateDeck returns a predefined deck.
func (cm *CardMaster) CreateDeck(set *card.Set) card.Deck {
	return cm.Deck
}

// TourneyPlayer is a generic NPC with a predefined character.
type TourneyPlayer struct {
	GenericNPC

	Character *room.Character
}

// PickPlayer returns a predefined player character.
func (tp *TourneyPlayer) PickPlayer() *room.Character {
	if tp.Character != nil {
		return tp.Character
	}

	return tp.GenericNPC.PickPlayer()
}

// Janet is... Janet.
type Janet struct {
	GenericNPC
}

// PickPlayer returns janet.
func (j *Janet) PickPlayer() *room.Character {
	return room.CharacterByName["janet"]
}

// CreateDeck has a 50% chance of picking ELK as a boss card.
func (j *Janet) CreateDeck(set *card.Set) card.Deck {
	d := j.GenericNPC.CreateDeck(set)

	rules, _ := set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	pickable := false

	for _, c := range set.ByBack(card.Boss, d) {
		if c.ID == card.TheEverlastingKing {
			pickable = true

			break
		}
	}

	for i := 0; i < int(rules.CardsPerDeck) && i < int(rules.BossCards); i++ {
		if pickable && j.randn(2) == 1 {
			d[i] = card.TheEverlastingKing

			break
		}
	}

	return d
}
