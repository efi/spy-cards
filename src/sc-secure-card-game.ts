type ExchangeDataFunc = (data: Uint8Array) => Promise<Uint8Array>;

type RNGSharedType = false /* local */ | true /* shared */ | 2 /* remote */;

interface SCGTurn<T = object> {
	readonly seed: Uint8Array;
	readonly data: T;
	seed2?: Uint8Array;
	promise?: Uint8Array;
	confirm1?: Uint8Array;
	confirm2?: Uint8Array;
}

interface SecureCardGameInit {
	hash?: AlgorithmIdentifier;
	seedLength?: number;
	randLength?: number;
	forReplay?: [number, number, number];
}

enum SyncPoint {
	InitialSeed,
	DeckPromise,
	TurnSeedPromise,
	TurnSeed,
	ConfirmTurn,
	Finalize,
}

async function doExchangeData(syncPoint: SyncPoint, syncIndex: number, exchangeDataAsync: ExchangeDataFunc, sendData: Uint8Array): Promise<Uint8Array> {
	let buf: number[] = [];
	SpyCards.Binary.pushUVarInt(buf, syncPoint);
	SpyCards.Binary.pushUVarInt(buf, syncIndex);
	buf.push(...SpyCards.toArray(sendData));
	try {
		const recvData = await exchangeDataAsync(new Uint8Array(buf));
		buf = SpyCards.toArray(recvData);
		const recvSyncPoint: SyncPoint = SpyCards.Binary.shiftUVarInt(buf);
		const recvSyncIndex = SpyCards.Binary.shiftUVarInt(buf);
		if (recvSyncPoint !== syncPoint || recvSyncIndex !== syncIndex) {
			throw new Error("received data for " + SyncPoint[recvSyncPoint] + (recvSyncIndex ? "[" + recvSyncIndex + "]" : "") + ": " + Base64.encode(recvData));
		}
		return new Uint8Array(buf);
	} catch (ex) {
		throw new Error("while exchanging data for " + SyncPoint[syncPoint] + (syncIndex ? "[" + syncIndex + "]" : "") + ": " + (ex.message || ex));
	}
}

class SecureCardGame {
	private readonly hash: AlgorithmIdentifier;
	private readonly seedLength: number;
	private readonly randLength: number;
	private readonly compat?: [number, number, number];

	private totalAdvanceCount: [number, number, number] = [0, 0, 0];
	private player: 0 | 1 | 2;
	private turns: SCGTurn[];
	private finalized: boolean;

	seed: Uint8Array;
	localRandSeed: Uint8Array;
	remoteRandSeed: Uint8Array;

	localDeckInitial: Uint8Array;
	private remoteDeckInitial: Uint8Array;
	private remoteCardBacks: Uint8Array;

	public constructor(config?: SecureCardGameInit) {
		config = config || {};
		this.hash = config.hash || "SHA-256";
		this.seedLength = config.seedLength || 16;
		this.randLength = config.randLength || 4;

		if (config.forReplay) {
			this.compat = config.forReplay;
			this.turns = [{ seed: null, data: null }];
			return;
		}

		this.player = 0;
		this.seed = new Uint8Array(this.seedLength * 2);
		this.localRandSeed = new Uint8Array(this.randLength);
		crypto.getRandomValues(this.seed.subarray(0, this.seedLength));
		crypto.getRandomValues(this.localRandSeed);
	}

	public async init(playerNumber: 1 | 2, exchangeDataAsync: ExchangeDataFunc) {
		if (this.player !== 0) {
			console.error("init called multiple times on a single connection!");
			return;
		}
		if (playerNumber !== 1 && playerNumber !== 2) {
			throw new Error("playerNumber must be either 1 or 2");
		}
		this.player = playerNumber;

		const sendSeed = this.seed.subarray(0, this.seedLength);

		if (playerNumber === 2) {
			this.seed.set(sendSeed, this.seedLength);
		}

		const recvSeed = await doExchangeData(SyncPoint.InitialSeed, 0, exchangeDataAsync, sendSeed);
		if (!(recvSeed instanceof Uint8Array) || recvSeed.length !== this.seedLength) {
			throw new Error("initial handshake: expected " + this.seedLength + " bytes of data, but received " + recvSeed.length + " bytes");
		}

		if (playerNumber === 1) {
			this.seed.set(recvSeed, this.seedLength);
		} else {
			this.seed.set(recvSeed, 0);
		}
	}

	public async setDeck(exchangeDataAsync: ExchangeDataFunc, deck: Uint8Array, cardBacks?: Uint8Array): Promise<Uint8Array> {
		if (this.player === 0) {
			throw new Error("init must be called before setDeck");
		}
		if (this.localDeckInitial) {
			throw new Error("setDeck has already been called");
		}
		if (!deck) {
			throw new Error("missing deck");
		}

		const typedDeck = new Uint8Array(deck);
		this.localDeckInitial = typedDeck;

		const buffer = new Uint8Array(this.seed.length + this.randLength + this.localDeckInitial.byteLength);
		buffer.set(this.seed, 0);
		buffer.set(this.localRandSeed, this.seed.length);
		buffer.set(new Uint8Array(this.localDeckInitial.buffer, 0), this.seed.length + this.randLength);

		const bufferHash = new Uint8Array(await crypto.subtle.digest(this.hash, buffer));
		const sendData = cardBacks ? new Uint8Array(bufferHash.length + cardBacks.length) : bufferHash;
		if (cardBacks) {
			sendData.set(bufferHash, 0);
			sendData.set(cardBacks, bufferHash.length);
		}

		const recvData = await doExchangeData(SyncPoint.DeckPromise, 0, exchangeDataAsync, sendData);
		if (!(recvData instanceof Uint8Array) || recvData.length < bufferHash.length) {
			throw new Error("while exchanging deck information: expected at least " + bufferHash.length + " bytes of data, but received " + recvData.length + " bytes");
		}

		if (cardBacks) {
			this.remoteCardBacks = new Uint8Array(recvData.buffer, bufferHash.length);
		} else if (recvData.length !== bufferHash.length) {
			throw new Error("unexpectedly received card backs");
		}

		this.turns = [];

		return cardBacks ? new Uint8Array(this.remoteCardBacks) : undefined;
	}

	public async beginTurn(exchangeDataAsync: ExchangeDataFunc): Promise<object> {
		if (!this.turns) {
			throw new Error("init and setDeck must be called before beginTurn may be called");
		}
		if (this.finalized) {
			throw new Error("cannot begin turn on finalized game");
		}

		const turn = {
			seed: new Uint8Array(this.randLength * 2),
			data: {}
		};
		const sendSeed = turn.seed.subarray((this.player - 1) * this.randLength).subarray(0, this.randLength);
		crypto.getRandomValues(sendSeed);
		const hashInput = new Uint8Array(this.seed.length + this.randLength);
		hashInput.set(this.seed, 0);
		hashInput.set(sendSeed, this.seed.length);

		const sendHash = new Uint8Array(await crypto.subtle.digest(this.hash, hashInput));
		const recvHash = await doExchangeData(SyncPoint.TurnSeedPromise, this.turns.length + 1, exchangeDataAsync, sendHash);
		if (!(recvHash instanceof Uint8Array) || sendHash.length !== recvHash.length) {
			throw new Error("setting up turn (1): expected " + sendHash.length + " bytes of data but received " + recvHash.length + " bytes");
		}

		const recvSeed = await doExchangeData(SyncPoint.TurnSeed, this.turns.length + 1, exchangeDataAsync, sendSeed);
		if (!(recvSeed instanceof Uint8Array) || sendSeed.length !== recvSeed.length) {
			throw new Error("setting up turn (2): expected " + sendSeed.length + " bytes of data but received " + recvSeed.length + " bytes");
		}

		hashInput.set(recvSeed, this.seed.length);
		const verifyHash = new Uint8Array(await crypto.subtle.digest(this.hash, hashInput));
		if (!recvHash.every(function (b, i) { return b === verifyHash[i] })) {
			throw new Error("remote hash did not match value. possible implementation error or cheating attempt.");
		}
		turn.seed.set(recvSeed, (2 - this.player) * this.randLength);
		this.turns.push(turn);

		await this.initTurnSeed(turn.seed, this.localRandSeed);

		return turn.data;
	}

	public async prepareTurn(data: Uint8Array): Promise<Uint8Array> {
		if (!this.turns || !this.turns.length) {
			throw new Error("init, setDeck, and beginTurn must be called before prepareTurn may be called");
		}
		if (this.finalized) {
			throw new Error("cannot prepare turn on finalized game");
		}

		const turn = this.turns[this.turns.length - 1];
		if (!turn.seed2) {
			turn.seed2 = new Uint8Array(this.randLength * 2);
		}

		const confirm = new Uint8Array(this.seed.length + this.randLength + data.length);
		confirm.set(this.seed, 0);
		crypto.getRandomValues(confirm.subarray(this.seed.length, this.seed.length + this.randLength));
		turn.seed2.set(confirm.subarray(this.seed.length, this.seed.length + this.randLength), this.player === 2 ? this.randLength : 0);
		confirm.set(data, this.seed.length + this.randLength);
		turn[this.player === 2 ? "confirm2" : "confirm1"] = confirm.subarray(this.seed.length);

		const promise = new Uint8Array(await crypto.subtle.digest(this.hash, confirm), 0);
		return promise;
	}

	public promiseTurn(promise: Uint8Array) {
		if (!this.turns || !this.turns.length) {
			throw new Error("init, setDeck, and beginTurn must be called before promiseTurn may be called");
		}
		if (this.finalized) {
			throw new Error("cannot modify turn on finalized game");
		}

		const turn = this.turns[this.turns.length - 1];
		turn.promise = new Uint8Array(promise);
	}

	public async confirmTurn(exchangeDataAsync: ExchangeDataFunc): Promise<Uint8Array> {
		if (!this.turns || !this.turns.length) {
			throw new Error("init, setDeck, and beginTurn must be called before confirmTurn may be called");
		}
		if (this.finalized) {
			throw new Error("cannot modify turn on finalized game");
		}
		const turn = this.turns[this.turns.length - 1];
		if (!turn.promise) {
			throw new Error("promiseTurn must be called before confirmTurn");
		}

		const confirmBuf = await doExchangeData(SyncPoint.ConfirmTurn, this.turns.length, exchangeDataAsync, turn[this.player === 2 ? "confirm2" : "confirm1"]);
		if (confirmBuf.length < this.randLength) {
			throw new Error("received turn confirmation buffer that is too short");
		}

		const confirmHashBuf = new Uint8Array(this.seed.length + confirmBuf.length);
		confirmHashBuf.set(this.seed, 0);
		confirmHashBuf.set(confirmBuf, this.seed.length);

		const confirmHash = new Uint8Array(await crypto.subtle.digest(this.hash, confirmHashBuf), 0);
		if (!confirmHash.every((b, i) => b === turn.promise[i])) {
			throw new Error("turn confirmation validation failed (implementation error or possible cheating)");
		}

		turn.confirm2 = confirmBuf;
		turn.seed2.set(confirmBuf.subarray(0, this.randLength), this.player === 2 ? 0 : this.randLength);

		await this.whenConfirmedTurn(turn.seed2);

		return confirmBuf.slice(this.randLength);
	}

	public async whenConfirmedTurn(seed2: Uint8Array) {
		this.sharedRandBuffer.set(seed2, 32 + this.seed.length);
		this.sharedRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.sharedRandBuffer.subarray(32)));
		this.sharedRandIdx = 0;
	}

	public async finalize(exchangeDataAsync: ExchangeDataFunc, verifyDeckAsync: (deck: Uint8Array, backs?: Uint8Array) => Promise<void>, verifyTurnAsync: (turn: SCGTurn) => Promise<void>) {
		if (this.finalized) {
			throw new Error("finalize already called");
		}
		if (!this.turns) {
			throw new Error("finalize called on uninitialized SecureCardGame instance");
		}
		this.finalized = true;

		const sendData = new Uint8Array(this.randLength + this.localDeckInitial.byteLength);
		sendData.set(this.localRandSeed, 0);
		sendData.set(new Uint8Array(this.localDeckInitial.buffer, 0), this.randLength);

		const recvData = await doExchangeData(SyncPoint.Finalize, 0, exchangeDataAsync, sendData);
		if (!(recvData instanceof Uint8Array) || recvData.length < this.randLength || ((recvData.length - this.randLength) % this.localDeckInitial.BYTES_PER_ELEMENT) !== 0) {
			throw new Error("finalizing match: expected to receive at least " + this.randLength + " bytes of data, but received " + recvData.length + " bytes");
		}

		this.remoteRandSeed = new Uint8Array(recvData.buffer, 0, this.randLength);
		this.remoteDeckInitial = new Uint8Array(recvData.buffer, this.randLength);

		await verifyDeckAsync(this.remoteDeckInitial, this.remoteCardBacks);
		for (let turn of this.turns) {
			await this.initTurnSeed(turn.seed, this.localRandSeed, this.remoteRandSeed);
			await verifyTurnAsync(turn);
		}
	}

	private sharedRandState: Uint8Array;
	private sharedRandIdx: number;
	private sharedRandBuffer: Uint8Array;

	private localRandState: Uint8Array;
	private localRandIdx: number;
	private localRandBuffer: Uint8Array;

	private remoteRandState: Uint8Array;
	private remoteRandIdx: number;
	private remoteRandBuffer: Uint8Array;

	async initTurnSeed(turnSeed: Uint8Array, localSeed: Uint8Array, remoteSeed?: Uint8Array) {
		if (!this.sharedRandBuffer) {
			this.sharedRandBuffer = new Uint8Array(32 + this.seed.length + this.randLength * 2);
			this.sharedRandBuffer.set(this.seed, 32);
		}
		this.sharedRandBuffer.set(turnSeed, 32 + this.seed.length);
		this.sharedRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.sharedRandBuffer.subarray(32)));
		this.sharedRandIdx = 0;

		const compat274 = this.compat && this.compat[0] === 0 && this.compat[1] === 2 && this.compat[2] <= 74;

		if (compat274) {
			if (!this.localRandBuffer) {
				this.localRandBuffer = new Uint8Array(32 + this.randLength);
			}

			this.localRandBuffer.set(this.sharedRandState, 0);
			this.localRandBuffer.set(localSeed, 32);
			this.localRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.localRandBuffer));
			this.localRandIdx = 0;

			if (remoteSeed) {
				this.localRandBuffer.set(remoteSeed, 32);
				this.remoteRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.localRandBuffer));
				this.remoteRandIdx = 0;
			}
			return;
		}

		if (!this.localRandBuffer) {
			this.localRandBuffer = new Uint8Array(32 + this.seed.length + this.randLength * 3);
			this.localRandBuffer.set(this.seed, 32);
		}

		this.localRandBuffer.set(turnSeed, 32 + this.seed.length);
		this.localRandBuffer.set(localSeed, 32 + this.seed.length + turnSeed.length);
		this.localRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.localRandBuffer.subarray(32)));
		this.localRandIdx = 0;

		if (remoteSeed) {
			if (!this.remoteRandBuffer) {
				this.remoteRandBuffer = new Uint8Array(32 + this.seed.length + this.randLength * 3);
				this.remoteRandBuffer.set(this.seed, 32);
			}

			this.remoteRandBuffer.set(turnSeed, 32 + this.seed.length);
			this.remoteRandBuffer.set(remoteSeed, 32 + this.seed.length + turnSeed.length);
			this.remoteRandState = new Uint8Array(await crypto.subtle.digest(this.hash, this.remoteRandBuffer.subarray(32)));
			this.remoteRandIdx = 0;
		}
	}

	public async rand(shared: RNGSharedType, max: number): Promise<number> {
		if (!this.turns || !this.turns.length) {
			throw new Error("rand cannot be called before a turn is started");
		}
		if (max < 1) {
			throw new Error("rand cannot generate a number between 0 and " + (max - 1));
		}
		if (max !== (max | 0)) {
			throw new Error("rand max must be an integer");
		}

		let numBytes = 0;
		for (let i = max; i; i = i >> 8) {
			numBytes++;
		}

		const compat274 = this.compat && this.compat[0] === 0 && this.compat[1] === 2 && this.compat[2] <= 74;

		let maxRawValue = max;
		while (maxRawValue + max <= (compat274 ? (numBytes << 3) : (1 << (numBytes << 3)))) {
			maxRawValue += max;
		}

		let rawValue;
		do {
			rawValue = await this.advanceRNGMulti(shared, numBytes);
		} while (compat274 ? rawValue > maxRawValue : rawValue >= maxRawValue);

		return rawValue % max;
	}

	private async advanceRNG(shared: RNGSharedType): Promise<number> {
		if (shared === 2) {
			let idx = this.remoteRandIdx;
			if (idx >= this.remoteRandState.length) {
				idx = 0;
				this.remoteRandState = await this.nextRandState(this.remoteRandState, this.remoteRandBuffer);
			}
			this.remoteRandIdx = idx + 1;
			this.totalAdvanceCount[2]++;
			return this.remoteRandState[idx];
		} else if (shared) {
			let idx = this.sharedRandIdx;
			if (idx >= this.sharedRandState.length) {
				idx = 0;
				this.sharedRandState = await this.nextRandState(this.sharedRandState, this.sharedRandBuffer);
			}
			this.sharedRandIdx = idx + 1;
			this.totalAdvanceCount[1]++;
			return this.sharedRandState[idx];
		} else {
			let idx = this.localRandIdx;
			if (idx >= this.localRandState.length) {
				idx = 0;
				this.localRandState = await this.nextRandState(this.localRandState, this.localRandBuffer);
			}
			this.localRandIdx = idx + 1;
			this.totalAdvanceCount[0]++;
			return this.localRandState[idx];
		}
	}

	private async advanceRNGMulti(shared: RNGSharedType, count: number): Promise<number> {
		let val = 0;
		for (let i = 0; i < count; i++) {
			val = val | (await this.advanceRNG(shared) << (i << 3));
		}
		return val;
	}

	private async nextRandState(currentState: Uint8Array, updateBuffer: Uint8Array): Promise<Uint8Array> {
		const compat274 = this.compat && this.compat[0] === 0 && this.compat[1] === 2 && this.compat[2] <= 74;
		if (compat274) {
			updateBuffer = new Uint8Array(this.seed.length + currentState.length);
			updateBuffer.set(this.seed, 0);
			updateBuffer.set(currentState, this.seed.length);
		} else {
			updateBuffer.set(currentState, 0);
		}
		return new Uint8Array(await crypto.subtle.digest(this.hash, updateBuffer));
	}

	public async shuffle<T>(shared: RNGSharedType, items: T[]) {
		for (let i = 1; i < items.length; i++) {
			const j = await this.rand(shared, i + 1);

			const tmp = items[i];
			items[i] = items[j];
			items[j] = tmp;
		}
	}

	public async publicShuffle<T>(cards: T[], cardBacks: number[]) {
		if (cards.length !== cardBacks.length) {
			throw new Error("cards and cardBacks must have the same length");
		}

		for (let i = 1; i < cards.length; i++) {
			const j = await this.rand(true, i + 1);

			const tmp = cards[i];
			cards[i] = cards[j];
			cards[j] = tmp;

			const tmp2 = cardBacks[i];
			cardBacks[i] = cardBacks[j];
			cardBacks[j] = tmp2;
		}
	}

	public async privateShuffle<T>(cards: T[], cardBacks: number[], remote: boolean) {
		if (cards.length !== cardBacks.length) {
			throw new Error("cards and cardBacks must have the same length");
		}

		const cardArrays: T[][] = [];
		const byBack: { [back: number]: T[] } = {};
		for (let i = 0; i < cards.length; i++) {
			if (!byBack[cardBacks[i]]) {
				cardArrays.push(byBack[cardBacks[i]] = []);
			}
			byBack[cardBacks[i]].push(cards[i]);
		}

		await this.shuffle(true, cardBacks);
		for (let arr of cardArrays) {
			await this.shuffle(remote ? 2 : false, arr);
		}

		for (let i = 0; i < cards.length; i++) {
			cards[i] = byBack[cardBacks[i]].shift();
		}
	}
}
