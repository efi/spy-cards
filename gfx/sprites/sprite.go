// Package sprites handles loading and rendering sprites.
package sprites

import (
	"git.lubar.me/ben/spy-cards/gfx"
)

// Sprite holds the data needed to render a sprite.
type Sprite struct {
	S0, S1 float32
	T0, T1 float32
	X0, X1 float32
	Y0, Y1 float32

	*spriteSheet
}

// Blank is a white square extending 1 unit in each direction.
var Blank = (&spriteSheet{
	AssetTexture: gfx.BlankAssetTexture(),

	width:     2,
	height:    2,
	unitPerPx: 2,
}).sprite(0, 0, 2, 2, 0.5, 0.5)

// New creates a new sprite.
func New(tex *gfx.AssetTexture, unitPerPx float32, width, height int, x, y, w, h, px, py float32) *Sprite {
	return (&spriteSheet{
		AssetTexture: tex,

		width:     width,
		height:    height,
		unitPerPx: unitPerPx,
	}).sprite(x, y, w, h, px, py)
}

type spriteSheet struct {
	*gfx.AssetTexture

	width, height int
	unitPerPx     float32
}

func loadSpriteSheet(url string, unitPerPx float32, width, height int, nearest bool) *spriteSheet {
	return &spriteSheet{
		AssetTexture: gfx.NewAssetTexture(url, nearest),

		width:     width,
		height:    height,
		unitPerPx: unitPerPx,
	}
}

func (s *spriteSheet) sprite(x, y, w, h, px, py float32) *Sprite {
	anchorX := w * px
	anchorY := h * py

	return &Sprite{
		X0: -anchorX / s.unitPerPx,
		Y0: -anchorY / s.unitPerPx,
		X1: (w - anchorX) / s.unitPerPx,
		Y1: (h - anchorY) / s.unitPerPx,
		S0: x / float32(s.width),
		T0: 1 - (y+h)/float32(s.height),
		S1: (x + w) / float32(s.width),
		T1: 1 - y/float32(s.height),

		spriteSheet: s,
	}
}
