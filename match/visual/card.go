// +build !headless

package visual

import (
	"context"
	"image/color"
	"math"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/internal"
	"git.lubar.me/ben/spy-cards/match"
	"git.lubar.me/ben/spy-cards/room"
	"golang.org/x/mobile/gl"
)

const (
	cardHoverInSpeed  = 30
	cardHoverOutSpeed = 20
	deckHoverInSpeed  = 60
	deckHoverOutSpeed = 25
	cardNearThreshold = 3
	flipSpeed         = 20
)

var (
	cardWidth  = sprites.CardFront.X1 - sprites.CardFront.X0
	cardHeight = sprites.CardFront.Y1 - sprites.CardFront.Y0

	effectHighlightColor = color.RGBA{68, 170, 255, 255}
)

type Card struct {
	*match.Card
	Active *match.ActiveCard
	ModTP  int64

	banned         bool
	unpickable     bool
	flip           uint8
	flipDir        int8
	cachedDef      *card.Def
	cachedDesc     *card.RichDescription
	portraitTex    *gfx.AssetTexture
	portraitSprite *sprites.Sprite
	batch          *sprites.Batch
	textBatch      *sprites.TextBatch
	lastEffect     *card.EffectDef
	hover          *HoverTilt
	coins          []*coin
	coinFaceBatch  *sprites.Batch
}

type coin struct {
	dx, dy         float32
	invisibleTimer uint64
	revealTimer    uint64
	stat, heads    bool
}

var cardBack = [...]*sprites.Sprite{
	card.Enemy:    sprites.BackEnemy,
	card.MiniBoss: sprites.BackMiniBoss,
	card.Boss:     sprites.BackBoss,
	card.Token:    sprites.BackToken,
}

var cardColor = [...]color.RGBA{
	card.Attacker: {0, 178, 0, 255},
	card.Effect:   {255, 117, 2, 255},
	card.MiniBoss: {127, 127, 127, 255},
	card.Boss:     {255, 164, 2, 255},
	card.Token:    {69, 96, 130, 255},
}

// Render draws a card to the screen.
func (c *Card) Render(ctx context.Context, cam *gfx.Camera, highlightEffect *card.EffectDef) {
	var flags sprites.RenderFlag

	if c.Def == nil {
		c.flip = 255
	}

	flip := float32(c.flip) / 255

	cam.PushTransform(gfx.RotationY(-flip * math.Pi))

	if c.Def == nil || c.Def != c.cachedDef {
		c.cachedDesc = nil
		c.portraitSprite = nil
		c.portraitTex = nil
		c.banned = false
		c.unpickable = false
	}

	if c.Active != nil && c.Active.Mode == match.ModeNumb {
		c.SetFlip(true)
	}

	if c.Active != nil && (c.Active.Mode == match.ModeSetup || c.Active.Mode == match.ModeSetupOriginal) {
		flags |= sprites.FlagHalfGlow
	}

	if c.Def != nil {
		if c.cachedDesc == nil {
			if c.Active != nil {
				c.cachedDesc = c.Active.Desc
			} else {
				c.cachedDesc = c.Def.Description(c.Set)
			}

			c.banned = false
			c.unpickable = false

			if c.Def.Rank == card.Token {
				c.banned = true
				c.unpickable = true
			} else {
				for _, f := range c.Set.Mode.GetAll(card.FieldBannedCards) {
					bc := f.(*card.BannedCards)

					listed := false

					if len(bc.Cards) == 0 {
						listed = c.Def.ID < 128
					} else {
						for _, id := range bc.Cards {
							if c.Def.ID == id {
								listed = true

								break
							}
						}
					}

					if listed {
						if bc.Flags&card.BannedCardRandomSummonable == 0 {
							c.banned = true
						} else {
							c.unpickable = true
						}
					}
				}
			}
		}
	}

	if c.batch != nil && c.lastEffect == highlightEffect && c.cachedDef == c.Def {
		c.batch.SetCamera(cam)
		c.textBatch.SetCamera(cam)
	} else {
		c.cachedDef = c.Def

		if c.batch == nil {
			c.batch = sprites.NewBatch(cam)
			c.textBatch = sprites.NewTextBatch(cam)
		} else {
			c.batch.Reset(cam)
			c.textBatch.Reset(cam)
		}

		c.batch.Append(cardBack[c.Back], 0, 0, 0.05, 1, 1, sprites.White, flags, math.Pi, 0, 0)

		simpleDesc := c.Def != nil && ((len(c.Def.Effects) == 1 && c.Def.Effects[0].Type == card.EffectStat && c.Def.Effects[0].Flags&card.FlagStatOpponent == 0) ||
			(len(c.Def.Effects) == 2 && c.Def.Effects[0].Type == card.EffectStat && c.Def.Effects[0].Flags&(card.FlagStatOpponent|card.FlagStatDEF) == 0 &&
				c.Def.Effects[1].Type == card.EffectStat && c.Def.Effects[1].Flags&(card.FlagStatOpponent|card.FlagStatDEF) == card.FlagStatDEF))

		if c.Def != nil {
			c.batch.Append(sprites.CardFront, 0, 0, 0, 1, 1, cardColor[c.Def.Rank], flags, 0, 0, 0)

			c.initPortraitSprite(ctx)

			if c.portraitSprite != nil {
				c.batch.Append(c.portraitSprite, 0, 0.45, -0.05, 1, 1, sprites.White, sprites.FlagNoDiscard|flags, 0, 0, 0)
			}

			c.batch.Append(sprites.CardFrontWindow, 0, 0, 0, 1, 1, cardColor[c.Def.Rank], flags, 0, 0, 0)

			c.batch.Append(sprites.WhiteBar, -0.05, 1.3, 0, 0.35, 0.6, sprites.White, flags, 0, 0, 0)

			switch {
			case !c.unpickable:
				c.batch.Append(sprites.TP, 0.9, 1.3, 0.025, 0.4, 0.4, sprites.White, flags, 0, 0, 0)
			case !c.banned:
				c.batch.Append(sprites.TPInf, 0.9, 1.3, 0.025, 0.4, 0.4, sprites.White, flags, 0, 0, 0)
			default:
				c.batch.Append(sprites.WhiteBar, 0.05, 1.3, 0, 0.35, 0.6, sprites.White, flags, 0, 0, 0)
			}

			c.batch.Append(sprites.Description, 0, -0.825, 0, 0.8, 0.75, sprites.White, sprites.FlagNoDiscard|flags, 0, 0, 0)

			if simpleDesc {
				if len(c.Def.Effects) == 1 {
					sprite := sprites.ATK
					if c.Def.Effects[0].Flags&card.FlagStatDEF == card.FlagStatDEF {
						sprite = sprites.DEF
					}

					c.batch.Append(sprite, -0.35, -0.8, -0.05, 0.45, 0.45, sprites.White, flags, 0, 0, 0)
				} else {
					c.batch.Append(sprites.ATK, -0.4, -0.8, -0.05, 0.45, 0.45, sprites.White, flags, 0, 0, 0)
					c.batch.Append(sprites.DEF, 0.5, -0.8, -0.05, 0.45, 0.45, sprites.White, flags, 0, 0, 0)
				}
			}
		}

		if c.Def != nil {
			if !c.unpickable {
				c.renderNumber(c.textBatch, 0.9, 1.3, 0.025, 0.4, 0.4, c.Def.TP+c.ModTP, false, true, false, flags)
			}

			nameWidth := float32(1.65)

			if c.unpickable && c.banned {
				nameWidth = 1.95
			}

			(&card.RichDescription{
				Text:  c.Def.DisplayName(),
				Color: sprites.Black,
			}).Typeset(sprites.FontBubblegumSans, -1, 1.175, 0, 0.5, 0.5, nameWidth, 0, nil, false, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
				sprites.DrawTextFuncEx(c.textBatch, sprites.FontBubblegumSans, text, x, y, z, sx, sy, func(rune) color.RGBA {
					return tint
				}, sprites.FlagNoDiscard|flags, 0, 0, 0)
			})

			if simpleDesc {
				if len(c.Def.Effects) == 1 {
					c.renderNumber(c.textBatch, 0.5, -0.8, -0.05, 1, 1, c.Def.Effects[0].Amount, c.Def.Effects[0].Flags&card.FlagStatInfinity == card.FlagStatInfinity, false, highlightEffect == c.Def.Effects[0], flags)
				} else {
					c.renderNumber(c.textBatch, -0.25, -0.8, -0.0125, 1, 1, c.Def.Effects[0].Amount, c.Def.Effects[0].Flags&card.FlagStatInfinity == card.FlagStatInfinity, false, highlightEffect == c.Def.Effects[0], flags)
					c.renderNumber(c.textBatch, 0.65, -0.8, -0.0125, 1, 1, c.Def.Effects[1].Amount, c.Def.Effects[1].Flags&card.FlagStatInfinity == card.FlagStatInfinity, false, highlightEffect == c.Def.Effects[1], flags)
				}
			} else {
				c.cachedDesc.Typeset(sprites.FontBubblegumSans, -0.9, -0.5, 0, 0.3, 0.375, 1.85, 0.95, highlightEffect, false, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
					if highlightEffect != nil && isEffect {
						sprites.DrawTextFuncEx(c.textBatch, sprites.FontBubblegumSans, text, x, y, z, sx, sy, func(rune) color.RGBA {
							return effectHighlightColor
						}, sprites.FlagBorder|sprites.FlagNoDiscard|flags, 0, 0, 0.05)
					}
					sprites.DrawTextFuncEx(c.textBatch, sprites.FontBubblegumSans, text, x, y, z, sx, sy, func(rune) color.RGBA {
						return tint
					}, sprites.FlagNoDiscard|flags, 0, 0, 0)
				})
			}

			visibleTribes := 0
			deadLander := false

			for _, t := range c.Def.Tribes {
				if t.Tribe == card.TribeDeadLander {
					deadLander = true
				}

				if t.Tribe != card.TribeCustom || !strings.HasPrefix(t.CustomName, "_") {
					visibleTribes++
				}
			}

			var tribeScale float32

			tribeSprite := sprites.TribeBubble
			tribeWide := float32(1.0)

			switch {
			case visibleTribes > 2:
				tribeScale = 2.0 / float32(visibleTribes)
			case visibleTribes == 1 && deadLander:
				tribeScale = 1.0
				tribeWide = 2.0
				tribeSprite = sprites.TribeBubbleWide
			default:
				tribeScale = 1.0
			}

			tribeIndex := 0

			for _, t := range c.Def.Tribes {
				if t.Tribe == card.TribeCustom && strings.HasPrefix(t.CustomName, "_") {
					continue
				}

				tx := ((float32(tribeIndex)+0.5)/float32(visibleTribes) - 0.5) * 2.2

				c.batch.Append(tribeSprite, tx, -1.4, 0, 0.65*tribeScale, 0.65, t.Color(), flags, 0, 0, 0)

				(&card.RichDescription{
					Text:  t.Name(),
					Color: sprites.White,
				}).Typeset(sprites.FontD3Streetism, tx, -1.475, 0, 0.3, 0.3, 0.9*tribeScale*tribeWide, 0, nil, false, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
					sprites.DrawTextCenteredFuncEx(c.textBatch, sprites.FontD3Streetism, text, x, y, z, sx, sy, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagNoDiscard|sprites.FlagBorder|flags, 0, 0, 0.05)
					sprites.DrawTextCenteredFuncEx(c.textBatch, sprites.FontD3Streetism, text, x, y, z, sx, sy, func(rune) color.RGBA {
						return tint
					}, false, sprites.FlagNoDiscard|flags, 0, 0, 0)
				})

				tribeIndex++
			}
		}

		c.lastEffect = highlightEffect
	}

	gfx.GL.Enable(gl.CULL_FACE)
	gfx.GL.CullFace(gl.FRONT)

	if c.flip >= 128 {
		c.renderCoins(cam)
	}

	c.batch.Render()
	c.textBatch.Render()

	if c.flip < 128 {
		c.renderCoins(cam)
	}

	gfx.GL.Disable(gl.CULL_FACE)

	cam.PopTransform()
}

func (c *Card) initPortraitSprite(ctx context.Context) {
	if c.portraitSprite != nil {
		return
	}

	switch c.Def.Portrait {
	case card.PortraitCustomEmbedded:
		tex := gfx.NewEmbeddedTexture("(embedded portrait:"+c.Def.DisplayName()+")", false, func() ([]byte, string, error) {
			return c.Def.CustomPortrait, "image/png", nil
		})
		c.portraitTex = tex
		c.portraitSprite = sprites.New(tex, 100, 128, 128, 0, 0, 128, 128, 0.5, 0.5)
	case card.PortraitCustomExternal:
		tex := gfx.NewCachedAssetTexture(internal.GetConfig(ctx).UserImageBaseURL + format.Encode32(c.Def.CustomPortrait) + ".png")
		c.portraitTex = tex
		c.portraitSprite = sprites.New(tex, 100, 128, 128, 0, 0, 128, 128, 0.5, 0.5)
	default:
		c.portraitSprite = sprites.Portraits[c.Def.Portrait]
	}
}

func (c *Card) renderCoins(cam *gfx.Camera) {
	if c.coinFaceBatch == nil {
		c.coinFaceBatch = sprites.NewBatch(cam)
	} else {
		c.coinFaceBatch.Reset(cam)
	}

	for _, coin := range c.coins {
		if coin.invisibleTimer != 0 {
			continue
		}

		spin := math.Pi/2 - float32(coin.revealTimer)/3
		sin := float32(math.Sin(float64(spin)))
		cos := float32(math.Cos(float64(spin)))

		face1, face2 := sprites.Happy, sprites.Sad
		scale1, scale2 := float32(0.5), float32(0.5)

		if coin.stat {
			face1, face2 = sprites.ATK, sprites.DEF
			scale1, scale2 = 0.25, 0.35
		}

		if coin.heads {
			face1, face2 = face2, face1
			scale1, scale2 = scale2, scale1
		}

		c.coinFaceBatch.Append(face1, coin.dx-cos*0.025, coin.dy, 0.5-sin*0.025, scale1, scale1, sprites.White, 0, spin+math.Pi/2, 0, 0)
		c.coinFaceBatch.Append(face2, coin.dx+cos*0.025, coin.dy, 0.5+sin*0.025, scale2, scale2, sprites.White, 0, spin-math.Pi/2, 0, 0)

		cam.PushTransform(gfx.MultiplyMatrix(
			gfx.Translation(coin.dx, coin.dy, 0.5),
			gfx.RotationY(spin),
		))
		gfx.GL.CullFace(gl.BACK)
		room.RenderCoin(cam)
		gfx.GL.CullFace(gl.FRONT)
		cam.PopTransform()
	}

	c.coinFaceBatch.Render()
}

func (c *Card) renderNumber(tb *sprites.TextBatch, x, y, z, sx, sy float32, num int64, inf, invert, highlight bool, flags sprites.RenderFlag) {
	sy *= 1.25
	y -= 0.25 * sy

	desc := card.RichDescription{
		Text:  strconv.FormatInt(num, 10),
		Color: sprites.White,
	}

	if num < 0 {
		if invert {
			desc.Color = sprites.Green
		} else {
			desc.Color = sprites.Red
		}
	}

	if inf {
		if num < 0 {
			desc.Text = "-∞"
		} else {
			desc.Text = "∞"
		}
	}

	desc.Typeset(sprites.FontD3Streetism, x, y, z, sx, sy, sx, 0, nil, false, func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool) {
		sprites.DrawTextCenteredFuncEx(tb, sprites.FontD3Streetism, text, x, y, z, sx, sy, func(rune) color.RGBA {
			if highlight {
				return effectHighlightColor
			}

			return sprites.Black
		}, false, sprites.FlagNoDiscard|sprites.FlagBorder|flags, 0, 0, 0.035)

		sprites.DrawTextCenteredFuncEx(tb, sprites.FontD3Streetism, text, x, y, z, sx, sy, func(rune) color.RGBA {
			return tint
		}, false, sprites.FlagNoDiscard|flags, 0, 0, 0)
	})
}

// Flip flips the card from face-up to face-down or vice versa.
func (c *Card) Flip() {
	c.SetFlip(!c.IsFlip())
}

// SetFlip flips the card to a specified side.
func (c *Card) SetFlip(down bool) {
	if down {
		c.flipDir = flipSpeed
	} else {
		c.flipDir = -flipSpeed
	}
}

// IsFlip returns true if the card is face-down.
func (c *Card) IsFlip() bool {
	return c.flipDir >= 0 && c.flip > 0
}
