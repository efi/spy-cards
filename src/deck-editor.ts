(async function () {
	let defs = new SpyCards.CardDefs();
	let customCardsRaw: string;
	let versionSuffix = "";
	let versionTextSuffix = "";
	let backLinkURL = ".";
	let linkSuffix = "";
	if (location.hash && location.hash.indexOf(",") !== -1) {
		customCardsRaw = location.hash.substr(location.hash.indexOf(",") + 1);
		const dotIndex = customCardsRaw.indexOf(".");
		try {
			let customCardData: string;
			if (dotIndex !== -1 && customCardsRaw.indexOf(";") === -1) {
				const modeName = customCardsRaw.substr(0, dotIndex);
				const modeRevision = parseInt(customCardsRaw.substr(dotIndex + 1), 10);
				customCardsRaw = null;

				versionSuffix = "-" + modeName;
				backLinkURL = "play.html?mode=" + modeName;
				if (modeRevision) {
					backLinkURL += "&rev=" + modeRevision;
				}
				const { Cards, Revision } = await SpyCards.fetchCustomCardSet(modeName, modeRevision);
				customCardData = Cards;
				versionTextSuffix = " (rev. " + Revision + ")";
				linkSuffix = "," + modeName + "." + Revision;
			} else {
				versionSuffix = "-custom";
				backLinkURL = "custom.html#" + customCardsRaw;
				customCardData = customCardsRaw;
				linkSuffix = "," + customCardsRaw;
			}
			defs = new SpyCards.CardDefs(await SpyCards.parseCustomCards(customCardData, "ignore-variant"));
		} catch (ex) {
			errorHandler(ex);
			return;
		}
	}
	await SpyCards.SpoilerGuard.banSpoilerCards(defs, new Uint8Array(0));

	const form = document.createElement("main");
	form.classList.add("play-online", "active");
	document.body.insertBefore(form, document.body.firstChild);

	const h1 = document.createElement("h1");
	h1.setAttribute("aria-live", "polite");
	h1.setAttribute("aria-atomic", "true");
	const status = document.createElement("p");
	status.setAttribute("role", "status");
	status.setAttribute("aria-live", "polite");
	status.setAttribute("aria-atomic", "true");
	const backLink = document.createElement("a");
	backLink.classList.add("back-link");
	backLink.href = backLinkURL;
	backLink.rel = "home";
	backLink.textContent = "← Back";

	const version = document.createElement("span");
	version.classList.add("version-number");
	version.textContent = "Version ";
	const versionLink = document.createElement("a");
	versionLink.href = "docs/changelog.html#v" + spyCardsVersionPrefix;
	versionLink.textContent = spyCardsVersionPrefix + versionSuffix;
	version.appendChild(versionLink);
	version.appendChild(document.createTextNode(versionTextSuffix));

	const code = document.createElement("input");
	code.type = "text";
	code.addEventListener("focus", function () {
		code.select();
	});

	let linkedDeck: SpyCards.Deck = null;
	let inLinkedDeck = false;

	function removeHash() {
		if (location.hash) {
			if (linkSuffix) {
				location.hash = "#" + linkSuffix;
			} else {
				history.pushState("", document.title, location.pathname + location.search);
			}
		}
	}

	function closeLinkedDeck() {
		linkedDeck = null;
		inLinkedDeck = false;
		showDefaultUI();

		removeHash();
	}

	const saveLinkedDeckButton = document.createElement("button");
	saveLinkedDeckButton.classList.add("btn1");
	saveLinkedDeckButton.textContent = "Save";
	saveLinkedDeckButton.addEventListener("click", function (e) {
		e.preventDefault();

		SpyCards.Decks.saveNewDeck(defs, linkedDeck);

		closeLinkedDeck();
	});

	const editLinkedDeckButton = document.createElement("button");
	editLinkedDeckButton.classList.add("btn1");
	editLinkedDeckButton.textContent = "Edit";
	editLinkedDeckButton.addEventListener("click", function (e) {
		e.preventDefault();

		SpyCards.UI.clear(form);
		form.appendChild(backLink);
		form.appendChild(version);
		h1.textContent = "Deck Editor";
		form.appendChild(h1);
		status.textContent = "";
		form.appendChild(status);

		const deck = linkedDeck;
		linkedDeck = null;
		inLinkedDeck = false;
		removeHash();

		SpyCards.Decks.editDeck(status, defs, deck).then((deck) => SpyCards.Decks.confirmSelectDeck(status, defs, deck, () => Promise.resolve(null))).then(showDefaultUI);
	});

	const closeLinkedDeckButton = document.createElement("button");
	closeLinkedDeckButton.classList.add("btn2");
	closeLinkedDeckButton.textContent = "Close";
	closeLinkedDeckButton.addEventListener("click", function (e) {
		e.preventDefault();

		closeLinkedDeck();
	});

	function parseLinkedDeck(noSaveButton?: boolean) {
		linkedDeck = null;

		try {
			linkedDeck = SpyCards.Decks.decode(defs, location.hash.substr(1).split(/,/, 2)[0]);
		} catch (ex) {
			// invalid deck code
			if (inLinkedDeck) {
				closeLinkedDeck();
			}
			return;
		}

		inLinkedDeck = true;
		SpyCards.UI.clear(form);
		form.appendChild(version);
		h1.textContent = "Linked Deck";
		form.appendChild(h1);
		if (!h1.id) {
			h1.id = SpyCards.UI.uniqueID();
		}
		code.setAttribute("aria-labelledby", h1.id);
		code.value = SpyCards.Decks.encode(defs, linkedDeck);
		code.readOnly = true;
		form.appendChild(code);
		form.appendChild(SpyCards.Decks.createDisplay(defs, linkedDeck));
		if (noSaveButton) {
			form.appendChild(editLinkedDeckButton);
		} else {
			form.appendChild(saveLinkedDeckButton);
		}
		form.appendChild(closeLinkedDeckButton);
	}

	let ignoreHashUpdate = false;

	async function showDefaultUI() {
		SpyCards.UI.clear(form);
		form.appendChild(backLink);
		form.appendChild(version);
		h1.textContent = "Deck Editor";
		form.appendChild(h1);
		status.textContent = "Select a deck.";
		form.appendChild(status);

		const exportButton = document.createElement("button");
		exportButton.classList.add("btn1");
		exportButton.textContent = "Export All";
		const importButton = document.createElement("button");
		importButton.classList.add("btn2");
		importButton.textContent = "Import";

		const decks = SpyCards.Decks.loadSaved(defs);

		exportButton.addEventListener("click", function (e) {
			e.preventDefault();

			showImportExport(true, decks.map((d) => SpyCards.Decks.encode(defs, d)).join("\n"));
		});

		importButton.addEventListener("click", function (e) {
			e.preventDefault();

			showImportExport(false, "");
		});

		if (decks.length) {
			form.appendChild(exportButton);
		}
		form.appendChild(importButton);

		let savedDeck = await SpyCards.Decks.selectSavedDeck(status, defs, decks, true);

		if (decks.length) {
			SpyCards.UI.remove(exportButton);
		}
		SpyCards.UI.remove(importButton);

		if (savedDeck) {
			ignoreHashUpdate = true;
			location.hash = "#" + SpyCards.Decks.encode(defs, savedDeck) + linkSuffix;
			parseLinkedDeck(true);
			return;
		}

		await SpyCards.Decks.selectDeck(status, defs, true, () => Promise.resolve(null));
		showDefaultUI();
	}

	function showImportExport(isExport: boolean, initialText: string) {
		SpyCards.UI.clear(form);
		form.appendChild(version);
		h1.textContent = isExport ? "Export Decks" : "Import Decks";
		form.appendChild(h1);
		const hintMessage = isExport ? "Copy these deck codes somewhere safe." : "Paste deck codes into the box, then click Import.";
		status.textContent = hintMessage;
		form.appendChild(status);

		const input = document.createElement("textarea");
		if (!status.id) {
			status.id = SpyCards.UI.uniqueID();
		}
		input.setAttribute("aria-labelledby", status.id);
		input.readOnly = isExport;
		input.value = initialText;
		input.rows = initialText.split(/\n/g).length;
		form.appendChild(input);

		if (isExport) {
			input.select();
		} else {
			input.rows++;
			input.addEventListener("input", function () {
				input.rows = input.value.split(/\n/g).length + 1;
			});

			const importButton = document.createElement("button");
			importButton.classList.add("btn1");
			importButton.textContent = "Import";
			importButton.addEventListener("click", function (e) {
				e.preventDefault();

				const codes = input.value.split(/\n/g).map(function (str) {
					return str.replace(/\s/g, "");
				}).filter(function (str) {
					return str.length;
				});

				const successful: SpyCards.Deck[] = [];
				const unsuccessful: string[] = [];
				for (let code of codes) {
					let deck;
					try {
						deck = SpyCards.Decks.decode(defs, code);
					} catch (ex) {
						unsuccessful.push(code);
						continue;
					}

					successful.push(deck);
				}

				const successfulCount = successful.length;
				// save in reverse order as the last-saved deck is on top
				while (successful.length) {
					SpyCards.Decks.saveNewDeck(defs, successful.pop());
				}

				input.value = unsuccessful.join("\n");
				input.rows = input.value.split(/\n/g).length + 1;

				if (successfulCount) {
					if (successfulCount === 1) {
						status.textContent = "Imported 1 deck.";
					} else {
						status.textContent = "Imported " + successfulCount + " decks.";
					}
					if (unsuccessful.length) {
						status.textContent += " ";
					}
				} else {
					status.textContent = "";
				}
				if (unsuccessful.length) {
					if (unsuccessful.length === 1) {
						status.textContent += "1 deck code was invalid.";
					} else {
						status.textContent += unsuccessful.length + " deck codes were invalid.";
					}
				}
				if (!successfulCount && !unsuccessful.length) {
					status.textContent = hintMessage;
				}
			});
			form.appendChild(importButton);
		}

		const backButton = document.createElement("button");
		backButton.classList.add("btn2");
		backButton.textContent = "Back";
		backButton.addEventListener("click", function (e) {
			e.preventDefault();

			showDefaultUI();
		});
		form.appendChild(backButton);
	}

	showDefaultUI();
	addEventListener("hashchange", function () {
		if (ignoreHashUpdate) {
			ignoreHashUpdate = false;
			return;
		}
		parseLinkedDeck();
	});
	if (location.hash && location.hash.indexOf(",") !== 1) {
		parseLinkedDeck();
	}
})();
