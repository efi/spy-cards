package mainmenu

import (
	"time"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// Neon is a declarative format for flashing neon signs.
type Neon struct {
	Rate    time.Duration
	Frames  int
	Toggle  []int
	Index   []int
	Sprite  []*sprites.Sprite
	X, Y, Z []float32
	FlipX   []bool
	Scale   float32
}

var neonMain = []*Neon{
	{
		Rate:   time.Second / 6,
		Frames: 24,
		Index:  []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 22, 23, 24},
		X:      []float32{-6.4},
		Y:      []float32{5.6},
		Z:      []float32{0},
		Scale:  150,
		Sprite: []*sprites.Sprite{
			sprites.NeonCoin[0], sprites.NeonCoin[1], sprites.NeonCoin[2], sprites.NeonCoin[3], sprites.NeonCoin[2], sprites.NeonCoin[1],
			sprites.NeonCoin[0], sprites.NeonCoin[1], sprites.NeonCoin[2], sprites.NeonCoin[3], sprites.NeonCoin[2], sprites.NeonCoin[1],
			sprites.NeonCoin[0], sprites.NeonCoin[1], sprites.NeonCoin[2], sprites.NeonCoin[3], sprites.NeonCoin[2], sprites.NeonCoin[1],
		},
		FlipX: []bool{
			false, false, false, false, true, true,
			false, false, false, false, true, true,
			false, false, false, false, true, true,
		},
	},
	{
		Rate:   time.Second / 6,
		Frames: 24,
		Toggle: []int{-1, 0, 17, 20, 24},
		Index:  []int{0, 18, 19, 24},
		X:      []float32{-5},
		Y:      []float32{5.8},
		Z:      []float32{0},
		Scale:  -150,
		Sprite: []*sprites.Sprite{sprites.NeonSparkle[0], sprites.NeonSparkle[1], sprites.NeonSparkle[0]},
	},
	{
		Rate:   time.Second / 6,
		Frames: 24,
		Toggle: []int{-1, 0, 13, 22, 24},
		Index:  []int{0, 19, 20, 24},
		X:      []float32{-7.8},
		Y:      []float32{5.2},
		Z:      []float32{0},
		Scale:  150,
		Sprite: []*sprites.Sprite{sprites.NeonSparkle[0], sprites.NeonSparkle[1], sprites.NeonSparkle[0]},
	},
}

var fjWallX = []float32{
	6.2 + 6*0.55,
	6.2 + 3*0.55,
	6.2 + 1*0.55,
	6.2 + -2*0.55,
	6.2 + -6*0.55,
}

var neonFJ = []*Neon{
	{
		Rate:   time.Second / 6,
		Frames: 7,
		Index:  []int{0, 1, 2, 4, 5, 7},
		X:      []float32{5},
		Y:      []float32{10.0 / 12.0, 5.0 / 12.0, 0.0 / 12.0, 7.0 / 12.0, 14.0 / 12.0},
		Z:      []float32{0},
		Scale:  125,
		Sprite: []*sprites.Sprite{sprites.NeonFJBee},
	},
	{
		Rate:   time.Second / 6,
		Frames: 7,
		Index:  []int{0, 1, 2, 4, 5, 7},
		X:      fjWallX,
		Y:      []float32{-1.3},
		Z:      []float32{0},
		Scale:  175,
		Sprite: []*sprites.Sprite{sprites.NeonFJWall},
	},
	{
		Rate:   time.Second / 6,
		Frames: 7,
		Index:  []int{0, 1, 2, 4, 5, 7},
		X:      fjWallX,
		Y:      []float32{2.4},
		Z:      []float32{0},
		Scale:  175,
		Sprite: []*sprites.Sprite{sprites.NeonFJWall},
	},
}

var neonMK = []*Neon{
	{
		Rate:   time.Second / 6,
		Frames: 2,
		Index:  []int{0, 1, 2},
		X:      []float32{4},
		Y:      []float32{0.5},
		Z:      []float32{0},
		Scale:  300,
		Sprite: []*sprites.Sprite{sprites.NeonMKWall[0], sprites.NeonMKWall[1]},
		FlipX:  []bool{true, true},
	},
	{
		Rate:   time.Second / 6,
		Frames: 2,
		Index:  []int{0, 1, 2},
		X:      []float32{8.4},
		Y:      []float32{0.5},
		Z:      []float32{0},
		Scale:  300,
		Sprite: []*sprites.Sprite{sprites.NeonMKWall[0], sprites.NeonMKWall[1]},
	},
	{
		Rate:   time.Second / 6,
		Frames: 5,
		Index:  []int{0, 2, 3, 4, 5},
		X:      []float32{6.2},
		Y:      []float32{-0.5},
		Z:      []float32{0},
		Scale:  300,
		Sprite: []*sprites.Sprite{sprites.NeonMKKnight},
		FlipX:  []bool{false, true, false, true},
	},
}

var neonHS = []*Neon{
	{
		Rate:   time.Second / 6,
		Frames: 20,
		Toggle: []int{0, 2, 4, 8, 12, 20},
		Index:  []int{0},
		X:      []float32{6.25},
		Y:      []float32{0.5},
		Z:      []float32{0},
		Scale:  200,
		Sprite: []*sprites.Sprite{sprites.NeonHSCrown},
	},
	{
		Rate:   time.Second / 6,
		Frames: 20,
		Toggle: []int{0, 2, 4, 8, 12, 13, 15, 17, 18, 19, 20},
		Index:  []int{0},
		X:      []float32{4},
		Y:      []float32{0},
		Z:      []float32{0},
		Scale:  200,
		Sprite: []*sprites.Sprite{sprites.NeonHSSparkle},
		FlipX:  []bool{true},
	},
	{
		Rate:   time.Second / 6,
		Frames: 20,
		Toggle: []int{0, 2, 4, 8, 12, 13, 15, 17, 18, 19, 20},
		Index:  []int{0},
		X:      []float32{8.4},
		Y:      []float32{0},
		Z:      []float32{0},
		Scale:  200,
		Sprite: []*sprites.Sprite{sprites.NeonHSSparkle},
	},
}

// RenderNeon draws a neon sign to the screen.
func RenderNeon(batch *sprites.Batch, defs ...*Neon) {
	now := time.Duration(time.Now().UnixNano())

	for _, def := range defs {
		frame := int(now / def.Rate % time.Duration(def.Frames))

		if def.Toggle != nil {
			shouldRender := true

			for i, t := range def.Toggle {
				if t > frame {
					shouldRender = i&1 != 0

					break
				}
			}

			if !shouldRender {
				continue
			}
		}

		var index int

		for i, x := range def.Index {
			if x > frame {
				index = i - 1

				break
			}
		}

		sprite := def.Sprite[len(def.Sprite)-1]
		if index < len(def.Sprite) {
			sprite = def.Sprite[index]
		}

		x := getf(def.X, index)
		y := getf(def.Y, index)
		z := getf(def.Z, index)

		var flipX bool
		if index < len(def.FlipX) {
			flipX = def.FlipX[index]
		}

		sx, sy := def.Scale, def.Scale
		if flipX {
			sx = -sx
		}

		batch.Append(sprite, x, y, z, sx, sy, sprites.White, sprites.FlagNoDiscard, 0, 0, 0)
	}
}

func getf(f []float32, i int) float32 {
	if len(f) <= i {
		return f[len(f)-1]
	}

	return f[i]
}
