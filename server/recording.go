package server

import (
	"bytes"
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/format"
	"github.com/jackc/pgtype"
	"golang.org/x/xerrors"
)

func uploadMatchRecording(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	w.Header().Add("Access-Control-Allow-Methods", http.MethodPut)
	w.Header().Add("Access-Control-Allow-Headers", "content-type")

	if r.Method == http.MethodOptions {
		if r.Header.Get("Access-Control-Request-Method") != http.MethodPut {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		w.WriteHeader(http.StatusNoContent)

		return
	}

	if r.Method != http.MethodPut {
		w.Header().Add("Allow", "PUT, OPTIONS")
		http.Error(w, "only PUT is allowed", http.StatusMethodNotAllowed)

		return
	}

	defer r.Body.Close()

	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println("match recording upload error (read)", err)
		http.Error(w, "failed to read uploaded match", http.StatusInternalServerError)

		return
	}

	var mr card.Recording

	err = mr.UnmarshalBinary(data)
	if err != nil {
		log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
		log.Println("match recording upload error (decode)", err)
		http.Error(w, "failed to decode uploaded match", http.StatusBadRequest)

		return
	}

	hash := sha256.Sum256(data)

	modePrefix := mr.ModeName

	var modeSuffix sql.NullString

	if i := strings.IndexByte(modePrefix, '.'); i != -1 {
		modeSuffix.String = modePrefix[i+1:]
		modeSuffix.Valid = true
		modePrefix = modePrefix[:i]
	}

	switch {
	case mr.ModeName == "custom":
		for _, c := range mr.CustomCards.Cards {
			if c.Portrait == 254 {
				http.Error(w, "cannot upload custom cards with embedded PNG files to this endpoint (try reuploading the card image on the card with the longest code)", http.StatusBadRequest)

				return
			}
		}
	case mr.ModeName == "":
		if len(mr.CustomCards.Cards) != 0 {
			http.Error(w, "vanilla recording contained custom cards", http.StatusBadRequest)

			return
		}
	case !modeSuffix.Valid || modeSuffix.String == "":
		log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
		log.Println("unexpected recording mode", mr.ModeName, "(no suffix)")
		http.Error(w, "unexpected recording mode "+mr.ModeName, http.StatusBadRequest)

		return
	default:
		var (
			modeName      string
			expectedCards string
			modeRevision  int
		)

		// TODO: fix this code for variants
		if false {
			if err := getCustomCardSet.QueryRow(modePrefix, modeSuffix).Scan(&modeName, &expectedCards, &modeRevision); xerrors.Is(err, sql.ErrNoRows) {
				log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
				log.Println("unexpected recording mode", mr.ModeName, "(not in DB)")
				http.Error(w, "unexpected recording mode "+mr.ModeName, http.StatusBadRequest)

				return
			} else if err != nil {
				log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
				log.Println("recording mode lookup failed", err)
				http.Error(w, "database error looking up mode", http.StatusInternalServerError)

				return
			}

			expectedCodes := strings.Split(expectedCards, ",")

			if mr.CustomCards.Mode != nil {
				b, err := mr.CustomCards.Mode.MarshalBinary()
				if err != nil {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code enc err", 0, expectedCodes[0], err)
					http.Error(w, "unexpected content for game mode", http.StatusBadRequest)

					return
				}

				if enc := base64.StdEncoding.EncodeToString(b); enc != expectedCodes[0] {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code didn't match ", 0, expectedCodes[0], enc)
					http.Error(w, "unexpected content for game mode", http.StatusBadRequest)

					return
				}

				expectedCodes = expectedCodes[1:]
			}

			if len(expectedCodes) != len(mr.CustomCards.Cards) {
				log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
				log.Println("card code didn't match (length)", len(expectedCodes), len(mr.CustomCards.Cards))
				http.Error(w, "unexpected content for custom card list", http.StatusBadRequest)

				return
			}

			for i, cc := range mr.CustomCards.Cards {
				b, err := cc.MarshalBinary()
				if err != nil {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code enc err", i, expectedCodes[i], err)
					http.Error(w, "unexpected content for custom card list", http.StatusBadRequest)

					return
				}

				if enc := base64.StdEncoding.EncodeToString(b); enc != expectedCodes[i] {
					log.Println("failed recording:", base64.StdEncoding.EncodeToString(data))
					log.Println("card code didn't match ", i, expectedCodes[i], enc)
					http.Error(w, "unexpected content for custom card list", http.StatusBadRequest)

					return
				}
			}
		}
	}

	var id format.FileID

	copy(id.Hash[:], hash[:])

	if err = insertMatchRecording.QueryRow(
		r.Header.Get("X-Forwarded-For"),
		hash[:],
		data,
		mr.FormatVersion,
		mr.Version[0],
		mr.Version[1],
		mr.Version[2],
		modePrefix,
		modeSuffix,
	).Scan(&id.ID); err != nil {
		log.Println("match recording upload error (store)", err)
		http.Error(w, "failed to store uploaded match", http.StatusInternalServerError)

		return
	}

	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(http.StatusCreated)

	b, _ := id.MarshalText()
	_, _ = w.Write(b)
}

func getMatchRecording(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	var id format.FileID

	err := id.UnmarshalText([]byte(r.URL.Path[len("/spy-cards/recording/get/"):]))
	if err != nil {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	var matchHash, matchData []byte

	err = getMatchRecordingData.QueryRow(id.ID).Scan(&matchHash, &matchData)
	if xerrors.Is(err, sql.ErrNoRows) {
		w.Header().Set("Cache-Control", "public, max-age=600")
		http.NotFound(w, r)

		return
	}

	if !bytes.Equal(id.Hash[:], matchHash[:8]) {
		w.Header().Set("Cache-Control", "public, max-age=86400")
		http.NotFound(w, r)

		return
	}

	w.Header().Set("Cache-Control", "public, max-age=31536000, immutable")
	w.Header().Set("Content-Type", "application/vnd.spycards.matchdata")
	w.Header().Set("Content-Length", strconv.Itoa(len(matchData)))
	w.WriteHeader(http.StatusOK)

	if r.Method == http.MethodGet {
		_, _ = w.Write(matchData)
	}
}

func randomMatchRecording(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet && r.Method != http.MethodHead {
		w.Header().Add("Allow", "GET, HEAD")
		http.Error(w, "only GET is allowed", http.StatusMethodNotAllowed)

		return
	}

	w.Header().Add("Cache-Control", "no-store")

	if !validateOrigin(w, r) {
		return
	}

	notCodes := strings.Split(r.URL.Query().Get("not"), ",")
	if len(notCodes) == 1 && notCodes[0] == "" {
		notCodes = nil
	}

	notIDs := make([]int64, len(notCodes))

	for i, c := range notCodes {
		var id format.FileID

		err := id.UnmarshalText([]byte(c))
		if err != nil {
			http.Error(w, "invalid request", http.StatusBadRequest)

			return
		}

		notIDs[i] = int64(id.ID)
	}

	var notIDsArray pgtype.Int8Array
	if err := notIDsArray.Set(notIDs); err != nil {
		log.Println("get random recording failed to assign array", err)
		http.Error(w, "database error", http.StatusInternalServerError)

		return
	}

	var (
		id   format.FileID
		hash []byte
	)

	switch err := getRandomMatchRecording.QueryRow(notIDsArray).Scan(&id.ID, &hash); {
	case xerrors.Is(err, sql.ErrNoRows):
		http.Error(w, "no more matches available", http.StatusServiceUnavailable)
	case err != nil:
		log.Println("get random recording failed", err)
		http.Error(w, "database error", http.StatusInternalServerError)
	default:
		copy(id.Hash[:], hash)
		w.WriteHeader(http.StatusOK)

		b, _ := id.MarshalText()
		_, _ = w.Write(b)
	}
}
