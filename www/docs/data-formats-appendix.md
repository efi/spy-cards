---
title: Data Formats (Appendices)
...

# Appendix A: Card Indices

*This section contains spoilers.*

## Global IDs

| ID | Name                 |
|----|----------------------|
|  0 | Zombiant             |
|  1 | Jellyshroom          |
|  2 | Spider               |
|  3 | Zasp                 |
|  4 | Cactiling            |
|  5 | Psicorp              |
|  6 | Thief                |
|  7 | Bandit               |
|  8 | Inichas              |
|  9 | Seedling             |
| 14 | Numbnail             |
| 15 | Mothiva              |
| 16 | Acornling            |
| 17 | Weevil               |
| 19 | Venus' Bud           |
| 20 | Chomper              |
| 21 | Acolyte Aria         |
| 23 | Kabbu                |
| 24 | Venus' Guardian      |
| 25 | Wasp Trooper         |
| 26 | Wasp Bomber          |
| 27 | Wasp Driller         |
| 28 | Wasp Scout           |
| 29 | Midge                |
| 30 | Underling            |
| 31 | Monsieur Scarlet     |
| 32 | Golden Seedling      |
| 33 | Arrow Worm           |
| 34 | Carmina              |
| 35 | Seedling King        |
| 36 | Broodmother          |
| 37 | Plumpling            |
| 38 | Flowerling           |
| 39 | Burglar              |
| 40 | Astotheles           |
| 41 | Mother Chomper       |
| 42 | Ahoneynation         |
| 43 | Bee-Boop             |
| 44 | Security Turret      |
| 45 | Denmuki              |
| 46 | Heavy Drone B-33     |
| 47 | Mender               |
| 48 | Abomihoney           |
| 49 | Dune Scorpion        |
| 50 | Tidal Wyrm           |
| 51 | Kali                 |
| 52 | Zombee               |
| 53 | Zombeetle            |
| 54 | The Watcher          |
| 55 | Peacock Spider       |
| 56 | Bloatshroom          |
| 57 | Krawler              |
| 58 | Haunted Cloth        |
| 61 | Warden               |
| 63 | Jumping Spider       |
| 64 | Mimic Spider         |
| 65 | Leafbug Ninja        |
| 66 | Leafbug Archer       |
| 67 | Leafbug Clubber      |
| 68 | Madesphy             |
| 69 | The Beast            |
| 70 | Chomper Brute        |
| 71 | Mantidfly            |
| 72 | General Ultimax      |
| 73 | Wild Chomper         |
| 74 | Cross                |
| 75 | Poi                  |
| 76 | Primal Weevil        |
| 77 | False Monarch        |
| 78 | Mothfly              |
| 79 | Mothfly Cluster      |
| 80 | Ironnail             |
| 81 | Belostoss            |
| 82 | Ruffian              |
| 83 | Water Strider        |
| 84 | Diving Spider        |
| 85 | Cenn                 |
| 86 | Pisci                |
| 87 | Dead Lander α        |
| 88 | Dead Lander β        |
| 89 | Dead Lander γ        |
| 90 | Wasp King            |
| 91 | The Everlasting King |
| 92 | Maki                 |
| 93 | Kina                 |
| 94 | Yin                  |
| 95 | ULTIMAX Tank         |
| 96 | Zommoth              |
| 97 | Riz                  |
| 98 | Devourer             |

Card IDs 128 and above follow this pattern:

- 128-159: custom attacker
- 160-191: custom effect
- 192-223: custom mini-boss
- 224-255: custom boss

For card IDs 256 and above, the pattern repeats.

## Short IDs (Indices)

| ID | Enemy           | Mini-Boss        | Boss                 |
|----|-----------------|------------------|----------------------|
|  0 | Seedling        | Ahoneynation     | Spider               |
|  1 | Underling       | Acolyte Aria     | Venus' Guardian      |
|  2 | Golden Seedling | Mothiva          | Heavy Drone B-33     |
|  3 | Zombiant        | Zasp             | The Watcher          |
|  4 | Zombee          | Astotheles       | The Beast            |
|  5 | Zombeetle       | Dune Scorpion    | ULTIMAX Tank         |
|  6 | Jellyshroom     | Primal Weevil    | Mother Chomper       |
|  7 | Bloatshroom     | Cross            | Broodmother          |
|  8 | Chomper         | Poi              | Zommoth              |
|  9 | Chomper Brute   | General Ultimax  | Seedling King        |
| 10 | Psicorp         | Cenn             | Tidal Wyrm           |
| 11 | Arrow Worm      | Pisci            | Peacock Spider       |
| 12 | Thief           | Monsieur Scarlet | Devourer             |
| 13 | Bandit          | Kabbu            | False Monarch        |
| 14 | Burglar         | Kali             | Maki                 |
| 15 | Ruffian         | Carmina          | Wasp King            |
| 16 | Security Turret | Riz              | The Everlasting King |
| 17 | Abomihoney      | Kina             |                      |
| 18 | Krawler         | Yin              |                      |
| 19 | Warden          | Dead Lander α    |                      |
| 20 | Haunted Cloth   | Dead Lander β    |                      |
| 21 | Mantidfly       | Dead Lander γ    |                      |
| 22 | Jumping Spider  |                  |                      |
| 23 | Mimic Spider    |                  |                      |
| 24 | Diving Spider   |                  |                      |
| 25 | Water Strider   |                  |                      |
| 26 | Belostoss       |                  |                      |
| 27 | Mothfly         |                  |                      |
| 28 | Mothfly Cluster |                  |                      |
| 29 | Wasp Scout      |                  |                      |
| 30 | Wasp Trooper    |                  |                      |
| 31 | Acornling       |                  |                      |
| 32 | Cactiling       |                  |                      |
| 33 | Flowerling      |                  |                      |
| 34 | Plumpling       |                  |                      |
| 35 | Inichas         |                  |                      |
| 36 | Denmuki         |                  |                      |
| 37 | Madesphy        |                  |                      |
| 38 | Numbnail        |                  |                      |
| 39 | Ironnail        |                  |                      |
| 40 | Midge           |                  |                      |
| 41 | Wild Chomper    |                  |                      |
| 42 | Weevil          |                  |                      |
| 43 | Bee-Boop        |                  |                      |
| 44 | Mender          |                  |                      |
| 45 | Leafbug Ninja   |                  |                      |
| 46 | Leafbug Archer  |                  |                      |
| 47 | Leafbug Clubber |                  |                      |
| 48 | Wasp Bomber     |                  |                      |
| 49 | Wasp Driller    |                  |                      |
| 50 | Venus' Bud      |                  |                      |

## Tribe

| ID | Name        |
|----|-------------|
|  0 | Seedling    |
|  1 | Wasp        |
|  2 | Fungi       |
|  3 | Zombie      |
|  4 | Plant       |
|  5 | Bug         |
|  6 | Bot         |
|  7 | Thug        |
|  8 | ???         |
|  9 | Chomper     |
| 10 | Leafbug     |
| 11 | Dead Lander |
| 12 | Mothfly     |
| 13 | Spider      |
| 14 | (custom)    |
| 15 | (none)      |

# Appendix B: Game Rules

## Mite Knight

| ID | Name                 | Type         | Default Value | Description |
|----|----------------------|--------------|---------------|-------------|
|  0 | PauseStopsAnimations | bool         | false         |             |
|  1 | PauseOverview        | bool         | false         |             |
|  2 | FloorCount           | float64      | 3             |             |
|  3 | MapSize              | \[2\]float64 | 25, 25        |             |
|  4 | MapSizePerFloor      | \[2\]float64 | 5, 5          |             |
|  5 | MaxRoomSize          | \[2\]float64 | 3, 3          |             |
|  6 | WizardMinFloor       | float64      | 1             |             |
|  7 | AntEnemyWeight       | float64      | 3             |             |
|  8 | WizardEnemyWeight    | float64      | 2             |             |
|  9 | DoubleBossMinFloor   | float64      | 2             |             |
| 10 | RoomCount            | float64      | 8             |             |
| 11 | RoomCountPerFloor    | float64      | 0.25          |             |
| 12 | EnemyCount           | float64      | 8             |             |
| 13 | EnemyCountPerFloor   | float64      | 1             |             |
| 14 | TimeLimit            | float64      | 300           |             |
| 15 | PlayerMaxHP          | float64      | 6             |             |
| 16 | CompassDelay         | float64      | 2             |             |
| 17 | ScorePerHit          | float64      | 10            |             |
| 18 | ScorePerKill         | float64      | 100           |             |
| 19 | ScorePerKey          | float64      | 500           |             |
| 20 | ScorePerDoor         | float64      | 400           |             |
| 21 | EnemyDamageImmunity  | float64      | 1             |             |
| 22 | PlayerDamageImmunity | float64      | 1.33333333333 |             |
| 23 | CamFollowSpeed       | float64      | 0.1           |             |
| 24 | CamRotationSpeed     | float64      | 0.1           |             |
| 25 | ScoreDigits          | float64      | 5             |             |

## Flower Journey

| ID | Name                      | Type    | Default Value | Description |
|----|---------------------------|---------|---------------|-------------|
|  0 | WallMin                   | float64 | -0.9          | Lower bound of wall Y position range in meters. |
|  1 | WallMax                   | float64 | 1.1           | Upper bound of wall Y position range in meters. |
|  2 | WallGapStart              | float64 | 2.5           | Distance scale between the midpoints of the upper and lower wall. The upper and lower walls are touching at around 1.47. |
|  3 | WallGap100                | float64 | 2.25          | Distance scale between the midpoints of the upper and lower wall after 100 walls have been passed. This value is linearly interpolated and extrapolated. |
|  4 | FirstWallDist             | float64 | 5             | The X axis position of the first wall. The bee starts at x=-3. |
|  5 | WallDist                  | float64 | 5             | The X axis distance between consecutive walls in meters. |
|  6 | CountdownLength           | float64 | 3             | The maximum number in the countdown (3, 2, 1, GO!) |
|  7 | CountdownTime             | float64 | 0.75          | The number of seconds between numbers in the countdown. |
|  8 | GoTime                    | float64 | 1             | The amount of time "GO!" is displayed at the end of the countdown before gameplay begins. |
|  9 | SpeedStart                | float64 | 0.05          | The bee's X axis speed at the start of the game, in meters per frame (60 frames per second). |
| 10 | Speed100                  | float64 | 0.1           | The bee's X axis speed after 100 walls have been passed. The speed is linearly interpolated and extrapolated. |
| 11 | FlapVelocity              | float64 | 4.5           | The Y axis velocity of the bee immediately after a flap, in meters per second. |
| 12 | GravityScale              | float64 | 1.5           | A multiplier that affects the gravity on the bee (a multiplier of 1 results in 9.8 meters per second per second gravity) |
| 13 | ScorePerWall              | float64 | 15            | The number of points awarded for passing a wall. |
| 14 | ScorePerWallCombo         | float64 | 0             | The number of points awarded for passing a wall, multiplied by the combo counter. |
| 15 | ScorePerPassedEnemy       | float64 | 30            | The number of points awarded for passing a wasp. |
| 16 | ScorePerPassedEnemyCombo  | float64 | 0             | The number of points awarded for passing a wasp, multiplied by the combo counter. |
| 17 | ScorePerKilledEnemy       | float64 | 10            | The number of points awarded for killing a wasp while invulnerable. |
| 18 | ScorePerKilledEnemyCombo  | float64 | 10            | The number of points awarded for killing a wasp while invulnerable, multiplied by the combo counter. |
| 19 | ScorePerFlower            | float64 | 10            | The number of points awarded for touching a flower. |
| 20 | ScorePerFlowerCombo       | float64 | 10            | The number of points awarded for touching a flower, multiplied by the combo counter. |
| 21 | HoneyInvulnTime           | float64 | 5             | The number of seconds after touching honey that the bee is invulenerable. |
| 22 | HoneyMinCombo             | float64 | 8             | The minimum combo count before honey can spawn. |
| 23 | EnemyHeightMin            | float64 | 1.25          |             |
| 24 | EnemyHeightMax            | float64 | 3.5           |             |
| 25 | EnemyMinCombo             | float64 | 4             |             |
| 26 | EnemyMinProgress          | float64 | 8             |             |
| 27 | EnemySpeed                | float64 | 0.75          |             |
