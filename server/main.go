// Package server implements the web service for Spy Cards Online.
package server

import (
	"flag"
	"log"
	"net/http"
	"strings"

	"github.com/jackc/pgtype"
	"nhooyr.io/websocket"
)

// Main is the main method for the Spy Cards Online web service.
func Main() {
	flagHTTP := flag.String("http", ":8335", "[address]:port to listen on")
	flagDB := flag.String("db", "", "data source name for PostgreSQL")
	flag.Parse()

	initDB(*flagDB)

	http.HandleFunc("/spy-cards/ws", websocketServer)

	http.HandleFunc("/spy-cards/", spyCardsLanding)
	http.HandleFunc("/spy-cards/log-in", loginPage)
	http.HandleFunc("/spy-cards/log-out", logoutPage)
	http.HandleFunc("/spy-cards/change-password", changePasswordPage)

	http.HandleFunc("/spy-cards/report-issue", handleIssueReportPost)

	http.HandleFunc("/spy-cards/user-img/upload", uploadUserImg)
	http.HandleFunc("/spy-cards/user-img/prt.json", getUserImgTanjerinList)
	http.HandleFunc("/spy-cards/user-img/whitelist.js", getUserImgListScript)
	http.HandleFunc("/spy-cards/user-img/community.json", getUserImgCommunityList)
	http.HandleFunc("/spy-cards/user-img/", serveUserImg)

	http.HandleFunc("/spy-cards/admin/", adminMiddleware)
	adminRouter.HandleFunc("/spy-cards/admin/", adminLanding)
	adminRouter.HandleFunc("/spy-cards/admin/portraits", adminPortraits)
	adminRouter.HandleFunc("/spy-cards/admin/reports", adminReports)
	adminRouter.HandleFunc("/spy-cards/admin/recordings", adminRecordings)
	adminRouter.HandleFunc("/spy-cards/admin/arcade-recordings", adminArcadeRecordings)
	adminRouter.HandleFunc("/spy-cards/admin/modes", adminModes)
	adminRouter.HandleFunc("/spy-cards/admin/users", adminUsers)
	adminRouter.HandleFunc("/spy-cards/admin/new-user", adminNewUser)
	adminRouter.HandleFunc("/spy-cards/admin/user", adminUser)

	http.HandleFunc("/spy-cards/custom/edit-mode/", editCustomMode)
	http.HandleFunc("/spy-cards/custom/api/", handleCustomCardAPI)

	http.HandleFunc("/spy-cards/recording/upload", uploadMatchRecording)
	http.HandleFunc("/spy-cards/recording/get/", getMatchRecording)
	http.HandleFunc("/spy-cards/recording/random", randomMatchRecording)

	http.HandleFunc("/spy-cards/arcade/api/upload", handleArcadeAPIUpload)
	http.HandleFunc("/spy-cards/arcade/api/get/", handleArcadeAPIGetRecording)
	http.HandleFunc("/spy-cards/arcade/api/scores", handleArcadeAPIHighScores)

	panic(http.ListenAndServe(*flagHTTP, nil))
}

func validateOrigin(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Add("Vary", "Origin")

	origin := r.Header.Get("Origin")
	if origin == "" {
		return true
	}

	var originOK bool
	if err := checkAllowedOrigin.QueryRow(origin).Scan(&originOK); err != nil {
		log.Println("checking origin", origin, err)
		http.Error(w, "error checking origin", http.StatusInternalServerError)

		return false
	}

	if !originOK {
		http.Error(w, "origin not whitelisted", http.StatusForbidden)

		return false
	}

	w.Header().Add("Access-Control-Allow-Origin", origin)

	return true
}

func websocketServer(w http.ResponseWriter, r *http.Request) {
	var origins pgtype.TextArray
	if err := getAllowedOrigins.QueryRow().Scan(&origins); err != nil {
		// keep going, we may be fine
		log.Println("failed to get allowed origins:", err)
	}

	opts := &websocket.AcceptOptions{}
	_ = origins.AssignTo(&opts.OriginPatterns)

	for i, o := range opts.OriginPatterns {
		opts.OriginPatterns[i] = o[strings.LastIndexByte(o, '/')+1:]
	}

	conn, err := websocket.Accept(w, r, opts)
	if err != nil {
		log.Println("websocket accept err:", err)

		return
	}

	handler(conn, r)
}
