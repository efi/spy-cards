"use strict";
(function () {
    const params = new URLSearchParams(location.search);
    let autoPlay = params.get("autoplay") || params.has("autoplay");
    function setAutoPlay(newAutoPlay) {
        if (newAutoPlay === false) {
            params.delete("autoplay");
        }
        else if (newAutoPlay === true) {
            params.set("autoplay", "");
        }
        else {
            params.set("autoplay", newAutoPlay);
        }
        autoPlay = newAutoPlay;
        let url = location.pathname;
        const search = params.toString().replace(/(^|&)autoplay=(&|$)/, "$1autoplay$2");
        if (search) {
            url += "?" + search;
        }
        if (location.hash && location.hash.length > 1) {
            url += location.hash;
        }
        history.replaceState(null, document.title, url);
    }
    const form = document.createElement("div");
    form.classList.add("play-online", "recording-viewer", "recording-viewer-main");
    const backLink = document.createElement("a");
    backLink.classList.add("back-link");
    backLink.href = ".";
    backLink.rel = "home";
    backLink.textContent = "← Back";
    form.appendChild(backLink);
    const version = document.createElement("span");
    version.classList.add("version-number");
    version.textContent = "Version ";
    const versionLink = document.createElement("a");
    versionLink.href = "docs/changelog.html#v" + spyCardsVersionPrefix;
    versionLink.textContent = spyCardsVersionPrefix;
    version.appendChild(versionLink);
    form.appendChild(version);
    const h1 = document.createElement("h1");
    h1.textContent = "Match Viewer";
    form.appendChild(h1);
    const prompt = document.createElement("label");
    prompt.textContent = "Match Recording Code: ";
    const codeEntry = document.createElement("textarea");
    codeEntry.required = true;
    prompt.appendChild(codeEntry);
    form.appendChild(prompt);
    codeEntry.addEventListener("input", function (e) {
        const isShort = codeEntry.value.length <= 29;
        codeEntry.classList.toggle("short", isShort);
        codeEntry.style.height = "";
        if (!isShort) {
            codeEntry.style.height = (codeEntry.scrollHeight + 48) + "px";
        }
    });
    const settingsPanel = document.createElement("div");
    settingsPanel.classList.add("settings");
    const autoPlayLabel = document.createElement("label");
    const autoPlayCheckbox = document.createElement("input");
    autoPlayCheckbox.type = "checkbox";
    autoPlayCheckbox.checked = !!autoPlay;
    autoPlayCheckbox.addEventListener("input", (e) => {
        setAutoPlay(autoPlayCheckbox.checked);
    });
    autoPlayLabel.appendChild(autoPlayCheckbox);
    autoPlayLabel.appendChild(document.createTextNode(" Auto-Play"));
    settingsPanel.appendChild(autoPlayLabel);
    form.appendChild(settingsPanel);
    const submitButton = document.createElement("button");
    submitButton.textContent = "Watch";
    submitButton.addEventListener("click", function (e) {
        e.preventDefault();
        codeEntry.setCustomValidity("");
        if (!codeEntry.value) {
            if (autoPlay) {
                setAutoPlay("random");
                autoPlayLoop();
                return;
            }
            codeEntry.reportValidity();
            return;
        }
        const code = codeEntry.value.replace(/\s+/g, "");
        let dataPromise;
        // max ID length is 18 bytes -> 29 Base32 characters
        // minimum raw match recording length is well over that
        if (code.length <= 29) {
            let valid;
            try {
                valid = CrockfordBase32.decode(code).length > 8;
            }
            catch (ex) {
                valid = false;
            }
            if (!valid) {
                codeEntry.setCustomValidity("This is not a valid match recording code.");
                codeEntry.reportValidity();
                return;
            }
            dataPromise = fetchUploadedRecording(code);
        }
        else {
            try {
                dataPromise = Promise.resolve(Base64.decode(code));
            }
            catch (ex) {
                codeEntry.setCustomValidity("Code is invalid.");
                codeEntry.reportValidity();
                return;
            }
        }
        ignoreHashChange = true;
        location.hash = code;
        if (autoPlay) {
            setAutoPlay("single");
        }
        codeEntry.disabled = true;
        submitButton.disabled = true;
        submitButton.textContent = "Loading...";
        dataPromise.then(async (rawData) => {
            const { ctx, match } = await processRecording(rawData);
            showMatchViewer(ctx, match);
        }, (error) => {
            codeEntry.disabled = false;
            submitButton.disabled = false;
            submitButton.textContent = "Watch";
            codeEntry.setCustomValidity(error.message);
            codeEntry.reportValidity();
        });
    });
    form.appendChild(submitButton);
    SpyCards.Audio.showUI(form);
    function setupInitialUI(code) {
        document.querySelectorAll(".readme").forEach((el) => {
            SpyCards.UI.remove(el);
        });
        document.body.appendChild(form);
        if (!codeEntry.disabled) {
            codeEntry.value = code;
            codeEntry.dispatchEvent(new InputEvent("input"));
        }
    }
    function fetchUploadedRecording(id) {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", match_recording_base_url + "get/" + id);
        xhr.responseType = "arraybuffer";
        return new Promise((resolve, reject) => {
            xhr.addEventListener("load", function (e) {
                if (xhr.status === 200) {
                    return resolve(new Uint8Array(xhr.response, 0));
                }
                reject("An error occurred while fetching this recording: " + xhr.status + " " + (xhr.statusText === "OK" ? "" : xhr.statusText));
            });
            xhr.addEventListener("error", function (e) {
                reject(new Error("An error occurred while fetching this recording."));
            });
            xhr.send();
        });
    }
    const processingDialog = document.createElement("div");
    processingDialog.classList.add("readme", "recording-viewer", "recording-viewer-processing");
    const processingH1 = document.createElement("h1");
    processingH1.textContent = "Processing...";
    processingDialog.appendChild(processingH1);
    const processingProgress = document.createElement("progress");
    processingDialog.appendChild(processingProgress);
    function getRandomMatchID(except) {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", match_recording_base_url + "random?not=" + except.join(","));
        return new Promise((resolve, reject) => {
            xhr.addEventListener("load", function (e) {
                if (xhr.status === 503) {
                    return resolve(null);
                }
                if (xhr.status === 200) {
                    return resolve(xhr.responseText);
                }
                reject(new Error("unexpected error getting random match ID: " + xhr.status));
            });
            xhr.addEventListener("error", function (e) {
                reject(new Error("random match ID request failed due to network error"));
            });
            xhr.send();
        });
    }
    async function autoPlayLoop() {
        const viewed = [];
        let doneViewing = Promise.resolve();
        for (;;) {
            try {
                let nextMatchID = await getRandomMatchID(viewed);
                if (nextMatchID === null) {
                    await doneViewing;
                    ignoreHashChange = true;
                    location.hash = "";
                    setAutoPlay(autoPlay);
                    SpyCards.Audio.Sounds.Fail.play();
                    SpyCards.Audio.Songs.Inside2.play();
                    const noMatchesAvailable = document.createElement("div");
                    noMatchesAvailable.classList.add("readme");
                    const noMatchesH1 = document.createElement("h1");
                    noMatchesH1.textContent = "No Matches Available";
                    const noMatchesDesc = document.createElement("p");
                    noMatchesDesc.textContent = "No more matches are available on the random match playlist. Checking for new matches every 30 seconds...";
                    noMatchesAvailable.appendChild(noMatchesH1);
                    noMatchesAvailable.appendChild(noMatchesDesc);
                    document.body.appendChild(noMatchesAvailable);
                    while (nextMatchID === null) {
                        await sleep(30000);
                        nextMatchID = await getRandomMatchID(viewed);
                    }
                    SpyCards.UI.remove(noMatchesAvailable);
                    SpyCards.Audio.Sounds.Charge.play();
                    SpyCards.Audio.stopMusic();
                }
                viewed.push(nextMatchID);
                const rawData = await fetchUploadedRecording(nextMatchID);
                const { ctx, match } = await processRecording(rawData);
                await doneViewing;
                ignoreHashChange = true;
                location.hash = nextMatchID;
                doneViewing = new Promise((resolve) => showMatchViewer(ctx, match, resolve));
            }
            catch (ex) {
                errorHandler(ex);
            }
        }
    }
    async function processRecording(rawData) {
        SpyCards.UI.remove(form);
        document.body.appendChild(processingDialog);
        processingProgress.value = 0;
        processingProgress.max = 5;
        const data = [].slice.call(rawData, 0);
        function updateProgress() {
            processingProgress.value = (rawData.length - data.length) / rawData.length * 4;
            return new Promise((resolve) => setTimeout(resolve, 1));
        }
        try {
            let defs;
            const formatVersion = SpyCards.Binary.shiftUVarInt(data);
            const spyCardsVersion = [
                SpyCards.Binary.shiftUVarInt(data),
                SpyCards.Binary.shiftUVarInt(data),
                SpyCards.Binary.shiftUVarInt(data)
            ];
            const mode = SpyCards.Binary.shiftUTF8String1(data);
            const perspective = data.shift();
            await updateProgress();
            const cosmeticData = [
                { character: SpyCards.Binary.shiftUTF8String1(data) },
                { character: SpyCards.Binary.shiftUTF8String1(data) }
            ];
            await updateProgress();
            const rematchCount = formatVersion >= 1 ? SpyCards.Binary.shiftUVarInt(data) : 0;
            await updateProgress();
            const sharedSeed = new Uint8Array(data.splice(0, 32));
            await updateProgress();
            const privateSeed = [
                new Uint8Array(data.splice(0, 4)),
                new Uint8Array(data.splice(0, 4))
            ];
            const customCardCount = SpyCards.Binary.shiftUVarInt(data);
            await updateProgress();
            const customCards = [];
            for (let i = 0; i < customCardCount; i++) {
                const len = SpyCards.Binary.shiftUVarInt(data);
                await updateProgress();
                customCards.push(Base64.encode(new Uint8Array(data.splice(0, len))));
                await updateProgress();
            }
            const packedVersion = (spyCardsVersion[0] << 16) | (spyCardsVersion[1] << 8) | spyCardsVersion[2];
            if (customCardCount) {
                const parsed = await SpyCards.parseCustomCards(customCards.join(","), "parse-variant");
                parsed.packedVersion = packedVersion;
                defs = new SpyCards.CardDefs(parsed);
            }
            else {
                defs = new SpyCards.CardDefs({
                    cards: [],
                    customCardsRaw: [],
                    packedVersion
                });
            }
            SpyCards.SpoilerGuard.banSpoilerCards(defs, new Uint8Array(0));
            const initialDeck = [];
            for (let i = 0; i < 2; i++) {
                const len = SpyCards.Binary.shiftUVarInt(data);
                await updateProgress();
                const deckCode = CrockfordBase32.encode(new Uint8Array(data.splice(0, len)));
                initialDeck.push(SpyCards.Decks.decode(defs, deckCode));
                await updateProgress();
            }
            const turnCount = SpyCards.Binary.shiftUVarInt(data);
            const turns = [];
            for (let i = 0; i < turnCount; i++) {
                const turn = {
                    seed: new Uint8Array(data.splice(0, 8)),
                    seed2: formatVersion >= 1 ? new Uint8Array(data.splice(0, 8)) : null,
                    ready: [
                        SpyCards.Binary.shiftUVarInt(data),
                        SpyCards.Binary.shiftUVarInt(data),
                    ]
                };
                turns.push(turn);
                await updateProgress();
            }
            if (data.length) {
                throw new Error("extra data after end");
            }
            const match = {
                formatVersion,
                spyCardsVersion,
                mode,
                perspective,
                cosmeticData,
                rematchCount,
                sharedSeed,
                privateSeed,
                customCards,
                initialDeck,
                turns
            };
            match.stage = new SpyCards.TheRoom.Stage();
            match.stage.setFlipped(perspective === 2);
            match.audience = [];
            for (let i = 0; i < 2; i++) {
                match.cosmeticData[i].player = new SpyCards.TheRoom.Player((i + 1), match.cosmeticData[i].character);
            }
            const scg = new SecureCardGame({ forReplay: match.spyCardsVersion });
            scg.seed = new Uint8Array(match.sharedSeed);
            const ctx = {
                net: null,
                player: match.perspective || 1,
                scg: scg,
                ui: {},
                matchData: new SpyCards.MatchData(),
                defs: defs,
                state: new SpyCards.MatchState(defs),
                fx: {
                    async coin() { },
                    async numb() { },
                    async setup() { },
                    async multiple() { },
                    async assist() { },
                    async beforeProcessEffect() { },
                    async afterProcessEffect() { },
                }
            };
            ctx.matchData.customModeName = mode;
            ctx.matchData.rematchCount = rematchCount;
            ctx.state.deck[0] = match.initialDeck[0].map((c) => ({
                card: c,
                back: c.getBackID(),
                player: 1
            }));
            ctx.state.deck[1] = match.initialDeck[1].map((c) => ({
                card: c,
                back: c.getBackID(),
                player: 2
            }));
            ctx.state.backs[0] = match.initialDeck[0].map((c) => c.getBackID());
            ctx.state.backs[1] = match.initialDeck[1].map((c) => c.getBackID());
            ctx.state.players[0] = match.cosmeticData[0].player;
            ctx.state.players[1] = match.cosmeticData[1].player;
            const effectProcessor = new SpyCards.Processor(ctx, new SpyCards.ProcessorCallbacks());
            ctx.effect = effectProcessor.effect;
            for (let turn of match.turns) {
                ctx.state.turn++;
                await scg.initTurnSeed(turn.seed, match.privateSeed[0], match.privateSeed[1]);
                turn.hpBefore = [ctx.state.hp[0], ctx.state.hp[1]];
                turn.handBefore = [
                    ctx.state.hand[0].map((c) => c.card),
                    ctx.state.hand[1].map((c) => c.card)
                ];
                turn.deckBefore = [
                    ctx.state.deck[0].map((c) => c.card),
                    ctx.state.deck[1].map((c) => c.card)
                ];
                await effectProcessor.shuffleAndDraw(true);
                for (let player = 0; player < 2; player++) {
                    for (let card of ctx.state.discard[player]) {
                        ctx.state.deck[player].push(card);
                        ctx.state.backs[player].push(card.card.getBackID());
                    }
                    ctx.state.discard[player] = [];
                }
                for (let player = 0; player < 2; player++) {
                    ctx.state.played[player].length = 0;
                    for (let i = 4; i >= 0; i--) {
                        if (turn.ready[player] & (1 << i)) {
                            const card = ctx.state.hand[player].splice(i, 1)[0];
                            ctx.state.played[player].unshift({
                                card: card.card,
                                back: card.back,
                                effects: card.card.effects.slice(0),
                                player: (player + 1)
                            });
                            ctx.state.discard[player].unshift(card);
                        }
                    }
                }
                turn.setup = [[], []];
                for (let player = 0; player < 2; player++) {
                    while (ctx.state.setup[player].length) {
                        const setup = ctx.state.setup[player].shift();
                        turn.setup[player].unshift({
                            card: setup.card,
                            back: setup.card.getBackID(),
                            player: (player + 1),
                            effects: setup.effects.slice(0),
                            originalDesc: setup.originalDesc
                        });
                        ctx.state.played[player].unshift({
                            card: setup.card,
                            back: setup.card.getBackID(),
                            setup: true,
                            originalDesc: setup.originalDesc,
                            player: (player + 1),
                            effects: setup.effects.slice(0)
                        });
                    }
                }
                if (match.formatVersion >= 1) {
                    await scg.whenConfirmedTurn(turn.seed2);
                }
                await effectProcessor.processRound();
                processingProgress.value = 4 + (ctx.state.turn / match.turns.length);
            }
            SpyCards.UI.remove(processingDialog);
            return { ctx, match };
        }
        catch (ex) {
            errorHandler(ex);
            throw ex;
        }
    }
    async function showMatchViewer(ctx, match, done) {
        document.querySelectorAll(".game-log").forEach((el) => {
            SpyCards.UI.remove(el);
        });
        document.body.appendChild(ctx.state.gameLog.el.cloneNode(true));
        await match.stage.init();
        match.audience = await match.stage.createAudience(match.sharedSeed, match.rematchCount);
        for (let p of match.cosmeticData) {
            if (p) {
                match.stage.setPlayer(p.player);
            }
        }
        SpyCards.Audio.Sounds.BattleStart0.play();
        if (!SpyCards.Audio.playingMusic()) {
            if (match.mode === "custom")
                SpyCards.Audio.Songs.Bounty.play(0.5);
            else
                SpyCards.Audio.Songs.Miniboss.play(0.5);
        }
        SpyCards.UI.html.classList.add("in-match", "in-recorded-match");
        if (autoPlay) {
            SpyCards.UI.html.classList.add("auto-play");
        }
        const viewerElements = [];
        const controlPanel = document.createElement("div");
        controlPanel.classList.add("play-online", "active");
        document.body.appendChild(controlPanel);
        viewerElements.push(controlPanel);
        ctx.ui.form = controlPanel;
        const roundInput = document.createElement("input");
        roundInput.type = "range";
        roundInput.min = "-1";
        roundInput.max = String(match.turns.length - 1);
        roundInput.value = "-1";
        const roundSelect = document.createElement("button");
        roundSelect.textContent = "Show Round Selector";
        const roundNext = document.createElement("button");
        roundNext.textContent = "Next Round";
        const roundLabel = document.createElement("label");
        const roundNumber = document.createElement("span");
        roundLabel.appendChild(roundNumber);
        roundLabel.appendChild(roundInput);
        roundLabel.appendChild(roundSelect);
        roundLabel.appendChild(roundNext);
        roundInput.style.display = "none";
        roundLabel.classList.add("round-number");
        roundNext.addEventListener("click", async (e) => {
            e.preventDefault();
            await animateRound();
            if (roundInput.valueAsNumber + 1 < match.turns.length) {
                roundInput.valueAsNumber++;
                roundInput.dispatchEvent(new InputEvent("input"));
            }
            else {
                const won = processor.checkWinner();
                const winner = ctx.state.players[won - 1];
                const loser = ctx.state.players[2 - won];
                SpyCards.UI.remove(myHand);
                SpyCards.UI.remove(theirHand);
                roundNumber.textContent = " Wins!";
                const winnerName = document.createElement("span");
                if (winner) {
                    winnerName.textContent = winner.name; // TODO
                }
                roundNumber.insertBefore(winnerName, roundNumber.firstChild);
                if (loser) {
                    loser.becomeAngry(-1);
                }
                SpyCards.Audio.stopMusic();
                if (won === match.perspective) {
                    SpyCards.Audio.Sounds.CrowdClap.play();
                    for (let a of match.audience) {
                        a.startCheer(-1);
                    }
                }
                else {
                    SpyCards.Audio.Sounds.CrowdGaspSlow.play();
                }
                if (done) {
                    await sleep(5000);
                    await match.stage.deinit();
                    SpyCards.UI.html.classList.remove("in-match", "in-recorded-match");
                    for (let el of viewerElements) {
                        SpyCards.UI.remove(el);
                    }
                    done();
                }
                else {
                    roundInput.style.display = "";
                    SpyCards.UI.remove(roundNext);
                    SpyCards.UI.remove(roundSelect);
                    autoPlay = false;
                    SpyCards.UI.html.classList.remove("auto-play");
                }
            }
        });
        roundSelect.addEventListener("click", (e) => {
            e.preventDefault();
            roundInput.style.display = "";
            SpyCards.UI.remove(roundNext);
            SpyCards.UI.remove(roundSelect);
        });
        let showPromise = Promise.resolve();
        roundInput.addEventListener("input", (e) => {
            if (roundInput.valueAsNumber === -1) {
                showPromise = showPromise.then(showOverview);
            }
            else {
                const num = roundInput.valueAsNumber;
                showPromise = showPromise.then(() => showRound(num));
            }
        });
        controlPanel.appendChild(roundLabel);
        if (autoPlay) {
            roundNext.style.display = "none";
            roundSelect.style.display = "none";
        }
        const myHand = document.createElement("div");
        myHand.classList.add("my-hand");
        document.body.appendChild(myHand);
        viewerElements.push(myHand);
        const theirHand = document.createElement("div");
        theirHand.classList.add("their-hand");
        document.body.appendChild(theirHand);
        viewerElements.push(theirHand);
        const hands = [
            ctx.player === 1 ? myHand : theirHand,
            ctx.player === 2 ? myHand : theirHand
        ];
        const myField = SpyCards.Fx.myField;
        const theirField = SpyCards.Fx.theirField;
        const fields = [
            ctx.player === 1 ? myField : theirField,
            ctx.player === 2 ? myField : theirField
        ];
        SpyCards.Fx.myHPEl.style.display = "none";
        document.body.appendChild(SpyCards.Fx.myHPEl);
        viewerElements.push(SpyCards.Fx.myHPEl);
        SpyCards.Fx.theirHPEl.style.display = "none";
        document.body.appendChild(SpyCards.Fx.theirHPEl);
        viewerElements.push(SpyCards.Fx.theirHPEl);
        const hpUI = [
            ctx.player === 1 ? SpyCards.Fx.myHPEl : SpyCards.Fx.theirHPEl,
            ctx.player === 2 ? SpyCards.Fx.myHPEl : SpyCards.Fx.theirHPEl
        ];
        const processor = new SpyCards.Processor(ctx, SpyCards.Fx.getCallbacks(ctx, match.audience));
        ctx.effect = processor.effect;
        ctx.fx = SpyCards.Fx.fx;
        const matchOverview = document.createElement("div");
        matchOverview.classList.add("match-overview");
        viewerElements.push(matchOverview);
        const overviewInner = document.createElement("div");
        overviewInner.classList.add("match-overview-inner");
        matchOverview.appendChild(overviewInner);
        const overviewVersus = document.createElement("div");
        overviewVersus.classList.add("versus");
        const overviewP1 = document.createElement("img"); // TODO
        const overviewP2 = document.createElement("img"); // TODO
        if (match.perspective === 2) {
            matchOverview.classList.add("perspective-p2");
            overviewInner.appendChild(SpyCards.Decks.createDisplay(ctx.defs, match.initialDeck[1]));
            overviewInner.appendChild(overviewVersus);
            overviewVersus.appendChild(overviewP2);
            overviewVersus.appendChild(overviewP1);
            overviewInner.appendChild(SpyCards.Decks.createDisplay(ctx.defs, match.initialDeck[0]));
        }
        else {
            matchOverview.classList.add("perspective-p1");
            overviewInner.appendChild(SpyCards.Decks.createDisplay(ctx.defs, match.initialDeck[0]));
            overviewInner.appendChild(overviewVersus);
            overviewVersus.appendChild(overviewP1);
            overviewVersus.appendChild(overviewP2);
            overviewInner.appendChild(SpyCards.Decks.createDisplay(ctx.defs, match.initialDeck[1]));
        }
        async function cleanup() {
            SpyCards.UI.remove(matchOverview);
            for (let hand of hands) {
                SpyCards.UI.clear(hand);
            }
            for (let field of fields) {
                SpyCards.UI.clear(field);
            }
        }
        async function showOverview() {
            roundNumber.textContent = "Match Overview";
            await cleanup();
            for (let hp of hpUI) {
                hp.style.display = "none";
            }
            document.body.appendChild(matchOverview);
            if (autoPlay) {
                await sleep(5000);
                roundNext.click();
            }
        }
        async function showRound(num) {
            ctx.state.turn = num + 1;
            roundNumber.textContent = "Round " + (num + 1);
            await cleanup();
            const turn = match.turns[num];
            await ctx.scg.initTurnSeed(turn.seed, match.privateSeed[ctx.player - 1], match.privateSeed[2 - ctx.player]);
            for (let player = 0; player < 2; player++) {
                hpUI[player].style.display = "";
                hpUI[player].textContent = String(turn.hpBefore[player]);
                ctx.state.hp[player] = turn.hpBefore[player];
                ctx.state.setup[player] = turn.setup[player].map((c) => ({
                    card: c.card,
                    effects: c.effects.slice(0),
                    originalDesc: c.originalDesc
                }));
                ctx.state.deck[player] = turn.deckBefore[player].map((c) => ({
                    card: c,
                    back: c.getBackID(),
                    player: (player + 1)
                }));
                ctx.state.backs[player] = turn.deckBefore[player].map((c) => c.getBackID());
                ctx.state.hand[player] = turn.handBefore[player].map((c) => ({
                    card: c,
                    back: c.getBackID(),
                    player: (player + 1)
                }));
            }
            await processor.shuffleAndDraw(true);
            for (let player = 0; player < 2; player++) {
                ctx.state.played[player].length = 0;
                const hand = ctx.state.hand[player];
                for (let i = 0; i < hand.length; i++) {
                    const el = hand[i].card.createEl(ctx.defs);
                    el.classList.add("in-hand");
                    if (turn.ready[player] & (1 << i)) {
                        el.classList.add("tapped");
                        const fieldEl = hand[i].card.createEl(ctx.defs);
                        fields[player].appendChild(fieldEl);
                        ctx.state.played[player].push({
                            card: hand[i].card,
                            back: hand[i].back,
                            el: fieldEl,
                            player: (player + 1),
                            effects: hand[i].card.effects.slice(0)
                        });
                    }
                    hands[player].appendChild(el);
                }
                while (ctx.state.setup[player].length) {
                    const setup = ctx.state.setup[player].pop();
                    const el = setup.card.createEl(ctx.defs);
                    el.classList.add("setup");
                    if (!setup.originalDesc) {
                        el.querySelectorAll(".card-desc > *").forEach((e) => {
                            SpyCards.UI.remove(e);
                        });
                        el.querySelector(".card-desc").appendChild(setup.effects[0].createEl(ctx.defs, setup.card));
                    }
                    fields[player].insertBefore(el, fields[player].firstChild);
                    ctx.state.played[player].unshift({
                        card: setup.card,
                        back: setup.card.getBackID(),
                        el: el,
                        setup: true,
                        originalDesc: setup.originalDesc,
                        player: (player + 1),
                        effects: setup.effects.slice(0)
                    });
                }
            }
            document.body.appendChild(myHand);
            document.body.appendChild(theirHand);
            if (match.formatVersion >= 1) {
                await ctx.scg.whenConfirmedTurn(turn.seed2);
            }
            if (autoPlay) {
                await sleep(1000);
                roundNext.click();
            }
        }
        async function animateRound() {
            if (roundInput.valueAsNumber === -1) {
                return;
            }
            try {
                roundNext.disabled = true;
                roundInput.disabled = true;
                myHand.classList.add("offscreen");
                theirHand.classList.add("offscreen");
                await processor.processRound();
                myHand.classList.remove("offscreen");
                theirHand.classList.remove("offscreen");
                roundNext.disabled = false;
                roundInput.disabled = false;
            }
            catch (ex) {
                errorHandler(ex, ctx.state);
                throw ex;
            }
        }
        showPromise = showPromise.then(showOverview);
    }
    let ignoreHashChange = false;
    function handleHashChange() {
        if (ignoreHashChange) {
            ignoreHashChange = false;
            return;
        }
        setupInitialUI(location.hash.substr(1));
    }
    addEventListener("hashchange", handleHashChange);
    if (location.hash && autoPlay !== "random") {
        handleHashChange();
        if (autoPlay && autoPlay !== "single") {
            setAutoPlay("single");
        }
        if (autoPlay) {
            submitButton.click();
        }
    }
    else {
        if (autoPlay === "random" || autoPlay === true) {
            ignoreHashChange = true;
            location.hash = "";
            setAutoPlay("random");
            autoPlayLoop();
        }
        else {
            setAutoPlay(false);
            setupInitialUI("");
        }
    }
})();
