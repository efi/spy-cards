//go:generate stringer -type ConnState,LogicalState -trimprefix State

package matchnet

import (
	"log"

	"git.lubar.me/ben/spy-cards/card"
	"nhooyr.io/websocket"
)

// ConnState is the connection state.
type ConnState uint64

// Connection states.
const (
	StateNone ConnState = iota
	StateHandshakeWait
	StateHandshakeClientWait
	StateHandshakeHostWait
	StateRTCConnecting
	StateRTCConnected
	StateRTCDisconnected
	StateRTCFailed
	StateOpponentWantsRelay
	StateOfferedRelay
	StateUsingRelay
	StateRelayFailed
)

// LogicalState is the application logic state.
type LogicalState uint64

// Application logic states.
const (
	StateUninitialized LogicalState = iota
	StateSecureInitializing
	StateSecureReady
	StateMatchReady
)

func (gc *GameConn) setState(s ConnState) {
	gc.stateLock.Lock()

	if (s == StateRTCConnecting || s == StateRTCConnected || s == StateRTCDisconnected || s == StateRTCFailed) && gc.connState >= StateOpponentWantsRelay {
		log.Println("DEBUG: skipping connection state change", gc.connState, "->", s)
		gc.stateLock.Unlock()

		return
	}

	if s == StateRelayFailed || gc.connState == StateRelayFailed {
		s = StateRelayFailed
	} else if (s == StateOpponentWantsRelay && gc.connState == StateOfferedRelay) || (s == StateOfferedRelay && gc.connState == StateOpponentWantsRelay) || gc.connState == StateUsingRelay {
		log.Println("DEBUG: rewriting connection state", gc.connState, "->", s, "->", StateUsingRelay)
		s = StateUsingRelay
	}

	if gc.connState != StateUsingRelay && s == StateUsingRelay && gc.logicalState == StateUninitialized {
		gc.logicalState = StateSecureInitializing
		go gc.initSecure()
	}

	log.Println("DEBUG: connection state change", gc.connState, "->", s)
	gc.connState = s
	gc.stateCond.Broadcast()
	gc.stateLock.Unlock()
}

func (gc *GameConn) setLogicalState(s LogicalState) {
	gc.stateLock.Lock()
	log.Println("DEBUG: logical state change", gc.logicalState, "->", s)
	gc.logicalState = s
	gc.stateCond.Broadcast()
	gc.stateLock.Unlock()
}

// State returns the network and logical state of the connection.
func (gc *GameConn) State() (ConnState, LogicalState) {
	gc.stateLock.Lock()
	cs, ls := gc.connState, gc.logicalState
	gc.stateLock.Unlock()

	return cs, ls
}

// ConnStateExcept waits until the connection state is not one of the
// arguments, then returns the connection state.
func (gc *GameConn) ConnStateExcept(except ...ConnState) ConnState {
	gc.stateLock.Lock()

	for {
		found := false

		for _, s := range except {
			if s == gc.connState {
				found = true

				break
			}
		}

		if !found {
			break
		}

		gc.stateCond.Wait()
	}

	s := gc.connState

	gc.stateLock.Unlock()

	return s
}

// AwaitMatchReady waits until the logical state is StateMatchReady.
func (gc *GameConn) AwaitMatchReady() {
	gc.stateLock.Lock()

	for gc.logicalState != StateMatchReady {
		gc.stateCond.Wait()
	}

	gc.stateLock.Unlock()
}

// SignalClosed returns the status code and reason for the SignalConn closing.
func (gc *GameConn) SignalClosed() (websocket.StatusCode, string) {
	gc.stateLock.Lock()
	code, reason := gc.closeCode, gc.closeReason
	gc.stateLock.Unlock()

	return code, reason
}

func (gc *GameConn) onCloseCode(code websocket.StatusCode, reason string) {
	gc.stateLock.Lock()

	gc.closeCode, gc.closeReason = code, reason
	log.Println("DEBUG: signal conn closed:", code, reason)

	if gc.connState == StateUsingRelay {
		gc.connState = StateRelayFailed
		gc.stateCond.Broadcast()
	}

	gc.stateLock.Unlock()
}

// BeginTurn synchronizes the per-turn seed, then sends on ch.
func (gc *GameConn) BeginTurn(ch chan<- *card.TurnData) {
	data, err := gc.SCG.BeginTurn(gc.exchangeData)
	if err != nil {
		log.Println("ERROR: synchronizing start of turn:", err)
	}

	ch <- data
}
