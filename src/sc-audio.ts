module SpyCards.Audio {
	const audioDebug = new URLSearchParams(location.search).has("audioDebug") ? (...message: any[]) => console.debug("AUDIO:", ...message) : (...message: any[]) => { };

	export const actx: AudioContext = new (window.AudioContext || (window as any).webkitAudioContext)();
	const musicGain = actx.createGain();
	musicGain.connect(actx.destination);
	export const musicFFT = actx.createAnalyser();
	musicFFT.connect(musicGain);
	const soundsGain = actx.createGain();
	soundsGain.connect(actx.destination);

	addEventListener("click", function () {
		if (actx.state === "suspended") {
			actx.resume();
		}
	}, true);

	export var musicVolume: number = 0;
	export var soundsVolume: number = 0;
	export var ignoreAudio: number = 0;
	let activeMusic: AudioBufferSourceNode;
	let forcedMusic: AudioBufferSourceNode;

	export function adoptForcedMusic() {
		if (activeMusic) {
			activeMusic.stop();
		}
		activeMusic = forcedMusic;
		forcedMusic = null;
		if (!musicVolume) {
			stopMusic();
		}
	}

	export function playingMusic() {
		return !!activeMusic;
	}

	export var startFetchAudio: () => void;
	const fetchAudioPromise = new Promise<void>((resolve) => startFetchAudio = resolve);

	export function setVolume(music: number, sounds: number) {
		musicVolume = music;
		soundsVolume = sounds;

		const settings = loadSettings();
		settings.audio.music = music;
		settings.audio.sounds = sounds;
		saveSettings(settings);

		musicGain.gain.setValueAtTime(music, actx.currentTime);
		soundsGain.gain.setValueAtTime(sounds, actx.currentTime);

		if (!music) {
			stopMusic();
		}
	}

	addEventListener("spy-cards-settings-changed", () => {
		const s = loadSettings().audio;
		musicVolume = s.music;
		soundsVolume = s.sounds;
		musicGain.gain.setValueAtTime(musicVolume, actx.currentTime);
		soundsGain.gain.setValueAtTime(soundsVolume, actx.currentTime);
	});

	export function showUI(form: HTMLElement) {
		const initialSettings = loadSettings().audio;
		musicVolume = initialSettings.music;
		soundsVolume = initialSettings.sounds;
		musicGain.gain.setValueAtTime(musicVolume, actx.currentTime);
		soundsGain.gain.setValueAtTime(soundsVolume, actx.currentTime);

		const reenableAudio = SpyCards.UI.button("Enable Audio", ["enable-audio", "audio-error"], async () => {
			await actx.resume();
			updateUI();
			Sounds.Confirm.play();
		});
		const enableAudio = SpyCards.UI.button("Enable Audio", ["enable-audio"], async () => {
			await actx.resume();
			setVolume(0.6, 0.6);
			updateUI();
			Sounds.Confirm.play();
		});
		const enableAudioMusic = SpyCards.UI.button("Music", ["enable-audio", "split", "split-1"], () => {
			setVolume(musicVolume ? 0 : 0.6, soundsVolume);
			updateUI();
		});
		const enableAudioSound = SpyCards.UI.button("Sounds", ["enable-audio", "split", "split-2"], () => {
			setVolume(musicVolume, soundsVolume ? 0 : 0.6);
			updateUI();
			Sounds.Confirm.play();
		});

		function updateUI() {
			SpyCards.UI.remove(reenableAudio);
			SpyCards.UI.remove(enableAudio);
			SpyCards.UI.remove(enableAudioMusic);
			SpyCards.UI.remove(enableAudioSound);

			if (actx.state === "suspended" && (musicVolume || soundsVolume)) {
				form.appendChild(reenableAudio);
				actx.resume().then(updateUI);
			} else if (musicVolume || soundsVolume) {
				if (window.requestIdleCallback) {
					requestIdleCallback(startFetchAudio);
				}

				enableAudioMusic.classList.toggle("enabled", musicVolume !== 0);
				enableAudioMusic.setAttribute("aria-pressed", String(musicVolume !== 0));
				enableAudioSound.classList.toggle("enabled", soundsVolume !== 0);
				enableAudioSound.setAttribute("aria-pressed", String(soundsVolume !== 0));
				form.appendChild(enableAudioMusic);
				form.appendChild(enableAudioSound);
			} else {
				enableAudio.setAttribute("aria-pressed", "false");
				form.appendChild(enableAudio);
			}
		}
		updateUI();
	}

	async function fetchAudio(name: string): Promise<AudioBuffer> {
		audioDebug("waiting to fetch audio: ", name);
		await fetchAudioPromise;
		audioDebug("fetching audio: ", name);
		const response = await fetch(name.indexOf("/") === -1 ? "audio/" + name + ".opus" : name);
		audioDebug("received response for audio: ", name);
		const body = await response.arrayBuffer();
		audioDebug("finished downloading audio: ", name);
		audioDebug("audio context state before decoding ", name, ": ", actx.state);
		if (actx.state === "suspended") {
			audioDebug("waiting to resume audio context for: ", name);
			await actx.resume();
			audioDebug("resumed audio context for: ", name);
		}
		const bufferPromise = actx.decodeAudioData(body);
		audioDebug("audio decoding promise for ", name, ": ", bufferPromise);
		const buffer = await bufferPromise;
		audioDebug("decoded audio: ", name);
		return buffer;
	}

	export class Sound {
		readonly name: string;
		readonly buffer: Promise<AudioBuffer>;
		readonly pitch: number;
		readonly volume: number;
		private gain: GainNode;
		private musicGain: GainNode;

		constructor(name: string, pitch: number = 1, volume: number = 1) {
			this.name = name;
			this.buffer = fetchAudio(name);
			this.pitch = pitch;
			this.volume = volume;
		}

		async createSource(): Promise<AudioBufferSourceNode> {
			startFetchAudio();

			const source = actx.createBufferSource();
			source.buffer = await this.buffer;
			source.playbackRate.setValueAtTime(this.pitch, actx.currentTime);
			return source;
		}

		async preload(): Promise<void> {
			startFetchAudio();

			await this.buffer;
		}

		async play(delay: number = 0, overridePitch?: number, overrideVolume?: number) {
			if (!soundsVolume) {
				return;
			}
			if (ignoreAudio) {
				return;
			}

			let dest = soundsGain;
			if (this.volume !== 1) {
				if (!this.gain) {
					this.gain = actx.createGain();
					this.gain.connect(dest);
					this.gain.gain.setValueAtTime(this.volume, actx.currentTime);
				}
				dest = this.gain;
			}
			if (overrideVolume) {
				const gain = actx.createGain();
				gain.connect(dest);
				gain.gain.setValueAtTime(overrideVolume, actx.currentTime);
				dest = gain;
			}

			const src = await this.createSource();
			if (overridePitch) {
				src.playbackRate.setValueAtTime(overridePitch * this.pitch, actx.currentTime);
			}
			src.connect(dest);
			src.start(actx.currentTime + delay);
		}

		async playAsMusic(delay: number = 0) {
			if (!musicVolume) {
				return;
			}
			if (ignoreAudio) {
				return;
			}

			let dest: AudioNode = musicFFT;
			if (this.volume !== 1) {
				if (!this.musicGain) {
					this.musicGain = actx.createGain();
					this.musicGain.connect(dest);
					this.musicGain.gain.setValueAtTime(this.volume, actx.currentTime);
				}
				dest = this.musicGain;
			}

			stopMusic();

			const src = await this.createSource();
			src.connect(dest);
			src.start(actx.currentTime + delay);

			activeMusic = src;
		}
	}

	export class Music extends Sound {
		readonly start: number;
		readonly end: number;

		constructor(name: string, start: number, end: number) {
			super(name);
			this.start = start;
			this.end = end;
		}

		async createSource(): Promise<AudioBufferSourceNode> {
			const source = await super.createSource();
			source.loop = true;
			source.loopStart = this.start;
			source.loopEnd = this.end;
			return source;
		}

		async play(delay: number = 0) {
			if (!musicVolume) {
				return;
			}
			if (ignoreAudio) {
				return;
			}

			stopMusic();

			const src = await this.createSource();
			src.connect(musicFFT);
			src.start(actx.currentTime + delay);

			activeMusic = src;
		}

		async forcePlay(delay: number = 0) {
			if (forcedMusic) {
				forcedMusic.stop();
			}
			if (ignoreAudio) {
				return;
			}

			const src = await this.createSource();
			src.connect(musicFFT);
			src.start(actx.currentTime + delay);
			forcedMusic = src;
		}
	}

	export function stopMusic() {
		if (activeMusic) {
			activeMusic.stop();
		}

		activeMusic = null;
	}

	export const Sounds = {
		AtkFail: new Sound("AtkFail"),
		AtkSuccess: new Sound("AtkSuccess"),
		BattleStart0: new Sound("BattleStart0"),
		Buzzer: new Sound("Buzzer"),
		CardSound2: new Sound("CardSound2", 1.2, 0.5),
		Charge: new Sound("Charge"),
		Coin: new Sound("Coin"),
		Confirm: new Sound("Confirm"),
		Confirm1: new Sound("Confirm1", 0.4, 0.5),
		CrowdCheer2: new Sound("CrowdCheer2", 1.2),
		CrowdClap: new Sound("CrowdClap"),
		CrowdGasp: new Sound("CrowdGasp"),
		CrowdGaspSlow: new Sound("CrowdGasp", 0.75),
		Damage0: new Sound("Damage0"),
		Death3: new Sound("Death3"),
		Fail: new Sound("Fail"),
		Heal: new Sound("Heal"),
		Lazer: new Sound("Lazer"),
		PageFlip: new Sound("PageFlip"),
		PageFlipCancel: new Sound("PageFlip", 0.7),
		Toss11: new Sound("Toss11"),
		FBCountdown: new Sound("FBCountdown"),
		FBDeath: new Sound("FBDeath"),
		FBFlower: new Sound("FBFlower"),
		FBGameOver: new Sound("FBGameOver"),
		FBPoint: new Sound("FBPoint"),
		FBStart: new Sound("FBStart"),
		MiteKnightIntro: new Sound("MiteKnightIntro"),
		MKDeath: new Sound("MKDeath"),
		MKGameOver: new Sound("MKGameOver"),
		MKHit: new Sound("MKHit"),
		MKHit2: new Sound("MKHit2"),
		MKKey: new Sound("MKKey"),
		MKOpen: new Sound("MKOpen"),
		MKPotion: new Sound("MKPotion"),
		MKShield: new Sound("MKShield"),
		MKStairs: new Sound("MKStairs"),
		MKWalk: new Sound("MKWalk"),
		PeacockSpiderNPCSummonSuccess: new Sound("PeacockSpiderNPCSummonSuccess"),
		Shot2: new Sound("Shot2"),
	};
	export const Songs = {
		Miniboss: new Music("Miniboss", 20.55, 87),
		Bounty: new Music("Bounty", 6.7, 52.5),
		Inside2: new Music("Inside2", 10.3, 72),
		FlyingBee: new Music("FlyingBee", 999, 999),
		MiteKnight: new Music("MiteKnight", 999, 999),
		TermiteLoop: new Music("TermiteLoop", 999, 999),
	};
}
