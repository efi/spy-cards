// Code generated by "stringer -type entityType -trimprefix entity"; DO NOT EDIT.

package miteknight

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[entityAnt-0]
	_ = x[entityWizard-1]
	_ = x[entityFireball-2]
	_ = x[entityRandomEnemy-3]
	_ = x[entityPotion-4]
	_ = x[entityKey-5]
	_ = x[entityDoor-6]
	_ = x[entityPlayer-7]
}

const _entityType_name = "AntWizardFireballRandomEnemyPotionKeyDoorPlayer"

var _entityType_index = [...]uint8{0, 3, 9, 17, 28, 34, 37, 41, 47}

func (i entityType) String() string {
	if i >= entityType(len(_entityType_index)-1) {
		return "entityType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _entityType_name[_entityType_index[i]:_entityType_index[i+1]]
}
