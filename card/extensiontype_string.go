// Code generated by "stringer -type ExtensionType -trimprefix Extension"; DO NOT EDIT.

package card

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[ExtensionSetStage-0]
	_ = x[ExtensionSetMusic-1]
}

const _ExtensionType_name = "SetStageSetMusic"

var _ExtensionType_index = [...]uint8{0, 8, 16}

func (i ExtensionType) String() string {
	if i >= ExtensionType(len(_ExtensionType_index)-1) {
		return "ExtensionType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _ExtensionType_name[_ExtensionType_index[i]:_ExtensionType_index[i+1]]
}
