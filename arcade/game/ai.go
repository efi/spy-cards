package game

import (
	"image/color"
	"math"

	"git.lubar.me/ben/spy-cards/arcade"
	"github.com/BenLubar/convnet/deepqlearn"
	"golang.org/x/xerrors"
)

// AIEnabled causes a neural network to play the game rather than the player.
var AIEnabled bool

// WeightedButton is a button with a weight (1 is normal).
type WeightedButton struct {
	Button arcade.Button
	Weight float64
}

// AIInputDesc describes an input to the AI.
type AIInputDesc struct {
	Name string
	Min  float64
	Max  float64
	Enum map[float64]color.RGBA
}

// AIConfig is the configuration for the AI.
type AIConfig struct {
	NumInput         int
	Press, Toggle    []WeightedButton
	PositionOverride []int
	InputDesc        []AIInputDesc
}

// AIForceButtons forces the AI to hold a set of buttons.
func (s *State) AIForceButtons(buttons ...arcade.Button) {
	s.aiButtons = buttons
	s.aiButtonSpam = false
}

// AIClearButtons returns control of the input to the AI.
func (s *State) AIClearButtons() {
	s.aiButtons = nil
	s.aiButtonSpam = false
}

// AISpamButton forces the AI to press a button every other frame.
func (s *State) AISpamButton(btn arcade.Button) {
	s.aiButtons = []arcade.Button{btn}
	s.aiButtonSpam = true
}

func (s *State) aiInput() []arcade.Button {
	if s.aiButtons != nil {
		if s.aiButtonSpam && s.TickCount%2 == 1 {
			return nil
		}

		return s.aiButtons
	}

	return s.aiOutput
}

func (s *State) initAI() error {
	if !AIEnabled {
		return nil
	}

	cfg := s.Interface.InitAI()
	if cfg == nil {
		return xerrors.Errorf("game: nil AI configuration for game %v", s.Interface.Type())
	}

	if cfg.PositionOverride == nil {
		cfg.PositionOverride = make([]int, cfg.NumInput)

		for i := range cfg.PositionOverride {
			cfg.PositionOverride[i] = i
		}
	}

	s.aiVisWidth = 16 * 19
	s.aiVisHeight = (len(cfg.PositionOverride) + 18) / 19 * 8

	var (
		outputWeights []float64
		totalWeight   float64
	)

	s.aiOutputs = make([][]arcade.Button, (1<<len(cfg.Toggle))*(1+len(cfg.Press)))

	for i := 0; i < 1<<len(cfg.Toggle); i++ {
		for j := -1; j < len(cfg.Press); j++ {
			var (
				weight float64
				output []arcade.Button
			)

			for k := 0; k < len(cfg.Toggle); k++ {
				if i&(1<<k) != 0 {
					output = append(output, cfg.Toggle[k].Button)
					weight += cfg.Toggle[k].Weight
				} else {
					weight++
				}
			}

			if j != -1 {
				output = append(output, cfg.Press[j].Button)
				weight += cfg.Press[j].Weight
			} else {
				weight++
			}

			s.aiOutputs = append(s.aiOutputs, output)
			outputWeights = append(outputWeights, weight)

			totalWeight += weight
		}
	}

	for _, w := range outputWeights {
		totalWeight += w
	}

	opts := deepqlearn.DefaultBrainOptions

	if s.Interface.Type() == arcade.MiteKnight {
		opts.TemporalWindow = 2
	} else {
		opts.TemporalWindow = 15
	}

	opts.LearningStepsBurnin = 120

	opts.RandomActionDistribution = make([]float64, len(outputWeights))

	for i, w := range outputWeights {
		opts.RandomActionDistribution[i] = w / totalWeight
	}

	opts.HiddenLayerSizes = []int{50, 50}

	brain, err := deepqlearn.NewBrain(cfg.NumInput, len(s.aiOutputs), opts)
	if err != nil {
		return xerrors.Errorf("game: failed to initialize AI: %w", err)
	}

	for len(cfg.InputDesc) < cfg.NumInput {
		cfg.InputDesc = append(cfg.InputDesc, AIInputDesc{})
	}

	s.aiBrain = brain
	s.aiConfig = cfg
	s.aiOutput = nil
	s.aiCurInput = nil
	s.aiPrevInput = nil

	return nil
}

func (s *State) updateAI(input []float64, reward float64) {
	if !AIEnabled {
		return
	}

	if !math.IsNaN(reward) {
		s.aiBrain.Backward(reward)
	}

	selectedChoice := s.aiBrain.Forward(input)
	s.aiOutput = s.aiOutputs[selectedChoice]

	s.aiPrevInput = s.aiCurInput
	s.aiCurInput = input
}
