---
title: Discord Landing Page
...

Hi! You're probably thinking something like "WHAT? SPY CARDS ONLINE?!" right now. That's normal.

Here are some links to get you started:

## Wait, I'm not here from Discord!

If you want to talk to us, we're in the [#spy-cards-spoilers](https://discord.gg/bsyVDUE) channel. As the name implies, Spy Cards is very spoiler-heavy, so bail out now if you haven't played Bug Fables yet and are avoiding spoilers.

## Clients

The "clients" for Spy Cards are how you connect and play.

- `SPYC` [Vanilla](..) (this one tries to match Bug Fables as closely as possible)
- `CSTM` [Custom](../custom.html) (this is also the page for the card editor)
- `5280` [Miles Mode](../play.html?mode=miles)
- `KAC0` [Kingdoms and Critters](../play.html?mode=kingcrit)
- `M1G3` [Midge Hell](../play.html?mode=midge)
- `BEAR` [Pierce and Defense Expansion](../play.html?mode=lazybear)
- `WTWC` [Way of the Win Condition](../play.html?mode=wincon)
- `VANP` [Vanilla Plus](../play.html?mode=vanillaplus)
- `(sp)` [Boss Mode](../play.html?mode=bossmode)

For the custom client, only the host needs to have the custom card definitions. They will be sent to whoever joins. All other clients have built-in card definitions.

## NPCs

Nobody online to play at 3 AM? No problem! Just open another tab to the same client and add `?npc` to the end of the URL to have a simulated Bug Fables NPC player. These use the same AI as in the game.

## Decks

Decks you create, whether in the [deck editor](../decks.html) or during a match, get saved in your browser. There is no hard limit on the number of decks you can store.

Clicking on a deck generates a shareable link to it. For example, here are the decks used by [Chuck](../decks.html#4HH0000007VXYZFG84210GG8), [Arie](../decks.html#310J10G84212NANCPAD6K7VW), [Crow](../decks.html#101AXEPH8MCJ8G845G), [Shay](../decks.html#511KHRWE631GRD6K9MMA5840) and [Carmina](../decks.html#3P7T52H8MA5273HG842YF7KR). Carmina uses a [different deck](../decks.html#01H00000000013HSMR) in the tutorial, and all players on Metal Island create new decks for each tournament.

## What's this about 3D Spy Cards?

At the start of a match, you [select a character](../room.html?select). This character shows up during the match. No more being forced to play as leif against 7 possible opponents!

The audience you see is the same audience your opponent sees, too.

## Termacade?!

[Yes](../arcade.html).

## Match Viewer

At the end of a match, you will receive a code that is a recording of the match you just played. You can use these codes directly in the [Match Viewer](../viewer.html), or click the button to upload the recording to the Spy Cards server, which produces a much shorter code.

On the viewer itself, step through the match using the controls at the top, or enable auto play to watch the match in "real time". You can even enable auto play without a recording code to watch random uploaded matches. (No custom matches are in the random playlist, to avoid abuse, but you can still upload and link directly to them.)
