---
title: Data Formats (Spy Cards Recording)
...

- varint FormatVersion (2)
- varint SpyCardsVersionMajor
- varint SpyCardsVersionMinor
- varint SpyCardsVersionPatch
- uint8 ModeLength
- byte\[ModeLength\] ModeName (UTF-8)
- uint8 Perspective (0 = none, 1 = player 1, 2 = player 2)
- CosmeticData Player1CosmeticData
- CosmeticData Player2CosmeticData
- uvarint RematchCount (since version 1)
- varint StartTimestamp (milliseconds since unix epoch, since version 2)
- uvarint ExtraFields (bitfield, since version 2)
    - 0: Player 1 Spoiler Guard (since version 2)
    - 1: Player 2 Spoiler Guard (since version 2)
- byte\[32\] Player1SpoilerGuard (if bit is set, since version 2)
- byte\[32\] Player2SpoilerGuard (if bit is set, since version 2)
- byte\[32\] SharedSeed
- byte\[4\] PrivateSeedPlayer1
- byte\[4\] PrivateSeedPlayer2
- varint NumberOfCustomCards
- CustomCardDef\[NumberOfCustomCards\] CustomCards
- EncodedDeck Player1InitialDeck
- EncodedDeck Player2InitialDeck
- varint NumberOfTurns
- TurnData\[NumberOfTurns\] Turns

# CosmeticData

- uint8 CharacterNameLength
- byte\[CharacterNameLength\] CharacterName (UTF-8)

# CustomCardDef

- varint EncodedLength
- byte\[EncodedLength\] CardData (see [Custom Card](#custom-card); not base64-encoded)

# EncodedDeck

- varint EncodedLength
- byte\[EncodedLength\] DeckData (see [Deck Code](#deck-code); not base32-encoded)

# TurnData

- byte\[8\] TurnSeed
- byte\[8\] TurnSeed2 (since version 1)
- varint Player1Ready (bitfield of cards played from hand)
- varint Player2Ready (bitfield of cards played from hand)
