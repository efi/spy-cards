package card

type basicEntry struct {
	rank  Rank
	index uint8
	name  string
}

var basicData = [128]*basicEntry{
	Zombiant:           {Attacker, 3, "Zombiant"},
	Jellyshroom:        {Attacker, 6, "Jellyshroom"},
	Spider:             {Boss, 0, "Spider"},
	Zasp:               {MiniBoss, 3, "Zasp"},
	Cactiling:          {Effect, 32, "Cactiling"},
	Psicorp:            {Attacker, 10, "Psicorp"},
	Thief:              {Attacker, 12, "Thief"},
	Bandit:             {Attacker, 13, "Bandit"},
	Inichas:            {Effect, 35, "Inichas"},
	Seedling:           {Attacker, 0, "Seedling"},
	Numbnail:           {Effect, 38, "Numbnail"},
	Mothiva:            {MiniBoss, 2, "Mothiva"},
	Acornling:          {Effect, 31, "Acornling"},
	Weevil:             {Effect, 42, "Weevil"},
	VenusBud:           {Effect, 50, "Venus' Bud"},
	Chomper:            {Attacker, 8, "Chomper"},
	AcolyteAria:        {MiniBoss, 1, "Acolyte Aria"},
	Kabbu:              {MiniBoss, 13, "Kabbu"},
	VenusGuardian:      {Boss, 1, "Venus' Guardian"},
	WaspTrooper:        {Attacker, 30, "Wasp Trooper"},
	WaspBomber:         {Effect, 48, "Wasp Bomber"},
	WaspDriller:        {Effect, 49, "Wasp Driller"},
	WaspScout:          {Attacker, 29, "Wasp Scout"},
	Midge:              {Effect, 40, "Midge"},
	Underling:          {Attacker, 1, "Underling"},
	MonsieurScarlet:    {MiniBoss, 12, "Monsieur Scarlet"},
	GoldenSeedling:     {Attacker, 2, "Golden Seedling"},
	ArrowWorm:          {Attacker, 11, "Arrow Worm"},
	Carmina:            {MiniBoss, 15, "Carmina"},
	SeedlingKing:       {Boss, 9, "Seedling King"},
	Broodmother:        {Boss, 7, "Broodmother"},
	Plumpling:          {Effect, 34, "Plumpling"},
	Flowerling:         {Effect, 33, "Flowerling"},
	Burglar:            {Attacker, 14, "Burglar"},
	Astotheles:         {MiniBoss, 4, "Astotheles"},
	MotherChomper:      {Boss, 6, "Mother Chomper"},
	Ahoneynation:       {MiniBoss, 0, "Ahoneynation"},
	BeeBoop:            {Effect, 43, "Bee-Boop"},
	SecurityTurret:     {Attacker, 16, "Security Turret"},
	Denmuki:            {Effect, 36, "Denmuki"},
	HeavyDroneB33:      {Boss, 2, "Heavy Drone B-33"},
	Mender:             {Effect, 44, "Mender"},
	Abomihoney:         {Attacker, 17, "Abomihoney"},
	DuneScorpion:       {MiniBoss, 5, "Dune Scorpion"},
	TidalWyrm:          {Boss, 10, "Tidal Wyrm"},
	Kali:               {MiniBoss, 14, "Kali"},
	Zombee:             {Attacker, 4, "Zombee"},
	Zombeetle:          {Attacker, 5, "Zombeetle"},
	TheWatcher:         {Boss, 3, "The Watcher"},
	PeacockSpider:      {Boss, 11, "Peacock Spider"},
	Bloatshroom:        {Attacker, 7, "Bloatshroom"},
	Krawler:            {Attacker, 18, "Krawler"},
	HauntedCloth:       {Attacker, 20, "Haunted Cloth"},
	Warden:             {Attacker, 19, "Warden"},
	JumpingSpider:      {Attacker, 22, "Jumping Spider"},
	MimicSpider:        {Attacker, 23, "Mimic Spider"},
	LeafbugNinja:       {Effect, 45, "Leafbug Ninja"},
	LeafbugArcher:      {Effect, 46, "Leafbug Archer"},
	LeafbugClubber:     {Effect, 47, "Leafbug Clubber"},
	Madesphy:           {Effect, 37, "Madesphy"},
	TheBeast:           {Boss, 4, "The Beast"},
	ChomperBrute:       {Attacker, 9, "Chomper Brute"},
	Mantidfly:          {Attacker, 21, "Mantidfly"},
	GeneralUltimax:     {MiniBoss, 9, "General Ultimax"},
	WildChomper:        {Effect, 41, "Wild Chomper"},
	Cross:              {MiniBoss, 7, "Cross"},
	Poi:                {MiniBoss, 8, "Poi"},
	PrimalWeevil:       {MiniBoss, 6, "Primal Weevil"},
	FalseMonarch:       {Boss, 13, "False Monarch"},
	Mothfly:            {Attacker, 27, "Mothfly"},
	MothflyCluster:     {Attacker, 28, "Mothfly Cluster"},
	Ironnail:           {Effect, 39, "Ironnail"},
	Belostoss:          {Attacker, 26, "Belostoss"},
	Ruffian:            {Attacker, 15, "Ruffian"},
	WaterStrider:       {Attacker, 25, "Water Strider"},
	DivingSpider:       {Attacker, 24, "Diving Spider"},
	Cenn:               {MiniBoss, 10, "Cenn"},
	Pisci:              {MiniBoss, 11, "Pisci"},
	DeadLanderα:        {MiniBoss, 19, "Dead Lander α"},
	DeadLanderβ:        {MiniBoss, 20, "Dead Lander β"},
	DeadLanderγ:        {MiniBoss, 21, "Dead Lander γ"},
	WaspKing:           {Boss, 15, "Wasp King"},
	TheEverlastingKing: {Boss, 16, "The Everlasting King"},
	Maki:               {Boss, 14, "Maki"},
	Kina:               {MiniBoss, 17, "Kina"},
	Yin:                {MiniBoss, 18, "Yin"},
	UltimaxTank:        {Boss, 5, "ULTIMAX Tank"},
	Zommoth:            {Boss, 8, "Zommoth"},
	Riz:                {MiniBoss, 16, "Riz"},
	Devourer:           {Boss, 12, "Devourer"},
}

// BasicIndex returns the index of the card in the list of vanilla cards
// with its back (boss, mini-boss, or enemy).
func (i ID) BasicIndex() int {
	if i >= ID(len(basicData)) {
		return -1
	}

	if basicData[i] == nil {
		return -1
	}

	return int(basicData[i].index)
}
