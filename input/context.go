// Package input processes button presses from a gamepad or keyboard.
package input

import (
	"context"
	"math"

	"git.lubar.me/ben/spy-cards/arcade"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/xerrors"
)

// Context is an input context.
type Context struct {
	state   State
	gamepad func() []arcade.Button
	size    size.Event
	touch   []touch.Event
	mouse   []mouse.Event
	key     []key.Event
}

type contextKey struct{}

// GetContext returns the Context created using NewContext.
//
// It panics if no context was created.
func GetContext(ctx context.Context) *Context {
	if c := ctx.Value(contextKey{}); c != nil {
		return c.(*Context)
	}

	panic(xerrors.New("input: no input context available"))
}

// NewContext creates a new input context.
func NewContext(ctx context.Context, gamepad func() []arcade.Button) (context.Context, *Context) {
	c := &Context{
		state: State{
			x: -1,
			y: -1,
		},

		gamepad: gamepad,
	}

	return context.WithValue(ctx, contextKey{}, c), c
}

// Tick updates the input context.
func (c *Context) Tick() {
	c.state.tick++

	c.state.lastX = c.state.x
	c.state.lastY = c.state.y
	c.state.lastClick = c.state.click

	for btn := range c.state.held {
		c.state.held[btn] = false
	}

	buttons := c.gamepad()

	for _, btn := range buttons {
		c.state.held[btn] = true
	}

	for btn, held := range c.state.held {
		if !held {
			c.state.pressed[btn] = false
			c.state.wasPressed[btn] = 0
		} else if c.state.wasPressed[btn] < c.state.tick {
			c.state.pressed[btn] = true
			c.state.wasPressed[btn] = ^uint64(0)
		}
	}

	for _, m := range c.mouse {
		switch m.Direction {
		case mouse.DirPress:
			c.state.click = true
		case mouse.DirRelease:
			c.state.click = false
		case mouse.DirStep:
			switch m.Button {
			case mouse.ButtonWheelUp:
				c.state.wheel--
			case mouse.ButtonWheelDown:
				c.state.wheel++
			}

			continue
		}

		c.state.x = m.X / float32(c.size.WidthPx)
		c.state.y = m.Y / float32(c.size.HeightPx)
	}

	c.mouse = c.mouse[:0]

	for _, t := range c.touch {
		switch t.Type {
		case touch.TypeBegin:
			c.state.click = true
		case touch.TypeEnd:
			c.state.click = false
		}

		c.state.x = t.X / float32(c.size.WidthPx)
		c.state.y = t.Y / float32(c.size.HeightPx)
	}

	c.touch = c.touch[:0]

	if c.state.click {
		c.state.drag += float32(math.Hypot(
			float64((c.state.x-c.state.lastX)*float32(c.size.WidthPx)),
			float64((c.state.y-c.state.lastY)*float32(c.size.HeightPx)),
		))
	} else {
		c.state.drag = 0
	}

	c.state.key = append(c.state.key, c.key...)
	c.key = c.key[:0]
}
