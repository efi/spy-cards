package card

import (
	"context"
	"encoding/base64"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"git.lubar.me/ben/spy-cards/format"
	"git.lubar.me/ben/spy-cards/internal"
	"golang.org/x/xerrors"
)

// Recording is a recording of a Spy Cards Online match.
type Recording struct {
	FormatVersion  uint64
	Version        [3]uint64
	ModeName       string
	Perspective    uint8 // 0 = none, n = Player n
	Cosmetic       [2]CosmeticData
	RematchCount   uint64
	Start          time.Time
	SpoilerGuard   [2]*[32]byte
	SharedSeed     [32]byte
	PrivateSeed    [2][4]byte
	CustomCards    Set
	CustomCardsRaw string
	InitialDeck    [2]Deck
	Rounds         []RecordingRound
}

// CosmeticData is non-essential data representing choices made by each player
// before the match began.
type CosmeticData struct {
	CharacterName string `json:"character"`
}

// RecordingRound is data for one round of Spy Cards Online.
type RecordingRound struct {
	TurnSeed  [8]byte
	TurnSeed2 [8]byte
	Ready     [2]uint64
}

// TurnData is the data stored for one round of Spy Cards Online to allow
// verification.
type TurnData struct {
	Ready    [2]uint64
	Played   [2][]ID
	InHand   [2]uint64
	HandID   [2][]ID
	Modified [2][][]ModifiedCardPosition
}

// ModifiedCardPosition is a card modified by the Modify Available Cards effect.
type ModifiedCardPosition struct {
	InHand   bool
	Position uint64
	CardID   ID
}

// MarshalText implements encoding.TextMarshaler.
func (sc *Recording) MarshalText() ([]byte, error) {
	b, err := sc.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(base64.StdEncoding.EncodeToString(b)), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (sc *Recording) UnmarshalText(b []byte) error {
	dec, err := base64.StdEncoding.DecodeString(string(b))
	if err != nil {
		return xerrors.Errorf("recording: decoding failed: %w", err)
	}

	return sc.UnmarshalBinary(dec)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (sc *Recording) MarshalBinary() ([]byte, error) {
	var w format.Writer

	w.UVarInt(sc.FormatVersion)
	w.UVarInt(sc.Version[0])
	w.UVarInt(sc.Version[1])
	w.UVarInt(sc.Version[2])
	w.String1(sc.ModeName)
	w.Write(sc.Perspective)

	for _, c := range sc.Cosmetic {
		w.String1(c.CharacterName)
	}

	if sc.FormatVersion >= 1 {
		w.UVarInt(sc.RematchCount)
	}

	if sc.FormatVersion >= 2 {
		w.UVarInt(uint64(sc.Start.UnixNano()) / uint64(time.Millisecond))

		var extraFields uint64

		if sc.SpoilerGuard[0] != nil {
			extraFields |= 1 << 0
		}

		if sc.SpoilerGuard[1] != nil {
			extraFields |= 1 << 1
		}

		w.UVarInt(extraFields)

		if sc.SpoilerGuard[0] != nil {
			w.Write(sc.SpoilerGuard[0])
		}

		if sc.SpoilerGuard[1] != nil {
			w.Write(sc.SpoilerGuard[1])
		}
	}

	w.Write(sc.SharedSeed)
	w.Write(sc.PrivateSeed)

	cards, err := sc.marshalCards()
	if err != nil {
		return nil, err
	}

	w.UVarInt(uint64(len(cards)))

	for _, c := range cards {
		sub, done := w.SubWriter()

		sub.Bytes(c)

		done()
	}

	for _, d := range sc.InitialDeck {
		b, err := d.MarshalBinary()
		if err != nil {
			return nil, err
		}

		sub, done := w.SubWriter()

		sub.Bytes(b)

		done()
	}

	w.UVarInt(uint64(len(sc.Rounds)))

	for _, r := range sc.Rounds {
		w.Write(r.TurnSeed)

		if sc.FormatVersion >= 1 {
			w.Write(r.TurnSeed2)
		}

		for _, mask := range r.Ready {
			w.UVarInt(mask)
		}
	}

	return w.Data(), nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (sc *Recording) UnmarshalBinary(b []byte) (err error) {
	defer format.Catch(&err)

	var r format.Reader

	r.Init(b)

	var rec Recording
	rec.FormatVersion = r.UVarInt()

	if rec.FormatVersion > 1 {
		return xerrors.Errorf("recording: invalid recording version %d", rec.FormatVersion)
	}

	rec.Version[0] = r.UVarInt()
	rec.Version[1] = r.UVarInt()
	rec.Version[2] = r.UVarInt()
	rec.ModeName = r.String1()
	rec.Perspective = r.Byte()

	for i := range rec.Cosmetic {
		rec.Cosmetic[i].CharacterName = r.String1()
	}

	if rec.FormatVersion >= 1 {
		rec.RematchCount = r.UVarInt()
	}

	if rec.FormatVersion >= 2 {
		start := r.UVarInt()
		rec.Start = time.Unix(int64(start/1000), int64(start%1000)*int64(time.Millisecond))

		extraFields := r.UVarInt()

		if extraFields&(1<<0) != 0 {
			r.Read(&rec.SpoilerGuard[0])
		} else {
			rec.SpoilerGuard[0] = nil
		}

		if extraFields&(1<<1) != 0 {
			r.Read(&rec.SpoilerGuard[1])
		} else {
			rec.SpoilerGuard[1] = nil
		}
	}

	r.Read(&rec.SharedSeed)
	r.Read(&rec.PrivateSeed)

	cards := make([]*format.Reader, r.UVarInt())
	for i := range cards {
		cards[i] = r.SubReader()
	}

	if err = rec.unmarshalCards(cards); err != nil {
		return err
	}

	for i := range rec.InitialDeck {
		sub := r.SubReader()

		if err = rec.InitialDeck[i].UnmarshalBinary(sub.Bytes(sub.Len())); err != nil {
			return err
		}
	}

	rec.Rounds = make([]RecordingRound, r.UVarInt())
	for i := range rec.Rounds {
		r.Read(&rec.Rounds[i].TurnSeed)

		if rec.FormatVersion >= 1 {
			r.Read(&rec.Rounds[i].TurnSeed2)
		}

		for j := range rec.Rounds[i].Ready {
			rec.Rounds[i].Ready[j] = r.UVarInt()
		}
	}

	*sc = rec

	return nil
}

func (sc *Recording) marshalCards() ([][]byte, error) {
	buffers := make([][]byte, 0, len(sc.CustomCards.Cards)+2)

	if sc.CustomCards.Mode != nil {
		b, err := sc.CustomCards.Mode.MarshalBinary()
		if err != nil {
			return nil, xerrors.Errorf("card: encoding game mode: %w", err)
		}

		buffers = append(buffers, b)

		if sc.CustomCards.Variant != -1 {
			var w format.Writer

			w.UVarInt(uint64(sc.CustomCards.Variant))
			buffers = append(buffers, w.Data())
		}
	}

	for _, c := range sc.CustomCards.Cards {
		b, err := c.MarshalBinary()
		if err != nil {
			return nil, xerrors.Errorf("card: encoding card: %w", err)
		}

		buffers = append(buffers, b)
	}

	return buffers, nil
}

func (sc *Recording) unmarshalCards(readers []*format.Reader) error {
	if sc.ModeName == "" && len(readers) != 0 {
		return xerrors.Errorf("recording: %d custom cards for modeless recording", len(readers))
	}

	if sc.ModeName != "" && len(readers) == 0 {
		return xerrors.Errorf("recording: no custom cards for mode=%q recording", sc.ModeName)
	}

	sc.CustomCards = Set{
		External: sc.ModeName,
		Variant:  -1,
	}

	if sc.CustomCards.External == "custom" {
		sc.CustomCards.External = ""
	}

	raw := make([]string, len(readers))
	rawCards := raw

	if len(readers) != 0 && readers[0].Len() != 0 && readers[0].Peek(1)[0] == 3 {
		b := readers[0].Bytes(readers[0].Len())

		raw[0] = base64.StdEncoding.EncodeToString(b)

		sc.CustomCards.Mode = &GameMode{}
		if err := sc.CustomCards.Mode.UnmarshalBinary(b); err != nil {
			return xerrors.Errorf("recording: parsing game mode: %w", err)
		}

		readers = readers[1:]

		if numVariants := len(sc.CustomCards.Mode.GetAll(FieldVariant)); numVariants != 0 {
			if len(readers) == 0 {
				return xerrors.New("recording: missing game mode variant number")
			}

			err := func() (err error) {
				defer format.Catch(&err)

				sc.CustomCards.Variant = int(readers[0].UVarInt())

				if readers[0].Len() != 0 {
					return xerrors.New("additional unparsed data in buffer")
				}

				return nil
			}()
			if err != nil {
				return xerrors.Errorf("recording: parsing variant number: %w", err)
			}

			if sc.CustomCards.Variant >= numVariants {
				return xerrors.Errorf("recording: variant number %d outside of range [0, %d)", sc.CustomCards.Variant, numVariants)
			}

			readers = readers[1:]
			rawCards = rawCards[1:]
		}
	}

	cardBuf := make([]Def, len(readers))
	sc.CustomCards.Cards = make([]*Def, len(readers))

	for i, r := range readers {
		b := r.Bytes(r.Len())

		rawCards[i] = base64.StdEncoding.EncodeToString(b)

		sc.CustomCards.Cards[i] = &cardBuf[i]

		if err := sc.CustomCards.Cards[i].UnmarshalBinary(b); err != nil {
			return xerrors.Errorf("recording: parsing card %d: %w", i, err)
		}
	}

	sc.CustomCardsRaw = strings.Join(raw, ",")

	return nil
}

// FetchRecording downloads and decodes a recording from the server, or
// decodes a base64-encoded recording, depending on the length of the code.
func FetchRecording(ctx context.Context, code string) (*Recording, error) {
	var b []byte

	if len(code) < 30 {
		req, err := http.NewRequestWithContext(ctx, http.MethodGet, internal.GetConfig(ctx).MatchRecordingBaseURL+"get/"+code, nil)
		if err != nil {
			return nil, xerrors.Errorf("card: fetching recording %q: %w", code, err)
		}

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, xerrors.Errorf("card: fetching recording %q: %w", code, err)
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			return nil, xerrors.Errorf("card: fetching recording %q: server returned %q", code, resp.Status)
		}

		b, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, xerrors.Errorf("card: fetching recording %q: %w", code, err)
		}
	} else {
		var err error

		b, err = base64.StdEncoding.DecodeString(code)
		if err != nil {
			return nil, xerrors.Errorf("card: decoding recording: %w", err)
		}
	}

	var rec Recording

	return &rec, rec.UnmarshalBinary(b)
}
