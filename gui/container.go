package gui

import "math"

func max(a, b float32) float32 {
	return float32(math.Max(float64(a), float64(b)))
}

func min(a, b float32) float32 {
	return float32(math.Min(float64(a), float64(b)))
}

type Border [4]float32 // top, right, bottom, left like in css

func (b Border) Values() (float32, float32, float32, float32) {
	return b[0], b[1], b[2], b[3]
}

type Container struct {
	Packable
	Control
	Rect
	Margin, Padding     Border
	MinWidth, MinHeight int
	MaxWidth, MaxHeight int
	Scroll, Layer       int
	Visible, Focusable,
	Enabled, Centered,
	scrollable bool
}

func NewContainer() *Container {
	return &Container{
		Control:   Control{},
		MaxWidth:  2000,
		MaxHeight: 1500,
		Visible:   true,
		Enabled:   true,
		Margin:    Border{5, 5, 5, 5},
		Padding:   Border{5, 5, 5, 5},
	}
}

func (c *Container) Show() {
	c.Visible = true
}

func (c *Container) Hide() {
	c.Visible = false
}

func (c *Container) IsVisible() bool { return c.Visible }

func (c *Container) Enable() {
	c.Enabled = true
}

func (c *Container) Disable() {
	c.Enabled = false
}

func (c *Container) DoScroll() { c.scrollable = true }

func (c *Container) NoScroll() { c.scrollable = false }

func (c *Container) Scrollable() bool { return c.scrollable }

func (c *Container) Pack(R Rect) Rect { // R should be the previous sibling
	if !c.Visible || c.Parent == nil {
		return Rect{}
	}
	p := c.Control.Parent
	px, py := c.Rect.Position()
	pw, ph := c.Rect.Size()
	if p.Scrollable() {
		ph = 0xFFFFFFFF
	}
	sx, sy := R.Position()
	sw, sh := R.Size()
	t, r, b, l := c.Margin.Values()
	mw, mh, Mw, Mh := float32(c.MinWidth), float32(c.MinHeight), float32(c.MaxWidth), float32(c.MaxHeight)
	if mw+sx+sw < pw { // if we can't fit anything on this width, we try the next line
		c.Rect.SetPosition(
			px+sx+sw+l,
			max(py, sy)+t)
		c.Rect.SetSize(
			min(Mw, max(mw, pw-(sx+sw+l+r))),
			min(Mh, max(mh, (ph-max(0, (sy-py)))-(t+b))))
		if c.Centered {
			c.Rect.x += (px + sx + sw + l + r - pw) / 2
		}
	} else {
		c.Rect.SetPosition(
			px+l,
			sy+sh+t)
		c.Rect.SetSize(
			min(Mw, max(mw, pw-(l+r))),
			min(Mh, max(mh, ph-(sy+sh+t+b))))
		if c.Centered {
			c.Rect.x += (px + l + r - pw) / 2
		}
	}
	return c.Rect // return our modified rect for the next sibling to use
}

func (c *Container) HasFocus() bool {
	if !c.Focusable {
		return false
	}
	if c.Control.root.focus.ok && c.Control.root.focus.control == Handler(&c.Control) {
		return true
	}
	for _, ch := range c.Control.GetChildren() {
		if ch.(*Container).HasFocus() {
			return true
		}
	}
	return false
}
