module git.lubar.me/ben/spy-cards

go 1.13

require (
	github.com/BenLubar/convnet v1.1.0
	github.com/BenLubar/opus v0.0.0-20200924184101-63bc690a6a04
	github.com/davecgh/go-spew v1.1.1
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/jackc/pgtype v1.5.0
	github.com/jackc/pgx/v4 v4.9.0
	github.com/pion/webrtc/v3 v3.0.0-beta.10.0.20201025043308-69bfb5841d55
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/exp v0.0.0-20201008143054-e3b2a7f2fdc7
	golang.org/x/image v0.0.0-20200927104501-e162460cd6b5
	golang.org/x/mobile v0.0.0-20200801112145-973feb4309de
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1
	nhooyr.io/websocket v1.8.6
)

replace (
	golang.org/x/exp => git.lubar.me/ben/exp v0.0.0-20201026223723-aa67a1c6ddf8
	golang.org/x/mobile => git.lubar.me/ben/mobile v0.0.0-20201026230246-71616f122470
)
