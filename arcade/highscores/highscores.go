// Package highscores implements the Termacade high scores viewer.
package highscores

import (
	"context"
	"image/color"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/audio"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/crt"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"golang.org/x/xerrors"
)

var nextGame = map[arcade.Game]arcade.Game{
	arcade.FlowerJourney: arcade.MiteKnight,
	arcade.MiteKnight:    arcade.FlowerJourney,
}

var nextMode = map[Mode]Mode{
	Recent:    Weekly,
	Weekly:    Quarterly,
	Quarterly: AllTime,
	AllTime:   Recent,
}

// Entry is a single item in the high scores list.
type Entry struct {
	Code   string `json:"c"`
	Score  int64  `json:"s"`
	Player string `json:"p"`
}

// HighScores is the high scores viewer.
type HighScores struct {
	Camera gfx.Camera
	CRT    crt.CRT

	prev  []Entry
	cur   []Entry
	next  []Entry
	first bool

	Game  arcade.Game
	Mode  Mode
	Page  int
	Index int

	loading time.Time
	loaded  time.Time
	fetched chan<- func() error

	batch *sprites.Batch
	tb    *sprites.TextBatch
}

// Run displays the high scores viewer.
func (hs *HighScores) Run(ctx context.Context) error {
	hs.Camera.SetDefaults()
	hs.Camera.Position = gfx.Identity()
	hs.CRT.SetDefaults()

	audio.TermiteLoop.PlayMusic(0, false)

	fetched := make(chan func() error, 1)
	hs.fetched = fetched

	hs.first = true

	ictx := input.GetContext(ctx)

	if hs.Game == 0 {
		hs.Game = arcade.FlowerJourney
	}

	if hs.Mode == 0 {
		hs.Mode = Weekly
	}

	hs.setParametersFromURL()

	hs.fetchPages(ctx)

	for {
		ictx.Tick()

		select {
		case f := <-fetched:
			if err := f(); err != nil {
				return err
			}
		default:
		}

		switch {
		case ictx.Consume(arcade.BtnConfirm):
			if hs.Index < len(hs.cur) {
				audio.Confirm.PlaySound(0, 0, 0)

				var viewer RecordingViewer

				viewer.Code = hs.cur[hs.Index].Code

				viewer.pushURL()

				if err := viewer.Run(ctx); err != nil {
					return err
				}

				audio.TermiteLoop.PlayMusic(0, false)

				viewer.popURL()
			} else {
				audio.Buzzer.PlaySound(0, 0, 0)
			}
		case ictx.Consume(arcade.BtnCancel):
			hs.clearURL()

			return ctx.Err()
		case ictx.Consume(arcade.BtnToggle):
			hs.first = true
			hs.Game = nextGame[hs.Game]
			hs.resetPages(ctx)
			audio.Confirm.PlaySound(0, 0, 0)
		case ictx.Consume(arcade.BtnSwitch):
			hs.first = true
			hs.Mode = nextMode[hs.Mode]
			hs.resetPages(ctx)
			audio.Confirm.PlaySound(0, 0, 0)
		case ictx.ConsumeAllowRepeat(arcade.BtnLeft, 60):
			hs.first = false
			if hs.Page != 0 {
				hs.prevPage(ctx)
				audio.PageFlip.PlaySound(0, 0, 0)
			} else if hs.Index != 0 {
				hs.Index = 0
				audio.Confirm1.PlaySound(0, 0, 0)
			}
		case ictx.ConsumeAllowRepeat(arcade.BtnRight, 60):
			hs.first = false

			switch {
			case len(hs.next) != 0:
				hs.nextPage(ctx)

				if last := len(hs.cur) - 1; hs.Index > last {
					hs.Index = last
				}

				audio.PageFlip.PlaySound(0, 0, 0)
			case hs.cur != nil && hs.Index != len(hs.cur)-1:
				hs.Index = len(hs.cur) - 1

				audio.Confirm1.PlaySound(0, 0, 0)
			case hs.next != nil:
				audio.Buzzer.PlaySound(0, 0, 0)
			}
		case ictx.ConsumeAllowRepeat(arcade.BtnUp, 12):
			if hs.cur != nil {
				hs.first = false
				if hs.Index == 0 {
					if hs.Page != 0 {
						hs.prevPage(ctx)
						hs.Index = 4

						audio.PageFlip.PlaySound(0, 0, 0)
					} else {
						audio.Buzzer.PlaySound(0, 0, 0)
					}
				} else {
					hs.Index--
					audio.Confirm1.PlaySound(0, 0, 0)
				}
			}
		case ictx.ConsumeAllowRepeat(arcade.BtnDown, 12):
			if hs.cur != nil {
				hs.first = false
				if hs.Index == 4 {
					if len(hs.next) != 0 {
						hs.nextPage(ctx)
						hs.Index = 0

						audio.PageFlip.PlaySound(0, 0, 0)
					} else {
						audio.Buzzer.PlaySound(0, 0, 0)
					}
				} else {
					if hs.Index+1 >= len(hs.cur) {
						audio.Buzzer.PlaySound(0, 0, 0)
					} else {
						hs.Index++
						audio.Confirm1.PlaySound(0, 0, 0)
					}
				}
			}
		}

		hs.CRT.Draw(hs.render)

		gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			return xerrors.Errorf("highscores: timed out: %w", err)
		}
	}
}

func whiteOrGray(white bool) color.RGBA {
	if white {
		return sprites.White
	}

	return sprites.Gray
}
