"use strict";
var SpyCards;
(function (SpyCards) {
    var Native;
    (function (Native) {
        if (!WebAssembly.instantiateStreaming) { // polyfill
            WebAssembly.instantiateStreaming = async (resp, importObject) => {
                const source = await (await resp).arrayBuffer();
                return await WebAssembly.instantiate(source, importObject);
            };
        }
        let go = new window.Go();
        let inst;
        let mod;
        let ready = WebAssembly.instantiateStreaming(fetch("spy-cards.wasm"), go.importObject).then(function (result) {
            inst = result.instance;
            mod = result.module;
        });
        const roomRPCPromise = new Promise((resolve) => Native.roomRPCInit = resolve);
        let roomRPCProcess;
        async function roomRPC() {
            if (new URLSearchParams(location.search).has("no3d") || SpyCards.loadSettings().disable3D) {
                return null;
            }
            if (!roomRPCProcess) {
                roomRPCProcess = run("-run=RoomRPC");
            }
            return roomRPCPromise;
        }
        Native.roomRPC = roomRPC;
        async function run(...argv) {
            ready = ready.then(async () => {
                go.argv = ["js"].concat(argv);
                await go.run(inst);
                inst = await WebAssembly.instantiate(mod, go.importObject);
            });
            return ready;
        }
        Native.run = run;
    })(Native = SpyCards.Native || (SpyCards.Native = {}));
})(SpyCards || (SpyCards = {}));
