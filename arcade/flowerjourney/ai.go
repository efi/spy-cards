package flowerjourney

import (
	"image/color"
	"math"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
)

// InitAI implements game.Interface.
func (fj *FlowerJourney) InitAI() *game.AIConfig {
	return &game.AIConfig{
		NumInput: 1 + // x velocity
			1 + // y velocity
			1 + // distance to ceiling
			1 + // distance to floor
			1 + // distance to nearest wall bottom
			1 + // distance to nearest wall top
			1 + // x distance to nearest wall
			2 + // item coordinate
			2 + // enemy coordinate
			1 + // invuln timer
			1, // item type

		Press: []game.WeightedButton{
			{
				Button: arcade.BtnConfirm,
				Weight: 0.1,
			},
		},

		InputDesc: []game.AIInputDesc{
			{Min: -0.2, Max: 0},
			{Min: -10, Max: 10},
			{Min: -5, Max: 5},
			{Min: -5, Max: 5},
			{Min: -5, Max: 5},
			{Min: -5, Max: 5},
			{Min: 0, Max: 20},
			{Min: -10, Max: 10},
			{Min: -3, Max: 3},
			{Min: -10, Max: 10},
			{Min: -3, Max: 3},
			{Min: 0, Max: 5},
			{Min: -1, Max: 1, Enum: map[float64]color.RGBA{
				-1:                  {0, 0, 0, 255},
				float64(itemFlower): {255, 102, 255, 255},
				float64(itemHoney):  {255, 136, 0, 255},
			}},
		},
	}
}

// AIInput implements game.Interface.
func (fj *FlowerJourney) AIInput() (input []float64, reward float64) {
	reward = -fj.lastScore
	fj.lastScore = fj.state.Score
	reward += fj.state.Score

	if fj.gameState == stateEnding && !fj.didLastAITick {
		reward -= 99
		fj.didLastAITick = true
	} else if fj.gameState != stateActive {
		return nil, math.NaN()
	}

	nearestWall := fj.walls[0]
	if nearestWall.passed {
		nearestWall.x += 100
	}

	for _, w := range fj.walls {
		if w.passed {
			w.x += 100
		}

		if nearestWall.x > w.x {
			nearestWall = w
		}
	}

	distanceFromTop := 5.25 - (fj.playerY - 0.05 + 0.2)
	distanceFromBottom := (fj.playerY - 0.05 - 0.2) - 0.2012
	distanceFromLower := (fj.playerY - 0.05 - 0.2) - (nearestWall.y + 0.95)
	distanceFromUpper := (nearestWall.y + nearestWall.gap - 1.05) - (fj.playerY - 0.05 + 0.2)

	distanceReward := math.Min(math.Min(distanceFromTop, distanceFromBottom), math.Min(distanceFromLower, distanceFromUpper))
	if distanceReward < 0 {
		distanceReward *= 3
	}

	reward += distanceReward

	var itemY, enemyY, itemType float64

	if fj.itemEnabled {
		itemY = fj.itemY - fj.playerY
		itemType = float64(fj.itemType)
	} else {
		itemType = -1
	}

	if fj.enemyEnabled {
		enemyY = fj.enemyY - fj.playerY
	}

	input = []float64{
		fj.speed,
		fj.playerYVelocity,
		distanceFromTop,
		distanceFromBottom,
		distanceFromUpper,
		distanceFromLower,
		nearestWall.x - fj.playerX,
		math.Min(10, fj.itemX-fj.playerX),
		itemY,
		math.Min(10, fj.enemyX-fj.playerX),
		enemyY,
		float64(fj.invul) / game.FrameRate,
		itemType,
	}

	return input, reward
}
