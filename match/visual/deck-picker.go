// +build !headless

package visual

import (
	"context"
	"image/color"
	"math"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/touchcontroller"
	"git.lubar.me/ben/spy-cards/card"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
	"git.lubar.me/ben/spy-cards/match"
)

type DeckPicker struct {
	cam     gfx.Camera
	ictx    *input.Context
	set     *card.Set
	decks   [][]*Card
	builder *DeckBuilder
	tb      *sprites.TextBatch

	aspect   float32
	scroll   float32
	wheel    float32
	momentum float32
	dist     float32
	oldX     float32
	oldY     float32
	active   int
	fromKB   bool
	wasClick bool

	AllowCreate bool
	Selected    card.Deck
}

func NewDeckPicker(ctx context.Context, set *card.Set) *DeckPicker {
	d := &DeckPicker{
		ictx: input.GetContext(ctx),
		set:  set,
	}

	d.cam.SetDefaults()
	d.cam.Position = gfx.Identity()
	d.cam.Rotation = gfx.Identity()
	d.cam.Offset = gfx.Identity()

	return d
}

func (d *DeckPicker) Populate() {
	rules, _ := d.set.Mode.Get(card.FieldGameRules).(*card.GameRules)
	if rules == nil {
		rules = &card.DefaultGameRules
	}

	d.decks = d.decks[:0]

	if d.AllowCreate {
		unknownDeck := make([]*Card, rules.CardsPerDeck)

		for i := range unknownDeck {
			back := card.Enemy

			if i < int(rules.BossCards) {
				back = card.Boss
			} else if i < int(rules.BossCards+rules.MiniBossCards) {
				back = card.MiniBoss
			}

			unknownDeck[i] = &Card{
				Card: &match.Card{
					Set:  d.set,
					Back: back,
				},
			}
		}

		d.decks = append(d.decks, unknownDeck)
	}

	savedDecks := match.LoadDecks()

	for _, deck := range savedDecks {
		if deck.Validate(d.set) == nil {
			visDeck := make([]*Card, len(deck))

			for i, id := range deck {
				def := d.set.Card(id)

				visDeck[i] = &Card{
					Card: &match.Card{
						Set:  d.set,
						Def:  def,
						Back: def.Rank.Back(),
					},
				}
			}

			d.decks = append(d.decks, visDeck)
		}
	}
}

func (d *DeckPicker) Update(ctx context.Context) {
	touchcontroller.SkipFrame = true

	if d.builder != nil {
		if d.builder.Confirmed {
			if len(d.builder.deck.Cards) == 0 {
				d.builder = nil

				d.Populate()
			} else {
				d.Selected = make(card.Deck, len(d.builder.deck.Cards))

				for i, c := range d.builder.deck.Cards {
					d.Selected[i] = c.Def.ID
				}

				return
			}
		} else {
			d.builder.Update()

			return
		}
	}

	if len(d.decks) == 1 && d.AllowCreate {
		d.builder = NewDeckBuilder(ctx, d.set)
		d.builder.Update()

		return
	}

	d.ictx.Tick()
	rx, ry, click := d.ictx.Mouse()

	w, h := gfx.Size()
	d.aspect = float32(w) / float32(h)
	d.cam.Perspective = gfx.Scale(-64/float32(w), -64/float32(h), 0.01)

	scale := float32(1)
	top := float32(-1)

	if len(d.decks) != 0 {
		scale = float32(w) / 32 / float32(len(d.decks[0])+3) / cardWidth
		top = float32(h)/64 - cardHeight*scale
	}

	top += d.scroll * cardHeight * 1.05 * scale

	switch wheel := d.ictx.ConsumeWheel(); {
	case wheel != 0:
		d.wheel += wheel * 5 / cardHeight / scale
	case d.ictx.ConsumeAllowRepeat(arcade.BtnUp, 30):
		if d.active > 0 {
			d.active--
		} else {
			d.active = 0
		}

		d.fromKB = true
	case d.ictx.ConsumeAllowRepeat(arcade.BtnDown, 30):
		if d.active+1 < len(d.decks) {
			if d.active < 0 {
				d.active = 0
			}

			d.active++
		}

		d.fromKB = true
	case (d.dist < 0.2 && d.wasClick && !click) || d.ictx.Consume(arcade.BtnConfirm):
		if d.active >= 0 && d.active < len(d.decks) {
			d.builder = NewDeckBuilder(ctx, d.set)
			d.builder.deck.ictx = d.ictx
			d.decks[d.active][0].hover = nil
			d.builder.deck.Cards = d.decks[d.active]
			d.builder.resetPicker()
			d.builder.Update()

			return
		}
	}

	if d.fromKB {
		d.scroll = d.scroll*0.9 + (float32(d.active+1)-float32(h)/64/cardHeight/scale/1.05)*0.1
	} else if !click {
		if d.scroll < 0 {
			d.scroll *= 0.9
		}
		if bottom := float32(len(d.decks)) + 0.5 - float32(h)/32/cardHeight/scale/1.05; d.scroll > bottom {
			d.scroll = (d.scroll-bottom)*0.9 + bottom
		}

		d.dist = 0
	}

	if rx != d.oldX || ry != d.oldY {
		d.fromKB = false

		if d.wasClick && click {
			amount := (ry - d.oldY) * float32(h) / 32 / cardHeight / scale / 1.05

			d.scroll -= amount
			d.momentum -= amount
			d.dist += float32(math.Abs(float64(amount)))
		}
	}

	if click {
		d.momentum *= 0.9
	} else {
		d.scroll += d.momentum * 0.1
		d.momentum *= 0.95
	}

	d.scroll += d.wheel * 0.1
	d.wheel *= 0.9

	d.oldX = rx
	d.oldY = ry
	d.wasClick = click

	var hovering bool

	for i := range d.decks {
		c := d.decks[i][0]
		if c.hover == nil {
			c.hover = &HoverTilt{}
			c.hover.x0 = -scale * cardWidth * float32(len(d.decks[0])-1) / 2
			c.hover.y0 = sprites.CardFront.Y0
			c.hover.x1 = scale * cardWidth * float32(len(d.decks[0])-1) / 2
			c.hover.y1 = sprites.CardFront.Y1
		}

		c.hover.y = top - scale*cardHeight*1.05*float32(i)
		c.hover.scale = scale * (1 + float32(c.hover.hover)/255*0.125)
		c.hover.computeCursor(&d.cam, rx, ry)

		c.hover.offX *= 0.05
		c.hover.offY *= 0.05

		dist := c.hover.falloff
		if d.fromKB {
			dist = 1000

			if d.active == i {
				dist = 1
			}
		}

		if !hovering && dist < cardNearThreshold {
			if c.hover.hover > 255-deckHoverInSpeed {
				c.hover.hover = 255
			} else {
				c.hover.hover += deckHoverInSpeed
				d.active = i
			}

			hovering = true
		} else {
			if c.hover.hover > deckHoverOutSpeed {
				c.hover.hover -= deckHoverOutSpeed
			} else {
				c.hover.hover = 0
			}
		}
	}

	if !hovering {
		d.active = -1
	}
}

func (d *DeckPicker) Render(ctx context.Context) {
	touchcontroller.SkipFrame = true

	if d.builder != nil {
		d.builder.Render(ctx)

		return
	}

	for i := range d.decks {
		c0 := d.decks[i][0]
		if c0.hover.mins[1] > 1.5 || c0.hover.maxs[1] < -1.5 {
			continue
		}

		if d.active != i {
			d.renderDeck(ctx, i)
		}
	}

	if d.active >= 0 && d.active < len(d.decks) {
		d.renderDeck(ctx, d.active)
	}
}

func (d *DeckPicker) renderDeck(ctx context.Context, i int) {
	c0 := d.decks[i][0]

	left := float32(len(d.decks[i])-1) * cardWidth / 2

	c0.hover.Render(&d.cam, func(cam *gfx.Camera) {
		for j, c := range d.decks[i] {
			cam.PushTransform(gfx.Translation(float32(j)*cardWidth-left, 0, 0))
			c.Render(ctx, &d.cam, nil)
			cam.PopTransform()
		}

		if i == 0 && d.AllowCreate {
			if d.tb == nil {
				d.tb = sprites.NewTextBatch(cam)
			} else {
				d.tb.Reset(cam)
			}

			sprites.DrawTextCenteredFuncEx(d.tb, sprites.FontBubblegumSans, "Create New Deck", 0, -0.5, 0, 2, 2, func(rune) color.RGBA { return sprites.Black }, false, sprites.FlagBorder, 0, 0, 0)

			c := color.RGBA{171, 171, 171, 255}
			if d.active == 0 {
				c = sprites.White
			}

			sprites.DrawTextCentered(d.tb, sprites.FontBubblegumSans, "Create New Deck", 0, -0.5, 0, 2, 2, c, false)

			d.tb.Render()
		}
	})
}
