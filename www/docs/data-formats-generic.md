---
title: Data Formats (Generic)
...

# Binary-to-Text

- **Base64:** <https://tools.ietf.org/html/rfc4648#section-4>
- **Crockford Base32:** <https://crockford.com/base32.html>

# Varint

Defined the same way as it is defined in Protocol Buffers, that is:

- Byte values between 0 and 127 represent their own value and stop.
- Other byte values represent their 7 least significant bits and continue.
- Byte order is little-endian.

# Match Code

(Crockford Base32 encoded)

15 random bytes

The first 20 bits are overridden based on the client type:

- Vanilla: 11001101 10111100 1100
- Custom: 01100110 01110101 0100

Some custom game modes have their own prefixes.

The prefix followed by all zeroes is used for the quick join match code.

# File ID

(Crockford Base32 encoded)

- 8 bytes: first 8 bytes of SHA-256 hash of file
- varint: opaque ID
