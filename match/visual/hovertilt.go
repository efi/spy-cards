package visual

import (
	"math"

	"git.lubar.me/ben/spy-cards/gfx"
)

type HoverTilt struct {
	x, y, z    float32
	x0, y0     float32
	x1, y1     float32
	dx, dy     float32
	scale      float32
	offX, offY float32
	falloff    float32
	divisor    float32
	mins, maxs gfx.Vector
	hover      uint8
}

func (h *HoverTilt) computeCursor(cam *gfx.Camera, cursorX, cursorY float32) {
	h.mins = gfx.MultiplyVector(
		cam.Perspective,
		gfx.MultiplyVector(
			cam.Combined(),
			gfx.Vector{h.x + h.scale*h.x0, h.y + h.scale*h.y0, h.z, 1},
		),
	)
	h.maxs = gfx.MultiplyVector(
		cam.Perspective,
		gfx.MultiplyVector(
			cam.Combined(),
			gfx.Vector{h.x + h.scale*h.x1, h.y + h.scale*h.y1, h.z, 1},
		),
	)

	h.divisor = h.x1 - h.x0
	if y := h.y1 - h.y0; y > h.divisor {
		h.divisor = y
	}

	h.offX = (cursorX*2 - 1 - (h.mins[0]+h.maxs[0])/2) / (h.maxs[0] - h.mins[0])
	h.offY = (1 - cursorY*2 - (h.mins[1]+h.maxs[1])/2) / (h.maxs[1] - h.mins[1])
	h.falloff = float32(math.Pow(
		1+math.Max(
			math.Abs(float64(h.offX/(h.x1-h.x0))),
			math.Abs(float64(h.offY/(h.y1-h.y0))),
		), 8,
	))
}

func (h *HoverTilt) Render(cam *gfx.Camera, render func(*gfx.Camera)) {
	cam.PushTransform(gfx.MultiplyMatrix(
		gfx.MultiplyMatrix(
			gfx.MultiplyMatrix(
				gfx.Translation(h.x, h.y, h.z),
				gfx.Scale(h.scale*h.divisor, h.scale*h.divisor, h.scale*h.divisor),
			),
			gfx.Perspective(90*math.Pi/180, 1, 0.3, 150),
		),
		gfx.MultiplyMatrix(
			gfx.MultiplyMatrix(
				gfx.Translation(0, 0, -4),
				gfx.Scale(4/h.divisor, 4/h.divisor, 4/h.divisor),
			),
			gfx.MultiplyMatrix(
				gfx.RotationX(h.offY*(0.3+float32(h.hover)/255*0.6)/h.falloff),
				gfx.RotationY(-h.offX*(0.3+float32(h.hover)/255*0.6)/h.falloff),
			),
		),
	))

	render(cam)

	cam.PopTransform()
}
