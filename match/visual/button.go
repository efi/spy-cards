package visual

import (
	"image/color"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"git.lubar.me/ben/spy-cards/input"
)

type ButtonStyle uint8

const (
	StyleBorder ButtonStyle = iota
	StyleShadow
)

type Button struct {
	Label           string
	X, Y, Z         float32
	Sx, Sy          float32
	InputFocus      *interface{}
	InputFocusLeft  func()
	InputFocusRight func()
	InputFocusUp    func()
	InputFocusDown  func()
	OnSelect        func()
	Font            sprites.FontID
	Centered        bool
	Style           ButtonStyle
}

func (b *Button) Update(cam *gfx.Camera, ictx *input.Context) {
	if b.InputFocus != nil && *b.InputFocus == b {
		switch {
		case ictx.Consume(arcade.BtnConfirm):
			if b.OnSelect != nil {
				b.OnSelect()
			}
		case ictx.Consume(arcade.BtnLeft):
			if b.InputFocusLeft != nil {
				b.InputFocusLeft()
			}
		case ictx.Consume(arcade.BtnRight):
			if b.InputFocusRight != nil {
				b.InputFocusRight()
			}
		case ictx.Consume(arcade.BtnUp):
			if b.InputFocusUp != nil {
				b.InputFocusUp()
			}
		case ictx.Consume(arcade.BtnDown):
			if b.InputFocusDown != nil {
				b.InputFocusDown()
			}
		}
	}

	mx, my, click := ictx.Mouse()
	lastX, lastY, lastClick := ictx.LastMouse()

	if sprites.TextHitTest(cam, b.Font, b.Label, b.X, b.Y, b.Z, b.Sx, b.Sy, mx, my, b.Centered) {
		if (mx != lastX || my != lastY) && b.InputFocus != nil {
			*b.InputFocus = b
		}

		if lastClick && !click && b.OnSelect != nil && !ictx.IsMouseDrag(10) {
			b.OnSelect()
		}
	} else if (mx != lastX || my != lastY) && b.InputFocus != nil && *b.InputFocus == b {
		*b.InputFocus = nil
	}
}

func (b *Button) Render(tb *sprites.TextBatch) {
	var dx float32

	if b.Centered {
		for _, l := range b.Label {
			dx += sprites.TextAdvance(b.Font, l, b.Sx)
		}

		dx /= -2
	}

	switch b.Style {
	case StyleBorder:
		borderColor := sprites.Black
		borderFlag := sprites.FlagBorder

		if b.InputFocus != nil && *b.InputFocus == b {
			borderColor = sprites.White
			borderFlag |= sprites.FlagRainbow
		}

		sprites.DrawTextFuncEx(tb, b.Font, b.Label, b.X+dx, b.Y, b.Z, b.Sx, b.Sy, func(rune) color.RGBA { return borderColor }, borderFlag, 0, 0, 0)
		sprites.DrawText(tb, b.Font, b.Label, b.X+dx, b.Y, b.Z, b.Sx, b.Sy, sprites.White)
	case StyleShadow:
		textColor := sprites.White

		if b.InputFocus != nil && *b.InputFocus == b {
			textColor = sprites.Yellow
		}

		sprites.DrawTextShadow(tb, b.Font, b.Label, b.X+dx, b.Y, b.Z, b.Sx, b.Sy, textColor)
	}
}
