package card

import (
	"encoding/base64"

	"git.lubar.me/ben/spy-cards/format"
	"golang.org/x/xerrors"
)

// GameMode is a custom Spy Cards Online game mode.
type GameMode struct {
	Fields []Field
}

// Get returns the first field of a given FieldType, or nil.
func (gm *GameMode) Get(t FieldType) Field {
	if gm == nil {
		return nil
	}

	for _, f := range gm.Fields {
		if f.Type() == t {
			return f
		}
	}

	return nil
}

// GetAll returns a slice of fields with a given FieldType.
func (gm *GameMode) GetAll(t FieldType) []Field {
	if gm == nil {
		return nil
	}

	var fields []Field

	for _, f := range gm.Fields {
		if f.Type() == t {
			fields = append(fields, f)
		}
	}

	return fields
}

// Variant returns the game mode modified by the Variant with the given index.
func (gm *GameMode) Variant(i int) (*GameMode, *Variant) {
	if i < 0 {
		return gm, nil
	}

	var variants []*Variant

	for _, f := range gm.Fields {
		if v, ok := f.(*Variant); ok {
			variants = append(variants, v)
		}
	}

	if i >= len(variants) {
		return gm, nil
	}

	v := variants[i]

	vm := &GameMode{}
	vm.Fields = append(vm.Fields, gm.Fields...)
	vm.Fields = append(vm.Fields, v.Rules...)

	return vm, v
}

// MarshalText implements encoding.TextMarshaler.
func (gm *GameMode) MarshalText() ([]byte, error) {
	b, err := gm.MarshalBinary()
	if err != nil {
		return nil, err
	}

	return []byte(base64.StdEncoding.EncodeToString(b)), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (gm *GameMode) UnmarshalText(b []byte) error {
	p, err := base64.StdEncoding.DecodeString(string(b))
	if err != nil {
		return xerrors.Errorf("card: decoding game mode: %w", err)
	}

	return gm.UnmarshalBinary(p)
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (gm *GameMode) MarshalBinary() ([]byte, error) {
	var w format.Writer

	w.UVarInt(3) // format version
	w.UVarInt(uint64(len(gm.Fields)))

	for _, f := range gm.Fields {
		w.UVarInt(uint64(f.Type()))

		sub, done := w.SubWriter()

		if err := f.Marshal(sub); err != nil {
			return nil, xerrors.Errorf("card: failed to encode field: %w", err)
		}

		done()
	}

	return w.Data(), nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (gm *GameMode) UnmarshalBinary(b []byte) (err error) {
	defer format.Catch(&err)

	var r format.Reader

	r.Init(b)

	switch formatVersion := r.UVarInt(); formatVersion {
	case 3:
		fields := make([]Field, r.UVarInt())

		for i := range fields {
			fields[i], err = unmarshalField(&r, formatVersion)
			if err != nil {
				return xerrors.Errorf("card: parsing game mode field %d (%T): %w", i, fields[i], err)
			}
		}

		gm.Fields = fields

		return nil
	default:
		return xerrors.Errorf("card: unknown game mode format version %d", formatVersion)
	}
}

func unmarshalField(r *format.Reader, formatVersion uint64) (f Field, err error) {
	defer format.Catch(&err)

	ft := FieldType(r.UVarInt())

	f, err = NewField(ft)
	if err != nil {
		return
	}

	sub := r.SubReader()

	err = f.Unmarshal(sub, formatVersion)
	if err != nil {
		return
	}

	if sub.Len() != 0 {
		err = xerrors.New("card: field contains extra data")
	}

	return
}
