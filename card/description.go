package card

import (
	"image/color"
	"strconv"
	"strings"

	"git.lubar.me/ben/spy-cards/gfx/sprites"
)

// RichDescription is a segment of a Spy Cards card description.
type RichDescription struct {
	Effect *EffectDef

	Text    string
	Content []*RichDescription

	// Color overrides the parent's color if it is non-zero.
	Color color.RGBA
}

// Typeset arranges the text in an area for rendering.
func (d *RichDescription) Typeset(font sprites.FontID, x, y, z, sx, sy, spaceX, spaceY float32, effect *EffectDef, center bool, render func(x, y, z, sx, sy float32, text string, tint color.RGBA, isEffect bool)) {
	if spaceY == 0 {
		// horizontal squish
		var width float32

		for _, l := range d.String() {
			width += sprites.TextAdvance(font, l, sx)
		}

		if width > spaceX {
			sx *= spaceX / width

			if center {
				x -= spaceX / 2
			}
		} else if center {
			x -= width / 2
		}

		d.walk(func(text string, tint color.RGBA, isEffect bool) {
			render(x, y, z, sx, sy, text, tint, isEffect)

			for _, l := range text {
				x += sprites.TextAdvance(font, l, sx)
			}
		}, d.Color, false, effect)

		return
	}

	// vertical squish
	var (
		height, width, lastSpace float32
		x0                       []float32
		newLines                 []int
		lastSpaceBytes           int
	)

	for i, l := range d.String() {
		width += sprites.TextAdvance(font, l, sx)

		if l == ' ' {
			lastSpace = width
			lastSpaceBytes = i + 1
		}

		if l == '\n' {
			if center {
				x0 = append(x0, x-width/2)
			} else {
				x0 = append(x0, x)
			}

			width = 0
			lastSpace = 0
			height += sy * 0.7

			newLines = append(newLines, i+1)
		}

		if width > spaceX && lastSpace != 0 {
			if center {
				x0 = append(x0, x-(lastSpace-0.3*sx)/2)
			} else {
				x0 = append(x0, x)
			}

			width -= lastSpace
			lastSpace = 0
			height += sy * 0.7

			newLines = append(newLines, lastSpaceBytes)
		}
	}

	if width != 0 {
		height += sy * 0.7

		if center {
			x0 = append(x0, x-width/2)
		} else {
			x0 = append(x0, x)
		}
	}

	if height > spaceY {
		y += 0.7 * sy
		sy *= spaceY / height
		y -= 0.7 * sy
	}

	x0 = append(x0, x)
	y0 := y
	i := 0

	d.walk(func(text string, tint color.RGBA, isEffect bool) {
		for len(newLines) != 0 && newLines[0] < i+len(text) {
			render(x0[0], y0, z, sx, sy, text[:newLines[0]-i], tint, isEffect)

			x0 = x0[1:]
			y0 -= 0.7 * sy

			text = text[newLines[0]-i:]
			i = newLines[0]
			newLines = newLines[1:]
		}

		render(x0[0], y0, z, sx, sy, text, tint, isEffect)

		for _, l := range text {
			x0[0] += sprites.TextAdvance(font, l, sx)
		}

		i += len(text)
	}, d.Color, false, effect)
}

func (d *RichDescription) walk(f func(string, color.RGBA, bool), tint color.RGBA, isEffect bool, effect *EffectDef) {
	if d.Color != (color.RGBA{}) {
		tint = d.Color
	}

	if d.Effect == effect {
		isEffect = true
	}

	f(d.Text, tint, isEffect)

	for _, c := range d.Content {
		c.walk(f, tint, isEffect, effect)
	}
}

// String implements fmt.Stringer.
func (d *RichDescription) String() string {
	return string(d.string(nil))
}

func (d *RichDescription) string(b []byte) []byte {
	b = append(b, d.Text...)

	for _, c := range d.Content {
		b = c.string(b)
	}

	return b
}

// Description returns a description of this card.
func (cd *Def) Description(set *Set) *RichDescription {
	desc := &RichDescription{Color: color.RGBA{0, 0, 0, 255}}

	for _, e := range cd.Effects {
		effectDesc := e.Description(cd, set)

		if len(desc.Content) != 0 && len(effectDesc) != 0 {
			desc.Content = append(desc.Content, &RichDescription{Text: "\n"})
		}

		desc.Content = append(desc.Content, effectDesc...)

		if e.Type == FlavorText && e.Flags&FlagFlavorTextHideRemaining != 0 {
			break
		}
	}

	return desc
}

// Description returns a description of this effect.
func (e *EffectDef) Description(card *Def, set *Set, parents ...*EffectDef) []*RichDescription {
	switch e.Type {
	case FlavorText:
		return e.describeFlavorText()
	case EffectStat:
		return e.describeEffectStat(parents)
	case EffectEmpower:
		return e.describeEffectEmpower(set)
	case EffectSummon:
		return e.describeEffectSummon(card, set)
	case EffectHeal:
		return e.describeEffectHeal()
	case EffectTP:
		return e.describeEffectTP()
	case EffectNumb:
		return e.describeEffectNumb(set)
	case EffectRawStat:
		return e.describeEffectRawStat()
	case EffectMultiplyHealing:
		return e.describeEffectMultiplyHealing()
	case EffectPreventNumb:
		return e.describeEffectPreventNumb(set)
	case EffectModifyAvailableCards:
		return e.describeEffectModifyAvailableCards(set)
	case EffectModifyCardCost:
		return e.describeEffectModifyCardCost(set)
	case EffectDrawCard:
		return e.describeEffectDrawCard()
	case CondCard:
		return e.describeCondCard(card, set, parents)
	case CondLimit:
		return e.describeCondLimit(card, set, parents)
	case CondWinner:
		return e.describeCondWinner(card, set, parents)
	case CondApply:
		return e.describeCondApply(card, set, parents)
	case CondCoin:
		return e.describeCondCoin(card, set, parents)
	case CondStat:
		return e.describeCondStat(card, set, parents)
	case CondInHand:
		return e.describeCondInHand(card, set, parents)
	case CondLastEffect:
		return e.describeCondLastEffect(card, set, parents)
	case CondOnNumb:
		return e.describeCondOnNumb(card, set, parents)
	case CondMultipleEffects:
		return e.describeCondMultipleEffects(card, set, parents)
	}

	return []*RichDescription{
		{
			Effect: e,
			Text:   "ERROR: unhandled effect type " + e.Type.String(),
			Color:  color.RGBA{255, 0, 0, 255},
		},
	}
}

func (e *EffectDef) describeAmount(infFlag EffectFlag) string {
	if e.Flags&infFlag != 0 {
		if e.Amount < 0 {
			return "-∞"
		}

		return "∞"
	}

	return strconv.FormatInt(e.Amount, 10)
}

func (e *EffectDef) describeCardName(set *Set, id ID) string {
	if c := set.Card(id); c != nil {
		return c.DisplayName()
	}

	return "MissingCard?" + strconv.FormatUint(uint64(id), 10) + "?"
}

func (e *EffectDef) describeFilter(set *Set) string {
	if e.Filter.IsSingleCard() {
		return "Card " + e.describeCardName(set, e.Filter[0].CardID)
	}

	var (
		tribes []string
		ranks  uint64

		hiddenTribe bool
	)

	for _, f := range e.Filter {
		switch f.Type {
		case FilterTribe:
			if f.Tribe == TribeCustom {
				if strings.HasPrefix(f.CustomTribe, "_") {
					hiddenTribe = true
				} else {
					tribes = append(tribes, f.CustomTribe)
				}
			} else {
				tribes = append(tribes, f.Tribe.String())
			}
		case FilterNotTribe:
			if f.Tribe == TribeCustom {
				if strings.HasPrefix(f.CustomTribe, "_") {
					hiddenTribe = true
				} else {
					tribes = append(tribes, "NOT "+f.CustomTribe)
				}
			} else {
				tribes = append(tribes, "NOT "+f.Tribe.String())
			}
		case FilterRank:
			ranks |= 1 << f.Rank
		}
	}

	if hiddenTribe && len(tribes) == 0 {
		tribes = append(tribes, "(hidden tribe)")
	}

	var rankNames []string

	if ranks == 1<<Attacker|1<<Effect {
		rankNames = []string{"Enemy"}
	} else {
		for r := Attacker; r < RankNone; r++ {
			if ranks&(1<<r) != 0 {
				rankNames = append(rankNames, r.String())
			}
		}
	}

	if len(rankNames) == 0 {
		if len(tribes) == 0 {
			return "all"
		}

		return strings.Join(tribes, "/")
	}

	rankName := strings.Join(rankNames, " or ")

	if len(tribes) == 0 {
		return rankName
	}

	return strings.Join(tribes, "/") + " " + rankName
}

func (e *EffectDef) describeFlavorText() []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Text: e.Text,

			Color: color.RGBA{34, 102, 136, 255},
		},
	}
}

func (e *EffectDef) describeEffectStat(parents []*EffectDef) []*RichDescription {
	var text []byte

	if e.Flags&FlagStatDEF != 0 {
		text = []byte("DEF")
	} else {
		text = []byte("ATK")
	}

	if len(parents) == 0 || (len(parents) == 1 && parents[0].Type == CondLimit) {
		text = append(text, ": "...)
	} else if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagStatInfinity)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectEmpower(set *Set) []*RichDescription {
	var text []byte

	if e.Flags&FlagStatDEF != 0 {
		if e.Amount < 0 {
			text = []byte("Break ")
		} else {
			text = []byte("Fortify ")
		}
	} else {
		if e.Amount < 0 {
			text = []byte("Enfeeble ")
		} else {
			text = []byte("Empower ")
		}
	}

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagStatInfinity)...)

	text = append(text, " ("...)
	text = append(text, e.describeFilter(set)...)
	text = append(text, ')')

	if e.Flags&FlagStatOpponent != 0 {
		text = append(text, " (opponent)"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectSummon(card *Def, set *Set) []*RichDescription {
	var text []byte

	if e.Flags&FlagSummonReplace != 0 {
		text = []byte("Turns into ")
	} else {
		text = []byte("Summon ")
	}

	if e.Amount != 1 {
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, ' ')

		if !e.Filter.IsSingleCard() {
			text = append(text, "random "...)
		}
	} else if !e.Filter.IsSingleCard() {
		text = append(text, "a random "...)
	}

	if e.Filter.IsSingleCard() {
		if e.Flags&FlagSummonReplace != 0 && e.Filter[0].CardID == card.ID && e.Amount == 1 {
			text = []byte("Resummon")
		} else {
			c := set.Card(e.Filter[0].CardID)

			if c != nil {
				text = append(text, c.DisplayName()...)
			} else {
				text = append(text, "MissingCard?"...)
				text = strconv.AppendUint(text, uint64(e.Filter[0].CardID), 10)
				text = append(text, '?')
			}
		}
	} else {
		text = append(text, e.describeFilter(set)...)

		if e.Amount == 1 {
			text = append(text, " card"...)
		} else {
			text = append(text, " cards"...)
		}
	}

	if e.Flags&FlagSummonOpponent != 0 {
		text = append(text, " as opponent"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectHeal() []*RichDescription {
	var before, amount, after string

	if e.Flags&FlagHealOpponent != 0 {
		if e.Amount < 0 {
			before = "Damage Opponent "
		} else {
			before = "Heal Opponent "
		}
	} else {
		if e.Amount < 0 {
			before = "Take "
			after = " Damage"
		} else {
			before = "Heal\u00a0"
		}
	}

	if e.Flags&FlagHealRaw != 0 {
		after += " (ignoring modifiers)"
	}

	switch {
	case e.Flags&FlagHealInfinity != 0:
		amount = "∞"
	case e.Amount >= 0:
		amount = strconv.FormatInt(e.Amount, 10)
	default:
		amount = strconv.FormatInt(-e.Amount, 10)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: before + amount + after,
		},
	}
}

func (e *EffectDef) describeEffectTP() []*RichDescription {
	text := []byte("TP")

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagTPInfinity)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectNumb(set *Set) []*RichDescription {
	text := []byte("Numb")

	if e.Flags&FlagNumbSelf != 0 {
		text = append(text, " Own"...)
	}

	if len(e.Filter) != 1 || e.Filter[0].Type != FilterRank || e.Filter[0].Rank != Attacker {
		text = append(text, ' ')
		text = append(text, e.describeFilter(set)...)
	}

	text = append(text, "\u00a0("...)

	text = append(text, e.describeAmount(FlagNumbInfinity)...)

	text = append(text, ')')

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectRawStat() []*RichDescription {
	if e.Flags&(FlagStatDEF|FlagStatOpponent) == FlagStatDEF|FlagStatOpponent {
		e.Amount = -e.Amount
		amount := e.describeAmount(FlagStatInfinity)
		e.Amount = -e.Amount

		return []*RichDescription{
			{
				Effect: e,

				Text: "Pierce (" + amount + ")",
			},
		}
	}

	var text []byte

	if e.Flags&FlagStatDEF != 0 {
		text = []byte("DEF")
	} else {
		text = []byte("ATK")
	}

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = append(text, e.describeAmount(FlagStatInfinity)...)

	if e.Flags&FlagStatOpponent != 0 {
		text = append(text, " for opponent"...)
	} else {
		text = append(text, " for self"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectMultiplyHealing() []*RichDescription {
	var when, what, who string

	switch e.Flags & FlagMultiplyHealingTypeMask {
	case FlagMultiplyHealingTypeAll:
		what = "Health Changes"
	case FlagMultiplyHealingTypePositive:
		what = "Healing"
	case FlagMultiplyHealingTypeNegative:
		what = "Damage Effects"
	}

	if e.Flags&FlagMultiplyHealingFuture != 0 {
		when = "Future "
	}

	if e.Flags&FlagMultiplyHealingOpponent != 0 {
		who = " for opponent"
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: "Multiply " + when + what + " by " + strconv.FormatInt(e.Amount, 10) + who,
		},
	}
}

func (e *EffectDef) describeEffectPreventNumb(set *Set) []*RichDescription {
	text := []byte("Prevent Numb")

	if e.Flags&FlagPreventNumbOpponent != 0 {
		text = append(text, " Opponent"...)
	}

	text = append(text, e.describeFilter(set)...)

	text = append(text, " ("...)
	text = strconv.AppendInt(text, e.Amount, 10)
	text = append(text, ')')

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectModifyAvailableCards(set *Set) []*RichDescription {
	var text []byte

	switch e.Flags & (FlagModifyCardsOpponent | FlagModifyCardsTypeMask | FlagModifyCardsTargetMask) {
	case FlagModifyCardsTypeAdd | FlagModifyCardsTargetAny:
		text = []byte("Summon To Hand")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeAdd | FlagModifyCardsTargetAny:
		text = []byte("Summon To Opponent's Hand")
	case FlagModifyCardsTypeAdd | FlagModifyCardsTargetHand:
		text = []byte("Add To Hand")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeAdd | FlagModifyCardsTargetHand:
		text = []byte("Add To Opponent's Hand")
	case FlagModifyCardsTypeAdd | FlagModifyCardsTargetDeck:
		text = []byte("Add To Deck")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeAdd | FlagModifyCardsTargetDeck:
		text = []byte("Add To Opponent's Deck")
	case FlagModifyCardsTypeAdd | FlagModifyCardsTargetField:
		text = []byte("Add To Field")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeAdd | FlagModifyCardsTargetField:
		text = []byte("Add To Opponent's Field")
	case FlagModifyCardsTypeRemove | FlagModifyCardsTargetAny:
		text = []byte("Exile")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeRemove | FlagModifyCardsTargetAny:
		text = []byte("Exile Opponent")
	case FlagModifyCardsTypeRemove | FlagModifyCardsTargetHand:
		text = []byte("Exile From Hand")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeRemove | FlagModifyCardsTargetHand:
		text = []byte("Exile From Opponent's Hand")
	case FlagModifyCardsTypeRemove | FlagModifyCardsTargetDeck:
		text = []byte("Exile From Deck")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeRemove | FlagModifyCardsTargetDeck:
		text = []byte("Exile From Opponent's Deck")
	case FlagModifyCardsTypeRemove | FlagModifyCardsTargetField:
		text = []byte("Exile From Field")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeRemove | FlagModifyCardsTargetField:
		text = []byte("Exile From Opponent's Field")
	case FlagModifyCardsTypeMove | FlagModifyCardsTargetAny:
		text = []byte("Play")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeMove | FlagModifyCardsTargetAny:
		text = []byte("Play Opponent")
	case FlagModifyCardsTypeMove | FlagModifyCardsTargetHand:
		text = []byte("Play From Hand")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeMove | FlagModifyCardsTargetHand:
		text = []byte("Play From Opponent's Hand")
	case FlagModifyCardsTypeMove | FlagModifyCardsTargetDeck:
		text = []byte("Play From Deck")
	case FlagModifyCardsOpponent | FlagModifyCardsTypeMove | FlagModifyCardsTargetDeck:
		text = []byte("Play From Opponent's Deck")
	}

	text = append(text, " ("...)
	text = strconv.AppendInt(text, e.Amount, 10)
	text = append(text, "): "...)

	text = append(text, e.describeFilter(set)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectModifyCardCost(set *Set) []*RichDescription {
	text := []byte("Modify TP Cost (")

	if e.Amount >= 0 {
		text = append(text, '+')
	}

	text = strconv.AppendInt(text, e.Amount, 10)
	text = append(text, "): "...)

	text = append(text, e.describeFilter(set)...)

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeEffectDrawCard() []*RichDescription {
	var text []byte

	amount := e.Amount
	ignoreMax := e.Flags&FlagDrawCardIgnoreMax != 0

	if amount < 0 {
		amount = -amount
		ignoreMax = false

		if e.Flags&FlagDrawCardOpponent != 0 {
			text = []byte("Opponent Discards ")
		} else {
			text = []byte("Discard ")
		}
	} else {
		if e.Flags&FlagDrawCardOpponent != 0 {
			text = []byte("Opponent Draws ")
		} else {
			text = []byte("Draw ")
		}
	}

	text = strconv.AppendInt(text, amount, 10)

	if amount == 1 {
		text = append(text, " Card"...)
	} else {
		text = append(text, " Cards"...)
	}

	if ignoreMax {
		text = append(text, " (ignoring limit)"...)
	}

	return []*RichDescription{
		{
			Effect: e,

			Text: string(text),
		},
	}
}

func (e *EffectDef) describeCondCard(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text []byte

	switch e.Flags & (FlagCondCardTypeMask | FlagCondCardOpponent) {
	case FlagCondCardTypeGreaterEqual:
		text = []byte("If ")
	case FlagCondCardTypeLessThan:
		text = []byte("Unless ")
	case FlagCondCardTypeGreaterEqual | FlagCondCardOpponent:
		text = []byte("VS ")
	case FlagCondCardTypeLessThan | FlagCondCardOpponent:
		text = []byte("Unless VS ")
	case FlagCondCardTypeEach:
		text = []byte("Per ")
	case FlagCondCardTypeEach | FlagCondCardOpponent:
		if e.Amount != 1 {
			text = []byte("VS every ")
		} else {
			text = []byte("VS each ")
		}
	}

	if e.Flags&FlagCondCardTypeMask == FlagCondCardTypeEach && e.Amount != 1 {
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, ' ')
	}

	if e.Filter.IsSingleCard() {
		text = append(text, e.describeCardName(set, e.Filter[0].CardID)...)
	} else {
		text = append(text, e.describeFilter(set)...)
	}

	if e.Flags&FlagCondCardTypeMask != FlagCondCardTypeEach && (!e.Filter.IsSingleCard() || e.Amount != 1) {
		text = append(text, " ("...)
		text = strconv.AppendInt(text, e.Amount, 10)
		text = append(text, ')')
	}

	text = append(text, ": "...)

	return []*RichDescription{
		{
			Effect: e,

			Text:    string(text),
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondLimit(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	if e.Amount == 1 && e.Flags&FlagCondLimitGreaterThan == 0 && e.Result.Type == EffectEmpower && e.Result.Flags&(FlagStatOpponent|FlagStatDEF) == 0 {
		return []*RichDescription{
			{
				Effect: e,

				Content: []*RichDescription{
					{
						Effect: e.Result,

						Text: "Unity (" + e.Result.describeAmount(FlagStatInfinity) + ", " + e.Result.describeFilter(set) + ")",
					},
				},
			},
		}
	}

	if len(parents) == 0 {
		var suffix string

		switch {
		case e.Amount == 1 && e.Flags&FlagCondLimitGreaterThan == 0:
			suffix = " (only once)"
		case e.Flags&FlagCondLimitGreaterThan == 0:
			suffix = " (max. " + strconv.FormatInt(e.Amount, 10) + " times)"
		case e.Amount == 1:
			suffix = " (except the first time)"
		default:
			suffix = " (except the first " + strconv.FormatInt(e.Amount, 10) + " times)"
		}

		return []*RichDescription{
			{
				Effect: e,

				Content: append(e.Result.Description(card, set, append(parents, e)...), &RichDescription{
					Effect: e,

					Text: suffix,
				}),
			},
		}
	}

	var prefix string

	switch {
	case e.Amount == 1 && e.Flags&FlagCondLimitGreaterThan == 0:
		prefix = "Only Once: "
	case e.Flags&FlagCondLimitGreaterThan == 0:
		prefix = "Max. " + strconv.FormatInt(e.Amount, 10) + " Times: "
	case e.Amount == 1:
		prefix = "Except First Time: "
	default:
		prefix = "Except First " + strconv.FormatInt(e.Amount, 10) + " Times: "
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    prefix,
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondWinner(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text string

	switch e.Flags & FlagCondWinnerTypeMask {
	case FlagCondWinnerTypeWinner:
		text = "On Win: "

		if e.Result.Type == EffectHeal && e.Result.Flags&FlagHealOpponent == 0 {
			return []*RichDescription{
				{
					Effect: e,

					Content: []*RichDescription{
						{
							Effect: e.Result,

							Text: "Lifesteal\u00a0(" + strconv.FormatInt(e.Result.Amount, 10) + ")",
						},
					},
				},
			}
		}
	case FlagCondWinnerTypeLoser:
		text = "On Lose: "
	case FlagCondWinnerTypeTie:
		text = "On Tie: "
	case FlagCondWinnerTypeNotTie:
		text = "Unless Tie: "
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    text,
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondApply(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	d := &RichDescription{
		Effect: e,

		Content: e.Result.Description(card, set, append(parents, e)...),
	}

	if e.Flags&FlagCondApplyOpponent != 0 {
		d.Content = append(d.Content, &RichDescription{
			Effect: e,

			Text: " as opponent",
		})
	}

	if e.Flags&FlagCondApplyNextRound != 0 && (len(parents) == 0 || parents[len(parents)-1].Type != CondApply || parents[len(parents)-1].Flags&(FlagCondApplyOriginalText|FlagCondApplyOpponent|FlagCondApplyNextRound) != e.Flags&(FlagCondApplyOriginalText|FlagCondApplyNextRound)) {
		d.Text = "Setup ("

		turns := 0

		for c := e; c.Type == CondApply && c.Flags&(FlagCondApplyOriginalText|FlagCondApplyNextRound) == e.Flags&(FlagCondApplyOriginalText|FlagCondApplyNextRound); c = c.Result {
			turns++

			if c.Flags&FlagCondApplyOpponent != 0 {
				break
			}
		}

		if turns != 1 {
			d.Text += strconv.Itoa(turns) + " turns) ("
		}

		d.Content = append(d.Content, &RichDescription{
			Effect: e,

			Text: ")",
		})
	}

	return []*RichDescription{d}
}

func (e *EffectDef) describeCondCoin(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	d := &RichDescription{
		Effect: e,

		Text:    "Coin\u00a0(" + strconv.FormatInt(e.Amount, 10) + "): ",
		Content: e.Result.Description(card, set, append(parents, e)...),
	}

	if e.Flags&FlagCondCoinTails != 0 {
		d.Content = append(d.Content, &RichDescription{
			Effect: e,
			Text:   " or ",
		})

		d.Content = append(d.Content, e.Result2.Description(card, set, append(parents, e)...)...)
	}

	return []*RichDescription{d}
}

func (e *EffectDef) describeCondStat(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	var text []byte

	switch e.Flags & (FlagCondStatLessThan | FlagCondStatOpponent) {
	case 0:
		text = []byte("If ")
	case FlagCondStatLessThan:
		text = []byte("Unless ")
	case FlagCondStatOpponent:
		text = []byte("VS ")
	case FlagCondStatLessThan | FlagCondStatOpponent:
		text = []byte("Unless VS ")
	}

	switch e.Flags & FlagCondStatTypeMask {
	case FlagCondStatTypeATK:
		text = append(text, "ATK ("...)
	case FlagCondStatTypeDEF:
		text = append(text, "DEF ("...)
	case FlagCondStatTypeHP:
		text = append(text, "HP ("...)
	case FlagCondStatTypeTP:
		text = append(text, "TP ("...)
	}

	text = strconv.AppendInt(text, e.Amount, 10)

	if e.Flags&FlagCondStatMultiple != 0 {
		text = append(text, '*')
	}

	text = append(text, "): "...)

	return []*RichDescription{
		{
			Effect: e,

			Text:    string(text),
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondInHand(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	text := "In Hand"
	if e.Flags&FlagCondInHandOnPlay == 0 {
		text = "Not Played"
	}

	switch {
	case e.Flags&FlagCondInHandOnPlay != 0 && e.Amount == 0 && e.Amount2 == 1:
		text = "On Draw"
	case e.Amount != 0 && e.Amount2 == 1:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " turn after " + strconv.FormatInt(e.Amount, 10) + ")"
	case e.Amount != 0 && e.Amount2 != 0:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " turns after " + strconv.FormatInt(e.Amount, 10) + ")"
	case e.Amount == 1:
		text += " (after " + strconv.FormatInt(e.Amount, 10) + " turn)"
	case e.Amount != 0:
		text += " (after " + strconv.FormatInt(e.Amount, 10) + " turns)"
	case e.Amount2 == 1:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " turn)"
	case e.Amount2 != 0:
		text += " (for " + strconv.FormatInt(e.Amount2, 10) + " turns)"
	}

	return []*RichDescription{
		{
			Effect: e,

			Text:    text + ": ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondLastEffect(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Content: append(
				e.Result.Description(card, set, append(parents, e)...),
				&RichDescription{
					Text: " (final)",
				},
			),
		},
	}
}

func (e *EffectDef) describeCondOnNumb(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Text:    "On Numb: ",
			Content: e.Result.Description(card, set, append(parents, e)...),
		},
	}
}

func (e *EffectDef) describeCondMultipleEffects(card *Def, set *Set, parents []*EffectDef) []*RichDescription {
	return []*RichDescription{
		{
			Effect: e,

			Content: append(
				append(
					e.Result.Description(card, set, append(parents, e)...),
					&RichDescription{
						Effect: e,
						Text:   " and ",
					},
				),
				e.Result2.Description(card, set, append(parents, e)...)...,
			),
		},
	}
}
