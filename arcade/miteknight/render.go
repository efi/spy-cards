// +build !headless

package miteknight

import (
	"context"
	"fmt"
	"math"
	"time"

	"git.lubar.me/ben/spy-cards/arcade"
	"git.lubar.me/ben/spy-cards/arcade/game"
	"git.lubar.me/ben/spy-cards/gfx"
	"git.lubar.me/ben/spy-cards/gfx/sprites"
	"golang.org/x/mobile/gl"
)

func clearHUD() {
	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)
}

// Render implements game.Interface.
func (mk *MiteKnight) Render() {
	mk.renderWorld()
	mk.renderHUD()
}

func (mk *MiteKnight) renderWorld() {
	mk.state.Camera.Perspective = gfx.Perspective(-60*math.Pi/180, 16.0/9.0, 0.1, 50)
	mk.state.Camera.Offset = gfx.Translation(0, 0.5, -0.5)
	mk.state.Camera.Rotation = gfx.RotationY(mk.camRotation)

	if mk.rotateX {
		mk.state.Camera.Perspective = gfx.Perspective(-90*math.Pi/180, 16.0/9.0, 5, 250)
		mk.state.Camera.Rotation = gfx.RotationX(75 * math.Pi / 180)
	}

	mk.state.Camera.Position = gfx.Translation(mk.camPos.x, mk.camPos.y, mk.camPos.z)

	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	if mk.floor >= len(mk.floors) {
		return
	}

	floor := mk.floors[mk.floor]

	gfx.GL.Disable(gl.BLEND)
	gfx.GL.Enable(gl.DEPTH_TEST)

	gfx.GL.Enable(gl.CULL_FACE)
	gfx.GL.UseProgram(geomProgram.Program)
	gfx.GL.Uniform1i(geomTex.Uniform, 0)
	gfx.GL.UniformMatrix4fv(geomPerspective.Uniform, mk.state.Camera.Perspective[:])
	combined := mk.state.Camera.Combined()
	gfx.GL.UniformMatrix4fv(geomCamera.Uniform, combined[:])
	gfx.GL.Uniform1f(geomTime.Uniform, float32(mk.state.TickCount)/game.FrameRate)

	if mk.rotateX {
		gfx.GL.Uniform1f(geomFogEnd.Uniform, 10000)
	} else {
		gfx.GL.Uniform1f(geomFogEnd.Uniform, 5)
	}

	if err := sprites.MKWall.Preload(); err != nil {
		return
	}

	gfx.Lock.Lock()

	gfx.GL.ActiveTexture(gl.TEXTURE0)
	gfx.GL.BindTexture(gl.TEXTURE_2D, sprites.MKWall.Texture())

	for _, b := range floor.staticGeometry {
		gfx.GL.BindBuffer(gl.ARRAY_BUFFER, b.Data)
		gfx.GL.VertexAttribPointer(posAttrib.Attrib, 3, gl.UNSIGNED_BYTE, false, 8, 0)
		gfx.GL.VertexAttribPointer(texAttrib.Attrib, 2, gl.UNSIGNED_BYTE, false, 8, 3)
		gfx.GL.VertexAttribPointer(colorAttrib.Attrib, 3, gl.UNSIGNED_BYTE, true, 8, 5)
		gfx.GL.EnableVertexAttribArray(posAttrib.Attrib)
		gfx.GL.EnableVertexAttribArray(texAttrib.Attrib)
		gfx.GL.EnableVertexAttribArray(colorAttrib.Attrib)
		gfx.GL.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, b.Element)
		gfx.GL.DrawElements(gl.TRIANGLES, b.Count, gl.UNSIGNED_SHORT, 0)
	}

	gfx.Lock.Unlock()

	gfx.GL.Disable(gl.CULL_FACE)

	for i := 0; i < len(mk.particles); i++ {
		mk.drawParticle(mk.particles[i])

		if mk.particles[i].end.Before(time.Now()) {
			mk.particles = append(mk.particles[:i], mk.particles[i+1:]...)
			i--
		}
	}

	if mk.batch == nil {
		mk.batch = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.batch.Reset(&mk.state.Camera)
	}

	if !floor.gotKey || floor.hasKey {
		mk.drawEntity(mk.batch, floor.door)
	}

	for _, item := range floor.items {
		mk.drawEntity(mk.batch, item)
	}

	for _, enemy := range floor.enemies {
		if !enemy.dead || enemy.dying {
			mk.drawEntity(mk.batch, enemy)
		}
	}

	mk.batch.Render()

	gfx.GL.Disable(gl.DEPTH_TEST)

	mk.batch.Reset(&mk.state.Camera)

	mk.drawEntity(mk.batch, floor.player)

	mk.batch.Render()

	gfx.GL.Enable(gl.BLEND)

	if mk.compassEnabled {
		mk.state.Camera.PushTransform(gfx.Translation(mk.compassPos.x, mk.compassPos.y, mk.compassPos.z))
		mk.state.Camera.PushTransform(gfx.RotationX(-math.Pi / 2))
		mk.state.Camera.PushTransform(gfx.RotationZ(mk.compassRotation))

		mk.batch.Reset(&mk.state.Camera)

		mk.batch.Append(sprites.MKCompass, 0, 0, 0, 0.55, 0.55, sprites.Yellow, 0, 0, 0, 0)

		mk.batch.Render()

		mk.state.Camera.PopTransform()
		mk.state.Camera.PopTransform()
		mk.state.Camera.PopTransform()
	}
}

func (mk *MiteKnight) drawEntity(batch *sprites.Batch, entity *entity) {
	x := float32(entity.x) + entity.ox()
	y := entity.height
	z := float32(entity.z) + entity.oz()

	bounce := entity.bounce
	if mk.state.TickCount < entity.tempBounce {
		bounce = 1.5
	}

	sx, sy := entityScale[entity.entityType], entityScale[entity.entityType]
	if entity.flipX {
		sx = -sx
	}

	bounceTime := float32(mk.state.TickCount)
	if mk.rules.PauseStopsAnimations && mk.pause != 0 {
		bounceTime = float32(mk.pause)
	}

	bounceTime = bounceTime / game.FrameRate * 7 * bounce

	sx += float32(math.Sin(float64(bounceTime))) * 0.1 * bounce * 0.75
	sy += float32(math.Cos(float64(bounceTime+1))) * 0.1 * bounce * 0.75

	blink := uint64(3)

	if entity.blinkDelay != 0 {
		blink = (mk.state.TickCount - entity.blinkStart) / entity.blinkDelay

		if blink > 2 {
			entity.blinkDelay = 0
		}
	}

	color := entityColor[entity.entityType]
	if blink == 0 || blink == 2 {
		color = sprites.White
	}

	rotationY := -mk.camRotation
	if entity.entityType == entityDoor {
		rotationY = 0
	}

	var flags sprites.RenderFlag

	if entity.billboard {
		flags |= sprites.FlagBillboard
	}

	if entity.fog {
		flags |= sprites.FlagMKFog
	}

	batch.Append(entity.sprite, x, y, z, sx, sy, color, flags, rotationY, entity.rotationZ, 0)
}

func (mk *MiteKnight) drawParticle(p *particleEffect) {
	lerp := float32(time.Since(p.start)) / float32(p.end.Sub(p.start))
	if lerp < 0 || lerp > 1 {
		return
	}

	dampened := lerp * float32(math.Pow(float64(1-p.def.dampen), float64(lerp*p.def.lifetime)))

	if p.def.additive {
		gfx.GL.Enable(gl.BLEND)
		gfx.GL.BlendFunc(gl.ONE, gl.ONE)
	}

	flags := sprites.FlagBillboard

	if p.def.additive {
		flags |= sprites.FlagNoDiscard
	}

	if mk.batch == nil {
		mk.batch = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.batch.Reset(&mk.state.Camera)
	}

	mk.batch.Append(
		p.sprite,
		p.initialPosition.x+p.initialOffset.x+p.velocity.x*dampened,
		p.initialPosition.y+p.initialOffset.y+p.velocity.y*dampened-p.def.gravity*(lerp*p.def.lifetime)*(lerp*p.def.lifetime+0.25),
		p.initialPosition.z+p.initialOffset.z+p.velocity.z*dampened,
		p.size*p.def.sizeFunc(lerp), p.size*p.def.sizeFunc(lerp), p.color,
		flags,
		-mk.camRotation,
		p.initialAngle+p.angular*lerp*p.def.lifetime,
		0,
	)

	mk.batch.Render()

	if p.def.additive {
		gfx.GL.Disable(gl.BLEND)
		gfx.GL.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)
	}
}

// Cleanup implements game.Interface.
func (mk *MiteKnight) Cleanup(ctx context.Context) error {
	for _, floor := range mk.floors {
		for _, b := range floor.staticGeometry {
			b.Delete()
		}
	}

	gfx.GL.DisableVertexAttribArray(posAttrib.Attrib)
	gfx.GL.DisableVertexAttribArray(texAttrib.Attrib)
	gfx.GL.DisableVertexAttribArray(colorAttrib.Attrib)

	return ctx.Err()
}

// RenderAttract implements game.Interface.
func (mk *MiteKnight) RenderAttract(progress int, fadeIn, fadeOut bool) {
	gfx.GL.ClearColor(0, 0, 0, 1)
	gfx.GL.Clear(gl.COLOR_BUFFER_BIT)

	if mk.batch == nil {
		mk.batch = sprites.NewBatch(&mk.state.Camera)
	} else {
		mk.batch.Reset(&mk.state.Camera)
	}

	mk.batch.Append(sprites.AttractMK, 0, 70, 0, 1, 1, sprites.White, 0, 0, 0, 0)

	mk.batch.Render()

	if mk.tb == nil {
		mk.tb = sprites.NewTextBatch(&mk.state.Camera)
	} else {
		mk.tb.Reset(&mk.state.Camera)
	}

	sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, "Mite Knight", 0, 74.8, 0, 2, 2, sprites.White, true)

	if !fadeIn && !fadeOut {
		if progress <= 100 {
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, fmt.Sprintf("Preparing Dungeon... (%d%%)", progress), 0, 68, 0, 1.5, 1.5, sprites.White, true)
		} else if math.Sin(float64(mk.state.TickCount)*5/60) > 0 {
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(arcade.BtnConfirm)+" TO START!", 0, 68, 0, 1.5, 1.5, sprites.White, true)
		}
	}

	mk.tb.Render()
}

func (mk *MiteKnight) renderHUD() {
	mk.state.Camera.SetDefaults()
	mk.state.Camera.Rotation = gfx.Identity()
	mk.state.Camera.Offset = gfx.Identity()

	if mk.overrideHUD != nil {
		mk.overrideHUD()
	} else {
		mk.state.Camera.PushTransform(gfx.Translation(0, 70, 0))

		if mk.tb == nil {
			mk.tb = sprites.NewTextBatch(&mk.state.Camera)
		} else {
			mk.tb.Reset(&mk.state.Camera)
		}

		mk.state.DrawButtons(mk.tb, -10, -4.5, 10, 2)

		sprites.DrawText(mk.tb, sprites.FontD3Streetism, fmt.Sprintf("Score: %0[2]*.0[1]f", mk.state.Score, int(mk.rules.ScoreDigits)), -10, -5.5, 10, 1.25, 1.25, sprites.White)
		sprites.DrawText(mk.tb, sprites.FontD3Streetism, fmt.Sprintf("Floor %d", mk.floor+1), 6.5, -5.5, 10, 1.25, 1.25, sprites.White)
		sprites.DrawText(mk.tb, sprites.FontD3Streetism, mk.timeText, -1, 4.35, 10, 1.5, 1.5, sprites.White)

		if mk.batch == nil {
			mk.batch = sprites.NewBatch(&mk.state.Camera)
		} else {
			mk.batch.Reset(&mk.state.Camera)
		}

		floor := mk.floors[mk.floor]

		for i := 0; i < floor.player.maxhp; i++ {
			filled := floor.player.hp > i

			sprite := sprites.MKHeartEmpty
			if filled {
				sprite = sprites.MKHeartFull
			}

			color := sprites.Gray
			if filled {
				color = sprites.Red
			}

			mk.batch.Append(sprite, -9+float32(i)*1.25, 4.8, 10, 3, 3, color, 0, 0, 0, 0)
		}

		if floor.hasKey {
			mk.batch.Append(sprites.MKKey, 9, 4.5, 10, 3, 3, sprites.Yellow, 0, 0, 0, 0)
		}

		mk.batch.Render()

		if mk.state.Playback != nil {
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, "Recorded by "+mk.state.Playback.PlayerName, 4.7, 4.8, 10, 0.8, 0.8, sprites.White, false)
			sprites.DrawTextCentered(mk.tb, sprites.FontD3Streetism, mk.state.Playback.Start.Format("Jan 2, 2006"), 4.7, 4.1, 10, 0.8, 0.8, sprites.White, false)
		}

		mk.tb.Render()

		if mk.pause != 0 {
			mk.state.Camera.PushTransform(gfx.Translation(-0.25, 0.2, 0.7))
			mk.state.Camera.PushTransform(gfx.Scale(0.075, 0.075, 0.075))

			mk.tb.Reset(&mk.state.Camera)

			sprites.DrawTextShadow(mk.tb, sprites.FontD3Streetism, "PAUSED", 0, 0, 0, 1, 1, sprites.White)
			sprites.DrawTextShadow(mk.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(arcade.BtnConfirm)+" TO CONTINUE", 0, -0.7*2, 0, 1, 1, sprites.White)
			sprites.DrawTextShadow(mk.tb, sprites.FontD3Streetism, "PRESS "+sprites.Button(arcade.BtnCancel)+" TO EXIT", 0, -0.7*3.5, 0, 1, 1, sprites.White)

			mk.tb.Render()

			mk.state.Camera.PopTransform()
			mk.state.Camera.PopTransform()
		}

		mk.state.Camera.PopTransform()
	}
}
