// +build js,wasm

package internal

import (
	"context"
	"io"
	"net/http"

	"golang.org/x/xerrors"
)

// OpenAsset opens a local asset or performs an HTTP request.
func OpenAsset(ctx context.Context, name string) (io.ReadCloser, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, name, nil)
	if err != nil {
		return nil, xerrors.Errorf("loading asset %q: %w", name, err)
	}

	resp, err := http.DefaultClient.Do(req) //nolint:bodyclose
	if err != nil {
		return nil, xerrors.Errorf("loading asset %q: %w", name, err)
	}

	return resp.Body, nil
}
