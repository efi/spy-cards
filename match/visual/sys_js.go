// +build js,wasm
// +build !headless

package visual

import (
	"context"
	"strconv"
	"strings"
	"syscall/js"

	"git.lubar.me/ben/spy-cards/match"
)

func displayCopyableText(ctx context.Context, label, text string) {
	doc := js.Global().Get("document")

	jsLabel := doc.Call("createElement", "label")
	jsLabel.Set("className", "copyable-text")
	jsLabel.Set("textContent", label+" ")

	jsInput := doc.Call("createElement", "input")
	jsInput.Set("type", "text")
	jsInput.Set("readOnly", true)
	jsInput.Set("value", text)
	jsLabel.Call("appendChild", jsInput)

	doc.Get("body").Call("appendChild", jsLabel)
	jsInput.Call("select")

	<-ctx.Done()

	if p := jsLabel.Get("parentNode"); p.Truthy() {
		p.Call("removeChild", jsLabel)
	}
}

func displayPastableText(ctx context.Context, label string, f func(string)) {
	doc := js.Global().Get("document")

	jsLabel := doc.Call("createElement", "label")
	jsLabel.Set("className", "copyable-text")
	jsLabel.Set("textContent", label+" ")

	jsInput := doc.Call("createElement", "input")
	jsInput.Set("type", "text")

	onInput := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		f(jsInput.Get("value").String())

		return js.Undefined()
	})
	defer onInput.Release()

	jsInput.Set("oninput", onInput)
	jsLabel.Call("appendChild", jsInput)

	doc.Get("body").Call("appendChild", jsLabel)
	jsInput.Call("focus")

	<-ctx.Done()

	if p := jsLabel.Get("parentNode"); p.Truthy() {
		p.Call("removeChild", jsLabel)
	}
}

func (v *Visual) onModeChange() {
	var suffix string

	switch v.Match.Init.Mode {
	case "":
		// no suffix
	case match.ModeCustom:
		suffix = "?custom"

		if v.state == StateInCardEditor {
			suffix += "=editor"
		} else if v.Match.Init.CachedVariant != nil {
			suffix += "&variant=" + strconv.FormatUint(v.Match.Init.Variant, 10)
		}

		if v.Match.Init.Custom != "" {
			suffix += "#" + v.Match.Init.Custom
		}
	default:
		i := strings.IndexByte(v.Match.Init.Mode, '.')

		suffix = "?mode=" + v.Match.Init.Mode[:i]

		if !v.Match.Init.Latest {
			suffix += "&rev=" + v.Match.Init.Mode[i+1:]
		}

		if v.Match.Init.CachedVariant != nil {
			suffix += "&variant=" + strconv.FormatUint(v.Match.Init.Variant, 10)
		}
	}

	js.Global().Get("history").Call("replaceState",
		js.Null(),
		js.Global().Get("document").Get("title"),
		js.Global().Get("location").Get("pathname").String()+suffix,
	)
}
