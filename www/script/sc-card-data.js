"use strict";
var SpyCards;
(function (SpyCards) {
    var CardData;
    (function (CardData) {
        let GlobalCardID;
        (function (GlobalCardID) {
            GlobalCardID[GlobalCardID["Zombiant"] = 0] = "Zombiant";
            GlobalCardID[GlobalCardID["Jellyshroom"] = 1] = "Jellyshroom";
            GlobalCardID[GlobalCardID["Spider"] = 2] = "Spider";
            GlobalCardID[GlobalCardID["Zasp"] = 3] = "Zasp";
            GlobalCardID[GlobalCardID["Cactiling"] = 4] = "Cactiling";
            GlobalCardID[GlobalCardID["Psicorp"] = 5] = "Psicorp";
            GlobalCardID[GlobalCardID["Thief"] = 6] = "Thief";
            GlobalCardID[GlobalCardID["Bandit"] = 7] = "Bandit";
            GlobalCardID[GlobalCardID["Inichas"] = 8] = "Inichas";
            GlobalCardID[GlobalCardID["Seedling"] = 9] = "Seedling";
            GlobalCardID[GlobalCardID["Numbnail"] = 14] = "Numbnail";
            GlobalCardID[GlobalCardID["Mothiva"] = 15] = "Mothiva";
            GlobalCardID[GlobalCardID["Acornling"] = 16] = "Acornling";
            GlobalCardID[GlobalCardID["Weevil"] = 17] = "Weevil";
            GlobalCardID[GlobalCardID["VenusBud"] = 19] = "VenusBud";
            GlobalCardID[GlobalCardID["Chomper"] = 20] = "Chomper";
            GlobalCardID[GlobalCardID["AcolyteAria"] = 21] = "AcolyteAria";
            GlobalCardID[GlobalCardID["Kabbu"] = 23] = "Kabbu";
            GlobalCardID[GlobalCardID["VenusGuardian"] = 24] = "VenusGuardian";
            GlobalCardID[GlobalCardID["WaspTrooper"] = 25] = "WaspTrooper";
            GlobalCardID[GlobalCardID["WaspBomber"] = 26] = "WaspBomber";
            GlobalCardID[GlobalCardID["WaspDriller"] = 27] = "WaspDriller";
            GlobalCardID[GlobalCardID["WaspScout"] = 28] = "WaspScout";
            GlobalCardID[GlobalCardID["Midge"] = 29] = "Midge";
            GlobalCardID[GlobalCardID["Underling"] = 30] = "Underling";
            GlobalCardID[GlobalCardID["MonsieurScarlet"] = 31] = "MonsieurScarlet";
            GlobalCardID[GlobalCardID["GoldenSeedling"] = 32] = "GoldenSeedling";
            GlobalCardID[GlobalCardID["ArrowWorm"] = 33] = "ArrowWorm";
            GlobalCardID[GlobalCardID["Carmina"] = 34] = "Carmina";
            GlobalCardID[GlobalCardID["SeedlingKing"] = 35] = "SeedlingKing";
            GlobalCardID[GlobalCardID["Broodmother"] = 36] = "Broodmother";
            GlobalCardID[GlobalCardID["Plumpling"] = 37] = "Plumpling";
            GlobalCardID[GlobalCardID["Flowerling"] = 38] = "Flowerling";
            GlobalCardID[GlobalCardID["Burglar"] = 39] = "Burglar";
            GlobalCardID[GlobalCardID["Astotheles"] = 40] = "Astotheles";
            GlobalCardID[GlobalCardID["MotherChomper"] = 41] = "MotherChomper";
            GlobalCardID[GlobalCardID["Ahoneynation"] = 42] = "Ahoneynation";
            GlobalCardID[GlobalCardID["BeeBoop"] = 43] = "BeeBoop";
            GlobalCardID[GlobalCardID["SecurityTurret"] = 44] = "SecurityTurret";
            GlobalCardID[GlobalCardID["Denmuki"] = 45] = "Denmuki";
            GlobalCardID[GlobalCardID["HeavyDroneB33"] = 46] = "HeavyDroneB33";
            GlobalCardID[GlobalCardID["Mender"] = 47] = "Mender";
            GlobalCardID[GlobalCardID["Abomihoney"] = 48] = "Abomihoney";
            GlobalCardID[GlobalCardID["DuneScorpion"] = 49] = "DuneScorpion";
            GlobalCardID[GlobalCardID["TidalWyrm"] = 50] = "TidalWyrm";
            GlobalCardID[GlobalCardID["Kali"] = 51] = "Kali";
            GlobalCardID[GlobalCardID["Zombee"] = 52] = "Zombee";
            GlobalCardID[GlobalCardID["Zombeetle"] = 53] = "Zombeetle";
            GlobalCardID[GlobalCardID["TheWatcher"] = 54] = "TheWatcher";
            GlobalCardID[GlobalCardID["PeacockSpider"] = 55] = "PeacockSpider";
            GlobalCardID[GlobalCardID["Bloatshroom"] = 56] = "Bloatshroom";
            GlobalCardID[GlobalCardID["Krawler"] = 57] = "Krawler";
            GlobalCardID[GlobalCardID["HauntedCloth"] = 58] = "HauntedCloth";
            GlobalCardID[GlobalCardID["Warden"] = 61] = "Warden";
            GlobalCardID[GlobalCardID["JumpingSpider"] = 63] = "JumpingSpider";
            GlobalCardID[GlobalCardID["MimicSpider"] = 64] = "MimicSpider";
            GlobalCardID[GlobalCardID["LeafbugNinja"] = 65] = "LeafbugNinja";
            GlobalCardID[GlobalCardID["LeafbugArcher"] = 66] = "LeafbugArcher";
            GlobalCardID[GlobalCardID["LeafbugClubber"] = 67] = "LeafbugClubber";
            GlobalCardID[GlobalCardID["Madesphy"] = 68] = "Madesphy";
            GlobalCardID[GlobalCardID["TheBeast"] = 69] = "TheBeast";
            GlobalCardID[GlobalCardID["ChomperBrute"] = 70] = "ChomperBrute";
            GlobalCardID[GlobalCardID["Mantidfly"] = 71] = "Mantidfly";
            GlobalCardID[GlobalCardID["GeneralUltimax"] = 72] = "GeneralUltimax";
            GlobalCardID[GlobalCardID["WildChomper"] = 73] = "WildChomper";
            GlobalCardID[GlobalCardID["Cross"] = 74] = "Cross";
            GlobalCardID[GlobalCardID["Poi"] = 75] = "Poi";
            GlobalCardID[GlobalCardID["PrimalWeevil"] = 76] = "PrimalWeevil";
            GlobalCardID[GlobalCardID["FalseMonarch"] = 77] = "FalseMonarch";
            GlobalCardID[GlobalCardID["Mothfly"] = 78] = "Mothfly";
            GlobalCardID[GlobalCardID["MothflyCluster"] = 79] = "MothflyCluster";
            GlobalCardID[GlobalCardID["Ironnail"] = 80] = "Ironnail";
            GlobalCardID[GlobalCardID["Belostoss"] = 81] = "Belostoss";
            GlobalCardID[GlobalCardID["Ruffian"] = 82] = "Ruffian";
            GlobalCardID[GlobalCardID["WaterStrider"] = 83] = "WaterStrider";
            GlobalCardID[GlobalCardID["DivingSpider"] = 84] = "DivingSpider";
            GlobalCardID[GlobalCardID["Cenn"] = 85] = "Cenn";
            GlobalCardID[GlobalCardID["Pisci"] = 86] = "Pisci";
            GlobalCardID[GlobalCardID["DeadLander\u03B1"] = 87] = "DeadLander\u03B1";
            GlobalCardID[GlobalCardID["DeadLander\u03B2"] = 88] = "DeadLander\u03B2";
            GlobalCardID[GlobalCardID["DeadLander\u03B3"] = 89] = "DeadLander\u03B3";
            GlobalCardID[GlobalCardID["WaspKing"] = 90] = "WaspKing";
            GlobalCardID[GlobalCardID["TheEverlastingKing"] = 91] = "TheEverlastingKing";
            GlobalCardID[GlobalCardID["Maki"] = 92] = "Maki";
            GlobalCardID[GlobalCardID["Kina"] = 93] = "Kina";
            GlobalCardID[GlobalCardID["Yin"] = 94] = "Yin";
            GlobalCardID[GlobalCardID["UltimaxTank"] = 95] = "UltimaxTank";
            GlobalCardID[GlobalCardID["Zommoth"] = 96] = "Zommoth";
            GlobalCardID[GlobalCardID["Riz"] = 97] = "Riz";
            GlobalCardID[GlobalCardID["Devourer"] = 98] = "Devourer";
        })(GlobalCardID = CardData.GlobalCardID || (CardData.GlobalCardID = {}));
        let BossCardOrder;
        (function (BossCardOrder) {
            BossCardOrder[BossCardOrder["Spider"] = 0] = "Spider";
            BossCardOrder[BossCardOrder["VenusGuardian"] = 1] = "VenusGuardian";
            BossCardOrder[BossCardOrder["HeavyDroneB33"] = 2] = "HeavyDroneB33";
            BossCardOrder[BossCardOrder["TheWatcher"] = 3] = "TheWatcher";
            BossCardOrder[BossCardOrder["TheBeast"] = 4] = "TheBeast";
            BossCardOrder[BossCardOrder["UltimaxTank"] = 5] = "UltimaxTank";
            BossCardOrder[BossCardOrder["MotherChomper"] = 6] = "MotherChomper";
            BossCardOrder[BossCardOrder["Broodmother"] = 7] = "Broodmother";
            BossCardOrder[BossCardOrder["Zommoth"] = 8] = "Zommoth";
            BossCardOrder[BossCardOrder["SeedlingKing"] = 9] = "SeedlingKing";
            BossCardOrder[BossCardOrder["TidalWyrm"] = 10] = "TidalWyrm";
            BossCardOrder[BossCardOrder["PeacockSpider"] = 11] = "PeacockSpider";
            BossCardOrder[BossCardOrder["Devourer"] = 12] = "Devourer";
            BossCardOrder[BossCardOrder["FalseMonarch"] = 13] = "FalseMonarch";
            BossCardOrder[BossCardOrder["Maki"] = 14] = "Maki";
            BossCardOrder[BossCardOrder["WaspKing"] = 15] = "WaspKing";
            BossCardOrder[BossCardOrder["TheEverlastingKing"] = 16] = "TheEverlastingKing";
        })(BossCardOrder = CardData.BossCardOrder || (CardData.BossCardOrder = {}));
        let MiniBossCardOrder;
        (function (MiniBossCardOrder) {
            MiniBossCardOrder[MiniBossCardOrder["Ahoneynation"] = 0] = "Ahoneynation";
            MiniBossCardOrder[MiniBossCardOrder["AcolyteAria"] = 1] = "AcolyteAria";
            MiniBossCardOrder[MiniBossCardOrder["Mothiva"] = 2] = "Mothiva";
            MiniBossCardOrder[MiniBossCardOrder["Zasp"] = 3] = "Zasp";
            MiniBossCardOrder[MiniBossCardOrder["Astotheles"] = 4] = "Astotheles";
            MiniBossCardOrder[MiniBossCardOrder["DuneScorpion"] = 5] = "DuneScorpion";
            MiniBossCardOrder[MiniBossCardOrder["PrimalWeevil"] = 6] = "PrimalWeevil";
            MiniBossCardOrder[MiniBossCardOrder["Cross"] = 7] = "Cross";
            MiniBossCardOrder[MiniBossCardOrder["Poi"] = 8] = "Poi";
            MiniBossCardOrder[MiniBossCardOrder["GeneralUltimax"] = 9] = "GeneralUltimax";
            MiniBossCardOrder[MiniBossCardOrder["Cenn"] = 10] = "Cenn";
            MiniBossCardOrder[MiniBossCardOrder["Pisci"] = 11] = "Pisci";
            MiniBossCardOrder[MiniBossCardOrder["MonsieurScarlet"] = 12] = "MonsieurScarlet";
            MiniBossCardOrder[MiniBossCardOrder["Kabbu"] = 13] = "Kabbu";
            MiniBossCardOrder[MiniBossCardOrder["Kali"] = 14] = "Kali";
            MiniBossCardOrder[MiniBossCardOrder["Carmina"] = 15] = "Carmina";
            MiniBossCardOrder[MiniBossCardOrder["Riz"] = 16] = "Riz";
            MiniBossCardOrder[MiniBossCardOrder["Kina"] = 17] = "Kina";
            MiniBossCardOrder[MiniBossCardOrder["Yin"] = 18] = "Yin";
            MiniBossCardOrder[MiniBossCardOrder["DeadLander\u03B1"] = 19] = "DeadLander\u03B1";
            MiniBossCardOrder[MiniBossCardOrder["DeadLander\u03B2"] = 20] = "DeadLander\u03B2";
            MiniBossCardOrder[MiniBossCardOrder["DeadLander\u03B3"] = 21] = "DeadLander\u03B3";
        })(MiniBossCardOrder = CardData.MiniBossCardOrder || (CardData.MiniBossCardOrder = {}));
        let AttackerCardOrder;
        (function (AttackerCardOrder) {
            AttackerCardOrder[AttackerCardOrder["Seedling"] = 0] = "Seedling";
            AttackerCardOrder[AttackerCardOrder["Underling"] = 1] = "Underling";
            AttackerCardOrder[AttackerCardOrder["GoldenSeedling"] = 2] = "GoldenSeedling";
            AttackerCardOrder[AttackerCardOrder["Zombiant"] = 3] = "Zombiant";
            AttackerCardOrder[AttackerCardOrder["Zombee"] = 4] = "Zombee";
            AttackerCardOrder[AttackerCardOrder["Zombeetle"] = 5] = "Zombeetle";
            AttackerCardOrder[AttackerCardOrder["Jellyshroom"] = 6] = "Jellyshroom";
            AttackerCardOrder[AttackerCardOrder["Bloatshroom"] = 7] = "Bloatshroom";
            AttackerCardOrder[AttackerCardOrder["Chomper"] = 8] = "Chomper";
            AttackerCardOrder[AttackerCardOrder["ChomperBrute"] = 9] = "ChomperBrute";
            AttackerCardOrder[AttackerCardOrder["Psicorp"] = 10] = "Psicorp";
            AttackerCardOrder[AttackerCardOrder["ArrowWorm"] = 11] = "ArrowWorm";
            AttackerCardOrder[AttackerCardOrder["Thief"] = 12] = "Thief";
            AttackerCardOrder[AttackerCardOrder["Bandit"] = 13] = "Bandit";
            AttackerCardOrder[AttackerCardOrder["Burglar"] = 14] = "Burglar";
            AttackerCardOrder[AttackerCardOrder["Ruffian"] = 15] = "Ruffian";
            AttackerCardOrder[AttackerCardOrder["SecurityTurret"] = 16] = "SecurityTurret";
            AttackerCardOrder[AttackerCardOrder["Abomihoney"] = 17] = "Abomihoney";
            AttackerCardOrder[AttackerCardOrder["Krawler"] = 18] = "Krawler";
            AttackerCardOrder[AttackerCardOrder["Warden"] = 19] = "Warden";
            AttackerCardOrder[AttackerCardOrder["HauntedCloth"] = 20] = "HauntedCloth";
            AttackerCardOrder[AttackerCardOrder["Mantidfly"] = 21] = "Mantidfly";
            AttackerCardOrder[AttackerCardOrder["JumpingSpider"] = 22] = "JumpingSpider";
            AttackerCardOrder[AttackerCardOrder["MimicSpider"] = 23] = "MimicSpider";
            AttackerCardOrder[AttackerCardOrder["DivingSpider"] = 24] = "DivingSpider";
            AttackerCardOrder[AttackerCardOrder["WaterStrider"] = 25] = "WaterStrider";
            AttackerCardOrder[AttackerCardOrder["Belostoss"] = 26] = "Belostoss";
            AttackerCardOrder[AttackerCardOrder["Mothfly"] = 27] = "Mothfly";
            AttackerCardOrder[AttackerCardOrder["MothflyCluster"] = 28] = "MothflyCluster";
            AttackerCardOrder[AttackerCardOrder["WaspScout"] = 29] = "WaspScout";
            AttackerCardOrder[AttackerCardOrder["WaspTrooper"] = 30] = "WaspTrooper";
        })(AttackerCardOrder = CardData.AttackerCardOrder || (CardData.AttackerCardOrder = {}));
        let EffectCardOrder;
        (function (EffectCardOrder) {
            EffectCardOrder[EffectCardOrder["Acornling"] = 0] = "Acornling";
            EffectCardOrder[EffectCardOrder["Cactiling"] = 1] = "Cactiling";
            EffectCardOrder[EffectCardOrder["Flowerling"] = 2] = "Flowerling";
            EffectCardOrder[EffectCardOrder["Plumpling"] = 3] = "Plumpling";
            EffectCardOrder[EffectCardOrder["Inichas"] = 4] = "Inichas";
            EffectCardOrder[EffectCardOrder["Denmuki"] = 5] = "Denmuki";
            EffectCardOrder[EffectCardOrder["Madesphy"] = 6] = "Madesphy";
            EffectCardOrder[EffectCardOrder["Numbnail"] = 7] = "Numbnail";
            EffectCardOrder[EffectCardOrder["Ironnail"] = 8] = "Ironnail";
            EffectCardOrder[EffectCardOrder["Midge"] = 9] = "Midge";
            EffectCardOrder[EffectCardOrder["WildChomper"] = 10] = "WildChomper";
            EffectCardOrder[EffectCardOrder["Weevil"] = 11] = "Weevil";
            EffectCardOrder[EffectCardOrder["BeeBoop"] = 12] = "BeeBoop";
            EffectCardOrder[EffectCardOrder["Mender"] = 13] = "Mender";
            EffectCardOrder[EffectCardOrder["LeafbugNinja"] = 14] = "LeafbugNinja";
            EffectCardOrder[EffectCardOrder["LeafbugArcher"] = 15] = "LeafbugArcher";
            EffectCardOrder[EffectCardOrder["LeafbugClubber"] = 16] = "LeafbugClubber";
            EffectCardOrder[EffectCardOrder["WaspBomber"] = 17] = "WaspBomber";
            EffectCardOrder[EffectCardOrder["WaspDriller"] = 18] = "WaspDriller";
            EffectCardOrder[EffectCardOrder["VenusBud"] = 19] = "VenusBud";
        })(EffectCardOrder = CardData.EffectCardOrder || (CardData.EffectCardOrder = {}));
        CardData.CardNameOverride = {
            [GlobalCardID.VenusBud]: "Venus' Bud",
            [GlobalCardID.AcolyteAria]: "Acolyte Aria",
            [GlobalCardID.VenusGuardian]: "Venus' Guardian",
            [GlobalCardID.WaspTrooper]: "Wasp Trooper",
            [GlobalCardID.WaspBomber]: "Wasp Bomber",
            [GlobalCardID.WaspDriller]: "Wasp Driller",
            [GlobalCardID.WaspScout]: "Wasp Scout",
            [GlobalCardID.MonsieurScarlet]: "Monsieur Scarlet",
            [GlobalCardID.GoldenSeedling]: "Golden Seedling",
            [GlobalCardID.ArrowWorm]: "Arrow Worm",
            [GlobalCardID.SeedlingKing]: "Seedling King",
            [GlobalCardID.MotherChomper]: "Mother Chomper",
            [GlobalCardID.BeeBoop]: "Bee-Boop",
            [GlobalCardID.SecurityTurret]: "Security Turret",
            [GlobalCardID.HeavyDroneB33]: "Heavy Drone B-33",
            [GlobalCardID.DuneScorpion]: "Dune Scorpion",
            [GlobalCardID.TidalWyrm]: "Tidal Wyrm",
            [GlobalCardID.TheWatcher]: "The Watcher",
            [GlobalCardID.PeacockSpider]: "Peacock Spider",
            [GlobalCardID.HauntedCloth]: "Haunted Cloth",
            [GlobalCardID.JumpingSpider]: "Jumping Spider",
            [GlobalCardID.MimicSpider]: "Mimic Spider",
            [GlobalCardID.LeafbugNinja]: "Leafbug Ninja",
            [GlobalCardID.LeafbugArcher]: "Leafbug Archer",
            [GlobalCardID.LeafbugClubber]: "Leafbug Clubber",
            [GlobalCardID.TheBeast]: "The Beast",
            [GlobalCardID.ChomperBrute]: "Chomper Brute",
            [GlobalCardID.GeneralUltimax]: "General Ultimax",
            [GlobalCardID.WildChomper]: "Wild Chomper",
            [GlobalCardID.PrimalWeevil]: "Primal Weevil",
            [GlobalCardID.FalseMonarch]: "False Monarch",
            [GlobalCardID.MothflyCluster]: "Mothfly Cluster",
            [GlobalCardID.WaterStrider]: "Water Strider",
            [GlobalCardID.DivingSpider]: "Diving Spider",
            [GlobalCardID.DeadLanderα]: "Dead Lander α",
            [GlobalCardID.DeadLanderβ]: "Dead Lander β",
            [GlobalCardID.DeadLanderγ]: "Dead Lander γ",
            [GlobalCardID.WaspKing]: "Wasp King",
            [GlobalCardID.TheEverlastingKing]: "The Everlasting King",
            [GlobalCardID.UltimaxTank]: "ULTIMAX Tank"
        };
        const vanillaCardByName = {};
        for (let card = 0; card < 128; card++) {
            if (!GlobalCardID[card]) {
                continue;
            }
            if (CardData.CardNameOverride[card]) {
                vanillaCardByName[CardData.CardNameOverride[card]] = card;
            }
            else {
                vanillaCardByName[GlobalCardID[card]] = card;
            }
        }
        // typo fixes
        vanillaCardByName["Belosstoss"] = GlobalCardID.Belostoss;
        function cardByName(name) {
            if (name.startsWith("Custom ")) {
                let rank;
                let index;
                if (name.startsWith("Custom Attacker #")) {
                    rank = SpyCards.Rank.Attacker;
                    index = parseInt(name.substr("Custom Attacker #".length), 10);
                }
                else if (name.startsWith("Custom Effect #")) {
                    rank = SpyCards.Rank.Effect;
                    index = parseInt(name.substr("Custom Effect #".length), 10);
                }
                else if (name.startsWith("Custom Mini-Boss #")) {
                    rank = SpyCards.Rank.MiniBoss;
                    index = parseInt(name.substr("Custom Mini-Boss #".length), 10);
                }
                else if (name.startsWith("Custom Boss #")) {
                    rank = SpyCards.Rank.Boss;
                    index = parseInt(name.substr("Custom Boss #".length), 10);
                }
                else {
                    throw new Error("unknown card name: " + name);
                }
                if (index >= 1) {
                    index--;
                    return 128 + ((index & ~31) << 2) | (rank << 5) | (index & 31);
                }
            }
            if (vanillaCardByName.hasOwnProperty(name)) {
                return vanillaCardByName[name];
            }
            throw new Error("unknown card name: " + name);
        }
        CardData.cardByName = cardByName;
    })(CardData = SpyCards.CardData || (SpyCards.CardData = {}));
})(SpyCards || (SpyCards = {}));
